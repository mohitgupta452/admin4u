package com.java;

public interface AlertMessages {
	
	String requestSent="Request has been  successfully submitted thank you";
	String roleAssigned="Role Successfully Assigned";
	String bookTicket="Ticket Successfully Booked";
	String downloadExcel="Excel File Successfully Downloaded";
	String uploadExcel="Dropdown Value Successfully Updated with Excel";
	String serviceUpdateSuccess="{\"success\":\"Successfully Updated\"}";
	String ServiceUpdateFailed="{\"failed\":\"failed to  Update\"}";
	String spocBookTicket="Ticket Successfully Booked";
	String MappingSaved="Dept-Cost Center Map Successfuly Saved";
	String seatAllocated="Seat Successfully Allocated";
	String seatNotAllocated="Seat Can't be Allocated";
	String smNotResponded="Alert Regarding not responding to Space Management request";



}
