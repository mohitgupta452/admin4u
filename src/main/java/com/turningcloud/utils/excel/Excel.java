package com.turningcloud.utils.excel;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.HSSFColor.YELLOW;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Himanshu Agarwal
 */
public abstract class Excel {
	private static Logger log = LoggerFactory.getLogger(Excel.class);

	private String workBookName = null;
	protected XSSFWorkbook wb = null;
	protected XSSFSheet sheet = null;
	protected File file = null;

	// will create a new xlsx file in specified location on executing
	// generateExcelFile() method
	@SuppressWarnings("deprecation")
	public Excel(String path, String workBookName, Boolean protectedExcel) throws Exception {
		if (path == null || path.equals("")) {
			throw new Exception("please provide parentDirectory");
		}
		if (workBookName == null || workBookName.equals("")) {
			this.workBookName = "default[" + new Date().getDate() + "].xlsx";
		} else
			this.workBookName = workBookName;

		this.file = new File(path + File.separator + this.workBookName);
		if (!(this.file.exists())) {
			log.debug("folders not exists..");
			(new File(file.getParent())).mkdirs();
			log.debug("folder created...");
		}
		this.wb = new XSSFWorkbook();
		this.sheet = this.wb.createSheet();
		if (protectedExcel)
			this.sheet.protectSheet("createCellStyle();");
		log.debug("Excel Created....");
	}

	// will read the specified Excel file
	public Excel(File file) throws Exception {

		wb = new XSSFWorkbook(file);
		sheet = wb.getSheetAt(0);

		this.file = file;
		log.debug("Excel Created....");
	}

	protected void closeExcelFile() {
		try {
			this.wb.close();
			log.debug("file closed");
		} catch (IOException ioe) {
			log.error(this.getClass().getName() + ioe.getMessage());
		}
	}

	protected void setLocked(String colomn, int startCell, int endCell, boolean status) {
		CellStyle style = this.wb.createCellStyle();
		style.setLocked(false);
		style.setWrapText(true);
		XSSFCell cell;

		for (int i = startCell; i <= endCell; i++) {
			cell = this.getCell(colomn + i);
			cell.setCellStyle(style);
		}
	}

	protected void setDatePatternWithLock(String datePattern, String colomn, int startCell, int endCell, boolean status) {
		CellStyle style = this.wb.createCellStyle();
		style.setLocked(false);
		style.setWrapText(true);
		CreationHelper createHelper = wb.getCreationHelper();
		style.setDataFormat(createHelper.createDataFormat().getFormat(datePattern));
		XSSFCell cell;

		for (int i = startCell; i <= endCell; i++) {
			cell = this.getCell(colomn + i);
			cell.setCellStyle(style);
		}
	}

	protected XSSFCell getCell(String cellName) {
		CellReference ref = new CellReference(cellName);
		XSSFRow row = sheet.getRow(ref.getRow());
		if (row == null) {
			row = sheet.createRow(ref.getRow());
		}

		XSSFCell cell = row.getCell(ref.getCol());
		if (cell == null) {
			cell = row.createCell(ref.getCol());
		}

		return cell;
	}

	public void mergeCell(String startCell, String endCell) {
		CellRangeAddress region = CellRangeAddress.valueOf(startCell + ":" + endCell);
		sheet.addMergedRegion(region);
	}

	public void setCellValue(String cellName, String value) {
		CellStyle cs = wb.createCellStyle();
		cs.setWrapText(true);
		cs.setBorderLeft((short) 2);
		cs.setBorderRight((short) 2);
		cs.setBorderBottom((short) 2);
		cs.setBorderTop((short) 2);
		if (value.contains(","))
			cs.setAlignment(cs.ALIGN_LEFT);
		else
			cs.setAlignment(cs.ALIGN_CENTER);
		cs.setVerticalAlignment(cs.VERTICAL_CENTER);
		cs.setWrapText(true);
		String crLf = Character.toString((char) 13) + Character.toString((char) 10);
		XSSFCell cell = this.getCell(cellName);
		cell.setCellStyle(cs);
		cell.setCellValue(value.replaceAll(", ", "," + crLf));

	}

	public void setCellValue(String cellName, Date value) {

		XSSFCell cell = this.getCell(cellName);

		cell.setCellValue(value);

	}

	public void setHeaders(Map<String, String> headers) {
		log.debug(this.getClass() + "setHeaders() call starts..");
		for (Entry<String, String> header : headers.entrySet()) {
			XSSFCell cell = this.getCell(header.getKey());
			cell.setCellValue(header.getValue());
			this.sheet.autoSizeColumn(cell.getColumnIndex());

			XSSFCellStyle Cell = wb.createCellStyle();
			// DEclare Font
			XSSFFont font = wb.createFont();
			font.setColor((short) 1);
			Cell.setLocked(true);
			Cell.setWrapText(true);
			Cell.setFillBackgroundColor(new XSSFColor(new byte[] { 127, 127, 127 }));
			Cell.setFillPattern(CellStyle.ALIGN_FILL);
			sheet.setColumnWidth(cell.getColumnIndex(), 6500);
			font.setBold(true);
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
			Cell.setFont(font);
			// Declare rowExcel Format
			Cell.setDataFormat(wb.createDataFormat().getFormat("#,#0.00"));
			// Declare Alignment
			Cell.setAlignment(Cell.ALIGN_CENTER);

			cell.setCellStyle(Cell);

		}
		log.debug(this.getClass() + "setHeaders() call End..");
	}

	public void setDataValidations(Map<String, String[]> validations) {
		log.debug(this.getClass() + "setDataValidations() call starts..");
		XSSFCell firstCell;
		XSSFCell lastCell;

		DataValidationHelper validationHelper = new XSSFDataValidationHelper(this.sheet);

		for (Entry<String, String[]> entry : validations.entrySet()) {

			String[] cellRange = entry.getKey().split(":");
			firstCell = getCell(cellRange[0]);
			lastCell = getCell(cellRange[1]);

			CellRangeAddressList range = new CellRangeAddressList();
			range.addCellRangeAddress(firstCell.getRowIndex(), firstCell.getColumnIndex(), lastCell.getRowIndex(),
					lastCell.getColumnIndex());

			DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(entry.getValue());

			DataValidation dataValidation = validationHelper.createValidation(constraint, range);
			dataValidation.setErrorStyle(DataValidation.ErrorStyle.WARNING);
			dataValidation.setSuppressDropDownArrow(true);
			sheet.addValidationData(dataValidation);
		}
		log.debug(this.getClass() + "setDataValidations() call End..");
	}

	public String excelReadSingleValue(String cellName) throws IOException {
		log.debug(cellName + "Reads");
		return this.getCell(cellName).getStringCellValue();

	}

	protected String[] excelReadRow(String cellRange) throws IOException {
		log.debug(cellRange + "Range Reads");
		String[] rowExcel;
		String[] cells = cellRange.split(":");

		CellReference stRow = new CellReference(cells[0]);
		CellReference endRow = new CellReference(cells[1]);
		rowExcel = new String[(endRow.getCol() - stRow.getCol()) + 1];
		int j = 0;
		for (int i = stRow.getCol(); i <= endRow.getCol(); i++) {
			// rowExcel.add(excelReadSingleValue("" +
			// ((char)(stRow.getCol()+65))+""+i ));
			rowExcel[j++] = excelReadSingleValue("" + ((char) (i + 65)) + (stRow.getRow() + 1));
		}
		return rowExcel;
	}

	public List<String> excelReadMultipleRowValue(String cellRange) throws IOException {
		return null;

	}

	public File generateExcelFile() {
		log.debug(this.getClass() + "generateExcelFile() call Starts..");
		FileOutputStream outputFile = null;
		try {
			if (file.exists()) {
				file.delete();
				log.info("file updated....by deleating previous file");
			}
			outputFile = new FileOutputStream(this.file);

			wb.write(outputFile);
			outputFile.flush();
			log.info("Date Successfully Inserted....");
			this.file.setReadOnly();

		} catch (Exception e) {
			log.error(this.getClass() + "generateExcelFile() having error" + e.getMessage());
		} finally {
			try {
				outputFile.close();
				this.closeExcelFile();
			} catch (IOException ioe) {

				log.error(this.getClass() + "generateExcelFile() file not close having error" + ioe.getMessage());
			}
		}

		log.debug(this.getClass() + "generateExcelFile() call Ends..");
		return this.file;
	}

}