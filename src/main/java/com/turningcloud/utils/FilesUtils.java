package com.turningcloud.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javax.servlet.http.Part;

import org.apache.commons.lang3.SystemUtils;

import com.admin4u.util.service.MultipartUtils;


/**
 * 
 * @author Ashish Singh Dev
 */
public final class FilesUtils {

	public static String saveFile(File parentDir, Part part) {

		File originalFile = null;
		String savedPath = null;

		try (InputStream input = part.getInputStream()) {

			originalFile = new File(parentDir, MultipartUtils.extractFileName(part));

			if (!(originalFile.exists()))
				(new File(originalFile.getParent())).mkdirs();

			if (originalFile.exists())
				originalFile.delete();

			Files.copy(input, originalFile.toPath());
			savedPath = originalFile.getAbsolutePath();

		} catch (IOException e) {
		} finally {

		}

		return savedPath;
	}

	public static File getPlatformBasedParentDir() {
		File parentDir = null;

		if (SystemUtils.IS_OS_WINDOWS) {
			parentDir = new File("c:"+File.separator+"admin4u");

		} else if (SystemUtils.IS_OS_LINUX) {
			parentDir = new File(SystemUtils.USER_HOME + File.separator + "admin4u");

		} else if (SystemUtils.IS_OS_MAC) {
			parentDir = new File(SystemUtils.USER_HOME + File.separator + "admin4u");
		}

		return parentDir;
	}
}
