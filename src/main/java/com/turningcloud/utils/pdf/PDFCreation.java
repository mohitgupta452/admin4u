package com.turningcloud.utils.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
//import com.turningcloud.utils.jsf.JSFUtils;

/**
 *
 * @author Himanshu Agarwal
 */
public class PDFCreation {

	protected static final Logger log = LoggerFactory.getLogger(PDFCreation.class);

	private File file;
	private Document pdfDoc;
	private PdfPTable my_table;
	private PdfPCell table_cell;
	private int colNo;
	private Paragraph paragraph;
	private static PdfWriter pdfWriter;

	public PDFCreation(String path, String fileName) throws Exception {

		this.file = new File(path, fileName);
		if (!(this.file.exists())) {
			log.debug("folders not exists..");
			(new File(file.getParent())).mkdirs();
			log.debug("folder created...");
		}
		
		FileOutputStream baos = new FileOutputStream(file);
		this.createPdf(baos);
	}

	/*public PDFCreation(File file) throws Exception {
		this.file = file;
		this.createPdf(boas);
	}*/

	private void createPdf(FileOutputStream baos) throws Exception
	{
		pdfDoc = new Document();
		pdfWriter = PdfWriter.getInstance(pdfDoc,baos );
		pdfDoc.open();
		PdfContentByte canvas = pdfWriter.getDirectContentUnder();
	//	Image image = Image.getInstance(JSFUtils.getEC().getRealPath("/assets/images/dalmia_logo.png"));
		// image.scaleAbsolute(PageSize.A4);
	//	image.setAbsolutePosition(500f, 800f);
	//	canvas.addImage(image);

	}
	
	

	public void createTable(int colNo, List<String> headingList, BaseColor color, BaseColor textColor)
			throws Exception {
		my_table = new PdfPTable(colNo);
		this.colNo = colNo;
		this.createTableHeading(headingList, color, textColor);
	}

	public void createTable(int colNo, String headingSeperatedByComma, BaseColor color, BaseColor textColor)
			throws Exception {
		my_table = new PdfPTable(colNo);
		my_table.setWidthPercentage(100f);
		this.colNo = colNo;
		this.createTableHeading(headingSeperatedByComma, color, textColor);
	}

	public void addData(String data) throws Exception {
		if (data.contains("[")) {
			data = (data).replace('[', ' ');
			data = (data).replace(']', ' ');
		}

		Font f = new Font(FontFamily.COURIER, 10.0f, Font.NORMAL, BaseColor.BLACK);
		Paragraph paragraph = new Paragraph(data, f);
		pdfDoc.add(paragraph);
	}
	
// function for heading	
	public void addHeader(String data)throws Exception{
		if (data.contains("[")) {
			data = (data).replace('[', ' ');
			data = (data).replace(']', ' ');
		}
		Font f = new Font(FontFamily.COURIER, 17.0f, Font.BOLD, BaseColor.BLACK);
		Paragraph paragraph = new Paragraph(data, f);
		pdfDoc.add(paragraph);
	}

	public void addDataAtCenter(String data) throws Exception {
		if (data.contains("[")) {
			data = (data).replace('[', ' ');
			data = (data).replace(']', ' ');
		}

		Font f = new Font(FontFamily.COURIER, 13.0f, Font.NORMAL, BaseColor.BLACK);
		Chunk c = new Chunk(data, f);
		c.setBackground(BaseColor.LIGHT_GRAY);

		Paragraph paragraph = new Paragraph(c);
		paragraph.setAlignment(Paragraph.ALIGN_CENTER);
		pdfDoc.add(paragraph);
	}

	private void createTableHeading(List<String> headingList, BaseColor color, BaseColor textColor)
			throws Exception, Exception {
		if (this.colNo == headingList.size()) {
			for (Object data : headingList) {
				table_cell = new PdfPCell(
						new Phrase(data.toString(), new Font(FontFamily.COURIER, 11f, Font.NORMAL, textColor)));
				table_cell.setBackgroundColor(color);
				table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table_cell.setBorder(5);
				table_cell.setBorderColor(BaseColor.BLACK);
				my_table.addCell(table_cell);
			}
		}
	}

	private void createTableHeading(String heading, BaseColor color, BaseColor textColor) throws Exception, Exception {
		String[] headingArray = heading.split(",");
		if (this.colNo == headingArray.length) {
			for (int i = 0; i < headingArray.length; i++) {
				table_cell = new PdfPCell(new Phrase(headingArray[i].toString(),
						new Font(FontFamily.COURIER, 11f, Font.NORMAL, textColor)));
				table_cell.setBackgroundColor(color);
				table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table_cell.setBorder(5);
				table_cell.setBorderColor(BaseColor.BLACK);
				my_table.addCell(table_cell);
			}
		}
	}

	public void createTableRowWithPading(String data, int rowSpan, int colSpan, BaseColor backGroundColor,
			BaseColor textColor) throws DocumentException, IOException {
		table_cell = new PdfPCell(new Phrase(data, new Font(FontFamily.COURIER, 9f, Font.NORMAL, textColor)));
		table_cell.setRowspan(rowSpan);
		table_cell.setColspan(colSpan);
		table_cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table_cell.setBackgroundColor(backGroundColor);
		my_table.addCell(table_cell);
	}

	public void createTableRow(String dataSeperatedByColon, boolean generateExtraCol)
			throws DocumentException, IOException {
		String[] dataArray = dataSeperatedByColon.split(":");
		if (!generateExtraCol || this.colNo == dataArray.length) {
			for (int i = 0; i < dataArray.length; i++) {
				if (dataArray[i].contains(",")) {
					String function = (dataArray[i]).substring(1, ((dataArray[i]).length() - 1));
					String[] tempString = function.split(",");
					String str = "";
					for (int j = 0; j < tempString.length; j++) {
						str += "-" + tempString[j].trim() + "" + Chunk.NEWLINE;
					}
					table_cell = new PdfPCell(
							new Phrase(str, new Font(FontFamily.COURIER, 9f, Font.NORMAL, BaseColor.BLACK)));
				}

				else if (dataArray[i].contains("[")) {
					String function = (dataArray[i]).substring(1, ((dataArray[i]).length() - 1));
					table_cell = new PdfPCell(
							new Phrase("-" + function, new Font(FontFamily.COURIER, 9f, Font.NORMAL, BaseColor.BLACK)));
				} else {
					table_cell = new PdfPCell(new Phrase(dataArray[i].toString(),
							new Font(FontFamily.COURIER, 9f, Font.NORMAL, BaseColor.BLACK)));
				}

				if (dataArray[i].contains(",") || dataArray[i].contains("[")) {
					table_cell.setVerticalAlignment(Element.ALIGN_LEFT);
				} else {
					table_cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				}

				my_table.addCell(table_cell);
			}
		} else if (generateExtraCol && this.colNo >= dataArray.length) {
			for (int i = 0; i < dataArray.length; i++) {
				if (dataArray[i].contains(",")) {
					String function = (dataArray[i]).substring(1, ((dataArray[i]).length() - 1));
					String[] tempString = function.split(",");
					String str = "";
					for (int j = 0; j < tempString.length; j++) {
						str += tempString[j] + "\n";
					}
					table_cell = new PdfPCell(
							new Phrase(str, new Font(FontFamily.COURIER, 10f, Font.NORMAL, BaseColor.BLACK)));
				} else if (dataArray[i].contains("[")) {
					String function = (dataArray[i]).substring(1, ((dataArray[i]).length() - 1));
					table_cell = new PdfPCell(
							new Phrase(function, new Font(FontFamily.COURIER, 10f, Font.NORMAL, BaseColor.BLACK)));
				} else {
					table_cell = new PdfPCell(new Phrase(dataArray[i].toString(),
							new Font(FontFamily.COURIER, 10f, Font.NORMAL, BaseColor.BLACK)));
				}
				table_cell.setVerticalAlignment(Element.ALIGN_CENTER);
				table_cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				my_table.addCell(table_cell);
			}
			for (int i = dataArray.length; i < colNo; i++) {
				table_cell = new PdfPCell(new Phrase(""));
				my_table.addCell(table_cell);
			}
		} else {
			log.error("data is larger than coloumn please correct the data..");
		}
	}

	public void createTableRow(List<Object> dataList) {
		if (this.colNo == dataList.size()) {
			for (Object data : dataList) {
				table_cell = new PdfPCell(new Phrase(data.toString()));
				my_table.addCell(table_cell);
			}
		} else if (this.colNo >= dataList.size()) {
			for (Object data : dataList) {
				table_cell = new PdfPCell(new Phrase(data.toString()));
				my_table.addCell(table_cell);
			}
			for (int i = dataList.size(); i < colNo; i++) {
				table_cell = new PdfPCell(new Phrase(""));
				my_table.addCell(table_cell);
			}
		} else {
			log.error("data is larger than coloumn please correct the data..");
		}
	}

	public void addTable(int[] colWidths) {
		try {
			my_table.setWidths(colWidths);
			pdfDoc.add(Chunk.NEWLINE);
			pdfDoc.add(my_table);
		} catch (DocumentException e) {
			log.error(e.getMessage());
		}
	}

	public void addTable() {
		try {
			pdfDoc.add(Chunk.NEWLINE);
			pdfDoc.add(my_table);
		} catch (DocumentException e) {
			log.error(e.getMessage());
		}
	}

	public void addSpace() {
		try {
			pdfDoc.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public File closePDF() {
		pdfDoc.close();
		return this.file;
	}

	/*
	 * public static void main(String[] args) throws Exception { List ss = new
	 * LinkedList(); ss.add("dfdsvds"); ss.add(155); ss.add("aaaa");
	 * ss.add("ccccccccccc"); ss.add(454); ss.add(5.154); ss.add("aaaa");
	 * ss.add("ccccccccccc"); ss.add("dfdsvds"); ss.add("eeeeeee"); PDFCreation
	 * pdfCreation = new PDFCreation("e:", "hxDSADSDSsh.pdf");
	 * pdfCreation.addData("sadsadsdss"); pdfCreation.addData("sadsadsdss");
	 * pdfCreation.addData("sadsadsdss"); pdfCreation.createTable(5,
	 * "fdf,fdsf,fsdf,fsdf,dfsef");
	 * pdfCreation.createTableRow("21,212,12,12,121");
	 * pdfCreation.createTableRow("re,ere,er,er,as");
	 * pdfCreation.createTableRow(
	 * "dsfsdfadasdasdads,dsadadsdasdeWSQWQ,WEWEWEWREWTREY,RYTUYTIUIYTHGSDFDDSA,5"
	 * ); pdfCreation.addTable(); pdfCreation.closePDF();
	 * 
	 * }
	 */

}
