package com.admin4u.servlet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUtils;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.MultipartUtils;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.UserSessionBean;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

@WebServlet("/AirTicketRequestServlet")
@MultipartConfig

public class AirTicketRequestServlet extends HttpServlet {
	
	
	
    private static final long serialVersionUID = 1L;
	Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private RequestResponseManager reqResManager;
	
	@Inject
	private UserSessionBean userSession;
    

    public AirTicketRequestServlet() {
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	 Map<String, String[]> reqMap = request.getParameterMap();
        
    	 //request.
         log.debug("atr servlet call()");
         String upn = userSession.getEmpData().getUserPrincipalName();
         
         String location=userSession.getLocationName();
         
         String action=request.getParameter("action").toString();
         
         if(action.equals("GENERATED"))
         {
        	 int total=Integer.parseInt(request.getParameter("totalPassengers"));
        	 List<String> paxNameList=new ArrayList<>();
        	// List<String> paxIdList=new ArrayList<>();
        	// List<String>	paxAgeList=new ArrayList<>();
        	 for(int counter=1;counter<=total;counter++){
        		 paxNameList.add(request.getParameter("passenger"+counter));
        		 //paxIdList.add(request.getParameter("employeeID"+counter));
        		// paxAgeList.add(request.getParameter("age"+counter));
        	 }
        	  reqMap.put("passenger", paxNameList.toArray(new String[0]))  ;
        	  reqMap.put("totalPax", new String[]{String.valueOf(total)});
        	 // reqMap.put("employeeID", paxIdList.toArray(new String[0]))  ;	 
        	 // reqMap.put("age", paxAgeList.toArray(new String[0]))  ;	 

        	  
        	  
        	 String serviceList=""; 
        	serviceList=Services.AIRTICKET.toString();
 			
 			if(request.getParameter("avail_cab") != null)
 				serviceList+="|"+"CAB";
 			if(request.getParameter("avail_accomodation") != null)
 				serviceList+="|"+Services.GUEST_HOUSE_BOOKING.toString();
    reqMap.put("serviceList",new String[] {serviceList});
    
	reqMap.put(RequestMetadata.REQUEST_CREATION_TIME,new String []{new Date().toString()});
	reqMap.put(RequestMetadata.LOCATION,new String[]{location});
	reqMap.put(RequestMetadata.REQUESTER,new String[] {upn});

	reqMap.put(RequestMetadata.Service,new String[] {Services.AIRTICKET.toString()});
	reqMap.put(RequestMetadata.Request_IP, new String []{request.getRemoteAddr()});
	Part part=request.getPart("upload");
	

	String filePath = File.separator + "Request"
			+ File.separator
			+ "Approoval";
	
	String upload=FilesUtils.saveFile(new File(FilesUtils.getPlatformBasedParentDir(),filePath), part);
	
	reqMap.put("upload",new String[] {upload});
	   
	String respMsg="";
	    if(reqResManager.insertData(reqMap) == null)
		   respMsg="Sorry Your Manager is not mapped ";
	    else
	    	respMsg =AlertMessages.requestSent;
	
	 HttpSession session = request.getSession(false);
     session.setAttribute("isPRG", "true");
	 response.sendRedirect("PRGServlet?status=success&url=user/tour_travel.jsp&alertMessage="+respMsg);
         }
     
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	doGet(request, response);
    	
    	
    }
}	