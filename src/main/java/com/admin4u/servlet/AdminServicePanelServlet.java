package com.admin4u.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.dto.SpaceManagementManager;
import com.admin4u.persistence.entity.LocServiceMap;
import com.admin4u.persistence.entity.NonApprovalDesignation;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.persistence.entity.SMFloor;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.ServiceManager;
import com.admin4u.util.enums.SMSpaceStationType;
import com.admin4u.views.UserSessionBean;
import com.java.AlertMessages;

@WebServlet("/AdminServicePanelServlet")
public class AdminServicePanelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private LocationManger locationManager;
	
	@Inject 
	private ServiceManager serviceManager;

	  @Inject
	 UserSessionBean userSessionBean;
	  @Inject
	 private SpaceManagementManager smManager; 

    public AdminServicePanelServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String action = request.getParameter("action");
		
		if(action.equalsIgnoreCase("manageServices")){
		String serviceName=request.getParameter("serviceName");
		Byte isActive=Byte.parseByte(request.getParameter("isActive"));
		String location=userSessionBean.getLocationName();
		String createdBy=userSessionBean.getUser().getDisplayName();
		LocServiceMap service=new LocServiceMap();
		service.setService(serviceName);
		service.setIsActive(isActive);
		service.setLocationName(location);
		service.setCreatedBy(createdBy);
		if(locationManager.turnOnOffServiceByServiceNameAndLocation(service))
		{	
			String successMsg=AlertMessages.serviceUpdateSuccess;
			response.setContentType("application/json");
			response.getWriter().write(successMsg);
		}
		else
		{
			String failedMsg=AlertMessages.ServiceUpdateFailed;
			response.setContentType("application/json");
			response.getWriter().write(failedMsg);
			
		}
		}
		
		
		
		else if(action.equalsIgnoreCase("manageDesignation"))
		{
			String designation = request.getParameter("designation");
			
			StringTokenizer updateDesig = new StringTokenizer(designation, "[,]");
     		
	     		   while (updateDesig.hasMoreTokens()) {  
	     			
	     	String  desig = updateDesig.nextToken().replace("\"", "");
	     	Byte isActive=Byte.parseByte(request.getParameter("isActive"));
			String updatedBy=userSessionBean.getUser().getDisplayName();
			NonApprovalDesignation  nonAppDesig = new NonApprovalDesignation();
			nonAppDesig.setActive(isActive);
			nonAppDesig.setDesignation(desig);
			nonAppDesig.setUpdatedBy(updatedBy);
			nonAppDesig.setUpdationTime(new Date());
			serviceManager.addNonApprovalDesignation(nonAppDesig);
	     	
	     		     } 
			
	     		String successMsg=AlertMessages.serviceUpdateSuccess;
	  			response.setContentType("application/json");
	  			response.getWriter().write(successMsg);
	
			
		}
		else if("MANAGESPACE".equalsIgnoreCase(action)){
				int buildingId=Integer.parseInt(request.getParameter("building"));
				int floorId=Integer.parseInt(request.getParameter("buildingFloor"));
				//String buildingLocation=request.getParameter("buildingLocation");
				String spaceCategory=request.getParameter("spaceCategory");
				int spaceAvailable=Integer.parseInt(request.getParameter("spaceAvail"));
				String setSpace=request.getParameter("setSpaceSetting");
				String completePattern="";
				int startRange=1;
				int endRange=spaceAvailable;
				if(!("None".equalsIgnoreCase(setSpace))){
				
				String buildingPattern=request.getParameter("buildingPattern");
				String floorPattern=request.getParameter("floorPattern");
				String spacePattern=request.getParameter("spacePattern");
				 startRange=Integer.parseInt(request.getParameter("startRange"));
				 endRange=Integer.parseInt(request.getParameter("endRange"));
			    completePattern=buildingPattern+"-"+floorPattern+"-"+spacePattern;

						
				
				}
				
				if(SMSpaceStationType.WORKSTATION.toString().equalsIgnoreCase(spaceCategory)){
					
					if(!("None".equalsIgnoreCase(setSpace))){
						log.debug("inside none {},{},{},{}",floorId,startRange,endRange,completePattern);

					smManager.addSeatsToFloorWithPattern(floorId, startRange, endRange,completePattern);
					}
					else
					{
						smManager.addSeatsToFloor(floorId, startRange, endRange);
					}
				}
				else {
					
					List<SMRoom> roomList=new ArrayList<>();
					int count=startRange;
					while(count<=endRange){
						SMRoom room=new SMRoom();
						if(!("None".equalsIgnoreCase(setSpace)))
						room.setRoomCode(completePattern+"-"+count);
						room.setRoomType(spaceCategory);
						room.setRoomNumber(count+"");
						roomList.add(room);
						count++;
					}
					smManager.addRooms(roomList, floorId);
				}
				
				

				 HttpSession session = request.getSession(false);
			     session.setAttribute("isPRG", "true");
				 response.sendRedirect("PRGServlet?status=success&url=user/admin_panel.jsp&alertMessage="+ AlertMessages.requestSent);



				
			
		}
		else if(action.equalsIgnoreCase("manageBuilding"))
		{
			int buildingId;
			String location=userSessionBean.getLocationName();

			String buildingName=request.getParameter("buildingName");
			
			String buildingFloor=request.getParameter("buildingFloor");
			String buildingLocation=request.getParameter("buildingLocation");
			SMBuilding building=new SMBuilding();
			building.setBuildingName(buildingName);
			building.setSubLocation(buildingLocation);
			SMFloor floor=new SMFloor();
			floor.setFloorCode(buildingFloor);
			if(smManager.isBuilding(buildingName))
			{
				buildingId=smManager.getBuildingByBuildingName(buildingName).getId();
				building.setId(buildingId);
			}
			else{
			 buildingId=smManager.addBuilding(building, locationManager.getLocationIdByName(location)).getId();
			}
			List<SMFloor> floorList=new ArrayList<>();
			floorList.add(floor);
			if(!smManager.isFloor(building, buildingFloor))
			smManager.addFloors(floorList, buildingId);
			HttpSession session = request.getSession(false);
		    session.setAttribute("isPRG", "true");
			response.sendRedirect("PRGServlet?status=success&url=user/admin_panel.jsp&alertMessage="+ AlertMessages.requestSent);


			
			
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
