package com.admin4u.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.service.bussiness.contract.ActionManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.HotelAndGuestHouseBean;
import com.admin4u.views.UserSessionBean;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

/**
 * Servlet implementation class MeetingRoomManagementServlet
 */
@MultipartConfig
@WebServlet("/MeetingRoomManagementServlet")
public class MeetingRoomManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private RequestResponseManager reqResManager;

	@Inject
	UserSessionBean userSession;
	
	@Inject
	ActionManager actionManager;
	
	public MeetingRoomManagementServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
 Map<String, String[]> reqMap = request.getParameterMap();
         
         String upn = userSession.getEmpData().getUserPrincipalName();
         
         String action=request.getParameter("action").toString();
         
         if(action.equals("GENERATED"))
         {
        	 String serviceList=""; 
 			serviceList=Services.MEETINGROOMMANAGEMENT.toString();
 			reqMap.put("serviceList",new String[] {serviceList});
        	 
    reqMap.put(RequestMetadata.LOCATION,new  String [] {userSession.getLocationName()});
	reqMap.put(RequestMetadata.REQUEST_CREATION_TIME,new String []{new Date().toString()});
	reqMap.put(RequestMetadata.REQUESTER,new String[] {upn});
	reqMap.put(RequestMetadata.REQUEST_TYPE,new String[] {"SELF"});
	reqMap.put(RequestMetadata.Request_IP,new String[]{request.getRemoteAddr()});
	reqMap.put(RequestMetadata.Service,new String[] {Services.MEETINGROOMMANAGEMENT.toString()});
	/*
	Part part=request.getPart("upload");
	String upload=FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), part);
	
	reqMap.put("upload",new String[] {upload});*/
	
	 RequestMaster requestMaster=reqResManager.insertData(reqMap);
	 
	 int roomId=Integer.parseInt(request.getParameter("conferenceRoom"));
	 actionManager.processSpaceManagementRequest(requestMaster, 5, roomId);
	 
	 HttpSession session = request.getSession(false);
     session.setAttribute("isPRG", "true");
	 response.sendRedirect("PRGServlet?status=success&url=user/meeting_room_management.jsp&alertMessage="+ AlertMessages.requestSent);
         }	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
