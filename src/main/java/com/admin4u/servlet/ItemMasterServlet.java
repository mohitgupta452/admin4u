package com.admin4u.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin4u.service.bussiness.contract.ProductManager;
import com.admin4u.util.service.RequestMetadata.ProductKey;
import com.java.AlertMessages;

@WebServlet("/ItemMasterServlet")
public class ItemMasterServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    @Inject
    private ProductManager productManager;
	
	public ItemMasterServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String action = request.getParameter("action");
		if("MANAGESPACE".equalsIgnoreCase(action)){
		List<Map<String,Object>> products =new ArrayList<>();
		Map<String,Object> productMap  = new HashMap<>();
		
		
		productMap.put(ProductKey.MASTERCATEGARY,request.getParameter("masterCat"));
		productMap.put(ProductKey.NAME,request.getParameter("masterItemName"));
		productMap.put(ProductKey.CATEGARY,request.getParameter("itemCatMaster"));
		productMap.put(ProductKey.PACKSIZE,request.getParameter("itemPackSize"));
		productMap.put(ProductKey.UNITS,request.getParameter("masterUnits"));
		productMap.put(ProductKey.PRICE,request.getParameter("itemCostPrice"));
		productMap.put(ProductKey.BARND,request.getParameter("brandName"));
		
		products.add(productMap);
		
		productManager.registerProduct(products);
		HttpSession session = request.getSession(false);
	    session.setAttribute("isPRG", "true");
		response.sendRedirect("PRGServlet?status=success&url=user/admin_panel.jsp&alertMessage="+ AlertMessages.requestSent);

		
		}
		/*else if("productDetails".equalsIgnoreCase(action)){
			
			productManager.getProductDetails();
		}
		*/
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
