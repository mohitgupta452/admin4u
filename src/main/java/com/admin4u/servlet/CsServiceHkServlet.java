package com.admin4u.servlet;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.UserSessionBean;
import com.java.AlertMessages;

@WebServlet("/CsServiceHkServlet")
public class CsServiceHkServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	 private Logger log = LoggerFactory.getLogger(getClass());   
	
	@Inject
	private RequestResponseManager reqResManager;
	
	@Inject
	private UserSessionBean userSession;
	
	@Inject
	private IDMService idmService;
	
	
    public CsServiceHkServlet() {
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
    	log.debug("servlet calls()");
 Map<String, String[]> reqMap = request.getParameterMap();
         
         log.debug("---hnk servlet call()---");
        // String upn = userSession.getEmpData().getUserPrincipalName();
         String upn = idmService.getEmployeeUPNByEmployeeId(request.getParameter("employeeID"));

        
         String action=request.getParameter("action").toString();
         
         if(action.equals("GENERATED"))
         {
        	 String serviceList=""; 
  			serviceList=Services.HOUSEKEEPING.toString();
  			reqMap.put("serviceList",new String[] {serviceList});
    reqMap.put(RequestMetadata.LOCATION,new  String [] {userSession.getLocationName()});
	reqMap.put(RequestMetadata.REQUEST_CREATION_TIME,new String []{new Date().toString()});
	reqMap.put(RequestMetadata.REQUESTER,new String[] {upn});
	reqMap.put(RequestMetadata.REQUEST_TYPE,new String[] {request.getParameter("username")});
	reqMap.put(RequestMetadata.Request_IP,new String[]{request.getRemoteAddr()});
	reqMap.put(RequestMetadata.Service,new String[] {Services.HOUSEKEEPING.toString()});
	reqMap.put(RequestMetadata.REQUEST_STATUS,new String[] {ApplicationMetaData.Status.ADMIN_GENRATED.toString()});

	reqResManager.insertData(reqMap);
	 
	 HttpSession session = request.getSession(false);
     session.setAttribute("isPRG", "true");
	 response.sendRedirect("PRGServlet?status=success&url=user/cservice_log/chp_service.jsp&alertMessage="+ AlertMessages.requestSent);
         }
     		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}