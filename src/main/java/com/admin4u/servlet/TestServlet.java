package com.admin4u.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.DesignationPriority;
import com.admin4u.persistence.entity.MeetingRoomBookingDetail;
import com.admin4u.persistence.entity.RoomStuff;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.persistence.entity.TatMaster;
import com.admin4u.service.bussiness.contract.EventTatManager;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.util.service.ApplicationMetaData.TatLevelKeys;
import com.admin4u.views.UserSessionBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       @Inject
	CrudService cs;
       @Inject
   	UserSessionBean us;
       @Inject
      GuestHouseManager  ghm;
       
       
       @Inject
      	LocationManger lm;
       @Inject
       EventTatManager etm;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		//List<String> listwl1=cs.findWithNamedQuery("Location.findAllSubLocation",QueryParameter.with("parent", lm.getLocationIdByName(us.getLocationName())).parameters());
		/*
		TatMaster tm=new TatMaster();
		tm.setTatService("cappwwwwwb");
		tm.setComments("dddddddddd");
		tm.setPriority("first");
		tm.setSla("fds");
	JsonObject level=new JsonObject();
	level.addProperty(TatLevelKeys.level1Designation.toString(), "aldjfalkf");
	level.addProperty(TatLevelKeys.level1TatTime.toString(), "15");
	level.addProperty(TatLevelKeys.level2TatTime.toString(), "10");
	tm.setTatLevel(level);
	etm.addTatPriority(tm);*/
	
	etm.getTatDetailByService("repair");
	System.out.println(etm.getTatDetailByService("repair").getTatLevel());
	DesignationPriority dp=new DesignationPriority();
	dp.setDesignation("admin");
	dp.setAboveAed(true);
	
	dp.setPriority("high");
	dp.setUpdatedBy("re");
	//etm.addDesignationWithApprov(dp);
	
	etm.getDesignationWithApprov("admin");
	
	//etm.updateTatLevelDesignation(1, TatLevelKeys.level1Designation, "ppp");
	
	//etm.updateTatLevelTatTime(1, TatLevelKeys.level1TatTime, 20);

		
		
		/*List<SMRoom> roomList=ghm.getRoomListByGuestHouseId(1);
		Gson gson=new Gson();
		JsonArray singleOccupacyRoomList= new JsonArray();
		JsonArray doubleOccupacyRoomList= new JsonArray();
		JsonArray tripleOccupacyRoomList= new JsonArray();

		

		for(SMRoom room:roomList){
			JsonObject jsonObj=new JsonObject();
			jsonObj.addProperty("roomNumber", room.getRoomCode());
			jsonObj.addProperty("roomId", room.getId());
			int i=0;
			for(RoomStuff roomStuff:room.getRoomStuffList()){
				i++;
			jsonObj.addProperty("stuff"+i, roomStuff.getStuffName());
			}
			
			if("SINGLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
				singleOccupacyRoomList.add(jsonObj);
			}
			else if("DOUBLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
				doubleOccupacyRoomList.add(jsonObj);

			}
			if("TRIPLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
				tripleOccupacyRoomList.add(jsonObj);

			}
			//System.out.println(room.getRoomTypeByCapacity());
			//System.out.println(room.getRoomStuffList().get(0).getStuffName());

		}
		//String json=gson.toJson(jsonArray);
		JsonObject jsonResponse=new JsonObject();
		jsonResponse.add("singleOccupacy", singleOccupacyRoomList);
		jsonResponse.add("doubleOccupacy", singleOccupacyRoomList);
		jsonResponse.add("tripleOccupacy", singleOccupacyRoomList);

		
		System.out.println(gson.toJson(jsonResponse));*/
		
	
		/* int singleOccupacayCount=5;
		int doubleOccupacayCount=10;
		int tripleOccupacayCount=7;

		SMBuilding guestHouse=new SMBuilding();
		guestHouse.setBuildingName("guestHouse1");
		guestHouse.setBuildingType("GUESTHOUSE");
		guestHouse.setBuildingAddress("new -delhi behind CP");
		
		List<SMRoom> roomList=new ArrayList<>();
		for(int i=1;i<=singleOccupacayCount;i++){
			SMRoom room=new SMRoom();
			room.setRoomCode("single"+i);   //code to add rooms
			room.setRoomType("GUESTHOUSEROOM");
			room.setRoomTypeByCapacity("single");
			roomList.add(room);
		}
		for(int i=1;i<=singleOccupacayCount;i++){
			SMRoom room=new SMRoom();
			room.setRoomCode("double"+i+"");
			room.setRoomType("GUESTHOUSEROOM");
			room.setRoomTypeByCapacity("double");
			roomList.add(room);
		}
		for(int i=1;i<=singleOccupacayCount;i++){
			SMRoom room=new SMRoom();
			room.setRoomCode("triple"+i+"");
			room.setRoomType("GUESTHOUSEROOM");
			room.setRoomTypeByCapacity("triple");
			roomList.add(room);
		}
		ghm.addNewGuestHouseWithRooms(guestHouse, roomList, 1);
		
		
		
		
		List<RoomStuff> roomStuffList=new ArrayList<>();
		RoomStuff roomStuff=new RoomStuff();
		roomStuff.setStuffName("sofa");
		roomStuffList.add(roomStuff);
		ghm.addGuestHouseRoomDetails(roomStuffList, 1);
	*/
		
		/*Date checkinTime=new Date();
		Date checkOutTime=new Date();
		try{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 checkinTime= sdf.parse("2016-09-09 14:55:00");
				
		 checkOutTime= sdf.parse("2016-09-09 15:05:00");
		}
		catch(ParseException e){
			System.out.println("Exception in parsing date and time  in c"+e.getMessage());
		}
		List<String> roomNo=new ArrayList<>();
		
		for(int i=1;i<15;i++){
			
	
		System.out.println(i+"is room available");
		System.out.println(roomNo.add(i+""+ghm.checkGuestRoomAvailability(i, checkinTime  , checkOutTime)));
		}
		

		

		for(String j :roomNo){
			System.out.println(j);
		}
		System.out.println("************"+ghm.checkGuestRoomAvailability(2, checkinTime  , checkOutTime));
		System.out.println("************1"+ghm.checkGuestRoomAvailability(1, checkinTime  , checkOutTime));
		
		for(MeetingRoomBookingDetail mrd:ghm.getRoomHistory(1)){
			System.out.println("______-"+mrd.getBookingStartDateTime());
		}
		System.out.println("end-------------------");
*/
		/*for(String s:listwl1)
		{
			System.out.println("worklocation values "+s);
		}
		System.out.println("location name "+us.getLocationName());
		System.out.println("location name "+lm.getLocationIdByName(us.getLocationName()));
*/
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}