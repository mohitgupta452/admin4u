package com.admin4u.servlet;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.DeptCostCenterMap;
import com.admin4u.persistence.entity.DropDowns;
import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.util.enums.ApplicationDropDowns;
import com.admin4u.util.service.ExcelCreation;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;
import com.turningcloud.utils.excel.Excel;

@WebServlet("/ManageDropDownServlet")
@MultipartConfig
public class ManageDropDownServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private Logger log = LoggerFactory.getLogger(getClass());
       
	@Inject
	private DropDownManager dropDownManager;
	
	private List<DeptCostCenterMap> dropCostCenterMap;
	
    public ManageDropDownServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Cost Center Department
		
		if("REMOVEDEPTCOSTCENTREMAP".equalsIgnoreCase(request.getParameter("action"))){
			int id=Integer.parseInt(request.getParameter("deptCostMapId"));
			dropDownManager.removeDeptCostCentreMap(id);
			response.setContentType("application/json");
			
		}
		
		
		if("MAPCOSTCENTDEPT".equalsIgnoreCase(request.getParameter("action")))
		{
		String department = request.getParameter("department");
		String costCenter = request.getParameter("costCenter");
		log.debug(department);
		log.debug(costCenter);
	 dropCostCenterMap =  dropDownManager.saveDeptCostCenterMap(department, costCenter);
	 HttpSession session = request.getSession(false);
     session.setAttribute("isPRG", "true");
	 response.sendRedirect("PRGServlet?status=success&url=user/cost_center_mapping.jsp&alertMessage="+ AlertMessages.MappingSaved);

	 
		}

		Map<String,String> keyValueMapForExcel=new HashMap<String,String>();
		if("download".equalsIgnoreCase(request.getParameter("action")))
			
		{
			String template=request.getParameter("ddTemplate");
			
		keyValueMapForExcel=dropDownManager.getKeyValueMapForExcel(template);
		
		
       String path= ExcelCreation.createExcel(template,keyValueMapForExcel);
        
        RequestDispatcher reqDisp= request.getRequestDispatcher("/DownloadController?filename="+path+File.separator+template+".xlsx");
        reqDisp.forward(request, response);
        
		}
		else if("upload".equalsIgnoreCase(request.getParameter("action")))
		{
			
			Part part=request.getPart("uploadedFile");
			String uploadPath=FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), part);

			File excel=new File(uploadPath);
			try
			{
			ExcelCreation excelUtility = new ExcelCreation(excel) ;
			String cellValue="";
			String dropDownData="";
			int cellNumber=3;
			while(cellNumber<=50)
			{
				cellValue=excelUtility.excelReadSingleValue("A"+cellNumber);
				if(cellValue.isEmpty())
				break;
				dropDownData=dropDownData+"|"+ cellValue;
					
				cellNumber++;
			}	
			
			boolean isDropDownNameExist=false;
			ApplicationDropDowns[] aDD=ApplicationDropDowns.values();
			for(ApplicationDropDowns dropDownName:aDD)
			{
				if(dropDownName.name().equals(excelUtility.excelReadSingleValue("A1")))
				{
					isDropDownNameExist=true;

					break;
				}
			}
			if(isDropDownNameExist)
			dropDownManager.updateDropDownData(excelUtility.excelReadSingleValue("A1"), dropDownData);
			
			}
			catch(Exception ex)
			{
				log.debug("exception in reading excel file{}",ex.getMessage());
				
			}
			
		
			HttpSession session = request.getSession(false);
		     session.setAttribute("isPRG", "true");
			 response.sendRedirect("PRGServlet?status=success&url=user/manager_dropdown.jsp&alertMessage="+ AlertMessages.uploadExcel);

			
			
		}
		else if("mapDeptCostcenter".equalsIgnoreCase(request.getParameter("action")))
				{
			
					
			}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}


	public List<DeptCostCenterMap> getDropCostCenterMap() {
		return dropCostCenterMap;
	}


	public void setDropCostCenterMap(List<DeptCostCenterMap> dropCostCenterMap) {
		this.dropCostCenterMap = dropCostCenterMap;
	}

	
}