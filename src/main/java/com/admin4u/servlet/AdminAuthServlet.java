package com.admin4u.servlet;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.dto.EmployeeDTO;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.EmployeeRoleServiceMaster;
import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.ActionManager;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.service.ApplicationMetaData.Action;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

/**
 * Servlet implementation class AdminAuthServlet
 */
@WebServlet("/adminauth")
@MultipartConfig
public class AdminAuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private RequestResponseManager reqResManager;

	@Inject
	private ActionManager actionManager;

	@Inject
	private UserSessionBean userSession;

	@Inject
	private RoleManager roleManager;

	@Inject
	private LocationManger locationManger;

	@Inject
	private ADStore adStore;
	
	@Inject
	private GuestHouseManager guestHouseManager;

	public AdminAuthServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action").toString();
		for(Entry<String, String[]>parm:request.getParameterMap().entrySet()){
			log.debug("key  {} value {}",parm.getKey(),parm.getValue());
			
		}
		int actionId = 0;
		if (!action.equals("GETSPOC"))
			actionId = request.getParameter("actionId") != null ? Integer.parseInt(request.getParameter("actionId"))
					: 0;

		if (action.equals(Action.ADMIN_SERVE.toString())) {
			List<Part> parts = (List) request.getParts();
			List<String> filesPath = new ArrayList<>();

			String filePath = File.separator + "Request "
					+ actionManager.getUserActionByActionId(actionId).getRequestMaster().getRequestId() + File.separator
					+ "Response";

			for (Part part : parts) {
				if (part != null && part.getSubmittedFileName() != null)
					filesPath
							.add(FilesUtils.saveFile(new File(FilesUtils.getPlatformBasedParentDir(), filePath), part));
			}
			
			Map<String, String[]> reqMap = request.getParameterMap();

//code to mange guest house booking request
			if(request.getParameterMap().containsKey("guestHouseRoomId")&&!request.getParameter("guestHouseRoomId").isEmpty()){
	    		if(request.getParameter("guestHouse").equalsIgnoreCase("Guest_House"))  {
				int roomId=Integer.parseInt(request.getParameter("guestHouseRoomId"));
	    		  Date checkInDateTime=new Date();
	    		  Date checkOutDateTime=new Date();
	    		  log.debug("inside auth servlet");
	    		  try{
	    				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	    				 checkInDateTime= sdf.parse(request.getParameter("checkinDateTime"));
	    						
	    				 checkOutDateTime= sdf.parse(request.getParameter("checkoutDateTime"));
	    				}
	    				catch(ParseException e){
	    					log.debug("exceptin in parsing date in guestHouseInvetory spoc serv",e.getMessage());
	    				}
	    		  
	    		  
	    		  guestHouseManager.processGuestHouseRequestNew(actionId, roomId, checkInDateTime, checkOutDateTime);
	    		  reqMap.putAll(guestHouseManager.getRoomDetailMap(roomId));
	    		}
	    	  }
			
//end			

			actionManager.adminServe(actionId, filesPath, reqMap);
			HttpSession session = request.getSession(false);
			session.setAttribute("isPRG", "true");

			response.sendRedirect(
					"PRGServlet?status=success&url=user/admintable.jsp&alertMessage=" + AlertMessages.bookTicket);

		} else if (action.equals(Action.ADMIN_CLOSE.toString())) {
			String reqData = actionManager.adminClose(actionId, request.getParameter("responseMsg"));
			response.setContentType("application/json");
			response.getWriter().write(reqData);

		} else if (action.equals(Action.ADMIN_FORWORD.toString())) {
			String reqData = actionManager.adminForword(actionId, request.getParameter("assignTo"));
			response.setContentType("application/json");
			response.getWriter().write(reqData);

		} else if (action.equals(Action.ADMIN_COMPLETE.toString())) {
			
			String msg = request.getParameter("extendMsg");
			//Date extentionTime =  new Date(request.getParameter("extendTimeDate"));
			Date extentionTime =new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			try {
				extentionTime= sdf.parse(request.getParameter("extendTimeDate"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
					
			String reqData = actionManager.extend(actionId, msg, extentionTime, Action.ADMIN_EXTEND);
			response.setContentType("application/json");
			response.getWriter().write(reqData);
		} else if (action.equals("GETSPOC")) {
			List<EmployeeRole> employeeRoles = roleManager.getRolesByLocation(request.getParameter("location"),
					Roles.SPOC);

			JsonObject jsonObject = new JsonObject();

			for (EmployeeRole employeeRole : employeeRoles) {
				for (EmployeeRoleServiceMaster roleServiceMaster : employeeRole.getEmployeeRoleServicemaster()) {
					jsonObject.addProperty(employeeRole.getUserPrincipalName(), roleServiceMaster.getService());
				}
			}
			log.debug("spoc details {}", jsonObject);
			response.getWriter().write(jsonObject.toString());

		} else if ("otherSpoc".equalsIgnoreCase(action)) {
			log.debug("get other spoc");
			String location = request.getParameter("location");
			List<EmployeeDTO> employeeDTOs = adStore.getEmpByLocation(locationManger.getSubLocations(location));

			log.debug("total other spocs {} for location {}", employeeDTOs.size(), location);

			JsonObject otherSpocs = new JsonObject();

			for (EmployeeDTO employeeDTO : employeeDTOs) {
				JsonObject empJson = new JsonObject();

				empJson.addProperty("empId",employeeDTO.getEmpID());
			   	empJson.addProperty("upn",employeeDTO.getUserPrincipalName());
			   	otherSpocs.add(String.valueOf(employeeDTO.getRowID()),empJson);
			}
			log.debug("other spocs {}", otherSpocs);
			response.getWriter().write(otherSpocs.toString());

		}

		else if (action.equals("CHECKTATEXPIRE")) {
			log.debug("check tat expire method called ");

		} else if (action.equalsIgnoreCase("ADMIN_APPROVE")) {
			String reqData = actionManager.adminApprove(actionId);
			response.setContentType("application/json");
			response.getWriter().write(reqData);
		} else if(action.equals("GETCOMMENTS"))
	       {
    	    String comments= actionManager.getCommentsByRequest(Integer.parseInt(request.getParameter("requestId")));
	    	   response.setContentType("application/json");

    	    response.getWriter().write(comments);
       }
       else if(action.equals("SETEXTENDDETAILS"))
       {
	    	  int tatTime=Integer.parseInt(request.getParameter("tatTime"));
	    	  String comment=request.getParameter("comment");
	    	  log.debug("tatTime {} comment {}",tatTime,comment);
	    	  Date tatDateTime=DateUtils.addHours(new Date(), tatTime);
	    	  log.debug("new date {}",tatDateTime);
	    	  String status=actionManager.extend(actionId, comment, tatDateTime,Action.ADMIN_EXTEND);
	    	   response.setContentType("application/json");
	    	   
    	   response.getWriter().write(status);
       }
		
		

		else
			response.sendRedirect("user/admintable.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
