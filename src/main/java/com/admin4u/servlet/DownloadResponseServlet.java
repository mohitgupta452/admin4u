package com.admin4u.servlet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.views.ResponsePDFBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfWriter;
import com.turningcloud.utils.FilesUtils;


@WebServlet("/DownloadResponseServlet")

public class DownloadResponseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	Logger log = LoggerFactory.getLogger(getClass());
	
	@Inject
	private   ResponsePDFBean responsePDFBean;
	
	@Inject
	private RequestResponseManager requestResponseManager;
	
    public DownloadResponseServlet() {
        super();
      
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		log.debug("------downloader of PDF------");
		
		int requestID=Integer.parseInt(request.getParameter("requestId"));
		
		 Map  jsonObject=  new Gson().fromJson(requestResponseManager.getResponseDataByReqID(requestID),Map.class);
		
		 
		 
	File filePath=	responsePDFBean.generatePdf(jsonObject);
	
	String queryStr=filePath.getPath()+"|"+request.getParameter("fileName");
	
	request.getRequestDispatcher("DownloadController?filename="+queryStr).forward(request, response);
	
		
		}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
	}

}
