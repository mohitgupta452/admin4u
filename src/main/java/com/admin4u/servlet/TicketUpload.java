package com.admin4u.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.turningcloud.utils.FilesUtils;

/**
 * Servlet implementation class TicketUpload
 */
@MultipartConfig
@WebServlet("/ticketupload")
public class TicketUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public TicketUpload() {
        super();
    }

	
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FilesUtils fu=new FilesUtils();
	Part part=request.getPart("file");
	fu.saveFile(fu.getPlatformBasedParentDir(), part);
	RequestDispatcher rd=request.getRequestDispatcher("user/tour_travel.java");
	rd.forward(request,response);
	}

}
