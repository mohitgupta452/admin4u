package com.admin4u.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.service.bussiness.impl.LocationManagerImpl;
import com.admin4u.util.enums.Roles;
import com.admin4u.views.UserSessionBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebServlet("/loginservlet")
public class LoginServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	 
    private Logger log = LoggerFactory.getLogger(getClass());
    
    @Inject
    private IDMService idmService;
    
    @Inject
    private  UserSessionBean userSessionBean;

	@Inject
	private RoleManager roleManager;
	
	
	@Inject
	private LocationManger locationManager;
	
	@Inject
	private ADStore adStore;
	
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	   
	    	
	    }
@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		
		    String userId=req.getParameter("userid");
		      if(idmService.isAuthenticated(userId))
		      {
		     EmployeeData empData=idmService.getEmpByMail(userId);
		    
		     HttpSession session;
		     session = req.getSession(true);
		     session.setAttribute("userID",userId);
		    String upn=empData.getUserPrincipalName();
		   	 userSessionBean.setEmpData(idmService.getEmpByUpn(upn));
		   	 
		   	 userSessionBean.setEmpRole(roleManager.getEmpRoleByUPN(upn));
		   	 //start code set location from ad
		   	 userSessionBean.setLocationName(locationManager.getParentLocation(empData.getL()));
		   	 //end location from ad
		   	 
		   	 if(idmService.isOtherSpoc(upn)){
		   		if(userSessionBean.getEmpRole().isEmpty()){
		   			List<String> roleList = new ArrayList<>();
		   			roleList.add(Roles.OTHERSPOC.toString());
		   			userSessionBean.setEmpRole(roleList);
		   		}
		   		else
		   			userSessionBean.getEmpRole().add(Roles.OTHERSPOC.toString());
		   	 }
		   	 
		   	 
		   	if(adStore.isManager(userSessionBean.getEmpData().getUserPrincipalName()))
			 {
		   		if(userSessionBean.getEmpRole().isEmpty()){
		   			List<String> roleList = new ArrayList<>();
		   			roleList.add(Roles.APPROVAL_AUTHORITY.toString());
		   			userSessionBean.setEmpRole(roleList);
		   		}
		   		else
		   			userSessionBean.getEmpRole().add(Roles.APPROVAL_AUTHORITY.toString());
			 }
		   	
	         resp.sendRedirect("user/index.jsp");
		     }
		      else
		     resp.sendRedirect("login.jsp");
		
	}
}
