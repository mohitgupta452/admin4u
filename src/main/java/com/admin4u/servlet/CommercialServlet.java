package com.admin4u.servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.admin4u.persistence.entity.Podetail;
import com.admin4u.persistence.entity.PrDetail;
import com.admin4u.service.bussiness.contract.VendorManager;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

@WebServlet("/CommercialServlet")
@MultipartConfig
public class CommercialServlet extends HttpServlet {

private static final long serialVersionUID = 1L;
 
	@Inject
	private VendorManager vendorManager;
    public CommercialServlet() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	
		Map<String , String> commercialMap = new HashMap<String , String>();
		commercialMap.put("poAmount", request.getParameter("poAmount"));
		commercialMap.put("poNumber",request.getParameter("poNumber") );
		commercialMap.put("createdBy", request.getParameter("createdBy"));
		commercialMap.put("from", request.getParameter("from"));
		commercialMap.put("releasedFromComm", request.getParameter("releasedFromComm"));
		commercialMap.put("releasedFromFinance", request.getParameter("releasedFromFinance"));
		commercialMap.put("relStatus", request.getParameter("relStatus"));
		commercialMap.put("to", request.getParameter("to"));
		commercialMap.put("prAccount", request.getParameter("prAccount"));
		commercialMap.put("prNumber", request.getParameter("prNumber"));
		commercialMap.put("prCreationDate", request.getParameter("prCreationDate"));
		commercialMap.put("releasedFromFh", request.getParameter("releasedFromFh"));
		commercialMap.put("releasedFromGhk", request.getParameter("releasedFromGhk"));
		commercialMap.put("releasedFromDeptGm", request.getParameter("releasedFromDeptGm"));
		commercialMap.put("prId",request.getParameter("prId") );
		commercialMap.put("poId",request.getParameter("poId") );

    Part commpart=request.getPart("commAttach");
	String commAttach=FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), commpart);
	commercialMap.put("upload" , commAttach);

	vendorManager.insertMISCommercialData(commercialMap);
	HttpSession session = request.getSession(false);
    session.setAttribute("isPRG", "true");
	response.sendRedirect("PRGServlet?status=success&url=user/vendor.jsp&alertMessage="+ AlertMessages.requestSent);
  
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
