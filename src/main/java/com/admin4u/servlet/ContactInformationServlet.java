package com.admin4u.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin4u.persistence.entity.ContactDetail;
import com.admin4u.service.bussiness.contract.VendorManager;
import com.java.AlertMessages;

@WebServlet("/ContactInformationServlet")
public class ContactInformationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Inject
	VendorManager vendorManager;
    public ContactInformationServlet() {
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
    	String action = request.getParameter("action");
    
    	if("action".equalsIgnoreCase(action))	{	
	Map<String , String> contactMap = new HashMap<String,String >();
	contactMap.put("firstName", request.getParameter("firstName"));
	contactMap.put("email", request.getParameter("comEmail"));
	contactMap.put("contactNo", request.getParameter("contactNo"));
	contactMap.put("companyName", request.getParameter("companyName"));
	contactMap.put("contactNo", request.getParameter("contactNo"));
	contactMap.put("salutation", request.getParameter("salutation"));
	contactMap.put("designation", request.getParameter("designation"));
	contactMap.put("companyDepart", request.getParameter("companyDepart"));
	contactMap.put("stateIncorporation", request.getParameter("contactStateIncorporation"));
	contactMap.put("contactIncorporation", request.getParameter("contactIncorporationYear"));
	contactMap.put("deliveryMethod", request.getParameter("deliveryMethod"));
	contactMap.put("type","type");
	vendorManager.insertContactData(contactMap);
	
	HttpSession session = request.getSession(false);
    session.setAttribute("isPRG", "true");
	response.sendRedirect("PRGServlet?status=success&url=user/vendor.jsp&alertMessage="+ AlertMessages.requestSent);
    	}
   else 
	    if(action.equalsIgnoreCase("callContactInactive"))
		if("callContactInactive".equalsIgnoreCase(action))
		{
			int contactId = Integer.parseInt(request.getParameter("ID"));
		    vendorManager.inActiveTheContactRow(contactId);
			
		}
	
	}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
