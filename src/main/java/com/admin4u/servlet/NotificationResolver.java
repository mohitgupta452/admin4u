package com.admin4u.servlet;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;

import com.admin4u.service.bussiness.contract.NotificationManager;
import com.admin4u.views.NotificationResolverBean;

@WebServlet("/NotificationResolver")
public class NotificationResolver extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Logger log;
	
	@Inject
	NotificationResolverBean notificationResolverBean;
       
    public NotificationResolver() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String referrer = request.getHeader("referer");
		
		int notificationId = Integer.parseInt(request.getParameter("notificationId")); 
		
		notificationResolverBean.setNotificationId(notificationId);
		notificationResolverBean.setRequestOrigin(referrer);
		
		log.debug("referer {}",referrer);
		log.debug("uri {}",request.getRequestURI());
		log.debug("url {}",request.getRequestURL());
		
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
