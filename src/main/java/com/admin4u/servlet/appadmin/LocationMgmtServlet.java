package com.admin4u.servlet.appadmin;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.admin4u.service.bussiness.contract.LocationManger;

/**
 * Servlet implementation class LocationMgmtServlet
 */
@WebServlet("/LocationMgmtServlet")
public class LocationMgmtServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	private LocationManger locationManger;

	public LocationMgmtServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		if ("MAPLOC".equals(action)) {
			
			String responseMsg=locationManger.mapSubLocation(Integer.parseInt(request.getParameter("parentLocation")),
					request.getParameterValues("subLocation[]"));
			response.getWriter().write(responseMsg);

		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
