package com.admin4u.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.UserSessionBean;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

@MultipartConfig
@WebServlet("/TrainTicketRequestServlet")
public class TrainTicketRequestServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private RequestResponseManager reqResManager;
	
	@Inject
	private UserSessionBean userSession;
    

    public TrainTicketRequestServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
 Map<String, String[]> reqMap = request.getParameterMap();
         
         String upn = userSession.getEmpData().getUserPrincipalName();
          String location=userSession.getLocationName();
        
         String action=request.getParameter("action").toString();
         
         if(action.equals("GENERATED"))
         {
        	
        	//multiple pax request handler 
        	 int total=Integer.parseInt(request.getParameter("totalPassengers"));
        	 List<String> paxNameList=new ArrayList<>();
        	// List<String> paxIdList=new ArrayList<>();
        	// List<String> paxAgeList=new ArrayList<>();


        	 for(int counter=1;counter<=total;counter++){
        		 paxNameList.add(request.getParameter("passenger"+counter));
        		// paxIdList.add(request.getParameter("employeeID"+counter));
        		// paxAgeList.add(request.getParameter("age"+counter));
        	 }
        	  reqMap.put("passenger", paxNameList.toArray(new String[0]))  ;
        	  reqMap.put("totalPax", new String[]{String.valueOf(total)});
        	 // reqMap.put("employeeID", paxIdList.toArray(new String[0]))  ;	 
        	 // reqMap.put("age", paxAgeList.toArray(new String[0]))  ;	 


        	 
        	 //end multiple pax request handler
        	 
        	 
        	 
        	 
        	 String serviceList=""; 
 			serviceList=Services.TRAINTICKET.toString();
 			          
 			if(request.getParameter("trainAvailCab") != null)
 				serviceList+="|"+"CAB";
 			if(request.getParameter("trainAvailAccomodation") != null)
 				serviceList+="|"+Services.GUEST_HOUSE_BOOKING.toString();	
 				
 			reqMap.put("serviceList",new String[] {serviceList});
	reqMap.put(RequestMetadata.REQUEST_CREATION_TIME,new String []{new Date().toString()});
	reqMap.put(RequestMetadata.LOCATION,new String[]{location});
	reqMap.put(RequestMetadata.REQUESTER,new String[] {upn});
	reqMap.put(RequestMetadata.REQUEST_TYPE,new String[] {request.getParameter("trainBookingFor")});
	reqMap.put(RequestMetadata.Service,new String[] {Services.TRAINTICKET.toString()});
	reqMap.put(RequestMetadata.Request_IP,new String[]{request.getRemoteAddr()});
	Part part=request.getPart("trainApprovedAttachment");
	String upload=FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), part);
	reqMap.put("upload",new String[] {upload});
	 
	String respMsg="";
    if(reqResManager.insertData(reqMap) == null)
	   respMsg="Sorry Your Manager is not mapped ";
    else
    	respMsg =AlertMessages.requestSent;
	 
	 HttpSession session = request.getSession(false);
     session.setAttribute("isPRG", "true");
	 response.sendRedirect("PRGServlet?status=success&url=user/tour_travel.jsp&alertMessage="+ AlertMessages.requestSent);

         }
    }
}