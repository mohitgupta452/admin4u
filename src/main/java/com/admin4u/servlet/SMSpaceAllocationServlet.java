package com.admin4u.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.dto.SpaceManagementManager;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.SMFloor;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.persistence.entity.SMSeat;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.bussiness.contract.ActionManager;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.enums.AllocationStatus;
import com.admin4u.util.enums.ApplicationDropDowns;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.SMSpaceStationType;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.MultipartUtils;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

@WebServlet("/SMSpaceAllocationServlet")
public class SMSpaceAllocationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
	
    private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	SpaceManagementManager smManager;
	
	@Inject
	private UserSessionBean userSession;
	
	@Inject
	private ActionManager actionManager;
	
	@Inject
	private IDMService idmManager;
    
    public SMSpaceAllocationServlet() {
    	 super();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String action=request.getParameter("action");
    	if("ALLOCATE".equalsIgnoreCase(action)){
    		int actionId=Integer.parseInt(request.getParameter("spaceActionId"));
    		String subAction=request.getParameter("spaceAction");
    		int seatId=Integer.parseInt(request.getParameter("seatRoom"));
    		int roomId=seatId;
			String seatType=request.getParameter("seatType");
			String employeeUPN=request.getParameter("employeeUPN");
			String isAllocated="";
			 String targetUrl="";
			boolean isAdmin=false;
			 if(ApplicationMetaData.Action.ADMIN_ALLOCATE.toString().equalsIgnoreCase(subAction)){
	        	 targetUrl="admintable.jsp";
	        	 isAdmin=true;
	         }
	         else if(ApplicationMetaData.Action.SPOC_ALLOCATE.toString().equalsIgnoreCase(subAction)){
	        	 targetUrl="spoc.jsp";
	        	 isAdmin=false;
	        	 
	         }
			if(SMSpaceStationType.WORKSTATION.toString().equalsIgnoreCase(seatType)){
				 isAllocated=smManager.allocateSeat(employeeUPN, seatId);
					//log.debug("value of {}",smManager.allocateSeat(roomId, employeeUPN));
				 if(AlertMessages.seatAllocated.equalsIgnoreCase(isAllocated)){
					 	if(isAdmin){
					 		actionManager.adminAllocate(actionId);
					 	}
					 	else{
					 		String status= actionManager.spocAllocate(actionId);
					 	}
						//actionManager.updateRequest(uam.getRequestMaster());
						isAllocated="true";
				 }

			}
			else if(SMSpaceStationType.CABIN.toString().equalsIgnoreCase(seatType)){
				if(smManager.allocateRoom(roomId, employeeUPN)){
					if(isAdmin){
				 		actionManager.adminAllocate(actionId);
				 	}
				 	else{
									
					UserActionMaster uam=actionManager.commonOperation(actionId, ApplicationMetaData.Action.valueOf(subAction));
					actionManager.updateRequest(uam.getRequestMaster());
				 	}
					
					isAllocated="true";
				}
			}
			
			 HttpSession session = request.getSession(false);
	         session.setAttribute("isPRG", "true");
	         String msg="";
	         if(isAllocated.equalsIgnoreCase("true")){
	        	 msg=AlertMessages.seatAllocated;
	         }
	         else{
	        	 msg=AlertMessages.seatNotAllocated;
	         }
	        
	        
	         
	    	 response.sendRedirect("PRGServlet?status=success&url=user/"+targetUrl+"&alertMessage="+ msg);
	    	 
		
    	}
    	
    	
    	
    	 
    	 
    	//space management spoc view methods starts
 		else if("SEATINFO".equalsIgnoreCase(action)){
 			int floorId=Integer.parseInt(request.getParameter("floorId"));
 			String seatType=request.getParameter("seatType");
 			String buildingFloor="";
 			String total="";
 			String assigned="";
 			String available="";
 					
 			
 			Map<String,String> responseMap;
 			responseMap=new HashMap<String,String>();
 			Map<String,String> availableSeatMap=new HashMap<>();

 			if(SMSpaceStationType.CABIN.toString().equalsIgnoreCase(seatType)||SMSpaceStationType.MEETING_ROOM.toString().equalsIgnoreCase(seatType)){
 				 buildingFloor=smManager.getFloorInfoByFloorId(floorId).getFloorCode();
 				 total=smManager.getRoomsByFloor(floorId, seatType).size()+"";
 				 assigned=smManager.getAllocatedRooms(floorId, seatType).size()+"";
 				 available=smManager.getAvailableRooms(floorId, seatType).size()+"";
 					
 				
 				

 				
 			}
 			else if(SMSpaceStationType.WORKSTATION.toString().equalsIgnoreCase(seatType)){
 				List<SMSeat> seatList=smManager.getAvailableSeatsByFloor(floorId);
 				 buildingFloor=smManager.getFloorInfoByFloorId(floorId).getFloorCode();
 				 total=smManager.getSeatsByFloor(floorId).size()+"";
 				 assigned=smManager.getAllocatedSeats(floorId, "allocated").size()+"";
 				 available=smManager.getAvailableSeatsByFloor(floorId).size()+"";
 				 
 				
 						
 			}
 			

 			responseMap.put("buildingFloor", buildingFloor);
 			responseMap.put("seatType", seatType);
 			responseMap.put("total", total);
 			responseMap.put("assigned", assigned);
 			responseMap.put("available", available);
 			Gson gson = new Gson(); 
 			String json = gson.toJson(responseMap);

 			response.setContentType("application/json");
 			response.getWriter().write(json);
 			log.debug(json);	
 		}
 		else if("GETAVAILABLESPACE".equalsIgnoreCase(action)){
 			int floorId=Integer.parseInt(request.getParameter("floorId"));
 			String seatType=request.getParameter("seatType");
 			Map<String,String>availableSpaceMap=new HashMap<>();
 			
 			if(SMSpaceStationType.WORKSTATION.toString().equalsIgnoreCase(seatType)){
 			List<SMSeat> availableSeatList=new ArrayList<>();
 			availableSeatList=smManager.getAvailableSeatsByFloor(floorId);
 			for(SMSeat seat:availableSeatList){
 				availableSpaceMap.put(seat.getId()+"", seat.getSeatCode());
 			}
 			
 			}
 			else if(SMSpaceStationType.CABIN.toString().equalsIgnoreCase(seatType)|| SMSpaceStationType.MEETING_ROOM.toString().equalsIgnoreCase(seatType)){
 				//String roomType="seat";
 				List<SMRoom>availableRoomList=smManager.getAvailableRooms(floorId, seatType);
 				for(SMRoom room:availableRoomList){
 					availableSpaceMap.put(room.getId()+"", "Code:"+room.getRoomCode());
 				}
 				
 			}
 			
 			Gson gson = new Gson(); 
 			String json = gson.toJson(availableSpaceMap);
 			response.setContentType("application/json");
 			response.getWriter().write(json);
 			log.debug(json);	
 		}
 		else if("getEmployeeUpn".equalsIgnoreCase(action)){
 			int actionId=Integer.parseInt(request.getParameter("actionId"));
 			UserActionMaster uam=actionManager.getUserActionByActionId(actionId);
 			JsonObject jsonObj=uam.getRequestMaster().getRequestData().getRequestData();
 			String employeeId=jsonObj.get("spacemanagement.employeeid").getAsString();
 			String employeeName=jsonObj.get("spacemanagement.employeename").getAsString();
 			String employeeUPN=idmManager.getEmployeeUPNByEmployeeId(employeeId);
 			log.debug("employeeid,employename,upn------{},{},{}",employeeId,employeeName,employeeUPN);
 			Map<String,String>responseMap=new HashMap<>();
 			responseMap.put("employeeUPN", employeeUPN);
 			responseMap.put("employeeName", employeeName);

 			Gson gson = new Gson(); 
 			String json = gson.toJson(responseMap);
 			response.setContentType("application/json");
 			response.getWriter().write(json);
 			log.debug(json);
 		}
 		else if("FLOORINFO".equalsIgnoreCase(action)){
 			int buildingId=Integer.parseInt(request.getParameter("buildingId"));
 			Map<String,String>floorMap=new HashMap<>();
 			
 			List<SMFloor>floorList=smManager.getFloorsByBuilding(buildingId);
 			
 			for(SMFloor floor:floorList){
 				floorMap.put(floor.getId()+"", floor.getFloorCode());
 			}
 			Gson gson = new Gson(); 
 			String json = gson.toJson(floorMap);
 			response.setContentType("application/json");
 			response.getWriter().write(json);
 			log.debug(json);	
 		}
    	
 		//end
    	
    	


    	
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    	
    		doGet(request, response);
    		
    	
    }
}