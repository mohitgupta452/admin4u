package com.admin4u.servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.dto.SpaceManagementManager;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.MeetingRoomBookingDetail;
import com.admin4u.persistence.entity.RoomStuff;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.persistence.entity.SMSeat;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.service.bussiness.contract.ServiceManager;
import com.admin4u.util.enums.AllocationStatus;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.SMSpaceStationType;
import com.admin4u.util.enums.StuffNameCodeMap;
import com.admin4u.util.service.ApplicationMetaData.Status;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class GuestHouseInventoryServlet
 */
@WebServlet("/GuestHouseInventoryServlet")
public class GuestHouseInventoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject 
	private ServiceManager serviceManager;

	  @Inject
	 UserSessionBean userSessionBean;
	  @Inject
	 private GuestHouseManager guestHouseManager; 
	  
	  @Inject
	  private LocationManger locationManager;
	  
	  @Inject
	  private RoleManager roleManager;
	 
   
    public GuestHouseInventoryServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("inside gh  servlet");
		request.getParameterMap().forEach((k,v)->{
			
			System.out.println("key "+k+"   value "+( v.length >= 0 ? v[0] :"na"));
		});

		String action=request.getParameter("action");
		
		if("ROOMLIST".equalsIgnoreCase(action)){
			int guestHouseId=Integer.parseInt(request.getParameter("ghId"));
			List<SMRoom> roomList=guestHouseManager.getRoomListByGuestHouseId(guestHouseId);
		Gson gson=new Gson();
			JsonArray singleOccupacyRoomList= new JsonArray();
			JsonArray doubleOccupacyRoomList= new JsonArray();
			JsonArray tripleOccupacyRoomList= new JsonArray();

			

			for(SMRoom room:roomList){
				JsonObject jsonObj=new JsonObject();
				jsonObj.addProperty("roomNumber", room.getRoomCode());
				jsonObj.addProperty("roomId", room.getId());
				
				
				
				int i=0;
				for(RoomStuff roomStuff:room.getRoomStuffList()){
					i++;
					if(roomStuff.getRoomStuffId()!=StuffNameCodeMap.BED)
				jsonObj.addProperty(roomStuff.getStuffName(), roomStuff.getStuffName());
					else
						jsonObj.addProperty("bed", roomStuff.getStuffName());

				}
				
				if("SINGLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
					singleOccupacyRoomList.add(jsonObj);
				}
				else if("DOUBLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
					doubleOccupacyRoomList.add(jsonObj);

				}
				else if("TRIPLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
					tripleOccupacyRoomList.add(jsonObj);

				}

			}
			JsonObject jsonResponse=new JsonObject();
			jsonResponse.add("singleOcupancy", singleOccupacyRoomList);
			jsonResponse.add("doubleOcupancy", doubleOccupacyRoomList);
			jsonResponse.add("tripleOcupancy", tripleOccupacyRoomList);

			response.setContentType("application/json");
			response.getWriter().write(gson.toJson(jsonResponse));
			
			
		}
		
		else if("SUBMIT".equalsIgnoreCase(action)){
			

			
			  //String row= request.getParameter("row");
			int singleOccupacayCount=Integer.parseInt(request.getParameter("single"));
			int doubleOccupacayCount=Integer.parseInt(request.getParameter("double"));
			int tripleOccupacayCount=Integer.parseInt(request.getParameter("tripple"));

			List<EmployeeRole> employeeRoleList=roleManager.getEmpByUpnAndRole(userSessionBean.getEmpData().getUserPrincipalName(),Roles.ADMIN);
			
			int locationId=locationManager.getLocationIdByName(employeeRoleList.get(0).getLocation());
			SMBuilding guestHouse=new SMBuilding();

			guestHouse.setBuildingName(request.getParameter("guestHouse"));

			guestHouse.setBuildingType("GUESTHOUSE");
			guestHouse.setBuildingAddress("address");
			
			List<SMRoom> roomList=new ArrayList<>();
			int j=1;
			for(int i=1;i<=singleOccupacayCount;i++){
				SMRoom room=new SMRoom();
				room.setRoomCode("R-"+j);   //code to add rooms
				room.setRoomType("GUESTHOUSEROOM");
				room.setRoomTypeByCapacity("SINGLE");
				roomList.add(room);
				j++;
			}
			for(int i=1;i<=doubleOccupacayCount;i++){
				SMRoom room=new SMRoom();
				room.setRoomCode("R-"+j+"");
				room.setRoomType("GUESTHOUSEROOM");
				room.setRoomTypeByCapacity("DOUBLE");
				roomList.add(room);
				j++;
			}
			for(int i=1;i<=tripleOccupacayCount;i++){
				SMRoom room=new SMRoom();
				room.setRoomCode("R-"+j+"");
				room.setRoomType("GUESTHOUSEROOM");
				room.setRoomTypeByCapacity("TRIPLE");
				roomList.add(room);
				j++;
			}
			String resp=guestHouseManager.addNewGuestHouseWithRooms(guestHouse, roomList, locationId);
			
			response.getWriter().write(resp);
			
		}
		else if("ADDROOMSTUFF".equalsIgnoreCase(action)){
			String stuffName=request.getParameter("stuffName");
			int roomId=Integer.parseInt(request.getParameter("roomId"));
			List<RoomStuff> roomStuffList=new ArrayList<>();
			RoomStuff roomStuff=new RoomStuff();
			roomStuff.setStuffName(stuffName);
			roomStuffList.add(roomStuff);
			guestHouseManager.addGuestHouseRoomDetails(roomStuffList, roomId);
			
		}
		else if("DELETEROOMSTUFF".equalsIgnoreCase(action)){
			String stuffName=request.getParameter("stuffName");
			int roomId=Integer.parseInt(request.getParameter("roomId"));

			
			guestHouseManager.deleteRoomStuffByRoomAndStuffName(roomId, stuffName);
			
		}
		else if("SPOC_SERV".equalsIgnoreCase(action)||"ADMIN_SERV".equalsIgnoreCase(action)){
			
			int roomId=Integer.parseInt(request.getParameter("roomId"));
			int requestId=Integer.parseInt(request.getParameter("requestId"));
			int actionId=Integer.parseInt(request.getParameter("actionId"));
			
			Date checkInTime=new Date();
			Date checkOutTime=new Date();
			try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			 checkInTime= sdf.parse(request.getParameter("checkInDateTime"));
					
			 checkOutTime= sdf.parse(request.getParameter("checkOutDateTime"));
			}
			catch(ParseException e){
				log.debug("exceptin in parsing date in guestHouseInvetory spoc serv",e.getMessage());
			}
			boolean isAllocated=false;
			String spocComment;
			if("SPOC_SERV".equalsIgnoreCase(action)&& request.getParameterMap().containsKey("spocComment"))
			 spocComment=request.getParameter("spocComment");
			else
				spocComment="";

			Map<String,String[]> allocationDetailMap=new HashMap<>();//for storing guest house allocation detail;
			
			
			isAllocated=guestHouseManager.allocateRoom(roomId, requestId,actionId,action, checkInTime, checkOutTime,spocComment);
			
			JsonObject jsonResponse=new JsonObject();
			if(isAllocated){
			jsonResponse.addProperty("msg", "Room Successfully Booked");
			jsonResponse.addProperty("status", "true");
			}
			else{
			jsonResponse.addProperty("msg", "Sorry this Room is already booked for this time slot");	
			jsonResponse.addProperty("status", "false");

			}
			response.setContentType("application/json");
			response.getWriter().write(new Gson().toJson(jsonResponse));
			
		}
		else if("GETAVAILABLESPACE".equalsIgnoreCase(action)){
 			int buildingId=Integer.parseInt(request.getParameter("buildingId"));
 			String occupacyType=request.getParameter("occupacyType");
 			Gson gson=new Gson();
 			
 			List<SMRoom> roomList=guestHouseManager.getRoomListByOccupacyType(buildingId, occupacyType);
			JsonArray occupacyRoomList= new JsonArray();

 			for(SMRoom room:roomList){
 				
 				JsonObject jsonObj=new JsonObject();
				jsonObj.addProperty("roomNumber", room.getRoomCode());
				jsonObj.addProperty("roomId", room.getId());
				int i=0;
				for(RoomStuff roomStuff:room.getRoomStuffList()){
					i++;
				jsonObj.addProperty("stuff"+i, roomStuff.getStuffName());
				}
								
					occupacyRoomList.add(jsonObj);
				
 			}
 			JsonObject jsonResponse=new JsonObject();
			jsonResponse.add("roomList", occupacyRoomList);
			response.setContentType("application/json");
			response.getWriter().write(gson.toJson(jsonResponse));
			
 			log.debug(gson.toJson(jsonResponse));	
 		
		}else if("AVAILABLEROOMLIST".equalsIgnoreCase(action)){
			int buildingId=Integer.parseInt(request.getParameter("buildingId"));
			Date checkinTime=new Date();
			Date checkOutTime=new Date();
			try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			 checkinTime= sdf.parse(request.getParameter("checkInDateTime"));
					
			 checkOutTime= sdf.parse(request.getParameter("checkOutDateTime"));
			}
			catch(ParseException e){
				log.debug("Exception in parsing date and time {} in c",e.getMessage());
			}
			List<SMRoom> roomList=guestHouseManager.getRoomListByGuestHouseId(buildingId);
			Gson gson=new Gson();
			JsonArray singleOccupacyRoomList= new JsonArray();
			JsonArray doubleOccupacyRoomList= new JsonArray();
			JsonArray tripleOccupacyRoomList= new JsonArray();
			JsonArray unavailableRoomList= new JsonArray();

			boolean isRoomAvailable=false;
			

			for(SMRoom room:roomList){
				log.debug("room Id"+room.getId());
				isRoomAvailable=guestHouseManager.checkGuestRoomAvailability(room.getId(), checkinTime, checkOutTime);
				/*if(isRoomAvailable){*/
					log.debug("value of is room available {}",isRoomAvailable);
				JsonObject jsonObj=new JsonObject();
				jsonObj.addProperty("roomNumber", room.getRoomCode());
				jsonObj.addProperty("roomId", room.getId());
				jsonObj.addProperty("type", room.getRoomTypeByCapacity());

				int i=0;
				for(RoomStuff roomStuff:room.getRoomStuffList()){
					i++;
				jsonObj.addProperty("stuff"+i, roomStuff.getStuffName());
				}
				
				//if(AllocationStatus.UNALLOCATED.toString().equalsIgnoreCase(room.getAllocationStatus())){
				if(isRoomAvailable){
				if("SINGLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
					singleOccupacyRoomList.add(jsonObj);
				}
				else if("DOUBLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
					doubleOccupacyRoomList.add(jsonObj);

				}
				else if("TRIPLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
					tripleOccupacyRoomList.add(jsonObj);

				}
				//}
				}
				else{
					JsonArray historyArray=new JsonArray();
					List<MeetingRoomBookingDetail>mrbList=guestHouseManager.getRoomHistory(room.getId());
					int j=0;
					jsonObj.addProperty("temp", "ttt");
					for(MeetingRoomBookingDetail mrbd :mrbList){
						JsonObject historyJsonObj=new JsonObject();
						historyJsonObj.addProperty("checkInTime", mrbd.getBookingStartDateTime().toString());
						historyJsonObj.addProperty("checkOutTime", mrbd.getBookingStartDateTime().toString());

						historyArray.add(historyJsonObj);
						
						/*j++;
					jsonObj.addProperty("checkIn"+j, mrbd.getBookingStartDateTime().toString());
					jsonObj.addProperty("checkout"+j, mrbd.getBookingEndDateTime().toString());
*/
					
					}
					jsonObj.add("history", historyArray);
					
					unavailableRoomList.add(jsonObj);

				}
				
				
					

			}
			JsonObject jsonResponse=new JsonObject();
			jsonResponse.add("singleOccupacy", singleOccupacyRoomList);
			jsonResponse.add("doubleOccupacy", doubleOccupacyRoomList);
			jsonResponse.add("tripleOccupacy", tripleOccupacyRoomList);
			jsonResponse.add("unavailableRoomList", unavailableRoomList);

		log.debug("inside {},,,,,,,,,,checkintime{},checkouttime{}",unavailableRoomList,checkinTime,checkOutTime);

			response.setContentType("application/json");
			response.getWriter().write(gson.toJson(jsonResponse));
			
			
		}
		else if("REQUESTDETAILS".equalsIgnoreCase(action)){
			int requestId=Integer.parseInt(request.getParameter("requestId"));
			Map<String,String> requestDetailMap=guestHouseManager.getCheckinCheckOutTime(requestId);
			response.setContentType("application/json");
			response.getWriter().write(new Gson().toJson(requestDetailMap));
			
			
			
			
		}
		else if("DELETEROOM".equalsIgnoreCase(action)){
			//String stuffName=request.getParameter("stuffName");
			int roomId=Integer.parseInt(request.getParameter("roomId"));

			Boolean isDeleted=	guestHouseManager.detleteRoom(roomId);
			Map<String,String> responseMap=new HashMap<>();
			
			if(isDeleted){
				responseMap.put("status", "deleted");
				responseMap.put("msg","Room Successfully Delete");
			}
			response.setContentType("application/json");
			response.getWriter().write(new Gson().toJson(responseMap));
			
		}
		else if("UPDATEBEDTYPE".equalsIgnoreCase(action)){
			//String stuffName=request.getParameter("stuffName");
			int roomId=Integer.parseInt(request.getParameter("roomId"));
			String bedType=request.getParameter("bedType");
			Boolean isUpdated=	guestHouseManager.updateBedType(roomId, bedType);
			Map<String,String> responseMap=new HashMap<>();
			
			if(isUpdated){
				responseMap.put("status", "Updated");
				responseMap.put("msg","Bed Type Successfully Updated");
			}
			response.setContentType("application/json");
			response.getWriter().write(new Gson().toJson(responseMap));
			
		}else if("UPDATEROOMCODE".equalsIgnoreCase(action)){
			//String stuffName=request.getParameter("stuffName");
			int roomId=Integer.parseInt(request.getParameter("roomId"));
			String roomCode=request.getParameter("roomCode");
			Boolean isUpdated=	guestHouseManager.updateRoomCode(roomId, roomCode);
			Map<String,String> responseMap=new HashMap<>();
			
			if(isUpdated){
				responseMap.put("status", "Updated");
				responseMap.put("msg","Room Number Successfully Updated");
			}
			response.setContentType("application/json");
			response.getWriter().write(new Gson().toJson(responseMap));
			
		}else if("DELETEGUESTHOUSE".equalsIgnoreCase(action)){
			//String stuffName=request.getParameter("stuffName");
			int guestHouseId=Integer.parseInt(request.getParameter("buildingId"));
			Boolean isUpdated=	guestHouseManager.deleteGuestHouse(guestHouseId);
			Map<String,String> responseMap=new HashMap<>();
			
			if(isUpdated){
				responseMap.put("status", "Deleted");
				responseMap.put("msg","Guest House Successfully Deleted");
			}
			response.setContentType("application/json");
			response.getWriter().write(new Gson().toJson(responseMap));
			
		}
		
		
	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
