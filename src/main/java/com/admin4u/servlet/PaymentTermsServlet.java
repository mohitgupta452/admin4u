package com.admin4u.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.admin4u.persistence.entity.Paymentterm;
import com.admin4u.service.bussiness.contract.VendorManager;
import com.java.AlertMessages;


@WebServlet("/PaymentTermsServlet")
public class PaymentTermsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	VendorManager vendorManager;
    public PaymentTermsServlet() {
        super();
    }

@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	String action = request.getParameter("action");
	
	if("action".equalsIgnoreCase(action)){	
		Map<String,String> paymentMap = new HashMap<String,String>();
		paymentMap.put("paymentTerms",request.getParameter("paymentTerms"));
		paymentMap.put("penalityTerm", request.getParameter("penalityTerm"));
		paymentMap.put("shippingMethod", request.getParameter("shippingMethod"));
		paymentMap.put("shippingTerms", request.getParameter("shippingTerms"));
		paymentMap.put("advance",request.getParameter("advance"));
		
		vendorManager.insertPaymentData(paymentMap);
		HttpSession session = request.getSession(false);
	    session.setAttribute("isPRG", "true");
		response.sendRedirect("PRGServlet?status=success&url=user/vendor.jsp&alertMessage="+ AlertMessages.requestSent);
	}
	else 
    if(action.equalsIgnoreCase("callPaymentInactive"))
	{
    	
		int paymentId = Integer.parseInt(request.getParameter("ID"));
	   vendorManager.inActiveThePaymentRow(paymentId);
		
	}
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
