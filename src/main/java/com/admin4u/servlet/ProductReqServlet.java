package com.admin4u.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;

import com.admin4u.persistence.entity.RequestData;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.util.service.RequestMetadata.ProductKey;
import com.java.AlertMessages;
import com.admin4u.views.UserSessionBean;

/**
 * Servlet implementation class ProductReqServlet
 */
@WebServlet("/ProductReqServlet")
public class ProductReqServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    
	
	@Inject
	private RequestResponseManager requestResponseManager;
	
	@Inject
	private Logger log;
	
	@Inject
	private UserSessionBean userSessionBean;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductReqServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		Map<String , String[]> map = request.getParameterMap();
			for (Map.Entry<String, String[]> entry : map.entrySet()) {
			    System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue()[0]);
			}	
		
		String action = request.getParameter("action");
		if("insertProductReqData".equalsIgnoreCase(action)){
		Map<String,String[]> reqMap= new HashMap<>();
		String serviceList=""; 
			serviceList=Services.INVENTORY.toString();
			reqMap.put("serviceList",new String[] {serviceList});
			reqMap.put(RequestMetadata.LOCATION,new String[] {userSessionBean.getLocationName()});
			reqMap.put(RequestMetadata.REQUEST_CREATION_TIME,new String []{new Date().toString()});
			reqMap.put(RequestMetadata.REQUESTER,new String[] { userSessionBean.getEmpData().getUserPrincipalName()});
			reqMap.put(RequestMetadata.Service,new String[] {Services.INVENTORY.toString()});
			reqMap.put(RequestMetadata.Request_IP, new String []{request.getRemoteAddr()});	
    	 reqMap.putAll(request.getParameterMap());
		requestResponseManager.insertData(reqMap);

		HttpSession session = request.getSession(false);
	    session.setAttribute("isPRG", "true");
		response.sendRedirect("PRGServlet?status=success&url=user/product_requisition.jsp&alertMessage="+ AlertMessages.requestSent);

		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
