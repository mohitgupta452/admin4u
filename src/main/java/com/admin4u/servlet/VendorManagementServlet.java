package com.admin4u.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.admin4u.persistence.entity.Address;
import com.admin4u.persistence.entity.ContactDetail;
import com.admin4u.persistence.entity.VendorInfo;
import com.admin4u.service.bussiness.contract.VendorManager;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

@WebServlet("/VendorManagementServlet")
@MultipartConfig
public class VendorManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	VendorManager vendorManager;
	
    public VendorManagementServlet() {
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	
    	Map<String , String > vendorInfoMap = new HashMap<String, String>();
		vendorInfoMap.put("vLegalName", request.getParameter("vLegalName"));
		vendorInfoMap.put("businessDescription", request.getParameter("businessDescription"));
		vendorInfoMap.put("companyFax", request.getParameter("companyFax"));
		vendorInfoMap.put("deliveryMethod", request.getParameter("deliveryMethod"));
		vendorInfoMap.put("stateIncorporation", request.getParameter("stateIncorporation"));
		vendorInfoMap.put("incorporationYear",request.getParameter("incorporationYear") );
		vendorInfoMap.put("tin", request.getParameter("tin"));
		vendorInfoMap.put("vendorName", request.getParameter("vendorName"));
		vendorInfoMap.put("serviceTax", request.getParameter("serviceTax"));
		vendorInfoMap.put("identificationNo", request.getParameter("identificationNo"));
		vendorInfoMap.put("vendorInfoId", request.getParameter("vendorInfoId"));
		vendorInfoMap.put("companyPhone", request.getParameter("companyPhone"));
		vendorInfoMap.put("id", request.getParameter("vendorInfoId"));
    	
		Part escpart=request.getPart("escAttach");
		String escAttach=FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), escpart);
		vendorInfoMap.put("escUpload",escAttach);
		
		Part panpart=request.getPart("panCard");
		String panCard=FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), panpart);
		vendorInfoMap.put("panUpload",panCard);
		
		Part decpart=request.getPart("decLetter");
		String decLetter = FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), decpart);
		vendorInfoMap.put("decUpload" , decLetter);
		
		Part cancelpart=request.getPart("cancelCheck");
		String cancelCheck=FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), cancelpart);
		vendorInfoMap.put("cancelUpload" , cancelCheck);
		
		String sameAddress = request.getParameter("sameAddress");
	if("on".equalsIgnoreCase(sameAddress)){
		vendorInfoMap.put("city", request.getParameter("city"));
		vendorInfoMap.put("country",request.getParameter("country") );
		vendorInfoMap.put("locality", request.getParameter("locality"));
		vendorInfoMap.put("pin", request.getParameter("pin"));
		vendorInfoMap.put("state", request.getParameter("state"));
		vendorInfoMap.put("street", request.getParameter("street"));
		vendorInfoMap.put("type", "Address Information");

		}
	else
	{
		vendorInfoMap.put("city", request.getParameter("city"));
		vendorInfoMap.put("country",request.getParameter("country") );
		vendorInfoMap.put("locality", request.getParameter("locality"));
		vendorInfoMap.put("pin", request.getParameter("pin"));
		vendorInfoMap.put("state", request.getParameter("state"));
		vendorInfoMap.put("street", request.getParameter("street"));
		vendorInfoMap.put("type", "Address Information");
		
		vendorInfoMap.put("mCity", request.getParameter("mailCity"));
		vendorInfoMap.put("mCountry",request.getParameter("mailCountry") );
		vendorInfoMap.put("mLocality", request.getParameter("mailLocality"));
		vendorInfoMap.put("mPin", request.getParameter("mailPin"));
		vendorInfoMap.put("mState", request.getParameter("mailState"));
		vendorInfoMap.put("mStreet", request.getParameter("mailStreet"));
		vendorInfoMap.put("mType", "Mail Address Information");
		
	}
	
		vendorManager.insertVendorData(vendorInfoMap , sameAddress);
		HttpSession session = request.getSession(false);
	    session.setAttribute("isPRG", "true");
		response.sendRedirect("PRGServlet?status=success&url=user/vendor.jsp&alertMessage="+ AlertMessages.requestSent);
		
	}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
