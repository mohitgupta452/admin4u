package com.admin4u.servlet;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.service.bussiness.contract.ActionManager;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.service.ApplicationMetaData.Action;
import com.java.AlertMessages;
import com.mysql.jdbc.TimeUtil;
import com.turningcloud.utils.FilesUtils;

@WebServlet("/SpocServlet")
@MultipartConfig
public class SpocServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Inject
	private ActionManager actionManager;
	
	@Inject
	private RequestResponseManager reqResManager;
	
	@Inject
	private GuestHouseManager guestHouseManager;
	
    public SpocServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		log.debug("spoc servlet call()");
		
	        String action=request.getParameter("action").toString();
	        int actionId=0;
			if(request.getParameterMap().containsKey("actionId"))
	        actionId=Integer.parseInt(request.getParameter("actionId"));
			
	    
	        if(action.equals("SPOC_SERVE"))
	       {
	        	List<Part> parts = (List) request.getParts();
				List<String> filesPath = new ArrayList<>();

				String filePath = File.separator + "Request "
						+ actionManager.getUserActionByActionId(actionId).getRequestMaster().getRequestId() + File.separator
						+ "Response";

				for (Part part : parts) {
					if (part != null && part.getSubmittedFileName() != null)
						filesPath
								.add(FilesUtils.saveFile(new File(FilesUtils.getPlatformBasedParentDir(), filePath), part));
				}
	    	   Map<String, String[]> reqMap = request.getParameterMap();

	    	//code to mange guest house booking request
				if(request.getParameterMap().containsKey("guestHouseRoomId")&&!request.getParameter("guestHouseRoomId").isEmpty()){
		    		if(request.getParameter("guestHouse").equalsIgnoreCase("Guest_House"))  {
					int roomId=Integer.parseInt(request.getParameter("guestHouseRoomId"));
		    		  Date checkInDateTime=new Date();
		    		  Date checkOutDateTime=new Date();
		    		  log.debug("inside auth servlet");
		    		  try{
		    				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		    				 checkInDateTime= sdf.parse(request.getParameter("checkinDateTime"));
		    						
		    				 checkOutDateTime= sdf.parse(request.getParameter("checkoutDateTime"));
		    				}
		    				catch(ParseException e){
		    					log.debug("exceptin in parsing date in guestHouseInvetory spoc serv",e.getMessage());
		    				}
		    		  
		    		  
		    		  guestHouseManager.processGuestHouseRequestNew(actionId, roomId, checkInDateTime, checkOutDateTime);
		    		  reqMap.putAll(guestHouseManager.getRoomDetailMap(roomId));
		    		}
		    	  }
				
	//end		
	     	  actionManager.spocServe(actionId,filesPath ,reqMap);

	     	 HttpSession session = request.getSession(false);
	         session.setAttribute("isPRG", "true");
	    	 response.sendRedirect("PRGServlet?status=success&url=user/spoc.jsp&alertMessage="+ AlertMessages.spocBookTicket);
	       }
	       else if(action.equals("SPOC_CLOSE"))
	       {
	    	   log.debug("inside spoc close block");
	    	   String reqData=  actionManager.spocClose(actionId,request.getParameter("responseMsg"));
	    	   log.debug("inside spoc close block");

	    	   response.setContentType("application/json");
	 	  	   response.getWriter().write(reqData);
	 	  		
	      }
	       else if(action.equals("SPOC_COMPLETE"))
	       {
	    	 
	    	   String msg = request.getParameter("extendMsg");
	    	  // log.debug("+++",msg);
	    	  // log.debug("++++",request.getParameter("extendTimeDate"));
	    	 //  Date extentionTime = new Date(request.getParameter("extendTimeDate"));
	    	    Date extentionTime =new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				try {
					extentionTime= sdf.parse(request.getParameter("extendTimeDate"));
				} catch (ParseException e) {
					e.printStackTrace();
				}
	    	   
	    	   String reqData= actionManager.extend(actionId, msg, extentionTime,Action.SPOC_EXTEND);

	    	   response.setContentType("application/json");
	 	  		response.getWriter().write(reqData);
	      }
	       else if(action.equals("GETCOMMENTS"))
	       {
	    	    String comments= actionManager.getCommentsByRequest(Integer.parseInt(request.getParameter("requestId")));
		    	   response.setContentType("application/json");

	    	    response.getWriter().write(comments);
	       }
	       else if(action.equals("SETEXTENDDETAILS"))
	       {
		    	  int tatTime=Integer.parseInt(request.getParameter("tatTime"));
		    	  String comment=request.getParameter("comment");
		    	  log.debug("tatTime {} comment {}",tatTime,comment);
		    	  Date tatDateTime=DateUtils.addHours(new Date(), tatTime);
		    	  log.debug("new date {}",tatDateTime);
		    	  String status=actionManager.extend(actionId, comment, tatDateTime,Action.SPOC_EXTEND);
		    	   response.setContentType("application/json");
		    	   
	    	   response.getWriter().write(status);
	       }
	        
	       else
	        response.sendRedirect("user/spoc.jsp");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
