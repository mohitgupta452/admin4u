package com.admin4u.servlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.PriorityTatMap;
import com.admin4u.service.bussiness.contract.NotificationManager;
import com.admin4u.service.bussiness.contract.ServiceManager;
import com.google.gson.Gson;

/**
 * Servlet implementation class ApprovalAuthServlet
 */
@WebServlet("/PriorityTatMapServlet")
public class PriorityTatMapServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private  Logger log = LoggerFactory.getLogger(getClass());
	
	@Inject
	private NotificationManager notificationManager;
	
	@Inject
	private ServiceManager serviceManager;
    
    public PriorityTatMapServlet() {
        super();
      
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action="";
        log.debug("Priority Tat Map  servlet call()");

              
          if(request.getParameterMap().containsKey("action"))
          {
         action=request.getParameter("action").toString();
        
        if(action.equals("UPDATE"))
        {
        	PriorityTatMap ptm=new  PriorityTatMap();
        	ptm.setId(Integer.parseInt(request.getParameter("priorityTatMapID")));
        	ptm.setPriority(request.getParameter("priority"));
        	ptm.setTurnAroundTime(Long.parseLong(request.getParameter("turnAroundTime")));
        	ptm.setService(request.getParameter("service"));
        	 
        	  log.debug("value of service {}",request.getParameter("service"));
        	  
        	  String requestJsonString=serviceManager.updateTatValue(ptm);
        	

        	  response.setContentType("application/json");
        	  PrintWriter out=response.getWriter();
               out.write(requestJsonString);
               log.debug("response  {}",requestJsonString);
            
        }
        else  if(action.equals("ADDNEWPRIORITY")){
        	
        	PriorityTatMap ptm=new  PriorityTatMap();
        	ptm.setPriority(request.getParameter("priority"));
        	ptm.setTurnAroundTime(Long.parseLong(request.getParameter("tat")));
        	ptm.setService(request.getParameter("service"));
        	
        	
      	  
      	  String requestJsonString=serviceManager.addNewPriority(ptm);;
        	response.setContentType("application/json");
       	  PrintWriter out=response.getWriter();
              out.write(requestJsonString);
        	
        }

        
          }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
