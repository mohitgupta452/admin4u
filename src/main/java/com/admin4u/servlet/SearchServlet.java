package com.admin4u.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.views.UserSessionBean;

@WebServlet("/Search")
public class SearchServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    @Inject
    private UserSessionBean userSession;
    
    @Inject
    private RequestResponseManager  requestResponseManager;
	
	public SearchServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Map<String,Object> searchMap = new HashMap<>();
		
		String searchBy=request.getParameter("searchby");
		
		String searchValue= request.getParameter(searchBy);
		
		searchMap.put(searchBy,searchValue);
		searchMap.put("requestBy",userSession.getEmpData().getUserPrincipalName());
		
		requestResponseManager.search(searchMap);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
