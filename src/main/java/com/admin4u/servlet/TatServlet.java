package com.admin4u.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;

import com.admin4u.persistence.entity.TatMaster;
import com.admin4u.service.bussiness.contract.EventTatManager;
import com.admin4u.util.service.ApplicationMetaData.TatLevelKeys;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.java.AlertMessages;

/**
 * Servlet implementation class TatServlet
 */
@WebServlet("/TatServlet")
public class TatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@Inject
	private EventTatManager etManager;
	
	@Inject
	private Logger log;
  
    public TatServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		log.debug("inside tatServlet ");

		String action=request.getParameter("action");
		
		if("SAVEDATA".equalsIgnoreCase(action)){
			log.debug("inside tatServlet SaveData");
			String tatDuration=request.getParameter("tatDuration");
			String priority=request.getParameter("priority");
			String tatService=request.getParameter("tatService");
			String level1TatTime=request.getParameter("level1TatTime");
			String level2TatTime=request.getParameter("level2TatTime");
			String level3TatTime=request.getParameter("level3TatTime");
			String level4TatTime=request.getParameter("level4TatTime");
			String spocComment=request.getParameter("spocComment");
			TatMaster tatMaster=new TatMaster();
			tatMaster.setPriority(priority);
			tatMaster.setTatService(tatService);
			tatMaster.setComments(spocComment);
			JsonObject tatLevel=new JsonObject();
			tatLevel.addProperty(TatLevelKeys.level1TatTime.toString(), level1TatTime !=null ?level1TatTime:null);
			tatLevel.addProperty(TatLevelKeys.level2TatTime.toString(), level2TatTime !=null ?level2TatTime:null);
			tatLevel.addProperty(TatLevelKeys.level3TatTime.toString(), level3TatTime !=null ?level3TatTime:null);
			tatLevel.addProperty(TatLevelKeys.level4TatTime.toString(), level4TatTime !=null ?level4TatTime:null);

			
			tatMaster.setTatLevel(tatLevel);
			boolean isTatExist=etManager.isTatExist(tatService);
			if(!isTatExist)
			etManager.addTatPriority(tatMaster);
			HttpSession session = request.getSession(false);
			session.setAttribute("isPRG", "true");
			if(isTatExist)
			response.sendRedirect(
					"PRGServlet?status=success&url=user/admin/tatService.jsp&alertMessage=" + "Tat Previously Exist");
			else
				response.sendRedirect(
						"PRGServlet?status=success&url=user/admin/tatService.jsp&alertMessage=" + "Tat Details Successfully Saved");

			


			
		}
		else if("DELETETAT".equalsIgnoreCase(action)){
			log.debug("inside delete");
			int tatId=Integer.parseInt(request.getParameter("tatId"));
			boolean status=etManager.deleteTatById(tatId);
			Map<String,String>resp=new HashMap<>();
			resp.put("msg", "Tat Details Not Deleted");

			
			if(status)
				resp.put("msg", "Tat Details Successfully Deleted");

			response.setContentType("application/json");
			response.getWriter().write(new Gson().toJson(resp));

			
		}
		
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
