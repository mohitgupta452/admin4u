package com.admin4u.servlet;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;

/*

Author: Ajay Singh Negi
*/

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.util.service.ExcelCreation;

@WebServlet("DownloadController")
class DownloadServlet extends HttpServlet{

	private static final long serialVersionUID = 1907896662110490431L;
	Logger log = LoggerFactory.getLogger(getClass());
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response){
	
		 String filespath=request.getParameter("filename");
		
		if(filespath != null && !filespath.isEmpty())
			if(!filespath.contains("|"))
				this.downloadSingleFile(filespath,request,response);
		
		try{
			List<File> files = new ArrayList<>();
			log.debug("file {}.....{}",request.getParameter("filename"),request.getParameterMap());
			String filePath="";
			StringTokenizer stringTokenizer=new StringTokenizer(filespath, "|");
			while(stringTokenizer.hasMoreTokens())
			{
				filePath=stringTokenizer.nextToken();
				files.add(new File(filePath));
				
			}
			ServletOutputStream zipout = response.getOutputStream();
			ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(zipout));

		for(File file : files)	
		{
			log.debug("file name  {}",file.getName());
		response.setContentType("APPLICATION/ZIP");
		
		response.setHeader("Content-Disposition",
				"attachment; filename=attachments.zip");
		
				zos.putNextEntry(new ZipEntry(file.getName()));
		
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);

		} catch (FileNotFoundException fnfe) {
			// If the file does not exists, write an error entry instead of
			// file
			// contents
			zos.write(("ERRORld not find file " + file.getName())
					.getBytes());
			zos.closeEntry();
			continue;
		}

		BufferedInputStream fif = new BufferedInputStream(fis);

		// Write the contents of the file
		int data = 0;
		while ((data = fif.read()) != -1) {
			zos.write(data);
		}
		fif.close();

		zos.closeEntry();
		System.out.println("Finishedng file " + file.getName());
		
	    }
		zos.close();
		}
			catch(Exception e)
			{
				e.getMessage();
			}
	
	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) 
	{
		doGet(request, response);
		
	}
	
	private void downloadSingleFile(String filePath,HttpServletRequest request,HttpServletResponse response)
	{
		try{
			Path path=Paths.get(filePath);
		PrintWriter out = response.getWriter();
		String filename = path.getFileName().toString();
		String filepath = path.toString();
		
		log.debug("file path {}....file name ...{}",filepath,filename);
		
		response.setContentType("APPLICATION/OCTET-STREAM");
		
		response.setHeader("Content-Disposition", "attachment; filename=\""
				+ filename + "\"");
		
		FileInputStream fileInputStream = new FileInputStream(filepath);
 
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
		}
		catch(IOException e)
		{
			e.getMessage();
		}
	}
	
	
}