package com.admin4u.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.service.MultipartUtils;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.RoleManagementBean;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

@WebServlet("/RoleManagementServlet")
public class Rms extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private RoleManager roleManager;

	@Inject
	private RoleManagementBean roleManagementBean;

	@Inject
	private UserSessionBean userSession;

	public Rms() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
         String userRole=request.getParameter("as");
		String action = request.getParameter("action");
		if (action.equalsIgnoreCase("DELETEROLE")) {
			roleManager.deleteRole(Integer.parseInt(request.getParameter("employeeRoleID")));
			Map<String, String> responseMap = new HashMap<String, String>();
			responseMap.put("isDeleted", "true");
			String json = new Gson().toJson(responseMap);
			response.setContentType("application/json");
			response.getWriter().write(json);
			log.debug(json);
		} else if (action.equalsIgnoreCase("forwordRoleJSP")) {
			roleManagementBean.setRole(userRole);
			log.debug("role {}",roleManagementBean.getRole());
			request.setAttribute("alertMessage",request.getParameter("alertMessage"));
			RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/user/role_management.jsp?as="+userRole);
			rd.forward(request, response);
		} else if ("assignRole".equalsIgnoreCase(action)) {
			String role = request.getParameter("role");
			String[] employeesUPN = request.getParameterValues("employeesUPN");
			String location = request.getParameter("location");
			String[] services = request.getParameterValues("services");
			String createdBy = userSession.getEmpData().getUserPrincipalName();
			String messages = roleManager.createRole(employeesUPN, Roles.valueOf(role), location, services,
					createdBy);
			response.sendRedirect("RoleManagementServlet?&action=forwordRoleJSP&as="+userRole+"&alertMessage="+messages);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}
}