package com.admin4u.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.UserSessionBean;
import com.java.AlertMessages;

@WebServlet("/cabrequestservlet")

public class CabRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	@Inject
	private RequestResponseManager reqResManager;
	
	@Inject
	private UserSessionBean userSession;
	
   
    public CabRequestServlet() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action="";
              Map<String, String[]> reqMap = request.getParameterMap();
              
              log.debug("hng servlet call()");
              String upn = userSession.getEmpData().getUserPrincipalName();
              String location=userSession.getLocationName();
              
             if(request.getParameterMap().containsKey("action"))
              action=request.getParameter("action").toString();
              
              log.debug("action.....{}..loc..{}",action,location);

                  if(action != null)   
                  {
              if(action.equals("GENERATED"))
              {
		reqMap.put(RequestMetadata.REQUEST_CREATION_TIME,new String []{new Date().toString()});
		reqMap.put(RequestMetadata.LOCATION,new String[]{location});
		reqMap.put(RequestMetadata.REQUESTER,new String[] {upn});
		reqMap.put(RequestMetadata.REQUEST_TYPE,new String[] {request.getParameter("bookingFor")});
		reqMap.put(RequestMetadata.Request_IP,new String[]{request.getRemoteAddr()});
		log.debug("hng {}",reqMap);
		 
		String serviceList=""; 
		serviceList=request.getParameter("cabselcect");
		reqMap.put("serviceList",new String[] {serviceList});
  if(request.getParameter("cabselcect").equalsIgnoreCase(Services.CABPICK.toString())){
	  reqMap.put(RequestMetadata.Service,new String[] {Services.CABPICK.toString()});
	  
		String respMsg="";
	    if(reqResManager.insertData(reqMap) == null)
		   respMsg="Sorry Your Manager is not mapped ";
	    else
	    	respMsg =AlertMessages.requestSent;    }
    if(request.getParameter("cabselcect").equalsIgnoreCase(Services.CABDROP.toString())){
    	 reqMap.put(RequestMetadata.Service,new String[] {Services.CABDROP.toString()});
    		String respMsg="";
    	    if(reqResManager.insertData(reqMap) == null)
    		   respMsg="Sorry Your Manager is not mapped ";
    	    else
    	    	respMsg =AlertMessages.requestSent;      
    }
    if(request.getParameter("cabselcect").equalsIgnoreCase(Services.CABDISPOSAL.toString())){
    	 reqMap.put(RequestMetadata.Service,new String[] {Services.CABDISPOSAL.toString()});
    		String respMsg="";
    	    if(reqResManager.insertData(reqMap) == null)
    		   respMsg="Sorry Your Manager is not mapped ";
    	    else
    	    	respMsg =AlertMessages.requestSent;      
    }
if(request.getParameter("cabselcect").equalsIgnoreCase(Services.CABAIRPORT.toString())){
	//reqMap.put(RequestMetadata.REQUEST_TYPE,new String[] {request.getParameter("bookingFor")});

	 reqMap.put(RequestMetadata.Service,new String[] {Services.CABAIRPORT.toString()});
		String respMsg="";
	    if(reqResManager.insertData(reqMap) == null)
		   respMsg="Sorry Your Manager is not mapped ";
	    else
	    	respMsg =AlertMessages.requestSent;      
    }		 
HttpSession session = request.getSession(false);
session.setAttribute("isPRG", "true");
response.sendRedirect("PRGServlet?status=success&url=user/tour_travel.jsp&alertMessage="+ AlertMessages.requestSent);
              }
                               }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
}
