package com.admin4u.servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.admin4u.persistence.entity.ContractDetail;
import com.admin4u.service.bussiness.contract.VendorManager;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;


@WebServlet("/ContractManagementServlet")
@MultipartConfig
public class ContractManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	VendorManager vendorManager;
    public ContractManagementServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		 Map<String , String> contractMap = new HashMap<String , String >();
		 contractMap.put("agreementType",request.getParameter("agreementType"));
		 contractMap.put("fixedCost",request.getParameter("fixedCost"));
		// contractDetail.setContractDetailscol(request.getParameter(""));
		 contractMap.put("escalation",request.getParameter("escalation"));
		 contractMap.put("landlordName",request.getParameter("landlordName"));
		 contractMap.put("leaseCommDate",request.getParameter("leaseCommDate"));
		 contractMap.put("leaseEndDate",request.getParameter("leaseEndDate"));
		 contractMap.put("lesseeName",request.getParameter("lesseeName"));
		 contractMap.put("lesserName",request.getParameter("lesserName"));
		 contractMap.put("lockPeriod",request.getParameter("lockPeriod"));
		 contractMap.put("monthlyMaintenance",request.getParameter("monthlyMaintenance"));
		 contractMap.put("monthlyRent",request.getParameter("monthlyRent"));
		 contractMap.put("escalationDate",request.getParameter("escalationDate"));
		 contractMap.put("noticePeriod",request.getParameter("noticePeriod"));
		 contractMap.put("parkingCharges",request.getParameter("parkingCharges"));
		 contractMap.put("paymentDate",request.getParameter("paymentDate"));
		 contractMap.put("powerBack",request.getParameter("powerBack"));
		 contractMap.put("initiated",request.getParameter("initiated"));
		 contractMap.put("securityDeposit",request.getParameter("securityDeposit"));
		 contractMap.put("totalArea",request.getParameter("totalArea"));
		 contractMap.put("totalRentPayable",request.getParameter("totalRentPayable"));
		 contractMap.put("id",request.getParameter("contractId"));
		 Part contractpart=request.getPart("contractAttach");
		 String contractAttach=FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), contractpart);
		 contractMap.put("upload",contractAttach);
	
		 vendorManager.insertMISContractData(contractMap);
		 HttpSession session = request.getSession(false);
		 session.setAttribute("isPRG", "true");
	     response.sendRedirect("PRGServlet?status=success&url=user/vendor.jsp&alertMessage="+ AlertMessages.requestSent);
		  
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
