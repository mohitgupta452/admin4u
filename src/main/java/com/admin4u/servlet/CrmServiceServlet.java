package com.admin4u.servlet;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.UserSessionBean;
import com.java.AlertMessages;

/**
 * Servlet implementation class RepaimaintainceServlet
 */
@WebServlet("/CrmServiceServlet")
public class CrmServiceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	@Inject
	 private RequestResponseManager reqResManager;
	
	@Inject
	private UserSessionBean userSession;
	
	@Inject
	private IDMService idmService;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
    public CrmServiceServlet() {
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
         Map<String, String[]> reqMap = request.getParameterMap();
         
         String upn = idmService.getEmployeeUPNByEmployeeId(request.getParameter("employeeID"));
         
         String action=request.getParameter("action").toString();
         
         if(action.equals("GENERATED"))
         {
        	 String serviceList=""; 
 			serviceList=Services.REPAIRMAINTENANCE.toString();
 			reqMap.put("serviceList",new String[] {serviceList});
        	 
    reqMap.put(RequestMetadata.LOCATION,new  String [] {userSession.getLocationName()});
	reqMap.put(RequestMetadata.REQUEST_CREATION_TIME,new String []{new Date().toString()});
	reqMap.put(RequestMetadata.REQUESTER,new String[] {upn});
	reqMap.put(RequestMetadata.REQUEST_TYPE,new String[] {"self"});
	reqMap.put(RequestMetadata.Request_IP,new String[]{request.getRemoteAddr()});
	reqMap.put(RequestMetadata.Service,new String[] {Services.REPAIRMAINTENANCE.toString()});
	reqMap.put(RequestMetadata.REQUEST_STATUS,new String[] {ApplicationMetaData.Status.ADMIN_GENRATED.toString()});

	 reqResManager.insertData(reqMap);
	 HttpSession session = request.getSession(false);
     session.setAttribute("isPRG", "true");
	 response.sendRedirect("PRGServlet?status=success&url=user/cservice_log/crm_service.jsp&alertMessage="+ AlertMessages.requestSent);
         }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
