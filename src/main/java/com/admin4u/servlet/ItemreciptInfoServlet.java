package com.admin4u.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;

import com.admin4u.service.bussiness.contract.InventoryManager;
import com.admin4u.util.service.RequestMetadata.ProductKey;
import com.java.AlertMessages;

/**
 * Servlet implementation class ItemreciptInfoServlet
 */
@WebServlet("/ItemreciptInfoServlet")
public class ItemreciptInfoServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    @Inject
    private InventoryManager inventoryManager;
    
    @Inject
    private Logger log;
	
	public ItemreciptInfoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		log.debug("++++++"+request.getParameter("receiptItemName1"));
		log.debug("tatal {}",request.getParameter("total"));
		
	List<Map<String,Object>> productsRecieve = new ArrayList<>();
	
	for(int i=1;i<=Integer.parseInt(request.getParameter("total"));i++)
	{
	Map<String,Object> product = new HashMap<>();
	
	product.put(ProductKey.PRODUCTID,request.getParameter("receiptItemName"+i));
	product.put(ProductKey.QUANTITY,request.getParameter("itemQtyRec"+i));
	product.put(ProductKey.ORDER_QUANTITY,request.getParameter("itemQtyOrd"+i));
	product.put(ProductKey.PONUMBER,request.getParameter("pNumber"+i));
	product.put(ProductKey.VENDORID,request.getParameter("prdRecSupplier"+i));
	product.put(ProductKey.PRICE, request.getParameter("itemUnitPrice"+i));
	product.put(ProductKey.RECIEVE_DATE,new Date());
	productsRecieve.add(product);
	}
	inventoryManager.updateStock(productsRecieve);
	HttpSession session = request.getSession(false);
    session.setAttribute("isPRG", "true");
	response.sendRedirect("PRGServlet?status=success&url=user/inventoryManager/stPrintItemInfo/product_receiptInfo.jsp&alertMessage="+ AlertMessages.requestSent);

	
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
