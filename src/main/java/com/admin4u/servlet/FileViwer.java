package com.admin4u.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/fileviwer")
public class FileViwer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public FileViwer() {
        super();
    }

    protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Path path=Paths.get(request.getParameter("path"));
		PrintWriter out = response.getWriter();
		String filename = path.getFileName().toString();
		String filepath = request.getParameter("path");
		
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline; filename=\""
				+ filename + "\"");
 
		
		FileInputStream fileInputStream = new FileInputStream(filepath);
 
		int i;
		while ((i = fileInputStream.read()) != -1) {
			out.write(i);
		}
		fileInputStream.close();
		out.close();
	}

		
		
	
	
	public void doPost(HttpServletRequest request,HttpServletResponse response)
	{
		
		
		
	}
}
