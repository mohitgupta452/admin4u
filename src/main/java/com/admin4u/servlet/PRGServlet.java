package com.admin4u.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java.AlertMessages;

@WebServlet("/PRGServlet")
public class PRGServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	Logger log= LoggerFactory.getLogger(getClass());

	public PRGServlet() {
	}

	
	protected void doGet(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		String url;
		String status;
		String alertMessage;
		url=request.getParameter("url");

		 HttpSession session = request.getSession(false);
	     
		if(session.getAttribute("isPRG")!=null){
			
			session.removeAttribute("isPRG");
			status=request.getParameter("status");
			alertMessage=request.getParameter("alertMessage");
			log.debug("inside prg servlet status={},url={},alert message={}",status,url,alertMessage);
			 request.setAttribute("alertMessage", alertMessage);
		}
		
		request.getRequestDispatcher(url).forward(request, response);

		
	}
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		
	}
}
