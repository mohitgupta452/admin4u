package com.admin4u.servlet;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.admin4u.service.bussiness.contract.ActionManager;
import com.admin4u.util.service.ApplicationMetaData.Action;
@WebServlet("/approvalauth")
public class ApprovalAuthServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private Logger log = LoggerFactory.getLogger(getClass());
    
	@Inject
	private ActionManager actionManager;
	
    public ApprovalAuthServlet() {
        super();
      
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action="";
        log.debug("approval  servlet call()");
              
          if(request.getParameterMap().containsKey("action"))
          {
         action=request.getParameter("action").toString();
         
         int actionId=Integer.parseInt(request.getParameter("actionId"));
        
        if(action.equals(Action.APPROVAL_AUTHORITY_ACCEPT.toString()) || action.equals(Action.APPROVAL_AUTHORITY_REJECT.toString()))
        {
        	 response.setContentType("application/json");
        	  PrintWriter out=response.getWriter();
        	  log.debug(request.getParameter("actionId"));
      	 String requestJsonString=actionManager.apprrovalAuthAction(actionId,Action.valueOf(action));
               out.write(requestJsonString);
               log.debug("response  {}",requestJsonString);
        }
          }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
