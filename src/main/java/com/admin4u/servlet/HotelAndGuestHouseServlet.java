package com.admin4u.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.HotelAndGuestHouseBean;
import com.admin4u.views.UserSessionBean;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

/**
 * Servlet implementation class HotelAndGuestHouseServlet
 */
@MultipartConfig
@WebServlet("/HotelAndGuestHouseServlet")
public class HotelAndGuestHouseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private RequestResponseManager reqResManager;

	@Inject
	UserSessionBean userSession;
	
	public HotelAndGuestHouseServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = "";
		Map<String, String[]> reqMap = request.getParameterMap();
		String upn = userSession.getEmpData().getUserPrincipalName();
	    String location=userSession.getLocationName();
	      

		if (request.getParameterMap().containsKey("action"))
			action = request.getParameter("action").toString();

		if (action != null) {
			
			String serviceList=""; 
			serviceList=request.getParameter("service").toString();
			if(request.getParameter("availCab") != null &&  !request.getParameter("availCab").equals("") )
				serviceList+="|"+request.getParameter("availCab");
			reqMap.put("serviceList",new String[] {serviceList});
			reqMap.put(RequestMetadata.LOCATION, new String[] {location});
			reqMap.put(RequestMetadata.REQUESTER, new String[] { upn });
			reqMap.put(RequestMetadata.Request_IP,new String [] {request.getRemoteAddr()});
			Part part = request.getPart("approvedAttachment");
			String upload = FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), part);
			reqMap.put("upload", new String[] { upload });
			
//manage multiple request
       	 int total=Integer.parseInt(request.getParameter("totalPassengers"));
       	 List<String> paxNameList=new ArrayList<>();
       	// List<String> paxIdList=new ArrayList<>();
       	 for(int counter=1;counter<=total;counter++){
       		 paxNameList.add(request.getParameter("passenger"+counter));
       		// paxIdList.add(request.getParameter("employeeID"+counter));
       	 }
       	  reqMap.put("passenger", paxNameList.toArray(new String[0]))  ;	
       	  reqMap.put("totalPax", new String[] {String.valueOf(total)});
       	  //reqMap.put("employeeID", paxIdList.toArray(new String[0]))  ;	 

	//end manage multiple request		
			
      	String respMsg="";
	    if(reqResManager.insertData(reqMap) == null)
		   respMsg="Sorry Your Manager is not mapped ";
	    else
	    	respMsg =AlertMessages.requestSent;
	    HttpSession session = request.getSession(false);
	     session.setAttribute("isPRG", "true");
		 response.sendRedirect("PRGServlet?status=success&url=user/hotel_guesthouse.jsp&alertMessage="+ AlertMessages.requestSent);
	    } 
		else response.sendRedirect("/user/hotel_guesthouse.jsp");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
