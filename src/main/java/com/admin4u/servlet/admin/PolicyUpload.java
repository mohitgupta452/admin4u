package com.admin4u.servlet.admin;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.admin4u.persistence.entity.PolicyDoc;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Roles;
import com.admin4u.views.UserSessionBean;
import com.turningcloud.utils.FilesUtils;
import com.turningcloud.utils.file.MultipartUtils;

/**
 * Servlet implementation class PolicyUpload
 */
@WebServlet("/PolicyUpload")
@MultipartConfig
public class PolicyUpload extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private LocationManger locationManger;

	@Inject
	private UserSessionBean userSessionBean;

	@Inject
	private RoleManager roleManager;

	public PolicyUpload() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action =  request.getParameter("action");
		
		if(action != null && "DELETEPOLICY".equals(action))
		{
			locationManger.removePolicy(Integer.parseInt(request.getParameter("policyId")));
			
			
		}else
		{
		PolicyDoc policyDoc = new PolicyDoc();

		String document = request.getParameter("document");

		policyDoc.setDocName(document);

		Part file = request.getPart("upload");

		String location = roleManager
				.getEmpByUpnAndRole(userSessionBean.getEmpData().getUserPrincipalName(), Roles.ADMIN).get(0)
				.getLocation();

		policyDoc
				.setPath(FilesUtils.saveFile(
						new File(FilesUtils.getPlatformBasedParentDir(),
								File.separator + location + File.separator + "PolicyDoc" + File.separator + document),
						file));

		policyDoc.setLocation(location);
		policyDoc.setIpAddress(request.getRemoteAddr());
		policyDoc.setUpdatedBy(userSessionBean.getEmpData().getUserPrincipalName());
		policyDoc.setUpdationTime(new Date());
		policyDoc.setActive(true);
		locationManger.uploadPolicyDoc(policyDoc);
		response.sendRedirect("user/admin_panel.jsp");
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
