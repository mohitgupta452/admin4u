package com.admin4u.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.entity.NotificationMaster;
import com.admin4u.service.bussiness.contract.NotificationManager;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.service.ApplicationMetaData.Status;
import com.admin4u.views.NotificationBean;
import com.admin4u.views.UserSessionBean;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class NotificationServlet
 */
@WebServlet("/NotificationServlet")
public class NotificationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Inject
	NotificationManager notificationManager;

	@Inject
	UserSessionBean userSession;

	@Inject
	NotificationBean notificationBean;

	@Inject
	CrudService crudService;

	Logger log = LoggerFactory.getLogger(getClass());

	public NotificationServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if ("MARKASREAD".equalsIgnoreCase(request.getParameter("action")) && request.getParameter("ids") != null
				&& !request.getParameter("ids").isEmpty()) {
			List<Integer> ids = new ArrayList<>();
			List<String> idStr = Arrays.asList(request.getParameter("ids").split(","));
			for (String id : idStr)
				ids.add(Integer.parseInt(id));
			String updatedIds = notificationManager.markAsRead(ids);

			response.getWriter().write(updatedIds);

		} else {

			response.setContentType("text/event-stream");

			response.setCharacterEncoding("UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Connection", "keep-alive");

			List<NotificationMaster> notificationList = notificationManager
					.getNotificationByUPNStatus(userSession.getEmpData().getUserPrincipalName(), Status.PENDING);
			PrintWriter writer = response.getWriter();
			writer.write("data: {\n");

			notificationList.forEach(notification -> log.debug("notification {}", notification.getMessage()));
			JsonArray noficationArray = new JsonArray();

			if (notificationList != null) {
				for (NotificationMaster notification : notificationList) {

					JsonObject notifyJson = new JsonObject();

					notifyJson.addProperty("id", notification.getNotificationId());
					notifyJson.addProperty("msg", notification.getMessage());
					notifyJson.addProperty("time", notificationBean.timeAgo(notification.getCreationTime()));
					String redirection = "";

					switch (notification.getRecieverRole() != null ? Roles.valueOf(notification.getRecieverRole())
							: Roles.REQUESTER) {

					case ADMIN:
						redirection = "user/admintable.jsp";
						break;
					case SPOC:
						redirection = "user/spoc.jsp";
						break;
					case REQUESTER:
						redirection = "user/user/user_task.jsp";
						break;

					case APPROVAL_AUTHORITY:
						redirection = "user/approvalauthority.jsp";
						break;
					default:
						break;
					}
					notifyJson.addProperty("redirection",
							redirection + "?search=" + String.valueOf(notification.getRequestMaster().getRequestId()));
					
					noficationArray.add(notifyJson);
				}
				writer.write("data:\"notificationArray\":" + noficationArray + "\n");
				writer.write("retry: 50000\n");
				writer.write("data: }\n\n");
				writer.flush();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
