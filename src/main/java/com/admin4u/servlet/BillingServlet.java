package com.admin4u.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.admin4u.persistence.entity.PaymentInfo;
import com.admin4u.service.bussiness.contract.VendorManager;
import com.java.AlertMessages;
import com.turningcloud.utils.FilesUtils;

@WebServlet("/BillingServlet")
@MultipartConfig
public class BillingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Inject
	private VendorManager vendorManager ;
    public BillingServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	      

	Map<String , String> paymentInfoMap = new HashMap<String , String>();
	/*paymentInfoMap.put("billDate",request.getParameter("billDate") );*/
/*	paymentInfoMap.put("period", request.getParameter("period"));*/
	paymentInfoMap.put("checkAmt", request.getParameter("checkAmt"));
	paymentInfoMap.put("checkNo", request.getParameter("checkNo"));
	paymentInfoMap.put("checkReceiveDate", request.getParameter("checkReceiveDate"));
	paymentInfoMap.put("invoiceAmt", request.getParameter("invoiceAmt"));
	paymentInfoMap.put("invoiceDate", request.getParameter("invoiceDate"));
	paymentInfoMap.put("invoiceNo", request.getParameter("invoiceNo"));
	paymentInfoMap.put("processAmt", request.getParameter("processAmt"));
	paymentInfoMap.put("company", request.getParameter("company"));
	paymentInfoMap.put("costCentre", request.getParameter("costCentre"));
	paymentInfoMap.put("invoiceReceiveDate", request.getParameter("invoiceReceiveDate"));
/*	paymentInfoMap.put("billLocation", request.getParameter("billLocation"));*/
	paymentInfoMap.put("expenses", request.getParameter("expenses"));
	paymentInfoMap.put("poNo", request.getParameter("poNo"));
	paymentInfoMap.put("billRemark", request.getParameter("billRemark"));
	/*paymentInfoMap.put("vendorName", request.getParameter("vendorName"));*/
	paymentInfoMap.put("id" , request.getParameter("billId"));

/*	Part billpart=request.getPart("billAttach");
	String billAttach=FilesUtils.saveFile(FilesUtils.getPlatformBasedParentDir(), billpart);
	paymentInfoMap.put("upload",billAttach);*/
	
	vendorManager.insertMISBillingData(paymentInfoMap);
	HttpSession session = request.getSession(false);
    session.setAttribute("isPRG", "true");
	response.sendRedirect("PRGServlet?status=success&url=user/vendor.jsp&alertMessage="+ AlertMessages.requestSent);
  

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
