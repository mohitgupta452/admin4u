package com.admin4u.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.LocServiceMap;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.util.enums.Services;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;

@WebServlet("/worklocation")
public class WorkLocationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private UserSessionBean userSession;
	
	@Inject
	private LocationManger locatonmanger;
	
    public WorkLocationServlet() {
        super();
    }
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action=request.getParameter("action");
		switch(action){
		case "WORKLOCATION":{
			String location=request.getParameter("location");
			List<String> ls=locatonmanger.getWorkLocation(location);
			String json = new Gson().toJson(ls);
			response.getWriter().write(json);
		}
		break;
        case "DELETEWORKLOCATION":{
        	String location=request.getParameter("location");
        	log.debug(location);
        	List<String> ls=locatonmanger.getWorkLocation(location);
        	log.debug("worklocation"+ls.toString());
        	String[] st=request.getParameterValues("worklocation");
        	List<String> ls2=new ArrayList<String>();
        	for(String s:st){
        		log.debug(s);
        		ls2.add(s);
        		
        	}
        	log.debug(ls2.toString());
        	Set<String> set1 = new HashSet<String>();
            set1.addAll(ls);

            Set<String> set2 = new HashSet<String>();
            set2.addAll(ls2);

            set1.removeAll(set2);
            log.debug("delete"+set1.toString());
            for (String diffElement : set1) {
              int a= locatonmanger.getWorkLocationID(diffElement);
              locatonmanger.delete(a); 
              
            }
            List<String> lsn=locatonmanger.getWorkLocation(location);
            String json = new Gson().toJson(lsn);
            log.debug(json);
            log.debug(location);
            log.debug(lsn.toString());
            response.getWriter().write(json);
		}
		break;
        case "CREATEWORKLOCATION":{
        	String locationName=request.getParameter("locationName");
            log.debug("location............" +locationName);
            String worklocationName=request.getParameter("worklocationName");
            log.debug("work............."+worklocationName);
            locatonmanger.insertData(locationName,worklocationName);
         
		}
		break;
        case "EDITLOCATION":{
        	 String worklocationName=request.getParameter("worklocationName");
        	   String newWorklocationName=request.getParameter("newworklocationName");
        	   log.debug(worklocationName);
        	   log.debug(newWorklocationName);
        	   int WID=locatonmanger.getWorkLocationID(worklocationName);
        	   locatonmanger.updateLocation(newWorklocationName,WID);
        	   List<String> ls=locatonmanger.getAllWorkLocationName();
        	   String json = new Gson().toJson(ls);
        	   log.debug(json);
        	   response.getWriter().write(json);
		}
		break;
		default:
	      break;
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}


