package com.admin4u.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.dto.SpaceManagementManager;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.LocServiceMap;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.persistence.entity.SMSeat;
import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.ADManager;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.util.enums.SMSpaceStationType;
import com.admin4u.util.enums.Services;
import com.admin4u.views.UserSessionBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.BuilderBasedDeserializer;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class AdminServicePanelServlet
 */
@WebServlet("/UtilityServlet")
public class UtilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	UserSessionBean userSession;
	@Inject
	LocationManger locatonmanger;
	@Inject
	IDMService idmService;
	@Inject
	private SpaceManagementManager smManager;
	
	@Inject
	private ADStore  adStore;

    public UtilityServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String action=request.getParameter("action");
		if("getEmpName".equalsIgnoreCase(action)){
			String employeeId=request.getParameter("employeeId");
			String employeeName=idmService.getEmployeeNameByEmployeeId(employeeId);
			
			Map<String,String> responseMap;
			
			responseMap=new HashMap<String,String>();
			responseMap.put("employeeName", employeeName);
			Gson gson = new Gson(); 
			String json = gson.toJson(responseMap);
			response.setContentType("application/json");
			response.getWriter().write(json);
			log.debug(json);	
		}
		else if("getEmpDetail".equalsIgnoreCase(action)){
			String employeeId=request.getParameter("employeeId");
			String employeeUPN=idmService.getEmployeeUPNByEmployeeId(employeeId);
			EmployeeData employeeData=idmService.getEmpByUpn(employeeUPN);
			String employeeSeatInfo=smManager.getEmployeeSeatInfo(employeeUPN);
			String reportingManager=adStore.getManager(employeeUPN).getEmpName();
			employeeData.setEmployeeSeat(employeeSeatInfo);
			employeeData.setManagerDN(reportingManager);
			ObjectMapper objectMapper=new ObjectMapper();
			String json=objectMapper.writeValueAsString(employeeData);
			log.debug("json string of employeeData {}",json);
			response.setContentType("application/json");
			response.getWriter().write(json);
			log.debug(json);	
		}
		else if("EMPLOYEEDATA".equalsIgnoreCase(action)){
			String employeeId=request.getParameter("employeeId");
			EmployeeData edata=idmService.getEmployeeDataByEmployeeId(employeeId);
			Map<String,String> responseMap=new HashMap<String,String>();

			if(edata!=null)
			{
						
			responseMap.put("employeeName", edata.getDisplayName());
			responseMap.put("department", edata.getDepartment());

			
			}
			else
				{
				responseMap.put("employeeName", "Invalid Id");
				
				responseMap.put("department", "not Defined");
				}
			Gson gson = new Gson(); 
			String json = gson.toJson(responseMap);
			response.setContentType("application/json");
			response.getWriter().write(json);
			log.debug(json);	
		}

		
		
		
		
		
			}

			protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				doGet(request, response);
			}

		}
