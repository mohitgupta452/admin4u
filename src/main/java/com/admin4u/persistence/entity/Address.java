package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(name="address")
@NamedQuery(name="Address.findAll", query="SELECT a FROM Address a")
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name ="`area`")
	private String area;
	
	@Column(name ="`city`")
	private String city;

	@Column(name ="`country`")
	private String country;

	@Column(name ="`ipAddress`")
	private String ipAddress;
 
	@Column(name ="`locality`")
	private String locality;
    
	@Column(name ="`pin`")
	private String pin;
  
	@Column(name ="`state`")
	private String state;

	@Column(name ="`street`")
	private String street;

	@Column(name ="`type`")
	private String type;

	@Column(name ="`updatedBy`")
	private String updatedBy;

	@Column(name ="`updationTime`")
	private Timestamp updationTime;

	//bi-directional many-to-one association to VendorInfo
	@OneToMany(mappedBy="address")
	private List<VendorInfo> vendorInfos;

	public Address() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getLocality() {
		return this.locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getPin() {
		return this.pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public List<VendorInfo> getVendorInfos() {
		return this.vendorInfos;
	}

	public void setVendorInfos(List<VendorInfo> vendorInfos) {
		this.vendorInfos = vendorInfos;
	}

	public VendorInfo addVendorInfo(VendorInfo vendorInfo) {
		getVendorInfos().add(vendorInfo);
		vendorInfo.setAddress(this);

		return vendorInfo;
	}

	public VendorInfo removeVendorInfo(VendorInfo vendorInfo) {
		getVendorInfos().remove(vendorInfo);
		vendorInfo.setAddress(null);

		return vendorInfo;
	}

}