package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the issue_limitation database table.
 * 
 */
@Entity
@Table(name="issue_limitation")
@NamedQueries({
@NamedQuery(name="IssueLimitation.findAll", query="SELECT i FROM IssueLimitation i"),
@NamedQuery(name="IssueLimitation.findByProductId",query="SELECT i from IssueLimitation i where i.productDetail.id=:productId")
})
public class IssueLimitation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String designation;

	@Column(name="lower_limit")
	private int lowerLimit;

	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="productId")
	private ProductDetail productDetail;

	@Column(name="upper_limit")
	private int upperLimit;

	public IssueLimitation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public int getLowerLimit() {
		return this.lowerLimit;
	}

	public void setLowerLimit(int lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public ProductDetail getProductDetail()
	{
		return productDetail;
	}

	public void setProductDetail(ProductDetail productDetail)
	{
		this.productDetail = productDetail;
	}

	public int getUpperLimit() {
		return this.upperLimit;
	}

	public void setUpperLimit(int upperLimit) {
		this.upperLimit = upperLimit;
	}

}