package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="pr_details")
@NamedQuery(name="PrDetail.findAll", query="SELECT p FROM PrDetail p")
public class PrDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String amount;

	private Timestamp creationDate;

	private String number;

	
	@Column(name="released_from_dept_GM")
	private  Timestamp releasedFromDeptGM;
	
	@Column(name="released_from_BHR")
	private Timestamp releasedFromBHR;
	
	

	public PrDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Timestamp getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Timestamp getReleasedFromDeptGM() {
		return releasedFromDeptGM;
	}

	public void setReleasedFromDeptGM(Timestamp releasedFromDeptGM) {
		this.releasedFromDeptGM = releasedFromDeptGM;
	}

	public Timestamp getReleasedFromBHR() {
		return releasedFromBHR;
	}

	public void setReleasedFromBHR(Timestamp releasedFromBHR) {
		this.releasedFromBHR = releasedFromBHR;
	}

	
	

}