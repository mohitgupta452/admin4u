package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.google.gson.JsonObject;

/**
 * Entity implementation class for Entity: RequestData
 *
 */
@Entity(name="requestdata")
@NamedQueries
({ 
//s@NamedQuery(name = "RequestData.findRequestData" , query = "SELECT r.requestData FROM requestdata r where r.requestDataId =:reqID"),

})

public class RequestData implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "requestDataID")
	private int requestDataId;
	
	//@Column(name="requestID")
	//private int requestID;
	
	@Column(name="requestData")
	private JsonObject requestData;
	
    @Temporal(TemporalType.TIMESTAMP)
	@Column(name="requestDate")
	private Date requestDate;
	
	  @Column(name="isActive")
	private String active;

	public int getRequestDataId() {
		return requestDataId;
	}

	public void setRequestDataId(int requestDataId) {
		this.requestDataId = requestDataId;
	}


	public JsonObject getRequestData() {
		return requestData;
	}

	public void setRequestData(JsonObject requestData) {
		this.requestData = requestData;
	}

	public RequestData() {
		super();
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}
}
