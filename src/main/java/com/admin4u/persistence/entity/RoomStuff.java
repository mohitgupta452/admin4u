package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: RoomStuff
 *
 */
@Entity(name="roomstuff")

@NamedQueries({
	@NamedQuery(name="RoomStuff.findAll",query="SELECT r from roomstuff r "),
	@NamedQuery(name="RoomStuff.findByRoom",query="SELECT r from roomstuff r where r.room=:room "),
	@NamedQuery(name="RoomStuff.findByRoomAndStuffName",query="SELECT r from roomstuff r where r.room=:room and r.stuffName=:stuffName"),
	@NamedQuery(name="RoomStuff.findByRoomAndStuffId",query="SELECT r from roomstuff r where r.room=:room and r.roomStuffId=:roomStuffId"),

	

	
})
public class RoomStuff implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	String stuffName;
	
	private int roomStuffId;
	
	@ManyToOne
	@JoinColumn(name="roomId")
	SMRoom room;

	public RoomStuff() {
		super();
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getStuffName()
	{
		return stuffName;
	}

	public void setStuffName(String suffName)
	{
		this.stuffName = suffName;
	}

	public SMRoom getRoom()
	{
		return room;
	}

	public void setRoom(SMRoom room)
	{
		this.room = room;
	}

	public int getRoomStuffId()
	{
		return roomStuffId;
	}

	public void setRoomStuffId(int roomStuffId)
	{
		this.roomStuffId = roomStuffId;
	}
   
}
