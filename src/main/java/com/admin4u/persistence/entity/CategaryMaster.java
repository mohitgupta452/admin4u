package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the categary_master database table.
 * 
 */
@Entity(name="categary_master")
@NamedQueries({
	@NamedQuery(name="CategaryMaster.findAll",query="SELECT c FROM categary_master c")
})
public class CategaryMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String categary;

	public CategaryMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategary() {
		return this.categary;
	}

	public void setCategary(String categary) {
		this.categary = categary;
	}

}