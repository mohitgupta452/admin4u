package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the stock_info database table.
 * 
 */
@Entity
@Table(name="stock_info")
@NamedQueries({
@NamedQuery(name="StockInfo.findAll", query="SELECT s FROM StockInfo s"),
@NamedQuery(name="StockInfo.findByProductId",query="SELECT s FROM StockInfo s WHERE s.productDetail.id=:productId")
})
public class StockInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="available_stock")
	private int availableStock;

	@Column(name="esitimated_cost")
	private String esitimatedCost;

	@OneToOne(cascade=CascadeType.ALL,targetEntity=ProductDetail.class)
	@JoinColumn(name="productId")
	private ProductDetail productDetail;

	@Column(name="reorder_quantity")
	private int reorderQuantity;

	@Column(name="reserved_quantity")
	private int reservedQuantity;

	@Column(name="total_stock")
	private int totalStock;

	private String updatedBy;

	private Date updationTime;

	public StockInfo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAvailableStock() {
		return this.availableStock;
	}

	public void setAvailableStock(int availableStock) {
		this.availableStock = availableStock;
	}

	public String getEsitimatedCost() {
		return this.esitimatedCost;
	}

	public void setEsitimatedCost(String esitimatedCost) {
		this.esitimatedCost = esitimatedCost;
	}

	public ProductDetail getProductDetail()
	{
		return productDetail;
	}

	public void setProductDetail(ProductDetail productDetail)
	{
		this.productDetail = productDetail;
	}

	public int getReorderQuantity() {
		return this.reorderQuantity;
	}

	public void setReorderQuantity(int reorderQuantity) {
		this.reorderQuantity = reorderQuantity;
	}

	public int getReservedQuantity() {
		return this.reservedQuantity;
	}

	public void setReservedQuantity(int reservedQuantity) {
		this.reservedQuantity = reservedQuantity;
	}

	public int getTotalStock() {
		return this.totalStock;
	}

	public void setTotalStock(int totalStock) {
		this.totalStock = totalStock;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

}