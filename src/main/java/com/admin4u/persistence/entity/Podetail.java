package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="podetails")
@NamedQuery(name="Podetail.findAll", query="SELECT p FROM Podetail p")
public class Podetail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	
	
	@Column(name="`amount`")
	private String amount;

	@Column(name="`end_date`")
	private Timestamp endDate;

	@Column(name="`ponumber`")
	private String ponumber;

	@Column(name="`released_date`")
	private Timestamp releasedDate;
	
	@Column(name="`released_from_comm`")
	private Timestamp releasedFromComm;
	
	@Column(name="`released_from_finance`")
	private Timestamp releasedFromFinance;
	
	@Column(name="`rel_status`")
	private String relStatus;
	
	@Column(name="`created_by`")
	private Timestamp createdBy;
	
	@Column(name="`from`")
	private Timestamp poFrom;
	
	@Column(name="`to`")
	private Timestamp to;

	private String upload;
	
	public Podetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getPonumber() {
		return this.ponumber;
	}

	public void setPonumber(String ponumber) {
		this.ponumber = ponumber;
	}

	public Timestamp getReleasedDate() {
		return this.releasedDate;
	}

	public void setReleasedDate(Timestamp releasedDate) {
		this.releasedDate = releasedDate;
	}


	public String getRelStatus() {
		return relStatus;
	}

	public void setRelStatus(String relStatus) {
		this.relStatus = relStatus;
	}

	

	public String getUpload() {
		return upload;
	}

	public void setUpload(String upload) {
		this.upload = upload;
	}

	public Timestamp getReleasedFromComm() {
		return releasedFromComm;
	}

	public void setReleasedFromComm(Timestamp releasedFromComm) {
		this.releasedFromComm = releasedFromComm;
	}

	public Timestamp getReleasedFromFinance() {
		return releasedFromFinance;
	}

	public void setReleasedFromFinance(Timestamp releasedFromFinance) {
		this.releasedFromFinance = releasedFromFinance;
	}

	public Timestamp getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Timestamp createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getPoFrom() {
		return poFrom;
	}

	public void setPoFrom(Timestamp poFrom) {
		this.poFrom = poFrom;
	}

	public Timestamp getTo() {
		return to;
	}

	public void setTo(Timestamp to) {
		this.to = to;
	}

	
	
}