package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;


/**
 * The persistent class for the tat_service_map database table.
 * 
 */
@Entity
@Table(name="tat_service_map")
@NamedQuery(name="TatServiceMaster.findAll", query="SELECT t FROM TatServiceMaster t")
public class TatServiceMaster implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String comments;

	private String priority;

	private String service;
	
	private String natureOfWork;

	private String sla;

	private Date tatLevel1;
	
	private Date tatLevel2;
	
	private Date tatLevel3;
	
	private Date tatLevel4;
	
	private Long tatLevel5;
	
	private int finalEsclanation;
	
	private String updatedBy;

	private String updationIp;

	private Date updationTime;
	
	private String type;

	public TatServiceMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPriority() {
		return this.priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getService() {
		return this.service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getSla() {
		return this.sla;
	}

	public void setSla(String sla) {
		this.sla = sla;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdationIp() {
		return this.updationIp;
	}

	public void setUpdationIp(String updationIp) {
		this.updationIp = updationIp;
	}

	public Date getTatLevel1() {
		return tatLevel1;
	}

	public void setTatLevel1(Date tatLevel1) {
		this.tatLevel1 = tatLevel1;
	}

	public Date getTatLevel2() {
		return tatLevel2;
	}

	public void setTatLevel2(Date tatLevel2) {
		this.tatLevel2 = tatLevel2;
	}

	public Date getTatLevel3() {
		return tatLevel3;
	}

	public void setTatLevel3(Date tatLevel3) {
		this.tatLevel3 = tatLevel3;
	}

	public Date getTatLevel4() {
		return tatLevel4;
	}

	public void setTatLevel4(Date tatLevel4) {
		this.tatLevel4 = tatLevel4;
	}
	
	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getFinalEsclanation() {
		return finalEsclanation;
	}

	public void setFinalEsclanation(int finalEsclanation) {
		this.finalEsclanation = finalEsclanation;
	}

	public String getNatureOfWork() {
		return natureOfWork;
	}

	public void setNatureOfWork(String natureOfWork) {
		this.natureOfWork = natureOfWork;
	}

	public Long getTatLevel5() {
		return tatLevel5;
	}

	public void setTatLevel5(Long tatLevel5) {
		this.tatLevel5 = tatLevel5;
	}
	
}