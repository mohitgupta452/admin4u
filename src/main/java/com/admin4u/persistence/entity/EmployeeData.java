package com.admin4u.persistence.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the employeedata database table.
 *
 */
@Entity
@Table(name = "employeedata")
@NamedQueries
({ 
@NamedQuery(name = "EmployeeData.findAll",query="select e From EmployeeData e"),
@NamedQuery(name = "EmployeeData.findByEmpByUpn", query = "SELECT e FROM EmployeeData e where e.userPrincipalName=:upn"),
@NamedQuery(name = "EmployeeData.findByEmpByMail", query = "SELECT e FROM EmployeeData e where e.mail=:mail"),
@NamedQuery(name = "EmployeeData.findDistinctDepartment", query = "SELECT DISTINCT (e.department)  FROM EmployeeData e WHERE e.department is not null"),
@NamedQuery(name = "EmployeeData.findDepartmentByUpn" , query = "SELECT e.department FROM EmployeeData e where e.userPrincipalName =:upn"),
@NamedQuery(name="EmployeeData.findByEmployeeId",query="SELECT e.userPrincipalName from EmployeeData e where e.employeeID=:employeeId"),
@NamedQuery(name = "EmployeeData.findCompanyByUpn" , query = "SELECT e.company FROM EmployeeData e where e.userPrincipalName =:upn"),
@NamedQuery(name = "EmployeeData.findDistinctCompany" , query = "SELECT DISTINCT e.company , e.l FROM EmployeeData e WHERE e.company is not null"),
@NamedQuery(name = "EmployeeData.findLocationByUpn" , query = "SELECT e.l FROM EmployeeData e where e.userPrincipalName =:upn"),
@NamedQuery(name = "EmployeeData.findDistinctLocation" , query = "SELECT DISTINCT (e.l) FROM EmployeeData e WHERE e.l is not null"),
@NamedQuery(name="EmployeeData.findNameByEmployeeId",query="SELECT e.displayName from EmployeeData e where e.employeeID=:employeeId"),
@NamedQuery(name="EmployeeData.findDesignation",query="SELECT DISTINCT (e.designation) from EmployeeData e WHERE e.designation is not null"),
@NamedQuery(name="EmployeeData.findEmpByEmployeeId",query="SELECT e from EmployeeData e where e.employeeID=:employeeId"),

})

public class EmployeeData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int empDataID;

	@Column(length = 60)
	private String company;

	@Column(length = 55)
	private String department;

	@Column(length = 100)
	private String designation;

	@Column(length = 100)
	private String displayName;

	@Column(length = 150)
	private String distinguishedName;

	@Column(length = 45)
	private String employeeID;
	

	@Column(length = 10)
	private String initials;

	@Column(length = 45)
	private String l;

	@Column(length = 100)
	private String mail;

	@Column(length = 150)
	private String managerDN;

	@Column(length = 45)
	private String mobile;

	@Column(length = 45)
	private String st;

	@Column(length = 45)
	private String telephoneNumber;

	@Column(length = 100)
	private String userPrincipalName;
	
	@Transient
	private String employeeSeat;

	public EmployeeData() {
	}

	public int getEmpDataID() {
		return this.empDataID;
	}

	public void setEmpDataID(int empDataID) {
		this.empDataID = empDataID;
	}


	
	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDistinguishedName() {
		return this.distinguishedName;
	}

	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}

	public String getEmployeeID() {
		return this.employeeID;
	}

	public void setEmployeeID(String employeeID) {
		this.employeeID = employeeID;
	}

	

	public String getInitials() {
		return this.initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public String getL() {
		return this.l;
	}

	public void setL(String l) {
		this.l = l;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getManagerDN() {
		return this.managerDN;
	}

	public void setManagerDN(String managerDN) {
		this.managerDN = managerDN;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSt() {
		return this.st;
	}

	public void setSt(String st) {
		this.st = st;
	}

	public String getTelephoneNumber() {
		return this.telephoneNumber;
	}

	public String getCompany() {
		return company;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getUserPrincipalName() {
		return this.userPrincipalName;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setUserPrincipalName(String userPrincipalName) {
		this.userPrincipalName = userPrincipalName.toLowerCase();
	}

	public String getEmployeeSeat()
	{
		return employeeSeat;
	}

	public void setEmployeeSeat(String employeeSeat)
	{
		this.employeeSeat = employeeSeat;
	}


	
   
}
