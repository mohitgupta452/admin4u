package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: MeetingRoom
 *
 */
@Entity(name="smroom")
@NamedQueries({
	@NamedQuery(name="SMRoom.findAll",query="SELECT m from smroom m "),
	@NamedQuery(name="SMRoom.findByFloorAndType",query="SELECT r from smroom r where r.floor=:floor and r.roomType=:roomType"),
	@NamedQuery(name="SMRoom.findByLBCodeRoomNumber",query="SELECT m from smroom m where m.locationBuildingCode=:locationBuildingCode and m.roomNumber=:roomNumber"),
	@NamedQuery(name="SMRoom.findAllByType",query="SELECT m from smroom m where m.roomType=:roomType and m.locationBuildingCode=:locationBuildingCode"),
	@NamedQuery(name="SMRoom.findByFloorAndRoomNumber",query="SELECT r from smroom r where r.floor=:floor and r.roomNumber=:roomNumber"),
	@NamedQuery(name="SMRoom.findByFloorTypeAndAllocationStatus",query="SELECT r from smroom r where r.floor=:floor and r.roomType=:roomType and r.allocationStatus=:allocationStatus"),
	@NamedQuery(name="SMRoom.findByEmployeeUPN",query="SELECT r from smroom r where r.employeeUPN=:employeeUPN"),
	@NamedQuery(name="SMRoom.findByFloorAndRoomCode",query="SELECT r from smroom r where r.floor=:floor and r.roomCode=:roomCode"),
	@NamedQuery(name="SMRoom.findByBuilding",query="SELECT r from smroom r where r.building=:building"),
	@NamedQuery(name="SMRoom.findByBuildingAndOccupacyType",query="SELECT r from smroom r where r.building=:building"),

	




	
	
})
public class SMRoom implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String locationBuildingCode;
	
	private String roomType;
	
	private String roomNumber;
	
	@Column(name="allocationStatus",columnDefinition="default 'UNALLOCATED'")
	private String allocationStatus;
	
	private String employeeUPN;
	
	private int totalNoOfSeats;
	
	private String roomCode;
	
	@ManyToOne
	@JoinColumn(name="floorId")
	private SMFloor floor;
	
	@OneToMany(mappedBy="meetingRoom",fetch=FetchType.LAZY)
	private List<MeetingRoomBookingDetail> meetingRoomBookingDetail;
	
	@OneToMany(mappedBy="room",fetch=FetchType.EAGER)
	private List<RoomStuff> roomStuffList;
	
	@ManyToOne
	@JoinColumn(name="buildingId")
	private SMBuilding building;//using for guest house 
	
	private String roomTypeByCapacity;

	public SMRoom() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public String getLocationBuildingCode() {
		return locationBuildingCode;
	}

	public void setLocationBuildingCode(String locationBuildingCode) {
		this.locationBuildingCode = locationBuildingCode;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public List<MeetingRoomBookingDetail> getMeetingRoomBookingDetail() {
		return meetingRoomBookingDetail;
	}

	public void setMeetingRoomBookingDetail(List<MeetingRoomBookingDetail> meetingRoomBookingDetail) {
		this.meetingRoomBookingDetail = meetingRoomBookingDetail;
	}

	public List<RoomStuff> getRoomStuffList()
	{
		return roomStuffList;
	}

	public SMBuilding getBuilding()
	{
		return building;
	}

	public void setBuilding(SMBuilding building)
	{
		this.building = building;
	}

	public void setRoomStuffList(List<RoomStuff> roomStuffList)
	{
		this.roomStuffList = roomStuffList;
	}

	public String getAllocationStatus() {
		return allocationStatus;
	}

	public void setAllocationStatus(String allocationStatus) {
		this.allocationStatus = allocationStatus;
	}

	public SMFloor getFloor() {
		return floor;
	}

	public void setFloor(SMFloor floor) {
		this.floor = floor;
	}

	public String getEmployeeUPN() {
		return employeeUPN;
	}

	public void setEmployeeUPN(String employeeUPN) {
		this.employeeUPN = employeeUPN;
	}

	public int getTotalNoOfSeats()
	{
		return totalNoOfSeats;
	}

	public void setTotalNoOfSeats(int totalNoOfSeats)
	{
		this.totalNoOfSeats = totalNoOfSeats;
	}

	public String getRoomCode()
	{
		return roomCode;
	}

	public void setRoomCode(String roomCode)
	{
		this.roomCode = roomCode;
	}

	public String getRoomTypeByCapacity()
	{
		return roomTypeByCapacity;
	}

	public void setRoomTypeByCapacity(String roomTypeByCapacity)
	{
		this.roomTypeByCapacity = roomTypeByCapacity;
	}
	
   
}
