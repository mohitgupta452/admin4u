package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the payment_info database table.
 * 
 */
@Entity
@Table(name="payment_info")
@NamedQuery(name="PaymentInfo.findAll", query="SELECT p FROM PaymentInfo p")
public class PaymentInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String attachment;

	@Column(name="check_amt")
	private String checkAmt;

	@Column(name="check_no")
	private String checkNo;

	@Column(name="check_receive_date")
	private Timestamp checkReceiveDate;

	private String discount;

	@Column(name="invoice_amt")
	private String invoiceAmt;

	@Column(name="invoice_date")
	private Timestamp invoiceDate;

	@Column(name="invoice_number")
	private String invoiceNumber;

	@Column(name="invoice_process_amt")
	private String invoiceProcessAmt;

	@Column(name="invoice_receive_date")
	private Timestamp invoiceReceiveDate;

	private String ipaddress;

	private String location;

	private int master_payment_Id;

	@Column(name="payment_method")
	private String paymentMethod;

	@Column(name="payment_time")
	private Timestamp paymentTime;

	@Column(name="po_no")
	private String poNo;

	@Column(name="remaining_amt")
	private String remainingAmt;

	private String remark;

	@Column(name="total_amt")
	private String totalAmt;

	@Column(name="transaction_id")
	private String transactionId;

	@Column(name="updated_by")
	private String updatedBy;

	@Column(name="updation_time")
	private Timestamp updationTime;

	private String vendor_Id;

	public PaymentInfo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAttachment() {
		return this.attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getCheckAmt() {
		return this.checkAmt;
	}

	public void setCheckAmt(String checkAmt) {
		this.checkAmt = checkAmt;
	}

	public String getCheckNo() {
		return this.checkNo;
	}

	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}

	public Timestamp getCheckReceiveDate() {
		return this.checkReceiveDate;
	}

	public void setCheckReceiveDate(Timestamp checkReceiveDate) {
		this.checkReceiveDate = checkReceiveDate;
	}

	public String getDiscount() {
		return this.discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getInvoiceAmt() {
		return this.invoiceAmt;
	}

	public void setInvoiceAmt(String invoiceAmt) {
		this.invoiceAmt = invoiceAmt;
	}

	public Timestamp getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Timestamp invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getInvoiceProcessAmt() {
		return this.invoiceProcessAmt;
	}

	public void setInvoiceProcessAmt(String invoiceProcessAmt) {
		this.invoiceProcessAmt = invoiceProcessAmt;
	}

	public Timestamp getInvoiceReceiveDate() {
		return this.invoiceReceiveDate;
	}

	public void setInvoiceReceiveDate(Timestamp invoiceReceiveDate) {
		this.invoiceReceiveDate = invoiceReceiveDate;
	}

	public String getIpaddress() {
		return this.ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getMaster_payment_Id() {
		return this.master_payment_Id;
	}

	public void setMaster_payment_Id(int master_payment_Id) {
		this.master_payment_Id = master_payment_Id;
	}

	public String getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Timestamp getPaymentTime() {
		return this.paymentTime;
	}

	public void setPaymentTime(Timestamp paymentTime) {
		this.paymentTime = paymentTime;
	}

	public String getPoNo() {
		return this.poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getRemainingAmt() {
		return this.remainingAmt;
	}

	public void setRemainingAmt(String remainingAmt) {
		this.remainingAmt = remainingAmt;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTotalAmt() {
		return this.totalAmt;
	}

	public void setTotalAmt(String totalAmt) {
		this.totalAmt = totalAmt;
	}

	public String getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getVendor_Id() {
		return this.vendor_Id;
	}

	public void setVendor_Id(String vendor_Id) {
		this.vendor_Id = vendor_Id;
	}

}