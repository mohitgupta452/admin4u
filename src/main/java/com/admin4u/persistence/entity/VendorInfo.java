package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="vendor_info")
@NamedQueries
({ 
@NamedQuery(name="VendorInfo.findAll", query="SELECT v FROM VendorInfo v"),
@NamedQuery(name="VendorInfo.findAllVendorName", query="SELECT v.name FROM VendorInfo v")
})
public class VendorInfo implements Serializable {
	private static final long serialVersionUID = 1L;

 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="`address_id`")
	private Address addressId;

	@Column(name="`bussiness_description`")
	private String bussinessDescription;

	@Column(name="`company`")
	private String company;

	@Column(name="`contactId`")
	private int contactId;

	@Column(name="`fax`")
	private String fax;

	@Column(name="`name`")
	private String name;

	@Column(name="`phone`")
	private String phone;

	@Column(name="`tin`")
	private String tin;
	
	@Column(name="`service_tax_no`")
	private int servicetax;
	
	@Column(name="`ein_ssn`")
	private int einssn;
	
	@Column(name="`state_of_incorporation`")
	private String stateOfIncorporation;
	
	@Column(name="`year_of_incorporation`")
	private String  yearOfIncorporation;
	
	@Column(name="`preff_delivery_method`")
	private String preffDeliveryMethod;
	
	@Column(name="`cancel_check`")
	private String cancelCheck;
	
	@Column(name="`pan_card`")
	private String panCard;
	
	@Column(name="`decl_letter`")
	private String declLetter;
	
	@Column(name="`escal_attach`")
	private String escalAttach;

	//bi-directional many-to-one association to Address
	@ManyToOne
	@JoinColumn(name="`mailingaddressId`")
	private Address address;

	//bi-directional many-to-one association to ContactDetail
	@ManyToOne
	@JoinColumn(name="`contact_details_id`")
	private ContactDetail contactDetail;

	
	

	public VendorInfo() {
	}


	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



/*	public int getAddressId() {
		return this.addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}*/
	
	

	public Address getAddressId() {
		return addressId;
	}



	public void setAddressId(Address addressId) {
		this.addressId = addressId;
	}	
	
	

	public String getBussinessDescription() {
		return this.bussinessDescription;
	}


	public void setBussinessDescription(String bussinessDescription) {
		this.bussinessDescription = bussinessDescription;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getContactId() {
		return this.contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getFax() {
		return this.fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTin() {
		return this.tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	
	public ContactDetail getContactDetail() {
		return this.contactDetail;
	}

	/*public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public int getMailingAddressId() {
		return mailingAddressId;
	}

	public void setMailingAddressId(int mailingAddressId) {
		this.mailingAddressId = mailingAddressId;
	}*/

	public void setContactDetail(ContactDetail contactDetail) {
		this.contactDetail = contactDetail;
	}

	public int getServicetax() {
		return servicetax;
	}

	public void setServicetax(int servicetax) {
		this.servicetax = servicetax;
	}

	public int getEinssn() {
		return einssn;
	}

	public void setEinssn(int einssn) {
		this.einssn = einssn;
	}

	public String getStateOfIncorporation() {
		return stateOfIncorporation;
	}

	public void setStateOfIncorporation(String stateOfIncorporation) {
		this.stateOfIncorporation = stateOfIncorporation;
	}

	public String getYearOfIncorporation() {
		return yearOfIncorporation;
	}

	public void setYearOfIncorporation(String yearOfIncorporation) {
		this.yearOfIncorporation = yearOfIncorporation;
	}

	public String getPreffDeliveryMethod() {
		return preffDeliveryMethod;
	}

	public void setPreffDeliveryMethod(String preffDeliveryMethod) {
		this.preffDeliveryMethod = preffDeliveryMethod;
	}

	public String getCancelCheck() {
		return cancelCheck;
	}

	public void setCancelCheck(String cancelCheck) {
		this.cancelCheck = cancelCheck;
	}

	public String getPanCard() {
		return panCard;
	}

	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}

	public String getDeclLetter() {
		return declLetter;
	}

	public void setDeclLetter(String declLetter) {
		this.declLetter = declLetter;
	}

	public String getEscalAttach() {
		return escalAttach;
	}

	public void setEscalAttach(String escalAttach) {
		this.escalAttach = escalAttach;
	}


	
	

}