package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: DesignationPriority
 *
 */
@Entity(name="designation_priority")

@NamedQueries({
@NamedQuery(name="DesignationPrioriy.findByDesignation", query="SELECT d FROM designation_priority d where d.designation=:designation"),



})


public class DesignationPriority implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String designation;
	
	private String priority;
	
	private boolean isAboveAed;
	
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updationTime;
	
	private String ipAddress;

	
	public DesignationPriority() {
		super();
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getDesignation()
	{
		return designation;
	}

	public void setDesignation(String designation)
	{
		this.designation = designation;
	}

	public String getPriority()
	{
		return priority;
	}

	public void setPriority(String priority)
	{
		this.priority = priority;
	}

	

	public String getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	public Date getUpdationTime()
	{
		return updationTime;
	}

	public void setUpdationTime(Date updationTime)
	{
		this.updationTime = updationTime;
	}

	public String getIpAddress()
	{
		return ipAddress;
	}

	public void setIpAddress(String ipaddress)
	{
		this.ipAddress = ipaddress;
	}

	public boolean isAboveAed()
	{
		return isAboveAed;
	}

	public void setAboveAed(boolean isAboveAed)
	{
		this.isAboveAed = isAboveAed;
	}
	
	
   
}
