package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the policy_doc database table.
 * 
 */
@Entity
@Table(name="policy_doc")

@NamedQueries({
	@NamedQuery(name="PolicyDoc.findAll", query="SELECT p FROM PolicyDoc p"),
	@NamedQuery(name="PolicyDoc.findByLocation",query="SELECT p FROM PolicyDoc p WHERE p.location=:location"),
	@NamedQuery(name="PolicyDoc.findByLocDoc",query="SELECT p FROM PolicyDoc p WHERE p.location=:location AND p.docName=:document")

})

public class PolicyDoc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private Boolean active;

	private String docName;

	private String ipAddress;

	private String location;

	private String path;

	private String updatedBy;

	private Date updationTime;

	public PolicyDoc() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDocName() {
		return this.docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}
	
}