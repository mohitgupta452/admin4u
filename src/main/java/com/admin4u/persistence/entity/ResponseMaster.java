package com.admin4u.persistence.entity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity(name = "responsedata")
@NamedQueries
({ 
@NamedQuery(name = "ResponseMaster.findResponseDataByReqID" , query = "SELECT r FROM responsedata r where r.requestMaster.requestId =:reqID"),
@NamedQuery(name = "ResponseMaster.findResponseMsgByReqID" , query = "SELECT r FROM responsedata r where r.requestMaster.requestId =:reqID"),

})
public class ResponseMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="responseID")
	private int responseID;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="serviceAvailTime")
	private Date serviceAvailTime;

	@Column(name="responseData")
	private String responseData;
	
	@Column(name="responseMsg")
	private String responseMsg;
	
	@Column(name="responseAttachment")
	private String responseAttachment;
	
	@JoinColumn(name="requestId")
    @OneToOne(targetEntity=RequestMaster.class)
	private RequestMaster requestMaster;

	public int getResponseID() {
		return responseID;
	}

	public void setResponseID(int responseID) {
		this.responseID = responseID;
	}

	public Date getServiceAvailTime() {
		return serviceAvailTime;
	}

	public void setServiceAvailTime(Date serviceAvailTime) {
		this.serviceAvailTime = serviceAvailTime;
	}

	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}

	public String getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}

	public String getResponseAttachment() {
		return responseAttachment;
	}

	public void setResponseAttachment(String responseAttachment) {
		this.responseAttachment = responseAttachment;
	}

	public RequestMaster getRequestMaster() {
		return requestMaster;
	}

	public void setRequestMaster(RequestMaster requestMaster) {
		this.requestMaster = requestMaster;
	}

}
