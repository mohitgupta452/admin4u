package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.google.gson.JsonObject;

/**
 * Entity implementation class for Entity: TatMaster
 *
 */
@Entity(name="tat_master")
@NamedQueries({
	@NamedQuery(name="TatMaster.findAll",query="select  t from tat_master t "),

	@NamedQuery(name="TatMaster.findByService",query="select  t from tat_master t where t.tatService=:tatServiceName"),
})

public class TatMaster implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	private	String tatService;
	
	private String sla;

	private JsonObject tatLevel;
	
	private String priority;
	
	private String comments;
	
	private String updatedBy;
	
	private String updationIp;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updationTime;

	

	public TatMaster() {
		super();
	}





	public int getId()
	{
		return id;
	}


	public JsonObject getTatLevel()
	{
		return tatLevel;
	}

	public void setTatLevel(JsonObject tatLevel)
	{
		this.tatLevel = tatLevel;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getTatService()
	{
		return tatService;
	}

	public void setTatService(String tatService)
	{
		this.tatService = tatService;
	}

	public String getSla()
	{
		return sla;
	}

	public void setSla(String sla)
	{
		this.sla = sla;
	}

	public String getPriority()
	{
		return priority;
	}

	public void setPriority(String priority)
	{
		this.priority = priority;
	}


	public String getComments()
	{
		return comments;
	}


	public void setComments(String comments)
	{
		this.comments = comments;
	}





	public String getUpdatedBy()
	{
		return updatedBy;
	}





	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}





	public String getUpdationIp()
	{
		return updationIp;
	}





	public void setUpdationIp(String updationIp)
	{
		this.updationIp = updationIp;
	}





	public Date getUpdationTime()
	{
		return updationTime;
	}





	public void setUpdationTime(Date updationTime)
	{
		this.updationTime = updationTime;
	}
   
}
