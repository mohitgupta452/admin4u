package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the paymentterms database table.
 * 
 */
@Entity
@Table(name="paymentterms")
@NamedQuery(name="Paymentterm.findAll", query="SELECT p FROM Paymentterm p")
public class Paymentterm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="`active`")
	private byte isActive;
	
	@Column(name="advance")
	private String advance;

	@Column(name="paymentTerms")
	private String paymentTerms;

	@Column(name="penaltyTerms")
	private String penaltyTerms;

	@Column(name="shippingMethod")
	private String shippingMethod;

	@Column(name="shippingTerms")
	private String shippingTerms;

	public Paymentterm() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdvance() {
		return this.advance;
	}

	public void setAdvance(String advance) {
		this.advance = advance;
	}

	public String getPaymentTerms() {
		return this.paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getPenaltyTerms() {
		return this.penaltyTerms;
	}

	public void setPenaltyTerms(String penaltyTerms) {
		this.penaltyTerms = penaltyTerms;
	}

	public String getShippingMethod() {
		return this.shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getShippingTerms() {
		return this.shippingTerms;
	}

	public void setShippingTerms(String shippingTerms) {
		this.shippingTerms = shippingTerms;
	}
	
	public byte getIsActive() {
		return isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

}