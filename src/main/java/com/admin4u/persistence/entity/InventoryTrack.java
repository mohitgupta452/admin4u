package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the inventory_track database table.
 * 
 */
@Entity
@Table(name="inventory_track")
@NamedQuery(name="InventoryTrack.findAll", query="SELECT i FROM InventoryTrack i")
public class InventoryTrack implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String ipAddress;

	private String issue_By;

	private Timestamp issue_Date;

	private String issue_To;

	@OneToOne(targetEntity=ProductDetail.class,cascade=CascadeType.ALL)
	@JoinColumn(name="productId")
	private ProductDetail productDetail;

	private String quantity;

	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	@JoinColumn(name="requestId")
	private RequestMaster requestMaster;

	private String status;

	private String type;

	private String updatedBy;

	private Timestamp updationTime;

	public InventoryTrack() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getIssue_By() {
		return this.issue_By;
	}

	public void setIssue_By(String issue_By) {
		this.issue_By = issue_By;
	}

	public Timestamp getIssue_Date() {
		return this.issue_Date;
	}

	public void setIssue_Date(Timestamp issue_Date) {
		this.issue_Date = issue_Date;
	}

	public String getIssue_To() {
		return this.issue_To;
	}

	public void setIssue_To(String issue_To) {
		this.issue_To = issue_To;
	}

	public ProductDetail getProductDetail()
	{
		return productDetail;
	}

	public void setProductDetail(ProductDetail productDetail)
	{
		this.productDetail = productDetail;
	}

	public String getQuantity() {
		return this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}


	public RequestMaster getRequestMaster()
	{
		return requestMaster;
	}

	public void setRequestMaster(RequestMaster requestMaster)
	{
		this.requestMaster = requestMaster;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

}