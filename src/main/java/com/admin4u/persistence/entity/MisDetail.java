package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the mis_details database table.
 * 
 */
@Entity
@Table(name="mis_details")
@NamedQuery(name="MisDetail.findAll", query="SELECT m FROM MisDetail m")
public class MisDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String attachment;

	private String companyName;

	private String costCenter;

	
	@Column(name="nature_of_expense")
	private String natureOfExpense;
    
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL )
	@JoinColumn(name="payment_info_id")
	private PaymentInfo paymentInfoId;

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL )
	@JoinColumn(name="podetails_id")
	private Podetail podetailsId;

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL )
	@JoinColumn(name="pr_details_id")
	private PrDetail prDetailsId;

	@Column(name="submission_to_acc")
	private Timestamp submissionToAcc;

	private String type;

	private int vendorId;

	public MisDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAttachment() {
		return this.attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCostCenter() {
		return this.costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	public String getNatureOfExpense() {
		return this.natureOfExpense;
	}

	public void setNatureOfExpense(String natureOfExpense) {
		this.natureOfExpense = natureOfExpense;
	}

	

	public PaymentInfo getPaymentInfoId() {
		return paymentInfoId;
	}

	public void setPaymentInfoId(PaymentInfo paymentInfoId) {
		this.paymentInfoId = paymentInfoId;
	}

	public Podetail getPodetailsId() {
		return podetailsId;
	}

	public void setPodetailsId(Podetail podetailsId) {
		this.podetailsId = podetailsId;
	}

	public PrDetail getPrDetailsId() {
		return prDetailsId;
	}

	public void setPrDetailsId(PrDetail prDetailsId) {
		this.prDetailsId = prDetailsId;
	}

	public Timestamp getSubmissionToAcc() {
		return this.submissionToAcc;
	}

	public void setSubmissionToAcc(Timestamp submissionToAcc) {
		this.submissionToAcc = submissionToAcc;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getVendorId() {
		return this.vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}

}