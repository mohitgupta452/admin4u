package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: EmployeeRoleServiceMaster
 *
 */
@Entity(name="emprole_service_map")

@NamedQueries({
	@NamedQuery(name="EmployeeRoleServiceMaster.findByRoleLocation", query="SELECT f FROM employeerole s JOIN s.employeeRoleServicemaster f  WHERE f.employeeRole.employeeRoleID= s.employeeRoleID AND f.service =:service AND s.location=:location AND s.role = :role"),
	@NamedQuery(name="EmployeeRoleServiceMaster.findByEmployeeRole", query="SELECT e FROM emprole_service_map e where e.employeeRole=:employeeRole")
})

public class EmployeeRoleServiceMaster implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int id;
	
	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="employeeRoleId")
	private EmployeeRole employeeRole;
	
	private String service;
	
	private Date creationTime;
	
	private boolean isActive; 
	
	private String roleName;
	
	public EmployeeRoleServiceMaster() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmployeeRole getEmployeeRole() {
		return employeeRole;
	}

	public void setEmployeeRole(EmployeeRole employeeRole) {
		this.employeeRole = employeeRole;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	
   
}
