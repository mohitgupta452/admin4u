package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the contract_details database table.
 * 
 */
@Entity
@Table(name="contract_details")
@NamedQuery(name="ContractDetail.findAll", query="SELECT c FROM ContractDetail c")
public class ContractDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String agreementType;

	private String anyOtherFixedCost;

	@Column(name="contract_detailscol")
	private String contractDetailscol;

	private String escalation;

	private String landlordName;

	private Timestamp leaseCommencementDate;

	private Timestamp leaseEndDate;

	private String lesseeName;

	private String lessorName;

	private Timestamp lockInPeriod;

	private String monthlyMaintenance;

	private String monthlyRent;

	private Timestamp nextEscalationDate;

	private Timestamp noticePeriod;

	private String parkingCharges;

	private Timestamp paymentDate;

	private String powerBackUpCharges;

	private String processToBeInitiated;

	private String securityDeposit;

	private String totalArea;

	private String totalRentPayable;
	
	private String upload;

	public ContractDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgreementType() {
		return this.agreementType;
	}

	public void setAgreementType(String agreementType) {
		this.agreementType = agreementType;
	}

	public String getAnyOtherFixedCost() {
		return this.anyOtherFixedCost;
	}

	public void setAnyOtherFixedCost(String anyOtherFixedCost) {
		this.anyOtherFixedCost = anyOtherFixedCost;
	}

	public String getContractDetailscol() {
		return this.contractDetailscol;
	}

	public void setContractDetailscol(String contractDetailscol) {
		this.contractDetailscol = contractDetailscol;
	}

	public String getEscalation() {
		return this.escalation;
	}

	public void setEscalation(String escalation) {
		this.escalation = escalation;
	}

	public String getLandlordName() {
		return this.landlordName;
	}

	public void setLandlordName(String landlordName) {
		this.landlordName = landlordName;
	}


	public String getLesseeName() {
		return this.lesseeName;
	}

	public void setLesseeName(String lesseeName) {
		this.lesseeName = lesseeName;
	}

	public String getLessorName() {
		return this.lessorName;
	}

	public void setLessorName(String lessorName) {
		this.lessorName = lessorName;
	}

	public String getMonthlyMaintenance() {
		return this.monthlyMaintenance;
	}

	public void setMonthlyMaintenance(String monthlyMaintenance) {
		this.monthlyMaintenance = monthlyMaintenance;
	}

	public String getMonthlyRent() {
		return this.monthlyRent;
	}

	public void setMonthlyRent(String monthlyRent) {
		this.monthlyRent = monthlyRent;
	}

	public Timestamp getNextEscalationDate() {
		return this.nextEscalationDate;
	}

	public void setNextEscalationDate(Timestamp nextEscalationDate) {
		this.nextEscalationDate = nextEscalationDate;
	}

	public String getParkingCharges() {
		return this.parkingCharges;
	}

	public void setParkingCharges(String parkingCharges) {
		this.parkingCharges = parkingCharges;
	}

	public Timestamp getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Timestamp paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPowerBackUpCharges() {
		return this.powerBackUpCharges;
	}

	public void setPowerBackUpCharges(String powerBackUpCharges) {
		this.powerBackUpCharges = powerBackUpCharges;
	}

	public String getProcessToBeInitiated() {
		return this.processToBeInitiated;
	}

	public void setProcessToBeInitiated(String processToBeInitiated) {
		this.processToBeInitiated = processToBeInitiated;
	}

	public String getSecurityDeposit() {
		return this.securityDeposit;
	}

	public void setSecurityDeposit(String securityDeposit) {
		this.securityDeposit = securityDeposit;
	}

	public String getTotalArea() {
		return this.totalArea;
	}

	public void setTotalArea(String totalArea) {
		this.totalArea = totalArea;
	}

	public String getTotalRentPayable() {
		return this.totalRentPayable;
	}

	public void setTotalRentPayable(String totalRentPayable) {
		this.totalRentPayable = totalRentPayable;
	}

	public String getUpload() {
		return upload;
	}

	public void setUpload(String upload) {
		this.upload = upload;
	}

	public Timestamp getLeaseCommencementDate() {
		return leaseCommencementDate;
	}

	public void setLeaseCommencementDate(Timestamp leaseCommencementDate) {
		this.leaseCommencementDate = leaseCommencementDate;
	}

	public Timestamp getLeaseEndDate() {
		return leaseEndDate;
	}

	public void setLeaseEndDate(Timestamp leaseEndDate) {
		this.leaseEndDate = leaseEndDate;
	}

	public Timestamp getLockInPeriod() {
		return lockInPeriod;
	}

	public void setLockInPeriod(Timestamp lockInPeriod) {
		this.lockInPeriod = lockInPeriod;
	}

	public Timestamp getNoticePeriod() {
		return noticePeriod;
	}

	public void setNoticePeriod(Timestamp noticePeriod) {
		this.noticePeriod = noticePeriod;
	}
	
	

	
}