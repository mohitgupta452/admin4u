package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * The persistent class for the event_tracker database table.
 * 
 */
@Entity
@Table(name="event_tracker")

@NamedQueries({
@NamedQuery(name="EventTracker.findAll", query="SELECT e FROM EventTracker e"),
@NamedQuery(name="EventTracker.findTimeUpEvent",query="SELECT e FROM EventTracker e WHERE e.eventTime <= :currentTime and e.status='PENDING'"),
})
public class EventTracker implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String event;
	
 	private  LocalDateTime eventTime;
	
	private String status;
	
	@JoinColumn(name="requestId")
	@ManyToOne(fetch=FetchType.LAZY)
	private RequestMaster requestMaster;
	
	@JoinColumn(name="actionId")
	@ManyToOne(fetch=FetchType.LAZY)
	private UserActionMaster userActionMaster;
	
	private Boolean active=true;
	
	Integer executionLevel;
	
	public EventTracker()
	{
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getEvent()
	{
		return event;
	}

	public void setEvent(String event)
	{
		this.event = event;
	}


	public LocalDateTime getEventTime()
	{
		return eventTime;
	}

	public void setEventTime(LocalDateTime eventTime)
	{
		this.eventTime = eventTime;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public RequestMaster getRequestMaster()
	{
		return requestMaster;
	}

	public void setRequestMaster(RequestMaster requestMaster)
	{
		this.requestMaster = requestMaster;
	}

	public UserActionMaster getUserActionMaster() {
		return userActionMaster;
	}

	public void setUserActionMaster(UserActionMaster userActionMaster) {
		this.userActionMaster = userActionMaster;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getExecutionLevel() {
		return executionLevel;
	}

	public void setExecutionLevel(Integer executionLevel) {
		this.executionLevel = executionLevel;
	}
	
}
