package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity(name="nonapproval_designation")
@NamedQueries({
	@NamedQuery(name="NonApprovalDesignation.findAllByDesignation",query="select nad from nonapproval_designation nad where nad.designation=:desig"),
	@NamedQuery(name="NonApprovalDesignation.findDesignationByActive",query="select nad.designation from nonapproval_designation nad where nad.active=:active"),	
})
public class NonApprovalDesignation implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "designation")
	private String designation;
	
	@Column(name = "type")
	private String type;
	
	@Column(name="active")
	byte active;
	
	@Column(name="updatedBy")
	private String updatedBy;
	
	@Column(name="updationTime")
	private Date updationTime;
	
	@Column(name="ipaddress")
	private String ipaddress;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	

	public byte getActive() {
		return active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public Date getUpdationTime() {
		return updationTime;
	}

	public void setUpdationTime(Date updationTime) {
		this.updationTime = updationTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	
	
	

}
