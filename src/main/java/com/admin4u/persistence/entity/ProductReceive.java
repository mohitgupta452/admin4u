package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the product_receive database table.
 * 
 */
@Entity
@Table(name="product_receive")
@NamedQuery(name="ProductReceive.findAll", query="SELECT p FROM ProductReceive p")
public class ProductReceive implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="payment_id")
	private String paymentId;

	private String product_Id;

	@Column(name="received_quantity")
	private String receivedQuantity;

	public ProductReceive() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPaymentId() {
		return this.paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getProduct_Id() {
		return this.product_Id;
	}

	public void setProduct_Id(String product_Id) {
		this.product_Id = product_Id;
	}

	public String getReceivedQuantity() {
		return this.receivedQuantity;
	}

	public void setReceivedQuantity(String receivedQuantity) {
		this.receivedQuantity = receivedQuantity;
	}

}