package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: SMSeat
 *
 */
@Entity(name="smseat")
@NamedQueries({
	@NamedQuery(name="SMSeat.findAll",query="SELECT s from smseat s "),
	@NamedQuery(name="SMSeat.findByFloor",query="SELECT s from smseat s where s.floor=:floor "),
	@NamedQuery(name="SMSeat.findByEmployeeUPN",query="SELECT s from smseat s where s.employeeUPN=:employeeUPN"),
	@NamedQuery(name="SMSeat.findByFloorAndAllocationStatus",query="SELECT s from smseat s where s.floor=:floor  and s.allocationStatus=:allocationStatus"),


	


	
})
public class SMSeat implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private int seatNumber;
	
	private String employeeUPN;
	
	private String allocationStatus;
	
	private String seatCode;
	
	@ManyToOne
	@JoinColumn(name="floorId")
	private SMFloor floor;
	
	

	public SMSeat() {
		super();
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getSeatNumber() {
		return seatNumber;
	}



	public void setSeatNumber(int seatNumber) {
		this.seatNumber = seatNumber;
	}



	public String getEmployeeUPN() {
		return employeeUPN;
	}



	public void setEmployeeUPN(String employeeUPN) {
		this.employeeUPN = employeeUPN;
	}



	public String getAllocationStatus() {
		return allocationStatus;
	}



	public void setAllocationStatus(String allocationStatus) {
		this.allocationStatus = allocationStatus;
	}



	public String getSeatCode()
	{
		return seatCode;
	}



	public void setSeatCode(String seatCode)
	{
		this.seatCode = seatCode;
	}



	public SMFloor getFloor() {
		return floor;
	}



	public void setFloor(SMFloor floor) {
		this.floor = floor;
	}
   
}
