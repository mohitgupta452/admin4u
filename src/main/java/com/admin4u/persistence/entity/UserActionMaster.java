package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the user_action database table.
 * 
 */
@Entity
@Table(name="user_action")
@NamedQueries({@NamedQuery(name="UserActionMaster.findByOwner",query="select u FROM UserActionMaster u WHERE u.owner=:upn"),
@NamedQuery(name="UserActionMaster.findByRole",query="select u FROM UserActionMaster u WHERE  u.role=:role"),
@NamedQuery(name="UserActionMaster.findByRoleAndLocation",query="select u FROM UserActionMaster u WHERE  u.role=:role AND u.location=:location"),
@NamedQuery(name="UserActionMaster.findByOnView",query="select u FROM UserActionMaster u WHERE  u.role=:role AND u.owner =:owner AND u.onView=:onView"),
@NamedQuery(name="UserActionMaster.findByServiceRoleAndLocation",query="select u FROM UserActionMaster u WHERE  u.role=:role AND u.location=:location AND u.service=:service"),
@NamedQuery(name="USerActionMaster.findByRoleAndOwnerUPN",query="select u FROM UserActionMaster u WHERE u.owner=:upn AND u.role=:role"),
@NamedQuery(name="UserActionMaster.findByStatusRoleRequest",query="select u from UserActionMaster u where u.status='PENDING' and u.role='APPROVAL_AUTHORITY' and u.requestMaster=:requestMaster"),
@NamedQuery(name="UserActionMaster.findByRequestMaster",query="select u from UserActionMaster u where u.requestMaster=:requestMaster"),
@NamedQuery(name="UserActionMaster.findByAdminRequests",query="select u FROM UserActionMaster u  WHERE  u.role=:role AND u.location=:location AND NOT u.status like '%forword%'"),
@NamedQuery(name="USerActionMaster.countByRoleAndOwnerUPN",query="select count(u.actionID) FROM UserActionMaster u WHERE u.owner=:upn AND u.role=:role"),


})
public class UserActionMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="actionId")
	private int actionID;

	private String action;

	private String actionBy;

	private Date actionTime;

	private Date assignTime;

	private String IPAddress;

	private String location;

	@Column(name="modify_time")
	private Date modifyTime;

	private byte onView;

	@Column(name="isRead")
	private boolean read;

	@Column(name="ownerRole")
	private String role;
	
	@Column(name="ownerUPN")
	private String owner;

	private String previousOwnerUPN;

	@ManyToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name="requestID")
	private RequestMaster requestMaster;

	private String service;

	private String status;
	
	@Column(name="active")
	private Boolean active=true;
	
	private Integer exclanationLevel=1;

	public UserActionMaster() {
	}


	public int getActionID() {
		return actionID;
	}


	public void setActionID(int actionID) {
		this.actionID = actionID;
	}


	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getActionBy() {
		return this.actionBy;
	}

	public void setActionBy(String actionBy) {
		this.actionBy = actionBy;
	}


	public String getIPAddress() {
		return this.IPAddress;
	}

	public void setIPAddress(String IPAddress) {
		this.IPAddress = IPAddress;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public byte getOnView() {
		return this.onView;
	}

	public void setOnView(byte onView) {
		this.onView = onView;
	}

	public String getPreviousOwnerUPN() {
		return this.previousOwnerUPN;
	}

	public void setPreviousOwnerUPN(String previousOwnerUPN) {
		this.previousOwnerUPN = previousOwnerUPN;
	}

	public String getService() {
		return this.service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public RequestMaster getRequestMaster() {
		return requestMaster;
	}

	public void setRequestMaster(RequestMaster requestMaster) {
		this.requestMaster = requestMaster;
	}

	public Date getActionTime() {
		return actionTime;
	}

	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}

	public Date getAssignTime() {
		return assignTime;
	}

	public void setAssignTime(Date assignTime) {
		this.assignTime = assignTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}


	public Boolean getActive() {
		return active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Integer getExclanationLevel() {
		return exclanationLevel;
	}


	public void setExclanationLevel(Integer exclanationLevel) {
		this.exclanationLevel = exclanationLevel;
	}


}