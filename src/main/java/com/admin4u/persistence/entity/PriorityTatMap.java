package com.admin4u.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity(name="priority_tat_map")
@NamedQueries({@NamedQuery(name="PriorityTatMap.findByPriority",query="select p FROM priority_tat_map p WHERE p.priority=:priority "),
@NamedQuery(name="PriorityTatMap.findAll",query="select p from priority_tat_map p"),
@NamedQuery(name="PriorityTatMap.findByService", query="select p from priority_tat_map p where p.service=:service")

	
} )

public class PriorityTatMap implements Serializable {

	
	private static final long serialVersionUID = -4391442882541095274L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="priority")
	private String priority;
	
	@Column(name="turnAroundTime")
	private long turnAroundTime;
	
	

	@Column(name="service")
	private String service;

	public PriorityTatMap() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public long getTurnAroundTime() {
		return turnAroundTime;
	}

	public void setTurnAroundTime(long turnAroundTime) {
		this.turnAroundTime = turnAroundTime;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

}
