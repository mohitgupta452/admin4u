package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the scheduledemail database table.
 * 
 */
@Entity
@Table(name="scheduledemail")
@NamedQuery(name="Scheduledemail.findAll", query="SELECT s FROM ScheduledEmail s")

public class ScheduledEmail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int scheduledemailid;

	private String attachments;

	private String emailevent;

	private String emailtype;

	private Date generationTime;

	private String mailbcc;

	@Lob
	private String mailbody;

	private String mailcc;

	private String mailsubject;

	private String mailto;

	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedtime;

	private Date processedTime;

	private int status;

	public ScheduledEmail() {
	}

	public int getScheduledemailid() {
		return this.scheduledemailid;
	}

	public void setScheduledemailid(int scheduledemailid) {
		this.scheduledemailid = scheduledemailid;
	}

	public String getAttachments() {
		return this.attachments;
	}

	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}

	public String getEmailevent() {
		return this.emailevent;
	}

	public void setEmailevent(String emailevent) {
		this.emailevent = emailevent;
	}

	public String getEmailtype() {
		return this.emailtype;
	}

	public void setEmailtype(String emailtype) {
		this.emailtype = emailtype;
	}


	public Date getGenerationTime() {
		return generationTime;
	}

	public void setGenerationTime(Date generationTime) {
		this.generationTime = generationTime;
	}

	public String getMailbcc() {
		return this.mailbcc;
	}

	public void setMailbcc(String mailbcc) {
		this.mailbcc = mailbcc;
	}

	public String getMailbody() {
		return this.mailbody;
	}

	public void setMailbody(String mailbody) {
		this.mailbody = mailbody;
	}

	public String getMailcc() {
		return this.mailcc;
	}

	public void setMailcc(String mailcc) {
		this.mailcc = mailcc;
	}

	public String getMailsubject() {
		return this.mailsubject;
	}

	public void setMailsubject(String mailsubject) {
		this.mailsubject = mailsubject;
	}

	public String getMailto() {
		return this.mailto;
	}

	public void setMailto(String mailto) {
		this.mailto = mailto;
	}

	public Date getModifiedtime() {
		return this.modifiedtime;
	}

	public void setModifiedtime(Date modifiedtime) {
		this.modifiedtime = modifiedtime;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getProcessedTime() {
		return processedTime;
	}

	public void setProcessedTime(Date processedTime) {
		this.processedTime = processedTime;
	}
	
}