package com.admin4u.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity(name="location")
@NamedQueries({
	@NamedQuery(name="Location.findAll",query="select l from location l "),
	@NamedQuery(name="Location.findLocationByName",query="select l from location l where l.locationName=:locationName"),
	@NamedQuery(name="Location.findAllLocationByType",query="select l.locationName from location l where l.type=:type"),
	@NamedQuery(name="Location.findAllLocationByParent",query="select l from location l where l.parent=:parent"),
	
})

public class Location implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "locationID")
	private int locationID;

	@Column(name="locationName")
	String locationName;
	
	@Column(name="locationShortName")
	private String locationShortName;
	
	@Column(name="isActive")
	boolean isActive;
	
	private int parent;
	
	private String type;
	
	private String tollfree;

	public int getLocationID() {
		return locationID;
	}

	public void setLocationID(int locationID) {
		this.locationID = locationID;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getLocationShortName() {
		return locationShortName;
	}

	public void setLocationShortName(String locationShortName) {
		this.locationShortName = locationShortName;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public int getParent()
	{
		return parent;
	}

	public void setParent(int parent)
	{
		this.parent = parent;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getTollfree() {
		return tollfree;
	}

	public void setTollfree(String tollfree) {
		this.tollfree = tollfree;
	}

}
