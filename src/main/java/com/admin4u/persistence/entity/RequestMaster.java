package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: RequestMaster
 *
 */
@Entity(name="request")
@NamedQueries({@NamedQuery(name="RequestMaster.findByUserAndService",query="select r FROM request r WHERE r.requestBy=:user AND r.service=:service AND( (r.requestDate >=:fromDate  AND r.requestDate<=:currentDate) OR r.requestStatus=:requestStatus)"),
	@NamedQuery(name="RequestMaster.findAll", query="SELECT r FROM request r"),
	@NamedQuery(name="RequestMaster.findAmendResMsgByRequestID", query="SELECT r.message FROM request r WHERE r.requestId=:reqID"),
	@NamedQuery(name="RequestMaster.findByUser",query="select r FROM request r WHERE r.requestBy=:upn ORDER BY r.requestDate DESC"),
	@NamedQuery(name="RequestMaster.findRequestIDByService", query="SELECT r.requestData FROM request r WHERE r.service=:service"),
}

		)


public class RequestMaster implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "requestID")
	private int requestId;
	
	@Column(name="location")
	private String location;
	
	@Column(name="requestBy")
	private String requestBy;
	
	@Column(name="requestType")
	private String requestType;
	
	@Column(name="requestDate")
	private  Date requestDate;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name="requestDataID")
	private RequestData requestData;
	
	@Column(name="service")
	private String service;
	
	@OneToMany(mappedBy="requestMaster")
	private List<UserActionMaster> userAction=new ArrayList<>();
	
	@Column(name="requestStatus")
	private String requestStatus;
	
	private  Date modifiedDate;
	
	private String ipAddress;
	
	private  Date closureDate;
	
	@Column(name="message")
	private String message;
	
	@Column(name="isComplete")
	private boolean complete;
	
	@Column(name="isExpire")
	private boolean expire;
	
	@Column(name="priority")
	private String priority;
	
	@OneToMany(mappedBy="request")
	private List<RequestServiceMap> requestService;
	
	@Column(name="isActive")
	private boolean active;
	
	@Column(name="workorder")
	private String workorder;
	
	@Transient
	private PriorityTatMap priorityByService;
	
	@OneToOne(mappedBy="requestMaster",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private ResponseMaster responseMaster;
	

	@OneToMany(mappedBy="requestMaster",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<EventTracker> eventTrackers;
	
	
	public String getPriority() {
		return priority;
	}


	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	

	public String getWorkorder() {
		return workorder;
	}


	public void setWorkorder(String workorder) {
		this.workorder = workorder;
	}

	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public List<UserActionMaster> getUserAction() {
		return userAction;
	}

	public void setUserAction(List<UserActionMaster> userAction) {
		this.userAction = userAction;
	}

	public boolean isComplete() {
		return complete;
	}


	public void setComplete(boolean complete) {
		this.complete = complete;
	}


	public String getService() {
		return service;
	}


	public void setService(String service) {
		this.service = service;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public boolean isActive() {
		return active;
	}


	public void setActive(boolean isActive) {
		this.active = isActive;
	}


	public String getRequestBy() {
		return requestBy;
	}

	public void setRequestBy(String requestBy) {
		this.requestBy = requestBy;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}


	public Date getRequestDate() {
		return requestDate;
	}

	

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}





	public String getRequestStatus() {
		return requestStatus;
	}





	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}





	public Date getModifiedDate() {
		return modifiedDate;
	}





	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}





	public String getIpAddress() {
		return ipAddress;
	}





	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}





	public Date getClosureDate() {
		return closureDate;
	}





	public void setClosureDate(Date closureDate) {
		this.closureDate = closureDate;
	}
	

	public RequestMaster() {
		super();
	}





	public RequestData getRequestData() {
		return requestData;
	}





	public void setRequestData(RequestData requestData) {
		this.requestData = requestData;
	}


	public List<RequestServiceMap> getRequestService() {
		return requestService;
	}


	public void setRequestService(List<RequestServiceMap> requestService) {
		this.requestService = requestService;
	}


	public PriorityTatMap getPriorityByService() {
		return priorityByService;
	}


	public void setPriorityByService(PriorityTatMap priorityByService) {
		this.priorityByService = priorityByService;
	}
   
	public ResponseMaster getResponseMaster() {
		return responseMaster;
	}


	public void setResponseMaster(ResponseMaster responseMaster) {
		this.responseMaster = responseMaster;
	}


	public boolean isExpire() {
		return expire;
	}


	public void setExpire(boolean expire) {
		this.expire = expire;
	}


	public List<EventTracker> getEventTrackers() {
		return eventTrackers;
	}


	public void setEventTrackers(List<EventTracker> eventTrackers) {
		this.eventTrackers = eventTrackers;
	}
	
	
	
}
