package com.admin4u.persistence.entity;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Transient;


@Entity(name="employeerole")
@NamedQueries({@NamedQuery( name="EmployeeRole.findByUPN",
query="SELECT r from employeerole r where r.userPrincipalName=:upn "),
@NamedQuery(name="EmployeeRole.findByRoleLocation", query="select e from employeerole e where e.role=:role AND e.location=:location"),
@NamedQuery(name="EmployeeRole.findByRoleLocationService", query="SELECT s FROM employeerole s JOIN s.employeeRoleServicemaster f  WHERE f.employeeRole.employeeRoleID= s.employeeRoleID AND f.service =:service AND s.location=:location AND s.role = :role"),
@NamedQuery(name="EmployeeRole.findByUPNAndRole", query="select e from employeerole e where e.userPrincipalName=:upn AND e.role=:role"),
@NamedQuery(name="EmployeeRole.findByRoleCreater", query="select e from employeerole e where e.createdBy=:createrUPN and e.role=:role" ),
@NamedQuery(name="EmployeeRole.findAllRole",query="select e from employeerole e where e.role=:role")
})
 
public class EmployeeRole implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "employeeRoleID")
	private int employeeRoleID;

	@Column(name = "role")
	private String role;
	
	@Column(name = "userPrincipalName")
	private String userPrincipalName;
	
	@Column(name="location")
	private String location;
	
	@Column(name="creationDate")
	private Date creationDate;
	
	@OneToMany(mappedBy="employeeRole",fetch=FetchType.EAGER)
	private List<EmployeeRoleServiceMaster> employeeRoleServicemaster;
	
	@Column(name="createdBy")
	private String createdBy;
	
	@Transient
	private String displayName;
	
	public List<EmployeeRoleServiceMaster> getEmployeeRoleServicemaster() {
		return employeeRoleServicemaster;
	}

	public void setEmployeeRoleServicemaster(List<EmployeeRoleServiceMaster> employeeRoleServicemaster) {
		this.employeeRoleServicemaster = employeeRoleServicemaster;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public int getEmployeeRoleID() {
		return employeeRoleID;
	}

	public void setEmployeeRoleID(int employeeRoleID) {
		this.employeeRoleID = employeeRoleID;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUserPrincipalName() {
		return userPrincipalName;
	}

	public void setUserPrincipalName(String userPrincipalName) {
		this.userPrincipalName = userPrincipalName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
