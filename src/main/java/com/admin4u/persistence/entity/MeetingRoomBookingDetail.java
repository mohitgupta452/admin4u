package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: MeetingRoomBookingDetail
 *
 */
@Entity(name="meetingroombookingdetail")
@NamedQueries({

	@NamedQuery(name="MeetingRoomBookingDetail.findAll",query="Select m from meetingroombookingdetail m"),
	@NamedQuery(name="MeetingRoomBookingDetail.findAllBookedRooms",query="select m from meetingroombookingdetail m where m.bookingStartDateTime>=:startDateTime and m.bookingStartDateTime<=:endDateTime"),
	@NamedQuery(name="MeetingRoomBookingDetail.findRoomAvailability",query="select m from meetingroombookingdetail m where m.meetingRoom=:meetingRoom and (( m.bookingStartDateTime between :startDateTime and :endDateTime ) or (m.bookingEndDateTime between :startDateTime and :endDateTime) or (m.bookingStartDateTime<=:startDateTime and m.bookingEndDateTime>=:startDateTime))"),
	@NamedQuery(name="MeetingRoomBookingDetail.findDetailByRequestId",query="select m from meetingroombookingdetail m where m.request=:request"),
	@NamedQuery(name="MeetingRoomBookingDetail.findRoomHistory",query="select m from meetingroombookingdetail m where m.meetingRoom=:meetingRoom and m.bookingStartDateTime>=:fromDate "),

})

public class MeetingRoomBookingDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date bookingStartDateTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date bookingEndDateTime;
	
	@ManyToOne
	@JoinColumn(name="meetingRoomId")
	private SMRoom meetingRoom;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="requestId")
	private RequestMaster request;

	public MeetingRoomBookingDetail() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public Date getBookingStartDateTime() {
		return bookingStartDateTime;
	}

	public void setBookingStartDateTime(Date bookingStartDateTime) {
		this.bookingStartDateTime = bookingStartDateTime;
	}

	public Date getBookingEndDateTime() {
		return bookingEndDateTime;
	}

	public void setBookingEndDateTime(Date bookingEndDateTime) {
		this.bookingEndDateTime = bookingEndDateTime;
	}

	public RequestMaster getRequest() {
		return request;
	}

	public void setRequest(RequestMaster request) {
		this.request = request;
	}

	public SMRoom getMeetingRoom() {
		return meetingRoom;
	}

	public void setMeetingRoom(SMRoom meetingRoom) {
		this.meetingRoom = meetingRoom;
	}
	
	
   
}
