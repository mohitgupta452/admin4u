package com.admin4u.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity(name="loc_service_map")

@NamedQueries({
	@NamedQuery(name="LocServiceMap.findByLocationID",query="select l from loc_service_map l where l.locationName=:locationName"),
	@NamedQuery(name="LocServiceMap.findByServiceName",query="select l from loc_service_map l where l.service=:serviceName and l.locationName=:serviceLocation"),
	@NamedQuery(name="LocServiceMap.findServiceStatusByServAndLocName",query="select l.isActive from loc_service_map l where l.service=:serviceName and l.locationName=:locationName")
})
public class LocServiceMap implements Serializable {
	
	
	private static final long serialVersionUID = 1624712835003455148L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private int locServiceMappingID;

	@Column(name="locationName")
	String locationName;
	
	@Column(name="service")
	private String service;
	
	@Column(name="isActive")
	byte isActive;
	
	@Column(name="createdBy")
	private String createdBy;
	
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public LocServiceMap() {
		// TODO Auto-generated constructor stub
	}
	public int getLocServiceMappingID() {
		return locServiceMappingID;
	}
	public void setLocServiceMappingID(int locServiceMappingID) {
		this.locServiceMappingID = locServiceMappingID;
	}
	
	
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public byte getIsActive() {
		return isActive;
	}
	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}
	

	
}
