package com.admin4u.persistence.entity;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="mis_details")
@NamedQuery(name="MISDetails.findAll", query="SELECT m FROM MISDetails  m")
public class MISDetails implements Serializable{

	private static final long serialVersionUID = 1L; 
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int misId;
	
	@Column(name="type")
	private String type;

	/*@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL )
	@JoinColumn(name="vendorId")
	private VendorInfo vendorInfoId; */
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL )
	@JoinColumn(name="pr_details_id")
	private PrDetail prDetailsId;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL )
	@JoinColumn(name="podetails_id")
	private Podetail poDetailsId;
	
	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL )
	@JoinColumn(name="payment_info_id")
	private PaymentInfo paymentInfoId;
	
	private String companyName;
	
	private String costCenter;
	
	@Column(name = "nature_of_expenses")
	private String natureOfExpenses;

	private String attachment;
	
	@Column(name="submission_to_acc")
	private Timestamp submissionToAcc;
	
	public int getMisId() {
		return misId;
	}

	public void setMisId(int misId) {
		this.misId = misId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/*public VendorInfo getVendorInfoId() {
		return vendorInfoId;
	}

	public void setVendorInfoId(VendorInfo vendorInfoId) {
		this.vendorInfoId = vendorInfoId;
	}
*/
	public PrDetail getPrDetailsId() {
		return prDetailsId;
	}

	public void setPrDetailsId(PrDetail prDetailsId) {
		this.prDetailsId = prDetailsId;
	}

	public Podetail getPoDetailsId() {
		return poDetailsId;
	}

	public void setPoDetailsId(Podetail poDetailsId) {
		this.poDetailsId = poDetailsId;
	}

	public PaymentInfo getPaymentInfoId() {
		return paymentInfoId;
	}

	public void setPaymentInfoId(PaymentInfo paymentInfoId) {
		this.paymentInfoId = paymentInfoId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	public String getNatureOfExpenses() {
		return natureOfExpenses;
	}

	public void setNatureOfExpenses(String natureOfExpenses) {
		this.natureOfExpenses = natureOfExpenses;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public Timestamp getSubmissionToAcc() {
		return submissionToAcc;
	}

	public void setSubmissionToAcc(Timestamp submissionToAcc) {
		this.submissionToAcc = submissionToAcc;
	}

}
