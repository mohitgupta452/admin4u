package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: SMBuilding
 *
 */
@Entity(name="smbuilding")

@NamedQueries({
@NamedQuery(name="SMBuilding.findAll", query="SELECT b FROM smbuilding b"),
@NamedQuery(name="SMBuilding.findByLocation", query="SELECT b FROM smbuilding b where b.location=:location"),
@NamedQuery(name="SMBuilding.findByBuildingName", query="SELECT b FROM smbuilding b where b.buildingName=:buildingName"),
@NamedQuery(name="SMBuilding.findByLocationAndType", query="SELECT b FROM smbuilding b where b.location=:location and b.buildingType=:buildingType"),



})

public class SMBuilding implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String buildingName;
	
	private int noOfFloors;
	
	private String subLocation;
	
	@ManyToOne
	@JoinColumn(name="locationId")
	private Location location;
	
	@OneToMany(mappedBy="building",fetch=FetchType.EAGER)
	private List<SMFloor> floorList;
	
	@OneToMany(mappedBy="building",cascade = CascadeType.ALL)
	private List<SMRoom> roomList;
	
	private String buildingType;
	
	private String buildingAddress;
	

	public SMBuilding() {
		super();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getBuildingName() {
		return buildingName;
	}


	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}


	public int getNoOfFloors() {
		return noOfFloors;
	}


	public void setNoOfFloors(int noOfFloors) {
		this.noOfFloors = noOfFloors;
	}


	public String getSubLocation()
	{
		return subLocation;
	}


	public void setSubLocation(String subLocation)
	{
		this.subLocation = subLocation;
	}


	public Location getLocation() {
		return location;
	}


	public void setLocation(Location location) {
		this.location = location;
	}


	public List<SMFloor> getFloorList() {
		return floorList;
	}


	public void setFloorList(List<SMFloor> floorList) {
		this.floorList = floorList;
	}


	public List<SMRoom> getRoomList()
	{
		return roomList;
	}


	public void setRoomList(List<SMRoom> roomList)
	{
		this.roomList = roomList;
	}


	public String getBuildingType()
	{
		return buildingType;
	}


	public void setBuildingType(String buildingType)
	{
		this.buildingType = buildingType;
	}


	public String getBuildingAddress()
	{
		return buildingAddress;
	}


	public void setBuildingAddress(String buildingAddress)
	{
		this.buildingAddress = buildingAddress;
	}
	
	
   
}
