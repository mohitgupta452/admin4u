package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: SMFloor
 *
 */
@Entity(name="smfloor")
@NamedQueries({
	@NamedQuery(name="SMFloor.findAll",query="SELECT f from smfloor f "),

	@NamedQuery(name="SMFloor.findByBuilding",query="select f from smfloor f where f.building=:building"),
	@NamedQuery(name="SMFloor.findByBuildingAndFloorCode",query="select f from smfloor f where f.building=:building and f.floorCode=:floorCode"),

	
})

public class SMFloor implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String floorCode;
	
	private int noOfRooms;
	
	private int noOfSeats;
	
	@ManyToOne
	@JoinColumn(name="buildingId")
	private SMBuilding building;
	
	@OneToMany(mappedBy="floor",fetch=FetchType.LAZY)
	private List<SMSeat> seatList;
	
	@OneToMany(mappedBy="floor",fetch=FetchType.LAZY)
	private List<SMRoom> roomList;
	
	public SMFloor() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFloorCode() {
		return floorCode;
	}

	public void setFloorCode(String floorCode) {
		this.floorCode = floorCode;
	}

	public int getNoOfRooms() {
		return noOfRooms;
	}

	public void setNoOfRooms(int noOfRooms) {
		this.noOfRooms = noOfRooms;
	}

	public int getNoOfSeats() {
		return noOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}

	public SMBuilding getBuilding() {
		return building;
	}

	public void setBuilding(SMBuilding building) {
		this.building = building;
	}

	public List<SMSeat> getSeatList() {
		return seatList;
	}

	public void setSeatList(List<SMSeat> seatList) {
		this.seatList = seatList;
	}

	public List<SMRoom> getRoomList() {
		return roomList;
	}

	public void setRoomList(List<SMRoom> roomList) {
		this.roomList = roomList;
	}
   
	
}
