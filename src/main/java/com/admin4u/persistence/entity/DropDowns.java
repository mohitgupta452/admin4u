package com.admin4u.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity(name="dropdowns")

@NamedQueries({
	@NamedQuery(name="DropDowns.findByDropDownName",query="select d from dropdowns d where d.fieldName=:fieldName")
	
	
})
public class DropDowns implements Serializable {
	
	
	private static final long serialVersionUID = 1624712835003455148L;

	@Id
	@Column(name = "id")
	private int dropDownID;

	@Column(name="fieldName")
	String fieldName;
	
	@Column(name="data")
	private String data;
	
	@Column(name="isActive")
	Boolean isActive;

	public int getDropDownID() {
		return dropDownID;
	}

	public void setDropDownID(int dropDownID) {
		this.dropDownID = dropDownID;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	
}
