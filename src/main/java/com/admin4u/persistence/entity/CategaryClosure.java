package com.admin4u.persistence.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity(name="categary_closure")
@NamedQueries({
	@NamedQuery(name="CategaryClosure.findByAncestor",query="SELECT cc FROM categary_closure cc WHERE cc.ancestor.id=:ancestor"),
	@NamedQuery(name="CategaryClosure.findByDepth",query="select cc from categary_closure cc where cc.depth=:depth")
})
public class CategaryClosure implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER,targetEntity=CategaryMaster.class)
	@JoinColumn(name="ancestor")
	private CategaryMaster ancestor;
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER,targetEntity=CategaryMaster.class)
	@JoinColumn(name="descendant")
	private CategaryMaster descendant;
	
	private int depth;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public CategaryMaster getAncestor()
	{
		return ancestor;
	}

	public void setAncestor(CategaryMaster ancestor)
	{
		this.ancestor = ancestor;
	}

	public CategaryMaster getdescendant()
	{
		return descendant;
	}

	public void setdescendant(CategaryMaster descendant)
	{
		this.descendant = descendant;
	}

	public int getDepth()
	{
		return depth;
	}

	public void setDepth(int depth)
	{
		this.depth = depth;
	}
	
}
