package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the contact_details database table.
 * 
 */
@Entity
@Table(name="contact_details")
@NamedQuery(name="ContactDetail.findAll", query="SELECT c FROM ContactDetail c")
public class ContactDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private byte active;
	
	@Column(name="contactPerson")
	private String contactPerson;

	@Column(name="email")
	private String email;

	@Column(name="mobile")
	private String mobile;

	@Column(name="name")
	private String name;

	@Column(name="phone")
	private String phone;

	@Column(name="type")
	private String type;

	//bi-directional many-to-one association to VendorInfo
	@OneToMany(mappedBy="contactDetail")
	private List<VendorInfo> vendorInfos;
	
	@Column(name="salutation")
	private String salutation;
	
	@Column(name="designation")
	private String designation;
	
	@Column(name="department")
	private String department;
	
	@Column(name="state_of_incorporation")
	private String stateOfIncorporation;
	
	@Column(name="year_of_incorporation")
	private String yearOfIncorporation;
	
	@Column(name="preff_delivery_method")
	private String preffDeliveryMethod;
	

	public ContactDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<VendorInfo> getVendorInfos() {
		return this.vendorInfos;
	}

	public void setVendorInfos(List<VendorInfo> vendorInfos) {
		this.vendorInfos = vendorInfos;
	}

	public VendorInfo addVendorInfo(VendorInfo vendorInfo) {
		getVendorInfos().add(vendorInfo);
		vendorInfo.setContactDetail(this);

		return vendorInfo;
	}

	public VendorInfo removeVendorInfo(VendorInfo vendorInfo) {
		getVendorInfos().remove(vendorInfo);
		vendorInfo.setContactDetail(null);

		return vendorInfo;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getStateOfIncorporation() {
		return stateOfIncorporation;
	}

	public void setStateOfIncorporation(String stateOfIncorporation) {
		this.stateOfIncorporation = stateOfIncorporation;
	}

	public String getYearOfIncorporation() {
		return yearOfIncorporation;
	}

	public void setYearOfIncorporation(String yearOfIncorporation) {
		this.yearOfIncorporation = yearOfIncorporation;
	}

	public String getPreffDeliveryMethod() {
		return preffDeliveryMethod;
	}

	public void setPreffDeliveryMethod(String preffDeliveryMethod) {
		this.preffDeliveryMethod = preffDeliveryMethod;
	}

	public byte getActive() {
		return active;
	}

	public void setActive(byte active) {
		this.active = active;
	}
	
	
	

}