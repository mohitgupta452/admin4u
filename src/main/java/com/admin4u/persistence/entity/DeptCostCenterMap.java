package com.admin4u.persistence.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the employeedata database table.
 *
 */
@Entity(name = "dept_costcenter_map")

@NamedQueries({ 
@NamedQuery(name = "DeptCostCenterMap.findAll", query = "SELECT d FROM dept_costcenter_map d"),
@NamedQuery(name = "DeptCostCenterMap.findCostCenterByDept" , query="SELECT d.costCenterCode FROM dept_costcenter_map d where d.department =:dept"),
@NamedQuery(name="DeptCostCenterMap.findCostCenterMapByDept",query="SELECT d from dept_costcenter_map d where d.department=:department "),

} )

public class DeptCostCenterMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="dept_costcenter_mapID")
	private int deptCostCenterMapID;

	@Column(name="department")
	private String department;

	@Column(name="costCenter")
	private String costCenter;

	@Column(name="costCenterCode")
	private String costCenterCode;

	
	public DeptCostCenterMap() {
	}


	public int getDeptCostCenterMapID() {
		return deptCostCenterMapID;
	}


	public void setDeptCostCenterMapID(int deptCostCenterMapID) {
		this.deptCostCenterMapID = deptCostCenterMapID;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	public String getCostCenter() {
		return costCenter;
	}


	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}


	public String getCostCenterCode() {
		return costCenterCode;
	}


	public void setCostCenterCode(String costCenterCode) {
		this.costCenterCode = costCenterCode;
	}

	

	
   
}
