package com.admin4u.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: NotificationMaster
 *
 */
@Entity(name="notification_log")

@NamedQueries({@NamedQuery(name="NotificationMaster.findAllByUpn",query="SELECT n from notification_log n WHERE n.reciever=:upn order by n.notificationId desc"),
@NamedQuery(name="NotificationMaster.findAllByUpnStatus",query="SELECT n from notification_log n WHERE n.reciever=:upn AND n.status=:status" ),
@NamedQuery(name="NotificationMaster.findByRequestIDAndUpn",query="SELECT n from notification_log n WHERE n.message=:requestID and n.reciever=:upn"),
@NamedQuery(name="NotificationMaster.findByTime",query="SELECT n from notification_log n WHERE  n.reciever=:upn and  n.creationTime >=:timeSlot and n.creationTime <= :currentTime")

})

public class NotificationMaster implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="notificationID")
	private int notificationId;
	
	private String message;
	
	private String sender;
	
	private String reciever;
	
	private String status="PENDING";
	
	private Date creationTime;
	
	private String recieverRole;
	
	@JoinColumn(name="actionId")
	@ManyToOne
	private UserActionMaster userActionMaster;
	
	@JoinColumn(name="requestId")
	@ManyToOne
	private RequestMaster requestMaster;
	
	@Column(name="isViewed")
	private  boolean read;
	
	
	
	public NotificationMaster() {
		super();
	}
	

	public int getNotificationId() {
		return notificationId;
	}




	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}




	public String getMessage() {
		return message;
	}




	public void setMessage(String message) {
		this.message = message;
	}




	public String getSender() {
		return sender;
	}




	public void setSender(String sender) {
		this.sender = sender;
	}




	public String getReciever() {
		return reciever;
	}




	public void setReciever(String reciever) {
		this.reciever = reciever;
	}




	public String getStatus() {
		return status;
	}




	public void setStatus(String status) {
		this.status = status;
	}




	public Date getCreationTime() {
		return creationTime;
	}




	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}


	public UserActionMaster getUserActionMaster() {
		return userActionMaster;
	}


	public void setUserActionMaster(UserActionMaster userActionMaster) {
		this.userActionMaster = userActionMaster;
	}


	public String getRecieverRole() {
		return recieverRole;
	}


	public void setRecieverRole(String recieverRole) {
		this.recieverRole = recieverRole;
	}


	public boolean isRead() {
		return read;
	}


	public void setRead(boolean read) {
		this.read = read;
	}


	public RequestMaster getRequestMaster() {
		return requestMaster;
	}


	public void setRequestMaster(RequestMaster requestMaster) {
		this.requestMaster = requestMaster;
	}
   
	
}
