package com.admin4u.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the product_detail database table.
 * 
 */
@Entity
@Table(name="product_detail")

@NamedQueries({
@NamedQuery(name="ProductDetail.findAll", query="SELECT p FROM ProductDetail p"),
@NamedQuery(name="ProductDetail.findProductDetailById", query="SELECT p FROM ProductDetail p WHERE p.id =:pdtId"),
@NamedQuery(name="ProductDetail.findAllActiveProd", query="SELECT p FROM ProductDetail p where p.active=:active")

})
public class ProductDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy=GenerationType.IDENTITY)
	private int id;

	private String brand;

	@JoinColumn(name="categary")
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private CategaryMaster categary;

	private String color;

	private String desciption;

	private String height;

	private String length;

	private String model;

	private String name;

	private String price;

	@Column(name="product_code")
	private String productCode;

	private String sku;

	private String subcategary;

	private String type;

	private String updatedBy;

	private Date updationTime;

	private String weight;

	private String width;
	
	private String unit;
	
	private Boolean active;
	
	@JoinColumn(name="master_categary")
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private CategaryMaster categaryMaster;

	public ProductDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDesciption() {
		return this.desciption;
	}

	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}

	public String getHeight() {
		return this.height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getLength() {
		return this.length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getProductCode() {
		return this.productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getSku() {
		return this.sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getSubcategary() {
		return this.subcategary;
	}

	public void setSubcategary(String subcategary) {
		this.subcategary = subcategary;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Date getUpdationTime()
	{
		return updationTime;
	}

	public void setUpdationTime(Date updationTime)
	{
		this.updationTime = updationTime;
	}

	public String getWeight() {
		return this.weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getWidth() {
		return this.width;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public CategaryMaster getCategaryMaster() {
		return categaryMaster;
	}

	public void setCategaryMaster(CategaryMaster categaryMaster) {
		this.categaryMaster = categaryMaster;
	}

	public CategaryMaster getCategary() {
		return categary;
	}

	public void setCategary(CategaryMaster categary) {
		this.categary = categary;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	
	
}