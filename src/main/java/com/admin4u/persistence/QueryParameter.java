package com.admin4u.persistence;
import java.util.HashMap;
import java.util.Map;
public class QueryParameter {

	@SuppressWarnings("rawtypes")
	private Map parameters = null;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private QueryParameter(String name, Object value) {
		this.parameters = new HashMap();
		this.parameters.put(name, value);
	}

	public static QueryParameter with(String name, Object value) {
		return new QueryParameter(name, value);
	}

	@SuppressWarnings("unchecked")
	public QueryParameter and(String name, Object value) {
		this.parameters.put(name, value);
		return this;
	}

	@SuppressWarnings("rawtypes")
	public Map parameters() {
		return this.parameters;
	}
}