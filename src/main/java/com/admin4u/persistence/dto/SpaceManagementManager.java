package com.admin4u.persistence.dto;

import java.util.Date;
import java.util.List;

import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.persistence.entity.SMSeat;
import com.admin4u.persistence.entity.MeetingRoomBookingDetail;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.persistence.entity.SMFloor;

public interface SpaceManagementManager {
	
	
//  meeting/conference room booking methods
	
	public boolean addRoom(SMRoom room,int floorId);
	
	public boolean addRooms(List<SMRoom> roomList,int floorId);

	
	public boolean updateRoomDetail(SMRoom room);
	
	public boolean deleteRoom(int roomId);
	
	public SMRoom getRoomDetail(int roomId);
	
	public List<SMRoom> getRoomsByType(int  buildingId,String roomType);
	
	public List<SMRoom> getRoomsByFloor(int floorId,String roomType);

	
	public boolean allocateRoom(int roomId,String employeeUPN);
	
	public boolean isRoomAvailable(int roomId);
	
	public boolean isRoomAvailableByRoomNo(int floorId,int roomNo );
	
	public  List<SMRoom> getAllocatedRooms(int id,String roomType);
	
	public List<SMRoom> getAvailableRooms(int id,String roomType);
	
	public  List<SMSeat> getAllocatedSeats(int floorId,String allocStatus);

	
	public boolean deallocateCurrentRoom(String employeeUPN);
	
	public String getEmployeeSeatInfo(String employeeUPN);

	
	//public List<MeetingRoom> getAllAvailableMeetingRooms(int buildingId);
	
	public boolean checkMeetingRoomAvailability(int roomId,Date startDateTime,Date endDateTime);
	
	boolean saveMeetingRoomBookingDetail(int roomId,Date meetingStartDateTime,Date meetingEndDateTime,int requestId);
	
	public boolean removeMeetingRoomBookingDetail(int requestId);

	
	//End meeting/conference room booking methods
	
	
	//start space management methods
	
	public boolean addBuildings(List<SMBuilding> buildingList,int locationId);
	
	public boolean removeBuildings(List<Integer>buildingIdList);
	
	public boolean updateBuildingInfo(SMBuilding building,int buildingId);
	
	public SMBuilding getBuildingInfoByBuildingId(int buildingId);
	
	public List<SMBuilding> getBuildingsByLocation(int locationId);
	
	public boolean isBuilding(String buildingName);
	
	public SMBuilding getBuildingByBuildingName(String buildingName);
	
	public boolean isFloor(SMBuilding building ,String floorCode);


	
	 
	
	
	public boolean addFloors(List<SMFloor> floorList,int buildingId);
	
	public boolean addFloorsByNumber(int floorCount,int buildingId);

	
	public boolean removeFloors(List<Integer> floorIdList);
	
	public boolean updateFloorInfo(SMFloor floor,int floorId);
	
	public SMFloor getFloorInfoByFloorId(int floorId);
	
	public List<SMFloor> getFloorsByBuilding(int buildingId);
	
	public SMBuilding addBuilding(SMBuilding building,int locationId);
	
	
	
	 
	
	
	public boolean addSeatsToFloor(int floorId,int startingFrom,int endTo);
	
	public boolean addSeatsToFloorWithPattern(int floorId,int startingFrom,int endTo,String pattern);

		
	public SMSeat getSeatInfoBySeatId(int seatId);
	
	public List<SMSeat> getSeatsByFloor(int floorId);
	
	public String allocateSeat(String employeeUPN,int seatId);
	
	public boolean deallocateCurrentSeat(String employeeUPN);

	
	public boolean isSeatAvailable(int seatId);
	
	public List<SMSeat> getAvailableSeatsByFloor(int floorId);
	
	
	
	
	//end space management methods

}
