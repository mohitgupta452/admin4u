package com.admin4u.persistence.dto;

import com.admin4u.service.bussiness.contract.MailManager.MailEvents;
import java.util.HashSet;
import java.util.Set;

public class MailDTO {

	private String to;
	private String cc;
	private String bcc;
	private String subject;
	private String body;
	private Set<String> attachments;
	private MailEvents event;

	public MailDTO() {
		attachments = new HashSet<>();
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Set<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(Set<String> attachments) {
		this.attachments = attachments;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public MailEvents getEvent() {
		return event;
	}

	public void setEvent(MailEvents event) {
		this.event = event;
	}

}
