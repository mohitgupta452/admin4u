package com.admin4u.util.service;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.turningcloud.utils.FilesUtils;
import com.turningcloud.utils.excel.Excel;

public class ExcelCreation extends Excel{
	
	private String path ;
	
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	
	public ExcelCreation(File file) throws Exception
	{
		super(file);
	}

	public ExcelCreation(String path, String workBookName, Boolean protectedExcel) throws Exception {
		super(path, workBookName, protectedExcel);
		// TODO Auto-generated constructor stub
		
	}
	
	public static String createExcel(String workBookName,Map<String,String> headers)
	{
		
		ExcelCreation excelCreation=null;
		String path= FilesUtils.getPlatformBasedParentDir().getPath()+File.separator+"excel";
		try {
	         excelCreation= new ExcelCreation(path, workBookName+".xlsx",false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		excelCreation.path=path;
		
		headers.put("A1",workBookName);
		excelCreation.setLocked("A",3,50,false);
		excelCreation.setHeaders(headers);
		excelCreation.generateExcelFile();
		return path;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
