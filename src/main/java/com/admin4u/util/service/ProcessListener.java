package com.admin4u.util.service;

import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;

import com.admin4u.util.enums.Event;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData.ProcessType;

@RequestScoped
public interface ProcessListener {
	
	public Map<String,String> getOwnerByProcess();

	public  ProcessType getProcessType(Services service);
	
	public String actionPerformed(ApplicationMetaData.Action action);
	
	public String getNextState();
	
	public String getNextRequestStatus();
	
	public boolean isRequestCloseAction();
	
	public Map<String,String> getNextOwner();
	
	public Map<String,String> getNotificationMap();
	
	public List<String> getApprovalProcess() ;
	
	public List<String> getNonApprovalProcess();
	
	public Map<String,String> getNotificationByEvent(Event event);
	
	public Map<String,String> getEmails();
	
	public Map<String,String> getNotificationOnEvent(Event event);
	
	public void setProcessType(ProcessType processType);


}
