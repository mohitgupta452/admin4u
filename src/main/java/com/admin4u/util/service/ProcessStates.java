package com.admin4u.util.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.util.enums.Event;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData.Action;
import com.admin4u.util.service.ApplicationMetaData.ProcessType;
import com.admin4u.views.UserSessionBean;

@RequestScoped
public class ProcessStates extends ProcessHelper {
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	private Action action;
	
	
	public Map<String,String> getOwnerByProcess()
	{
		Map<String,String> ownerStatusMap= new HashMap<>();
		
		String owner= processProp.getProperty("NEXTOWNER."+processType.toString());
	     log.debug("owner {}",owner);
	     
	     if(owner.contains("."))
	    		 {
	    	List<String> ownerList=Arrays.asList(owner.split("\\."));
	    	for(String onr : ownerList)
	    	{
	    		 ownerStatusMap.put(onr,"PENDING");
	    	}
	    		 }
	     else
	    	 ownerStatusMap.put(owner, "PENDING");
	     return ownerStatusMap;
	}

	@Override
	public String actionPerformed(Action action) {
		this.action=action;
		return null;
	}


	@Override
	public String getNextState() {
		
		 return processProp.getProperty(processType.toString()+"."+action.toString()+".SELFSTATUS");
	}


	@Override
	public String getNextRequestStatus() {
		return processProp.getProperty(processType.toString()+"."+action.toString()+".REQUESTSTATUS");
	}


	@Override
	public boolean isRequestCloseAction() {
		
		List<String> reqCloseAction=Arrays.asList(processProp.getProperty("REQUEST.CLOSE.STATUS").split("\\."));
		if(reqCloseAction.contains(action.toString()))
			return true;
		return false;
	}


	public Map<String, String> getNextOwner() {
		
        Map<String,String> nextOwnerMap= new HashMap<>();
        System.out.println(processType.toString()+"."+action.toString());
		String ownerByAction= processProp.getProperty(processType.toString()+"."+action.toString());
		
		log.debug("onr {}...role {}",ownerByAction);
		
		 if(ownerByAction.contains("."))
		 {
	List<String> ownerList=Arrays.asList(ownerByAction.split("\\."));
	for(String onr : ownerList)
	{
		System.out.println("get by {}"+action+"."+onr+".STATUS");
		nextOwnerMap.put(onr,processProp.getProperty(processType.toString()+"."+action.toString()+"."+onr+".STATUS"));
	}
		 }
		return nextOwnerMap;
	}


	public Map<String, String> getNotificationMap() {

		Map<String,String> notificationMap= new HashMap<>();
		
		String notifyTo ;
		notifyTo=processProp.getProperty(processType.toString()+"."+action.toString()+".NOTIFY");
		log.debug("role {}....notify to {}",notifyTo);
		System.out.println(processType.toString()+"."+action.toString()+".NOTIFY");
		
		if(notifyTo.contains("."))
		 {
	List<String> notifyToList=Arrays.asList(notifyTo.split("\\."));
	for(String notify : notifyToList)
	{
		System.out.println(processType.toString()+"."+action.toString()+".NOTIFY."+"."+notify+".MSG");
		  notificationMap.put(notify,processProp.getProperty(processType.toString()+"."+action.toString()+".NOTIFY."+notify+".MSG"));//get notification msg
	}
		 }else
			 notificationMap.put(notifyTo, processProp.getProperty(processType.toString()+"."+action.toString()+".NOTIFY"+"."+notifyTo+".MSG"));
		
		log.debug("notification map {}",notificationMap);
	return notificationMap;
	}
	
	public Map<String,String> getNotificationByEvent(Event event)
	{
       Map<String,String> notificationMap= new HashMap<>();
		
		 notificationMap.put(Roles.REQUESTER.toString(),processProp.getProperty("EVENT."+event.toString()+"MSG.REQUESTER"));
		 notificationMap.put("AUTH",processProp.getProperty("EVENT."+event.toString()+"MSG.AUTH"));
		
		log.debug("notification map {}",notificationMap);
	return notificationMap;
	}
	
	public Map<String,String> getEmails()
	{
      Map<String,String> mailMap= new HashMap<>();
		
		String mailTo=processProp.getProperty(processType.toString()+"."+action.toString()+".MAILTO");
		log.debug("role {}...",mailTo);
		if(mailTo.contains("."))
		 {
	       List<String> mailToList=Arrays.asList(mailTo.split("\\."));
	for(String  mail : mailToList)
	{
		  mailMap.put(mail,processProp.getProperty(processType.toString()+"."+action.toString()+"."+mail+".EMAIL"));//get notification msg
	}
		 }else
			 mailMap.put(mailTo, processProp.getProperty(processType.toString()+"."+action.toString()+"."+mailTo+".EMAIL"));
		
		log.debug("mail map {}",mailMap);
		return mailMap;
	
	}
	
	public Map<String,String> getNotificationOnEvent(Event event)
	{
		 Map<String,String> notificationMap= new HashMap<>();
			
		String notifyTo=  processProp.getProperty(event.toString()+".NOTIFYTO");
		  
		if(notifyTo.contains("."))
		 {
	       List<String> notifyToList=Arrays.asList(notifyTo.split("\\."));
	for(String notify : notifyToList)
	{
		  notificationMap.put(notify,processProp.getProperty(event.toString()+".MSG."+notify));//get notification msg
	}
		 }else
			 notificationMap.put(notifyTo,processProp.getProperty(event.toString()+".MSG."+notifyTo));
		
	return notificationMap;
		
	}

}