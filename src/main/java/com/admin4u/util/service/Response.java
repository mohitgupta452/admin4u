package com.admin4u.util.service;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.service.bussiness.contract.IDMService;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sun.mail.imap.IdleManager;

@RequestScoped
@Named("res")
public class Response {
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	JsonObject jsonObj;
	
	@Inject
	private IDMService idmService;
	
	private void commonAtrToMap(RequestMaster requestMaster)
	{
		log.debug("inside common attrb to map");	
				
		jsonObj.addProperty("Location", requestMaster.getLocation());
		jsonObj.addProperty("Booking For", requestMaster.getRequestType());
		jsonObj.addProperty("Request Date", requestMaster.getRequestDate().toString());
	}
	
	/*@SuppressWarnings("unchecked")
	public String getRequestData(String service,RequestMaster requestMaster)
	{
		
	     if(service.equalsIgnoreCase("GUEST_HOUSE_BOOKING") || service.equalsIgnoreCase("HOTEL_BOOKING"))
	    	 service="hotelguesthouse".toUpperCase();
	     String reqdata= requestMaster.getRequestData().getRequestData();
	     
			
			log.debug("req data json return {}",reqdata);
			
			JsonObject jsonResponse= new JsonObject();
			
			Gson gson = new Gson();
			
			JsonObject jsonDup= new JsonObject();
			
			JsonObject jsonObj = new Gson().fromJson(reqdata, JsonObject.class);
		     
			for(java.util.Map.Entry<String,JsonElement> entry : jsonObj.entrySet())
			{
				if(! entry.getKey().contains("approovalAttachment"))
				jsonDup.addProperty(entry.getKey().replace(service.toLowerCase()+".",""),entry.getValue().toString().replace("\"",""));
			}
			
		    JsonObject jsonCommn=new Gson().fromJson(gson.toJson(this.commonAtrToMap(requestMaster)),JsonObject.class);
		    
		    Map firstObject = new Gson().fromJson(jsonCommn, HashMap.class);  
		    Map secondObj = (new Gson().fromJson(jsonDup, HashMap.class));
		    secondObj.putAll(firstObject);
		    
		    
		     jsonCommn=new Gson().fromJson(gson.toJson(secondObj),JsonObject.class);
		    
		    
		    log.debug("response json : {}",secondObj);
			
		return jsonCommn.toString();
		
	}

*/
	
	
	@SuppressWarnings("unchecked")
	public JsonObject getRequestData(String service,RequestMaster requestMaster)
	{
		/*JsonObject*/ jsonObj = new JsonObject();
		Properties fProperties = new Properties();
		try {
			fProperties.load(this.getClass().getClassLoader().getResourceAsStream("serviceAttribute.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		         List<String> atrList=  Arrays.asList(fProperties.getProperty("json").split("\\."));
		         jsonObj= requestMaster.getRequestData().getRequestData();
			 log.debug("value of json object {}",jsonObj);
			 jsonObj=this.removeService(jsonObj);
			for(String atr : atrList)
			{
				log.debug("atr   {}",atr);

		              if(jsonObj.has(service.toLowerCase()+"."+atr)){
		            	  
		            	  log.debug("value of atr{} and value of ",atr);
		            	  jsonObj.addProperty(atr,jsonObj.get(service.toLowerCase()+"."+atr).toString().replace("\"",""));
		              }
		              }
			
		//	gson.toJson();
			//calling method to append request data
			this.commonAtrToMap(requestMaster);
			
			
			jsonObj.remove("Request For");
			log.debug("response json {}",jsonObj);
		return jsonObj;
	}
	
	//method to remove service name from request data and to split date and time for view
	public  JsonObject removeService(JsonObject jsonObject ) throws JsonException{
		JsonObject json=new JsonObject();
	   for(Map.Entry<String, JsonElement>keyVal:jsonObject.entrySet()){
		   String propertyName="";
		   String propertyValue;
		   boolean isAdded=false;
		  
		   
		   StringTokenizer st=new StringTokenizer(keyVal.getKey(), ".");
		   if(st.hasMoreTokens()){
		    propertyName=st.nextToken();
		    if(st.hasMoreTokens())
		    propertyName=st.nextToken();
		    
		    if(propertyName.toLowerCase().contains("datetime"))
			   {
				String dateName;
				String timeName;
				String prefixDateTime;
				dateName=keyVal.getValue().getAsString().split("\\s")[0];
				timeName=keyVal.getValue().getAsString().split("\\s")[1];
				//dateName=propertyName.split("datetime")[0];
				
				   json.addProperty(propertyName, "DATE:"+dateName+" TIME:"+timeName);
				  // json.addProperty("new TIme", timeName);
				   isAdded=true;

			   }
		   }
		    log.debug("property Name {} value {}",propertyName,keyVal.getValue().toString());
		    if(!isAdded && keyVal.getValue()!=null){
		    	if( ! propertyName.equalsIgnoreCase("approovalAttachment"))
		    	{
		    		if(keyVal.getValue().isJsonArray())
		    			json.add(propertyName, keyVal.getValue());
		    		else
		   json.addProperty(propertyName, keyVal.getValue().toString().replaceAll("\"",""));
		   isAdded=false;
		    }
		    }	   
	   }
		
	    return json;
	}
	
	
}
