package com.admin4u.util.service;
public interface RequestMetadata {
	
		String LOCATION="location";
		String REQUESTER="requester";
		String REQUEST_TYPE="bookingFor";
		String REQUEST_FOR="requestFor";
		String REQUEST_DATE="requestdate";
		String REQUEST_STATUS="requestStatus";
		String REQUEST_CREATION_TIME="reqCreationTime";
		String Request_Modified_Time="requestModifiedTime";
		String Request_IP="requestIP";
		String REQUEST_ID="requestID";
		String Service="service";
		String EMPROLE="empRole";
		String UPN="upn";
		String SERVICE_LIST="serviceList";
		
		String [] REQUEST={LOCATION,REQUESTER,REQUEST_TYPE,REQUEST_FOR,REQUEST_DATE,REQUEST_CREATION_TIME,
				       Request_Modified_Time,Request_IP,Service,EMPROLE,UPN,SERVICE_LIST};



		public interface ProductKey{
			
			String NAME="name";
			String BARND="brand";
			String UNITS="units";
			String PRICE="price";
			String MASTERCATEGARY="mastercategary";
			String SUBCATEGARY="subcategary";
			String CATEGARY ="categary";
			String PACKSIZE="packsize";
			String PRODUCTID="productId";
			String QUANTITY="quantity";
			String TYPE="type";
			String STATUS="status";
			String ORDER_QUANTITY="orderQuantity";
			String VENDORID="vendorId";
			String RECIEVE_DATE="reieveDate";
			String PONUMBER="poNumber";
			
			
	
		}
		
}
