package com.admin4u.util.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData.Action;
import com.admin4u.util.service.ApplicationMetaData.ProcessType;


@RequestScoped
public abstract class ProcessHelper implements ProcessListener{
	
	
	private  List<String> approvalProcess;
	
	private List<String> nonApprovalProcess;
	
	protected ProcessType processType;
	
	protected Properties processProp; 
	
	
    public ProcessHelper() {

    	 processProp= new Properties();
		
		try {
			this.processProp.load(ProcessListener.class.getClassLoader().getResourceAsStream("processfsm.properties"));
			
			approvalProcess=Arrays.asList(processProp.getProperty(ProcessType.APPROVALPROCESS.toString()).split("\\."));
			
			nonApprovalProcess=Arrays.asList(processProp.getProperty(ProcessType.NONAPPROVALPROCESS.toString()).split("\\."));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(approvalProcess+"     "+nonApprovalProcess);
		
	}	
	
	public ProcessType getProcessType(Services service)
	{
           
		if(approvalProcess.contains(service.toString()))
			return  processType= ProcessType.APPROVALPROCESS;
		else 
			if(nonApprovalProcess.contains(service.toString()))
				return processType=ProcessType.NONAPPROVALPROCESS;
		return null;
	}
	
	public void setProcessType(ProcessType processType)
	{
		this.processType =processType;
	}

	@Override
	public List<String> getApprovalProcess() {
		System.out.println("approvaloprocess : "+approvalProcess);
		return approvalProcess;
	}

	@Override
	public List<String> getNonApprovalProcess() {
		System.out.println("nonapprovaloprocess : "+nonApprovalProcess);
		return nonApprovalProcess;
	}

	

}
