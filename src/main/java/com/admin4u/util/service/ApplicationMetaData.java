package com.admin4u.util.service;

public interface ApplicationMetaData {
	
	public enum Action {
		
		GENERATED,
		
		APPROVAL_AUTHORITY_ACCEPT,
		APPROVAL_AUTHORITY_REJECT,
		
		ADMIN_COMPLETE,
		ADMIN_REJECT,
		ADMIN_SERVE,
		ADMIN_FORWORD,
		ADMIN_CLOSE,
		ADMIN_ALLOCATE,
		ADMIN_APPROVE,
		ADMIN_EXTEND,
		
		SPOC_EXTEND,
		SPOC_REJECT,
		SPOC_CLOSE,
		SPOC_SERVE,
		SPOC_COMPLETE,
		SPOC_ALLOCATE,
		SPOC_APPROVE,
		;
	}
	
	public enum Status{
		ESCLATED,
		COMPLETE,
		PENDING,
		CLOSED,
		APPROVED,
		REJECTED,
		CANCELED,
		AMENDED,
		EXPIRE,
		CANCELLED_BY_USER,
		ADMIN_GENRATED,
		REOPEN,
		TIME_UP,
		SEND,
		AUTO_APPROVED,
		EXTENDED
		;
	}
	
	public enum ProcessType{
		APPROVALPROCESS,
		NONAPPROVALPROCESS,
		SKIPAPPROVAL;
	}
	
	public enum TatLevelKeys{
		level1Designation,
		level2Designation,
		level3Designation,
		level4Designation,
		levelFinalDesignation,
		level1TatTime,
		level2TatTime,
		level3TatTime,
		level4TatTime,
		levelFinalTatTime,
		levelFinal,
		
		;
	}
	
	
	public interface Google {
		String OauthURL = "https://accounts.google.com/o/oauth2/auth?client_id=820070288819-b2ikdb1gc0s4gp1vc5lrhpqsguivgeei.apps.googleusercontent.com&response_type=code&scope=email&redirect_uri=http://server.turningideas.com:8180/admin4u/Oauth2callback&access_type=online&approval_prompt=auto";
		String OauthCallbackURL = "/Oauth2callback";
	}

}
