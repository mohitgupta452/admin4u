package com.admin4u.util.convertor;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Converter(autoApply=true)
public class StringToJsonConvertor implements AttributeConverter<JsonObject,String>{
	
	@Override
	public String convertToDatabaseColumn(JsonObject jsonObject) {
	
		return	jsonObject.toString();
	
	}

	@Override
	public JsonObject convertToEntityAttribute(String str) {
		return new Gson().fromJson(str,JsonObject.class);
	}

}
