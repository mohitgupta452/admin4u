package com.admin4u.util.enums;

public enum ApplicationDropDowns {
	cost_centre,
	booking_for,
	booking_type,
	guest_type,
	hotel_location,
	avail_cab,
	department,
	nature_of_work,
	source,
	destination,
	company_name,
	preferred_time_slot,
	start_point,
	end_point,
	travel_class,
	cab_preference,
	airport_list,
	electrical,
	hk_natureofwork,
	plumbing,
	civil,
	carpentery,
	hvca;
}
