package com.admin4u.util.enums;


public enum Services {
  
	AIRTICKET("Air Ticket"),
    TRAINTICKET("Train Ticket"),
    CABPICK ("Pick up only"),
    CABDROP ("Drop Only"),
    CABDISPOSAL ("Cab at disposal"),
    CABAIRPORT ("Airport Transfer"),
    CAB("Cab"),
    HOTEL_BOOKING ("Hotel Booking"),
    GUEST_HOUSE_BOOKING ("Guest House Booking"),
    HOUSEKEEPING ("House Keeping"), 
    REPAIRMAINTENANCE("Repair And Maintenance"),
	ELECTRICAL_SERVICES ("Electrical Services"),
	SPACEMANAGEMENT ("Space Management"),
	MEETINGROOMMANAGEMENT ("Meeting Room"),
	INVENTORY ("Inventory"),
	HOTEL("Hotel");
	
	private String displayName;
	
	private Services(String display)
	{
		this.displayName= display;
	}
	
	public String getDisplayName()
	{
	 return	this.displayName;
		
	}

 }