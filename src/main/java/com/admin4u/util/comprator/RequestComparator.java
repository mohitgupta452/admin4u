package com.admin4u.util.comprator;

import java.util.Comparator;
import java.util.Map;

public class RequestComparator implements Comparator<Map<String,Object>>{

	public RequestComparator() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public int compare(Map<String,Object> r1,Map<String,Object> r2){
		
		Integer requestID1=(int)r1.get("requestID");
		Integer requestID2=(int)r2.get("requestID");
		
		return requestID1.compareTo(requestID2);
	}

}
