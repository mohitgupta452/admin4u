package com.admin4u.auth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.pojo.GooglePojo;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@WebServlet("/Oauth2callback")
public class Oauth2Callback extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private IDMService idmService;
	
	@Inject
	private RoleManager roleManager;
	
	@Inject
	private UserSessionBean userSession;
	
	Logger log = LoggerFactory.getLogger(getClass());

	public Oauth2Callback() {
		super();

	}

	private void createSession(HttpServletRequest request, HttpServletResponse response, Map<String, String> userInfo) {
		HttpSession session = request.getSession(true);

		session.setAttribute("upn", userInfo.get("upn"));
		session.setAttribute("name", userInfo.get("name"));
		session.setAttribute("department", userInfo.get("department"));
		userSession.setEmpRole(roleManager.getEmpRoleByUPN(userInfo.get("upn")));
		userSession.setEmpData(idmService.getEmpByUpn(userInfo.get("upn")));
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		log.debug("Entering doGet Method.{}",request.getParameterMap());

		try {
			// get code
			String code = request.getParameter("code");
			// format parameters to post
			String urlParameters = String.format("code=%s&client_id=820070288819-b2ikdb1gc0s4gp1vc5lrhpqsguivgeei.apps.googleusercontent.com&client_secret=wIxKCk2oqcVtKbhm1vKRblj5&redirect_uri=http://server.turningideas.com:8180/admin4u/Oauth2callback&grant_type=authorization_code",
					URLEncoder.encode(code, "UTF-8"));

			// post parameters
			log.debug("Sending Google Oauth Request");
			long startTime = System.currentTimeMillis();

			URL url = new URL("https://accounts.google.com/o/oauth2/token");
			URLConnection urlConn = url.openConnection();
			urlConn.setDoOutput(true);

			urlConn.setRequestProperty("Accept-Charset", "UTF-8");
			urlConn.setRequestProperty("USER-AGENT",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
			urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

			try (OutputStreamWriter writer = new OutputStreamWriter(urlConn.getOutputStream())) {
				writer.write(urlParameters);
				writer.flush();
			}

			log.debug("Response Time is:{}", System.currentTimeMillis() - startTime);

			// get output in outputString
			String line, outputString = "";

			try (BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()))) {
				while ((line = reader.readLine()) != null) {
					outputString += line;
				}
			}

			log.debug(outputString);

			// get Access Token
			JsonObject json = (JsonObject) new JsonParser().parse(outputString);
			String access_token = json.get("access_token").getAsString();

			log.debug(access_token);

			// get User Info
			startTime = System.currentTimeMillis();
			log.debug("Sending Request for user Identity");
			url = new URL("https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + access_token);
			urlConn = url.openConnection();

			urlConn.setRequestProperty("Accept-Charset", "UTF-8");
			urlConn.setRequestProperty("USER-AGENT",
					"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

			outputString = "";

			try (BufferedReader reader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()))) {
				while ((line = reader.readLine()) != null) {
					outputString += line;
				}
			}

			log.debug("Response Time is {}", System.currentTimeMillis() - startTime);

			log.debug(outputString);

			// Convert JSON response into Pojo class
			GooglePojo data = new Gson().fromJson(outputString, GooglePojo.class);

			// identify user and manage session

			log.debug(data.toString());

			// getting data From GooglePojo to Map
			/*Map<String, String> userData = new HashMap<String, String>();
			userData.put("firstName", data.getGiven_name());
			userData.put("lastName", data.getFamily_name());
			userData.put("name", data.getName());
			userData.put("emailId", data.getEmail());
			userData.put("userLoginId", data.getEmail());
			userData.put("registeredUsing", "google");*/

			
			// password null for users redirected from googleloginId
			Map<String,String> result =idmService.login(data.getEmail()); 
			log.debug("{}",result);

			if (result.get("responseMsg").equals("login failed")) {
				
				response.sendRedirect("login.jsp");
					log.error("Unable to loggin", data);
				}
				if (result.get("responseMsg").equals("success")) {
					log.debug("User logged in.{}",data);
					createSession(request, response, result);
					response.sendRedirect("index.jsp");
				}
				return;

		} catch (ProtocolException | MalformedURLException e) {
			log.error(e.getMessage());
		}
		log.debug("leaving doGet");
	}

}