package com.admin4u.service.bussiness.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.EmployeeRoleServiceMaster;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.Services;

@Transactional
@Stateless
public class IDMServiceImpl implements IDMService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private CrudService crudService;

	@Inject
	private RoleManager roleManager;

	@Inject
	private LocationManger locationManger;

	@PersistenceContext(unitName = "admin4u")
	private EntityManager entityManager;

	@Override
	public List<EmployeeData> getAllEmployeeData() {
		return crudService.findWithNamedQuery("EmployeeData.findAll");
	}

	@Override
	public EmployeeData getEmpByUpn(String upn) {
		@SuppressWarnings("unchecked")
		List<EmployeeData> employeeDataList = crudService.findWithNamedQuery("EmployeeData.findByEmpByUpn",
				QueryParameter.with("upn", upn).parameters());
		if (!employeeDataList.isEmpty()) {
			EmployeeData employeeData = employeeDataList.get(0);
			return employeeData;
		}
		return null;
	}

	@Override
	public EmployeeData getEmpByMail(String mail) {
		@SuppressWarnings("unchecked")
		List<EmployeeData> employeeDataList = crudService.findWithNamedQuery("EmployeeData.findByEmpByMail",
				QueryParameter.with("mail", mail).parameters());
		if (!employeeDataList.isEmpty() && employeeDataList.size() > 0) {
			EmployeeData employeeData = employeeDataList.get(0);
			return employeeData;
		}
		return null;
	}

	@Override
	public String assignServiceToEmp(String upn, Services service) {
		EmployeeRole employeeRole = roleManager.getEmpRoleByUpn(upn).get(0);
		List<EmployeeRoleServiceMaster> empRoleServiceMaster = employeeRole.getEmployeeRoleServicemaster();
		for (EmployeeRoleServiceMaster empRoleService : empRoleServiceMaster) {
			if (Services.valueOf(empRoleService.getService()).equals(service.toString()))
				return "service already mapped to this role";
		}
		EmployeeRoleServiceMaster empRoleService = new EmployeeRoleServiceMaster();

		empRoleService.setService(service.toString());
		empRoleService.setEmployeeRole(employeeRole);
		empRoleService.setCreationTime(new Date());
		empRoleService.setActive(true);

		crudService.create(empRoleService);

		return "service assigned";
	}

	@Override
	public Map<String, String> login(String loginId) {
		Map<String, String> resultMap = new HashMap<>();
		List<EmployeeData> empDatalist = crudService.findWithNamedQuery("EmployeeData.findByEmpByUpn",
				QueryParameter.with("upn", loginId).parameters());
		if (!empDatalist.isEmpty()) {
			EmployeeData empData = empDatalist.get(0);
			resultMap.put("responseMsg", "success");
			resultMap.put("name", empData.getDisplayName());
			resultMap.put("employeeId", empData.getEmployeeID());
			resultMap.put("department", empData.getDepartment());
			resultMap.put("company", empData.getCompany());
			resultMap.put("upn", empData.getUserPrincipalName());
			return resultMap;
		}
		resultMap.put("responseMsg", "login failed");
		return resultMap;
	}

	@Override

	public String getEmpNameByUpn(String upn) {
		if (upn != null)
			return this.getEmpByUpn(upn).getDisplayName() != null ? this.getEmpByUpn(upn).getDisplayName() : "N/A";
		return "n/a";
	}

	@Override
	public String getEmployeeUPNByEmployeeId(String employeeId) {

		List<String> employeeUPNList = crudService.findWithNamedQuery("EmployeeData.findByEmployeeId",
				QueryParameter.with("employeeId", employeeId).parameters());
		String employeeUPN = employeeUPNList.get(0);
		return employeeUPN;
	}

	@Override
	public String getEmployeeNameByEmployeeId(String employeeId) {

		List<String> employeeNameList = crudService.findWithNamedQuery("EmployeeData.findNameByEmployeeId",
				QueryParameter.with("employeeId", employeeId).parameters());
		if (!employeeNameList.isEmpty()) {
			String employeeName = employeeNameList.get(0);
			return employeeName;

		}
		return "Invalid employee ID";
	}

	@Override
	public EmployeeData getEmployeeDataByEmployeeId(String employeeId) {

		List<EmployeeData> employeeDataList = crudService.findWithNamedQuery("EmployeeData.findEmpByEmployeeId",
				QueryParameter.with("employeeId", employeeId).parameters());
		if (!employeeDataList.isEmpty()) {
			EmployeeData employeeData = employeeDataList.get(0);
			return employeeData;

		}
		return null;
	}

	public boolean isAuthenticated(String mail) {
		log.debug("mail id for authentication : {}", mail);

		if (mail != null) {
			EmployeeData employeeData = this.getEmpByMail(mail);

			if (employeeData != null && employeeData.getL() != null && employeeData.getDisplayName() != null
					&& employeeData.getUserPrincipalName() != null
					&& locationManger.getParentLocation(employeeData.getL()) != null) {
				log.debug("Employee {} with upn {} has been authenticated for admin4u", employeeData.getDisplayName(),
						employeeData.getUserPrincipalName());
				return true;
			}
		}

		log.debug("Admin4u authentication has been failed for email : {} ", mail);
		return false;

	}

	public boolean isOtherSpoc(String upn) {
		
		Query query=  entityManager.createNativeQuery("select count(u.actionId) FROM user_action u WHERE u.ownerUPN='" + upn
				+ "' AND u.ownerRole='" + Roles.OTHERSPOC.toString() + "'");
		   BigInteger actionCount=(BigInteger) query.getSingleResult();
		
		if (actionCount.intValue() >0)
			return true;
		return false;
	}

}
