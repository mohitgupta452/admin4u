package com.admin4u.service.bussiness.impl;


import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import com.admin4u.persistence.entity.ScheduledEmail;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.dto.MailDTO;
import com.admin4u.service.bussiness.contract.MailManager;

/**
 * Author : Ashish Singh Dev
 */
@Stateless
public class MailManagerImpl implements MailManager {

	@Inject
	private Logger log;

	@Inject
	private CrudService crudService;

	public MailManagerImpl() {
	}

	private void sendEmail(List<ScheduledEmail> emails) {

		Properties props = new Properties();

		props.put("mail.smtp.host", "192.168.17.238");
		props.put("mail.smtp.auth", "false");
		props.put("mail.debug", "false");
		props.put("mail.smtp.port", "25");
		// Put below to false, if no https is needed
		props.put("mail.smtp.starttls.enable", "false");

		/*
		 * Session session = Session.getInstance(props, new Authenticator() {
		 *
		 * @Override protected PasswordAuthentication
		 * getPasswordAuthentication() { return new
		 * PasswordAuthentication("amsprojectteam@dalmiabharat.com",
		 * "welcome123!@"); } });
		 */
		Session session = Session.getInstance(props);
		Transport transport = null;

		for (ScheduledEmail mail : emails) {
			try {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(
						new InternetAddress("noreply@dalmiabharat.com"));

				for (String to : splitAndTrim(mail.getMailto())) {
					message.addRecipient(Message.RecipientType.TO,
							new InternetAddress(to));
				}

				if (mail.getMailcc() != null) {
					for (String cc : splitAndTrim(mail.getMailcc())) {
						message.addRecipient(Message.RecipientType.CC,
								new InternetAddress(cc));
					}
				}
				if (mail.getMailbcc() != null) {
					for (String bcc : splitAndTrim(mail.getMailbcc())) {
						message.addRecipient(Message.RecipientType.CC,
								new InternetAddress(bcc));
					}
				}

				message.setSubject(mail.getMailsubject());

				if (mail.getAttachments() != null
						&& !mail.getAttachments().isEmpty()) {

					Multipart multipart = new MimeMultipart("related");

					MimeBodyPart messageBodyPart = new MimeBodyPart();
					messageBodyPart.setContent(mail.getMailbody(), "text/html");
					// messageBodyPart.setHeader("Content-Type", "\"text/html;
					// charset=\"UTF-8\";");
					// add mail body part to maild body
					multipart.addBodyPart(messageBodyPart);

					for (String attachmentPath : splitAndTrim(
							mail.getAttachments())) {

						try {
							MimeBodyPart fileBodyPart = new MimeBodyPart();

							File newFile = new File(attachmentPath);
							((MimeBodyPart) fileBodyPart).attachFile(newFile);

							String contentType = Files
									.probeContentType(newFile.toPath());
							fileBodyPart.setHeader("Content-Type", "\""
									+ contentType + "; charset=\"UTF-8\";");
							fileBodyPart.setFileName(newFile.getName());
							fileBodyPart.setDisposition(MimeBodyPart.INLINE);
							multipart.addBodyPart(fileBodyPart);
						} catch (IOException e) {
							e.printStackTrace();
						}

					}
					message.setContent(multipart, "multipart/mixed");

				} else {
					message.setContent(mail.getMailbody(),
							"text/html; charset=utf-8");
				}
				Transport.send(message);

				// Update mail Status and processed Time
				mail.setStatus(0);
				mail.setProcessedTime(new Date());
				crudService.update(mail);

			} catch (MessagingException e) {
				//e.printStackTrace();
				log.error("email fails" + e.getMessage());
			} finally {
				if (transport != null) {
					try {
						transport.close();
					} catch (MessagingException e) {
						//e.printStackTrace();
						log.error("Unable to close Mail Transport Connection.Due to: "+e.getMessage());
					}
				}
			}
		}

	}

	/**
	 * Description: Wrapper method for asynchronously send emails. Fire and
	 * Forget. Since it doesn't return Future<V> interface.
	 *
	 * @param emails
	 */
	@Asynchronous
	public void sendEmailAsync(List<ScheduledEmail> emails) {
		sendEmail(emails);
	}

	private List<String> splitAndTrim(String string) {
		List<String> trimmedList = new ArrayList<>();

		if (string == null || string.isEmpty()) {
			return trimmedList;
		}

		for (String toTrim : string.split(";")) {
			String trimmed = toTrim.trim();
			if (trimmed == null || trimmed.isEmpty()) {
				continue;
			}
			trimmedList.add(trimmed);
		}
		return trimmedList;
	}

	@Override
	public void sendEmail(MailEvents event, boolean sendAsynchronously) {

		InetAddress ip;
		try {

			ip = InetAddress.getLocalHost();
			//// System.out.println("Current IP address : " +
			//// ip.getHostAddress());

			if (ip.getHostAddress().contains("192.168.16.189")) {
				@SuppressWarnings({"unchecked"})
				List<ScheduledEmail> foundEmails = crudService
						.findWithNamedQuery(
								"ScheduledEmail.find_By_EmailEvent_AND_Status",
								QueryParameter
										.with("emailEvent", event.toString())
										.and("status", 1).parameters());

				if (sendAsynchronously) {
					sendEmailAsync(foundEmails);
				} else {
					sendEmail(foundEmails);
				}
			} else {
				log.error("mail ip is not please use ip 192.168.16.189");
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();

		}

	}

	@Override
	public void sendEmail(boolean sendAsynchronously) {
		InetAddress ip;
		try {

			ip = InetAddress.getLocalHost();

			if (ip.getHostAddress().contains("192.168.16.189")) {
				@SuppressWarnings({"unchecked"})
				List<ScheduledEmail> foundEmails = crudService
						.findWithNamedQuery("ScheduledEmail.find_By_Status",
								QueryParameter.with("status", 1).parameters());

				if (sendAsynchronously) {
					sendEmailAsync(foundEmails);
				} else {
					sendEmail(foundEmails);
				}
			} else {
				log.error("mail ip is not please use ip 192.168.16.189");
			}

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void saveEmail(MailDTO mailDTO) {
		crudService.create(convert(mailDTO, true));
	}

	private ScheduledEmail convert(MailDTO mailDTO,
			boolean isConvertForSaving) {

		ScheduledEmail scheduledEmail = new ScheduledEmail();

		scheduledEmail.setMailto(mailDTO.getTo());
		scheduledEmail.setMailcc(mailDTO.getCc());
		scheduledEmail.setMailbcc(mailDTO.getBcc());
		scheduledEmail.setMailsubject(mailDTO.getSubject());
		scheduledEmail.setMailbody(mailDTO.getBody());
	//	scheduledEmail.setEmailevent(mailDTO.getEvent() != null ? mailDTO.getEvent().toString() : "N.A");

		StringBuilder attachmentString = new StringBuilder();

		Set<String> attachmentPathList = mailDTO.getAttachments();

		for (String attachmentPath : attachmentPathList) {
			attachmentString.append(attachmentPath + ";");
		}

		scheduledEmail.setAttachments(attachmentString.toString());

		if (isConvertForSaving) {
			scheduledEmail.setStatus(1);
			scheduledEmail.setGenerationTime(new Date());
		} else {
			scheduledEmail.setStatus(0);
		}

		return scheduledEmail;
	}

	private MailDTO convert(ScheduledEmail mail) {

		MailDTO mailDTO = new MailDTO();

		mailDTO.setTo(mail.getMailto());
		mailDTO.setCc(mail.getMailcc());
		mailDTO.setBcc(mail.getMailbcc());
		mailDTO.setSubject(mail.getMailsubject());
		mailDTO.setBody(mail.getMailbody());

		List<String> attachments = Arrays
				.asList(mail.getAttachments().split(";"));

		for (String attachmentPath : attachments) {
			mailDTO.getAttachments().add(attachmentPath.trim());
		}

		return mailDTO;
	}

}
