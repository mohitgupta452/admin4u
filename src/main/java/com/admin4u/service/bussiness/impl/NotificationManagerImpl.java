package com.admin4u.service.bussiness.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.management.Notification;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.dto.MailDTO;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.EventTracker;
import com.admin4u.persistence.entity.NotificationMaster;
import com.admin4u.persistence.entity.PriorityTatMap;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.MailManager;
import com.admin4u.service.bussiness.contract.NotificationManager;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.AllocationStatus;
import com.admin4u.util.enums.Event;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.ApplicationMetaData.Action;
import com.admin4u.util.service.ApplicationMetaData.Status;
import com.admin4u.util.service.ProcessListener;
import com.admin4u.views.ResponsePDFBean;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.java.AlertMessages;

@Stateless
@Transactional
public class NotificationManagerImpl implements NotificationManager {

	@Inject
	private CrudService crudService;

	@Inject
	private RoleManager roleManager;

	@Inject
	private ADStore adStore;

	@Inject
	private UserSessionBean userSession;

	@Inject
	private ProcessListener processListener;

	@Inject
	private MailManager mailManager;

	@Inject
	private ResponsePDFBean responsePDFBean;

	@Inject
	private IDMService idmService;
	
	@PersistenceContext(unitName = "admin4u")
	private EntityManager entityManager;

	private Logger log = LoggerFactory.getLogger(getClass());

	public void setNotificationByAction(Map<String, String> notificationMap, UserActionMaster userAction,
			RequestMaster requestMaster) {

		log.debug("notification MAp {}", notificationMap);
		for (Entry<String, String> entry : notificationMap.entrySet()) {

			if (entry.getKey().equals("REQUESTER")) {
				NotificationMaster notification = new NotificationMaster();
				notification.setCreationTime(new Date());
				notification.setSender(userSession.getEmpData().getUserPrincipalName());
				notification.setUserActionMaster(userAction);
				notification.setMessage(String.format(entry.getValue(), requestMaster.getWorkorder() != null
						? requestMaster.getWorkorder() : String.valueOf(requestMaster.getRequestId())));
				notification.setReciever(userAction.getRequestMaster().getRequestBy());
				notification.setRequestMaster(requestMaster);
				notification.setRecieverRole(entry.getKey());
				crudService.create(notification);
			} else if (entry.getKey().equals(Roles.APPROVAL_AUTHORITY.toString())) {
				NotificationMaster notification = new NotificationMaster();
				notification.setCreationTime(new Date());
				notification.setSender(userSession.getEmpData().getUserPrincipalName());
				notification.setUserActionMaster(userAction);
				notification.setMessage(String.format(entry.getValue(), requestMaster.getWorkorder() != null
						? requestMaster.getWorkorder() : String.valueOf(requestMaster.getRequestId())));
				notification.setReciever(
						adStore.getManager(userAction.getRequestMaster().getRequestBy()).getUserPrincipalName());
				notification.setRecieverRole(entry.getKey());
				notification.setRequestMaster(requestMaster);
				crudService.create(notification);
			} else
				for (EmployeeRole employeeRole : roleManager.getEmpByRole(entry.getKey(), requestMaster.getLocation(),
						requestMaster.getService())) {
					NotificationMaster notification = new NotificationMaster();
					notification.setReciever(employeeRole.getUserPrincipalName());
					notification.setRecieverRole(entry.getKey());
					notification.setMessage(String.format(entry.getValue(), requestMaster.getWorkorder() != null
							? requestMaster.getWorkorder() : String.valueOf(requestMaster.getRequestId())));
					notification.setCreationTime(new Date());
					notification.setSender(userSession.getEmpData().getUserPrincipalName());
					notification.setUserActionMaster(userAction);
					notification.setRequestMaster(requestMaster);
					crudService.create(notification);
				}
		}
	}

	public void generateEmail(RequestMaster requestMaster, Map<String, String> mailMap,Action action) {
		log.debug("mail map {}", mailMap);

		Set<String> attachments = new HashSet<>();
		if (action.toString().contains("SERVE")) {

			attachments.addAll(Arrays.asList(requestMaster.getResponseMaster().getResponseAttachment().split("|")));

			attachments.add(responsePDFBean
					.generatePdf(new Gson().fromJson(requestMaster.getResponseMaster().getResponseData(), Map.class))
					.getAbsolutePath());

		}

		mailMap.forEach((mailTo, mail) -> {
			MailDTO mailDto = new MailDTO();
			switch (Roles.valueOf(mailTo)) {
			case ADMIN:

				mailDto.setTo("kaustubh@turningcloud.com");
				mailDto.setCc("kaustubh@turningcloud.com");
				mailDto.setBody(String.format(mail, requestMaster.getRequestBy(), requestMaster.getRequestBy(),
						requestMaster.getService(), requestMaster.getWorkorder() != null ? requestMaster.getWorkorder()
								: String.valueOf(requestMaster.getRequestId())));
				mailDto.setSubject("ADMIN4U Mail Trial");
				mailManager.saveEmail(mailDto);
				break;
			case APPLICATION_ADMIN:
				break;
			case APPROVAL_AUTHORITY:
				mailDto.setTo("kaustubh@turningcloud.com");
				mailDto.setCc("kaustubh@turningcloud.com");
				mailDto.setBody(
						String.format(mail, adStore.getManager(requestMaster.getRequestBy()).getUserPrincipalName(),
								idmService.getEmpNameByUpn(requestMaster.getRequestBy()), requestMaster.getService(),
								requestMaster.getWorkorder() != null ? requestMaster.getWorkorder()
										: String.valueOf(requestMaster.getRequestId())));
				mailDto.setSubject("ADMIN4U Mail Trial");
				mailManager.saveEmail(mailDto);
				break;
			case REQUESTER:
				mailDto.setTo("kaustubh@turningcloud.com");
				mailDto.setCc("kaustubh@turningcloud.com");
				mailDto.setBody(String.format(mail, idmService.getEmpNameByUpn(requestMaster.getRequestBy()),
						requestMaster.getWorkorder() != null ? requestMaster.getWorkorder()
								: String.valueOf(requestMaster.getRequestId()),requestMaster.getService()));
				mailDto.setSubject("ADMIN4U Mail Trial");
				mailDto.setAttachments(attachments);
				mailManager.saveEmail(mailDto);
				break;
			case SPOC:
				mailDto.setTo("kaustubh@turningcloud.com");
				mailDto.setCc("kaustubh@turningcloud.com");
				mailDto.setBody(String.format(mail, requestMaster.getRequestBy(), requestMaster.getRequestBy(),
						requestMaster.getService(), requestMaster.getWorkorder() != null ? requestMaster.getWorkorder()
								: String.valueOf(requestMaster.getRequestId())));
				mailDto.setSubject("ADMIN4U Mail Trial");
				mailManager.saveEmail(mailDto);
				break;
			default:
				break;
			}
		});
		mailManager.sendEmail(false);
	}

	public void setRequestEventTime(EventTracker eventTracker) {
		Event event = Event.valueOf(eventTracker.getEvent());

		processListener.getNotificationByEvent(event).forEach((Reciever, Msg) -> {

			NotificationMaster notificationMaster = new NotificationMaster();

			if (Reciever.equals("REQUESTER"))
				notificationMaster.setReciever(eventTracker.getRequestMaster().getRequestBy());
			else {
				RequestMaster requestMaster = eventTracker.getRequestMaster();
				for (UserActionMaster userActionMaster : requestMaster.getUserAction()) {
					if (userActionMaster.getStatus().equals(Status.PENDING.toString()))
						userActionMaster.setStatus(Status.EXPIRE.toString());
				}
			}
			notificationMaster.setMessage(Msg);
			notificationMaster = crudService.create(notificationMaster);
			log.debug("new notification generated  with id :  {}  msg : {}  requestId : {}",
					notificationMaster.getNotificationId(), notificationMaster.getMessage(),
					notificationMaster.getUserActionMaster().getRequestMaster().getRequestId());
		});
	}

	private List<String> convert(List<NotificationMaster> notificationList) {
		List<String> notification = new ArrayList<>();
		for (NotificationMaster notifyMaster : notificationList) {
			notification.add(notifyMaster.getMessage());
		}
		log.debug("notification List {}", notification);
		return notification;
	}

	@Override
	public List<String> getNotificationByUPN(String UPN) {
		@SuppressWarnings("unchecked")
		List<NotificationMaster> notificationList = crudService.findWithNamedQuery("NotificationMaster.findAllByUpn",
				QueryParameter.with("upn", UPN).parameters());
		return this.convert(notificationList);
	}

	@Override
	public List<NotificationMaster> getLimitedNotificationByUPN(String UPN, int limit) {
		@SuppressWarnings("unchecked")
		List<NotificationMaster> notificationList = crudService.findWithNamedQuery("NotificationMaster.findAllByUpn",
				QueryParameter.with("upn", UPN).parameters(), limit);

		notificationList.forEach(notification -> {
			if (Status.PENDING.toString().equals(notification.getStatus()) || notification.getStatus() == null) {
				notification.setStatus(Status.SEND.toString());
				entityManager.persist(notification);
			}
		});

		return notificationList;
	}

	@Override
	public List<NotificationMaster> getNotificationByUPN(String UPN, String status) {
		@SuppressWarnings("unchecked")
		List<NotificationMaster> notificationList = crudService.findWithNamedQuery("NotificationMaster.findAllByUpn",
				QueryParameter.with("upn", UPN).parameters());
		return notificationList;
	}

	public List<NotificationMaster> getCurrentNotification(String upn, Date lastTime) {
		return crudService.findWithNamedQuery("NotificationMaster.findByTime",
				QueryParameter.with("timeSlot", lastTime).and("currentTime", new Date()).and("upn", upn).parameters());

	}

	@Override
	public List<NotificationMaster> getNotificationByUPNStatus(String UPN, Status status) {
		@SuppressWarnings("unchecked")
		List<NotificationMaster> notificationList = crudService.findWithNamedQuery(
				"NotificationMaster.findAllByUpnStatus",
				QueryParameter.with("upn", UPN).and("status", status.toString()).parameters());

		if (status.toString().equals(Status.PENDING.toString()))
			notificationList.forEach(notification -> {
				notification.setStatus(Status.SEND.toString());
				entityManager.persist(notification);
			});

		return notificationList;
	}

	private NotificationMaster getNotificationById(int id) {
		return crudService.find(NotificationMaster.class, id);

	}

	public String markAsRead(List<Integer> notificationsId) {

		JsonArray notifyArray = new JsonArray();

		notificationsId.forEach(id -> {
			try {

				NotificationMaster notificationMaster = entityManager.getReference(NotificationMaster.class, id);

				notificationMaster.setRead(true);

				entityManager.persist(notificationMaster);

				notifyArray.add(id);

			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		return notifyArray.toString();
	}

	/*
	 * 
	 * TAT section not discussed yet.
	 * 
	 * 
	 * @Override public long getTATTimeByPriority(String priority) {
	 * List<PriorityTatMap> priorityTatMap =
	 * crudService.findWithNamedQuery("PriorityTatMap.findByPriority",
	 * QueryParameter.with("priority", priority).parameters()); long tat = 0;
	 * for (PriorityTatMap ptm : priorityTatMap) tat = ptm.getTurnAroundTime();
	 * return tat;
	 * 
	 * }
	 * 
	 * 
	 * @Override public void genrateTatNotifications(String role) {
	 * 
	 * List<Map<String, Object>> reqMap =
	 * this.getRequestForTatNotification(role); List<NotificationMaster>
	 * notificationList = new ArrayList<NotificationMaster>(); long tat = 0; for
	 * (Map<String, Object> map : reqMap) { if (map.get("priority") != null) {
	 * tat = this.getTATTimeByPriority(map.get("priority").toString()); Date
	 * requestDate = new Date(); try { DateFormat dateFormat = new
	 * SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); requestDate =
	 * dateFormat.parse(map.get("requestdate").toString()); long
	 * currentTimeStamp = new Date().getTime();
	 * 
	 * long tatExpTime = requestDate.getTime() + tat;
	 * 
	 * boolean isTatExpired = false;
	 * 
	 * if (currentTimeStamp > tatExpTime) { isTatExpired = true; }
	 * 
	 * if (map.get("requestStatus").toString().equals("PENDING") &&
	 * isTatExpired) { String owner = map.get("owner").toString(); //
	 * List<NotificationMaster> // nmList=crudService.findWithNamedQuery(
	 * "NotificationMaster.findByRequestIDAndUpn",QueryParameter.with(
	 * "requestID",map.get("requestID")).and("upn", // owner).parameters());
	 * 
	 * // if(nmList.isEmpty()) { NotificationMaster notification = new
	 * NotificationMaster(); String message = "requestID" +
	 * map.get("requestID").toString() + " Tat time is Expired";
	 * notification.setMessage(message); notification.setCreationTime(new
	 * Date()); // notification.setUserAction((int)map.get("requestID"));
	 * notification.setReciever(owner); crudService.create(notification); } }
	 * 
	 * }
	 * 
	 * catch (Exception ex) {
	 * 
	 * log.debug("Exception in ", ex.getMessage()); }
	 * 
	 * }
	 * 
	 * } log.debug("notification list contains", notificationList);
	 * 
	 * }
	 * 
	 * 
	 * 
	 * public List<PriorityTatMap> getAllTats() {
	 * 
	 * return crudService.findWithNamedQuery("PriorityTatMap.findAll");
	 * 
	 * }
	 * 
	 * public String updateTatValue(PriorityTatMap ptm) {
	 * 
	 * PriorityTatMap newPtm = crudService.find(PriorityTatMap.class,
	 * ptm.getId()); newPtm.setPriority(ptm.getPriority());
	 * newPtm.setTurnAroundTime(ptm.getTurnAroundTime());
	 * 
	 * PriorityTatMap updatedPtm = crudService.update(newPtm);
	 * 
	 * Map<String, Object> responseMap = new HashMap<String, Object>();
	 * responseMap.put("id", updatedPtm.getId()); responseMap.put("priority",
	 * updatedPtm.getPriority()); responseMap.put("tat",
	 * updatedPtm.getTurnAroundTime());
	 * 
	 * return new Gson().toJson(responseMap); }
	 */
	public String addNewPriority(PriorityTatMap ptm) {

		PriorityTatMap updatedPtm = crudService.create(ptm);

		Map<String, Object> responseMap = new HashMap<String, Object>();
		responseMap.put("id", updatedPtm.getId());
		responseMap.put("priority", updatedPtm.getPriority());
		responseMap.put("tat", updatedPtm.getTurnAroundTime());

		return new Gson().toJson(responseMap);
	}

	public void setSMUnrespondedAlertNotification(Map<String, String> notificationMap, UserActionMaster userAction) {
		NotificationMaster notification;
		for (Entry<String, String> entry : notificationMap.entrySet()) {

			if (entry.getKey().equals(Roles.APPROVAL_AUTHORITY.toString())) {
				notification = new NotificationMaster();
				notification.setMessage(String.format(entry.getValue(), userAction.getRequestMaster().getRequestId()));
				notification.setReciever(
						adStore.getManager(userAction.getRequestMaster().getRequestBy()).getUserPrincipalName());
				notification.setCreationTime(new Date());
				notification.setSender("SystemAlertManager");
				notification.setUserActionMaster(userAction);
				crudService.create(notification);
			} else if (entry.getKey().equals(Roles.SM_REPORTING_MANAGER2.toString())) {
				notification = new NotificationMaster();
				notification.setMessage(String.format(entry.getValue(), userAction.getRequestMaster().getRequestId()));
				String reportingManagerUpn = adStore.getManager(userAction.getRequestMaster().getRequestBy())
						.getUserPrincipalName();
				notification.setReciever(adStore.getManager(reportingManagerUpn).getUserPrincipalName());
				notification.setCreationTime(new Date());
				notification.setSender("SystemAlertManager");
				notification.setUserActionMaster(userAction);
				crudService.create(notification);
			} else
				for (EmployeeRole employeeRole : roleManager.getEmpByRole(entry.getKey(),
						userAction.getRequestMaster().getLocation(), userAction.getRequestMaster().getService())) {
					notification = new NotificationMaster();
					notification.setUserActionMaster(userAction);
					notification.setCreationTime(new Date());
					notification
							.setMessage(String.format(entry.getValue(), userAction.getRequestMaster().getRequestId()));
					notification.setReciever(employeeRole.getUserPrincipalName());
					notification.setSender("SystemAlertManager");

					crudService.create(notification);
				}

		}
	}

	public boolean processSpaceManagementSystemNotification(RequestMaster requestMaster, EventTracker requestEvent) {

		if (ApplicationMetaData.Status.PENDING.toString().equalsIgnoreCase(requestMaster.getRequestStatus())) {

			Map<String, String> notificationMap = new HashMap<>();
			List<UserActionMaster> userAction = crudService.findWithNamedQuery("UserActionMaster.findByRequestMaster",
					QueryParameter.with("requestMaster", requestMaster).parameters());
			if (Event.ALERT_SM_SPOC.toString().equalsIgnoreCase(requestEvent.getEvent())) {
				notificationMap.put(Roles.SPOC.toString(), AlertMessages.smNotResponded + "spoc");
				this.setSMUnrespondedAlertNotification(notificationMap, userAction.get(0));
			} else if (Event.ALERT_SM_SPOC_REPORTINGMGR.toString().equalsIgnoreCase(requestEvent.getEvent())) {
				notificationMap.put(Roles.APPROVAL_AUTHORITY.toString(), AlertMessages.smNotResponded);
				this.setSMUnrespondedAlertNotification(notificationMap, userAction.get(0));
			} else if (Event.ALERT_SM_SPOC_REPORTINGMGR_REPORTINGMGR.toString()
					.equalsIgnoreCase(requestEvent.getEvent())) {
				notificationMap.put(Roles.SM_REPORTING_MANAGER2.toString(), AlertMessages.smNotResponded);
				this.setSMUnrespondedAlertNotification(notificationMap, userAction.get(0));
			}
		}

		return true;
	}

}
