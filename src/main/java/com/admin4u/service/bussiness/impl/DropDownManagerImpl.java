package com.admin4u.service.bussiness.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.DeptCostCenterMap;
import com.admin4u.persistence.entity.DropDowns;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.Location;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.service.bussiness.contract.DropDownManager;


@Stateless
@Transactional
public class DropDownManagerImpl implements DropDownManager{
	
	Logger log= LoggerFactory.getLogger(getClass());
	
	@Inject
	CrudService crudService;
@Override	
public DropDowns getDropDownDataByName(String dropDownName){
	
	List<DropDowns> listDD=crudService.findWithNamedQuery("DropDowns.findByDropDownName",QueryParameter.with("fieldName", dropDownName).parameters());
	DropDowns dropDownObject=null;
	for(DropDowns dd:listDD )
	{
		dropDownObject=dd;
	}
	
	return dropDownObject;
}

@Override	
public DropDowns getDropDownDataByID(int dropDownID){
	
	return crudService.find(DropDowns.class, dropDownID);
	
}

@Override
public Map<String,String> getKeyValueMapForExcel(String dropDownName){
	
	Map<String,String> keyValueMapForExcel=new HashMap<>();
	DropDowns dropDown=getDropDownDataByName(dropDownName);
	if(dropDown != null && dropDown.getData() != null)
	{
	StringTokenizer stringTokenizer=new StringTokenizer(dropDown.getData(), "|");
	int cellNumber=3;
	while(stringTokenizer.hasMoreTokens())
	{
		keyValueMapForExcel.put("A"+cellNumber,stringTokenizer.nextToken());
		cellNumber++;
	}
	}
	return keyValueMapForExcel;
	
}

@Override
public boolean updateDropDownData(String dropDownName,String data){
	
	List <DropDowns>ls = crudService.findWithNamedQuery("DropDowns.findByDropDownName",QueryParameter.with("fieldName", dropDownName).parameters());
	
	if(ls.isEmpty())
	{
		DropDowns newDropDown=new DropDowns();
		newDropDown.setFieldName(dropDownName);
		newDropDown.setData(data);
		crudService.create(newDropDown);
	}
	else{
		for(DropDowns dropDown:ls)
		{
			dropDown.setData(data);
			crudService.update(dropDown);
		}
	}
	return true;
	
}

@Override
public List<String> getDropDownValuesListByName(String dropDownName){
	
	List<String> dropDownValues=new ArrayList<String>();
	DropDowns dropDown=getDropDownDataByName(dropDownName);
	   if(dropDown != null && dropDown.getData() != null)
	   {
	StringTokenizer stringTokenizer=new StringTokenizer(dropDown.getData(), "|");
	
	while(stringTokenizer.hasMoreTokens())
	{
		dropDownValues.add(stringTokenizer.nextToken());
		
	}
	   }
	   
	   log.debug("drop down {}  value : {}",dropDownName,dropDownValues);
	
	return dropDownValues;
	
}
@Override
public List<String> getDepartmentList(){
	List<String> departments;

	departments=crudService.findWithNamedQuery("EmployeeData.findDistinctDepartment");
	return departments;
}


@Override
public List<DeptCostCenterMap> getAllDeptCostCenterMap(){
	
	return crudService.findWithNamedQuery("DeptCostCenterMap.findAll");
}

@Override
public List<DeptCostCenterMap> saveDeptCostCenterMap(String department,String costCenter){
	
	log.debug("insert.............................");
	List<DeptCostCenterMap> dccmList=crudService.findWithNamedQuery("DeptCostCenterMap.findCostCenterMapByDept",QueryParameter.with("department", department).parameters());
	
	log.debug("updated {},{},{}",department,costCenter,dccmList.isEmpty());

	if(dccmList.isEmpty()){
		DeptCostCenterMap dccm=new DeptCostCenterMap();
		dccm.setDepartment(department);
		dccm.setCostCenterCode(costCenter);
	crudService.create(dccm);
	}
	else
	{
		DeptCostCenterMap dccmForUpdate;
		dccmForUpdate=crudService.find(DeptCostCenterMap.class, dccmList.get(0).getDeptCostCenterMapID());
		//dccmForUpdate.setCostCenter(costCenter);
		dccmForUpdate.setCostCenterCode(costCenter);
		crudService.update(dccmForUpdate);
		log.debug("updated {},{}",department,costCenter);
		
	}
	log.debug("done................................");
	return crudService.findWithNamedQuery("DeptCostCenterMap.findAll");
	
}
@Override
public boolean removeDeptCostCentreMap(int deptCostCentreMapId){
	crudService.delete(DeptCostCenterMap.class, deptCostCentreMapId);
	return true;
}

public List<String> getAllLocationName(){
	List <Location>ls =crudService.findWithNamedQuery("Location.findAll");
	List<String> lis=new ArrayList<>();
	for(Location lc:ls){
		lis.add(lc.getLocationName());
	}
	
	return lis;
}

@Override
public List<String> getAllLocationByType(String type){
	List<String> locationList=crudService.findWithNamedQuery("Location.findAllLocationByType", QueryParameter.with("type", type).parameters());
	return locationList;
}

@Override
public String getCostCenter(String upn)
{
	String dept ="";
	String cost="";
	log.debug("-----get cost center----");
 	List<String> department =crudService.findWithNamedQuery("EmployeeData.findDepartmentByUpn",QueryParameter.with("upn", upn).parameters());
 	
 	for(String deptt : department)
 	{
	dept = deptt;
	}
	
 	List<String> costcenter =  crudService.findWithNamedQuery("DeptCostCenterMap.findCostCenterByDept",QueryParameter.with("dept", dept).parameters());
 	
 	for(String costcen : costcenter){
 		cost = costcen;
 		}
 	log.debug(cost);
 	
 	return cost;
}

@Override
public String getDepartment(String upn)
{
	String dept ="";
 	List<String> department =crudService.findWithNamedQuery("EmployeeData.findDepartmentByUpn",QueryParameter.with("upn", upn).parameters());
 	
	for(String deptt : department)
 	{
	dept = deptt;
	}
	
	return dept;
}

@Override
public String getCompanyName(String upn)
{
	String comp ="";
 	List<String> companyName =crudService.findWithNamedQuery("EmployeeData.findCompanyByUpn",QueryParameter.with("upn", upn).parameters());
 	
	for(String company : companyName)
 	{
	comp = company;
	}
	log.debug(comp);
	
	return comp;
}

@Override
public List<EmployeeData> getAllCompanyName()
{
	
	List<EmployeeData>cl =   crudService.findWithNamedQuery("EmployeeData.findDistinctCompany");
	
	return cl;
	
}


@Override
public String getLocation(String upn)
{
	String loc ="";
 	List<String> locationName =crudService.findWithNamedQuery("EmployeeData.findLocationByUpn",QueryParameter.with("upn", upn).parameters());
 	
	for(String location : locationName)
 	{
	loc = location;
	}
	
	return loc;
}

/*@Override
public List<String> getAllLocation()
{
	return crudService.findWithNamedQuery("EmployeeData.findDistinctLocation");
	
}*/
}