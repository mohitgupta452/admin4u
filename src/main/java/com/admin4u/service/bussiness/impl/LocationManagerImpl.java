package com.admin4u.service.bussiness.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.DropDowns;
import com.admin4u.persistence.entity.LocServiceMap;
import com.admin4u.persistence.entity.Location;
import com.admin4u.persistence.entity.PolicyDoc;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Roles;
import com.admin4u.views.LocationMapBean;
import com.admin4u.views.UserSessionBean;

@Stateless
@Transactional
public class LocationManagerImpl implements LocationManger {
	Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private CrudService crudService;

	@Inject
	private UserSessionBean userSession;

	@Inject
	private RoleManager roleManager;

	@Inject
	private LocationMapBean locationMapBean;

	public String mapSubLocation(int parentLocId, String[] subLocations) {
		String responseMsg="";
		log.debug("location mapp  parent {} subloction {}",parentLocId,subLocations);
	for(String subLocation : subLocations)
	{
		log.debug("location mapp  parent {} subloction {}",parentLocId,subLocation);
		if (isLocationExist(subLocation))
			responseMsg= "Mapping already exist";
		else {
			Location location = new Location();
			location.setActive(true);
			location.setParent(parentLocId);
			location.setLocationName(subLocation);
			location.setType("SUBLOCATION");
			location = crudService.create(location);
			log.debug("sub location {} mapped with location {} , sublocation id {}", location.getLocationName(),
					location.getParent(), location.getLocationID());
			responseMsg= "location "+location.getLocationName() +" is successfully mapped to location "+this.getLocById(parentLocId).getLocationName();
		}
		log.debug("response  {}",responseMsg);
	}

		return responseMsg;
	}

	public String addNewLocation(String locationName) {
		if (this.getLocByName(locationName) == null) {
			return "location already exist";
		} else {
			Location location = new Location();
			location.setActive(true);
			location.setParent(0);
			location.setLocationName(locationName);
			location.setType("PARENT");
			location = crudService.create(location);
		}

		return null;
	}
	
	public String removeMapping(int id)
	{
		crudService.delete(Location.class, id);
		return null;
	}
	

	private boolean isLocationExist(String location) {

		if (this.getLocByName(location) != null)
			return true;
		return false;

	}

	private Location getLocById(int id) {
		return crudService.find(Location.class, id);
	}

	@Override
	public String getParentLocation(String locationName) {
		int locationId = this.getLocationIdByName(locationName);

		Location childLocation = crudService.find(Location.class, this.getLocationIdByName(locationName));

		if (childLocation != null) {
			if ("Parent".equalsIgnoreCase(childLocation.getType()) || childLocation.getParent() == 0) {
				return childLocation.getLocationName();
			} else if (childLocation.getParent() != 0) {
				Location parentLocation = crudService.find(Location.class, childLocation.getParent());
				return parentLocation.getLocationName();
			}
		}
		return null;
	}

	@Override
	public List<String> getWorkLocation(String location) {

		List<String> listwl = crudService.findWithNamedQuery("worklocation.findByLocationName",
				QueryParameter.with("location", location).parameters());
		return listwl;

	}

	@Override
	public List<String> getWorkLocationBYCurrentLocation() {
		String location = userSession.getLocationName();
		int locationId = this.getLocationIdByName(location);
		List<Location> locations = crudService.findWithNamedQuery("Location.findAllLocationByParent",
				QueryParameter.with("parent", locationId).parameters());

		List<String> listwl = locations.stream().map(Location::getLocationName).collect(Collectors.toList());
		return listwl;

	}

	@Override
	public List<String> getAllWorkLocationName() {

//		log.debug("get all location");
//		List<Worklocation> ls = crudService.findWithNamedQuery("worklocation.findAll");
//		List<String> lis = new ArrayList<>();
//		for (Worklocation lc : ls) {
//			lis.add(lc.getWorklocationName());
//		}
		return null;
	}

	@Override
	public int getWorkLocationID(String worklocation) {
		List<Integer> lw = crudService.findWithNamedQuery("worklocation.findLocationID",
				QueryParameter.with("worklocation", worklocation).parameters());
		log.debug(lw.toString());
		int wk1 = 0;
		for (int wk : lw) {
			wk1 = wk;
		}

		return wk1;
	}

	@Override
	public void delete(int worklocationId) {
		//crudService.delete(Worklocation.class, worklocationId);
	}

	@Override
	public void insertData(String locationName, String worklocationName) {
//		Worklocation wkn = new Worklocation();
//		wkn.setLocationName(locationName);
//		wkn.setWorklocationName(worklocationName);
//		wkn.setActive(true);
//		crudService.create(wkn);
	}

	@Override
	public void updateLocation(String newWorklocation, int worklocationId) {
//		Worklocation wk = crudService.find(Worklocation.class, worklocationId);
//		wk.setWorklocationName(newWorklocation);
//		wk = crudService.update(wk);
	}

	@Override
	public List<LocServiceMap> getLocServiceMapByLocation(String locationName) {

		return crudService.findWithNamedQuery("LocServiceMap.findByLocationID",
				QueryParameter.with("locationName", locationName).parameters());

	}

	@Override
	public boolean getServiceStatusByLocation(String serviceName, String locationName) {

		List<Byte> lb = crudService.findWithNamedQuery("LocServiceMap.findServiceStatusByServAndLocName",
				QueryParameter.with("serviceName", serviceName).and("locationName", locationName).parameters());
		if (lb.get(0) == 1)
			return true;
		else
			return false;
	}

	@Override
	public boolean turnOnOffServiceByServiceNameAndLocation(LocServiceMap service) {

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		boolean isUpdated = false;
		parameterMap.put("serviceName", service.getService());
		parameterMap.put("serviceLocation", service.getLocationName());
		List<LocServiceMap> lsp = crudService.findWithNamedQuery("LocServiceMap.findByServiceName", parameterMap);
		if (lsp.isEmpty()) {
			LocServiceMap newService = new LocServiceMap();
			newService.setService(service.getService());
			newService.setIsActive(Byte.parseByte("1"));
			newService.setLocationName(service.getLocationName());

			crudService.create(newService);

		}
		for (LocServiceMap lsm : lsp) {
			lsm.setIsActive(service.getIsActive());

			if (crudService.update(lsm).getIsActive() == service.getIsActive())
				isUpdated = true;
			else
				isUpdated = false;
		}
		return isUpdated;
	}

	@Override
	public int getLocationIdByName(String locationName) {
		List<Location> location = crudService.findWithNamedQuery("Location.findLocationByName",
				QueryParameter.with("locationName", locationName).parameters());
		log.debug("location id name{}", locationName);
		int locationId = 0;
		if (!location.isEmpty()) {
			locationId = location.get(0).getLocationID();
			log.debug("location id {}", location.get(0).getLocationID());

		}
		return locationId;
	}

	public Location getLocByName(String loc) {
		List<Location> locations = crudService.findWithNamedQuery("Location.findLocationByName",
				QueryParameter.with("locationName", loc).parameters());

		if (locations.size() > 0)
			return locations.get(0);
		return null;
	}

	public List<Location> getAllParentLocation(int locId) {
		return crudService.findWithNamedQuery("Location.findAllLocationByParent",
				QueryParameter.with("parent", locId).parameters());
	}

	@SuppressWarnings("unchecked")
	private List<PolicyDoc> getByLocAndDoc(String location, String document) {
		return crudService.findWithNamedQuery("PolicyDoc.findByLocDoc",
				QueryParameter.with("document", document).and("location", location).parameters());

	}

	private boolean isPolicyDocExist(String location, String document) {
		if (this.getByLocAndDoc(location, document).size() > 0)
			return true;
		return false;
	}

	public String uploadPolicyDoc(PolicyDoc policyDoc) {

		if (isPolicyDocExist(policyDoc.getLocation(), policyDoc.getDocName())) // delete
																				// document
																				// if
																				// already
																				// exist.
			this.removePolicy(this.getByLocAndDoc(policyDoc.getLocation(), policyDoc.getDocName()).get(0).getId());

		crudService.create(policyDoc);
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<PolicyDoc> getPoliciesByLoc(String location) {
		return crudService.findWithNamedQuery("PolicyDoc.findByLocation",
				QueryParameter.with("location", location).parameters());
	}

	private PolicyDoc policyDocById(int id) {
		return crudService.find(PolicyDoc.class, id);
	}

	private boolean deleteFile(String path) {
		File file = new File(path);
		if (file.exists())
			file.delete();
		return true;
	}

	public String removePolicy(int policyId) {

		this.deleteFile(this.policyDocById(policyId).getPath()); // delete file
																	// from disk
		crudService.delete(PolicyDoc.class, policyId);

		return null;
	}

	@Override
	public List<String> getSubLocations(String location) {
		List<Location> locationMasters= this.getAllParentLocation(this.getLocationIdByName(location));
		return locationMasters.stream().map(Location :: getLocationName).collect(Collectors.toList());
	}

}
