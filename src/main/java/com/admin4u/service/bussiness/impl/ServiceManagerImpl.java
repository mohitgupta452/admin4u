package com.admin4u.service.bussiness.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.management.Query;
import javax.transaction.Transactional;

import org.mortbay.util.ajax.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.RequestServiceMap;
import com.admin4u.persistence.entity.TatServiceMaster;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.LocServiceMap;
import com.admin4u.persistence.entity.NonApprovalDesignation;
import com.admin4u.persistence.entity.NotificationMaster;
import com.admin4u.persistence.entity.PriorityTatMap;
import com.admin4u.persistence.entity.RequestData;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.bussiness.contract.ServiceManager;
import com.admin4u.service.utils.Request;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.util.service.Response;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

@Stateless
@Transactional
public class ServiceManagerImpl implements ServiceManager {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private CrudService crudService;

	Properties prop;
	
	
	@Override 
	public boolean turnOnOffServiceByServiceNameAndLocation(LocServiceMap service){
		
		Map<String,Object> parameterMap =new HashMap<String,Object>();
		 boolean isUpdated=false;
		parameterMap.put("serviceName", service.getService());
		parameterMap.put("serviceLocation", service.getLocationName());
		List<LocServiceMap> lsp = crudService.findWithNamedQuery("LocServiceMap.findByServiceName", parameterMap);
		if (lsp.isEmpty()) {
			LocServiceMap newService = new LocServiceMap();
			newService.setService(service.getService());
			newService.setIsActive(Byte.parseByte("1"));
			newService.setLocationName(service.getLocationName());

			crudService.create(newService);

		}
		for (LocServiceMap lsm : lsp) {
			lsm.setIsActive(service.getIsActive());

			if (crudService.update(lsm).getIsActive() == service.getIsActive())
				isUpdated = true;
			else
				isUpdated = false;
		}
		return isUpdated;
	}

	@Override
	public long getTATTimeByPriority(String priority) {
		List<PriorityTatMap> priorityTatMap = crudService.findWithNamedQuery("PriorityTatMap.findByPriority",QueryParameter.with("priority", priority).parameters());
		long tat = 0;
		for (PriorityTatMap ptm : priorityTatMap)
			tat = ptm.getTurnAroundTime();
		return tat;

	}

	@Override
	public void genrateTatNotifications(String role) {

		List<Map<String, Object>> reqMap = this.getRequestForTatNotification(role);
		List<NotificationMaster> notificationList = new ArrayList<NotificationMaster>();
		long tat = 0;
		for (Map<String, Object> map : reqMap) {
			if (map.get("priority") != null) {
				tat = this.getTATTimeByPriority(map.get("priority").toString());
				Date requestDate = new Date();
				try {
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					requestDate = dateFormat.parse(map.get("requestdate").toString());
					long currentTimeStamp = new Date().getTime();

					long tatExpTime = requestDate.getTime() + tat;

					boolean isTatExpired = false;

					if (currentTimeStamp > tatExpTime) {
						isTatExpired = true;
					}

					if (map.get("requestStatus").toString().equals("PENDING") && isTatExpired) {
						String owner = map.get("owner").toString();
						{
							NotificationMaster notification = new NotificationMaster();
							String message = "requestID" + map.get("requestID").toString() + " Tat time is Expired";
							notification.setMessage(message);
							notification.setCreationTime(new Date());
							// notification.setUserAction((int)map.get("requestID"));
							notification.setReciever(owner);
							crudService.create(notification);
						}
					}

				}

				catch (Exception ex) {

					log.debug("Exception in ", ex.getMessage());
				}

			}

		}
		log.debug("notification list contains", notificationList);

	}

	@Override
	public List<Map<String, Object>> getRequestForTatNotification(String role) {

		log.debug("getrequest for notification called");
		// RequestMaster request=new RequestMaster();

		List<Map<String, Object>> userReqActionList = new ArrayList<>();

		@SuppressWarnings("unchecked")
		List<UserActionMaster> userActionList = crudService.findWithNamedQuery("UserActionMaster.findByRole",
				QueryParameter.with("role", role).parameters());

		for (UserActionMaster userAction : userActionList) {

			Map<String, Object> actionMap = new HashMap<String, Object>();

			actionMap.put("status", userAction.getStatus());
			actionMap.put("owner", userAction.getOwner());
			actionMap.put("actionId", userAction.getActionID());
			actionMap.put("isRead", userAction.isRead());

			log.debug("action id {}", userAction.getActionID());

			//actionMap.putAll(this.commonAtrToMap(userAction.getRequestMaster()));

			log.debug("action data retrieval  map : {}", actionMap);
			userReqActionList.add(actionMap);
		}
		return userReqActionList;
	}

	public List<PriorityTatMap> getAllTats() {

		return crudService.findWithNamedQuery("PriorityTatMap.findAll");

	}

	public String updateTatValue(PriorityTatMap ptm) {

		PriorityTatMap newPtm = crudService.find(PriorityTatMap.class, ptm.getId());
		newPtm.setPriority(ptm.getPriority());
		newPtm.setTurnAroundTime(ptm.getTurnAroundTime());
		newPtm.setService(ptm.getService());

		PriorityTatMap updatedPtm = crudService.update(newPtm);

		Map<String, Object> responseMap = new HashMap<String, Object>();
		responseMap.put("id", updatedPtm.getId());
		responseMap.put("priority", updatedPtm.getPriority());
		responseMap.put("tat", updatedPtm.getTurnAroundTime());
		responseMap.put("service", updatedPtm.getService());

		return new Gson().toJson(responseMap);
	}

	public String addNewPriority(PriorityTatMap ptm) {

		PriorityTatMap updatedPtm = crudService.create(ptm);

		Map<String, Object> responseMap = new HashMap<String, Object>();
		responseMap.put("id", updatedPtm.getId());
		responseMap.put("priority", updatedPtm.getPriority());
		responseMap.put("tat", updatedPtm.getTurnAroundTime());
		responseMap.put("service", updatedPtm.getService());


		return new Gson().toJson(responseMap);
	}
	
	
	
	@Override
	public PriorityTatMap getPriorityByService(String service){
	
		List<PriorityTatMap> priorityList=crudService.findWithNamedQuery("PriorityTatMap.findByService",QueryParameter.with("service", service).parameters());
		if(!priorityList.isEmpty()){
		PriorityTatMap priority=priorityList.get(0);
			return priority;
		}
		else
			return new PriorityTatMap();
			}
	
	
	@Override
	public List<String> getAllDesignation()
	{
		
		return crudService.findWithNamedQuery("EmployeeData.findDesignation");
	}

	@Override
	public boolean addNonApprovalDesignation(NonApprovalDesignation nonAppdesig) {
	

		 boolean isUpdated=false;
List<NonApprovalDesignation> nonApp = crudService.findWithNamedQuery("NonApprovalDesignation.findAllByDesignation", QueryParameter.with("desig", nonAppdesig.getDesignation()).parameters());
		if (nonApp.isEmpty()) {
		
		NonApprovalDesignation newDesig = new NonApprovalDesignation();
		newDesig.setDesignation(nonAppdesig.getDesignation());
		newDesig.setUpdatedBy(nonAppdesig.getUpdatedBy());
		newDesig.setActive(nonAppdesig.getActive());
		newDesig.setUpdationTime(nonAppdesig.getUpdationTime());
		
		crudService.create(newDesig);
		     isUpdated = true;
		}
		for (NonApprovalDesignation nad : nonApp) {
			nad.setActive(nonAppdesig.getActive());

			if (crudService.update(nad).getActive() == nonAppdesig.getActive())
				isUpdated = true;
			else
				isUpdated = false;
		}
		return isUpdated;
		
	}
	
	@Override
	public List<String> getNonApprovalDesignationByActive() {
		
		return crudService.findWithNamedQuery("NonApprovalDesignation.findDesignationByActive", QueryParameter.with("active",(byte)1).parameters());		
        
	}
	
	public List<NonApprovalDesignation> getActive()
	{
		return crudService.findWithNamedQuery("NonApprovalDesignation.findActive&Designation");
	}
	
	
	
	public String mapService(TatServiceMaster tatServiceMaster)
	{
		crudService.create(tatServiceMaster);
		return null;
	}
	
	
	
}
