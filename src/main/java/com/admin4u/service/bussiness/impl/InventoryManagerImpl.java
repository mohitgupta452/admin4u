package com.admin4u.service.bussiness.impl;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.CategaryMaster;
import com.admin4u.persistence.entity.InventoryTrack;
import com.admin4u.persistence.entity.IssueLimitation;
import com.admin4u.persistence.entity.StockInfo;
import com.admin4u.service.bussiness.contract.InventoryManager;
import com.admin4u.service.bussiness.contract.ProductManager;
import com.admin4u.util.enums.HostType;
import com.admin4u.util.service.RequestMetadata.ProductKey;

@Stateless
@Transactional
public class InventoryManagerImpl implements InventoryManager
{
	@Inject
	private CrudService	crudService;
	
	@Inject
	private ProductManager productManager;

	@Inject
	private Logger		log;

	public void assignProduct(Map<Integer, Integer> assignedProducts, String assignTo,
			String assignBy)
	{
		assignedProducts.forEach((productId, consumed) -> {

			if (this.isStockAvailable(this.getStockInfo(productId), consumed))
			{
				
				StockInfo stockInfo= this.getStockInfo(productId);
				
				stockInfo.setAvailableStock(stockInfo.getAvailableStock()-consumed);//deduct issued item quantity from stock 
				crudService.update(stockInfo);
				
				InventoryTrack inventoryTrack = new InventoryTrack();// enter record in inventory track 
				inventoryTrack.setIssue_By(assignBy);
				inventoryTrack.setIssue_To(assignTo);
				inventoryTrack.setProductDetail(productManager.getProductDetails(productId));
				inventoryTrack.setQuantity(String.valueOf(consumed));
				inventoryTrack = crudService.create(inventoryTrack);

				log.debug(
						"Inventory track updated with id : {} ,assigned product : {} assigned to : {} assigned by {} assigned quantity {}",
						inventoryTrack.getId(), inventoryTrack.getProductDetail().getName(),
						inventoryTrack.getIssue_To(), inventoryTrack.getIssue_By());
			}
			else
				log.debug("Product not available , demand quantity :  {} available quantity : {} ",
						consumed, this.getStockInfo(productId).getAvailableStock());
		});
	}
	
	

	private StockInfo getStockInfo(int productId)
	{
		List<StockInfo> stockInfos = crudService.findWithNamedQuery("StockInfo.findByProductId",
				QueryParameter.with("productId", productId).parameters());
		if (stockInfos.size() > 0)
			return stockInfos.get(0);
		return null;
	}

	private boolean isStockAvailable(StockInfo stockInfo, int consumed)
	{
		if (stockInfo.getAvailableStock() >= consumed)
			return true;
		return false;
	}

	private int getAvailableStock(int available, int consumed, HostType host)
	{
		int op = 1;
		if (host == HostType.CONSUMER)
		{
			op *= -1;
		}
		return available + (op * consumed);
	}

	private boolean reorderState(StockInfo stockInfo)
	{
		if (stockInfo.getAvailableStock() <= stockInfo.getReorderQuantity())
			return true; // fire alert for reorder on true 
		return false;
	}

	
	public String updateStock(List<Map<String,Object>> products)
	{
		boolean isNew=false;
		
		for(Map<String,Object> product : products)
          {
			log.debug("product details {}",product);
			
			StockInfo stockInfo = this.getStockInfo(Integer.parseInt(product.get("productId").toString()));	

			   if(stockInfo == null)
			     {
				stockInfo = new StockInfo();
				isNew=true;
			     }
			  if(isNew)
			    {
				  stockInfo.setUpdationTime(new Date());
				stockInfo.setAvailableStock(Integer.parseInt(product.get(ProductKey.QUANTITY).toString()));
				stockInfo.setProductDetail(productManager.getProductDetails(Integer.parseInt(product.get("productId").toString())));
				crudService.create(stockInfo);
			    }
			  else
			  {
				  stockInfo.setUpdationTime(new Date());
				stockInfo.setAvailableStock(stockInfo.getAvailableStock()+Integer.parseInt(product.get(ProductKey.QUANTITY).toString()));
			      crudService.update(stockInfo); 
			  }
			
          	}	
		return null;
	}


	private List<IssueLimitation> issueLimitByProdId(int productId)
	{
		return crudService.findWithNamedQuery("IssueLimitation.findByProductId",
				QueryParameter.with("productId", productId).parameters());
	}

	private IssueLimitation issueLimitation(int id)
	{
		return crudService.find(IssueLimitation.class, id);
	}

	public void setLimitations(List<IssueLimitation> issueLimitations)
	{
		issueLimitations.forEach(issueLimitation -> {

			if (issueLimitation.getId() != 0)
				crudService.update(issueLimitation);
			else
				crudService.create(issueLimitation);
		});
	}

	public void manageCategary(List<CategaryMaster> categaryMasters)
	{
		categaryMasters.forEach(categaryMaster -> {

			crudService.create(categaryMaster);
		});
	}
	
	@Override
	public List<StockInfo> getAllStockInfo()
	{
		return crudService.findWithNamedQuery("StockInfo.findAll");
	}

	private void setClosure()
	{

	}

	public void getStockInfo()
	{

	}
}
