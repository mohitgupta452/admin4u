package com.admin4u.service.bussiness.impl;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nonnull;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletRequestEvent;
import javax.transaction.Transactional;

import org.apache.http.impl.cookie.DateParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.EmployeeRoleServiceMaster;
import com.admin4u.persistence.entity.EventTracker;
import com.admin4u.persistence.entity.RequestData;
import com.admin4u.persistence.entity.ProductDetail;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.RequestServiceMap;
import com.admin4u.persistence.entity.ResponseMaster;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.EventTatManager;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.NotificationManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.service.utils.Request;
import com.admin4u.util.enums.Event;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.ApplicationMetaData.Action;
import com.admin4u.util.service.ApplicationMetaData.ProcessType;
import com.admin4u.util.service.ApplicationMetaData.Status;
import com.admin4u.util.service.ProcessListener;
import com.admin4u.util.service.ProcessStates;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.util.service.Response;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Transactional
@Stateless

public class RequestResponseManagerImpl implements RequestResponseManager {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private CrudService crudService;

	@PersistenceContext(unitName = "admin4u")
	private EntityManager entityManager;

	@Inject
	private RoleManager roleManager;

	@Inject
	private IDMService idmService;

	@Inject
	private UserSessionBean userSession;

	@Inject
	private NotificationManager notificationManager;

	@Inject
	private ADStore adStore;

	private String status;

	private ProcessType processType;

	private ProcessListener processListener;

	@Inject
	private GuestHouseManager guestHouseManager;

	@Inject
	private EventTatManager eventTatManager;

	public RequestMaster insertData(Map<String, String[]> requestMap) {

		processListener = new ProcessStates();
		Request request = new Request();
		request.filterRequest(requestMap);
		Map<String, Object> reqMap = request.getCommonRequestMap();

		Services currentService = Services.valueOf(reqMap.get(RequestMetadata.Service).toString());
		this.processType = processListener.getProcessType(currentService);
		if (processType.toString().equals(ProcessType.APPROVALPROCESS.toString())
				&& adStore.getManager(userSession.getEmpData().getUserPrincipalName()) == null)
			return null;
		processListener.getOwnerByProcess();
		processListener.actionPerformed(Action.GENERATED);
		RequestMaster requestMaster = this.convert(reqMap);
		RequestData requestData = new RequestData();

		log.debug("before insert data ");
		requestData.setRequestData(request.getRequestDataJson());
		log.debug("inside insert data{} ", request.getRequestDataJson());
		requestMaster.setRequestData(requestData);
		requestMaster = crudService.create(requestMaster);

		if (processType.toString().equals(ProcessType.NONAPPROVALPROCESS.toString())) // Generate
																						// Work
																						// order
																						// no
																						// for
																						// non
																						// approval
																						// process

			requestMaster.setWorkorder(this.generateWorkOrder(requestMaster));

		requestMaster = crudService.update(requestMaster);

		log.debug("request generated {},{},{}", requestMaster.getRequestBy(), requestMaster.getRequestData());
		List<String> serviceList = (List<String>) reqMap.get(RequestMetadata.SERVICE_LIST);

		this.setRequestOwner(request.getRequestDataJson(), requestMaster, serviceList);
		this.setReqService(requestMaster, serviceList);

		this.setRequestEvent(requestMaster, Services.valueOf(reqMap.get(RequestMetadata.Service).toString()), request);
		return requestMaster;

	}

	@Override
	public String generateWorkOrder(RequestMaster requestMaster) {
		String workorder = null;
		String query = "select r.workorder from request r where r.workorder like '"
				+ requestMaster.getLocation().substring(0, 3) + "\\/"
				+ idmService.getEmpByUpn(requestMaster.getRequestBy()).getDepartment().substring(0, 3)
				+ "\\/%' ORDER BY r.requestId DESC LIMIT 1";

		javax.persistence.Query emQuery = entityManager.createNativeQuery(query);

		String requestMasters = emQuery.getSingleResult().toString();

		if (requestMasters == null || requestMasters.isEmpty()) // if
																// workorder
																// serial
																// not exist
			return (requestMaster.getLocation().substring(0, 3) + "/"
					+ idmService.getEmpByUpn(requestMaster.getRequestBy()).getDepartment().substring(0, 3) + "/" + 001)
							.toUpperCase();

		workorder = requestMasters;
		log.debug("work order {} ",workorder);
		return workorder.substring(0,8) + (Integer.parseInt(workorder.substring(8))+1);
	}

	private RequestMaster convert(Map<String, Object> reqMap) {
		RequestMaster requestMaster = new RequestMaster();
		requestMaster.setLocation(reqMap.get(RequestMetadata.LOCATION).toString());
		requestMaster.setRequestBy(reqMap.get(RequestMetadata.REQUESTER).toString());
		requestMaster.setRequestType(reqMap.get(RequestMetadata.REQUEST_TYPE).toString());
		requestMaster.setIpAddress(reqMap.get(RequestMetadata.Request_IP).toString());
		requestMaster.setRequestDate(new Date());

		requestMaster.setRequestStatus("PENDING");
		requestMaster.setService(reqMap.get(RequestMetadata.Service).toString());
		requestMaster.setComplete(false);

		log.debug("service lis {}", reqMap.get(RequestMetadata.SERVICE_LIST));
		return requestMaster;

	}

	private void setReqService(RequestMaster requestMaster, List<String> serviceList) {
		for (String service : serviceList) {
			RequestServiceMap requestService = new RequestServiceMap();
			requestService.setService(service);
			requestService.setRequest(requestMaster);
			crudService.create(requestService);
		}
	}

	private void setRequestOwner(JsonObject reqMap, RequestMaster requestMaster, List<String> serviceList) {

		Map<String, String> ownerMap = new HashMap<>();

		if (userSession.getEmpData().getDesignation().equals("AED")
				&& processType.toString().equals(ProcessType.APPROVALPROCESS.toString())) {
			processListener.setProcessType(ProcessType.NONAPPROVALPROCESS);
			requestMaster.setWorkorder(this.generateWorkOrder(requestMaster));
		}
		// #1:Assign cab immediately in case of odd hours
		else if (requestMaster.getService().matches("(.*)CAB(.*)")) {
			log.debug("inside Assign cab immediately in case of odd hours");
			if (this.isCabOddHoursBooking(requestMaster, reqMap)) {
				requestMaster.setRequestStatus(Status.AUTO_APPROVED.toString());
				processListener.setProcessType(ProcessType.NONAPPROVALPROCESS);
				requestMaster.setWorkorder(this.generateWorkOrder(requestMaster));
			}
		}
		// end #1;
		ownerMap.putAll(processListener.getOwnerByProcess());

		for (Entry<String, String> entry : ownerMap.entrySet()) {

			if (entry.getKey().equals(Roles.SPOC.toString())) {

				for (String service : serviceList) {
					UserActionMaster userAction = new UserActionMaster();
					userAction.setRole(entry.getKey());
					userAction.setStatus(entry.getValue());
					userAction.setLocation(requestMaster.getLocation());
					userAction.setRequestMaster(requestMaster);
					userAction.setRead(false);
					userAction.setStatus(Status.PENDING.toString());
					userAction.setService(service);
					userAction.setAssignTime(new Date());
					userAction = crudService.create(userAction);
				}
			} else if (entry.getKey().equals(Roles.APPROVAL_AUTHORITY.toString())) {

				if (adStore.getManager(userSession.getEmpData().getUserPrincipalName()) != null) {
					UserActionMaster userAction = new UserActionMaster();

					userAction.setOwner(
							adStore.getManager(userSession.getEmpData().getUserPrincipalName()).getUserPrincipalName());
					userAction.setRole(entry.getKey());
					userAction.setStatus(entry.getValue());
					userAction.setLocation(requestMaster.getLocation());
					userAction.setRequestMaster(requestMaster);
					userAction.setRead(false);
					userAction.setAssignTime(new Date());
					userAction.setStatus(Status.PENDING.toString());
					userAction = crudService.create(userAction);
					notificationManager.setNotificationByAction(processListener.getNotificationMap(), userAction,
							requestMaster);
					notificationManager.generateEmail(userAction.getRequestMaster(),
							processListener.getNotificationMap(),Action.GENERATED);
					eventTatManager.createTatEvent(userAction);
				}
			} else {

				UserActionMaster userAction = new UserActionMaster();
				userAction.setRole(entry.getKey());
				userAction.setStatus(entry.getValue());
				userAction.setAssignTime(new Date());
				userAction.setLocation(requestMaster.getLocation());
				userAction.setRequestMaster(requestMaster);
				userAction.setStatus(Status.PENDING.toString());
				userAction.setRead(false);
				crudService.create(userAction);
				notificationManager.setNotificationByAction(processListener.getNotificationMap(), userAction,
						requestMaster);
				notificationManager.generateEmail(userAction.getRequestMaster(), processListener.getNotificationMap(),Action.GENERATED);
			}
		}
	}

	// start set request owner method for multipassenger request

	private boolean isCabOddHoursBooking(RequestMaster requestMaster, JsonObject reqMap) {
		boolean isOddHours = false;
		int hours = 0;
		int minutes = 0;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String cabTimingStr = reqMap.get(requestMaster.getService().toLowerCase() + ".pickupdatetime").getAsString();
		try {
			Date cabTiming = dateFormatter.parse(cabTimingStr);

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(cabTiming);
			hours = calendar.get(Calendar.HOUR_OF_DAY);
			minutes = calendar.get(Calendar.MINUTE);

		} catch (Exception e) {
			log.debug("exception in date time {}", e.getMessage());
		}
		/*
		 * if (!(hours > 6 && hours < 22)) { isOddHours = true; }
		 */
		if (!(hours * 60 + minutes > (9 * 60) + 30 && hours < 18)) {
			isOddHours = true;
		}
		log.debug("value of is odd hours {},{},{}", isOddHours, hours, cabTimingStr);
		// System.exit(0);
		return isOddHours;
	}

	// start set request owner method for multipassenger request

	/*
	 * @Override public void setRequestOwnerForMultiPassenger(Map<String,
	 * Object> reqMap, RequestMaster requestMaster, List<String> serviceList,
	 * String employeeId) throws Exception {
	 * 
	 * log.debug("inside multi passenger set request owner"); Map<String,
	 * String> ownerMap = new HashMap<>();
	 * 
	 * if (userSession.getEmpData().getDesignation().equals("AED")) {
	 * processListener.actionPerformed(Action.APPROVAL_AUTHORITY_ACCEPT);
	 * ownerMap.putAll(processListener.getNextOwner()); } else
	 * ownerMap.putAll(processListener.getOwnerByProcess());
	 * 
	 * for (Entry<String, String> entry : ownerMap.entrySet()) {
	 * 
	 * if (adStore.getManager(idmService.getEmployeeUPNByEmployeeId(employeeId))
	 * != null) { UserActionMaster userAction = new UserActionMaster();
	 * 
	 * userAction.setOwner(
	 * adStore.getManager(idmService.getEmployeeUPNByEmployeeId(employeeId)).
	 * getUserPrincipalName()); userAction.setRole(entry.getKey());
	 * userAction.setStatus(entry.getValue());
	 * userAction.setLocation(requestMaster.getLocation());
	 * userAction.setRequestMaster(requestMaster); userAction.setRead(false);
	 * crudService.create(userAction); } else { log.debug(
	 * "Manager of {} is not mapped",
	 * userSession.getUser().getUserPrincipalName()); throw new Exception(
	 * "Manager of user  is not found");
	 * 
	 * }
	 * 
	 * } }
	 */

	// end set request owner method

	private Map<String, Object> commonAtrToMap(RequestMaster requestMaster) {
		Map<String, Object> requestDataMap = new HashMap<String, Object>();
		requestDataMap.put(RequestMetadata.LOCATION, requestMaster.getLocation());
		requestDataMap.put(RequestMetadata.REQUESTER, idmService.getEmpNameByUpn(requestMaster.getRequestBy()));
		requestDataMap.put(RequestMetadata.Service, requestMaster.getService());
		requestDataMap.put("ServiceDisplay", Services.valueOf(requestMaster.getService()).getDisplayName());
		requestDataMap.put("requestType", requestMaster.getRequestType());
		requestDataMap.put(RequestMetadata.REQUEST_DATE, requestMaster.getRequestDate());
		requestDataMap.put(RequestMetadata.REQUEST_STATUS, requestMaster.getRequestStatus());
		requestDataMap.put(RequestMetadata.Request_IP, requestMaster.getIpAddress());
		requestDataMap.put(RequestMetadata.REQUEST_ID, requestMaster.getRequestId());
		requestDataMap.put("isActive", requestMaster.isActive());
		requestDataMap.put("isComplete", requestMaster.isComplete());
		requestDataMap.put("isExpire", requestMaster.isActive());

		if (requestMaster.getResponseMaster() != null)
			requestDataMap.put("responseAttachment", requestMaster.getResponseMaster().getResponseAttachment() != null
					? requestMaster.getResponseMaster().getResponseAttachment() : null);
		requestDataMap.put("priority", requestMaster.getPriority());
		requestDataMap.put("workorder", requestMaster.getWorkorder());
		List<String> srvList = new ArrayList<>();

		String allServices = "";
		for (RequestServiceMap requestService : requestMaster.getRequestService()) {
			srvList.add(requestService.getService());
			allServices = allServices + " / " + Services.valueOf(requestService.getService()).getDisplayName();
		}
		requestDataMap.put("allServices", allServices.replaceFirst("/", ""));
		requestDataMap.put(RequestMetadata.SERVICE_LIST, srvList);
		log.debug("services : {} ", srvList);
		log.debug("request data retrieval  map : {}", requestDataMap);
		return requestDataMap;
	}

	@Override
	public List<Map<String, Object>> getRequest(String user, String service) {

		log.debug("getRequest() called");
		@SuppressWarnings("unchecked")
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		Date fromDate = cal.getTime();

		List<RequestMaster> reqList = crudService.findWithNamedQuery("RequestMaster.findByUserAndService",
				QueryParameter.with("user", user).and("service", service).and("fromDate", fromDate)
						.and("currentDate", new Date()).and("requestStatus", Status.PENDING.toString()).parameters());

		List<Map<String, Object>> reqDataList = new ArrayList<>();
		for (RequestMaster requestMaster : reqList) {
			reqDataList.add(this.commonAtrToMap(requestMaster));

		}
		log.debug("request Data {}", reqDataList);
		return reqDataList;
	}

	private List<Map<String, Object>> getUserActionMaster(List<UserActionMaster> userActionList, Roles role) {
		List<Map<String, Object>> userReqActionList = new ArrayList<>();

		for (UserActionMaster userAction : userActionList) {

			Map<String, Object> actionMap = new HashMap<String, Object>();

			actionMap.put("status", userAction.getStatus());
			actionMap.put("actionId", userAction.getActionID());
			actionMap.put("isRead", userAction.isRead());

			actionMap.putAll(this.commonAtrToMap(userAction.getRequestMaster()));

			if (role.toString().equals(Roles.SPOC.toString()) && userAction.getService() != null) {
				actionMap.replace(RequestMetadata.Service, userAction.getService());
				List<String> srvList = new ArrayList<>();
				srvList.add(userAction.getService());
				actionMap.remove(RequestMetadata.SERVICE_LIST);
				actionMap.put(RequestMetadata.SERVICE_LIST, srvList);
				log.debug("service ... {}", srvList);
			}

			if (userAction.getRequestMaster().getRequestData().getRequestData().has("approovalAttachment"))
				actionMap.put("approovalAttachment", userAction.getRequestMaster().getRequestData().getRequestData()
						.get(userAction.getRequestMaster().getService().toLowerCase() + ".approovalAttachment"));

			log.debug("action data retrieval  map : {}", actionMap);
			userReqActionList.add(actionMap);
		}
		log.debug("user requesr List {}", userReqActionList);
		return userReqActionList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Map<String, Object>> getRequestByOwner(String upn, Roles role) {// get
																				// all
																				// user
																				// request
																				// of
																				// admin
		log.debug("getrequest BY owner() called");

		List<UserActionMaster> userActionList = new ArrayList<>();

		if (role.toString().equals(Roles.ADMIN.toString()))
			userActionList.addAll(crudService.findWithNamedQuery("USerActionMaster.findByRoleAndOwnerUPN",
					QueryParameter.with("upn", upn).and("role", Roles.ADMIN.toString()).parameters()));

		for (EmployeeRole empRole : roleManager.getEmpByUpnAndRole(upn, role)) {
			userActionList.addAll(crudService.findWithNamedQuery("UserActionMaster.findByAdminRequests",
					QueryParameter.with("role", role.toString()).and("location", empRole.getLocation()).parameters()));
		}

		userActionList.forEach(userAction -> log.debug("request {}", userAction.getRequestMaster().getRequestId()));

		return this.getUserActionMaster(userActionList, role);
	}

	@Override
	public List<Map<String, Object>> getSpocRequest(String upn, Roles roles) {
		List<UserActionMaster> userActionList = new ArrayList<>();
		List<EmployeeRole> employeeRoles = roleManager.getEmpByUpnAndRole(upn, roles);

		userActionList.addAll(crudService.findWithNamedQuery("USerActionMaster.findByRoleAndOwnerUPN",
				QueryParameter.with("upn", upn).and("role", roles.toString()).parameters()));

		if (roles.toString().equals(Roles.SPOC.toString()))
			for (EmployeeRole empRole : employeeRoles) {
				for (EmployeeRoleServiceMaster employeeRoleServiceMaster : empRole.getEmployeeRoleServicemaster())
					userActionList.addAll(crudService
							.findByNativeQuery("select * FROM user_action u WHERE  u.ownerRole='" + roles.toString()
									+ "' AND u.location='" + empRole.getLocation() + "' AND u.service like '%"
									+ employeeRoleServiceMaster.getService() + "%'", UserActionMaster.class));

			}

		return this.getUserActionMaster(userActionList, roles);
	}

	@Override
	public List<Map<String, Object>> getAuthRequest(String upn) { // request for
																	// approval
																	// authority
		List<UserActionMaster> userActionMaster = crudService.findWithNamedQuery(
				"USerActionMaster.findByRoleAndOwnerUPN",
				QueryParameter.with("upn", upn).and("role", Roles.APPROVAL_AUTHORITY.toString()).parameters());

		return this.getUserActionMaster(userActionMaster, Roles.APPROVAL_AUTHORITY);

	}

	public RequestMaster getRequestById(int requestId) {
		return crudService.find(RequestMaster.class, requestId);
	}

	private UserActionMaster getActionById(int actionId) {
		return crudService.find(UserActionMaster.class, actionId);
	}

	private String getServiceByRequestId(int requestId) {
		return this.getRequestById(requestId).getService();
	}

	@Override
	public String getRequestDataByReqID(int requestID, int... actionIDs) {
		for (int actionID : actionIDs) {
			UserActionMaster userAction = crudService.find(UserActionMaster.class, actionID);
			userAction.setRead(true);
			crudService.update(userAction);
		}
		RequestMaster requestMaster = this.getRequestById(requestID);
		/*
		 * <<<<<<< HEAD JsonObject responseObj = new
		 * Response().getRequestData(requestMaster.getService(),requestMaster);
		 * responseObj.addProperty("Requester",idmService.getEmpNameByUpn(
		 * requestMaster.getRequestBy()));
		 * 
		 * 
		 * 
		 * 
		 * log.debug("request data {} ",responseObj);
		 * 
		 * //Gson gson = new Gson(); //return gson.toJson(responseObj);
		 * 
		 * return responseObj.toString(); =======
		 */
		JsonObject responseObj = new Response().getRequestData(requestMaster.getService(), requestMaster);
		responseObj.addProperty("Requester", idmService.getEmpNameByUpn(requestMaster.getRequestBy()));
		responseObj.addProperty("displayService", Services.valueOf(requestMaster.getService()).getDisplayName());
		responseObj.addProperty("Service", requestMaster.getService());

		log.debug("request data {} ", responseObj);

		// Gson gson = new Gson();
		// return gson.toJson(responseObj);

		return responseObj.toString();

	}

	@Override
	public void insertResonseData(Map<String, String[]> requestMap) {

		ResponseMaster response = new ResponseMaster();
		JsonObject json = new JsonObject();
		for (Entry<String, String[]> entry : requestMap.entrySet()) {
			if (entry.getValue().length > 0)
				json.addProperty(entry.getKey(), entry.getValue()[0]);
			log.debug("{}", json);
		}
		String path = requestMap.get("upload").toString();

		response.setResponseAttachment(path);
		response.setResponseData(json.toString());

		RequestMaster requestMaster = this.getReqByActionId(Integer.parseInt(requestMap.get("actionId")[0]));

		response.setRequestMaster(requestMaster);

		response = crudService.create(response);
	}

	@Override
	public String getResponseDataByReqID(int requestID) {

		List<ResponseMaster> respData = crudService.findWithNamedQuery("ResponseMaster.findResponseDataByReqID",
				QueryParameter.with("reqID", requestID).parameters());

		JsonObject responseJson = new Gson().fromJson(respData.size() > 0 ? respData.get(0).getResponseData() : null,
				JsonObject.class);
		
		responseJson.addProperty("Msg",respData.size() > 0 ? respData.get(0).getResponseMsg() : null);

		return responseJson.toString();

	}

	public List<Map<String, Object>> getaAllReqByUser(String upn) {
		List<RequestMaster> reqList = crudService.findWithNamedQuery("RequestMaster.findByUser",
				QueryParameter.with("upn", upn).parameters());

		List<Map<String, Object>> reqDataList = new ArrayList<>();
		for (RequestMaster requestMaster : reqList) {
			reqDataList.add(this.commonAtrToMap(requestMaster));

		}
		log.debug("request Data {}", reqDataList);
		return reqDataList;

	}

	@Override
	public String getResponseMsgByReqID(int requestID) {
		String res = "";
		List<ResponseMaster> respData = crudService.findWithNamedQuery("ResponseMaster.findResponseMsgByReqID",
				QueryParameter.with("reqID", requestID).parameters());

		for (ResponseMaster resD : respData) {
			res = resD.getResponseMsg();
		}

		return res;
	}

	@Override
	public String getAmendReqMsgByReqID(int requestId) {
		return this.getRequestById(requestId).getMessage();
	}

	private RequestMaster getReqByActionId(int actionId) {
		UserActionMaster userAction = this.getActionById(actionId);
		return userAction.getRequestMaster();
	}

	@Override
	public List<UserActionMaster> getRequestbyView(String owner, String role) {
		@SuppressWarnings("unchecked")
		List<UserActionMaster> userActionList = crudService.findWithNamedQuery("UserActionMaster.findByOnView",
				QueryParameter.with("onView", false).and("owner", owner).and("role", role).parameters());
		return userActionList;

	}

	public void setRequestEvent(RequestMaster requestMaster, Services service, Request request) {
		EventTracker eventTracker = new EventTracker();

		eventTracker.setEvent(Event.CANCELTIMEUP.toString());
		eventTracker.setRequestMaster(requestMaster);
		eventTracker.setStatus(Status.PENDING.toString());

		JsonObject requestData = request.getRequestDataJson();
		log.debug("request Data {}", requestData);

		LocalDateTime travelTime = null;

		EventTracker serviceTimeEvent = new EventTracker();
		serviceTimeEvent.setRequestMaster(requestMaster);

		if (service.toString().equals(Services.AIRTICKET.toString())
				|| service.toString().equals(Services.TRAINTICKET.toString())) {
			String timeSlot = requestData.get(service.toString().toLowerCase() + ".timeslot").toString().split("\\:")[0]
					.replace("\"", "").trim();
			String[] traveldate = requestData.get(service.toString().toLowerCase() + ".traveldate").toString()
					.replaceAll("\"", "").split("-");
			List<String> travelDate = Arrays.asList(traveldate);

			log.debug("travel date {}", travelDate);
			travelTime = LocalDateTime.of(LocalDate.of(Integer.parseInt(travelDate.get(0)),
					Month.of(Integer.parseInt(travelDate.get(1))), Integer.parseInt(travelDate.get(2))),
					LocalTime.of(Integer.parseInt(timeSlot), 00));
			log.debug("travel time vale {}", travelTime);
			EventTracker eventTracker2 = new EventTracker();// set air/train
															// request
			// expire after 72
			// hours
			eventTracker2.setEvent(Event.EXPIRE.toString());
			eventTracker2.setEventTime(travelTime.plusHours(72));
			eventTracker2.setStatus(Status.PENDING.toString());
			eventTracker2.setRequestMaster(requestMaster);
			crudService.create(eventTracker2);
			// start code for cancel event
			eventTracker.setEventTime(travelTime);
			crudService.create(eventTracker);

			// end code for cancel event

		} else if (service.toString().toLowerCase().contains("cab")) {
			String pickupDateTime = requestData.get(service.toString().toLowerCase() + ".pickupdatetime").toString()
					.replaceAll("\"", "");
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			travelTime = LocalDateTime.parse(pickupDateTime, dateTimeFormatter);

			EventTracker eventTracker2 = new EventTracker();// set cab request
															// expire after 72
															// hours
			eventTracker2.setEvent(Event.EXPIRE.toString());
			eventTracker2.setEventTime(travelTime.plusHours(72));
			eventTracker2.setStatus(Status.PENDING.toString());
			eventTracker2.setRequestMaster(requestMaster);
			crudService.create(eventTracker2);
			// start code for cancel event
			eventTracker.setEventTime(travelTime);
			crudService.create(eventTracker);
			// end code for cancel event

		} else if (service.toString().equals(Services.GUEST_HOUSE_BOOKING.toString())
				|| service.toString().equals(Services.HOTEL_BOOKING.toString())) {

			String checkinTime = requestData.get("hotelguesthouse.checkindatetime").toString().replaceAll("\"", "");
			String checkOutTime = requestData.get("hotelguesthouse.checkoutdatetime").toString().replaceAll("\"", "");
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			LocalDateTime checkIn = LocalDateTime.parse(checkinTime, dateTimeFormatter);
			LocalDateTime checkOut = LocalDateTime.parse(checkOutTime, dateTimeFormatter);

			// start code for cancel event
			eventTracker.setEventTime(checkIn);
			crudService.create(eventTracker);
			if (service.toString().equals(Services.GUEST_HOUSE_BOOKING.toString())) {
				EventTracker eventTracker2 = new EventTracker();// for
																// unallocating
																// room

				eventTracker2.setEvent(Event.UNALLOCATE_GUESTHOUSE_ROOMS.toString());// change
																						// event
																						// name
																						// as
																						// Unallocate
																						// rooms
				eventTracker2.setEventTime(checkOut);
				eventTracker2.setStatus(Status.PENDING.toString());
				eventTracker2.setRequestMaster(requestMaster);
				crudService.create(eventTracker2);
			}

		}
		/*
		 * else if(service.toString().toLowerCase().contains("cab")) {
		 * log.debug("request data {}",requestData.toString()); String
		 * pickupDateTime=
		 * requestData.get(service.toString().toLowerCase()+".pickupdatetime").
		 * toString().replaceAll("\"",""); DateTimeFormatter dateTimeFormatter=
		 * DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		 * eventTracker.setStatus(Status.PENDING.toString());
		 * eventTracker.setEventTime(travelTime);
		 * crudService.create(eventTracker); }
		 */
		// EVENT close time up

	}

	public void search(Map<String, Object> searchMap) {// for user request
														// search

		log.debug("search map {}", searchMap);

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<RequestMaster> criteriaQuery = criteriaBuilder.createQuery(RequestMaster.class);

		Root<RequestMaster> requestMaster = criteriaQuery.from(RequestMaster.class);

		criteriaQuery.select(requestMaster);

		TypedQuery<RequestMaster> q = entityManager.createQuery(criteriaQuery);

		List<Predicate> criteria = new ArrayList<>();

		for (Entry<String, Object> entry : searchMap.entrySet()) {
			if (entry.getValue() != null) {
				if (entry.getKey().toUpperCase().contains("ID"))
					criteria.add(criteriaBuilder.equal(requestMaster.get(entry.getKey()),
							Integer.parseInt(entry.getValue().toString())));
				else if (entry.getKey().toUpperCase().contains("DATE")) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					try {
						criteria.add(criteriaBuilder.equal(requestMaster.get(entry.getKey()),
								dateFormat.parse(entry.getValue().toString())));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else
					criteria.add(criteriaBuilder.equal(requestMaster.get(entry.getKey()), entry.getValue().toString()));
			}
		}
		if (criteria.size() == 0) {
			throw new RuntimeException("no criteria");
		} else if (criteria.size() == 1) {
			criteriaQuery.where(criteria.get(0));
		} else {
			criteriaQuery.where(criteria.toArray(new Predicate[0]));
		}
		entityManager.createQuery(criteriaQuery).getResultList().forEach(reqMaster -> {

			log.debug("search details Request Id : {}  RequestBy : {}   RequestData : {}", reqMaster.getRequestId(),
					reqMaster.getRequestBy(), reqMaster.getRequestData().getRequestData());

		});
	}

	@Override
	public void setRequestEvent(RequestMaster requestMaster) {

	}

	public JsonObject getRequestCount() {

		String upn = userSession.getEmpData().getUserPrincipalName();
		JsonObject statusCount = new JsonObject();
		List<EmployeeRole> employeeRoles = roleManager.getEmpRoleByUpn(upn);
		for (Status status : Status.values()) {
			int count = 0;
			javax.persistence.Query q;
			BigInteger singleCount;

			for (EmployeeRole empRole : employeeRoles) {
				if (empRole.getRole().equals(Roles.SPOC.toString())) {
					count = 0;
					StringBuilder queryService = new StringBuilder();
					List<EmployeeRoleServiceMaster> employeeRoleServiceMasters = empRole.getEmployeeRoleServicemaster();

					for (EmployeeRoleServiceMaster employeeRoleServiceMaster : employeeRoleServiceMasters) {
						log.debug("services  {}  list size {}", employeeRoleServiceMaster.getService(),
								employeeRoleServiceMasters.size());
						queryService.append("u.service like '%" + employeeRoleServiceMaster.getService() + "%'");
						if (employeeRoleServiceMasters
								.indexOf(employeeRoleServiceMaster) < (employeeRoleServiceMasters.size() - 1))
							queryService.append(" OR ");
					}

					String query = "select count(u.requestId) from user_action u WHERE  u.ownerRole='"
							+ Roles.SPOC.toString() + "' AND u.location='" + empRole.getLocation() + "' AND ("
							+ queryService + ") AND u.status='" + status.toString() + "'";

					q = entityManager.createNativeQuery(query);

					singleCount = (BigInteger) q.getSingleResult();
					count += singleCount.intValue();

					q = entityManager
							.createNativeQuery("select count(u.requestId) FROM user_action u WHERE u.ownerUPN='" + upn
									+ "' AND u.ownerRole='SPOC' AND u.status='" + status.toString() + "'");
					singleCount = (BigInteger) q.getSingleResult();
					count += singleCount.intValue();

					statusCount.addProperty("SPOC" + status.toString(), count);
				}

				if (empRole.getRole().equals(Roles.ADMIN.toString())) {
					count = 0;
					q = entityManager
							.createNativeQuery("select count(u.requestId) FROM user_action u WHERE u.ownerUPN='" + upn
									+ "' AND u.ownerRole='ADMIN' AND u.status='" + status.toString() + "'");
					singleCount = (BigInteger) q.getSingleResult();
					count += singleCount.intValue();

					String querystr = "select count(u.requestId) u FROM user_action u  WHERE  u.ownerRole='ADMIN' AND u.location='"
							+ empRole.getLocation() + "' AND u.status='" + status.toString() + "'";
					q = entityManager.createNativeQuery(querystr);
					singleCount = (BigInteger) q.getSingleResult();
					count += singleCount.intValue();

					statusCount.addProperty("ADMIN" + status.toString(), count);

				}

				if (userSession.hasRole(Roles.APPROVAL_AUTHORITY)) {
					count = 0;
					q = entityManager
							.createNativeQuery("select count(u.requestId) FROM user_action u WHERE u.ownerUPN='" + upn
									+ "' AND u.ownerRole='" + Roles.APPROVAL_AUTHORITY.toString() + "'");
					singleCount = (BigInteger) q.getSingleResult();
					count += singleCount.intValue();

					statusCount.addProperty("APPROVAL_AUTHORITY" + status.toString(), count);

				}

			}
			count = 0;
			q = entityManager.createNativeQuery("select count(r.requestID) FROM request r WHERE r.requestBy='" + upn
					+ "' AND r.requestStatus='" + status.toString() + "'");

			singleCount = (BigInteger) q.getSingleResult();
			count += singleCount.intValue();
			statusCount.addProperty("REQUESTER" + status.toString(), count);
		}
		return statusCount;

	}

	@Override
	public List<RequestData> getProductRequisitionData(String service) {
		List<RequestData> reqId = crudService.findWithNamedQuery("RequestMaster.findRequestIDByService",
				QueryParameter.with("service", service).parameters());

		return reqId;

	}

	@Override
	public List<ProductDetail> getProductDetails(int pdtid) {

		return crudService.findWithNamedQuery("ProductDetail.findProductDetailById",
				QueryParameter.with("pdtId", pdtid).parameters());

	}

}
