package com.admin4u.service.bussiness.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.EmployeeRoleServiceMaster;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.Services;

@Transactional
@Stateless
public class RoleManagerImpl implements RoleManager {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private CrudService crudService;

	@Inject
	private IDMService idmService;

	@Override
	public String createRole(String[] upns, Roles role, String location, String[] services, String createdBy) {
		
		String respMsg="";

		
		
		for (String upn : upns) {

			if (idmService.getEmpByUpn(upn) != null) {
				if (!this.isRoleExist(upn, role)) {
					EmployeeRole employeeRole = new EmployeeRole();
					employeeRole.setRole(role.toString());
					employeeRole.setUserPrincipalName(upn);
					employeeRole.setLocation(location);
					employeeRole.setCreationDate(new Date());
					employeeRole.setCreatedBy(createdBy);
					employeeRole = crudService.create(employeeRole);

					if (role.toString().equalsIgnoreCase("SPOC")) {
						// List<EmployeeRoleServiceMaster> empRoleServList=new
						// ArrayList<>();
						 List<String> servs= Arrays.asList(services);
						 if(servs.contains("HotelGuestHouse"))
						 {
							 servs.remove("HotelGuestHouse");
							 servs.add(Services.HOTEL_BOOKING.toString());
							 servs.add(Services.GUEST_HOUSE_BOOKING.toString());
						 }
						
						for (String service : servs) {
							EmployeeRoleServiceMaster empRoleServMaster = new EmployeeRoleServiceMaster();
							empRoleServMaster.setService(service);
							empRoleServMaster.setEmployeeRole(employeeRole);
							empRoleServMaster.setActive(true);
							crudService.create(empRoleServMaster);
						}

					}
					respMsg=respMsg+role.toString() + " for " + idmService.getEmpByUpn(upn).getDisplayName() + " created.\n";
				}
				respMsg=respMsg+role.toString() + " for " + idmService.getEmpByUpn(upn).getDisplayName() + " already exist.\n";
			}

		}
		return respMsg;
	}

	@Override
	public boolean deleteRole(int employeeRoleID) {
		boolean isDeleted = false;

		EmployeeRole employeeRole = crudService.find(EmployeeRole.class, employeeRoleID);
		if (employeeRole.getRole().equalsIgnoreCase("spoc")) {
			List<EmployeeRoleServiceMaster> empRoleServMasterList = crudService.findWithNamedQuery(
					"EmployeeRoleServiceMaster.findByEmployeeRole",
					QueryParameter.with("employeeRole", employeeRole).parameters());
			for (EmployeeRoleServiceMaster ersm : empRoleServMasterList) {
				crudService.delete(EmployeeRoleServiceMaster.class, ersm.getId());
			}
			isDeleted = true;
		} else {
			crudService.delete(EmployeeRole.class, employeeRoleID);
			isDeleted = true;
		}
		return isDeleted;
	}

	@SuppressWarnings("unchecked")
	public List<EmployeeRole> getEmpByUpnAndRole(String upn, Roles role) {
		return crudService.findWithNamedQuery("EmployeeRole.findByUPNAndRole",
				QueryParameter.with("upn", upn).and("role", role.toString()).parameters());
	}

	private boolean isRoleExist(String upn, Roles role) {
		List<EmployeeRole> empRoleList = this.getEmpByUpnAndRole(upn, role);
		if (empRoleList.isEmpty() == false) {
			return true;
		}
		return false;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<EmployeeRole> getEmpByRole(String role, String location, String service) {
		List<EmployeeRole> empRole = null;
		if (role.equals("SPOC")) {
			empRole = crudService.findWithNamedQuery("EmployeeRole.findByRoleLocationService",
					QueryParameter.with("role", role).and("location", location).and("service", service).parameters());
		} else
			empRole = crudService.findWithNamedQuery("EmployeeRole.findByRoleLocation",
					QueryParameter.with("role", role).and("location", location).parameters());
		return empRole;

	}

	public List<String> getEmpRoleByUPN(String upn) {

		List<String> employeeRole = new ArrayList<String>();
		@SuppressWarnings("unchecked")
		List<EmployeeRole> empRoleList = crudService.findWithNamedQuery("EmployeeRole.findByUPN",
				QueryParameter.with("upn", upn).parameters());
		if (!empRoleList.isEmpty())
			for (EmployeeRole empRole : empRoleList) {
				employeeRole.add(empRole.getRole());
			}
		return employeeRole;
	}

	public List<EmployeeRole> getEmployeeRolesByCreater(String createrUPN, Roles role) {

		List<EmployeeRole> empRoleList = crudService.findWithNamedQuery("EmployeeRole.findByRoleCreater",
				QueryParameter.with("createrUPN", createrUPN).and("role", role.toString()).parameters());
		List<EmployeeRole> empROleWithDisplayName = new ArrayList<EmployeeRole>();
		for (EmployeeRole empRole : empRoleList) {
			empRole.setDisplayName(idmService.getEmpNameByUpn(empRole.getUserPrincipalName()));
			empROleWithDisplayName.add(empRole);
		}
		return empROleWithDisplayName;
	}

	@Override
	public List<EmployeeRole> getEmpRoleByUpn(String upn) {

		return crudService.findWithNamedQuery("EmployeeRole.findByUPN", QueryParameter.with("upn", upn).parameters());

	}

	public List<EmployeeRole> getRolesByLocation(String location, Roles role) {
		return crudService.findWithNamedQuery("EmployeeRole.findByRoleLocation",
				QueryParameter.with("role", role.toString()).and("location", location).parameters());

	}

	public List<EmployeeRole> getAllRole(Roles role) {
		return crudService.findWithNamedQuery("EmployeeRole.findAllRole",
				QueryParameter.with("role", role.toString()).parameters());
	}

}
