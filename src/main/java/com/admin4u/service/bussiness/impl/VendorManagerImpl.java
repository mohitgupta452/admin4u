
package com.admin4u.service.bussiness.impl;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.entity.Address;
import com.admin4u.persistence.entity.ContactDetail;
import com.admin4u.persistence.entity.ContractDetail;
import com.admin4u.persistence.entity.MisDetail;
import com.admin4u.persistence.entity.PaymentInfo;
import com.admin4u.persistence.entity.Paymentterm;
import com.admin4u.persistence.entity.Podetail;
import com.admin4u.persistence.entity.PrDetail;
import com.admin4u.persistence.entity.VendorInfo;
import com.admin4u.service.bussiness.contract.VendorManager;

@Stateless
@Transactional
public class VendorManagerImpl implements VendorManager
{


	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private CrudService crudService;
	/* start inserting methods */	
    @Override	
	public void insertVendorData(Map<String , String> vendorInfoMap , String sameAddress) {
     
    	
    		
    	if(Integer.parseInt(vendorInfoMap.get("id")) == 0){	
    	
		VendorInfo newVendorInfo = new VendorInfo();
		newVendorInfo = this.vendorInfoData(vendorInfoMap, newVendorInfo);
		
		ContactDetail contactDetail = new ContactDetail();
		contactDetail.setPhone(vendorInfoMap.get("companyPhone"));
		contactDetail.setEmail("email");
		contactDetail = crudService.create(contactDetail);
		newVendorInfo.setContactDetail(contactDetail);
	    newVendorInfo.setContactId(contactDetail.getId());
	    
	    if("on".equalsIgnoreCase(sameAddress)){
	    	
	  Address newAddress =	this.addressData(vendorInfoMap);
	  newVendorInfo.setAddress(newAddress);
	  newVendorInfo.setAddressId(newAddress);
	 
	    }
	    else{
	    	    Address newAddress = this.addressData(vendorInfoMap);
	    		newVendorInfo.setAddressId(newAddress);
	    		 
	    		  Address newMailAddress = new Address();
	    		  newMailAddress.setCity(vendorInfoMap.get("mCity"));
	    		  newMailAddress.setStreet(vendorInfoMap.get("mStreet"));
	    		  newMailAddress.setLocality(vendorInfoMap.get("mLocality"));
	    		  newMailAddress.setState(vendorInfoMap.get("mState"));
	    		  newMailAddress.setCountry(vendorInfoMap.get("mCountry")); 
	    		  newMailAddress.setType(vendorInfoMap.get("mtype"));
	    	      newMailAddress = crudService.create(newMailAddress);
	    		  newVendorInfo.setAddress(newMailAddress);
	    }
	    newVendorInfo	= crudService.create(newVendorInfo);
    	}
    	else
    	{
    		VendorInfo updateVendorInfo = crudService.find(VendorInfo.class,Integer.parseInt(vendorInfoMap.get("id")));
    		updateVendorInfo = this.vendorInfoData(vendorInfoMap, updateVendorInfo);
    		crudService.update(updateVendorInfo);
    	}
   
    }
    	
	
    
    private Address addressData(Map<String , String> vendorInfoMap)
    {
    	  Address newAddress = new Address();
    	  newAddress.setCity(vendorInfoMap.get("city"));
    	  newAddress.setStreet(vendorInfoMap.get("street"));
    	  newAddress.setLocality(vendorInfoMap.get("locality"));
    	  newAddress.setState(vendorInfoMap.get("state"));
    	  newAddress.setCountry(vendorInfoMap.get("country")); 
    	  newAddress.setType(vendorInfoMap.get("type"));
    	  newAddress = crudService.create(newAddress);
    	  return newAddress;
    }
    
    private VendorInfo vendorInfoData(Map<String, String> vendorInfoMap, VendorInfo  vendorInfo)
    {
    	vendorInfo.setBussinessDescription(vendorInfoMap.get("businessDescription"));
		vendorInfo.setCancelCheck(vendorInfoMap.get("cancelUpload"));
		vendorInfo.setCompany(vendorInfoMap.get("vendorName"));
		vendorInfo.setDeclLetter(vendorInfoMap.get("decUpload"));
		vendorInfo.setEinssn(Integer.parseInt(vendorInfoMap.get("identificationNo")));
		vendorInfo.setEscalAttach(vendorInfoMap.get("escUpload"));
		vendorInfo.setFax(vendorInfoMap.get("companyFax"));
		vendorInfo.setName(vendorInfoMap.get("vLegalName"));
		vendorInfo.setPanCard(vendorInfoMap.get("panUpload"));
		vendorInfo.setPhone(vendorInfoMap.get("companyPhone"));
		vendorInfo.setPreffDeliveryMethod(vendorInfoMap.get("deliveryMethod"));
		vendorInfo.setServicetax(Integer.parseInt(vendorInfoMap.get("serviceTax")));
		vendorInfo.setStateOfIncorporation(vendorInfoMap.get("stateIncorporation"));
		vendorInfo.setTin(vendorInfoMap.get("tin"));
		vendorInfo.setYearOfIncorporation(vendorInfoMap.get("incorporationYear"));
		return vendorInfo;
    }

	@Override
	public void insertMISBillingData(Map<String , String> paymentInfoMap) {
		
		if(Integer.parseInt(paymentInfoMap.get("id")) == 0){
			
	    PaymentInfo newPaymentInfo = new PaymentInfo();
	    
	    newPaymentInfo = this.paymentInfoData(paymentInfoMap , newPaymentInfo);
		newPaymentInfo = crudService.create(newPaymentInfo);
		
		MisDetail misDetails = new MisDetail();
		misDetails.setPaymentInfoId(newPaymentInfo);
		misDetails.setType("Billing MIS");
		misDetails = this.setMisReport(paymentInfoMap , misDetails);
		crudService.create(misDetails);
		
		}
		else
		{
			PaymentInfo updatePaymentInfo = crudService.find(PaymentInfo.class, Integer.parseInt(paymentInfoMap.get("id")));
			updatePaymentInfo = this.paymentInfoData(paymentInfoMap, updatePaymentInfo);
			crudService.update(updatePaymentInfo);
			
			MisDetail updatemisDetails = new MisDetail();
			updatemisDetails = this.setMisReport(paymentInfoMap , updatemisDetails);
			crudService.create(updatemisDetails);
		}
	}	
	
	private PaymentInfo paymentInfoData(Map<String,String>paymentInfoMap , PaymentInfo paymentInfo){
		
		/*Timestamp billDate = Timestamp.valueOf(paymentInfoMap.get("billDate")+":00.0");*/
	    Timestamp checkReceiveDate = Timestamp.valueOf(paymentInfoMap.get("checkReceiveDate")+":00.0");
		Timestamp invoiceDate = Timestamp.valueOf(paymentInfoMap.get("invoiceDate")+":00.0");
		/*Timestamp period = Timestamp.valueOf(paymentInfoMap.get("period")+":00.0");*/
	    /*Timestamp invoiceReceiveDate = Timestamp.valueOf(paymentInfoMap.get("invoiceReceiveDate")+":00.0");*/
			paymentInfo.setInvoiceAmt(paymentInfoMap.get("invoiceAmt"));
			paymentInfo.setInvoiceDate(invoiceDate);
			paymentInfo.setInvoiceNumber(paymentInfoMap.get("invoiceNo"));
			paymentInfo.setInvoiceProcessAmt(paymentInfoMap.get("processAmt"));
			paymentInfo.setLocation(paymentInfoMap.get("billLocation"));
			paymentInfo.setRemark(paymentInfoMap.get("billRemark"));
			paymentInfo.setCheckAmt(paymentInfoMap.get("checkAmt"));
			paymentInfo.setCheckReceiveDate(checkReceiveDate);
			paymentInfo.setCheckNo(paymentInfoMap.get("checkNo"));
			paymentInfo.setPoNo(paymentInfoMap.get("poNo"));
			return paymentInfo;
		}
	
/*	private MisDetail misDetailData(Map<String,String>paymentInfoMap , MisDetail misDetail){
		misDetail.setCompanyName(paymentInfoMap.get("company"));
		misDetail.setCostCenter(paymentInfoMap.get("costCentre"));
		misDetail.setNatureOfExpense(paymentInfoMap.get("expenses"));
		return  misDetail;
	}*/
		
	
	
	@Override
	public void insertMISContractData(Map<String , String > contractDetailsMap) {
		// TODO Auto-generated method stub
		
		if(Integer.parseInt(contractDetailsMap.get("id")) == 0){
		ContractDetail newContractDetails = new ContractDetail();
		newContractDetails = this.contractDetailsData(contractDetailsMap, newContractDetails);
		newContractDetails = crudService.create(newContractDetails);
		}
		else
		{
			ContractDetail updateContractDetails = crudService.find(ContractDetail.class,Integer.parseInt( contractDetailsMap.get("id")));
			updateContractDetails = this.contractDetailsData(contractDetailsMap, updateContractDetails);
			crudService.update(updateContractDetails);
		}
	}
	
	private ContractDetail contractDetailsData(Map<String , String> contractMap , ContractDetail contractDetails)
	{
		
		Timestamp leaseCommDate = Timestamp.valueOf(contractMap.get("leaseCommDate")+":00.0");
		Timestamp leaseEndDate = Timestamp.valueOf(contractMap.get("leaseEndDate")+":00.0");
		Timestamp lockPeriod = Timestamp.valueOf(contractMap.get("lockPeriod")+":00.0");
		Timestamp noticePeriod = Timestamp.valueOf(contractMap.get("noticePeriod")+":00.0");
		Timestamp escalationDate = Timestamp.valueOf(contractMap.get("escalationDate")+":00.0");
		Timestamp paymentDate = Timestamp.valueOf(contractMap.get("paymentDate")+":00.0");
		
		contractDetails.setAgreementType(contractMap.get("agreementType"));
		contractDetails.setAnyOtherFixedCost(contractMap.get("fixedCost"));
	    //newContractDetails.setContractDetailscol(contractDetails.getContractDetailscol());
	    contractDetails.setEscalation(contractMap.get("escalation"));
	    contractDetails.setLandlordName(contractMap.get("landlordName"));
		contractDetails.setLeaseCommencementDate(leaseCommDate);
		contractDetails.setLeaseEndDate(leaseEndDate);
		contractDetails.setLesseeName(contractMap.get("lesseeName"));
		contractDetails.setLessorName(contractMap.get("lesserName"));
		contractDetails.setLockInPeriod(lockPeriod);
		contractDetails.setMonthlyMaintenance(contractMap.get("monthlyMaintenance"));
		contractDetails.setMonthlyRent(contractMap.get("monthlyRent"));
	    contractDetails.setNextEscalationDate(escalationDate);
		contractDetails.setNoticePeriod(noticePeriod);
		contractDetails.setParkingCharges(contractMap.get("parkingCharges"));
        contractDetails.setPaymentDate(paymentDate);
		contractDetails.setPowerBackUpCharges(contractMap.get("powerBack"));
		contractDetails.setProcessToBeInitiated(contractMap.get("initiated"));
		contractDetails.setSecurityDeposit(contractMap.get("securityDeposit"));
		contractDetails.setTotalArea(contractMap.get("totalArea"));
		contractDetails.setTotalRentPayable(contractMap.get("totalRentPayable"));
		contractDetails.setUpload(contractMap.get("upload"));
		return contractDetails;
	}

	@Override
	public void insertMISCommercialData(Map<String , String > commercialMap) {
		
		if(Integer.parseInt(commercialMap.get("poId")) == 0 && Integer.parseInt(commercialMap.get("prId")) ==0){
		
		Podetail newPoDetail = new Podetail();
        newPoDetail = this.poDetailData(commercialMap, newPoDetail);
		newPoDetail = crudService.create(newPoDetail);
		
		PrDetail newPrDetail = new PrDetail();
        newPrDetail = this.prDetailData(commercialMap, newPrDetail);
		newPrDetail = crudService.create(newPrDetail);
		
		MisDetail misDetails = new MisDetail();
		misDetails.setPodetailsId(newPoDetail);
        misDetails.setPrDetailsId(newPrDetail);
		misDetails.setType("Commercial MIS");
		misDetails= this.setMisReport(commercialMap , misDetails);
		crudService.create(misDetails);
		}
		else
		{
			Podetail updatePoDetail = crudService.find(Podetail.class, Integer.parseInt(commercialMap.get("poId")));
			updatePoDetail = this.poDetailData(commercialMap, updatePoDetail);
			crudService.update(updatePoDetail);

			PrDetail updatePrDetail = crudService.find(PrDetail.class, Integer.parseInt(commercialMap.get("prId")));
			updatePrDetail = this.prDetailData(commercialMap, updatePrDetail);
		    crudService.create(updatePrDetail);
		}
		
	}
	
	private PrDetail prDetailData(Map<String , String > commercialMap , PrDetail prDetail ) 
	{

		Timestamp creationDate = Timestamp.valueOf(commercialMap.get("prCreationDate")+":00.0");
	/*	Timestamp releasedFromFh = Timestamp.valueOf(commercialMap.get("releasedFromFh")+":00.0");*/
		Timestamp releasedFromDeptGm = Timestamp.valueOf(commercialMap.get("releasedFromDeptGm")+":00.0");
		Timestamp releasedFromGhk = Timestamp.valueOf(commercialMap.get("releasedFromGhk")+":00.0");
		
		prDetail.setAmount(commercialMap.get("prAccount"));
		prDetail.setNumber(commercialMap.get("prNumber"));
		prDetail.setReleasedFromDeptGM(releasedFromDeptGm);
		prDetail.setCreationDate(creationDate);
		prDetail.setReleasedFromBHR(releasedFromGhk);
		return prDetail;
	}
	private Podetail poDetailData(Map<String , String > commercialMap , Podetail poDetail )
	{
		Timestamp from = Timestamp.valueOf(commercialMap.get("from")+":00.0");
		Timestamp releasedFromComm = Timestamp.valueOf(commercialMap.get("releasedFromComm")+":00.0");
		Timestamp createdBy = Timestamp.valueOf(commercialMap.get("createdBy")+":00.0");
		Timestamp to = Timestamp.valueOf(commercialMap.get("to")+":00.0");
		Timestamp releasedFromFinance = Timestamp.valueOf(commercialMap.get("releasedFromFinance")+":00.0");
		poDetail.setAmount(commercialMap.get("poAmount"));
		poDetail.setPonumber(commercialMap.get("poNumber"));
	    poDetail.setPoFrom(from);
		poDetail.setReleasedFromComm(releasedFromComm);
		poDetail.setCreatedBy(createdBy);
		poDetail.setTo(to);
		poDetail.setRelStatus(commercialMap.get("relStatus"));
        /*  newPoDetail.setEndDate(commercialMap.get(""));
		newPoDetail.setReleasedDate(commercialMap.get(""));*/
		poDetail.setReleasedFromFinance(releasedFromFinance);
		poDetail.setUpload(commercialMap.get("upload"));
		return poDetail;
	}
	
	private MisDetail setMisReport(Map<String , String >map , MisDetail misDetails)
	{
		MisDetail newMisDetails = new MisDetail();
		newMisDetails.setType(misDetails.getType());
		newMisDetails.setPaymentInfoId(misDetails.getPaymentInfoId());
		newMisDetails.setPodetailsId(misDetails.getPodetailsId());
		newMisDetails.setPrDetailsId(misDetails.getPrDetailsId());
		misDetails.setCompanyName(map.get("company"));
		misDetails.setCostCenter(map.get("costCentre"));
		misDetails.setNatureOfExpense(map.get("expenses"));
		
		return misDetails;
		/*crudService.create(newMisDetails);*/
	}
	
	@Override
	public void insertPaymentData(Map<String ,String> paymentMap) {
		// TODO Auto-generated method stub
		Paymentterm newPayment = new Paymentterm();
		newPayment.setAdvance(paymentMap.get("advance"));
		newPayment.setPaymentTerms(paymentMap.get("paymentTerms"));
		newPayment.setPenaltyTerms(paymentMap.get("penalityTerm"));
		newPayment.setShippingMethod(paymentMap.get("shippingMethod"));
		newPayment.setShippingTerms(paymentMap.get("shippingTerms"));
		newPayment.setIsActive((byte)1);
		crudService.create(newPayment);
	}
	
	
	@Override
	public void insertContactData(Map<String , String> contactMap) {
		
		ContactDetail newContactDetail = new ContactDetail();
		newContactDetail.setContactPerson(contactMap.get("firstName"));
		newContactDetail.setEmail(contactMap.get("email"));
		newContactDetail.setMobile(contactMap.get("contactNo"));
        newContactDetail.setName(contactMap.get("companyName"));
        newContactDetail.setPhone(contactMap.get("contactNo"));
        newContactDetail.setType(contactMap.get("type"));
        newContactDetail.setSalutation(contactMap.get("salutation"));
        newContactDetail.setDesignation(contactMap.get("designation"));
        newContactDetail.setDepartment(contactMap.get("companyDepart"));
        newContactDetail.setStateOfIncorporation(contactMap.get("stateIncorporation"));
        newContactDetail.setYearOfIncorporation(contactMap.get("contactIncorporation"));
        newContactDetail.setPreffDeliveryMethod(contactMap.get("deliveryMethod"));
        newContactDetail.setActive((byte)1);
        
        crudService.create(newContactDetail);
	}

/* end inserting methods */	
	
	
	
	/* fetch method starts*/
	@Override
	public List<VendorInfo> getVendorData() {
		
		return crudService.findWithNamedQuery("VendorInfo.findAll");
	}

	@Override
	public List<ContactDetail> getContactDetailsData() {
		
		return crudService.findWithNamedQuery("ContactDetail.findAll");
	}

	@Override
	public List<Paymentterm> getPaymentDetailsData() {
		
	     return crudService.findWithNamedQuery("Paymentterm.findAll");
	}

	@Override
	public List<PaymentInfo> getBillingDetailsData() {
		
		return crudService.findWithNamedQuery("PaymentInfo.findAll");
	}

	@Override
	public List<ContractDetail> getContractDetailsData() {
		
		return crudService.findWithNamedQuery("ContractDetail.findAll");
	}

	@Override
	public List<Podetail> getPoDetailsData(){
		return crudService.findWithNamedQuery("Podetail.findAll");
	}
	
	@Override
	public List<PrDetail> getPrDetailsData(){
		 
		return crudService.findWithNamedQuery("PrDetail.findAll");
	}
	

	@Override
	public List<MisDetail> getMisDetailData() {
		
		return crudService.findWithNamedQuery("MISDetails.findAll");
	}

	
	// Inactive functions impl
	
	@Override
	public boolean inActiveThePaymentRow(int paymentId) {
		
		Paymentterm payment = crudService.find(Paymentterm.class, paymentId);
		payment.setIsActive((byte)0);
		crudService.update(payment);
		return true;
		
	}

	@Override
	public boolean inActiveTheContactRow(int contactId) {
		
		ContactDetail contactDetail = crudService.find(ContactDetail.class, contactId);
		contactDetail.setActive((byte)0);
		crudService.update(contactDetail);
		
		return true;
	}

	// for cab vendor in bookticket (admin)
	@Override
	public List<String> allVendorName(){
		return crudService.findWithNamedQuery("VendorInfo.findAllVendorName");
	}
	

	/* fetch method ends */

}
