package com.admin4u.service.bussiness.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.dto.EmployeeDTO;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.service.bussiness.contract.ADManager;
import com.unboundid.asn1.ASN1OctetString;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.controls.SimplePagedResultsControl;
import com.unboundid.util.LDAPTestUtils;

/**
 *
 * @author Ashish Singh Dev
 *
 */
@Named("ADManager")
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ADManagerImpl implements ADManager {

	private static final long serialVersionUID = -591646180607646279L;

	private static Logger log = LoggerFactory.getLogger(ADManagerImpl.class);

	@Inject
	private CrudService crudService;

	private final String returnedAtts[] = { "initials", "displayName", "title", "mail", "employeeID", "mobile",
			"manager", "distinguishedName", "l", "st", "telephoneNumber", "company", "department",
			"userPrincipalName" };
	// private final Hashtable<String, String> env = new Hashtable<String,
	// String>(15);

	@PostConstruct
	private void initData() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EmployeeDTO> getAllRecords() {

		List<EmployeeDTO> empDTOList = new ArrayList<>();
		List<EmployeeData> empDataList = crudService.findWithNamedQuery("EmployeeData.findAll");

		for (EmployeeData empData : empDataList) {
			empDTOList.add(convert(empData));
		}

		return empDTOList;

	}

	private String buildSearchFilter(String searchText, SearchCriteria criteria) {

		String searchFilter = null;

		switch (criteria) {

		case BY_CONTACT_NO:
			searchFilter = "(&(objectClass=person)(|(telephoneNumber=%s)(mobile=%s)))";
			searchFilter = String.format(searchFilter, searchText, searchText);
			break;
		case BY_EMAIL:
			searchFilter = "(&(objectClass=person)(mail=%s))";
			searchFilter = String.format(searchFilter, searchText);
			break;
		case BY_EMP_CODE:
			searchFilter = "(&(objectClass=person)(employeeID=%s))";
			searchFilter = String.format(searchFilter, searchText);
			break;
		case BY_EMP_NAME:
			searchFilter = "(&(objectClass=person)(displayName=%s))";
			searchFilter = String.format(searchFilter, searchText + "*");
			break;
		case ALL:
			searchFilter = "(&(objectClass=person)(objectClass=organizationalPerson)(objectClass=user)(userPrincipalName=*))";
			break;
		case BY_UPN:
			searchFilter = "(&(objectClass=person)(userPrincipalName=%s))";
			searchFilter = String.format(searchFilter, searchText, searchText);
			break;
		default:
			searchFilter = "(&(objectClass=person)(mail=%s))";
			searchFilter = String.format(searchFilter, searchText);
			break;
		}

		return searchFilter;
	}

	private EmployeeData mapAttrsToEntity(SearchResultEntry entry) {

		EmployeeData empData = new EmployeeData();

		Collection<Attribute> attributes = entry.getAttributes();

		java.util.Iterator<Attribute> iter = attributes.iterator();
		while (iter.hasNext()) {

			Attribute attr = iter.next();

			String attributeKey = attr.getName();

			switch (attributeKey) {

			case "initials":
				empData.setInitials(attr.getValue());
				break;
			case "displayName":
				empData.setDisplayName(attr.getValue());
				break;
			case "employeeID":
				empData.setEmployeeID(attr.getValue());
				break;
			case "mail":
				empData.setMail(attr.getValue());
				break;
			case "title":
				empData.setDesignation(attr.getValue());
				break;
			case "mobile":
				empData.setMobile(attr.getValue());
				break;
			case "l":
				empData.setL(attr.getValue());
				break;
			case "st":
				empData.setSt(attr.getValue());
				break;
			case "distinguishedName":
				empData.setDistinguishedName(attr.getValue());
				break;
			case "telephoneNumber":
				empData.setTelephoneNumber(attr.getValue());
				break;
			case "company":
				empData.setCompany(attr.getValue());
				break;
			case "department":
				empData.setDepartment(attr.getValue());
				break;
			case "userPrincipalName":
				empData.setUserPrincipalName(attr.getValue());
				break;
			case "manager":
				empData.setManagerDN(attr.getValue());
				break;

			}

			log.debug("Attribute: {}, Value : {}", attr.getName(), attr.getValue());
		}

		return empData;
	}

	private List<EmployeeData> search(LDAPConnection conn, String searchText, SearchCriteria criteria)
			throws LDAPException {

		List<EmployeeData> empDataList = new ArrayList<>();

		String searchFilter = buildSearchFilter(searchText, criteria);
		SearchRequest searchRequest = null;

		if (conn.getConnectedIPAddress().equals("192.168.17.171")) {
			searchRequest = new SearchRequest("DC=dalmiabharat,DC=com", SearchScope.SUB, searchFilter, returnedAtts);
		} else if (conn.getConnectedIPAddress().equals("192.168.16.133")) {
			searchRequest = new SearchRequest("DC=ocl,DC=dalmiabharat,DC=com", SearchScope.SUB, searchFilter,
					returnedAtts);
		}

		searchRequest.setSizeLimit(50000);

		ASN1OctetString resumeCookie = null;

		int count = 0;
		while (true) {
			searchRequest.setControls(new SimplePagedResultsControl(100, resumeCookie));

			SearchResult searchResult = conn.search(searchRequest);

			for (SearchResultEntry e : searchResult.getSearchEntries()) {
				count++;
				empDataList.add(mapAttrsToEntity(e));

			}

			LDAPTestUtils.assertHasControl(searchResult, SimplePagedResultsControl.PAGED_RESULTS_OID);
			SimplePagedResultsControl responseControl = SimplePagedResultsControl.get(searchResult);
			if (responseControl.moreResultsToReturn()) {
				resumeCookie = responseControl.getCookie();
			} else {
				break;
			}
		}

		log.debug("Found Records:{}", count);

		return empDataList;
	}

	@Override
	@Transactional
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	//@TransactionTimeout(unit = TimeUnit.MINUTES, value = 60)
	// @Schedule(year = "*", minute = "0/3", hour = "*", dayOfMonth = "*")
	public void syncADRecords() {

		List<EmployeeData> empDataList = new ArrayList<>();
		LDAPConnectionOptions conOptions = new LDAPConnectionOptions();
		conOptions.setFollowReferrals(false);

		LDAPConnection connOCL = null;
		LDAPConnection connDalmia = null;

		try {
			connOCL = new LDAPConnection(conOptions, "192.168.16.133", 389, "amsprojectteam@ocl.dalmiabharat.com",
					"welcome123!@");

			connDalmia = new LDAPConnection(conOptions, "192.168.17.171", 389, "amsprojectteam@dalmiabharat.com",
					"welcome123!@");

			empDataList.addAll(search(connDalmia, null, SearchCriteria.ALL));

			empDataList.addAll(search(connOCL, null, SearchCriteria.ALL));

		} catch (LDAPException e) {
			e.printStackTrace();
		} finally {
			connDalmia.close();
			connOCL.close();
		}

		for (EmployeeData empData : empDataList) {

			List<EmployeeData> storedEmployeeList = crudService.findWithNamedQuery("EmployeeData.find_By_UPN",
					QueryParameter.with("upn", empData.getUserPrincipalName()).parameters(), 1);

			EmployeeData foundEmp = null;

			if (storedEmployeeList != null && !storedEmployeeList.isEmpty()) {
				foundEmp = storedEmployeeList.get(0);
				empData.setEmpDataID(foundEmp.getEmpDataID());
				crudService.update(empData);
			} else {
				crudService.create(empData);
			}

		}

	}

	@Override
	public EmployeeDTO searchByUPN(String upn) {
		@SuppressWarnings("unchecked") // "EmployeeData.find_By_UPN"
		List<EmployeeData> empDataList = crudService.findWithNamedQuery("EmployeeData.find_By_UPN",
				QueryParameter.with("upn", upn.toLowerCase()).parameters(), 1);
		if (empDataList.isEmpty()) {
			return null;
		}
		return convert(empDataList.get(0));

	}

	private EmployeeData convert(EmployeeDTO empDTO) {

		EmployeeData empData = new EmployeeData();

		empData.setCompany(empDTO.getCompany());
		empData.setDepartment(empDTO.getDepartment());
		empData.setDesignation(empDTO.getDesignation());
		empData.setDisplayName(empDTO.getEmpName());
		empData.setDistinguishedName(empDTO.getEmpDN());
		empData.setEmployeeID(empDTO.getEmpID());
		empData.setInitials(empDTO.getInitials());
		empData.setL(empDTO.getLocation());
		empData.setMail(empDTO.getEmail());
		empData.setManagerDN(empDTO.getManagerDN());
		empData.setMobile(empDTO.getMobile());
		empData.setSt(empDTO.getState());
		empData.setTelephoneNumber(empDTO.getTelephoneNo());
		empData.setUserPrincipalName(empDTO.getUserPrincipalName());

		return empData;
	}

	private EmployeeDTO convert(EmployeeData empData) {

		EmployeeDTO empDTO = new EmployeeDTO();

		empDTO.setRowID(empData.getEmpDataID());
		empDTO.setCompany(empData.getCompany());
		empDTO.setDepartment(empData.getDepartment());
		empDTO.setDesignation(empData.getDesignation());
		empDTO.setEmpName(empData.getDisplayName());
		empDTO.setEmpDN(empData.getDistinguishedName());
		empDTO.setEmpID(empData.getEmployeeID());
		empDTO.setInitials(empData.getInitials());
		empDTO.setLocation(empData.getL());
		empDTO.setEmail(empData.getMail());
		empDTO.setManagerDN(empData.getManagerDN());
		empDTO.setMobile(empData.getMobile());
		empDTO.setState(empData.getSt());
		empDTO.setTelephoneNo(empData.getTelephoneNumber());
		empDTO.setUserPrincipalName(empData.getUserPrincipalName());

		return empDTO;
	}

}
