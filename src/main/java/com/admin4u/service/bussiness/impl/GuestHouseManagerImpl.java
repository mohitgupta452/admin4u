
package com.admin4u.service.bussiness.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.mortbay.log.Log;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.Location;
import com.admin4u.persistence.entity.MeetingRoomBookingDetail;
import com.admin4u.persistence.entity.NotificationMaster;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.ResponseMaster;
import com.admin4u.persistence.entity.RoomStuff;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.bussiness.contract.ActionManager;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.NotificationManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.AllocationStatus;
import com.admin4u.util.enums.Services;
import com.admin4u.util.enums.StuffNameCodeMap;
import com.admin4u.util.service.ApplicationMetaData.Status;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Transactional
@Stateless
public class GuestHouseManagerImpl implements GuestHouseManager {

	@Inject
	private CrudService crudService;
	
	@Inject
	private ActionManager actionManager;
	
	@Inject
	private NotificationManager notificationManager;
	
	@Inject
	private RequestResponseManager reqResManager;
	
	
	public GuestHouseManagerImpl()
	{
	}

	@Override
	public String addNewGuestHouseWithRooms(SMBuilding guestHouse, List<SMRoom> roomList, int locationId) {
		Location location = new Location();
		location.setLocationID(locationId);
		guestHouse.setLocation(location);
		guestHouse.setBuildingType("GUESTHOUSE");

		for (SMRoom room : roomList) {
			room.setRoomType("GUESTHOUSEROOM");
			room.setAllocationStatus(AllocationStatus.UNALLOCATED.toString());
			room.setBuilding(guestHouse);

		}

		guestHouse.setRoomList(roomList);

		SMBuilding building = crudService.create(guestHouse);

		return "{" + "\"ghId\":\"" + building.getId() + "\"}";
	}

	@Override
	public boolean addGuestHouseRoomDetails(List<RoomStuff> roomStuffList, int roomId) {
		SMRoom room = new SMRoom();
		room.setId(roomId);
		for (RoomStuff roomStuff : roomStuffList) {
			roomStuff.setRoom(room);
			roomStuff.setRoomStuffId(0);
			crudService.create(roomStuff);
		}

		return true;
	}

	@Override
	public List<SMBuilding> getGuestHouseByLocation(int locationID) {
		Location location = new Location();
		location.setLocationID(locationID);
		String buildingType = "GUESTHOUSE";
		List<SMBuilding> buildingList = crudService.findWithNamedQuery("SMBuilding.findByLocationAndType",
				QueryParameter.with("location", location).and("buildingType", buildingType).parameters());
		return buildingList;
	}

	@Override
	public List<SMRoom> getRoomListByGuestHouseId(int buildingId) {
		SMBuilding building = new SMBuilding();
		building.setId(buildingId);
		List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByBuilding",
				QueryParameter.with("building", building).parameters());
		return roomList;
	}

	@Override
	public List<RoomStuff> getRoomStuffByRoomId(int roomId) {
		SMRoom room = new SMRoom();
		room.setId(roomId);
		List<RoomStuff> roomStuffList = crudService.findWithNamedQuery("RoomStuff.findByRoom",
				QueryParameter.with("room", room).parameters());
		return roomStuffList;

	}

	@Override
	public boolean deleteRoomStuff(List<Integer> roomStuffIdList) {
		for (int roomStuffId : roomStuffIdList) {
			crudService.delete(RoomStuff.class, roomStuffId);
		}
		return true;
	}

	@Override
	public boolean updateRoomStuffsByRoomId(int roomId, List<String> stuffNameList) {// Incomplete
																						// method
		SMRoom room = new SMRoom();
		room.setId(roomId);
		for (String stuffName : stuffNameList) {
			List<RoomStuff> roomStuffList = crudService.findWithNamedQuery("RoomStuff.findByRoomAndStuffName",
					QueryParameter.with("room", room).and("stuffName", stuffName).parameters());
			if (roomStuffList.isEmpty()) {
				RoomStuff roomStuff = new RoomStuff(); // Incomplete method not is use yet
				roomStuff.setRoom(room);
				roomStuff.setStuffName(stuffName);
				crudService.create(roomStuff);

			} else {

			}
		}
		return true;
	}

	@Override
	public boolean deleteRoomStuffByRoomAndStuffName(int roomId, List<String> stuffNameList) {
		SMRoom room = new SMRoom();
		room.setId(roomId);
		for (String stuffName : stuffNameList) {
			List<RoomStuff> roomStuffList = crudService.findWithNamedQuery("RoomStuff.findByRoomAndStuffName",
					QueryParameter.with("room", room).and("stuffName", stuffName).parameters());
			if (!roomStuffList.isEmpty()) {
				crudService.delete(RoomStuff.class, roomStuffList.get(0).getId());
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean addRoomStuffByRoomAndStuffName(int roomId, String stuffName) {
		SMRoom room = new SMRoom();
		room.setId(roomId);
		RoomStuff roomStuff = new RoomStuff();
		roomStuff.setRoom(room);
		roomStuff.setStuffName(stuffName);
		crudService.create(roomStuff);

		return true;
	}

	@Override
	public boolean deleteRoomStuffByRoomAndStuffName(int roomId, String stuffName) {

		SMRoom room = new SMRoom();
		room.setId(roomId);

		List<RoomStuff> roomStuffList = crudService.findWithNamedQuery("RoomStuff.findByRoomAndStuffName",
				QueryParameter.with("room", room).and("stuffName", stuffName).parameters());
		if (!roomStuffList.isEmpty()) {
			crudService.delete(RoomStuff.class, roomStuffList.get(0).getId());
			return true;
		}

		return false;
	}

	@Override
	public boolean allocateRoom(int roomId,int requestId,int actionId,String action,Date checkInDateTime,Date checkOutDateTime,String spocComment){
		RequestMaster requestMaster=crudService.find(RequestMaster.class, requestId);
		SMRoom room=crudService.find(SMRoom.class, roomId);
		boolean isRoomAvailable=false;
		

		if(AllocationStatus.UNALLOCATED.toString().equalsIgnoreCase(room.getAllocationStatus())){
			
			
			
			try{
			isRoomAvailable=this.processGuestHouseRequest(requestMaster, roomId,checkInDateTime,checkOutDateTime);
			}
			catch(ParseException p)
			{
				Log.debug("exception in {} method AllocateRoom() {} ",this.getClass().getName(),p.getStackTrace());
			}

			
//saving response
			if(isRoomAvailable){
			ResponseMaster response=new ResponseMaster();
			JsonObject responseJson=new JsonObject();
			responseJson.addProperty("name", room.getBuilding().getBuildingName());
			responseJson.addProperty("address", room.getBuilding().getBuildingAddress());
			responseJson.addProperty("typeType", room.getRoomTypeByCapacity());
			responseJson.addProperty("roomNumber", room.getRoomCode());
			responseJson.addProperty("spocComment", spocComment);

			responseJson.addProperty("serviceList","GUEST_HOUSE_BOOKING");

			JsonObject jsonData=new JsonObject();
			jsonData=new Gson().fromJson(requestMaster.getRequestData().getRequestData(),JsonObject.class);
			responseJson.addProperty("checkIn",checkInDateTime.toString() );
			responseJson.addProperty("checkOut",checkOutDateTime.toString() );

			response.setResponseData(new Gson().toJson(responseJson));
			response.setRequestMaster(requestMaster);
			crudService.create(response);
			if("SPOC_SERV".equalsIgnoreCase(action))
			actionManager.guestHouseSpocServe(actionId);
			else if("ADMIN_SERV".equalsIgnoreCase(action))
			actionManager.guestHouseAdminServe(actionId);
			Log.debug("inside method Allocate room of class={}",this.getClass().getName());
				
			return true;
			}
			//end saving response
			
			
			//room.setEmployeeUPN(employeeUpn);
			/*room.setAllocationStatus(AllocationStatus.ALLOCATED.toString());

			crudService.update(room);*/
			return false;
		}

		return false;
	}

	@Override
	public boolean unallocateRoom(int roomId) {

		SMRoom room = crudService.find(SMRoom.class, roomId);
		if (AllocationStatus.ALLOCATED.toString().equalsIgnoreCase(room.getAllocationStatus())) {
			room.setAllocationStatus(AllocationStatus.UNALLOCATED.toString());
			// room.setEmployeeUPN(employeeUpn);
			crudService.update(room);
			return true;
		}

		return false;
	}
	
	
	public boolean processGuestHouseRequest(RequestMaster requestMaster,int roomId,Date checkInDateTime,Date checkOutDateTime) throws ParseException
	
	{
		SMRoom room=new SMRoom();
		room.setId(roomId);
		
		JsonObject requestData=requestMaster.getRequestData().getRequestData();
		String servicePrefix=requestMaster.getService().toLowerCase();
		if(Services.HOTEL_BOOKING.toString().equalsIgnoreCase(requestMaster.getService())||Services.GUEST_HOUSE_BOOKING.toString().equalsIgnoreCase(requestMaster.getService())){
			servicePrefix="hotelguesthouse";
		}
				
		String checkin = requestData.get(servicePrefix+".checkindatetime").toString().replaceAll("\"", "");
		String checkOut = requestData.get(servicePrefix+".checkoutdatetime").toString().replaceAll("\"", "");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Log.debug("checkin {} checkout {}",checkin,checkOut);
		Date checkinTime= sdf.parse(checkin);
		
		Date checkOutTime= sdf.parse(checkOut);
		
		if(!(checkinTime.equals(checkInDateTime)&& checkOutDateTime.equals(checkOutTime))){
			NotificationMaster notification=new NotificationMaster();
			notification.setReciever(requestMaster.getRequestBy());
			notification.setCreationTime(new Date());
			notification.setSender("system Genrated");
			notification.setMessage("Your Hotel/Guest House booking time has been revised for the request id-"+requestMaster.getRequestId());
			crudService.create(notification);
			
		}
		

		
		MeetingRoomBookingDetail guestRoomBookingDetail= new MeetingRoomBookingDetail();
		
		guestRoomBookingDetail.setBookingStartDateTime(checkInDateTime);
		guestRoomBookingDetail.setBookingEndDateTime(checkOutDateTime);
		guestRoomBookingDetail.setMeetingRoom(room);
		guestRoomBookingDetail.setRequest(requestMaster);
		
		if(checkGuestRoomAvailability(roomId,  checkinTime, checkOutTime)){

		crudService.create(guestRoomBookingDetail);
		return true;
		}

		return  false;
	}

@Override
public boolean processGuestHouseRequestNew(int actionId,int roomId,Date checkInDateTime,Date checkOutDateTime) 
	
	{
		UserActionMaster uam=crudService.find(UserActionMaster.class, actionId);
		
		RequestMaster requestMaster=uam.getRequestMaster();
		String servicePrefix=requestMaster.getService().toLowerCase();
		if(Services.HOTEL_BOOKING.toString().equalsIgnoreCase(requestMaster.getService())||Services.GUEST_HOUSE_BOOKING.toString().equalsIgnoreCase(requestMaster.getService())){
			servicePrefix="hotelguesthouse";
		}
		SMRoom room=new SMRoom();
		room.setId(roomId);
		
		JsonObject requestData=requestMaster.getRequestData().getRequestData();
				
		String checkin = requestData.get(servicePrefix+".checkindatetime").toString().replaceAll("\"", "");
		String checkOut = requestData.get(servicePrefix+".checkoutdatetime").toString().replaceAll("\"", "");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Log.debug("checkin {} checkout {}",checkin,checkOut);
		Date checkinTime=new Date();
		
		Date checkOutTime= new Date();
		
		try{
			checkinTime= sdf.parse(checkin);
			checkOutTime= sdf.parse(checkOut);
			
		}catch(ParseException e){
			Log.debug("Exception in method {}",e.getMessage());
		}
		
		if(!(checkinTime.equals(checkInDateTime)&& checkOutDateTime.equals(checkOutTime))){
			NotificationMaster notification=new NotificationMaster();
			notification.setReciever(requestMaster.getRequestBy());
			notification.setCreationTime(new Date());
			notification.setSender("system Genrated");
			notification.setMessage("Your Hotel/Guest House booking time has been revised for the request id-"+requestMaster.getRequestId());
			crudService.create(notification);
			
		}
		

		
		MeetingRoomBookingDetail guestRoomBookingDetail= new MeetingRoomBookingDetail();
		
		guestRoomBookingDetail.setBookingStartDateTime(checkInDateTime);
		guestRoomBookingDetail.setBookingEndDateTime(checkOutDateTime);
		guestRoomBookingDetail.setMeetingRoom(room);
		guestRoomBookingDetail.setRequest(requestMaster);
		
		if(checkGuestRoomAvailability(roomId,  checkinTime, checkOutTime)){

		crudService.create(guestRoomBookingDetail);
		return true;
		}

		return  false;
	}

	
	
	@Override
	public List<SMRoom> getRoomListByOccupacyType(int buildingId, String occupacyType) {
		SMBuilding building = new SMBuilding();
		building.setId(buildingId);
		List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByBuildingAndOccupacyType",
				QueryParameter.with("building", building).and("occupacyType", occupacyType).parameters());
		return roomList;
	}
	
	

	@Override
	public List<Map<String,String>> getGuestHouseRoomCounts(int locationId){
		
		Location location=new Location();
		location.setLocationID(locationId);
		String buildingType="GUESTHOUSE";
		List<SMBuilding> buildingList=crudService.findWithNamedQuery("SMBuilding.findByLocationAndType",QueryParameter.with("location", location).and("buildingType", buildingType).parameters());
	//Log.debug("{}",buildingList);	
		
		//Map<String,String> responseMap=new HashMap<>();
		List<Map<String,String>> responseMapList=new ArrayList<>();
		for(SMBuilding building:buildingList){
			int singleOccupancyRoomCount=0;
			int doubleOccupancyRoomCount=0;
			int tripleOccupancyRoomCount=0;
			Map<String,String> responseMap=new HashMap<>();	
			for(SMRoom room:building.getRoomList()){
				if("SINGLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
					singleOccupancyRoomCount++;
				}
				if("DOUBLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
					doubleOccupancyRoomCount++;
				}
				if("TRIPLE".equalsIgnoreCase(room.getRoomTypeByCapacity())){
					tripleOccupancyRoomCount++;
				}
			}
			
			responseMap.put("GuestHouseName", building.getBuildingName());
			responseMap.put("guestHouseId", building.getId()+"");
			responseMap.put("singleOccupancy", singleOccupancyRoomCount+"");
			responseMap.put("doubleOccupancy", doubleOccupancyRoomCount+"");
			responseMap.put("tripleOccupancy", tripleOccupancyRoomCount+"");
			responseMapList.add(responseMap);
			
		}
		return responseMapList;

		
	}


	@Override
	public List<MeetingRoomBookingDetail> getMeetingRoomByReqId(RequestMaster requestMaster) {
		return crudService.findWithNamedQuery("MeetingRoomBookingDetail.findDetailByRequestId",
				QueryParameter.with("request", requestMaster).parameters());

	}
	
	@Override
	public boolean checkGuestRoomAvailability(int roomId, Date startDateTime, Date endDateTime) {
		SMRoom meetingRoom = crudService.find(SMRoom.class, roomId);
		List<MeetingRoomBookingDetail> bookingDetailList = crudService.findWithNamedQuery(
				"MeetingRoomBookingDetail.findRoomAvailability", QueryParameter.with("meetingRoom", meetingRoom)
						.and("startDateTime", startDateTime).and("endDateTime", endDateTime).parameters());
		if (bookingDetailList.isEmpty())
			return true;
		else
			return false;

	}
	
	@Override
	public List<MeetingRoomBookingDetail> getRoomHistory(int roomId) {
		SMRoom meetingRoom = crudService.find(SMRoom.class, roomId);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date fromDate=new Date();

		
		List<MeetingRoomBookingDetail> roomBookingHistory= crudService.findWithNamedQuery(
				"MeetingRoomBookingDetail.findRoomHistory", QueryParameter.with("meetingRoom", meetingRoom)
						.and("fromDate", fromDate).parameters());
		
			return roomBookingHistory;

	}
	
	@Override
public Map<String,String> getCheckinCheckOutTime(int requestId) 
	
	{
		
		RequestMaster requestMaster=crudService.find(RequestMaster.class, requestId);
		SMRoom room=new SMRoom();
		
		JsonObject requestData=requestMaster.getRequestData().getRequestData();
		String 	servicePrefix=requestMaster.getService().toLowerCase();

		if(Services.HOTEL_BOOKING.toString().equalsIgnoreCase(requestMaster.getService())||Services.GUEST_HOUSE_BOOKING.toString().equalsIgnoreCase(requestMaster.getService())){
			servicePrefix="hotelguesthouse";
		}
		
		Log.debug("value of prefix{}",servicePrefix);
		String checkin = requestData.get(servicePrefix+".checkindatetime").toString().replaceAll("\"", "");
		String checkOut = requestData.get(servicePrefix+".checkoutdatetime").toString().replaceAll("\"", "");
		String location = requestData.get(servicePrefix+".hotellocation").toString().replaceAll("\"", "");
		Log.debug("value of location{}",location);

		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Log.debug("checkin {} checkout {}",checkin,checkOut);
		Map<String,String>responseMap=new HashMap<>();
		try{
		Date checkinTime= sdf.parse(checkin);
		
		Date checkOutTime= sdf.parse(checkOut);
		responseMap.put("checkInTime", checkin);
		responseMap.put("checkOutTime", checkOut);
		responseMap.put("location", location);
		}
		catch (ParseException e)
		{
			Log.debug("exception during parsing {}",e.getMessage());
		}
		
		
		return  responseMap;
	}

@Override	
public	Map<String,String[]> getRoomDetailMap(int roomId){
		SMRoom room=crudService.find(SMRoom.class, roomId);
		Map<String,String[]> responseMap=new HashMap<>();
		responseMap.put("nameAddress ", new String[]{room.getBuilding().getBuildingName()});
		//responseMap.put("address",new String[] {room.getBuilding().getBuildingAddress()});
		responseMap.put("typeType", new String[]{room.getRoomTypeByCapacity()});
		responseMap.put("roomNumber", new String[]{room.getRoomCode()});


		
		return responseMap;
		
		
	}

@Override
public boolean detleteRoom(int roomId){
		crudService.delete(SMRoom.class, roomId);
	
	return true;
}


public boolean updateBedType(int roomId,String bedType){
	
	SMRoom room = new SMRoom();
	room.setId(roomId);
		List<RoomStuff> roomStuffList = crudService.findWithNamedQuery("RoomStuff.findByRoomAndStuffId",
				QueryParameter.with("room", room).and("roomStuffId", StuffNameCodeMap.BED).parameters());
		if (roomStuffList.isEmpty()) {
			RoomStuff roomStuff = new RoomStuff(); 
			roomStuff.setRoom(room);
			roomStuff.setRoomStuffId(StuffNameCodeMap.BED);
			roomStuff.setStuffName(bedType);
			crudService.create(roomStuff);

		} else {
			
			RoomStuff roomStuff = roomStuffList.get(0); 
			roomStuff.setStuffName(bedType);
			crudService.update(roomStuff);
			

		}
	return true;
	
	
	
}

public boolean updateRoomCode(int roomId,String roomCode){
	SMRoom room=crudService.find(SMRoom.class, roomId);
	room.setRoomCode(roomCode);
	crudService.update(room);
	return true;
}

	
public boolean deleteGuestHouse(int guestHouseId){
	
	crudService.delete(SMBuilding.class, guestHouseId);
	return true;
}



}
