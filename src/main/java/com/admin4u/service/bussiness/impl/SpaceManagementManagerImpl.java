package com.admin4u.service.bussiness.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.dto.SpaceManagementManager;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.persistence.entity.SMSeat;
import com.admin4u.util.enums.AllocationStatus;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.java.AlertMessages;
import com.admin4u.persistence.entity.Location;
import com.admin4u.persistence.entity.MeetingRoomBookingDetail;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.persistence.entity.SMFloor;

@Stateless
@Transactional
public class SpaceManagementManagerImpl implements SpaceManagementManager {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	CrudService crudService;

	public SpaceManagementManagerImpl() {
	}

	// Conference/Meeting room management start here
	@Override
	public boolean addRoom(SMRoom room, int floorId) {
		room.setAllocationStatus(AllocationStatus.UNALLOCATED.toString());
		List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByFloorAndRoomNumber",
				QueryParameter.with("floor", room.getFloor()).and("roomNumber", room.getRoomNumber()).parameters());
		;
		SMFloor floor = crudService.find(SMFloor.class, floorId);
		room.setFloor(floor);
		if (roomList.isEmpty()) {
			crudService.create(room);
		} else {
			return false;
		}

		return true;
	}

	@Override
	public boolean addRooms(List<SMRoom> roomList, int floorId) {
		SMFloor floor = crudService.find(SMFloor.class, floorId);

		for (SMRoom room : roomList) {
			room.setAllocationStatus(AllocationStatus.UNALLOCATED.toString());

			// List<SMRoom>
			// roomList1=crudService.findWithNamedQuery("SMRoom.findByFloorAndRoomCode",QueryParameter.with("floor",
			// room.getFloor()).and("roomCode",
			// room.getRoomCode()).parameters());;
			room.setFloor(floor);
			// if(roomList.isEmpty()){
			crudService.create(room);

			// }
			/*
			 * else { }
			 */
		}

		return true;
	}

	@Override
	public boolean updateRoomDetail(SMRoom room) {

		SMRoom roomDB = crudService.find(SMRoom.class, room.getId());
		if (room.getAllocationStatus() != null) {
			roomDB.setAllocationStatus(room.getAllocationStatus());
		}
		if (room.getRoomType() != null) {
			roomDB.setRoomType(room.getRoomType());
		}
		if (room.getRoomNumber() != null) {

			roomDB.setRoomNumber(room.getRoomNumber());
		}
		if (room.getFloor() != null) {

			roomDB.setFloor(room.getFloor());
		}
		/*
		 * if(room.getTotalNoOfSeats()!=0 &&
		 * room.getTotalNoOfSeats()!=roomDB.getTotalNoOfSeats()){
		 * 
		 * roomDB.setFloor(room.getFloor()); }
		 */

		crudService.update(roomDB);
		return true;
	}

	@Override
	public String getEmployeeSeatInfo(String employeeUPN) {
		String seatInfo = "";
		List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByEmployeeUPN",
				QueryParameter.with("employeeUPN", employeeUPN).parameters());
		if (!roomList.isEmpty()) {
			seatInfo = roomList.get(0).getFloor().getBuilding().getBuildingName() + "-"
					+ roomList.get(0).getFloor().getFloorCode() + "-RN: " + roomList.get(0).getRoomNumber();
		}

		List<SMSeat> seatList = crudService.findWithNamedQuery("SMSeat.findByEmployeeUPN",
				QueryParameter.with("employeeUPN", employeeUPN).parameters());
		if (!seatList.isEmpty()) {
			seatInfo = seatList.get(0).getFloor().getBuilding().getBuildingName() + "-"
					+ seatList.get(0).getFloor().getFloorCode() + "-SN: " + seatList.get(0).getSeatNumber();
		}

		return seatInfo;

	}

	@Override
	public boolean deleteRoom(int roomId) {

		crudService.delete(SMRoom.class, roomId);
		return true;
	}

	@Override
	public SMRoom getRoomDetail(int roomId) {

		return crudService.find(SMRoom.class, roomId);
	}

	@Override
	public List<SMRoom> getRoomsByType(int buildingId, String roomType) {
		SMBuilding building = crudService.find(SMBuilding.class, buildingId);
		List<SMRoom> roomListByBuilding = new ArrayList<>();
		for (SMFloor floor : building.getFloorList()) {

			List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByFloorAndType",
					QueryParameter.with("floor", floor).and("roomType", roomType).parameters());
			roomListByBuilding.addAll(roomList);

		}

		return roomListByBuilding;
	}

	@Override
	public List<SMRoom> getRoomsByFloor(int floorId, String roomType) {

		SMFloor floor = crudService.find(SMFloor.class, floorId);
		List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByFloorAndType",
				QueryParameter.with("floor", floor).and("roomType", roomType).parameters());

		return roomList;
	}

	@Override
	public boolean allocateRoom(int roomId, String employeeUPN) {
		SMRoom room = crudService.find(SMRoom.class, roomId);
		if (this.isRoomAvailable(roomId)) {
			deallocateCurrentRoom(employeeUPN);

			room.setAllocationStatus("allocated");
			room.setEmployeeUPN(employeeUPN);
			crudService.update(room);
			return true;
		}
		return false;

	}

	@Override
	public boolean deallocateCurrentRoom(String employeeUPN) {
		List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByEmployeeUPN",
				QueryParameter.with("employeeUPN", employeeUPN).parameters());
		if (!roomList.isEmpty()) {
			roomList.get(0).setAllocationStatus("unallocated");
			// roomList.get(0).setEmployeeUPN("");
			crudService.update(roomList.get(0));
			return true;
		} else
			return false;
	}

	@Override
	public boolean isRoomAvailable(int roomId) {
		SMRoom room = crudService.find(SMRoom.class, roomId);
		if ("unallocated".equalsIgnoreCase(room.getAllocationStatus()) || room.getAllocationStatus() == null
				|| room.getAllocationStatus().isEmpty()) {
			return true;

		} else {
			return false;
		}
	}

	@Override
	public boolean isRoomAvailableByRoomNo(int floorId, int roomNo) {
		SMFloor floor = crudService.find(SMFloor.class, floorId);

		List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByFloorAndRoomNumber",
				QueryParameter.with("floor", floor).and("roomNumber", roomNo + "").parameters());
		if (!roomList.isEmpty()) {
			if (this.isRoomAvailable(roomList.get(0).getId())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<SMRoom> getAllocatedRooms(int floorId, String roomType) {
		String allocationStatus = AllocationStatus.ALLOCATED.toString();
		SMFloor floor = crudService.find(SMFloor.class, floorId);
		List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByFloorTypeAndAllocationStatus",
				QueryParameter.with("floor", floor).and("roomType", roomType).and("allocationStatus", allocationStatus)
						.parameters());

		return roomList;
	}

	@Override
	public List<SMRoom> getAvailableRooms(int floorId, String roomType) {

		String allocationStatus = AllocationStatus.UNALLOCATED.toString();
		SMFloor floor = crudService.find(SMFloor.class, floorId);
		List<SMRoom> roomList = crudService.findWithNamedQuery("SMRoom.findByFloorTypeAndAllocationStatus",
				QueryParameter.with("floor", floor).and("roomType", roomType).and("allocationStatus", allocationStatus)
						.parameters());

		return roomList;

	}

	/*
	 * @Override public List<MeetingRoom> getAllAvailableMeetingRooms(int
	 * buildingId) {
	 * 
	 * 
	 * return null; }
	 */

	@Override
	public boolean checkMeetingRoomAvailability(int roomId, Date startDateTime, Date endDateTime) {
		SMRoom meetingRoom = crudService.find(SMRoom.class, roomId);
		List<MeetingRoomBookingDetail> bookingDetailList = crudService.findWithNamedQuery(
				"MeetingRoomBookingDetail.findRoomAvailability", QueryParameter.with("meetingRoom", meetingRoom)
						.and("startDateTime", startDateTime).and("endDateTime", endDateTime).parameters());
		if (bookingDetailList.isEmpty())
			return true;
		else
			return false;

	}

	public boolean addBuilding() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addBuildings(List<SMBuilding> buildingList, int locationId) {
		log.debug("after calling");
		Location location = crudService.find(Location.class, locationId);
		for (SMBuilding building : buildingList) {
			building.setLocation(location);
			crudService.create(building);

		}
		return true;
	}

	@Override

	public boolean isBuilding(String buildingName){
		List<SMBuilding> buildingList=crudService.findWithNamedQuery("SMBuilding.findByBuildingName",QueryParameter.with("buildingName", buildingName).parameters());
		if(buildingList.isEmpty())
		return false;
		else 
		return true;	
	}
	
	
	
	
	

	@Override
	public SMBuilding getBuildingByBuildingName(String buildingName){
		List<SMBuilding> buildingList=crudService.findWithNamedQuery("SMBuilding.findByBuildingName",QueryParameter.with("buildingName", buildingName).parameters());
		return buildingList.get(0);
	}

	
	@Override
	public SMBuilding addBuilding(SMBuilding building,int locationId) {
	log.debug("after calling");
		Location location = crudService.find(Location.class, locationId);

		building.setLocation(location);
		SMBuilding createdBuilding = crudService.create(building);

		return createdBuilding;
	}

	@Override
	public boolean removeBuildings(List<Integer> buildingIdList) {

		for (int buildingId : buildingIdList) {
			crudService.delete(SMBuilding.class, buildingId);
		}

		return true;
	}

	@Override
	public boolean updateBuildingInfo(SMBuilding building, int buildingId) {
		SMBuilding buildingDB = crudService.find(SMBuilding.class, buildingId);
		if (building.getBuildingName() != null) {
			buildingDB.setBuildingName(building.getBuildingName());
		}
		if (building.getLocation() != null) {
			buildingDB.setLocation(building.getLocation());
			;
		}
		if (building.getNoOfFloors() != 0) {
			buildingDB.setNoOfFloors(building.getNoOfFloors());
			;
		}
		crudService.update(buildingDB);
		return true;
	}

	@Override
	public SMBuilding getBuildingInfoByBuildingId(int buildingId) {
		SMBuilding building = crudService.find(SMBuilding.class, buildingId);
		return building;
	}

	@Override
	public List<SMBuilding> getBuildingsByLocation(int locationId) {
		Location location = crudService.find(Location.class, locationId);
		List<SMBuilding> buildingList = crudService.findWithNamedQuery("SMBuilding.findByLocation",
				QueryParameter.with("location", location).parameters());

		return buildingList;
	}

	@Override
	public boolean addFloors(List<SMFloor> floorList, int buildingId) {
		SMBuilding building = crudService.find(SMBuilding.class, buildingId);
		for (SMFloor floor : floorList) {
			floor.setBuilding(building);
			crudService.create(floor);
		}

		return true;
	}

	@Override
	public boolean addFloorsByNumber(int floorCount, int buildingId) {
		SMBuilding building = crudService.find(SMBuilding.class, buildingId);
		for (int count = 1; count <= floorCount; count++) {
			SMFloor floor = new SMFloor();
			floor.setBuilding(building);
			floor.setFloorCode("floor-" + count);
			crudService.create(floor);
		}

		return true;
	}

	@Override
	public boolean removeFloors(List<Integer> floorIdList) {
		for (int floorId : floorIdList) {
			crudService.delete(SMFloor.class, floorId);
		}
		return true;
	}

	@Override
	public boolean updateFloorInfo(SMFloor floor, int floorId) {
		SMFloor floorDB = crudService.find(SMFloor.class, floorId);
		if (floor.getFloorCode() != null) {
			floorDB.setFloorCode(floor.getFloorCode());
		}
		if (floor.getNoOfRooms() != 0) {
			floorDB.setNoOfRooms(floor.getNoOfRooms());
			;
		}
		if (floor.getNoOfSeats() != 0) {
			floorDB.setNoOfSeats(floor.getNoOfSeats());
			;
		}

		return true;
	}

	@Override
	public boolean isFloor(SMBuilding building ,String floorCode){
		List<SMFloor> floorList=crudService.findWithNamedQuery("SMFloor.findByBuildingAndFloorCode",QueryParameter.with("building", building).and("floorCode", floorCode).parameters());
		if(floorList.isEmpty())
		return false;
		else 
		return true;	
	}

	@Override
	public SMFloor getFloorInfoByFloorId(int floorId) {

		SMFloor floor = crudService.find(SMFloor.class, floorId);
		return floor;
	}

	@Override
	public List<SMFloor> getFloorsByBuilding(int buildingId) {
		SMBuilding building = crudService.find(SMBuilding.class, buildingId);
		// List<SMFloor> floorList=building.getFloorList();
		List<SMFloor> floorList = crudService.findWithNamedQuery("SMFloor.findByBuilding",
				QueryParameter.with("building", building).parameters());

		return floorList;
	}

	@Override
	public boolean addSeatsToFloor(int floorId, int startingFrom, int endTo) {
		SMFloor floor = crudService.find(SMFloor.class, floorId);
		int seatNumber = startingFrom;
		while (seatNumber <= endTo) {

			SMSeat seat = new SMSeat();
			seat.setAllocationStatus(AllocationStatus.UNALLOCATED.toString());
			seat.setFloor(floor);
			seat.setSeatNumber(seatNumber);
			crudService.create(seat);
			seatNumber++;
		}
		return true;
	}

	@Override
	public boolean addSeatsToFloorWithPattern(int floorId, int startingFrom, int endTo, String pattern) {
		SMFloor floor = crudService.find(SMFloor.class, floorId);
		int seatNumber = startingFrom;
		while (seatNumber <= endTo) {

			SMSeat seat = new SMSeat();
			seat.setAllocationStatus(AllocationStatus.UNALLOCATED.toString());
			seat.setFloor(floor);
			seat.setSeatNumber(seatNumber);
			seat.setSeatCode(pattern + "-" + seatNumber);
			crudService.create(seat);
			seatNumber++;
		}
		return true;
	}

	@Override
	public SMSeat getSeatInfoBySeatId(int seatId) {

		SMSeat seat = crudService.find(SMSeat.class, seatId);
		return seat;
	}

	@Override
	public List<SMSeat> getSeatsByFloor(int floorId) {
		SMFloor floor = crudService.find(SMFloor.class, floorId);
		List<SMSeat> seatList = crudService.findWithNamedQuery("SMSeat.findByFloor",
				QueryParameter.with("floor", floor).parameters());

		return seatList;
	}

	@Override
	public String allocateSeat(String employeeUPN, int seatId) {

		SMSeat seat = crudService.find(SMSeat.class, seatId);
		if (AllocationStatus.UNALLOCATED.toString().equalsIgnoreCase(seat.getAllocationStatus())
				|| seat.getAllocationStatus() == null || seat.getAllocationStatus().isEmpty()) {
			seat.setEmployeeUPN(employeeUPN);
			seat.setAllocationStatus(AllocationStatus.ALLOCATED.toString());
			crudService.update(seat);
			return AlertMessages.seatAllocated;
		}

		return AlertMessages.seatNotAllocated;
	}

	@Override
	public boolean deallocateCurrentSeat(String employeeUPN) {

		List<SMSeat> seatList = crudService.findWithNamedQuery("SMSeat.findByEmployeeUPN",
				QueryParameter.with("employeeUPN", employeeUPN).parameters());
		if (!seatList.isEmpty()) {
			seatList.get(0).setAllocationStatus("unallocated");
			seatList.get(0).setEmployeeUPN("");
			crudService.update(seatList.get(0));
			return true;
		} else
			return false;

	}

	@Override
	public boolean isSeatAvailable(int seatId) {
		SMSeat seat = crudService.find(SMSeat.class, seatId);
		if (!AllocationStatus.ALLOCATED.toString().equalsIgnoreCase(seat.getAllocationStatus())) {
			return true;
		}

		return false;
	}

	@Override
	public List<SMSeat> getAvailableSeatsByFloor(int floorId) {
		SMFloor floor = crudService.find(SMFloor.class, floorId);
		List<SMSeat> seatList = crudService.findWithNamedQuery("SMSeat.findByFloor",
				QueryParameter.with("floor", floor).parameters());
		List<SMSeat> seatListToReturn = new ArrayList<>();
		for (SMSeat seat : seatList) {
			if (AllocationStatus.UNALLOCATED.toString().equalsIgnoreCase(seat.getAllocationStatus())
					|| seat.getAllocationStatus() == null || seat.getEmployeeUPN().equalsIgnoreCase("")) {
				seatListToReturn.add(seat);
			}
		}
		return seatListToReturn;
	}

	@Override
	public List<SMSeat> getAllocatedSeats(int floorId, String allocStatus) {
		String allocationStatus = AllocationStatus.ALLOCATED.toString();
		SMFloor floor = crudService.find(SMFloor.class, floorId);
		List<SMSeat> seatList = crudService.findWithNamedQuery("SMSeat.findByFloorAndAllocationStatus",
				QueryParameter.with("floor", floor).and("allocationStatus", allocationStatus).parameters());

		return seatList;
	}

	@Override
	public boolean saveMeetingRoomBookingDetail(int roomId, Date meetingStartDateTime, Date meetingEndDateTime,
			int requestId) {
		SMRoom meetingRoom = new SMRoom();
		meetingRoom.setId(roomId);
		RequestMaster request = new RequestMaster();
		request.setRequestId(requestId);
		MeetingRoomBookingDetail mrbd = new MeetingRoomBookingDetail();
		mrbd.setBookingStartDateTime(meetingStartDateTime);
		mrbd.setBookingEndDateTime(meetingEndDateTime);
		mrbd.setMeetingRoom(meetingRoom);
		mrbd.setRequest(request);
		crudService.create(mrbd);
		return true;
	}

	
	@Override
	public boolean  removeMeetingRoomBookingDetail(int requestId){
		
		RequestMaster request=new RequestMaster();
		request.setRequestId(requestId);
		List<MeetingRoomBookingDetail> meetingRoomDetailList=crudService.findWithNamedQuery("MeetingRoomBookingDetail.findDetailByRequestId",QueryParameter.with("request", request).parameters());
		if(!meetingRoomDetailList.isEmpty()){
			crudService.delete(MeetingRoomBookingDetail.class, meetingRoomDetailList.get(0).getId());
			return true;
		}
		else
			return false;
	}

	
	
	
	//Conference/Meeting room management End here


	// Conference/Meeting room management End here

}
