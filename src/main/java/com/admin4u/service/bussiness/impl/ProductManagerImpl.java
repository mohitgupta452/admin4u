package com.admin4u.service.bussiness.impl;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.CategaryClosure;
import com.admin4u.persistence.entity.CategaryMaster;
import com.admin4u.persistence.entity.ProductDetail;
import com.admin4u.persistence.entity.StockInfo;
import com.admin4u.service.bussiness.contract.ProductManager;
import com.admin4u.util.enums.Type;
import com.admin4u.util.service.RequestMetadata.ProductKey;

@Stateless
@Transactional
public class ProductManagerImpl  implements ProductManager {
	
	@Inject
	private CrudService crudService;
	
	@Inject
	private Logger log;

	@Override
	public String registerProduct(List<Map<String, Object>> products) {
		
		products.forEach(product->
		{
			ProductDetail productDetail= new ProductDetail();
			
			productDetail.setName(product.get(ProductKey.NAME).toString());
			productDetail.setBrand(product.get(ProductKey.BARND).toString());
			productDetail.setCategary(this.getCategaryById(Integer.parseInt(product.get(ProductKey.CATEGARY).toString())));
			productDetail.setUnit(product.get(ProductKey.UNITS).toString());
			productDetail.setCategaryMaster(this.getCategaryById(Integer.parseInt(product.get(ProductKey.MASTERCATEGARY).toString())));
			
			productDetail=crudService.create(productDetail);
			
			log.debug("Product {} registered with Id : {} ",productDetail.getName(),productDetail.getId());
			
		});
		return null;
	}
	
	private CategaryMaster getCategaryById(int Id)
	{
		return crudService.find(CategaryMaster.class,Id);
	}
	
	public List<CategaryMaster> getAllCategary()
	{
		return crudService.findWithNamedQuery("CategaryMaster.findAll");
		
	}
	
	public List<ProductDetail> getProductsByCategary(int categaryId,Type type)
	{
		return crudService.findByNativeQuery("SELECT * from product_detail p WHERE "+type.toString().toLowerCase()+"="+categaryId,ProductDetail.class);
	}
	
	public List<CategaryClosure> getCatDescendant(int ancestor)
	{
		return crudService.findWithNamedQuery("CategaryClosure.findByAncestor",QueryParameter.with("ancestor",ancestor).parameters());
	}
	
	public List<CategaryClosure> getCategoriesByDepth(int depth)
	{
		return crudService.findWithNamedQuery("CategaryClosure.findByDepth",QueryParameter.with("depth",depth).parameters());
	}
	
	public CategaryClosure addSubCategory(int masterCat,String subcat)
	{
		CategaryMaster categaryMaster= new CategaryMaster();
		categaryMaster.setCategary(subcat);
		
		 categaryMaster= crudService.create(categaryMaster);
		 
		 CategaryMaster ancestor = this.getCategaryById(masterCat);
		 
		 CategaryClosure categaryClosure= new CategaryClosure();
		 
		 categaryClosure.setAncestor(ancestor);
		 categaryClosure.setdescendant(categaryMaster);
		 categaryClosure.setDepth(1);
		
		 return crudService.create(categaryClosure);
	}

     @Override
	public List<ProductDetail> getAllProduct()
	{
		return crudService.findWithNamedQuery("ProductDetail.findAllActiveProd",QueryParameter.with("active",true).parameters());
	}
	
	@Override
	public ProductDetail getProductDetails(int productId)
	{
		return crudService.find(ProductDetail.class, productId);
	}
	
	
	
	private boolean isProductExist()
	{
	
		return false;
	}

	@Override
	public List<ProductDetail> getProductDetails() {
		
		return crudService.findWithNamedQuery("ProductDetail.findAll");
	}

	@Override
	public List<StockInfo> getStockDetails() {
		
		return crudService.findWithNamedQuery("StockInfo.findAll");
	}

}
