package com.admin4u.service.bussiness.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.dto.SpaceManagementManager;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.EventTracker;
import com.admin4u.persistence.entity.MeetingRoomBookingDetail;
import com.admin4u.persistence.entity.NotificationMaster;
import com.admin4u.persistence.entity.RequestData;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.RequestServiceMap;
import com.admin4u.persistence.entity.ResponseMaster;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.ActionManager;
import com.admin4u.service.bussiness.contract.EventTatManager;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.MailManager;
import com.admin4u.service.bussiness.contract.MailManager.MailEvents;
import com.admin4u.service.bussiness.contract.NotificationManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.AllocationStatus;
import com.admin4u.util.enums.Event;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.ApplicationMetaData.Action;
import com.admin4u.util.service.ApplicationMetaData.Status;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.admin4u.util.service.ProcessStates;
import com.admin4u.views.UserSessionBean;
import com.admin4u.util.service.ProcessListener;

@Transactional
@Stateless
public class ActionManagerImpl implements ActionManager {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Inject
	private CrudService crudService;

	@Inject
	private RequestResponseManager requestResponseManager;

	@Inject
	private NotificationManager notificationManager;

	@Inject
	private IDMService idmService;

	@Inject
	private RoleManager roleManager;

	@Inject
	private UserSessionBean userSession;

	@Inject
	private SpaceManagementManager smManager;

	@Inject
	private GuestHouseManager guestHouseManager;

	private ProcessListener processListener;

	@Inject
	private MailManager mailManager;

	@Inject
	private EventTatManager eventTatManager;

	@Inject
	private ADStore adStore;

	@PostConstruct
	private void init() {
		processListener = new ProcessStates();

	}

	public UserActionMaster commonOperation(int actionId, Action action) {

		UserActionMaster userActionMaster = this.getUserActionByActionId(actionId);

		processListener.getProcessType(Services.valueOf(userActionMaster.getRequestMaster().getService()));

		userActionMaster.setAction(action.toString());
		userActionMaster.setActionTime(new Date());
		userActionMaster.setActionBy(userSession.getEmpData().getUserPrincipalName());
		try {
			userActionMaster.setIPAddress(InetAddress.getLocalHost().getHostAddress());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		processListener.actionPerformed(action);
		userActionMaster.setStatus(processListener.getNextState());

		notificationManager.setNotificationByAction(processListener.getNotificationMap(), userActionMaster,
				userActionMaster.getRequestMaster());

		try {
			if (MailEvents.valueOf(action.toString()) != null)
				notificationManager.generateEmail(userActionMaster.getRequestMaster(), processListener.getEmails(),action);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
		}

		return userActionMaster;

	}

	public RequestMaster updateRequest(RequestMaster requestMaster) {
		requestMaster.setRequestStatus(processListener.getNextRequestStatus());

		if (requestMaster.getRequestStatus().equals(Status.AUTO_APPROVED.toString())) {
			String managerUpn = adStore.getManager(requestMaster.getRequestBy()).getUserPrincipalName();
			NotificationMaster notification = new NotificationMaster();
			notification.setReciever(managerUpn);
			notification.setRequestMaster(requestMaster);
			notification.setMessage(
					"The cab odd hours booking of " + idmService.getEmpNameByUpn(requestMaster.getRequestBy())
							+ " has been processed with workorder no " + requestMaster.getWorkorder());
			notification.setSender(userSession.getEmpData().getUserPrincipalName());

		}
		if (processListener.isRequestCloseAction()) {
			requestMaster.setComplete(true);
			requestMaster.setClosureDate(new Date());
		}

		return requestMaster;
	}

	public UserActionMaster getUserActionByActionId(int actionId) {
		return crudService.find(UserActionMaster.class, actionId);
	}

	@Override
	public String apprrovalAuthAction(int actionId, Action action) {

		if (this.getUserActionByActionId(actionId).getActive()) {
			UserActionMaster userAction = this.commonOperation(actionId, action);

			// int requestId=userAction.getRequestMaster().getRequestId();

			RequestMaster requestMaster = userAction.getRequestMaster();

			requestMaster = this.updateRequest(userAction.getRequestMaster());
			this.setNextOwner(userAction);
			if (action.toString().equals(Action.APPROVAL_AUTHORITY_ACCEPT.toString()))
				requestMaster.setWorkorder(requestResponseManager.generateWorkOrder(requestMaster));
			userAction.setActive(false);

			userAction.setRequestMaster(requestMaster);

			crudService.update(userAction);
			return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
		}
		return null;

	}

	@Override
	public String adminServe(int actionId, List<String> attachments, Map<String, String[]> requestMap) {

		if (this.getUserActionByActionId(actionId).getActive()) {
			String attachment = "";

			for (String attach : attachments) {
				attachment += attach + "|";
			}

			UserActionMaster userAction = this.commonOperation(actionId, Action.ADMIN_SERVE);
			RequestMaster requestMaster = this.updateRequest(userAction.getRequestMaster());
			requestMaster.setUserAction(this.updateOthers(userAction, Status.COMPLETE.toString()));

			ResponseMaster responseMaster = this.insertResonseData(requestMap, requestMaster);

			responseMaster.setResponseAttachment(attachment);
			requestMaster.setResponseMaster(responseMaster);
			userAction.setActive(false);
			userAction.setRequestMaster(requestMaster);
			crudService.update(userAction);

			return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
		}
		return null;
	}

	private ResponseMaster actionClose(String responseMsg, RequestMaster requestMaster) {

		ResponseMaster responseMaster = requestMaster.getResponseMaster();
		if (responseMaster == null)
			responseMaster = new ResponseMaster();

		responseMaster.setRequestMaster(requestMaster);
		responseMaster.setResponseMsg(responseMsg);
		requestMaster.setResponseMaster(responseMaster);
		return responseMaster;
	}

	@Override
	public String adminClose(int actionId, String responseMsg) {

		if (this.getUserActionByActionId(actionId).getActive()) {
			UserActionMaster userAction = this.commonOperation(actionId, Action.ADMIN_CLOSE);
			RequestMaster requestMaster = this.updateRequest(userAction.getRequestMaster());

			ResponseMaster responseMaster = this.actionClose(responseMsg, userAction.getRequestMaster());

			requestMaster.setResponseMaster(responseMaster);
			requestMaster.setUserAction(this.updateOthers(userAction, Status.CLOSED.toString()));
			userAction.setRequestMaster(requestMaster);
			userAction.setActive(false);
			crudService.update(userAction);

			return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
		}
		return null;
	}

	@Override
	public String adminReject(int actionId) {

		if (this.getUserActionByActionId(actionId).getActive()) {
			UserActionMaster userAction = this.commonOperation(actionId, Action.ADMIN_REJECT);
			RequestMaster requestMaster = this.updateRequest(userAction.getRequestMaster());
			requestMaster.setUserAction(this.updateOthers(userAction, Status.CLOSED.toString()));
			userAction.setRequestMaster(requestMaster);
			userAction.setActive(false);
			crudService.update(userAction);
			return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
		}
		return null;
	}
	
	public String getCommentsByRequest(int requestId)
	{
		RequestMaster  requestMaster=requestResponseManager.getRequestById(requestId);
		
		JsonObject requestData = requestMaster.getRequestData().getRequestData();
		
		String service= requestMaster.getService();
		
		if(requestData.has(service.toLowerCase()+".subnatureofwork"))
			service=service+"_"+requestData.get(service.toLowerCase()+".subnatureofwork").getAsString();
		log.debug("value of service {}",service);
		return eventTatManager.getCommentsByservice(service);
	}
	
	public String getEventNameForExtend(int requestId)
	{
		RequestMaster  requestMaster=requestResponseManager.getRequestById(requestId);
		
		JsonObject requestData = requestMaster.getRequestData().getRequestData();
		
		String service= requestMaster.getService();
		
		if(requestData.has(service.toLowerCase()+".subnatureofwork"))
			service=service+"_"+requestData.get(service.toLowerCase()+".subnatureofwork").getAsString();
		log.debug("value of service {}",service);
		return service;
	}

	@Override
	public String extend(int actionId, String msg, Date exclanationTime, Action action) {

		UserActionMaster userAction = this.commonOperation(actionId, action);
		RequestMaster requestMaster = this.updateRequest(userAction.getRequestMaster());
		
		String eventService="";

		if(requestMaster.getService().equals(Services.HOUSEKEEPING.toString()))
			eventService=requestMaster.getRequestData().getRequestData().get(Services.HOUSEKEEPING.toString().toLowerCase()+".natureofwork").getAsString();
		else
			eventService=this.getEventNameForExtend(requestMaster.getRequestId());
	
		ResponseMaster responseMaster = new ResponseMaster();

		responseMaster.setResponseData("{\"esclationTime\":\""+exclanationTime+"\"}");
		responseMaster.setResponseMsg(msg);
		responseMaster.setRequestMaster(requestMaster);
		requestMaster.setResponseMaster(responseMaster);

		crudService.update(userAction);
		
	log.debug("user action master updated");	
		LocalDateTime ldt = LocalDateTime.ofInstant(exclanationTime.toInstant(), ZoneId.systemDefault());
		EventTracker et=new EventTracker();
		et.setActive(true);
		et.setEvent("TAT_"+eventService);
		et.setEventTime(ldt);
		et.setRequestMaster(requestMaster);
		
		et.setUserActionMaster(userAction);
		crudService.create(et);
		log.debug("event created master updated");	
	
		
		return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
	}

	@Override
	public String adminForword(int actionId, String forwordTo) {

		if (this.getUserActionByActionId(actionId).getActive()) {
			log.debug("forword to {}", forwordTo);
			String assingTOUPN = forwordTo.split("\\_")[0];
			Roles assignTORole = Roles.valueOf(forwordTo.split("\\_")[1]);

			UserActionMaster userAction = this.commonOperation(actionId, Action.ADMIN_FORWORD);
			RequestMaster requestMaster = userAction.getRequestMaster();
			userAction.setActionBy(userSession.getEmpData().getUserPrincipalName());
			try {
				userAction.setIPAddress(InetAddress.getLocalHost().getHostAddress());
			} catch (UnknownHostException e) {

				e.printStackTrace();
			}
			userAction.setStatus("Forword_TO_" + idmService.getEmpNameByUpn(assingTOUPN));
			userAction.setAction("FORWORD");
			userAction.setActionTime(new Date());
			crudService.update(userAction);

			userAction = new UserActionMaster();
			userAction.setOwner(assingTOUPN);
			userAction.setAssignTime(new Date());
			try {
				userAction.setIPAddress(InetAddress.getLocalHost().getHostAddress());
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			userAction.setStatus(Status.PENDING.toString());
			userAction.setRequestMaster(requestMaster);
			userAction.setLocation(requestMaster.getLocation());
			userAction.setRole(Roles.valueOf(assignTORole.toString()).toString());
			crudService.create(userAction);

			return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
		}
		return null;
	}

	@Override
	public String spocServe(int actionId, List<String> attachments, Map<String, String[]> requestMap) {

		if (this.getUserActionByActionId(actionId).getActive()) {
			//UserActionMaster userActionMaster = this.commonOperation(actionId, Action.SPOC_SERVE);
		UserActionMaster userActionMaster=	this.getUserActionByActionId(actionId);

			RequestMaster requestMaster = userActionMaster.getRequestMaster();
			boolean reqState = true;
			
		if(userActionMaster.getService() != null )
		{
			List<UserActionMaster> actionMasters = new ArrayList<>();

			for (UserActionMaster actionMaster : requestMaster.getUserAction()) {
				
				if (actionMaster.getRole().equals(Roles.SPOC.toString())
						&& actionMaster.getService().equals(userActionMaster.getService())
						&& actionMaster.getActionID() != userActionMaster.getActionID()) {
					actionMaster.setStatus("COMPLETE");
					actionMasters.add(actionMaster);
				}
				else
				if (actionMaster.getRole().equals(Roles.SPOC.toString())
						&& actionMaster.getStatus().equals(Status.PENDING.toString())
						&& actionMaster.getActionID() != userActionMaster.getActionID())
					reqState = false;
			}
			requestMaster.setUserAction(actionMasters);
			requestMaster=crudService.update(requestMaster);
		}
			if (reqState) {
				log.debug("reqest satatus {}", reqState);
				userActionMaster = this.commonOperation(actionId, Action.SPOC_SERVE);
				requestMaster = this.updateRequest(requestMaster);
				requestMaster.setUserAction(this.updateOthers(userActionMaster, Status.COMPLETE.toString()));
			} else {
				userActionMaster.setStatus(Status.COMPLETE.toString());
				userActionMaster.setAction(Action.SPOC_COMPLETE.toString());
				userActionMaster.setActionTime(new Date());

				requestMaster.setRequestStatus(Status.PENDING.toString());
			}

			userActionMaster.setActionBy(userSession.getEmpData().getUserPrincipalName());
			try {
				userActionMaster.setIPAddress(InetAddress.getLocalHost().getHostAddress());
			} catch (UnknownHostException e) {

				e.printStackTrace();
			}
			String attachment="";
			ResponseMaster responseMaster = this.insertResonseData(requestMap, requestMaster);
			if (responseMaster != null && responseMaster.getResponseAttachment() != null)
				attachment = responseMaster.getResponseAttachment();
			for(String files : attachments)
				attachment=files+"|"+attachment;
			responseMaster.setResponseAttachment(attachment);
			requestMaster.setResponseMaster(responseMaster);
			userActionMaster.setRequestMaster(requestMaster);
			crudService.update(userActionMaster);
			
			
			return "{" + "\"status\":\"" + userActionMaster.getStatus() + "\"}";
		}
		return null;
	}

	public List<UserActionMaster> updateOthers(UserActionMaster userActionMaster, String status) {
		log.debug("update reamin user action status");
		List<UserActionMaster> actionMasters = new ArrayList<>();
		for (UserActionMaster actionMaster : userActionMaster.getRequestMaster().getUserAction()) {
			if (!actionMaster.getRole().equals(Roles.APPROVAL_AUTHORITY.toString())
					|| status.equals(Status.CANCELED.toString())) {
				actionMaster.setStatus(status);
				actionMaster.setActive(false);
				actionMasters.add(actionMaster);

			}
		}
		return actionMasters;
	}

	@Override
	public String spocClose(int actionId, String responseMsg) {

		if (this.getUserActionByActionId(actionId).getActive()) {
			UserActionMaster userAction = this.commonOperation(actionId, Action.SPOC_CLOSE);
			RequestMaster requestMaster = this.updateRequest(userAction.getRequestMaster());
			requestMaster.setUserAction(this.updateOthers(userAction, Status.CLOSED.toString()));
			ResponseMaster responseMaster = this.actionClose(responseMsg, requestMaster);
			responseMaster.setRequestMaster(requestMaster);
			requestMaster.setResponseMaster(responseMaster);
			userAction.setRequestMaster(requestMaster);
			crudService.update(userAction);

			return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
		}
		return null;
	}

	@Override
	public String spocComplete(int actionId, String message) {

		if (this.getUserActionByActionId(actionId).getActive()) {
			UserActionMaster userAction = this.commonOperation(actionId, Action.SPOC_COMPLETE);
			RequestMaster requestMaster = userAction.getRequestMaster();
			ResponseMaster responseMaster = requestMaster.getResponseMaster();
			if (responseMaster == null)
				responseMaster = new ResponseMaster();
			responseMaster.setResponseMsg(message);
			requestMaster = this.updateRequest(requestMaster);
			responseMaster.setRequestMaster(requestMaster);
			requestMaster.setResponseMaster(responseMaster);
			requestMaster.setUserAction(this.updateOthers(userAction, Status.COMPLETE.toString()));
			userAction.setRequestMaster(requestMaster);
			crudService.update(userAction);

			return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
		}
		return "{" + "\"status\":\"" + "Sorry operation can not be performed." + "\"}";
	}

	@Override
	public String reopenRequest(int requestId, String message) {
		RequestMaster requestMaster = requestResponseManager.getRequestById(requestId);
		requestMaster.setComplete(false);
		requestMaster.setRequestStatus(Status.REOPEN.toString());
		requestMaster.setMessage(message);
		
          List<UserActionMaster> actionMasters = requestMaster.getUserAction();
		
		for (UserActionMaster actionMaster : actionMasters ) {
				actionMaster.setStatus(Status.REOPEN.toString());
				actionMaster.setActive(true);
				actionMasters.add(actionMaster);
			}

		requestMaster.setUserAction(actionMasters);
		notificationManager.setNotificationByAction(processListener.getNotificationOnEvent(Event.REOPEN), null,
				requestMaster);
		return "{" + "\"status\":\"" + requestMaster.getRequestStatus() + "\"}";

	}

	@Override
	public String amendRequest(int requestId, String problem, String workloc) {

		RequestMaster requestMaster = requestResponseManager.getRequestById(requestId);

		RequestData requestData = requestMaster.getRequestData();

		JsonObject reqDataJson = requestData.getRequestData();

		JsonArray ammendProblemArr;
		JsonArray ammendWLArr;

		if (reqDataJson.get("AmmendProblem") != null && reqDataJson.get("AmmendProblem").isJsonArray()) {
			ammendProblemArr = reqDataJson.get("AmmendProblem").getAsJsonArray();
			ammendWLArr = reqDataJson.get("AmmendWorkLocation").getAsJsonArray();
		} else {
			ammendProblemArr = new JsonArray();
			ammendWLArr = new JsonArray();
		}
		ammendProblemArr.add(problem);
		ammendWLArr.add(workloc);
		reqDataJson.add("AmmendProblem", ammendProblemArr);
		reqDataJson.add("AmmendWorkLocation", ammendWLArr);

		log.debug("request data {}", reqDataJson);

		requestMaster.setRequestStatus(Status.AMENDED.toString());

		List<UserActionMaster> actionMasters = requestMaster.getUserAction();
		
		for (UserActionMaster actionMaster : actionMasters ) {
				actionMaster.setStatus(Status.AMENDED.toString());
				actionMasters.add(actionMaster);

			}

		requestMaster.setUserAction(actionMasters);

		requestData.setRequestDate(new Date());

		requestData.setRequestData(reqDataJson);

		requestMaster.setRequestData(requestData);

		notificationManager.setNotificationByAction(processListener.getNotificationOnEvent(Event.AMMEND), null,
				requestMaster);

		log.debug("reqmaster  {}", crudService.update(requestMaster).getRequestData().getRequestData());

		return "{" + "\"status\":\"" + requestMaster.getRequestStatus() + "\"}";
	}

	private void setNextOwner(UserActionMaster userActionMaster) {

		Map<String, String> owner = processListener.getNextOwner();
		RequestMaster requestMaster = userActionMaster.getRequestMaster();

		for (Entry<String, String> entry : owner.entrySet()) {
			log.debug("{}...{}. {}..{}", entry.getKey(), entry.getValue(), requestMaster.getLocation(),
					requestMaster.getService());
			if (entry.getKey().equals("SPOC")) {
				for (RequestServiceMap serviceMap : requestMaster.getRequestService()) {
					UserActionMaster userAction = new UserActionMaster();
					userAction.setRequestMaster(requestMaster);
					userAction.setRole(entry.getKey());
					userAction.setStatus(entry.getValue());
					userAction.setLocation(requestMaster.getLocation());
					userAction.setRead(false);
					userAction.setService(serviceMap.getService());
					userAction.setAssignTime(new Date());
					userAction = crudService.create(userAction);
					if (userAction.getService().equals(requestMaster.getService()))
						eventTatManager.createTatEvent(userAction);
				}
			} else {
				UserActionMaster userAction = new UserActionMaster();
				userAction.setRequestMaster(requestMaster);
				userAction.setRole(entry.getKey());
				userAction.setStatus(entry.getValue());
				userAction.setLocation(requestMaster.getLocation());
				userAction.setRead(false);
				userAction.setAssignTime(new Date());
				crudService.create(userAction);

			}
		}
	}

	public ResponseMaster insertResonseData(Map<String, String[]> requestMap, RequestMaster requestMaster) {

		String responseAttach = "";
		JsonObject json = new JsonObject();

		ResponseMaster responseMaster = requestMaster.getResponseMaster();
		if (responseMaster != null) {
			log.debug("inside response master not null");
			if (responseMaster.getResponseData() != null) {
				String response = responseMaster.getResponseData();

				json = new Gson().fromJson(response, JsonObject.class);

			}
		} else
			responseMaster = new ResponseMaster();

		for (Entry<String, String[]> entry : requestMap.entrySet()) {
			if (entry.getValue().length > 0)

				if (!entry.getKey().equals("upload"))
					json.addProperty(entry.getKey(), entry.getValue()[0]);
			log.debug("entry {}", entry);
		}

		responseMaster.setResponseMsg(requestMap.get("comment").length > 0 ? requestMap.get("comment")[0] : "");

		responseMaster.setResponseData(json.toString());
		
		responseMaster.setRequestMaster(requestMaster);

		return responseMaster;

	}

	private RequestMaster getReqByActionId(int actionId) {
		UserActionMaster userAction = this.getUserActionByActionId(actionId);
		return userAction.getRequestMaster();
	}

	@Override
	public String cancelRequest(int requestID) {

		RequestMaster req = crudService.find(RequestMaster.class, requestID);
		req.setRequestStatus("CANCELLED");
		if (!req.isComplete()) {
			req.setComplete(true);
			List<UserActionMaster> actionMasters = this.updateOthers(req.getUserAction().get(0),
					Status.CANCELED.toString());
			req.setUserAction(actionMasters);
			return crudService.update(req).getRequestStatus();
		} else
			return "You Cant Cancel this Request";

	}

	
	public void processEvent(int requestId) {
		RequestMaster requestMaster = crudService.find(RequestMaster.class, requestId);
		requestMaster.setExpire(true);
		crudService.update(requestMaster);

	}

	@Override
	public String adminApprove(int actionId) {
		UserActionMaster userAction = this.commonOperation(actionId, Action.ADMIN_APPROVE);
		RequestMaster requestMaster = this.updateRequest(userAction.getRequestMaster());
		requestMaster.setUserAction(this.updateOthers(userAction, AllocationStatus.ALLOCATED.toString()));
		crudService.update(userAction);
		return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
	}

	@Override
	public String adminAllocate(int actionId) {
		UserActionMaster userAction = this.commonOperation(actionId, Action.ADMIN_ALLOCATE);
		RequestMaster requestMaster = this.updateRequest(userAction.getRequestMaster());
		requestMaster.setUserAction(this.updateOthers(userAction, AllocationStatus.ALLOCATED.toString()));
		crudService.update(userAction);
		return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
	}

	@Override
	public String spocAllocate(int actionId) {
		UserActionMaster userAction = this.commonOperation(actionId, Action.SPOC_ALLOCATE);
		RequestMaster requestMaster = this.updateRequest(userAction.getRequestMaster());
		requestMaster.setUserAction(this.updateOthers(userAction, AllocationStatus.ALLOCATED.toString()));
		crudService.update(userAction);
		return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";
	}

	@Override
	public RequestMaster processSpaceManagementRequest(RequestMaster request, int noOfDays, int roomId) {
		log.debug("inside processSpaceManagementRequest");
		JsonObject jsonObj = request.getRequestData().getRequestData();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		// System.exit(0);
		Date startDateTime = new Date();
		Date endDateTime = new Date();
		try {
			log.debug("value of start date json {}", jsonObj.get("meetingroommanagement.startdatetime").getAsString());
			log.debug("value of start date json {}", jsonObj.get("meetingroommanagement.enddatetime").getAsString());

			String startDateString = jsonObj.get("meetingroommanagement.startdatetime").getAsString();
			String endDateString = jsonObj.get("meetingroommanagement.enddatetime").getAsString();

			startDateTime = dateFormatter.parse(startDateString.trim());
			endDateTime = dateFormatter.parse(endDateString.trim());
		} catch (Exception e) {
			log.debug("date format exception {}", e.getMessage());
		}
		log.debug("after parsing");
		long diff = Math.abs(startDateTime.getTime() - endDateTime.getTime());
		long diffDays = diff / (24 * 60 * 60 * 1000);
		log.debug("diff days {}", diffDays);
		if (diffDays <= 5) {
			log.debug("value of check meeting room {}",
					smManager.checkMeetingRoomAvailability(roomId, startDateTime, endDateTime));
			// update the request status as completed
			request.setRequestStatus(AllocationStatus.ALLOCATED.toString());
			// this.updateRequest(request);
			if (smManager.checkMeetingRoomAvailability(roomId, startDateTime, endDateTime)) {
				List<UserActionMaster> userActionList = crudService.findWithNamedQuery(
						"UserActionMaster.findByRequestMaster",
						QueryParameter.with("requestMaster", request).parameters());
				smManager.saveMeetingRoomBookingDetail(roomId, startDateTime, endDateTime, request.getRequestId());
				for (UserActionMaster userAction : userActionList) {
					userAction.setStatus(AllocationStatus.ALLOCATED.toString());
					crudService.update(userAction);
				}
				crudService.update(request);
			}
		}

		return null;
	}

	@Override
	public String guestHouseSpocServe(int actionId) {

		if (this.getUserActionByActionId(actionId).getActive()) {
			UserActionMaster userActionMaster = this.commonOperation(actionId, Action.SPOC_SERVE);

			RequestMaster requestMaster = userActionMaster.getRequestMaster();
			boolean reqState = true;

			for (UserActionMaster actionMaster : requestMaster.getUserAction()) {
				if (actionMaster.getService() != null && actionMaster.getRole().equals(Roles.SPOC.toString())
						&& actionMaster.getService().equals(userActionMaster.getService())
						&& actionMaster.getActionID() != userActionMaster.getActionID()) {
					actionMaster.setStatus("COMPLETE");
					crudService.update(actionMaster);
				}

				if (actionMaster.getRole().equals(Roles.SPOC.toString())
						&& actionMaster.getStatus().equals(Status.PENDING.toString())
						&& actionMaster.getActionID() != userActionMaster.getActionID())
					reqState = false;
			}
			if (reqState) {
				log.debug("reqest satatus {}", reqState);
				userActionMaster = this.commonOperation(actionId, Action.SPOC_SERVE);
				requestMaster = this.updateRequest(requestMaster);
				requestMaster.setUserAction(this.updateOthers(userActionMaster, Status.COMPLETE.toString()));
			} else {
				userActionMaster.setStatus(Status.COMPLETE.toString());
				userActionMaster.setAction(Action.SPOC_COMPLETE.toString());
				userActionMaster.setActionTime(new Date());

				requestMaster.setRequestStatus(Status.PENDING.toString());
			}

			userActionMaster.setActionBy(userSession.getEmpData().getUserPrincipalName());
			try {
				userActionMaster.setIPAddress(InetAddress.getLocalHost().getHostAddress());
			} catch (UnknownHostException e) {

				e.printStackTrace();
			}

			crudService.update(userActionMaster);

			return "{" + "\"status\":\"" + userActionMaster.getStatus() + "\"}";
		}
		return null;
	}

	@Override
	public String guestHouseAdminServe(int actionId) {

		UserActionMaster userAction = this.commonOperation(actionId, Action.ADMIN_SERVE);
		RequestMaster requestMaster = this.updateRequest(userAction.getRequestMaster());
		requestMaster.setUserAction(this.updateOthers(userAction, Status.COMPLETE.toString()));

		userAction.setActive(false);
		userAction.setRequestMaster(requestMaster);
		crudService.update(userAction);

		return "{" + "\"status\":\"" + userAction.getStatus() + "\"}";

	}

}
