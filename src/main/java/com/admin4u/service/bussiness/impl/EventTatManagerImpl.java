package com.admin4u.service.bussiness.impl;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.xml.bind.ParseConversionEvent;

import org.junit.experimental.categories.Categories.IncludeCategory;
import org.slf4j.Logger;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.entity.DesignationPriority;
import com.admin4u.persistence.entity.EventTracker;
import com.admin4u.persistence.entity.MeetingRoomBookingDetail;
import com.admin4u.persistence.entity.NotificationMaster;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.persistence.entity.TatMaster;
import com.admin4u.persistence.entity.TatServiceMaster;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.ActionManager;
import com.admin4u.service.bussiness.contract.EventTatManager;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.AllocationStatus;
import com.admin4u.util.enums.Event;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData.Action;
import com.admin4u.util.service.ApplicationMetaData.Status;
import com.google.gson.JsonObject;
import com.admin4u.util.service.ApplicationMetaData.TatLevelKeys;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Stateless
@Transactional
public class EventTatManagerImpl implements EventTatManager {

	@Inject
	private Logger log;

	@PersistenceContext(unitName = "admin4u")
	private EntityManager entityManager;

	@Inject
	private CrudService crudService;

	private List<TatMaster> tatMasters;

	@Inject
	private ADStore adStore;
	
	@Inject
	private RequestResponseManager requestResponseManager;

	@Inject
	private RoleManager roleManager;
	
	@Inject
	private ActionManager actionManager;
	
	@Inject
	private GuestHouseManager guestHouseManager;

	@PostConstruct
	private void init() {
		this.tatMasters = crudService.findWithNamedQuery("TatMaster.findAll");
	}

	public void createTatEvent(UserActionMaster userActionMaster) {

		EventTracker tatTracker = new EventTracker();

		String service = userActionMaster.getRequestMaster().getService();

		if (service.equals(Services.HOUSEKEEPING.toString())) {
			JsonObject requestData = userActionMaster.getRequestMaster().getRequestData().getRequestData();

			if (requestData.has(Services.HOUSEKEEPING.toString().toLowerCase() + ".natureofwork"))
				service = requestData.get(Services.HOUSEKEEPING.toString().toLowerCase() + ".natureofwork")
						.getAsString();
		}

		else if (service.equals(Services.REPAIRMAINTENANCE.toString())) {
			JsonObject requestData = userActionMaster.getRequestMaster().getRequestData().getRequestData();

			if (requestData.has(Services.REPAIRMAINTENANCE.toString().toLowerCase() + ".subnatureofwork"))
				service = requestData.get(Services.REPAIRMAINTENANCE.toString().toLowerCase() + "subnatureofwork")
						.getAsString();
		}
		
		if(service.contains("CAB"))
			service="CAB";

		TatMaster tatMaster = this.getTatByService(service).get(0);

		userActionMaster.getRequestMaster().setPriority(tatMaster.getPriority());

		tatTracker.setUserActionMaster(userActionMaster);

		JsonObject tatConfig = tatMaster.getTatLevel();

		tatTracker.setUserActionMaster(userActionMaster);
		tatTracker.setRequestMaster(userActionMaster.getRequestMaster());
		tatTracker.setActive(true);
		tatTracker.setExecutionLevel(1);
		tatTracker
				.setEventTime(LocalDateTime.now().plus(Duration.ofMillis(tatConfig.get("level1TatTime").getAsLong())));
		tatTracker.setEvent("TAT_" + service);
		tatTracker.setExecutionLevel(1);
		tatTracker.setActive(true);

		crudService.create(tatTracker);
	}


	 @Schedule(hour="*/1", dayOfWeek="*",
	 dayOfMonth="*", month="*", year="*", info="tat executer")

	public void getTatUp(final Timer t) {
		
		log.debug("@ Executing TAT scheduler at {}",new Date());
		
		String query = "select * from event_tracker e where e.eventTime <'" + LocalDateTime.now()
				+ "' AND e.active=1  AND e.event like 'TAT_%'";

		List<EventTracker> eventTrackers = crudService.findByNativeQuery(query, EventTracker.class);

		eventTrackers.forEach(eventTracker -> {

			TatMaster tatMaster = this.getTatByService(eventTracker.getEvent().substring(4)).get(0);

			JsonObject tatConfig = tatMaster.getTatLevel();

			if (tatConfig.has("level" + (eventTracker.getExecutionLevel() + 1) + "TatTime")) {

				Long tatTime = tatConfig.get("level" + (eventTracker.getExecutionLevel() + 1) + "TatTime").getAsLong();
				eventTracker.setEventTime(LocalDateTime.now().minus(Duration.ofMillis(tatTime)));
				eventTracker.setActive(true);
			} else
				eventTracker.setActive(false);

			eventTracker.setExecutionLevel(eventTracker.getExecutionLevel() + 1);
			
			if(eventTracker.getUserActionMaster().getActive())
		eventTracker.setUserActionMaster(this.updateUserAction(eventTracker.getUserActionMaster()));

			crudService.update(eventTracker);
		});

	}

	public List<TatMaster> getTatByService(String sevice) throws NoSuchElementException {

		List<TatMaster> tatMaster = this.tatMasters.stream().filter(tatMastr -> tatMastr.getTatService().equals(sevice))
				.collect(Collectors.toList());
		return tatMaster;

	}

	public void getTatByService() {

		String query = "select actionId,executionLevel  from tat_log where eventTime < currentTime AND isActive=1";

		List<EventTracker> eventTrackers = crudService.findByNativeQuery(query, EventTracker.class);

		eventTrackers.forEach(eventTracker -> {

			this.updateUserAction(eventTracker.getUserActionMaster());

		});

	}

	private UserActionMaster updateUserAction(UserActionMaster userActionMaster) {
		int exclanationlvl = userActionMaster.getExclanationLevel();
          
		userActionMaster.setStatus(Status.ESCLATED.toString());
		userActionMaster.setActive(false);
		crudService.update(userActionMaster);

		NotificationMaster notificationMaster = new NotificationMaster();
		notificationMaster.setCreationTime(new Date());
		notificationMaster.setMessage("Request with workorder no " + userActionMaster.getRequestMaster().getWorkorder()
				+ " has been esclated.");
		notificationMaster.setReciever(userActionMaster.getOwner());
		notificationMaster.setRequestMaster(userActionMaster.getRequestMaster());
		notificationMaster.setStatus(Status.PENDING.toString());
		notificationMaster.setSender("SYSTEM GENERATED");
		notificationMaster.setRecieverRole(Roles.SPOC.toString());
		notificationMaster.setRead(false);
		crudService.create(notificationMaster);

		
		
		UserActionMaster actionMaster = new UserActionMaster();

		actionMaster.setPreviousOwnerUPN(userActionMaster.getOwner());
		actionMaster.setRole(Roles.OTHERSPOC.toString());
		actionMaster.setOwner(adStore
				.getManager(
						userActionMaster.getRole().equals(Roles.SPOC.toString())
								? roleManager
										.getEmpByRole(Roles.SPOC.toString(),
												userActionMaster.getRequestMaster().getLocation(),
												userActionMaster.getRequestMaster().getService())
										.get(0).getUserPrincipalName()
								: userActionMaster.getOwner())
				.getUserPrincipalName());
		actionMaster.setActive(true);
		actionMaster.setStatus(Status.PENDING.toString());
		actionMaster.setAssignTime(new Date());
		actionMaster.setExclanationLevel(exclanationlvl + 1);
		actionMaster = crudService.create(actionMaster);

		notificationMaster = new NotificationMaster();
		notificationMaster.setCreationTime(new Date());
		notificationMaster.setMessage("You have one new request with workorder no "
				+ userActionMaster.getRequestMaster().getWorkorder() + " for closure.");
		notificationMaster.setReciever(actionMaster.getOwner());
		notificationMaster.setRequestMaster(actionMaster.getRequestMaster());
		notificationMaster.setStatus(Status.PENDING.toString());
		notificationMaster.setSender("SYSTEM GENERATED");
		notificationMaster.setRecieverRole(Roles.SPOC.toString());
		notificationMaster.setRead(false);
		crudService.create(notificationMaster);
		return actionMaster;
	}

	@Override
	public boolean addTatPriority(TatMaster tatMaster) {

		JsonObject tatLevel = tatMaster.getTatLevel();
		JsonObject tatConfig = new JsonObject();
		String finalLevel = "";
		if (tatLevel.has(TatLevelKeys.level1TatTime.toString())
				&& tatLevel.get(TatLevelKeys.level1TatTime.toString()).getAsString() != null
				&& !tatLevel.get(TatLevelKeys.level1TatTime.toString()).getAsString().equals("")) {
			finalLevel = "1";
			int hours = Integer.parseInt(tatLevel.get(TatLevelKeys.level1TatTime.toString()).getAsString());
			tatConfig.addProperty(TatLevelKeys.level1TatTime.toString(), TimeUnit.HOURS.toMillis(hours));
			if (tatLevel.has(TatLevelKeys.level2TatTime.toString())
					&& tatLevel.get(TatLevelKeys.level2TatTime.toString()).getAsString() != null
					&& !tatLevel.get(TatLevelKeys.level2TatTime.toString()).getAsString().equals("")) {
				finalLevel = "2";
				hours = Integer.parseInt(tatLevel.get(TatLevelKeys.level2TatTime.toString()).getAsString());
				tatConfig.addProperty(TatLevelKeys.level2TatTime.toString(), TimeUnit.HOURS.toMillis(hours));

				if (tatLevel.has(TatLevelKeys.level3TatTime.toString())
						&& tatLevel.get(TatLevelKeys.level3TatTime.toString()).getAsString() != null
						&& !tatLevel.get(TatLevelKeys.level3TatTime.toString()).getAsString().isEmpty()) {
					finalLevel = "3";
					hours = Integer.parseInt(tatLevel.get(TatLevelKeys.level3TatTime.toString()).getAsString());
					tatConfig.addProperty(TatLevelKeys.level3TatTime.toString(), TimeUnit.HOURS.toMillis(hours));

					if (tatLevel.has(TatLevelKeys.level4TatTime.toString())
							&& tatLevel.get(TatLevelKeys.level4TatTime.toString()).getAsString() != null
							&& !tatLevel.get(TatLevelKeys.level4TatTime.toString()).getAsString().isEmpty()) {
						finalLevel = "4";
						hours = Integer.parseInt(tatLevel.get(TatLevelKeys.level4TatTime.toString()).getAsString());
						tatConfig.addProperty(TatLevelKeys.level4TatTime.toString(), TimeUnit.HOURS.toMillis(hours));
					}

				}
			}
		}

		tatConfig.addProperty(TatLevelKeys.levelFinal.toString(), finalLevel);

		tatMaster.setTatLevel(tatConfig);
		tatMaster.setUpdationTime(new Date());

		crudService.create(tatMaster);
		return true;
	}

	@Override

	public List<TatMaster> getAllTatPriority() {
		List<TatMaster> tatMasterList = crudService.findWithNamedQuery("TatMaster.findAll");
		return tatMasterList;
	}

	@Override
	public TatMaster getTatDetailByService(String tatServiceName) {
		List<TatMaster> tatMasterList = crudService.findWithNamedQuery("TatMaster.findByService",
				QueryParameter.with("tatServiceName", tatServiceName).parameters());
		if (tatMasterList.isEmpty())
			return null;
		return tatMasterList.get(0);
	}

	@Override
	public boolean deleteTatByService(String tatServiceName) {
		List<TatMaster> tatMasterList = crudService.findWithNamedQuery("TatMaster.findByService",
				QueryParameter.with("tatService", tatServiceName).parameters());
		if (tatMasterList.isEmpty())
			return false;
		crudService.delete(TatMaster.class, tatMasterList.get(0).getId());
		return true;
	}

	@Override
	public boolean deleteTatById(int tatId) {
		crudService.delete(TatMaster.class, tatId);

		return true;
	}

	@Override
	public boolean updateTatLevelDesignation(int tatDetailId, TatLevelKeys tatLevelDesignationKeyName, String value) {
		TatMaster tatMaster = crudService.find(TatMaster.class, tatDetailId);
		JsonObject tatLevel = tatMaster.getTatLevel();
		tatLevel.addProperty(tatLevelDesignationKeyName.toString(), value);

		tatMaster.setUpdationTime(new Date());
		crudService.update(tatMaster);
		return true;
	}

	@Override
	public boolean updateTatLevelTatTime(int tatDetailId, TatLevelKeys tatLevelTimeKeyName, int timeInHours) {
		long timeInMillis = TimeUnit.HOURS.toMillis(timeInHours);
		TatMaster tatMaster = crudService.find(TatMaster.class, tatDetailId);
		JsonObject tatLevel = tatMaster.getTatLevel();
		tatLevel.addProperty(tatLevelTimeKeyName.toString(), timeInMillis);
		tatMaster.setUpdationTime(new Date());
		String finalLevel = "";
		if (tatLevel.has(TatLevelKeys.level1TatTime.toString())) {
			finalLevel = "1";
		}
		if (tatLevel.has(TatLevelKeys.level2TatTime.toString())) {
			finalLevel = "2";
		}
		if (tatLevel.has(TatLevelKeys.level3TatTime.toString())) {
			finalLevel = "3";
		}
		if (tatLevel.has(TatLevelKeys.level4TatTime.toString())) {
			finalLevel = "4";
		}
		tatLevel.addProperty(TatLevelKeys.levelFinal.toString(), finalLevel);

		crudService.update(tatMaster);
		return true;
	}

	@Override
	public boolean addDesignationWithApprov(DesignationPriority dp) {
		dp.setUpdationTime(new Date());
		crudService.create(dp);
		return true;
	}

	@Override
	public DesignationPriority getDesignationWithApprov(String designation) {
		List<DesignationPriority> dpList = crudService.findWithNamedQuery("DesignationPrioriy.findByDesignation",
				QueryParameter.with("designation", designation).parameters());
		if (dpList.isEmpty())
			return null;
		return dpList.get(0);
	}

	@Override
	public boolean updatePriority(String designation, String priority) {
		List<DesignationPriority> dpList = crudService.findWithNamedQuery("DesignationPrioriy.findByDesignation",
				QueryParameter.with("designation", designation).parameters());
		if (dpList.isEmpty())
			return false; // for designation priority
		DesignationPriority dp = dpList.get(0);
		dp.setPriority(priority);
		crudService.update(dp);
		return true;
	}

	@Override
	public boolean updateIsApprove(String designation, boolean isAboveAed) {
		List<DesignationPriority> dpList = crudService.findWithNamedQuery("DesignationPrioriy.findByDesignation",
				QueryParameter.with("designation", designation).parameters());
		if (dpList.isEmpty())
			return false;
		DesignationPriority dp = dpList.get(0);
		dp.setAboveAed(isAboveAed);
		crudService.update(dp);
		return true;
	}

	public EventTracker getEventByAction(int actionId) {
		String querystr = "select * from event_tracker where actionId=" + actionId + " AND service like 'TAT_%'";

		Query query = entityManager.createNativeQuery(querystr);

		return (com.admin4u.persistence.entity.EventTracker) query.getSingleResult();

	}

	@Override
	public boolean isTatExist(String tatServiceName) {
		List<TatMaster> tatMasterList = crudService.findWithNamedQuery("TatMaster.findByService",
				QueryParameter.with("tatServiceName", tatServiceName).parameters());
		if (tatMasterList.isEmpty())
			return false;

		return false;
	}

	public String getCommentsByservice(String service) {
		
		List<TatMaster> tatMasters = this.getTatByService(service);
		JsonObject commentsConfig = new JsonObject();

		commentsConfig.addProperty("service", service);
		for (TatMaster tatMaster : tatMasters) {
			commentsConfig.addProperty(tatMaster.getComments(),
					TimeUnit.MILLISECONDS.toHours(tatMaster.getTatLevel().get(TatLevelKeys.level1TatTime.toString()).getAsLong()));
		}

		return commentsConfig.toString();

	}
	
	
	 
	// @Schedule(second="*/10", minute="*", hour="1-23", dayOfWeek="Mon-Fri",
		// dayOfMonth="*", month="*", year="*", info="MyTimer")
  private  void manageEvent(final Timer t) {

		List<EventTracker> eventTrackers=crudService.findWithNamedQuery("EventTracker.findTimeUpEvent",
				QueryParameter.with("currentTime",LocalDateTime.now()).parameters());
		for(EventTracker eventTracker:eventTrackers)
		{
		int requestId=eventTracker.getRequestMaster().getRequestId();
		
		Event event = Event.valueOf(eventTracker.getEvent());

		RequestMaster requestMaster = requestResponseManager.getRequestById(requestId);

		LocalDateTime eventTime = eventTracker.getEventTime();

		switch (event) {
		case EXPIRE: {
			requestMaster.setExpire(true);
			requestMaster.setRequestStatus(Status.EXPIRE.toString());
			List<UserActionMaster> actionMasters = actionManager.updateOthers(requestMaster.getUserAction().get(0),
					Status.EXPIRE.toString());
			requestMaster.setUserAction(actionMasters);
			eventTracker.setStatus(Status.COMPLETE.toString());
			eventTracker.setRequestMaster(requestMaster);
			eventTracker.setActive(false);
			crudService.update(eventTracker);// generate notification
			log.debug("inside manage event");
		}
			break;
		case CANCELTIMEUP:
			requestMaster.setActive(true);
			requestMaster.setRequestStatus(Status.TIME_UP.toString());
			eventTracker.setStatus(Status.COMPLETE.toString());
			eventTracker.setRequestMaster(requestMaster);
			eventTracker.setActive(false);
			crudService.update(eventTracker);// generate notification
			log.debug("inside manage event cancel time up");

			break;

		case UNALLOCATE_GUESTHOUSE_ROOMS: {

			List<MeetingRoomBookingDetail> bookingDetails = guestHouseManager
					.getMeetingRoomByReqId(eventTracker.getRequestMaster());

			bookingDetails.forEach(bookingDetail -> {

				SMRoom room = bookingDetail.getMeetingRoom();
				room.setAllocationStatus(AllocationStatus.UNALLOCATED.toString());
			});

		}
			break;
		default:
			break;
		}

		crudService.update(requestMaster);
	}

	}
}
