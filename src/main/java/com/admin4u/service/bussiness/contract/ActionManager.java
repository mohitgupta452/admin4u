package com.admin4u.service.bussiness.contract;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.persistence.entity.EventTracker;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.ApplicationMetaData.Action;

@Local
public interface ActionManager {
	
	public String apprrovalAuthAction(int actionId , ApplicationMetaData.Action action);
	
	public String adminServe(int actionId,List<String> attachments, Map<String,String[]> requestMap);
	
	public String adminClose(int actionId,String message);
	
	public String adminReject(int actionId);
	
	public String extend(int actionId,String msg ,Date exclanationTime , Action action);
	
	public String adminForword(int actionId,String forwordTo);
	
	public String spocServe(int actionId, List<String> attachment,Map<String,String[]> requestMap);
	
	public String spocClose(int actionId,String message);
	
	public String spocComplete(int actionId,String message);
	
	public String amendRequest(int requestId , String problem,String workloc);
	
	public UserActionMaster commonOperation(int actionId,Action action);
	
	public RequestMaster updateRequest( RequestMaster requestMaster);

	public UserActionMaster getUserActionByActionId(int actionId);

	public String cancelRequest(int requestID);
	
	public RequestMaster processSpaceManagementRequest(RequestMaster request,int noOfDays,int roomId);
	
	public String adminApprove(int actionId);
	
	public String adminAllocate(int actionId);
	
	public String spocAllocate(int actionId);
	
	public void processEvent(int requestId);
	
	public String guestHouseSpocServe(int actionId);
	
	public String guestHouseAdminServe(int actionId);
	
	public String reopenRequest(int requestId, String message);
	
	public String getCommentsByRequest(int requestId);
	
	List<UserActionMaster> updateOthers(UserActionMaster userActionMaster, String status);

	
}
