package com.admin4u.service.bussiness.contract;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.admin4u.persistence.entity.ContactDetail;
import com.admin4u.persistence.entity.ContractDetail;
import com.admin4u.persistence.entity.MisDetail;
import com.admin4u.persistence.entity.PaymentInfo;
import com.admin4u.persistence.entity.Paymentterm;
import com.admin4u.persistence.entity.Podetail;
import com.admin4u.persistence.entity.PrDetail;
import com.admin4u.persistence.entity.VendorInfo;

@Local
public interface VendorManager
{
	
	/*inserting methods */
	public void insertVendorData(Map<String , String> vendorInfoMap, String sameAddress);
	/*public void insertMISBillingData(PaymentInfo paymentInfo);*/
	public void insertMISBillingData(Map<String , String > paymentInfoMap);
	/*public void insertMISContractData(ContractDetail contractDetails);*/
	public void insertMISContractData(Map<String , String> contractDetailsMap);
    /*public void insertMISCommercialData(Podetail poDetail , PrDetail prDetail);*/
	public void insertMISCommercialData(Map<String , String> commercialMap);
	/*public void insertPaymentData(Paymentterm payment);*/
	public void insertPaymentData(Map<String , String> paymentMap);
	/*public void insertContactData(ContactDetail contactDetail);*/
	public void insertContactData(Map<String ,String > contactMap);
	
	/* get methods*/
	public List<VendorInfo> getVendorData();
	public List<ContactDetail> getContactDetailsData();
	public List<Paymentterm> getPaymentDetailsData();
	public List<PaymentInfo> getBillingDetailsData();
	public List<ContractDetail>  getContractDetailsData();
	public List<Podetail> getPoDetailsData();
	public List<PrDetail> getPrDetailsData();
	public List<MisDetail> getMisDetailData();
	public List<String> allVendorName();
	
	/* inactive payment row*/
	public boolean inActiveThePaymentRow(int paymentId);
	public boolean inActiveTheContactRow(int contactId);
}
