package com.admin4u.service.bussiness.contract;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.admin4u.persistence.entity.ProductDetail;
import com.admin4u.persistence.entity.RequestData;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.utils.Request;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.Services;
import com.google.gson.JsonObject;

@Local
public interface RequestResponseManager {
	
	public RequestMaster insertData(Map<String, String[]> requestMap) ;
	
	public List<Map<String, Object>> getRequest(String user, String service);
	
	public RequestMaster getRequestById(int requestId);
	
	public List<Map<String, Object>> getRequestByOwner(String owner, Roles role);
	
	public String getRequestDataByReqID(int requestID,int... actionIDs);
	
	public void insertResonseData(Map<String, String[]> requestMap);
	
	public String getResponseDataByReqID(int requestID);
	
	public List<UserActionMaster> getRequestbyView(String owner,String role);
	
	public List<Map<String, Object>> getSpocRequest(String upn,Roles roles);
	
	public List<Map<String,Object>> getAuthRequest(String upn);
	
	public String getResponseMsgByReqID(int requestId);
	
	public String getAmendReqMsgByReqID(int requestId);
	
	public void setRequestEvent(RequestMaster requestMaster);

	
	public void search(Map<String,Object> searchMap);
	
	public List<Map<String , Object>> getaAllReqByUser(String upn);

		
	public void setRequestEvent(RequestMaster requestMaster, Services service, Request request);
	
	//public void setRequestOwnerForMultiPassenger(Map<String, Object> reqMap, RequestMaster requestMaster,List<String> serviceList,String employeeId) throws Exception;
	
	
	public List<RequestData> getProductRequisitionData(String service);
	
	public List<ProductDetail> getProductDetails(int productid);
	
	public JsonObject getRequestCount();
	
	public String generateWorkOrder(RequestMaster requestMaster);


}
