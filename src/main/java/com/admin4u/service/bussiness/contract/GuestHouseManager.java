package com.admin4u.service.bussiness.contract;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.admin4u.persistence.entity.MeetingRoomBookingDetail;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.RoomStuff;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.persistence.entity.SMRoom;

public interface GuestHouseManager
{
	
	public String addNewGuestHouseWithRooms(SMBuilding guestHouse,List<SMRoom> roomList,int LocationId);
	
	public boolean addGuestHouseRoomDetails(List<RoomStuff> roomStuffList,int roomId);

	public List<SMBuilding> getGuestHouseByLocation(int locationID);

	public List<SMRoom> getRoomListByGuestHouseId(int buildingId);

	public List<RoomStuff> getRoomStuffByRoomId(int roomId);
	public boolean deleteRoomStuff(List<Integer> roomStuffIdList);
	
	public boolean updateRoomStuffsByRoomId(int roomId,List<String> stuffNameList);
	
	public boolean deleteRoomStuffByRoomAndStuffName(int roomId,List<String> stuffNameList);
	
	public boolean addRoomStuffByRoomAndStuffName(int roomId, String stuffName);
	
	public boolean deleteRoomStuffByRoomAndStuffName(int roomId,String stuffName);
	
	public boolean allocateRoom(int roomId,int requestId,int actionId,String action,Date checkInDateTime,Date checkOutDateTime,String spocComment);
	
	public boolean unallocateRoom(int roomId);
	
	public List<SMRoom> getRoomListByOccupacyType(int buildingId,String occupacyType);
	
	public List<Map<String,String>> getGuestHouseRoomCounts(int locationId);
	

	public List<MeetingRoomBookingDetail> getMeetingRoomByReqId(RequestMaster requestMaster);

	public boolean checkGuestRoomAvailability(int roomId, Date startDateTime, Date endDateTime);
	
	public List<MeetingRoomBookingDetail> getRoomHistory(int roomId);
	
	public Map<String,String> getCheckinCheckOutTime(int requestId); 
	
	public boolean processGuestHouseRequestNew(int actionId,int roomId,Date checkInDateTime,Date checkOutDateTime);
	
	public	Map<String,String[]> getRoomDetailMap(int roomId);
	
	public boolean updateRoomCode(int roomId,String roomCode);
	
	public boolean updateBedType(int roomId,String bedType);
	
	public boolean detleteRoom(int roomId);
	
	public boolean deleteGuestHouse(int guestHouseId);








	
	



	
	

}
