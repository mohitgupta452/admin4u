package com.admin4u.service.bussiness.contract;

import java.util.List;

import javax.ejb.Local;

import com.admin4u.persistence.entity.LocServiceMap;
import com.admin4u.persistence.entity.Location;
import com.admin4u.persistence.entity.PolicyDoc;

@Local
public interface LocationManger {
	public List<String> getWorkLocation(String location);
	
	public List<String> getAllWorkLocationName();
	
	public int getWorkLocationID(String worklocation); 
	
	public void delete(int worklocationID);
	
	public void insertData(String locationName,String workLocationName);
	
	public void updateLocation(String newWorklocation,int worklocationId);
	
	public List<String> getWorkLocationBYCurrentLocation();
	
	public List<LocServiceMap> getLocServiceMapByLocation(String locationName);
	
	public boolean getServiceStatusByLocation(String serviceName,String locationName);
	
	public boolean turnOnOffServiceByServiceNameAndLocation(LocServiceMap service);
	
	public int getLocationIdByName(String locationName);
	
	public String getParentLocation(String locationName);
	
	public List<String> getSubLocations(String location);
	
	public String uploadPolicyDoc(PolicyDoc policyDoc);
	
	public List<PolicyDoc> getPoliciesByLoc(String location);
	
	public String removePolicy(int policyId);
	
	public Location getLocByName(String loc);
	
	public List<Location> getAllParentLocation(int locId);
	
	public String mapSubLocation(int parentLocId, String[] subLocations) ;

}
