package com.admin4u.service.bussiness.contract;

import java.util.List;

import javax.ejb.Local;

import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.persistence.entity.DesignationPriority;
import com.admin4u.persistence.entity.EventTracker;
import com.admin4u.persistence.entity.TatMaster;
import com.admin4u.util.service.ApplicationMetaData.TatLevelKeys;

@Local
public interface EventTatManager {
	
	enum ExclanationLevel{
		
		LEVEL_ONE(1),
		LEVEL_TWO(2),
		LEVEL_THREE(3),
		LEVEL_FOUR(4);
		
        private int exclanationLvl;
        
		private ExclanationLevel(int exclanationLvl) {
			this.exclanationLvl=exclanationLvl;
		}
	}
	public void createTatEvent(UserActionMaster userActionMaster);

	public boolean addTatPriority(TatMaster tatMaster);

	public TatMaster getTatDetailByService(String tatServiceName);

	public boolean deleteTatByService(String tatServiceName);

	public boolean deleteTatById(int tatId);

	public boolean updateTatLevelDesignation(int tatDetailId, TatLevelKeys tatLevelDesignationKeyName,
			String value);

	public boolean updateTatLevelTatTime(int tatDetailId, TatLevelKeys tatLevelTimeKeyName,
			int timeInHours);

	public boolean addDesignationWithApprov(DesignationPriority dp);

	public DesignationPriority getDesignationWithApprov(String designation);

	public boolean updatePriority(String designation, String priority);

	boolean updateIsApprove(String designation, boolean isApprove);

	List<TatMaster> getAllTatPriority();

	public  EventTracker getEventByAction(int actionId);
	public boolean isTatExist(String serviceName);
	
	public String getCommentsByservice(String service);

}
