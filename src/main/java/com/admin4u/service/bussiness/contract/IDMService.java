package com.admin4u.service.bussiness.contract;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.util.enums.Services;

@Local
public interface IDMService {
	
	public List<EmployeeData> getAllEmployeeData();
	
	public EmployeeData getEmpByUpn(String upn);
	
	public EmployeeData getEmpByMail(String mail);
	
	public String assignServiceToEmp(String upn,Services service);
	
	public Map<String,String> login (String loginId);
	
	public String  getEmpNameByUpn(String upn);
	
	
	public String getEmployeeUPNByEmployeeId(String employeeId);
	
	public String getEmployeeNameByEmployeeId(String employeeId);

	public EmployeeData getEmployeeDataByEmployeeId(String employeeId);
	
	public boolean isAuthenticated(String mail);
	
	public boolean isOtherSpoc(String upn);



}
