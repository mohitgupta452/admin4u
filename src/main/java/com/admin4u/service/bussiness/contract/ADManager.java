package com.admin4u.service.bussiness.contract;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import com.admin4u.persistence.dto.EmployeeDTO;

/**
 *
 * @author Ashish Singh Dev
 *
 */
@Local
public interface ADManager extends Serializable {

	public enum SearchCriteria {
		ALL, BY_EMP_NAME, BY_CONTACT_NO, BY_EMAIL, BY_EMP_CODE, BY_UPN;
	}

	public List<EmployeeDTO> getAllRecords();

	public EmployeeDTO searchByUPN(String upn);

	public void syncADRecords();

}
