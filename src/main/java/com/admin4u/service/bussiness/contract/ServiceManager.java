package com.admin4u.service.bussiness.contract;

import java.util.List;
import java.util.Map;

import com.admin4u.persistence.entity.LocServiceMap;
import com.admin4u.persistence.entity.NonApprovalDesignation;
import com.admin4u.persistence.entity.PriorityTatMap;

public interface ServiceManager {
	
	public boolean turnOnOffServiceByServiceNameAndLocation(LocServiceMap service);
	
	public long getTATTimeByPriority(String priority);
	
	public void genrateTatNotifications(String role);
	
	public List<Map<String, Object>> getRequestForTatNotification(String role);
	
	public String addNewPriority(PriorityTatMap ptm);
	
	public String updateTatValue(PriorityTatMap ptm);
	
	public List<PriorityTatMap> getAllTats() ;

	public PriorityTatMap getPriorityByService(String service);
	
	public List<String> getAllDesignation();
	
	public boolean addNonApprovalDesignation(NonApprovalDesignation nonAppdesig);
	
	public List<String> getNonApprovalDesignationByActive();
	
	

}
