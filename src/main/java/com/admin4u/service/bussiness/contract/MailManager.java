package com.admin4u.service.bussiness.contract;

import com.admin4u.persistence.dto.MailDTO;
import com.admin4u.service.bussiness.contract.MailManager.MailEvents;

public interface MailManager {
	
	public  enum MailEvents{GENERATED,
		APPROVAL_AUTHORITY_ACCEPT,
		ADMIN_COMPLETE,
		ADMIN_SERVE,
		SPOC_SERVE,
		;
		
	}
	
	public void sendEmail(MailEvents event, boolean sendAsynchronously);
	
	public void sendEmail(boolean sendAsynchronously);
	
	public void saveEmail(MailDTO mailDTO);

}
