package com.admin4u.service.bussiness.contract;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

import com.admin4u.persistence.entity.ProductDetail;
import com.admin4u.persistence.entity.StockInfo;
import com.admin4u.util.enums.HostType;


@Local

public interface InventoryManager
{
	public void assignProduct(Map<Integer, Integer> assignedProducts, String assignTo,String assignBy);
	
	public String updateStock(List<Map<String,Object>> products);
	
	public List<StockInfo> getAllStockInfo();
	

}
