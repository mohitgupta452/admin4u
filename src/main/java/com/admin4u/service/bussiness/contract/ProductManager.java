package com.admin4u.service.bussiness.contract;

import java.util.*;

import javax.ejb.Local;

import com.admin4u.persistence.entity.CategaryClosure;
import com.admin4u.persistence.entity.CategaryMaster;
import com.admin4u.persistence.entity.ProductDetail;
import com.admin4u.persistence.entity.StockInfo;
import com.admin4u.util.enums.Type;

@Local
public interface ProductManager {
	
	public String registerProduct(List<Map<String,Object>> products);
	
	public ProductDetail getProductDetails(int productId);
	
	public List<ProductDetail> getProductsByCategary(int categaryId,Type type);
	
	public List<CategaryMaster> getAllCategary();
	
	public List<CategaryClosure> getCatDescendant(int ancestor);
	
	public List<CategaryClosure> getCategoriesByDepth(int depth);
	
	public CategaryClosure addSubCategory(int masterCat,String subcat);

	public List<ProductDetail> getProductDetails();
	
	public List<StockInfo> getStockDetails();

	public List<ProductDetail> getAllProduct();



	

}
