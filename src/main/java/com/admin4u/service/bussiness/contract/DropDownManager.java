package com.admin4u.service.bussiness.contract;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.admin4u.persistence.entity.DeptCostCenterMap;
import com.admin4u.persistence.entity.DropDowns;
import com.admin4u.persistence.entity.EmployeeData;

@Local
public interface DropDownManager {
	
	
	
	public DropDowns getDropDownDataByName(String dropDownName);
	
	public DropDowns getDropDownDataByID(int dropDownID);
	
	Map<String,String> getKeyValueMapForExcel(String dropDownName);
	public boolean updateDropDownData(String dropDownName,String data);
	public List<String> getDropDownValuesListByName(String dropDownName);
	
	public List<String> getDepartmentList();
	public List<DeptCostCenterMap> getAllDeptCostCenterMap();
	public List<DeptCostCenterMap> saveDeptCostCenterMap(String department,String costCenter);
	public List<String> getAllLocationName();

	boolean removeDeptCostCentreMap(int deptCostCentreMapId);

	
	public String getCostCenter(String upn);
	public String getDepartment(String upn);
	public String getCompanyName(String upn);
	public List<EmployeeData> getAllCompanyName();
	public String getLocation(String upn);
	
	public List<String> getAllLocationByType(String type);
	//public List<String> getAllLocation();


}