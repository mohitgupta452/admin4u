package com.admin4u.service.bussiness.contract;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.admin4u.persistence.entity.EventTracker;
import com.admin4u.persistence.entity.NotificationMaster;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.util.service.ApplicationMetaData.Action;
import com.admin4u.util.service.ApplicationMetaData.Status;
@Local
public interface NotificationManager {
	
	public void setNotificationByAction(Map<String,String> notificationMap,UserActionMaster userAction,RequestMaster requestMaster);
	
	public List<NotificationMaster> getNotificationByUPN(String UPN,String status);
	
	public List<NotificationMaster> getNotificationByUPNStatus(String UPN,Status status);
	
	public List<NotificationMaster> getLimitedNotificationByUPN(String UPN,int limit);
	
	public List<String> getNotificationByUPN(String UPN);
	
	public void generateEmail(RequestMaster requestMaster,Map<String,String> mailMap,Action action);

	public boolean processSpaceManagementSystemNotification(RequestMaster requestMaster,EventTracker requestEvent);
	
	public List<NotificationMaster> getCurrentNotification(String upn,Date lastTime);
	
	public String markAsRead(List<Integer> notificationsId);	
	

}
