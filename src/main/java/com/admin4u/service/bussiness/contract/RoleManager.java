package com.admin4u.service.bussiness.contract;

import java.util.List;

import javax.ejb.Local;

import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.util.enums.Roles;

@Local
public interface RoleManager {
	
	public String createRole(String[] upns,Roles role,String location,String[] services,String createdBy);
	
	public boolean deleteRole(int employeeRoleID);
	
	public List<EmployeeRole> getEmpByRole(String role,String location,String service);
	
	public List<String> getEmpRoleByUPN(String upn);
	
	public List<EmployeeRole> getEmployeeRolesByCreater(String createrUPN,Roles roles);
	
	public List<EmployeeRole> getEmpRoleByUpn(String upn);
	
	public List<EmployeeRole> getEmpByUpnAndRole(String upn,Roles role);
	
	public List<EmployeeRole> getRolesByLocation(String location,Roles role);
	
	public List<EmployeeRole> getAllRole(Roles role);

}
