package com.admin4u.service.bussiness.adstoreUtil;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.boot.model.source.internal.hbm.ManyToOneAttributeColumnsAndFormulasSource;
import org.slf4j.Logger;

import com.admin4u.persistence.CrudService;
import com.admin4u.persistence.QueryParameter;
import com.admin4u.persistence.dto.EmployeeDTO;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.service.bussiness.contract.ADManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RoleManager;

/**
 *
 * @author Ashish Singh Dev
 *
 */
@Named
@ApplicationScoped
public class ADStore implements Serializable {

	private static final long serialVersionUID = 672279954616895809L;

	@Inject
	private Logger log;

	private List<EmployeeDTO> adStore;

	@Inject
	private ADManager adManager;

	@Inject
	private RoleManager roleManager;

	@Inject
	private CrudService crudService;

	public ADStore() {
	}

	@PostConstruct
	private void initData() {
		adStore = adManager.getAllRecords();
		log.debug("Total Employee Records Found in Database which have email are {}", adStore.size());
	}

	public List<EmployeeDTO> getAdStore() {
		return adStore;
	}

	public void setAdStore(List<EmployeeDTO> adStore) {
		this.adStore = adStore;
	}

	public Set<String> getDistinctLocations() {
		return adStore.stream().filter(emp -> {
			if(emp.getLocation() == null || emp.getLocation().equals(","))return false; 
			return   !emp.getLocation().trim().isEmpty();
		}).map(EmployeeDTO::getLocation).collect(Collectors.toSet());
	}

	public EmployeeDTO getEmployeeByUPN(String upn) {

		Optional<EmployeeDTO> empDTO = adStore.stream().filter(emp -> emp.getUserPrincipalName().equals(upn))
				.findFirst();

		if (!empDTO.isPresent()) {
			return null;
		}

		return empDTO.get();

	}

	public EmployeeDTO getManager(String userPrincipalName) {

		EmployeeDTO empDTO = getEmployeeByUPN(userPrincipalName);

		if (empDTO == null) {
			return null;
		}

		Optional<EmployeeDTO> manager = adStore.stream().filter(emp -> emp.getEmpDN().equals(empDTO.getManagerDN()))
				.findFirst();

		if (!manager.isPresent()) {
			return null;
		}

		return manager.get();
	}

	public List<EmployeeDTO> getEmpByLocation(List<String> locations) {
		log.debug("locations {}", locations);
		return adStore.stream().filter(emp -> locations.contains(emp.getLocation())).collect(Collectors.toList());
	}

	public boolean isManager(String upn) {
		String name = upn.split("\\@")[0];

		if (crudService.findByNativeQuery("select * from employeedata where managerDN like '%CN=" + name + "%'",
				EmployeeData.class).size() > 0)
			return true;
		return false;
	}

	public String getEmail(String userPrincipalName) {

		EmployeeDTO empDTO = getEmployeeByUPN(userPrincipalName);

		if (empDTO == null) {
			return "";
		}

		return empDTO.getEmail();
		/*
		 * Optional<EmployeeDTO> user = adStore.stream().filter(emp ->
		 * emp.getEmpDN().equals(empDTO.getManagerDN())) .findFirst();
		 * 
		 * if (!user.isPresent()) { return null; } return user.get().getEmail();
		 */
	}

	public String getManagerEmail(String userPrincipalName) {
		EmployeeDTO manager = getManager(userPrincipalName);
		if (manager == null) {
			return "";
		}
		return manager.getEmail();
	}

	public Map<String, String> getAuditDepartmentEmp() {

		Map<String, String> upnVsName = new HashMap<>();
		for (EmployeeDTO empDTO : adStore) {
			if (!(empDTO.getDepartment() == null || empDTO.getDepartment().isEmpty())) {
				if (empDTO.getDepartment().equals("Internal Audit")) {
					upnVsName.put(empDTO.getUserPrincipalName(), empDTO.getEmpName());
				}
			}
		}

		return upnVsName;
	}

	public Map<String, String> getAuditDepartmentEmpExceptHEADAndManager() {

		// List<EmployeeRole> emprole =
		// roleManager.getUsersByRole(Roles.IA_HEAD, Roles.IA_MANAGER);
		/*
		 * List<String> upnNegelated = new LinkedList<>(); emprole.forEach(e ->
		 * upnNegelated.add(e.getUserPrincipalName()));
		 */

		List<EmployeeRole> emprole = crudService.findByNativeQuery("SELECT * FROM dalmia_ams.employeerole",
				EmployeeRole.class);

		List<EmployeeRole> upnNegelated = emprole.stream()
				.filter(e -> (!(e.getRole().equals("IA_HEAD") || e.getRole().equals("IA_MANAGER")))).distinct()
				.collect(Collectors.toList());

		Map<String, String> upnVsName = new HashMap<>();
		upnNegelated.forEach(e -> upnVsName.put(e.getUserPrincipalName(), e.getDisplayName()));

		/*
		 * upnVsName.put(upnNegelated.getUserPrincipalName(),
		 * empDTO.getEmpName());
		 * 
		 * for (EmployeeDTO empDTO : adStore) { if (!(empDTO.getDepartment() ==
		 * null || empDTO.getDepartment().isEmpty())) { if
		 * (empDTO.getDepartment().equals("Internal Audit")) { if (upnNegelated
		 * != null && upnNegelated.contains(empDTO.getUserPrincipalName())) { }
		 * else { upnVsName.put(empDTO.getUserPrincipalName(),
		 * empDTO.getEmpName()); } } } }
		 */

		return upnVsName;
	}

	public String getUPNByEmail(String email) {

		Map<String, String> upnVsName = new HashMap<>();
		for (EmployeeDTO empDTO : adStore) {
			if (empDTO.getEmail() != null && !empDTO.getEmail().isEmpty() && empDTO.getEmail().equals(email)) {
				return empDTO.getUserPrincipalName();
			}
		}

		return null;
	}

	public String getUPNByEmailIgnoreCase(String email) {

		Map<String, String> upnVsName = new HashMap<>();
		for (EmployeeDTO empDTO : adStore) {
			if (empDTO.getEmail() != null && !empDTO.getEmail().isEmpty()
					&& (empDTO.getEmail().toLowerCase()).equals(email)) {
				return empDTO.getUserPrincipalName();
			}
		}

		return null;
	}

	public Map<String, String> getAuditName() {

		Map<String, String> upnVsName = new HashMap<>();
		for (EmployeeDTO empDTO : adStore) {
			// if (!(empDTO.getDepartment() == null ||
			// empDTO.getDepartment().isEmpty())) {

			upnVsName.put(empDTO.getUserPrincipalName(), empDTO.getEmpName());
			// }
		}

		return upnVsName;
	}
}
