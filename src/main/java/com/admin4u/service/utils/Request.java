package com.admin4u.service.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.util.enums.Services;
import com.admin4u.util.service.RequestMetadata;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@RequestScoped
public class Request
{

	private Logger					log	= LoggerFactory.getLogger(getClass());

	private Map<String, Object>		commonRequestMap;
	
	private  JsonObject requestData;
	
	public Request()
	{
		commonRequestMap = new HashMap<>();
	}

	private Properties loadProperties(String propFileName) throws IOException
	{
		Properties prop = new Properties();

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

		if (inputStream != null)
		{
			prop.load(inputStream);
		}
		else
		{
			throw new FileNotFoundException("Unable to find property file - " + propFileName);
		}
		return prop;

	}

	private void getReqMap(Map<String,String[]> requestMap)
	{
		for (String reqAtr : RequestMetadata.REQUEST)
		{
			if (requestMap.containsKey(reqAtr))
			{
				if (reqAtr.equals(RequestMetadata.SERVICE_LIST))
				{
					List<String> serviceList = Arrays
							.asList(requestMap.get(reqAtr)[0].split("\\|"));
					commonRequestMap.put("serviceList", serviceList);
				}
				else
					commonRequestMap.put(reqAtr,
							requestMap.get(reqAtr) != null ? requestMap.get(reqAtr)[0] : "");
			}
		}
	}

	
	
	private JsonObject replaceKey(Properties prop,Map<String,String[]> requestMap,String service)
	{
		JsonObject json = new JsonObject();

		//code to insert multiple passenger details in json
		if (requestMap.containsKey("passenger"))
		{
			List<String> employeeNameList = new ArrayList<String>(
					Arrays.asList(requestMap.get("passenger")));
			employeeNameList.removeAll(Arrays.asList("", null));
			log.debug("request.java inside multi request handler ");

			if (employeeNameList.size() > 1)
			{
			int i = 1;
				for (String employeeName : employeeNameList)
				{
					log.debug("pax name size{}", employeeNameList.size());
					json.addProperty("Passenger" + i + "_Name", employeeName);
					i++;
				}
			}
			
			json.addProperty("totalPax", requestMap.get("totalPax")[0]);
		}

		Set<String> proNames = prop.stringPropertyNames();

		List<String> filterProp = proNames.stream()
				.filter((proName -> proName.contains(service.toLowerCase())))
				.collect(Collectors.toList());

		//end
		requestMap.forEach((k,v)->{
			log.debug("request map  key {}  val {}",k,v.length>0?v[0]:"na");
		});
		for (String atr : filterProp)
		{
			log.debug("pro {} .. ", atr);
			if (requestMap.containsKey(prop.getProperty(atr))
					&& requestMap.get(prop.getProperty(atr)).length > 0)
			{
				if (requestMap.get(prop.getProperty(atr))[0] != null
						&& !requestMap.get(prop.getProperty(atr))[0].equals(""))
					json.addProperty(atr.trim(), requestMap.get(prop.getProperty(atr))[0]);
			}
		}
		log.debug("req data map {} ", json);
		return json;

	}
	
	private JsonObject replaceKeyForInventory(Properties prop,Map<String,String[]> requestMap,String service)
	{
		log.debug("inside inventory key replacement");
		JsonObject json = new JsonObject();

		Set<String> proNames = prop.stringPropertyNames();

		List<String> filterProp = proNames.stream()
				.filter((proName -> proName.contains(service.toLowerCase())))
				.collect(Collectors.toList());
		
		int count=Integer.parseInt(requestMap.get("total")[0]);
		
		for (String atr : filterProp)
		{
			for(int i=0;i<=count ;i++)
			{
			if (requestMap.containsKey(prop.getProperty(atr)+i)
					&& requestMap.get(prop.getProperty(atr)+i).length > 0)
			{
				
				if (requestMap.get(prop.getProperty(atr)+i)[0] != null
						&& !requestMap.get(prop.getProperty(atr)+i)[0].equals(""))
				{
					
					json.addProperty(((atr)+i).trim(), requestMap.get(prop.getProperty(atr)+i)[0]);

				}
			}
			}
		}
		log.debug("common req data map {} ", json);
		return json;
	}

	
	
	public void filterRequest(Map<String, String[]> requestMap)
	{
		Properties properties=null;

		log.debug("req map  {} ", requestMap);
		
		String service ="";
		if(requestMap.get("service")[0].equals(Services.HOTEL_BOOKING.toString()) || 
			requestMap.get("service")[0].equals(Services.GUEST_HOUSE_BOOKING.toString()))
			service ="hotelguesthouse".toUpperCase();
		else
          service=	requestMap.get("service")[0];
		try
		{
			properties=this.loadProperties("serviceAttribute.properties");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		this.getReqMap(requestMap);
		requestData= this.replaceKey(properties,requestMap,service);
	}

	
	public Map<String, Object> getCommonRequestMap()
	{
		return commonRequestMap;
	}

	public JsonObject getRequestDataJson()		
	{
		return this.requestData;
	}

}
