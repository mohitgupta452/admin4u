package com.admin4u.service.ws;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.internal.process.RespondingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.LocServiceMap;
import com.admin4u.persistence.entity.ProductDetail;
import com.admin4u.persistence.entity.RequestData;
import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.persistence.entity.UserActionMaster;
import com.admin4u.service.bussiness.contract.EventTatManager;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.ProductManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.enums.Type;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.RequestMetadata;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


@Local
@Stateless
@Path("/data")
public class RestDataRetrievalHelper implements Serializable
{

	private static final long	serialVersionUID	= 5406295124886195991L;

	private Logger log =LoggerFactory.getLogger(getClass());
	
	@Inject
	private EventTatManager eventTatManager;
	
	@Inject
	private RequestResponseManager requestResponseManager;
	
	@Inject
	private UserSessionBean userSession;
	
	@Inject
	private IDMService idmService;
	
	@Inject
	private LocationManger locationManager;
	
	@Inject
	private ProductManager productManager;

	private Response okEmptyJsonResponse()
	{
		return Response.ok("{}", MediaType.APPLICATION_JSON).build();
	}

	@GET
	@Path("/test")
	@Produces("text/event-stream")
	public Response getTestDetails()
	{
		String data="data: {\n";
		List<UserActionMaster> userActionList=requestResponseManager.getRequestbyView(userSession.getEmpData().getUserPrincipalName(),"APPROVAL_ATHORITY");
		
		
		JsonArray jsonArray= new JsonArray();
		int count=1;
		for(UserActionMaster userAction:userActionList)
		{
			JsonObject jsonObj = new JsonObject();

			RequestMaster requestMaster= userAction.getRequestMaster();
			
			Calendar date = Calendar.getInstance();
			date.add(Calendar.SECOND, 30);
			Date dateBefore30Sec=date.getTime();
			if(requestMaster.getRequestDate().compareTo(dateBefore30Sec)>0)
			{
			jsonObj.addProperty(RequestMetadata.LOCATION, requestMaster.getLocation());
			jsonObj.addProperty(RequestMetadata.REQUESTER,idmService.getEmpNameByUpn(requestMaster.getRequestBy()));
			jsonObj.addProperty(RequestMetadata.Service, requestMaster.getService());
			jsonObj.addProperty("requestType", requestMaster.getRequestType());
			jsonObj.addProperty(RequestMetadata.REQUEST_DATE, requestMaster.getRequestDate().toString());
			jsonObj.addProperty(RequestMetadata.REQUEST_STATUS, requestMaster.getRequestStatus());
			jsonObj.addProperty(RequestMetadata.Request_IP, requestMaster.getIpAddress());
			jsonObj.addProperty(RequestMetadata.REQUEST_ID, requestMaster.getRequestId());
			jsonObj.addProperty("isComplete", requestMaster.isComplete());
			jsonObj.addProperty("priority", requestMaster.getPriority());
			jsonObj.addProperty("workorder", requestMaster.getWorkorder());
			jsonObj.addProperty("status", userAction.getStatus());
			jsonObj.addProperty("owner", userAction.getOwner());
			jsonObj.addProperty("actionId", userAction.getActionID());
			jsonObj.addProperty("isRead", userAction.isRead());

			JsonObject reqDataJsonObj = new Gson().fromJson(requestMaster.getRequestData().getRequestData(), JsonObject.class);

				
			if (reqDataJsonObj.get("approovalAttachment") != null) {
				String path = reqDataJsonObj.get("approovalAttachment").getAsString();
				log.debug("path {}", path);
				jsonObj.addProperty("approvedAttachment", path);
			}
			jsonArray.add(jsonObj);
			}
		}	
			data+="data:\"count\":"+jsonArray+"\n";
			data+="retry: 100000\n";
			count++;
		data+="data: }\n\n";
		return Response.ok(data).build();
	}
	
	@GET
	@Path("/servicestatus")
	public Response getServiceStatus()
	{
		String location=userSession.getLocationName();
		log.debug("getservicestatus"+location);
	List<LocServiceMap> locationServices=locationManager.getLocServiceMapByLocation("location");
	Map<String,Byte> locationServicesMap;
	locationServicesMap=new HashMap<String,Byte>();
	locationServices=locationManager.getLocServiceMapByLocation(location);
	for(LocServiceMap lsmObj:locationServices){
	locationServicesMap.put(lsmObj.getService(), lsmObj.getIsActive());
	}
	Gson gson = new Gson(); 
	String json = gson.toJson(locationServicesMap);
	log.debug(json);	
	return Response.ok(json).build();
	}

	@GET
	@Path("/showresponsedata/{requestID}")
	public Response showResponseData(@PathParam("requestID") int requestID)
	{
		 String responseData = requestResponseManager.getResponseDataByReqID(requestID);
		 log.debug("Response Data : {}",responseData);
		 return Response.ok(responseData).build();
	}
	
	@GET
	@Path("showrequestdata/{requestID}")
	public Response showRequestData(@PathParam("requestID") int requestID){
			String requestData =requestResponseManager.getRequestDataByReqID(requestID);
			return Response.ok(requestData).build();
	}
	
	@GET
	@Path("/showresponsemsg/{requestID}")
	public Response showResponseMsg(@PathParam("requestID") int requestID)
	{
		String responseMsg = requestResponseManager.getResponseMsgByReqID(requestID);
		return Response.ok("{" + "\"responseMsg\":\"" +responseMsg+ "\"}").build();
		
	}
	
/*	@GET
	@Path("/showamendmsg/{requestID}")
	public Response showAmendMsg(@PathParam("requestID") int requestID)
	{
		String amendMsg = requestResponseManager.getAmendReqMsgByReqID(requestID);
		return Response.ok("{" + "\"amendMsg\":\"" +amendMsg+ "\"}").build();
	}*/
	
	@GET
	@Path("/showreopenmsg/{requestID}")
	public Response showReopenMsg(@PathParam("requestID") int requestID)
	{
		String reopenMsg = requestResponseManager.getAmendReqMsgByReqID(requestID);
		return Response.ok("{" + "\"amendMsg\":\"" +reopenMsg+ "\"}").build();
	}
	
	@GET
	@Path("/getallcategory")
	public Response getAllCatgery()
	{
		return Response.ok(productManager.getCategoriesByDepth(0)).build();
	}
	
	@GET
	@Path("/getprodbycategory/{categaryId}")
	public Response getProdByCategary(@PathParam("categaryId") int categaryId)
	{
		return Response.ok(productManager.getProductsByCategary(categaryId,Type.MASTER_CATEGARY)).build();
	}
	
	@GET
	@Produces("application/json")
	@Path("/getsubcategory/{ancestor}")
	public Response getSubCategory(@PathParam("ancestor") int ancestor)
	{
		return Response.ok(productManager.getCatDescendant(ancestor)).build();
	}
	
	@GET
	@Produces("application/json")
	@Path("/getproductdetails")
	public Response getProductDetails()
	{
		return Response.ok(productManager.getProductDetails()).build();
	}
	
/*	@GET
	@Produces("application/json")
	@Path("/getallstockinfo/{service}")
	public Response getStockDetails(@PathParam("service") String service)
	{
		log.debug("+++++++");
		System.out.println("acti++++");
		return Response.ok(requestResponseManager.getProductRequisitionData(service)).build();
	}*/
	
	
	@GET
	@Path("/getproductreqdata")
	public Response getProductRequisitionData()
	{
		List<RequestData> requestdata = requestResponseManager.getProductRequisitionData(Services.INVENTORY.toString());
		return Response.ok(requestdata).build();
	}
	
	@GET
	@Produces("application/json")
	@Path("/getproductdetail/{productid}")
	public Response getProductDetail(@PathParam("productid") int productid)
	{
		List<ProductDetail> pdtDetails =  requestResponseManager.getProductDetails(productid);
		return Response.ok(pdtDetails ).build();
	}
	
	@GET
	@Produces("application/json")
	@Path("/getstatuscount")
	public Response getStatusCount()
	{
		return Response.ok(requestResponseManager.getRequestCount().toString()).build();
	}
}
