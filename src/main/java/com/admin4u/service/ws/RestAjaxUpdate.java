package com.admin4u.service.ws;
import java.io.Serializable;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.admin4u.service.bussiness.contract.ActionManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.ProductManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.views.UserSessionBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Local
@Stateless
@Path("update")
public class RestAjaxUpdate implements Serializable
{
	private static final long		serialVersionUID	= 5975416992295063271L;

	private Logger					log					= LoggerFactory.getLogger(getClass());

	@Inject
	private RequestResponseManager	requestResponseManager;

	@Inject
	private UserSessionBean			userSession;

	@Inject
	private ActionManager			actionManager;

	@Inject
	private LocationManger			locationManager;
	
	@Inject
	private ProductManager productManager;

	private Response okEmptyJsonResponse()
	{
		return Response.ok("{}", MediaType.APPLICATION_JSON).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/cancelrequest/{requestID}")
	public Response cancelrequest(@PathParam("requestID") int requestID)
	{
	   String responseJson= "{\"status\" :\""+actionManager.cancelRequest(requestID)+"\"}";
	   log.debug("response data : {}",responseJson);
		return Response.ok(responseJson.toString(), MediaType.APPLICATION_JSON).build();

	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/reopenrequest")
	public Response reopenRequest(String requestData)
	{
		JsonObject jsonObj = new Gson().fromJson(requestData, JsonObject.class);

		int requestId = jsonObj.get("requestId").getAsInt();
		String reopenMsg = jsonObj.get("reopenMsg").getAsString();

		String responseJson =actionManager.reopenRequest(requestId,reopenMsg);
		log.debug("response data : {}", responseJson);
		return Response.ok(responseJson.toString(), MediaType.APPLICATION_JSON).build();

	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/amendrequest")
	public Response ammedRequest(String requestData)
	{
		
		log.debug("request Data {}",requestData);
		
		JsonObject jsonObj = new Gson().fromJson(requestData, JsonObject.class);

		
		int requestId = jsonObj.get("requestId").getAsInt();
		String description = jsonObj.get("amend_description").getAsString();
		
		String workLocation = jsonObj.get("amend_work").getAsString();
		
		String responseJson = actionManager.amendRequest(requestId, description,workLocation);
		log.debug("response data : {}", responseJson);
		return Response.ok(responseJson.toString(), MediaType.APPLICATION_JSON).build();

	}
	
	
	
	@GET
	@Path("/changelocation/{location}")
	public Response changeLocation(@PathParam("location") String location)
	{
		userSession.setLocationName(location);
		//userSession.setLocationId(locationManager.getLocationIdByName(location));;

		log.debug("location change service {} : ", location);
		return Response.ok().build();
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addsubcategory")
	public Response addSubCategory(String category)
	{
		JsonObject jsonObj = new Gson().fromJson(category, JsonObject.class);
		
		int masterCat=jsonObj.get("masterCat").getAsInt();
		String subcat=jsonObj.get("subcat").getAsString();
		
		return Response.ok(productManager.addSubCategory(masterCat, subcat)).build();
	}
}
