package com.admin4u.views;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.admin4u.persistence.entity.ContactDetail;
import com.admin4u.persistence.entity.ContractDetail;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.MisDetail;
import com.admin4u.persistence.entity.PaymentInfo;
import com.admin4u.persistence.entity.Paymentterm;
import com.admin4u.persistence.entity.Podetail;
import com.admin4u.persistence.entity.PrDetail;
import com.admin4u.persistence.entity.VendorInfo;
import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.VendorManager;


@Named("vendorManagBean")
@RequestScoped
public class VendorBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Logger log= LoggerFactory.getLogger(getClass());
	
	@Inject
	VendorManager  vendorManager;
	@Inject
	DropDownManager dropDownManager;
	public VendorBean() {
	}
	private List<VendorInfo> vendorInfoDataList;
	private List<ContactDetail> contactDetailsList;
	private List<Paymentterm> paymentDetailsList;
	private List<PaymentInfo> billingDetailsList;
	private List<ContractDetail> contractDetailsList;
	private List<Podetail> poDetailsList;
	private List<PrDetail> prDetailsList;
	private List<MisDetail> misDetailList;
	private List<EmployeeData> getCompanyNameList;
	
	
	
	
	
	@PostConstruct
	public  void init()
	{
		vendorInfoDataList = vendorManager.getVendorData();
		contactDetailsList = vendorManager.getContactDetailsData();
		paymentDetailsList = vendorManager.getPaymentDetailsData();
		billingDetailsList = vendorManager.getBillingDetailsData();
		contractDetailsList = vendorManager.getContractDetailsData();
		poDetailsList = vendorManager.getPoDetailsData();
		prDetailsList = vendorManager.getPrDetailsData();
	    getCompanyNameList = dropDownManager.getAllCompanyName();
	   
		
	}
	
	public List<VendorInfo> getVendorInfoDataList() {
		return vendorInfoDataList;
	}
	public void setVendorInfoDataList(List<VendorInfo> vendorInfoDataList) {
		this.vendorInfoDataList = vendorInfoDataList;
	}
	public List<ContactDetail> getContactDetailsList() {
		return contactDetailsList;
	}
	public void setContactDetailsList(List<ContactDetail> contactDetailsList) {
		this.contactDetailsList = contactDetailsList;
	}
	public List<Paymentterm> getPaymentDetailsList() {
		return paymentDetailsList;
	}
	public void setPaymentDetailsList(List<Paymentterm> paymentDetailsList) {
		this.paymentDetailsList = paymentDetailsList;
	}

	public List<PaymentInfo> getBillingDetailsList() {
		return billingDetailsList;
	}

	public void setBillingDetailsList(List<PaymentInfo> billingDetailsList) {
		this.billingDetailsList = billingDetailsList;
	}

	public List<ContractDetail> getContractDetailsList() {
		return contractDetailsList;
	}

	public void setContractDetailsList(List<ContractDetail> contractDetailsList) {
		this.contractDetailsList = contractDetailsList;
	}

	public List<Podetail> getPoDetailsList() {
		return poDetailsList;
	}

	public void setPoDetailsList(List<Podetail> poDetailsList) {
		this.poDetailsList = poDetailsList;
	}

	public List<PrDetail> getPrDetailsList() {
		return prDetailsList;
	}

	public void setPrDetailsList(List<PrDetail> prDetailsList) {
		this.prDetailsList = prDetailsList;
	}
	
	

	public VendorManager getVendorManager() {
		return vendorManager;
	}

	public void setVendorManager(VendorManager vendorManager) {
		this.vendorManager = vendorManager;
	}

	public List<MisDetail> getMisDetailList() {
		return misDetailList;
	}

	public void setMisDetailList(List<MisDetail> misDetailList) {
		this.misDetailList = misDetailList;
	}

	public List<EmployeeData> getGetCompanyNameList() {
		return getCompanyNameList;
	}

	public void setGetCompanyNameList(List<EmployeeData> getCompanyNameList) {
		this.getCompanyNameList = getCompanyNameList;
	}
	
	

}
