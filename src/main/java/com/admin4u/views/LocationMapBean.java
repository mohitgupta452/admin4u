package com.admin4u.views;

import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.entity.Location;
import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.LocationManger;

@Named("locMap")
@RequestScoped
public class LocationMapBean {
	
	@Inject 
	private LocationManger locationManger;
	
	@Inject
	private ADStore adStore;
	
 	 private List<Location> locations;
	
     private Set<String> subLocations;

	public LocationMapBean() {
	}
	
	@PostConstruct
	private void init()
	{
	locations=	locationManger.getAllParentLocation(0);
	
	 subLocations = adStore.getDistinctLocations();
	 subLocations.forEach(loction->System.out.println("location"+loction));
		
	}

	public List<Location> getLocations() {
		return locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public Set<String> getSubLocations() {
		return subLocations;
	}

	public void setSubLocations(Set<String> subLocations) {
		this.subLocations = subLocations;
	}
	
}
