package com.admin4u.views;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.entity.PolicyDoc;
import com.admin4u.service.bussiness.contract.LocationManger;

@Named("dashboard")
@RequestScoped

public class DashboardBean {

	 @Inject
	 private LocationManger locationManger;
	 
	 @Inject
	 private UserSessionBean userSessionBean;
	 
	 private List<PolicyDoc> policyDocs;	
	
	public DashboardBean() {
	}
	
	
	@PostConstruct
    private void init()
    {
		policyDocs=locationManger.getPoliciesByLoc(userSessionBean.getLocationName());
    }


	public List<PolicyDoc> getPolicyDocs() {
		return policyDocs;
	}


	public void setPolicyDocs(List<PolicyDoc> policyDocs) {
		this.policyDocs = policyDocs;
	}
	
}
