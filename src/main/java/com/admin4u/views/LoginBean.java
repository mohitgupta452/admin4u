package com.admin4u.views;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.util.service.ApplicationMetaData;

@RequestScoped
@Named("login")
public class LoginBean implements Serializable{

@Inject
private LocationManger locationManger;
	
private static final long serialVersionUID = 1L;

private String userid;

private String googleAuthURL;


@PostConstruct
public void initForm()
{
	googleAuthURL=ApplicationMetaData.Google.OauthURL;
	
}



public String getUserid() {
	return userid;
}
public void setUserid(String userid) {
	this.userid = userid;
}
public String getGoogleAuthURL() {
	return googleAuthURL;
}
public void setGoogleAuthURL(String googleAuthURL) {
	this.googleAuthURL = googleAuthURL;
}

}
