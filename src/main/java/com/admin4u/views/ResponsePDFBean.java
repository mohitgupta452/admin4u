package com.admin4u.views;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.CMYKColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.turningcloud.utils.FilesUtils;
import com.turningcloud.utils.pdf.PDFCreation;

@Named("dpdf")
@RequestScoped
public class ResponsePDFBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private  Logger logger;
	
	PDFCreation pdfCreation = null;
	public ResponsePDFBean() {
		
	}
	
	public  File generatePdf( Map<String , String> map)
	{
		File file=null;
		Log.debug("response map {}",map);
		if(map.get("serviceList")==null)
			map.put("serviceList", "attachment");
		Logger log = LoggerFactory.getLogger(getClass());
	
		String path = FilesUtils.getPlatformBasedParentDir().getPath()+File.separator+"pdf";
		
		
		try{
			pdfCreation = new PDFCreation(path, map.get("serviceList") +".pdf");
		

	if((map.get("serviceList").equals("TRAINTICKET" )) || (map.get("serviceList").equals("AIRTICKET")))	
			{	
		pdfCreation.addHeader("Ticket Details");
		pdfCreation.addSpace();
		pdfCreation.addData("Departure Date & Time  :   "+map.get("departureDateAndTime") +"       Booking Date  :   "+map.get("bookingDate"));
		pdfCreation.addSpace();
			}
		
	 
		 if( ("CAB".equals(map.get("cabServiceList"))))
	 {
	
        pdfCreation.addHeader("Cab Booking Details");
		pdfCreation.addSpace();
		pdfCreation.addData("Cab Type  :       "+map.get("cabType") +"              Cab No  :         "+map.get("cabNo"));
		pdfCreation.addData("Cab Provider  :   "+map.get("cabProvider") +"          Driver Name  :       "+map.get("driverName"));
		pdfCreation.addData("Driver No  :      "+map.get("driverNo") +"             Pick Up Date And Time  :   "+map.get("pickDateAndTime"));
	   } 
	if(("HOTEL".equals(map.get("hotelServiceList"))))
       {
		
		if(("Guest_House".equals(map.get("guestHouse"))))
	    {
			pdfCreation.addHeader("Accomodation Details");
			pdfCreation.addSpace();
			pdfCreation.addData("Guest House     :       "+map.get("nameAddress ") +"");
			pdfCreation.addData("Occupancy Type  :       "+map.get("typeType") +"              Room No.  :        "+map.get("roomNumber"));
			pdfCreation.addData("Check In        :      "+map.get("checkinDateTime") +"        Check Out  :        "+map.get("checkoutDateTime"));
			pdfCreation.addData("Location        :      "+map.get("location") );

			pdfCreation.addSpace();
			  }else{
		pdfCreation.addHeader("Accomodation Details");
		pdfCreation.addSpace();
		pdfCreation.addData("Location             :       "+map.get("location") +"             Hotel Address                :        "+map.get("hotelBookConfirm"));
		pdfCreation.addData("CheckIn Date & Time  :        "+map.get("checkinDateTime")+"      Check Out Date & Time        :      "+map.get("checkoutDateTime"));
		//pdfCreation.addData("Check Out :      "+map.get("checkoutDateTime") );
		pdfCreation.addSpace();
			  }
		  }
	
	/*if(("GUEST_HOUSE_BOOKING".equals(map.get("serviceList"))))
    {
		pdfCreation.addHeader("Accomodation Details");
		pdfCreation.addSpace();
		pdfCreation.addData("Guest House :       "+map.get("name") +"              Address :        "+map.get("address"));
		pdfCreation.addData("Occupancy Type  :       "+map.get("roomType") +"       Room No.  :        "+map.get("roomNumber"));
		pdfCreation.addData("Check In  :      "+map.get("checkIn") +"              Check Out  :        "+map.get("checkOut"));
		pdfCreation.addSpace();
		  }*/
		}
		catch( Exception exception)
		{
			exception.printStackTrace();
		}
	
        file = pdfCreation.closePDF();
 
		return file;
	}
		
		
}	
		
	