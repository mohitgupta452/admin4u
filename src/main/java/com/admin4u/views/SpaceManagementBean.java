package com.admin4u.views;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.dto.SpaceManagementManager;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.persistence.entity.SMFloor;
import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.comprator.RequestComparator;
import com.admin4u.util.enums.SMSpaceStationType;
import com.admin4u.util.enums.Services;

@Named("spaceManagement")
@RequestScoped
public class SpaceManagementBean implements Serializable{

	
	private static final long serialVersionUID = -3485697228276626320L;

	@Inject 
	private DropDownManager dropDownManager;

	@Inject 
	private RequestResponseManager requestResponseManager;
	
	@Inject
	private UserSessionBean userSession;
	
	@Inject
	private SpaceManagementManager smManager;
	
	@Inject
	private LocationManger locationManager;
	
	
		private  List<Map<String,Object>> USERsmDataList;
	
	private List<String> departmentDropDown;
	
	private List<String> locationDropDown;
	
	//memebers for test.jsp page
	
	private Map<String,String> buildingFloor;
	
	//end 
	
	private List<SMSpaceStationType> spaceCategoryList;
	
	public SpaceManagementBean() {
	}
	@PostConstruct
	public void init()
	{
		USERsmDataList=requestResponseManager.getRequest(userSession.getUser().getUserPrincipalName(),Services.SPACEMANAGEMENT.toString());
		Collections.sort(USERsmDataList,new RequestComparator().reversed());

		departmentDropDown=dropDownManager.getDropDownValuesListByName("department");
		locationDropDown=dropDownManager.getAllLocationName();
		
		buildingFloor=new HashMap<String, String>();
		for(SMBuilding building:smManager.getBuildingsByLocation(locationManager.getLocationIdByName(userSession.getLocationName()))){
			for(SMFloor floor:smManager.getFloorsByBuilding(building.getId())){
				buildingFloor.put(floor.getId()+"", building.getBuildingName()+"-"+floor.getFloorCode());
			}
		}
		
		spaceCategoryList=Arrays.asList(SMSpaceStationType.values());

	}
	
	public List<Map<String, Object>> getUSERsmDataList()
	{
		return USERsmDataList;
	}
	public void setUSERsmDataList(List<Map<String, Object>> uSERsmDataList)
	{
		USERsmDataList = uSERsmDataList;
	}
	public List<String> getDepartmentDropDown()
	{
		return departmentDropDown;
	}
	public void setDepartmentDropDown(List<String> departmentDropDown)
	{
		this.departmentDropDown = departmentDropDown;
	}
	public List<String> getLocationDropDown()
	{
		return locationDropDown;
	}
	public void setLocationDropDown(List<String> locationDropDown)
	{
		this.locationDropDown = locationDropDown;
	}
	public Map<String,String> getBuildingFloor()
	{
		return buildingFloor;
	}
	public void setBuildingFloor(Map<String,String> buildingFloor)
	{
		this.buildingFloor = buildingFloor;
	}
	public List<SMSpaceStationType> getSpaceCategoryList()
	{
		return spaceCategoryList;
	}
	public void setSpaceCategoryList(List<SMSpaceStationType> spaceCategoryList)
	{
		this.spaceCategoryList = spaceCategoryList;
	}

	
	
	
	
	
	
	
	
	

}