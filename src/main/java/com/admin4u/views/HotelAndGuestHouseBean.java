package com.admin4u.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mortbay.log.Log;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.comprator.RequestComparator;
import com.admin4u.util.enums.Services;


@Named("hng")
@RequestScoped
public class HotelAndGuestHouseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	
	
	@Inject 
	private RequestResponseManager reqResManager;
	
	@Inject
	private UserSessionBean userSession;
	

	private List<Map<String,Object>> HODhngDataList;
	
	private List<Map<String,Object>> ADMINhngDataList;
	
	private  List<Map<String,Object>> USERhngDataList;


	public HotelAndGuestHouseBean() {
	}
	
	@PostConstruct
	public void init()
	{
		HODhngDataList=new ArrayList();
		ADMINhngDataList=new ArrayList();
		USERhngDataList=new ArrayList<>();//reqResManager.getRequest(userSession.getUser().getUserPrincipalName(),Services.HOTEL_BOOKING.toString());
		//USERhngDataList.addAll(reqResManager.getRequest(userSession.getUser().getUserPrincipalName(),Services.GUEST_HOUSE_BOOKING.toString()));
		//Collections.sort(USERhngDataList,new RequestComparator().reversed());
	    
	}
	
	
	

	
	
	public List<Map<String, Object>> getHODhngDataList() {
		return HODhngDataList;
	}

	public void setHODhngDataList(List<Map<String, Object>> hODhngDataList) {
		HODhngDataList = hODhngDataList;
	}

	public List<Map<String, Object>> getADMINhngDataList() {
		return ADMINhngDataList;
	}

	public void setADMINhngDataList(List<Map<String, Object>> aDMINhngDataList) {
		ADMINhngDataList = aDMINhngDataList;
	}

	public List<Map<String, Object>> getUSERhngDataList() {
		return USERhngDataList;
	}

	public void setUSERhngDataList(List<Map<String, Object>> uSERhngDataList) {
		USERhngDataList = uSERhngDataList;
	}

	
	
	

}
