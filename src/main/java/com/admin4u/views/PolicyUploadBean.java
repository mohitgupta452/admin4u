package com.admin4u.views;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.Location;
import com.admin4u.persistence.entity.PolicyDoc;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Roles;

@Named("policyUp")
@RequestScoped
public class PolicyUploadBean {

	private List<String> locations;

	@Inject
	private LocationManger locationManger;

	@Inject
	private UserSessionBean userSessionBean;

	@Inject
	private RoleManager roleManager;
	
	private List<PolicyDoc> policyDocs;

	public PolicyUploadBean() {
	}

	@PostConstruct
	private void init() {
		
		policyDocs = new ArrayList<>();
		
		locations = new ArrayList<>();

		List<EmployeeRole> employeeRoles = roleManager
				.getEmpByUpnAndRole(userSessionBean.getEmpData().getUserPrincipalName(), Roles.ADMIN);

		employeeRoles.forEach(employeeRole -> {
			this.locations.add(employeeRole.getLocation());
			policyDocs.addAll(locationManger.getPoliciesByLoc(employeeRole.getLocation()));
			
		});
		
		

	}

	public List<String> getLocations() {
		return locations;
	}

	public void setLocations(List<String> locations) {
		this.locations = locations;
	}

	public List<PolicyDoc> getPolicyDocs() {
		return policyDocs;
	}

	public void setPolicyDocs(List<PolicyDoc> policyDocs) {
		this.policyDocs = policyDocs;
	}
	
	

}
