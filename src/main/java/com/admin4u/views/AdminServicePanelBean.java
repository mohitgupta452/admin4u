package com.admin4u.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.dto.SpaceManagementManager;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.LocServiceMap;
import com.admin4u.persistence.entity.NonApprovalDesignation;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.service.bussiness.contract.ServiceManager;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.SMSpaceStationType;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData;
@Named("ASPBean")
@RequestScoped
public class AdminServicePanelBean implements Serializable{

	
	private static final long serialVersionUID = 1L;

	@Inject
	private Logger _LOGGER; 
	
	@Inject 
	private LocationManger locationManager;
	
	@Inject
	private  UserSessionBean userSession;
	
	@Inject
	private ServiceManager serviceManager;
	
	@Inject
	private SpaceManagementManager smManager;
	
	@Inject
	private GuestHouseManager ghManager;
	
	@Inject
	private RoleManager roleManager;
	
	private List<LocServiceMap> locationServices;
	
	private Map<String,Byte> locationServicesMap;
	
	private List<String> designationList;
	
	private List<String> getNonApprovalDesignation;
	
	private List<SMSpaceStationType> spaceCategoryList;
	
	private List<SMBuilding> buildingList;
	
	private List<Map<String,String>> ghinventoryList;

	public AdminServicePanelBean() {
	}
	
	@PostConstruct
	public void init()
	{
		locationServicesMap=new HashMap<String,Byte>();
		locationServices=locationManager.getLocServiceMapByLocation(userSession.getLocationName());
		for(LocServiceMap lsmObj:locationServices){
		locationServicesMap.put(lsmObj.getService(), lsmObj.getIsActive());
		designationList = serviceManager.getAllDesignation();
		
		getNonApprovalDesignation = serviceManager.getNonApprovalDesignationByActive();
		setSpaceCategoryList(Arrays.asList(SMSpaceStationType.values()));
		buildingList=smManager.getBuildingsByLocation(locationManager.getLocationIdByName(userSession.getLocationName()));
		List<EmployeeRole> employeeRoleList=roleManager.getEmpByUpnAndRole(userSession.getEmpData().getUserPrincipalName(),Roles.ADMIN);
		
		int locationId=locationManager.getLocationIdByName(employeeRoleList.get(0).getLocation());
		ghinventoryList = ghManager.getGuestHouseRoomCounts(locationId);
		_LOGGER.debug("guest house count {}",ghinventoryList.size());
		}
	}

	
	


	public List<Map<String, String>> getGhinventoryList() {
		return ghinventoryList;
	}

	public void setGhinventoryList(List<Map<String, String>> ghinventoryList) {
		this.ghinventoryList = ghinventoryList;
	}

	public List<String> getGetNonApprovalDesignation() {
		return getNonApprovalDesignation;
	}

	public void setGetNonApprovalDesignation(List<String> getNonApprovalDesignation) {
		this.getNonApprovalDesignation = getNonApprovalDesignation;
	}

	

	public List<SMSpaceStationType> getSpaceCategoryList()
	{
		return spaceCategoryList;
	}

	public void setSpaceCategoryList(List<SMSpaceStationType> spaceCategoryList)
	{
		this.spaceCategoryList = spaceCategoryList;
	}

	public List<SMBuilding> getBuildingList()
	{
		return buildingList;
	}

	public void setBuildingList(List<SMBuilding> buildingList)
	{
		this.buildingList = buildingList;
	}

	public List<LocServiceMap> getLocationServices() {
		return locationServices;
	}

	public void setLocationServices(List<LocServiceMap> locationServices) {
		this.locationServices = locationServices;
	}

	public Map<String,Byte> getLocationServicesMap() {
		return locationServicesMap;
	}

	public void setLocationServicesMap(Map<String,Byte> locationServicesMap) {
		this.locationServicesMap = locationServicesMap;
	}

	public List<String> getDesignationList() {
		return designationList;
	}

	public void setDesignationList(List<String> designationList) {
		this.designationList = designationList;
	}
	
	}

