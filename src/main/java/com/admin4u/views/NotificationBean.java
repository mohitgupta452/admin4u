package com.admin4u.views;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.entity.NotificationMaster;
import com.admin4u.service.bussiness.contract.NotificationManager;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.service.ApplicationMetaData.Status;

@Named("notification")
@SessionScoped
public class NotificationBean implements Serializable {

	private static final long serialVersionUID = -7328316678619091030L;

	private List<NotificationMaster> notificationList;

	private List<NotificationMaster> allNotificationList;

	private int notificationCount;

	@Inject
	private NotificationManager notifiactionManager;

	@Inject
	NotificationManager notificationManager;

	@Inject
	private UserSessionBean userSession;

	private Map<String, Map<String, String>> notificationViewMap;

	public NotificationBean() {
	}

	@PostConstruct
	public void init() {
		notificationViewMap = new HashMap<>();

		notificationList = notifiactionManager
				.getLimitedNotificationByUPN(userSession.getEmpData().getUserPrincipalName(), 6);

		notificationList.forEach(notificationMaster -> {

			Map<String, String> notificationMap = new HashMap<>();

			notificationMap.put("id", String.valueOf(notificationMaster.getNotificationId()));
			notificationMap.put("msg", notificationMaster.getMessage());
			notificationMap.put("time", this.timeAgo(notificationMaster.getCreationTime()));
			notificationMap.put("isRead", String.valueOf(notificationMaster.isRead()));
			String redirection = "";

			switch (notificationMaster.getRecieverRole() != null ? Roles.valueOf(notificationMaster.getRecieverRole())
					: Roles.REQUESTER) {

			case ADMIN:
				redirection = "user/admintable.jsp";
				break;

			case SPOC:
				redirection = "user/spoc.jsp";
				break;
			case REQUESTER:
				redirection = "user/user/user_task.jsp";
				break;

			case APPROVAL_AUTHORITY:
				redirection = "user/approvalauthority.jsp";
				break;

			default:
				break;
			}
			notificationMap.put("redirection",
					redirection + "?search=" + String.valueOf(notificationMaster.getRequestMaster().getRequestId()));
			notificationViewMap.put(String.valueOf(notificationMaster.getNotificationId()), notificationMap);
		});

		allNotificationList = notificationManager.getNotificationByUPN(userSession.getEmpData().getUserPrincipalName(),
				null);

		notificationCount = allNotificationList.size();

	}

	public String timeAgo(Date notificationCreationDate) {
		long duration = Calendar.getInstance().getTime().getTime() - notificationCreationDate.getTime();
		int day = (int) TimeUnit.MILLISECONDS.toDays(duration);
		int hours = (int) TimeUnit.MILLISECONDS.toHours(duration);
		if (day < 1) {
			if (hours < 1)
				return TimeUnit.MILLISECONDS.toMinutes(duration) + " minutes ago";
			return hours + " hours ago";
		} else if (day > 30) {
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			return "on " + format.format(notificationCreationDate);
		}

		return day + " days ago";
	}

	public List<NotificationMaster> getAllNotificationList() {
		return allNotificationList;
	}

	public void setAllNotificationList(List<NotificationMaster> allNotificationList) {
		this.allNotificationList = allNotificationList;
	}

	public int getNotificationCount() {
		return notificationCount;
	}

	public void setNotificationCount(int notificationCount) {
		this.notificationCount = notificationCount;
	}

	public List<NotificationMaster> getNotificationList() {
		return notificationList;
	}

	public void setNotificationList(List<NotificationMaster> notificationList) {
		this.notificationList = notificationList;
	}

	public Map<String, Map<String, String>> getNotificationViewMap() {
		return notificationViewMap;
	}

	public void setNotificationViewMap(Map<String, Map<String, String>> notificationViewMap) {
		this.notificationViewMap = notificationViewMap;
	}

}
