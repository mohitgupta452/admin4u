package com.admin4u.views;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mortbay.log.Log;

import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.service.ApplicationMetaData;

@Named("roleBaseDashboardBean")
@RequestScoped
public class RoleBaseDashboardBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject 
	UserSessionBean userSessionBean;
	
	@Inject
	private ADStore adStore;
	
	List<String> roleList=new ArrayList<String>();
	
	public RoleBaseDashboardBean() {
	}

	private Map<String,String> componentsToDisplayMap;
	@PostConstruct
	public void init()
	{
		
		componentsToDisplayMap=new HashMap<String,String>();
		 roleList=userSessionBean.getEmpRole();
			
		   if(adStore.isManager(userSessionBean.getEmpData().getUserPrincipalName()))
		 {
				componentsToDisplayMap.put("Approval Authority Table","user/approvalauthority.jsp");
				
			}
		 for(String role:roleList){

		if("Admin".equalsIgnoreCase(role))
		{
			componentsToDisplayMap.put(" User Request Table","user/admintable.jsp");
			componentsToDisplayMap.put("Admin Panel","user/admin_panel.jsp");
			componentsToDisplayMap.put("Role Management","user/role_management.jsp");
		  //componentsToDisplayMap.put("Drop Down Manager","user/manager_dropdown.jsp");
		}
		 if("Spoc".equalsIgnoreCase(role))
		{

			componentsToDisplayMap.put(" User Request Table","user/spoc.jsp");
			
		}
		 
		 if(Roles.APPLICATION_ADMIN.toString().equalsIgnoreCase(role))
			{
				componentsToDisplayMap.put("Drop Down Manager","user/manager_dropdown.jsp");
				componentsToDisplayMap.put("Role Management","user/role_management.jsp");
				componentsToDisplayMap.put("Cost-Dept Mapping","user/cost_center_mapping.jsp");
				componentsToDisplayMap.put("Priority Settings","user/priorityTatMap.jsp");


			}
		
		
		 }
				
	}
	public Map<String,String> getComponentsToDisplayMap() {
		return componentsToDisplayMap;
	}
	public void setComponentsToDisplayMap(Map<String,String> componentsToDisplayMap) {
		this.componentsToDisplayMap = componentsToDisplayMap;
	}
	
}
