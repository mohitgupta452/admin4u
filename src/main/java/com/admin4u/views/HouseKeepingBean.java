package com.admin4u.views;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mortbay.jetty.Request;
import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.comprator.RequestComparator;
import com.admin4u.util.enums.Services;

@Named("hkb")
@RequestScoped
public class HouseKeepingBean implements Serializable {
	/**
	 * 
	 */
	private Logger log= LoggerFactory.getLogger(getClass());
	
	private static final long serialVersionUID = 1L;
	

	
	@Inject 
	private RequestResponseManager requestResponseManager;
	
	@Inject
	private UserSessionBean userSession;
	
	public HouseKeepingBean() {
	}
	private List<Map<String,Object>> HODhngDataList;
	
	private List<Map<String,Object>> ADMINhngDataList;
	
	private  List<Map<String,Object>> USERhngDataList;
	
	
	
	@PostConstruct
	public void init()
	{
		USERhngDataList=new ArrayList<>();//requestResponseManager.getRequest(userSession.getUser().getUserPrincipalName(),Services.HOUSEKEEPING.toString());
		//Collections.sort(USERhngDataList,new RequestComparator().reversed());

	}
	public List<Map<String, Object>> getHODhngDataList() {
		return HODhngDataList;
	}

	public void setHODhngDataList(List<Map<String, Object>> hODhngDataList) {
		HODhngDataList = hODhngDataList;
	}

	public List<Map<String, Object>> getADMINhngDataList() {
		return ADMINhngDataList;
	}

	public void setADMINhngDataList(List<Map<String, Object>> aDMINhngDataList) {
		ADMINhngDataList = aDMINhngDataList;
	}

	public List<Map<String, Object>> getUSERhngDataList() {
		return USERhngDataList;
	}

	public void setUSERhngDataList(List<Map<String, Object>> uSERhngDataList) {
		USERhngDataList = uSERhngDataList;
	}


	
	
	
	
	
	
}
