package com.admin4u.views;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.entity.RequestMaster;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.service.ProcessListener;
import com.admin4u.util.service.ProcessStates;

@Named("userRequest")
@RequestScoped
public class UserReqBean implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private RequestResponseManager requestResponseManager;
	
	@Inject
	private UserSessionBean sessionBean;
	
	private List<Map<String,Object>> requestMasters;
	
	private List<String> approvalProcessList;
	
	private List<String> nonApprovalProcessList; 

	public UserReqBean() {
	}
	
	@PostConstruct
	private void initForm()
	{
		ProcessListener processListener = new ProcessStates();
		requestMasters=requestResponseManager.getaAllReqByUser(sessionBean.getEmpData().getUserPrincipalName());
		approvalProcessList=processListener.getApprovalProcess();
		nonApprovalProcessList=processListener.getNonApprovalProcess();
	}

	public List<Map<String, Object>> getRequestMasters() {
		return requestMasters;
	}

	public void setRequestMasters(List<Map<String, Object>> requestMasters) {
		this.requestMasters = requestMasters;
	}

	public List<String> getApprovalProcessList() {
		return approvalProcessList;
	}

	public void setApprovalProcessList(List<String> approvalProcessList) {
		this.approvalProcessList = approvalProcessList;
	}

	public List<String> getNonApprovalProcessList() {
		return nonApprovalProcessList;
	}

	public void setNonApprovalProcessList(List<String> nonApprovalProcessList) {
		this.nonApprovalProcessList = nonApprovalProcessList;
	}
	
	


	
}
