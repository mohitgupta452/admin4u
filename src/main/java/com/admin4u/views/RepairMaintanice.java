package com.admin4u.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.comprator.RequestComparator;
import com.admin4u.util.enums.Services;

@Named("rpm")
@RequestScoped
public class RepairMaintanice{

	@Inject 
	private DropDownManager dropDownManager;

	@Inject 
	private RequestResponseManager requestResponseManager;
	
	@Inject
	private UserSessionBean userSession;
	
	private List<Map<String,Object>> HODrpmDataList;
	
	private List<Map<String,Object>> ADMINrpmDataList;
	
	private  List<Map<String,Object>> USERrpmDataList;
	

	
	
	public RepairMaintanice() {
	}
	@PostConstruct
	public void init()
	{
		USERrpmDataList=new ArrayList();//requestResponseManager.getRequest(userSession.getUser().getUserPrincipalName(),Services.REPAIRMAINTENANCE.toString());
		//Collections.sort(USERrpmDataList,new RequestComparator().reversed());

		

	}
	
	
	public List<Map<String, Object>> getHODrpmDataList() {
		return HODrpmDataList;
	}

	public void setHODrpmDataList(List<Map<String, Object>> hODrpmDataList) {
		HODrpmDataList = hODrpmDataList;
	}

	public List<Map<String, Object>> getADMINrpmDataList() {
		return ADMINrpmDataList;
	}

	public void setADMINrpmDataList(List<Map<String, Object>> aDMINrpmDataList) {
		ADMINrpmDataList = aDMINrpmDataList;
	}

	public List<Map<String, Object>> getUSERrpmDataList() {
		return USERrpmDataList;
	}

	public void setUSERrpmDataList(List<Map<String, Object>> uSERrpmDataList) {
		USERrpmDataList = uSERrpmDataList;
	}
	
	
	
	
	
	

}