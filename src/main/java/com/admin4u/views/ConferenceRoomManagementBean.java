package com.admin4u.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.dto.SpaceManagementManager;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.persistence.entity.SMFloor;
import com.admin4u.persistence.entity.SMRoom;
import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.comprator.RequestComparator;
import com.admin4u.util.enums.SMSpaceStationType;
import com.admin4u.util.enums.Services;

@Named("crmBean")
@RequestScoped
public class ConferenceRoomManagementBean implements Serializable{

	
	private static final long serialVersionUID = -3485697228276626320L;

	@Inject 
	private DropDownManager dropDownManager;

	@Inject 
	private RequestResponseManager requestResponseManager;
	
	@Inject
	private UserSessionBean userSession;
	
	@Inject
	private SpaceManagementManager smManager;
	
	@Inject
	private LocationManger locationManager;
	
	private  List<Map<String,Object>> USERcrmDataList;
	
	private List<String> departmentDropDown;
	
	private List<String> locationDropDown;
	
	//memebers for test.jsp page
	
	private Map<String,String> buildingFloor;
	
	private List<SMRoom>conferenceRoomList;
	
	//end 
	
	
	public ConferenceRoomManagementBean() {
	}
	public List<Map<String, Object>> getUSERcrmDataList()
	{			
		return USERcrmDataList;
	}
	public void setUSERcrmDataList(List<Map<String, Object>> uSERcrmDataList)
	{
		USERcrmDataList = uSERcrmDataList;
	}
	@PostConstruct
	public void init()
	{
		USERcrmDataList=requestResponseManager.getRequest(userSession.getUser().getUserPrincipalName(),Services.MEETINGROOMMANAGEMENT.toString());
		Collections.sort(USERcrmDataList,new RequestComparator().reversed());
		conferenceRoomList=new ArrayList<>();
		/*departmentDropDown=dropDownManager.getDropDownValuesListByName("department");
		locationDropDown=dropDownManager.getAllLocationName();
		*/
		//conferenceRoomList=smManager.getRoomsByType(1, SMSpaceStationType.MEETING_ROOM.toString());
		
		//buildingFloor=new HashMap<String, String>();
		for(SMBuilding building:smManager.getBuildingsByLocation(locationManager.getLocationIdByName(userSession.getLocationName()))){
			/*for(SMFloor floor:smManager.getFloorsByBuilding(building.getId())){
				buildingFloor.put(floor.getId()+"", building.getBuildingName()+"-"+floor.getFloorCode());
				
			}*/
			conferenceRoomList.addAll(smManager.getRoomsByType(building.getId(), SMSpaceStationType.MEETING_ROOM.toString()));
		}

	}
	
	
	public List<String> getDepartmentDropDown()
	{
		return departmentDropDown;
	}
	public void setDepartmentDropDown(List<String> departmentDropDown)
	{
		this.departmentDropDown = departmentDropDown;
	}
	public List<String> getLocationDropDown()
	{
		return locationDropDown;
	}
	public void setLocationDropDown(List<String> locationDropDown)
	{
		this.locationDropDown = locationDropDown;
	}
	public Map<String,String> getBuildingFloor()
	{
		return buildingFloor;
	}
	public void setBuildingFloor(Map<String,String> buildingFloor)
	{
		this.buildingFloor = buildingFloor;
	}
	public List<SMRoom> getConferenceRoomList()
	{
		return conferenceRoomList;
	}
	public void setConferenceRoomList(List<SMRoom> conferenceRoomList)
	{
		this.conferenceRoomList = conferenceRoomList;
	}
	public DropDownManager getDropDownManager()
	{
		return dropDownManager;
	}
	public void setDropDownManager(DropDownManager dropDownManager)
	{
		this.dropDownManager = dropDownManager;
	}

	
	
	
	
	
	
	
	
	

}