package com.admin4u.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.comprator.RequestComparator;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData;
@Named("tat")
@RequestScoped
public class TourAndTravelBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

	@Inject 
	private RequestResponseManager requestResponseManager;
	
	@Inject
	private UserSessionBean userSession;
	
	
	
	@PostConstruct
	public void init()
	{
		USERtatDataList=new ArrayList<>();// requestResponseManager.getRequest(userSession.getEmpData().getUserPrincipalName(),Services.AIRTICKET.toString());
		USERtatcabDataList=new ArrayList<>();//requestResponseManager.getRequest(userSession.getEmpData().getUserPrincipalName(),Services.CABPICK.toString());
		//USERtatcabDataList.addAll(requestResponseManager.getRequest(userSession.getEmpData().getUserPrincipalName(),Services.CABDROP.toString()));
		//USERtatcabDataList.addAll(requestResponseManager.getRequest(userSession.getEmpData().getUserPrincipalName(),Services.CABDISPOSAL.toString()));
		USERtattrainDataList=new ArrayList<>();//= requestResponseManager.getRequest(userSession.getEmpData().getUserPrincipalName(),Services.TRAINTICKET.toString());
		//USERtatcabDataList.addAll(requestResponseManager.getRequest(userSession.getEmpData().getUserPrincipalName(),Services.CABAIRPORT.toString()));
		
		//Collections.sort(USERtatDataList,new RequestComparator().reversed());
		//Collections.sort(USERtatcabDataList,new RequestComparator().reversed());
		//Collections.sort(USERtattrainDataList,new RequestComparator().reversed());
		HODtatDataList= new ArrayList<>();
		ADMINtatDataList= new ArrayList<>();
		

	}

	private List<Map<String,Object>> HODtatDataList;
	
	private List<Map<String,Object>> ADMINtatDataList;
	
	private  List<Map<String,Object>> USERtatDataList;
	
	private  List<Map<String,Object>> USERtatcabDataList;
	
	private  List<Map<String,Object>> USERtattrainDataList;
	
	
	

	
	public List<Map<String, Object>> getUSERtattrainDataList() {
		return USERtattrainDataList;
	}

	public void setUSERtattrainDataList(List<Map<String, Object>> uSERtattrainDataList) {
		USERtattrainDataList = uSERtattrainDataList;
	}

	public TourAndTravelBean() {
	}
	
	public List<Map<String, Object>> getHODtatDataList() {
		return HODtatDataList;
	}

	public void setHODtatDataList(List<Map<String, Object>> hODtatDataList) {
		HODtatDataList = hODtatDataList;
	}

	public List<Map<String, Object>> getADMINtatDataList() {
		return ADMINtatDataList;
	}

	public void setADMINtatDataList(List<Map<String, Object>> aDMINtatDataList) {
		ADMINtatDataList = aDMINtatDataList;
	}

	public List<Map<String, Object>> getUSERtatDataList() {
		return USERtatDataList;
	}

	public void setUSERtatDataList(List<Map<String, Object>> uSERtatDataList) {
		USERtatDataList = uSERtatDataList;
	}
	public List<Map<String, Object>> getUSERtatcabDataList() {
		return USERtatcabDataList;
	}

	public void setUSERtatcabDataList(List<Map<String, Object>> uSERtatcabDataList) {
		USERtatcabDataList = uSERtatcabDataList;
	}

	}