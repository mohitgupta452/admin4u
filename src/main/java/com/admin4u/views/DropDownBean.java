package com.admin4u.views;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.VendorManager;
import com.admin4u.util.enums.ApplicationDropDowns;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

@Named("dropDown")
@ApplicationScoped
public class DropDownBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private DropDownManager dropDownManager;

	@Inject
	private UserSessionBean userSession;

	@Inject
	private VendorManager vendorManager;

	public DropDownBean() {

	}
	// tour and travel drop downs value list starts

	private List<String> bookingTypeDropDown;

	private List<String> bookingForDropDown;

	private List<String> guestTypeDropDown;

	private List<String> hotelLocationDropDown;

	private List<String> guestHouseLocationDropDown;

	private List<String> availCabDropDown;

	private List<String> costCentreDropDown;

	private List<String> prefTimeSlotDropDown;

	private List<String> sourceDropDown;

	private List<String> destinationDropDown;

	private List<String> companyDropDown;

	private List<String> travelClassDropDown;

	private List<String> cabPreferenceDropDown;

	private List<String> startPointDropDown;

	private List<String> endPointDropDown;

	private List<String> airportDropDown;

	private List<String> workLocationDropDown;

	private List<String> hkNatureOfWork;

	private JsonObject repairNatureOfWork;

	// tour and travel value dropdown list ends

	// hotel guest house dropdown list starts
	private String costcenter;
	private String department;
	private String companyName;
	private List<EmployeeData> getCompanyNameList;
	private String locationName;
	private List<String> locationNameList;

	// hotel guest house dropdown list ends

	// house keeping dropdown list starts
	private List<String> departmentDropDown;
	private List<String> natureOfWorkDropDown;
	// house keeping dropdown list ends

	// admin table dropdown starts

	private List<String> allVendorName;
	// admin table dropdown starts

	@PostConstruct
	public void init()
	{
		// tour and travel dropdowns starts
		bookingTypeDropDown=dropDownManager.getDropDownValuesListByName("booking_type");
		bookingForDropDown=dropDownManager.getDropDownValuesListByName("booking_for");
		guestTypeDropDown=dropDownManager.getDropDownValuesListByName("guest_type");
		hotelLocationDropDown=dropDownManager.getDropDownValuesListByName("hotel_location");
		availCabDropDown=dropDownManager.getDropDownValuesListByName("avail_cab");
		costCentreDropDown=dropDownManager.getDropDownValuesListByName("cost_centre");
		prefTimeSlotDropDown=dropDownManager.getDropDownValuesListByName("preferred_time_slot");
		sourceDropDown=dropDownManager.getDropDownValuesListByName("source");
		destinationDropDown=dropDownManager.getDropDownValuesListByName("destination");
		companyDropDown=dropDownManager.getDropDownValuesListByName("company_name");
		travelClassDropDown=dropDownManager.getDropDownValuesListByName("travel_class");
		cabPreferenceDropDown=dropDownManager.getDropDownValuesListByName("cab_preference");
		startPointDropDown=dropDownManager.getDropDownValuesListByName("start_point");
		endPointDropDown=dropDownManager.getDropDownValuesListByName("end_point");
		airportDropDown=dropDownManager.getDropDownValuesListByName("airport_list");
		// tour and travel dropdowns starts
		
		
		// hotel guest house dropdown starts
	
		/* --------  already done in tour and travel   ---------- */
		
		//bookingTypeDropDown=dropDownManager.getDropDownValuesListByName("booking_type");
		//bookingForDropDown=dropDownManager.getDropDownValuesListByName("booking_for");
		//guestTypeDropDown=dropDownManager.getDropDownValuesListByName("guest_type");
		//hotelLocationDropDown=dropDownManager.getDropDownValuesListByName("hotel_location");
		//availCabDropDown=dropDownManager.getDropDownValuesListByName("avail_cab");
		//costCentreDropDown=dropDownManager.getDropDownValuesListByName("cost_centre");
		String upn = userSession.getEmpData().getUserPrincipalName();
	    costcenter = dropDownManager.getCostCenter(upn);
	    department = dropDownManager.getDepartment(upn);
	    companyName = dropDownManager.getCompanyName(upn);
	    getCompanyNameList = dropDownManager.getAllCompanyName();
	    locationName = dropDownManager.getLocation(upn);
	    guestHouseLocationDropDown=dropDownManager.getDropDownValuesListByName("guest_house_location");
	    
		//hotel guest house dropdown ends
	    
	    // house keeping dropdown  starts
	    
	    
	    departmentDropDown=dropDownManager.getDropDownValuesListByName("department");
		natureOfWorkDropDown=dropDownManager.getDropDownValuesListByName("nature_of_work");
		workLocationDropDown = dropDownManager.getDropDownValuesListByName("work_location");
		
		hkNatureOfWork=dropDownManager.getDropDownValuesListByName(ApplicationDropDowns.hk_natureofwork.toString());
		
		repairNatureOfWork = new JsonObject();
		
		repairNatureOfWork.add("ELECTRICAL", (JsonArray)new Gson().toJsonTree(dropDownManager.getDropDownValuesListByName("electrical"),new TypeToken<List<String>>(){}.getType()));
		repairNatureOfWork.add("PLUMBING",(JsonArray)new Gson().toJsonTree(dropDownManager.getDropDownValuesListByName(ApplicationDropDowns.plumbing.toString()),new TypeToken<List<String>>(){}.getType()));
		repairNatureOfWork.add("CIVIL",(JsonArray)new Gson().toJsonTree(dropDownManager.getDropDownValuesListByName(ApplicationDropDowns.civil.toString()),new TypeToken<List<String>>(){}.getType()));
		repairNatureOfWork.add("CARPENTERY",(JsonArray)new Gson().toJsonTree(dropDownManager.getDropDownValuesListByName(ApplicationDropDowns.carpentery.toString()),new TypeToken<List<String>>(){}.getType()));
		repairNatureOfWork.add("HVCA",(JsonArray)new Gson().toJsonTree(dropDownManager.getDropDownValuesListByName(ApplicationDropDowns.hvca.toString()),new TypeToken<List<String>>(){}.getType()));
//	    
	    // house keeping dropdown ends
		
		//repairmaintenance dropdown starts
		
		/* -------- already in house kepping --------- */
		
		//departmentDropDown=dropDownManager.getDropDownValuesListByName("department");
		//natureOfWorkDropDown=dropDownManager.getDropDownValuesListByName("nature_of_work");
		
		//repairmaintenance dropdown ends 
		
		allVendorName = vendorManager.allVendorName();
	}

	// setter and getter starts
	public List<String> getBookingTypeDropDown() {
		return bookingTypeDropDown;
	}

	public void setBookingTypeDropDown(List<String> bookingTypeDropDown) {
		this.bookingTypeDropDown = bookingTypeDropDown;
	}

	public List<String> getBookingForDropDown() {
		return bookingForDropDown;
	}

	public void setBookingForDropDown(List<String> bookingForDropDown) {
		this.bookingForDropDown = bookingForDropDown;
	}

	public List<String> getGuestTypeDropDown() {
		return guestTypeDropDown;
	}

	public void setGuestTypeDropDown(List<String> guestTypeDropDown) {
		this.guestTypeDropDown = guestTypeDropDown;
	}

	public List<String> getHotelLocationDropDown() {
		return hotelLocationDropDown;
	}

	public void setHotelLocationDropDown(List<String> hotelLocationDropDown) {
		this.hotelLocationDropDown = hotelLocationDropDown;
	}

	public List<String> getAvailCabDropDown() {
		return availCabDropDown;
	}

	public void setAvailCabDropDown(List<String> availCabDropDown) {
		this.availCabDropDown = availCabDropDown;
	}

	public List<String> getCostCentreDropDown() {
		return costCentreDropDown;
	}

	public void setCostCentreDropDown(List<String> costCentreDropDown) {
		this.costCentreDropDown = costCentreDropDown;
	}

	public List<String> getPrefTimeSlotDropDown() {
		return prefTimeSlotDropDown;
	}

	public void setPrefTimeSlotDropDown(List<String> prefTimeSlotDropDown) {
		this.prefTimeSlotDropDown = prefTimeSlotDropDown;
	}

	public List<String> getSourceDropDown() {
		return sourceDropDown;
	}

	public void setSourceDropDown(List<String> sourceDropDown) {
		this.sourceDropDown = sourceDropDown;
	}

	public List<String> getDestinationDropDown() {
		return destinationDropDown;
	}

	public void setDestinationDropDown(List<String> destinationDropDown) {
		this.destinationDropDown = destinationDropDown;
	}

	public List<String> getCompanyDropDown() {
		return companyDropDown;
	}

	public void setCompanyDropDown(List<String> companyDropDown) {
		this.companyDropDown = companyDropDown;
	}

	public List<String> getTravelClassDropDown() {
		return travelClassDropDown;
	}

	public void setTravelClassDropDown(List<String> travelClassDropDown) {
		this.travelClassDropDown = travelClassDropDown;
	}

	public List<String> getCabPreferenceDropDown() {
		return cabPreferenceDropDown;
	}

	public void setCabPreferenceDropDown(List<String> cabPreferenceDropDown) {
		this.cabPreferenceDropDown = cabPreferenceDropDown;
	}

	public List<String> getStartPointDropDown() {
		return startPointDropDown;
	}

	public void setStartPointDropDown(List<String> startPointDropDown) {
		this.startPointDropDown = startPointDropDown;
	}

	public List<String> getEndPointDropDown() {
		return endPointDropDown;
	}

	public void setEndPointDropDown(List<String> endPointDropDown) {
		this.endPointDropDown = endPointDropDown;
	}

	public List<String> getAirportDropDown() {
		return airportDropDown;
	}

	public void setAirportDropDown(List<String> airportDropDown) {
		this.airportDropDown = airportDropDown;
	}

	public String getCostcenter() {
		return costcenter;
	}

	public void setCostcenter(String costcenter) {
		this.costcenter = costcenter;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public List<EmployeeData> getGetCompanyNameList() {
		return getCompanyNameList;
	}

	public void setGetCompanyNameList(List<EmployeeData> getCompanyNameList) {
		this.getCompanyNameList = getCompanyNameList;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public List<String> getLocationNameList() {
		return locationNameList;
	}

	public void setLocationNameList(List<String> locationNameList) {
		this.locationNameList = locationNameList;
	}

	public List<String> getDepartmentDropDown() {
		return departmentDropDown;
	}

	public void setDepartmentDropDown(List<String> departmentDropDown) {
		this.departmentDropDown = departmentDropDown;
	}

	public List<String> getNatureOfWorkDropDown() {
		return natureOfWorkDropDown;
	}

	public void setNatureOfWorkDropDown(List<String> natureOfWorkDropDown) {
		this.natureOfWorkDropDown = natureOfWorkDropDown;
	}

	public List<String> getAllVendorName() {
		return allVendorName;
	}

	public void setAllVendorName(List<String> allVendorName) {
		this.allVendorName = allVendorName;
	}

	public List<String> getGuestHouseLocationDropDown() {
		return guestHouseLocationDropDown;
	}

	public void setGuestHouseLocationDropDown(List<String> guestHouseLocationDropDown) {
		this.guestHouseLocationDropDown = guestHouseLocationDropDown;
	}

	public List<String> getWorkLocationDropDown() {
		return workLocationDropDown;
	}

	public void setWorkLocationDropDown(List<String> workLocationDropDown) {
		this.workLocationDropDown = workLocationDropDown;
	}

	public List<String> getHkNatureOfWork() {
		return hkNatureOfWork;
	}

	public void setHkNatureOfWork(List<String> hkNatureOfWork) {
		this.hkNatureOfWork = hkNatureOfWork;
	}

	public JsonObject getRepairNatureOfWork() {
		return repairNatureOfWork;
	}

	public void setRepairNatureOfWork(JsonObject repairNatureOfWork) {
		this.repairNatureOfWork = repairNatureOfWork;
	}

	// getter and setter ends

}
