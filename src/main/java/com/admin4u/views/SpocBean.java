package com.admin4u.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.persistence.entity.SMBuilding;
import com.admin4u.service.bussiness.contract.GuestHouseManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.comprator.RequestComparator;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.service.ProcessListener;
import com.admin4u.util.service.ProcessStates;

@Named("spoc")
@RequestScoped
public class SpocBean {

	@Inject
	private RequestResponseManager requestResponseManager;
	
	@Inject
	private UserSessionBean userSession;
	
	@Inject
	private GuestHouseManager guestHouseManager;
	
	@Inject
	private LocationManger locationManager;
	
	@Inject
	private RoleManager roleManager;
	
	private List<String> approvalServiceList;
	
	private List<String> nonApprovalServiceList;
	
	private List<Map<String,Object>> spocDataList;
	
	private List<SMBuilding>guestHouseList;
	
	private ProcessListener processListener;
	
	public SpocBean() {
	}
	
	@PostConstruct
	public void init()
	{
		processListener= new ProcessStates();
		
		spocDataList= new ArrayList<>();
		
		String location =null;
		if(userSession.hasRole(Roles.SPOC))
		{
		spocDataList.addAll(requestResponseManager.getSpocRequest(userSession.getEmpData().getUserPrincipalName(),Roles.SPOC));
		List<EmployeeRole> employeeRoleList=roleManager.getEmpByUpnAndRole(userSession.getEmpData().getUserPrincipalName(),Roles.SPOC);

		location=employeeRoleList.get(0).getLocation();
		}

		if(userSession.hasRole(Roles.OTHERSPOC))
		{
		spocDataList.addAll(requestResponseManager.getSpocRequest(userSession.getEmpData().getUserPrincipalName(),Roles.OTHERSPOC));
		if(location == null)
			location= locationManager.getParentLocation(userSession.getEmpData().getL());
		}
		
		Collections.sort(spocDataList,new RequestComparator().reversed());

		approvalServiceList=processListener.getApprovalProcess();
		nonApprovalServiceList=processListener.getNonApprovalProcess();
		
		
		int locationId=locationManager.getLocationIdByName(location);
	
		setGuestHouseList(guestHouseManager.getGuestHouseByLocation(locationId));
	}

	public List<Map<String,Object>> getSpocDataList() {
		return spocDataList;
	}

	public void setSpocDataList(List<Map<String,Object>> spocDataList) {
		this.spocDataList = spocDataList;
	}

	public List<String> getApprovalServiceList() {
		return approvalServiceList;
	}

	public void setApprovalServiceList(List<String> approvalServiceList) {
		this.approvalServiceList = approvalServiceList;
	}

	public List<String> getNonApprovalServiceList() {
		return nonApprovalServiceList;
	}

	public void setNonApprovalServiceList(List<String> nonApprovalServiceList) {
		this.nonApprovalServiceList = nonApprovalServiceList;
	}

	public List<SMBuilding> getGuestHouseList()
	{
		return guestHouseList;
	}

	public void setGuestHouseList(List<SMBuilding> guestHouseList)
	{
		this.guestHouseList = guestHouseList;
	}
	

}
