package com.admin4u.views;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named("notiResolver")
@RequestScoped
public class NotificationResolverBean {
	
	
	private  int notificationId;
	
	private  String requestOrigin;

	public NotificationResolverBean() {
	}

	public int getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}

	public String getRequestOrigin() {
		return requestOrigin;
	}

	public void setRequestOrigin(String requestOrigin) {
		this.requestOrigin = requestOrigin;
	}
	
	

}
