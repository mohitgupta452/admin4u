package com.admin4u.views;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.util.enums.Services;
@Named("lobn")
@RequestScoped
public class LocationBean {
	
	@Inject 
	DropDownManager dropDownManager;
	@Inject 
	LocationManger locationManger;
	
	@Inject 
	UserSessionBean userSession;
	
	private List<String> locationName;
	private List<String> workLocationName;
	private List<String> workLocationByName;
	private List<String> CurrentWorklocation;
	@PostConstruct
	public void init()
	{
	locationName=dropDownManager.getAllLocationByType("Parent");
	CurrentWorklocation=locationManger.getWorkLocationBYCurrentLocation();
	
	}
	

	public List<String> getCurrentWorklocation() {
		return CurrentWorklocation;
	}


	public void setCurrentWorklocation(List<String> currentWorklocation) {
		CurrentWorklocation = currentWorklocation;
	}


	public List<String> getWorkLocationByName() {
		return workLocationByName;
	}


	public void setWorkLocationByName(List<String> workLocationByName) {
		this.workLocationByName = workLocationByName;
	}


	public List<String> getWorkLocationName() {
		return workLocationName;
	}




	public void setWorkLocationName(List<String> workLocationName) {
		this.workLocationName = workLocationName;
	}




	public List<String> getLocationName() {
		return locationName;
	}

	public void setLocationName(List<String> locationName) {
		this.locationName = locationName;
	}
	
	

}
