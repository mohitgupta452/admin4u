package com.admin4u.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.TatMaster;
import com.admin4u.service.bussiness.contract.EventTatManager;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData.TatLevelKeys;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Named("tatservicebean")
@RequestScoped
public class TatServiceBean implements Serializable {
	
	@Inject
	private DropDownBean dropDownBean;

	private static final long serialVersionUID = 1L;

	private Logger log = LoggerFactory.getLogger(getClass());

	private List<TatMaster> tatMasterList;

	private List<Map<String, String>> tatServiceList;

	public List<Map<String, String>> getTatServiceList() {
		return tatServiceList;
	}

	public void setTatServiceList(List<Map<String, String>> tatServiceList) {
		this.tatServiceList = tatServiceList;
	}

	@Inject
	private UserSessionBean userSession;

	@Inject
	private EventTatManager etManager;
	
	private List<String> services;

	public TatServiceBean() {

	}

	@PostConstruct
	public void init() {
		setTatMasterList(etManager.getAllTatPriority());
		tatServiceList = new ArrayList<>();
		for (TatMaster tm : tatMasterList) {
			Map<String, String> tempMap = new HashMap<>();
			tempMap.put("priority", tm.getPriority());
			tempMap.put("service", tm.getTatService());
			tempMap.put("id", tm.getId() + "");
			JsonObject level = tm.getTatLevel();
                  
			if(level.has(TatLevelKeys.level1TatTime.toString()))
			tempMap.put("level1",
					level.get(TatLevelKeys.level1TatTime.toString()).getAsString().replaceAll("\"","").equals("") || TatLevelKeys.level1TatTime.toString() == null
							? "N/A"
							: TimeUnit.MILLISECONDS
							.toHours(level.get(TatLevelKeys.level1TatTime.toString()).getAsLong()) + "");
			if(level.has(TatLevelKeys.level2TatTime.toString()))
			tempMap.put("level2",
					level.get(TatLevelKeys.level2TatTime.toString()).getAsString().replaceAll("\"","").equals("") || TatLevelKeys.level2TatTime.toString() == null
							? "N/A"
							: TimeUnit.MILLISECONDS
									.toHours(level.get(TatLevelKeys.level2TatTime.toString()).getAsLong()) + "");
			if(level.has(TatLevelKeys.level3TatTime.toString()))
			tempMap.put("level3",
					level.get(TatLevelKeys.level3TatTime.toString()).getAsString().replaceAll("\"","").equals("") || TatLevelKeys.level3TatTime.toString() == null
							? "N/A"
							: TimeUnit.MILLISECONDS
									.toHours(level.get(TatLevelKeys.level3TatTime.toString()).getAsLong()) + "");
			if(level.has(TatLevelKeys.level4TatTime.toString()))
			tempMap.put("level4",
					level.get(TatLevelKeys.level4TatTime.toString()).getAsString().replaceAll("\"","").equals("") || TatLevelKeys.level4TatTime.toString() == null
							? "N/A"
							: TimeUnit.MILLISECONDS
									.toHours(level.get(TatLevelKeys.level4TatTime.toString()).getAsLong()) + "");

			/*
			 * for(Map.Entry<String,
			 * JsonElement>itr:tm.getTatLevel().entrySet()){
			 * tempMap.put("service", tm.get);
			 * 
			 * }
			 */

			tatServiceList.add(tempMap);
		}
		
		
		services = new ArrayList<>();
		services.add(Services.AIRTICKET.toString());
		services.add(Services.TRAINTICKET.toString());
		services.add(Services.CAB.toString());
		services.add(Services.HOTEL_BOOKING.toString());
		services.add(Services.GUEST_HOUSE_BOOKING.toString());
		dropDownBean.getHkNatureOfWork().forEach(hkService->{
			services.add(Services.HOUSEKEEPING.toString()+"_"+hkService);
		});
		
		           for(Entry<String, JsonElement> entry : dropDownBean.getRepairNatureOfWork().entrySet())
		           {
		        	   if(entry.getValue().isJsonArray())
		        	   new Gson().fromJson(entry.getValue(),List.class).forEach(repairServices->{
		        		   services.add(Services.REPAIRMAINTENANCE.toString()+"_"+repairServices);
		        	   });
		           }

	}

	public List<TatMaster> getTatMasterList() {
		return tatMasterList;
	}

	public void setTatMasterList(List<TatMaster> tatMasterList) {
		this.tatMasterList = tatMasterList;
	}

	public List<String> getServices() {
		return services;
	}

	public void setServices(List<String> services) {
		this.services = services;
	}
	
	

}
