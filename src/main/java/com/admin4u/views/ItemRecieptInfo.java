package com.admin4u.views;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.entity.CategaryMaster;
import com.admin4u.persistence.entity.ProductDetail;
import com.admin4u.persistence.entity.StockInfo;
import com.admin4u.persistence.entity.VendorInfo;
import com.admin4u.service.bussiness.contract.ProductManager;
import com.admin4u.service.bussiness.contract.VendorManager;

@Named("itemRecieptInfo")
@RequestScoped
public class ItemRecieptInfo {
	
	@Inject
	private ProductManager productManager;
	
	@Inject
	private VendorManager vendorManager;
	
	private List<CategaryMaster> allCategary;
	
	private List<VendorInfo> vendorInfos;
	
	private List<StockInfo> stockInfoList; 
	
	

	public ItemRecieptInfo() {
	}
	
	@PostConstruct
	private void intForm()
	{
		allCategary=productManager.getAllCategary();
		
		vendorInfos=vendorManager.getVendorData();
		
		stockInfoList = productManager.getStockDetails();
	}

	public List<CategaryMaster> getAllCategary() {
		return allCategary;
	}

	public void setAllCategary(List<CategaryMaster> allCategary) {
		this.allCategary = allCategary;
	}

	public List<VendorInfo> getVendorInfos() {
		return vendorInfos;
	}

	public void setVendorInfos(List<VendorInfo> vendorInfos) {
		this.vendorInfos = vendorInfos;
	}

	public List<StockInfo> getStockInfoList() {
		return stockInfoList;
	}

	public void setStockInfoList(List<StockInfo> stockInfoList) {
		this.stockInfoList = stockInfoList;
	}
	
	
	
	
}
