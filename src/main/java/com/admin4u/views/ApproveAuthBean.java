package com.admin4u.views;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.comprator.RequestComparator;
import com.admin4u.util.enums.Roles;

@Named("approveauth")
@RequestScoped
public class ApproveAuthBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject 
	private RequestResponseManager reqResManager;

	@Inject
	UserSessionBean userSession;
	
	private List<Map<String,Object>> HODDataList;
	
	@PostConstruct
	public void init()
	{
	HODDataList=reqResManager.getAuthRequest(userSession.getEmpData().getUserPrincipalName());
	Collections.sort(HODDataList,new RequestComparator().reversed());

	}
	
	public List<Map<String, Object>> getHODDataList() {
		return HODDataList;
	}
	public void setHODDataList(List<Map<String, Object>> hODDataList) {
		HODDataList = hODDataList;
	}
}
