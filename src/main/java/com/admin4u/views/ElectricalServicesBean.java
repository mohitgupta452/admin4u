package com.admin4u.views;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.util.enums.Services;

@Named("electricalService")
@RequestScoped
public class ElectricalServicesBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject 
	private DropDownManager dropDownManager;
	
	@Inject 
    private RequestResponseManager requestResponseManager;
	
	@Inject
	private UserSessionBean userSession;
	
	private Logger log= LoggerFactory.getLogger(getClass());
public ElectricalServicesBean() {
	}

    private List<String> categoryOfElectricalServices;
    private List<Map<String , Object>> USERDataList;

	@PostConstruct
	public void Init()
	{
		USERDataList=requestResponseManager.getRequest(userSession.getUser().getUserPrincipalName(),Services.ELECTRICAL_SERVICES.toString());
		
		 categoryOfElectricalServices = dropDownManager.getDropDownValuesListByName("electrical_category");
	}

	public List<String> getCategoryOfElectricalServices() {
		return categoryOfElectricalServices;
	}

	public void setCategoryOfElectricalServices(List<String> categoryOfElectricalServices) {
		this.categoryOfElectricalServices = categoryOfElectricalServices;
	}

	public List<Map<String, Object>> getUSERDataList() {
		return USERDataList;
	}

	public void setUSERDataList(List<Map<String, Object>> uSERDataList) {
		USERDataList = uSERDataList;
	}
	
	
	
}
