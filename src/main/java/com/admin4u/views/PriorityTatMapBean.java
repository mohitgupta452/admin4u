package com.admin4u.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.admin4u.persistence.entity.PriorityTatMap;
import com.admin4u.service.bussiness.contract.ServiceManager;
import com.admin4u.util.enums.Services;

@RequestScoped
@Named("priorityTat")
public class PriorityTatMapBean implements Serializable  {

	private static final long serialVersionUID = 4299312914240820991L;
	
	@Inject
	private ServiceManager serviceManager;
	
	private List<PriorityTatMap> priorityTatList;
	
	private Services[] services;
	

	public PriorityTatMapBean() {
	}
	
	@PostConstruct
	public void init(){
		
		setPriorityTatList(serviceManager.getAllTats());
		setServices(Services.values());
		for(Services as:services){
			System.out.println("Name of enum"+as.name());
		}
	}

	public List<PriorityTatMap> getPriorityTatList() {
		return priorityTatList;
	}

	public void setPriorityTatList(List<PriorityTatMap> priorityTatList) {
		this.priorityTatList = priorityTatList;
	}

	public Services[] getServices() {
		return services;
	}

	public void setServices(Services[] services) {
		this.services = services;
	}
	

}
