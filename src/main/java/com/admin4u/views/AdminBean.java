package com.admin4u.views;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.CategaryClosure;
import com.admin4u.persistence.entity.CategaryMaster;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.service.bussiness.contract.ProductManager;
import com.admin4u.service.bussiness.contract.RequestResponseManager;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.service.bussiness.contract.ServiceManager;
import com.admin4u.util.comprator.RequestComparator;
import com.admin4u.util.enums.Roles;
import com.admin4u.util.enums.SMSpaceStationType;
import com.admin4u.util.enums.Services;
import com.admin4u.util.service.ApplicationMetaData;
import com.admin4u.util.service.ProcessListener;
import com.admin4u.util.service.ProcessStates;
import com.admin4u.util.service.RequestMetadata;

@Named("admin")
@RequestScoped
public class AdminBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Logger log= LoggerFactory.getLogger(getClass());
	
	@Inject 
    private RequestResponseManager  reqResManager;	
	
	@Inject
	private  UserSessionBean userSession;
	
	@Inject
	private RoleManager roleManager;
	
	@Inject
	private ProductManager productManager;
	
	
	
	private List<Map<String,Object>> ADMINDataList;
	
	private List<EmployeeRole> employeeRoles;
	
	private List<String> approvalProcessList;
	
	private List<String> nonApprovalProcessList; 
	
	private List<CategaryClosure> masterCategary;
	
	private List<String> categary;
	
	
	
	public AdminBean() {
	}
	
	@PostConstruct
	public void init() 
	{
		
	setEmployeeRoles(roleManager.getAllRole(Roles.ADMIN));
	
	List<EmployeeRole> employeeRole =new ArrayList<>();
	employeeRole.addAll(employeeRoles);
	
	for(EmployeeRole empRole : employeeRole)
	{
		if(empRole.getUserPrincipalName().equals(userSession.getEmpData().getUserPrincipalName()))
           employeeRoles.remove(empRole);
	  }
	employeeRole.clear();
	
	
	ProcessListener processListener = new ProcessStates();
		
	ADMINDataList=reqResManager.getRequestByOwner(userSession.getEmpData().getUserPrincipalName(),Roles.ADMIN);
	
	
	for(Map<String,Object> adminMap : ADMINDataList)
	{
		log.debug("detaisl {}   {}",adminMap,adminMap.get(RequestMetadata.REQUEST_ID));
	}
	
	Collections.sort(ADMINDataList,new RequestComparator().reversed());
	
	approvalProcessList=processListener.getApprovalProcess();
	
	nonApprovalProcessList=processListener.getNonApprovalProcess();
	
	masterCategary=productManager.getCategoriesByDepth(0);
	

	}
	public List<Map<String, Object>> getADMINDataList() {
		return ADMINDataList;
	}
	public void setADMINDataList(List<Map<String, Object>> aDMINDataList) {
		ADMINDataList = aDMINDataList;
		
	}

	public List<String> getApprovalProcessList() {
		return approvalProcessList;
	}

	public void setApprovalProcessList(List<String> approvalProcessList) {
		this.approvalProcessList = approvalProcessList;
	}

	public List<String> getNonApprovalProcessList() {
		return nonApprovalProcessList;
	}

	public void setNonApprovalProcessList(List<String> nonApprovalProcessList) {
		this.nonApprovalProcessList = nonApprovalProcessList;
	}

	

	public List<EmployeeRole> getEmployeeRoles() {
		return employeeRoles;
	}

	public void setEmployeeRoles(List<EmployeeRole> employeeRoles) {
		this.employeeRoles = employeeRoles;
	}

	public List<CategaryClosure> getMasterCategary() {
		return masterCategary;
	}

	public void setMasterCategary(List<CategaryClosure> masterCategary) {
		this.masterCategary = masterCategary;
	}

	
	
	

}
