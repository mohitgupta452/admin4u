package com.admin4u.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.DeptCostCenterMap;
import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.service.bussiness.contract.DropDownManager;

@Named("costCenterMapBean")
@RequestScoped
public class CostCenterMapBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject 
	private DropDownManager dropDownManager;
	
	@Inject
	private  UserSessionBean userSession;
	
	private List<String> departmentList;
	private List<String> costCenterDropDown;
	private List<DeptCostCenterMap> dropCostCenterMap;
	
	
   public CostCenterMapBean() 
    {
	}
	
	@PostConstruct
	public void init()
	{	
	    dropCostCenterMap = dropDownManager.getAllDeptCostCenterMap();
		
		departmentList=dropDownManager.getDepartmentList();
		
		costCenterDropDown=dropDownManager.getDropDownValuesListByName("cost_centre");
	}

	public List<String> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(List<String> departmentList) {
		this.departmentList = departmentList;
	}

	public List<String> getCostCenterDropDown() {
		return costCenterDropDown;
	}

	public void setCostCenterDropDown(List<String> costCenterDropDown) {
		this.costCenterDropDown = costCenterDropDown;
	}

	public List<DeptCostCenterMap> getDropCostCenterMap() {
		return dropCostCenterMap;
	}

	public void setDropCostCenterMap(List<DeptCostCenterMap> dropCostCenterMap) {
		this.dropCostCenterMap = dropCostCenterMap;
	}



	
	
	
			

	}

