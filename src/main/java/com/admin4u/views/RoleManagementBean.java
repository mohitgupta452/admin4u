package com.admin4u.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.junit.experimental.categories.Categories.IncludeCategory;
import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.EmployeeRole;
import com.admin4u.service.bussiness.contract.DropDownManager;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Roles;
import com.admin4u.persistence.entity.EmployeeRoleServiceMaster;
@Named("empDataBean")

@RequestScoped

public class RoleManagementBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject 
	private RoleManager roleManager;
	
	
	@Inject 
	private DropDownManager dropDownManager;
	
	@Inject
	private IDMService idmService;
	
	@Inject 
	private UserSessionBean userSession;
	
	private List<EmployeeData> employeeDataList;
	
	private List<EmployeeData> tempEmployeeDataList;
	
	private List<EmployeeRole> locationDropdownList;
	
	private List<EmployeeRole> employeeRoleList;
	
	private List<String> roleDropDownList;
	
	private List<String> newLocationDropDownList;
	
	private String role;
	

	public RoleManagementBean() {
	}
	
	public void init()
	{	
		setTempEmployeeDataList(idmService.getAllEmployeeData());
			
		setEmployeeDataList(tempEmployeeDataList);
		
		roleDropDownList=new ArrayList<>();
		newLocationDropDownList=new ArrayList<>();
		
			if(role.equalsIgnoreCase(Roles.ADMIN.toString())){
				roleDropDownList.add(Roles.SPOC.toString());
				locationDropdownList=roleManager.getEmpByUpnAndRole(userSession.getEmpData().getUserPrincipalName(),Roles.ADMIN);
				for(EmployeeRole empRole:locationDropdownList){
				newLocationDropDownList.add(empRole.getLocation());
				employeeRoleList=roleManager.getEmployeeRolesByCreater(userSession.getEmpData().getUserPrincipalName(),Roles.SPOC);
				}
			}
			if(role.equalsIgnoreCase(Roles.APPLICATION_ADMIN.toString())){
				roleDropDownList.add(Roles.ADMIN.toString());
				newLocationDropDownList.addAll(dropDownManager.getAllLocationByType("Parent"));
				employeeRoleList=roleManager.getEmployeeRolesByCreater(userSession.getEmpData().getUserPrincipalName(),Roles.ADMIN);
			}
		}

	public List<EmployeeData> getEmployeeDataList() {
		return employeeDataList;
	}

	public void setEmployeeDataList(List<EmployeeData> employeeDataList) {
		this.employeeDataList = employeeDataList;
	}

	public List<EmployeeData> getTempEmployeeDataList() {
		return tempEmployeeDataList;
	}

	public void setTempEmployeeDataList(List<EmployeeData> tempEmployeeDataList) {
		this.tempEmployeeDataList = tempEmployeeDataList;
	}

	public List<EmployeeRole> getEmployeeRoleList() {
		return employeeRoleList;
	}

	public void setEmployeeRoleList(List<EmployeeRole> employeeRoleList) {
		this.employeeRoleList = employeeRoleList;
	}

	public List<EmployeeRole> getLocationDropdownList() {
		return locationDropdownList;
	}

	public void setLocationDropdownList(List<EmployeeRole> locationDropdownList) {
		this.locationDropdownList = locationDropdownList;
	}

	public List<String> getRoleDropDownList()
	{
		return roleDropDownList;
	}

	public void setRoleDropDownList(List<String> roleDropDownList)
	{
		this.roleDropDownList = roleDropDownList;
	}

	public List<String> getNewLocationDropDownList()
	{
		return newLocationDropDownList;
	}

	public void setNewLocationDropDownList(List<String> newLocationDropDownList)
	{
		this.newLocationDropDownList = newLocationDropDownList;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	this.init();
	}

	}

