package com.admin4u.views;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import com.admin4u.persistence.entity.EmployeeData;
import com.admin4u.persistence.entity.Location;
import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.IDMService;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.service.bussiness.contract.RoleManager;
import com.admin4u.util.enums.Roles;

@Named("userSession")
@SessionScoped
public class UserSessionBean implements Serializable {

	private static final long serialVersionUID = -8202628869277670789L;
	
	@Inject
	private LocationManger locationManager;

	private EmployeeData empData;
	
	private List<String> empRole;

	private String locationName;
	
	private int locationId;
	
	@Inject
	private RoleManager roleManager;
	
    private Location locationMaster;

	public UserSessionBean() {
	}
	
	@PostConstruct
	private void init()
	{
			}
	
	public boolean hasRole(Roles role)
	{
		if(empRole.contains(role.toString()) || empRole.contains(role.toString().toLowerCase()))
				return true;
		return false;
	}

	public EmployeeData getUser() {
		return empData;
	}

	public EmployeeData getEmpData() {
		return empData;
	}

	public void setEmpData(EmployeeData empData) {
		this.empData = empData;
	}

	public List<String> getEmpRole() {
		return empRole;
	}

	public void setEmpRole(List<String> empRole) {
		this.empRole = empRole;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
		locationMaster=locationManager.getLocByName(locationName);
	}

	public List<String> getRole() {
		return empRole;
	}

	public int getLocationId()
	{
		return locationId;
	}

	public void setLocationId(int locationId)
	{
		this.locationId = locationId;
	}

	public Location getLocationMaster() {
		return locationMaster;
	}

	public void setLocationMaster(Location locationMaster) {
		this.locationMaster = locationMaster;
	}

}
