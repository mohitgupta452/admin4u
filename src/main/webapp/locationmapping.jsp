<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Location Mapping</title>
<link rel="stylesheet" type="text/css"
	href="assets/select2/select2-bootstrap.css" />
</head>
<body>

<form>
<select name="parentLocation" class="admin-locationMap">
<c:forEach  items="${locMap.locations}" var="locationParent">
<option value="${locationParent.locationID}">${locationParent.locationName}</option>
</c:forEach>
</select>
<select name="subLocation" class="admin-locationMap"  multiple="multiple" size=20>
<c:forEach  items="${locMap.subLocations}" var="subLocation">
<option>${subLocation}</option>
</c:forEach>
</select>
<button type="button" onclick="mapSubLocation()">Map</button>
</form>
<script src="assets/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>
<script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/modernizr.min.js"></script>
<script type="text/javascript" src="assets/js/customJS/locationMapping.js"></script>
</body>
</html>