<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8"/>
		<title>Mini Ajax File Upload Form</title>

	 <!-- Google web fonts -->
 <link href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel='stylesheet' />  

		<!-- The main CSS file -->
<link href="assets/js/multiple-upload/css/style_multi.css" rel="stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  	</head>

	<body>
	
	
	<div class="container">
  <h2>Modal Example</h2>
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

  <!-- Modal -->
  <form id="upload" method="post" action= "" enctype="multipart/form-data">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
       

			
		
<div id="drop">

				<a>Browse</a>
				<input type="file" name="upl" multiple />
</div>

			<ul>
				
			</ul>
	
        
        
      </div>
      
    </div>
  </div>
  </form>
</div>

		<!-- JavaScript Includes -->
	 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
		<script src="assets/js/multiple-upload/js/jquery.knob.js"></script>

		<!-- jQuery File Upload Dependencies -->
		<script src="assets/js/multiple-upload/js/jquery.ui.widget.js"></script>
		<script src="assets/js/multiple-upload/js/jquery.iframe-transport.js"></script>
		<script src="assets/js/multiple-upload/js/jquery.fileupload.js"></script>
		
		<!-- Our main JS file -->
		<script src="assets/js/multiple-upload/js/script.multi.upload.js"></script>


		<!-- Only used for the demos. Please ignore and remove. --> 
<script src="http://cdn.tutorialzine.com/misc/enhance/v1.js" async></script> 
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	</body>
</html>