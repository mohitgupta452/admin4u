<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="com.admin4u.views.RoleManagementBean"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:admin-layout>
<jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    
      <script src ="assets/js/customJS/role_management.js"></script>
    <!--dynamic table-->
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>

    </jsp:attribute>
    <jsp:body>
        <div class="page-heading">
              </div>
    <!-- page heading end-->

    <!--body wrapper start-->
        <div class="wrapper">
           <!--  Body contents goes here -->
<section class="panel">
<header class="panel-heading custom-tab dark-tab">Head
</header>
    
<form action="RoleManagementServlet" method="post" id="roleManagement" novalidate= "novalidate"  class="cmxform form-horizontal adminex-form" style="padding:30px;background: #fff;">
 
 <input type="hidden" name="as" value="${param.as}">
 <div class="row"> 
 <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <div class="col-sm-5">
  
                                <label>Role</label>
                          </div>
                           <div class="col-sm-7">
                            <select class="form-control" id="role" name="role">
                               <option value="">select...</option>
                                <c:forEach items="${empDataBean.roleDropDownList}" var="role">
                                <option ><c:out value="${role}"></c:out></option>
                                
                                </c:forEach>
                               
                                
                              </select>     
                              <br>
             <button type="button" class="btn btn-warning"  data-target="#myModal" data-toggle="modal" ><i class="fa fa-hand-o-right"></i> Add Employee</button>
                                                  
                                </div>
                                
                           </div>
                           </div>
                           
                           <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <div class="col-sm-5">
  
                                <label>Location</label>
                          </div>
                           <div class="col-sm-7">
                            <select class="form-control" id="location" name="location">
                               <option value="">select...</option>
                               <c:forEach items="${empDataBean.newLocationDropDownList}" var="locName">
                                <option><c:out value="${locName}"></c:out> </option>
                                
                                </c:forEach>
                    
                                
                              </select>   
                                
                              <br>
                              <center><button class="btn btn-primary" type="submit" name="action" value="assignRole" ><i class="fa fa-hand-o-right"></i> Submit</button></center>
                              
                                </div>
                                
                           </div>
                           
                           <!-- =========Modal code=========== -->

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="serviceModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Select Services</h4>
                                    </div>
                                    <div class="modal-body">
                                    <br>
                                    
                                     <div class="row">
                                            
                           
                    <div class="col-sm-12">
                        <div class="form-group"> 
                        <h3>Select Services</h3>
                           <div class="">
                            <table>
                            <tr><td><input type="checkbox" class="selectedServices" name="services" value="AIRTICKET"/>
								<label>Air Request</label></td>
								</tr>
								
								 <tr><td><input type="checkbox" class="selectedServices" name="services" value="TRAINTICKET"/>
								<label>Train Request</label></td>
								</tr>
								
								 <tr><td><input type="checkbox" class="selectedServices" name="services" value="CAB"/>
								<label>Cab </label></td>
								</tr>
								
								<tr><td><input type="checkbox" class="selectedServices" name="services" value="HotelGuestHouse"/>
								<label>Hotel Guest House </label></td>
								</tr>
								
								<tr><td><input type="checkbox" class="selectedServices" name="services" value="HOUSEKEEPING"/>
								<label>HOUSEKEEPING</label></td>
								</tr>
								
								<tr><td><input type="checkbox" class="selectedServices" name="services" value="REPAIRMAINTENANCE"/>
								<label>Repair Maintenance </label></td>
								</tr>
                            </table>
                                                  
                                </div>
                                
                           </div>
                                            <center><button type="button" class="btn btn-info"  data-dismiss="modal" class="close"><i class="fa fa-hand-o-right"></i> OK</button></center>
                      
                      </div>
                                          </div>
                                    
                                    
                                    
                                    

                                       
                                    </div>

                                </div>
                            </div>
                        </div>


<!-- ==========End Modal==================== -->
                           
                           
                           </div>
<br>
<!-- =========Modal code=========== -->

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Add Employee</h4>
                                    </div>
                                    <div class="modal-body">
                                    <div class="row" style="height:150px;overflow:auto; border: 1px solid gray;">
                                    <table class="display table table-bordered" id="about">
                                    <tr><td>Name</td><td>Email</td><td>Comapany</td></tr>
                                    </table>
                                    
                                    
                                    </div>
                                    <br>
                                    
                                     <div class="row" style="height: 250px;overflow:auto;">
                                            
                           
                    <div class="col-md-12">
                        <div class="form-group"> 
                            
                          
                                <div  id="sourceDiv">
								<table class="display table table-bordered"><thead>
								<tr><th>Employee Name</th>
								<th>Email</th>
								<th>Company</th>
								</tr></thead>
								<tbody>
								<c:forEach items="#{empDataBean.employeeDataList}" var="employee">
								<tr><td> <input type="checkbox" class="selectedEmployees" name="employeesUPN" value="<c:out value="${employee.userPrincipalName}"></c:out>"/>
								<label><c:out value="${employee.displayName} ">
								</c:out></label></td><td><c:out value="${employee.userPrincipalName} "></c:out></td>
								<td><c:out value="${employee.company} "></c:out></td> </tr>
								</c:forEach>
								</tbody>
                                </table>
                                </div>
                     
                      
                      </div>
                      
                      </div>
                                          </div>
                                    
                                    
                                        <center><button type="button" class="btn btn-info" id="modalOkButton" data-dismiss="modal" class="close"><i class="fa fa-hand-o-right"></i> OK</button></center>
                                    
                                    

                                       
                                    </div>

                                </div>
                            </div>
                        </div>


<!-- ==========End Modal==================== -->

<div id="targetDiv" class="ms-list" style="width:200px;height:150px;float: right;border: 1px solid #ccc;margin-top: -31px;margin-bottom: 35px;overflow: auto;padding:20px;" >


</div>
          
          
          
  </form>


  <!-- =============================Table Start Here -->
<section class="panel">
<table class="table table-bordered table-striped table-condensed">
<tr>
 <th>Employee Name</th> 
 <th>Role</th>
 <th>Location</th>
 <th>Action</th>  
</tr>
         <c:forEach  items="#{empDataBean.employeeRoleList}" var="employee">

<tr id="rid${employee.employeeRoleID}"><td><c:out value="${employee.displayName}"></c:out></td> 
<td><c:out value="${employee.role}"></c:out><c:forEach items="${employee.employeeRoleServicemaster}" var="spocService">
<li style="margin-left:50px;"><c:out value="${spocService.service}"></c:out></li>
</c:forEach></td>
<td><c:out value="${employee.location}"></c:out></td>
<td><button type="button" class="btn btn-danger" onclick="callDeleteRole(${employee.employeeRoleID})" ><i class="fa fa-hand-o-right"></i> Remove</button></td></tr>

</c:forEach>
</table>

</section><br>
  <!-- ==============table end here -->
  


</section>




        </div>
        <!--body wrapper end-->
                      
       

  
        
    </jsp:body>
</t:admin-layout>
