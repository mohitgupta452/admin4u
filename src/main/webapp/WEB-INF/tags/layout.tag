<!DOCTYPE html>
<%@tag description="Admin4U" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="req" value="${pageContext.request}" />
<c:set var="uri" value="${req.requestURI}" />
<c:set var="url">${req.requestURL}</c:set>
<%@attribute name="header" fragment="true"%>
<%@attribute name="extrajs" fragment="true"%>
<html lang="en">
<head>
<base
	href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/" />
<meta charset="utf-8">
<meta name="viewport"Overall Project template
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="keywords"
	content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
<meta name="description" content="">
<meta name="author" content="Adminpanel">
<link rel="shortcut icon" href="" onclick="return false;"
	type="image/png">

<title>Admin4U</title>

<link href="https://fonts.googleapis.com/css?family=Scope+One" rel="stylesheet">
<!--common-->
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"
	rel="stylesheet">
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/style-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/css/custom.css">


<!--select2-->
<link rel="stylesheet" type="text/css" href="assets/select2/select2.css" />
<link rel="stylesheet" type="text/css"
	href="assets/select2/select2-bootstrap.css" />

 <!--gritter css-->
 <!--  <link rel="stylesheet" type="text/css" href="js/gritter/css/jquery.gritter.css" /> -->
<!--select2-->
<!--===style=====-->

<style>

body {
	margin: 0;
	padding: 0;
}
.ref-dropdown {
	border-radius: 30px !important;
	margin-top: -9px;
	/*   background-color: #eee;*/
	width: 200px;
	cursor: pointer;
		background: rgba(255,255,255,1);
background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,255,255,1)), color-stop(47%, rgba(246,246,246,1)), color-stop(100%, rgba(237,237,237,1)));
background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%);
background: -o-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%);
background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%);
background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed', GradientType=0 );
pointer:cursor;
border:none;
}
.dashboard-text
{
	color:#ccc;
	font-family:arial;
}
/* enable absolute positioning */
.inner-addon {
	position: relative;
}

/* style glyph */
.inner-addon .glyphicon {
	position: absolute;
	padding: 10px;
	pointer-events: none;
}

/* align glyph */
{
	left: 0px;
}

.right-addon .glyphicon {
	right: 0px;
}

/* add padding  */
.left-addon input {
	padding-left: 30px;
}

.right-addon input {
	padding-right: 30px;
}
.imgbg:hover {
	background-color:#fff;
	color:#263238 !important;
	-webkit-transition: all 500ms ease;
	-moz-transition: all 500ms ease;
	-ms-transition: all 500ms ease;
	-o-transition: all 500ms ease;
	transition: all 500ms ease;

}
.imgbg:hover h2 {
	color:#455A64;
	font-weight:400;
	-webkit-transition: all 600ms ease;
	-moz-transition: all 600ms ease;
	-ms-transition: all 600ms ease;
	-o-transition: all 600ms ease;
	transition: all 600ms ease;

}
.notification-menu .dropdown-menu-head ul
{
border:1px solid transparent;
}
.notification-menu .dropdown-menu-head ul li a span
{
    font-family: sans-serif;
    color: #666;
}
.notification-menu .dropdown-menu
{
	border-bottom-left-radius: 20px;
	border-bottom-right-radius: 20px;
}
.foot
{
	display:none !important;
}
.status-text
{
	font-family: tahoma;
    font-size: 13px;
    text-transform: uppercase;
    color: #455A64;
    font-weight: 600;
}
body::-webkit-scrollbar {
    width: 1em;
}
 
body::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
}
 
body::-webkit-scrollbar-thumb {
  background-color: darkgrey;
  outline: 1px solid slategrey;
}

.bdg-box
{
	padding-top:8px;
}
.bdg-ul{
    border-radius: 20px;
    text-align: center;
    padding: 6px;
    background: rgba(235,233,249,1);
background: -moz-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.84) 50%, rgba(206,199,236,0.83) 51%, rgba(193,191,234,0.67) 100%);
background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(235,233,249,1)), color-stop(50%, rgba(216,208,239,0.84)), color-stop(51%, rgba(206,199,236,0.83)), color-stop(100%, rgba(193,191,234,0.67)));
background: -webkit-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.84) 50%, rgba(206,199,236,0.83) 51%, rgba(193,191,234,0.67) 100%);
background: -o-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.84) 50%, rgba(206,199,236,0.83) 51%, rgba(193,191,234,0.67) 100%);
background: -ms-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.84) 50%, rgba(206,199,236,0.83) 51%, rgba(193,191,234,0.67) 100%);
background: linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.84) 50%, rgba(206,199,236,0.83) 51%, rgba(193,191,234,0.67) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebe9f9', endColorstr='#c1bfea', GradientType=1 );
}
.badge{
background-color:#546E7A;
color:#fff;
}
.bdg1:hover{
background-color:#FB8C00 !important;
color:#fff !important;
}
.bdg2:hover{
background-color:#8BC34A !important;
color:#fff !important;
}
.bdg3:hover{
background-color:#f44336 !important;
color:#fff !important;
}
.bdg4:hover{
background-color:#03A9F4 !important;
color:#fff !important;
}

/* css for avatar icon hide */
.notification-menu .dropdown-toggle img
{
display:none;
}
.notification-menu .dropdown-toggle
{
	padding: 15px 10px !important;
}
</style>

<!--   ====End style===-->
<jsp:invoke fragment="header" />

</head>

<body class="sticky-header" onload="start();">

	<section>
		<!-- left side start-->
		<div class="left-side sticky-left-side" style="background-color: transparent !important;
	width: 0px !important;">

			<!--logo and iconic logo start-->
			<div class="logo logo-style" >
			<a href="user/index.jsp">
		<table>
					<tbody>
						<tr>
							<td><img src="assets/images/logo.png" alt=""
								style="height: 49px; margin-left: 30px; margin-top: -3px;">
							</td>
							<td class="td-space">
								<div class="row">
									<label class="label-size">Admin4U</label>
								</div>
								<div class="row">

								
								<label class ="label-loc-size" id="log_service_loc">
                                <c:out value="${userSession.locationMaster.tollfree}"></c:out></label>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</a>
			</div>

			<div class="logo-icon text-center">
				<a href="user/index.jsp"><img src="assets/images/logo_icon.png" alt="" style="height: 45px;width: 45px">
				</a>
			</div>
			<!--logo and iconic logo end-->

			<!-- leftbar menu placed here -->
			<div class="left-side-inner">

				<!-- visible to small devices only -->
				<div class="visible-xs hidden-sm hidden-md hidden-lg">
					<div class="media logged-user">
						<img alt="" src="assets/images/photos/user-avatar.png"
							class="media-object">
						<div class="media-body">
							<h4>
								<a href="#"> Administrator</a>
							</h4>
						</div>
					</div>

					<h5 class="left-nav-title">Account Information</h5>
					<ul class="nav nav-pills nav-stacked custom-nav">

						<!-- Role Based List  -->

						<!-- <!-- userSession.empRole == Admin -->





						userSession.empRole == Spoc



						userSession.empRole == Approver -->



						<li><a href="pages/temp/lock_screen.html"><i
								class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
					</ul>
				</div>

				<!--sidebar nav start-->
				<!-- 				<ul class="nav nav-pills nav-stacked custom-nav"> -->
				<!-- 					<li class=""><a href="user/index.jsp" alt=""><i -->
				<!-- 							class="fa fa-home"></i> <span>Dashboard</span></a></li> -->

				<!-- 					<li class="TOURTRAVEL" id=""><a href="user/tour_travel.jsp" -->
				<!-- 						alt=""><i class="fa fa-plane"></i> <span>Tour & Travels</span></a></li> -->
				<!-- 					<li class="HOTEL" id=""><a href="user/hotel_guesthouse.jsp" -->
				<!-- 						alt=""><i class="fa fa-book"></i> <span>Hotel / Guest -->
				<!-- 								House</span></a></li> -->
				<!-- 					<li class="REPAIRMAINTENANCE" id=""><a -->
				<!-- 						href="user/repair_maintenance.jsp"><i class="fa fa-cogs"></i> -->
				<!-- 							<span>Repair Maintenance</span></a></li> -->

				<!-- 					<li class="HOUSEKEEPING" id="" style="border-bottom: 1px solid;"><a -->
				<!-- 						href="user/house_keeping.jsp"><i class="fa fa-bullhorn"></i> <span>HouseKeeping</span></a></li> -->

				<!-- 					<li class=""><a href="" onclick="return false;"><i -->
				<!-- 							class="fa fa-envelope"></i> <span>Pest Control</span></a></li> -->

				<!-- 					<li class="VENDORMANAGEMENT"><a href="user/vendor.jsp"><i -->
				<!-- 							class="fa fa-tasks"></i> <span>Vendor Management</span></a></li> -->
				<!-- 					<li class=""><a href="" onclick="return false;"><i -->
				<!-- 							class="fa fa-bar-chart-o"></i> <span>Utility Bills -->
				<!-- 								Management</span></a></li> -->
				<!-- 					<li class=""><a href="" onclick="return false;"><i -->
				<!-- 							class="fa fa-th-list"></i> <span>Dispatch Management</span></a></li> -->

				<!-- 					<li class="" style="border-bottom: 1px solid;"><a href="" -->
				<!-- 						onclick="return false;"><i class="fa fa-map-marker"></i> <span>Meeting -->
				<!-- 								Room</span></a></li> -->
				<!-- 					<li class="ELECTRICAL_SERVICES"><a -->
				<!-- 						href="user/electrical_services.jsp"><i class="fa fa-file-text"></i> -->
				<!-- 							<span>Electrical Services</span></a></li> -->
				<!-- 					<li><a href="" onclick="return false;"><i -->
				<!-- 							class="fa fa-sign-in"></i> <span>Logistics </span></a></li> -->
				<!-- 				</ul> -->
				<!--sidebar nav end-->

			</div>




		</div>
		<!-- left side end-->

		<!-- main content start-->
		<div class="main-content">

			<!-- header section start-->
			<div class="header-section">

				<!--toggle button start-->
<!-- 				<a class="toggle-btn"><i class="fa fa-bars"></i></a> -->
				<!--toggle button end-->
				<div class="col-md-2 bdg-box">
					<ul class="list-inline bdg-ul">

						<li><a href="#" data-toggle="tooltip" title="Pending"><span id="pendingStCount" class="badge bdg1"></span></a></li>
						<li><a href="#" data-toggle="tooltip" title="Accepted"><span id="acceptedStCount" class="badge bdg2"></span></a></li>
						<li><a href="#" data-toggle="tooltip" title="Rejected"><span  id="rejectedStCount"class="badge bdg3"></span></a></li>
						<li><a href="#" data-toggle="tooltip" title="Complete"><span id="completedStCount" class="badge bdg4" ></span></a></li>


					</ul>
				</div>
				
							
				<div id="msg">
					<c:if test="${not empty requestScope.alertMessage}">
						<div class='alert alert-success' id="msgAlert">
							<strong>Success!</strong>
							<c:out value="${requestScope.alertMessage}"></c:out>
						</div>
					</c:if>
				</div>



				<!--search end-->
				<div class="menu-left">
					<ul class="notification-menu">

					<!-- 	<li><a href="#"
							class="btn btn-default dropdown-toggle info-number"
							data-toggle="dropdown" onclick=""> <i class="fa fa-bell-o"></i>
								<span class="badge" id="notificationCount"><c:out
										value="${notification.notificationCount }"></c:out></span>
						</a>
							<div class="dropdown-menu dropdown-menu-head pull-right">
								<form id="notificationFrm">
									<h5 class="title">Notifications</h5>
									<ul class="dropdown-list normal-list" id="notificationMenu">

										<c:forEach items="${notification.notificationViewMap.entrySet()}"
											var="notify">

											<li class="new">
											            
											            <a href="${notify.value.redirection}">
											            <span id="notificationSpan"> <c:out
														value="${notify.value.msg}"></c:out>
														<c:out
														value="${notify.value.time}"></c:out>
														
														</span>
														</a>
														
														</li>
										</c:forEach>

										<li class="new"><a href="user/allNotifications.jsp">See
												All Notifications</a></li>
									</ul>
								</form>
							</div></li> -->
						<li>
							<!-- <div class="inner-addon right-addon">
								<i class="glyphicon glyphicon-th"></i> <input type="text"
									class="form-control search-input" placeholder="Search" />
							</div> -->
						</li>
					</ul>
				</div>



				<!--updates menu start -->
				<div class="menu-right">
				
					<ul class="notification-menu">
					<li class=""><a href="#"
							class="btn btn-default dropdown-toggle info-number"
							data-toggle="dropdown">
									
								<div class="col-sm-6">
								<label class="search-label" style="font-family: sans-serif;">Change service location</label>
								</div>
								<div class="col-sm-6">
									<select class="form-control select2" id="globalLocation"
										name="location" style="width: 150px; position: inherit;">
										<c:forEach items="${lobn.locationName}" var="location">
											<option value="${location}"
												<c:if test="${userSession.locationName==location }"><c:out value="selected"/></c:if>>
												<c:out value="${location}"></c:out></option>

										</c:forEach>
									</select>
								</div>
						</a></li>
						
<!-- New Notification -->
<li><a href="javascript:;" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown" onclick="markAsRead()"> <i class="fa fa-bell-o"></i>
			<span class="badge" id="notificationCount"></span>
						</a>
			<div class="dropdown-menu dropdown-menu-head pull-right" id="main-div">
<form id="notificationFrm">
	<h5 class="title">Notifications</h5>
	<ul class="dropdown-list normal-list list" id="notificationMenu">

		<c:forEach items="${notification.notificationViewMap.entrySet()}"
			var="notify">

			<li class="new">
	            <a href="${notify.value.redirection}">
	<span id="${notify.value.id}" class="
 				<c:if test="${notify.value.isRead == true}">
			      <c:out value="readed"></c:out></c:if>
			         <c:if test="${notify.value.isRead == false}"><c:out value="unread"></c:out>
			         </c:if>">
			            <c:out value="${notify.value.msg}"></c:out>
						<c:out
						value="${notify.value.time}"></c:out>
						</span>
						</a>
						</li>
		</c:forEach>
		<li class="new"><a href="user/allNotifications.jsp">See
												All Notifications</a></li></ul>
								</form></div></li>
							<!--Notification End  -->

							<!-- <li data-toggle="tooltip" title="Status"><a href="javascript:;" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown" onclick="statusCount()"> <i class="fa fa-chevron-circle-down" aria-hidden="true"></i>
								<span class="badge" id="notificationCount"></span>
						</a>
							<div class="dropdown-menu dropdown-menu-head pull-right">
								<form id="notificationFrm">
									<h5 class="title">Status</h5>
									<ul class="dropdown-list normal-list">
										<li><span class="badge" id ="pendingStCount" style="background-color:#FB8C00;color:#fff;"></span> <p class="status-text">Pending</p></li>
										<li><span class="badge" id ="acceptedStCount" style="background-color:#8BC34A; color:#fff;"></span> <p class="status-text">Accepted</p></li>
										<li><span class="badge"  id ="rejectedStCount"style="background-color:#f44336; color:#fff;"></span> <p class="status-text">Rejected</p></li>
										<li><span class="badge" id ="completedStCount"style="background-color:#0288D1; color:#fff;"></span> <p class="status-text">Completed</p></li>
										


									</ul>
								</form>
							</div></li> -->

<!-- 							<li data-toggle="tooltip" title="Status"><a href="javascript:;" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown" onclick="markAsRead()"> <i class="fa fa-chevron-circle-down" aria-hidden="true"></i> -->
<!-- 								<span class="badge" id="notificationCount"></span> -->
<!-- 						</a> -->
<!-- 							<div class="dropdown-menu dropdown-menu-head pull-right"> -->
<!-- 								<form id="notificationFrm"> -->
<!-- 									<h5 class="title">Status</h5> -->
<!-- 									<ul class="dropdown-list normal-list"> -->
<!-- 										<li><span class="badge" style="background-color:#FB8C00;color:#fff;">5</span> <p class="status-text">Pending</p></li> -->
<!-- 										<li><span class="badge" style="background-color:#8BC34A; color:#fff;">5</span> <p class="status-text">Accepted</p></li> -->
<!-- 										<li><span class="badge" style="background-color:#f44336; color:#fff;">5</span> <p class="status-text">Rejected</p></li> -->
<!-- 										<li><span class="badge" style="background-color:#0288D1; color:#fff;">5</span> <p class="status-text">Completed</p></li> -->
<!-- 									</ul> -->
<!-- 								</form> -->
<!-- 							</div></li> -->

<!-- End -->

<!-- 
						<li><a href="#"
							class="btn btn-default dropdown-toggle info-number"
							data-toggle="dropdown" onclick=""> <i class="fa fa-bell-o"></i>
								<span class="badge" id="notificationCount"><c:out
										value="${notification.notificationCount }"></c:out></span>
						</a>
							<div class="dropdown-menu dropdown-menu-head pull-right">
								<form id="notificationFrm">
									<h5 class="title">Notifications</h5>
									<ul class="dropdown-list normal-list" id="notificationMenu">

										<c:forEach items="${notification.notificationList}"
											var="notify">

											<li class="new"><span id="notificationSpan"> <c:out
														value="${notify['message']}"></c:out></span></li>
										</c:forEach>

										<li class="new"><a href="user/allNotifications.jsp">See
												All Notifications</a></li>
									</ul>
								</form>
							</div></li> -->
						
						 <!-- <li class="req-table"><a href="user/role_base_dashboard.jsp"><i class="fa fa-cog"></i> </a></li> -->
						

                <li class="">
                    <a href="#" class="btn btn-default dropdown-toggle logo-style" data-toggle="dropdown">
                        <img src="assets/images/photos/user-avatar.png" alt="">
                       <c:out value="${userSession.empData.displayName}"/>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                        <c:forEach  var="role" items="${userSession.empRole}">
							<c:if test="${role == 'ADMIN'}">

   							<li><a href="user/s_asadmin.jsp"><i class="fa fa-cog"></i> <span>Switch as Administrator</span></a></li>
							</c:if>
							<c:if test="${role == 'SPOC' || role == 'OTHERSPOC' }">
								<li><a href="user/spoc.jsp"><i class="fa fa-cog"></i> <span>Switch as Spoc</span></a></li>
							</c:if>
							<c:if test="${role == 'APPLICATION_ADMIN'}">
							<li><a href="user/app_admin/app_admin.jsp"><i class="fa fa-cog"></i> <span>Switch as App Admin</span></a></li>
							</c:if>
							<c:if test="${role == 'APPROVAL_AUTHORITY' }">
							<li><a href="user/approvalauthority.jsp"><i class="fa fa-cog"></i> <span> My Task</span></a></li>
							</c:if>
						</c:forEach>
						<li><a href="user/user/user_task.jsp"><i class="fa fa-sign-out"></i>My Request</a></li>
                        <li><a href="LogoutServlet"><i class="fa fa-sign-out"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
				<!--notification menu end -->

			</div>
			<!-- header section end-->
			<!--body wrapper start-->
			<div class="wrapper wrap"  id="newStuff">

				<jsp:doBody />


			</div>

			<!--body wrapper end-->
			<!--footer section start-->

			<footer class="sticky-footer foot"> 2016 &copy; Admin4U by
				Turningcloud </footer>
			<!--footer section end-->
		</div>
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Change Location</h4>
					</div>
					<div class="modal-body">
						<p>DO you want location change</p>
					</div>
					<div class="modal-footer">

						<button type="button" class="btn btn-danger"
							id="locationChangeButton">yes</button>
						<button type="button" class="btn btn-default"
							onclick="window.location.reload()" data-dismiss="modal">NO</button>
					</div>
				</div>

			</div>
		</div>
		<!-- main content end-->
	</section>
	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="assets/js/jquery-1.10.2.min.js"></script>
	<script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/modernizr.min.js"></script>
	<script src="assets/js/jquery.nicescroll.js"></script>
	<script src="assets/vendor/blockui/blockUI.js"></script>
	<script src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/additional-methods.js"></script>

	<script src="assets/js/customJS/index.js"></script>
	<!--select2-->
	<script src="assets/select2/select.js"></script>
	<!--select2-->
	<!--common scripts for all pages-->
	<script src="assets/js/scripts.js"></script>
	
	<script type="text/javascript"></script>
	
	
	
	<!-- Placed js at the end of the document so the pages load faster -->
<!--data table-->
<script type="text/javascript" src="assets/js/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>


<script>
     function start() {
    	    	 
	    if(typeof(EventSource!=="Undefined"))
		{
         var eventSource = new EventSource("NotificationServlet");
        
        eventSource.onmessage = function(event) {
			console.log("response data"+event.data);
			var j = JSON.parse(event.data);

			console.log(j);
			
	        };
		}
	else
		{

			conslole.log("Unsupported Browser");
		} 
     } 
    </script>

	<script>
     $.ajaxSetup({
        beforeSend:function(){
            $.blockUI({
				message : '<img src="assets/images/loading.gif" width="100"/>',
				css : {

					 z-index:9999!important,
					border : 'none',
					backgroundColor : 'transparent',
					cursor : 'wait'
						
				}

            });
			document.body.style.cursor = 'wait';
        },
        complete:function(xhr,status){
        	document.body.style.cursor = 'auto';
			$.unblockUI();
        }
    }); 


    </script>
	<jsp:invoke fragment="extrajs" />

</body>
</html> 