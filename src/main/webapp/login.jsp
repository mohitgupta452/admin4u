<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="#" type="image/png">

<title>Admin4u</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/style-responsive.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway|Abel"
	rel="stylesheet">



<style>

.form-signin input[type="text"], .form-signin input[type="password"] {
	border: 1px solid rgba(0, 0, 0, 0.2) !important;
	border-radius: 30px !important;
	background-color: #fff;
    color: gray;
    -webkit-border-radius: 30px !important;
}
.form-signin p
{
	margin-top: -9px;
    margin-bottom: 18px;
    font-size: 13px !important;
}
.login-head
{
color: #78909C;
    font-family: fantasy;
    font-size: 33px;
}
</style>
<script>
	$(document).ready(function() {
		$("body").tooltip({
			selector : '[data-toggle=tooltip]'
		});
	});
</script>
</head>

<body class="login-body">

	<div class="container">


		<div class="form-signin-heading text-center">
			<h1 class="sign-title">Sign In</h1>
			<!-- 	<img src="assets/images/login-logo.png" alt="" /> -->
		</div>
<!-- 		<h1 style="font-family: abel; color: #fff;">Admin4u</h1> -->
		<form class="form-signin" action="loginservlet" method="POST"
			role="form">
			<div class="login-wrap main-div">
			<h2 class="login-head">SignIn</h2><p>for Admin4u</p>
				<div class="form-group">
					<input type="text" class="form-control inp_field" name="userid"
						placeholder="User ID" autofocus>
				</div>

				<!-- <div class="form-group">
					<input type="password" class="form-control inp_field"
						name="password" placeholder="Password">
				</div> -->

				

				<!-- <button class="btn col-sm-12 login_btn" type="submit">Login</button> -->

				<div class="panel panel-primary"
					style="background-color: transparent !important;">
					<div class="panel-body">
						<div class="form-group">
							<div class="row ">
								<center class="custom_or">
									<span style="color: #fff;">OR</span>
								</center>
								<a href="${login.googleAuthURL}"
									class="btn google_btn col-sm-12 custom_brand"><i
									class="fa fa-google-plus" aria-hidden="true"></i> Sign in With
									Google</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>

		<!-- Modal -->
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
			tabindex="-1" id="myModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title">Forgot Password ?</h4>
					</div>
					<div class="modal-body">
						<p>Enter your e-mail address below to reset your password.</p>
						<input type="text" name="email" placeholder="Email"
							autocomplete="off" class="form-control placeholder-no-fix">

					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
						<button class="btn btn-primary" type="button">Submit</button>
					</div>


				</div>
			</div>
		</div>
		<!-- modal -->
	</div>

	<div class="footer">
		<p
			style="float: right; color: #fff; padding-top: 15px; padding-right: 10px; font-family: 'Raleway', sans-serif;">&copy
			Admin4u 2016</p>
	</div>

	<!-- Placed js at the end of the document so the pages load faster -->

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="assets/js/jquery-1.10.2.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/modernizr.min.js"></script>

</body>
</html>
