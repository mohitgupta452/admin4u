<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:layout>

	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">
    
    
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    
    <!--dynamic table-->
    <script type="text/javascript" language="javascript"src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
    
<script type="text/javascript"	src="assets/js/data-tables/DT_bootstrap.js"></script>
<script		src="assets/js/dynamic_table_init.js"></script>
    
    
    
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
 
    <script src="assets/js/customJS/meeting_room_management.js"></script>
<script src="assets/js/customJS/utility.js"></script>
<script src="assets/js/pickers-init.js"></script>
<script src="assets/js/pickers-init2.js"></script>



    </jsp:attribute>
	<jsp:body>
	
	<!--  
<c:if test="${not empty sessionScope.msg}"> 
   <div class="alert alert-success" style="width:100%;height:40px; font-size:20px;text-align:center;">
    ${sessionScope.msg}
    ${ session.removeAttribute("msg")}
    </div>
</c:if> 

-->
      <div class="page-heading">
        <!-- Cab/Ticket -->
          <section class="panel">
                        <header class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs">
                                <!-- <li class="active">
                                    <a href="#ticket_requests" data-toggle="tab">Ticket Booking</a>
                                </li> -->
                               <!--  <li >
                                    <a href="#cab_requests" data-toggle="tab">Cab Request</a>
                                </li> -->
                                <li class="active">
                                    <a href="#repair_maintenance" data-toggle="tab">Meeting/Conference Room Management</a>
                                </li>
                               <!--  <li>
                                    <a href="#Requests" data-toggle="tab">Requests</a>
                                </li> -->
                                <!-- <li>
                                    <a href="#admin_requests" data-toggle="tab">Booking View</a>
                                </li> -->
                               <!--  <li>
                                    <a href="#admin_travel_requests" data-toggle="tab">Admin</a>
                                </li> -->
                               <!--  <li>
                                    <a href="#user_confirmation" data-toggle="tab">User Confirmation</a>
                                </li> -->
                                
                                <!-- <li>
                                    <a href="#admin_travel_department" data-toggle="tab">Transport Admin</a>
                                </li> -->
                            </ul>
                        </header>

<!-- =====Tab Pannel Start Here -->
<div class="panel-body">
    <div class="tab-content">
        
<!--============= Form for Ticket Requests ==================-->

<!--============= End Here Ticket Requests ==================-->


<!-- ==========Form Hotel / Guest House Booking =======  -->
    <div class="tab-pane active" id="repair_maintenance">
             <!--==========Tab Start Here========-->                    
                              
             <!--==========Row@2 ================-->
             <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-12">
                                <label ><b>Meeting/Conference Information</b></label>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                             <form action="MeetingRoomManagementServlet" method="post" id="meetingManagementForm" enctype="multipart/form-data">
                
                <div class="row">
                    
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Meeting Type<span class="requiredField"> *</span> </label>
                            </div>
                            <div class="col-sm-7">
                              <select  class="form-control " id="meetingType" name="meetingType">
                                <option value="">Select...</option>
                                  
                                  <option value="Internal">Internal</option>
                                  <option value="External">External</option>
                              </select> 
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Conference Room <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
							<select  class="form-control" id="conferenceRoom" name="conferenceRoom">
                                <option value="">Select...</option>
                                  <c:forEach items="${crmBean.conferenceRoomList }" var="conferenceRoom">
                                  <option value="${conferenceRoom.id }">${conferenceRoom.roomCode}</option>
                                  
                                  </c:forEach>
                                  
                              </select>       
                                                    </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5" >
                                <label >No. Of Participant </label>
                            </div>
                           <div class="col-sm-7">
                          <input type ="text" placeholder="No. Of Participants" class="form-control autoFilled" id="noOfParticipants" name="noOfParticipants">
                              
                            </div>
                        </div>
                    </div>
                    
                   
                </div>
                <br>
            <!-- ==================Row 3=============== -->
                <div class="row">
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Start DateTime <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
									<div class="input-group date form_datetime-component">
                                            <input type="text" class="form-control" readonly="" size="16" name="startDateTime" id="startDateTime"  placeholder="Start Date-Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>
									<label class="error" for="startDateTime" generated="true"></label>
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>End Date-Time<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
									<div class="input-group date form_datetime-component">
                                            <input type="text" class="form-control" readonly="" size="16" name="endDateTime" id="endDateTime"  placeholder="End Date-Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>
									<label class="error" for="endDateTime" generated="true"></label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label> Required IT Support / F&B</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="radio" value="Yes" class="form-control autoFilled" id="isRequiredItSupportYes" name="isRequiredItSupport"><label>Yes</label>
                              <input type ="radio" value="No" class="form-control autoFilled" id="isRequiredItSupportNo" name="isRequiredItSupport"><label>No</label>
                              
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <br>
            <!-- ======================End Row @ 3==================== -->
            <!-- ==================Row start @4=============== -->
    	
    	<!--start of booking for external modal  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="bookingForExternalModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Booking For-External</h4>
                                    </div>
                                    <div class="modal-body">

                                        
                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Pax Name<span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <input class="form-control" id="externalPaxName" name="externalPaxName" />
                              						

                                                </div>
                                            </div>


                                           
                                                 <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                                                </div>
                                            </div>
                                
                                    </div>

                                </div>
                            </div>
                        </div>

<!-- end of modal -->
    	          
	
            <!-- ======================End Row @ 4==================== -->
                    
                
                
                <br>
                
           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center><button class="btn btn-info "  name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            
</form>
          
            <!-- ======================Dynamic table Start here ====== -->
                       
                                                        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           USER VIEW
            <span class="tools pull-right">
                      </span>
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
                        <table class="display table table-bordered "
											id="hidden-table-info">
                <thead class="cf">
        <tr>
            <th>Request ID</th>
            <th>Requester</th>
            
            <th class="hidden-phone">Booking For</th>
            <th class="hidden-phone">Request Date</th>
            <th class="hidden-phone">Status</th>
            <th>Action</th>
            
        
												</tr>
        </thead>
        <tbody>
       <c:forEach  items="${crmBean.USERcrmDataList}" var="valueMap">

        <tr class="gradeX" id="rid${valueMap['requestID']}">
         <td><a class="showRequestDataModal"href="javascript:;"  onclick="callShowRequestData(<c:out value="${valueMap['requestID']}">
         </c:out>,'<c:out value="${valueMap['service']}"/>')"><c:out value="${valueMap['requestID']}"></c:out></a></td>
         <td  class="hidden-phone"><c:out value="${valueMap['requester']}"></c:out></td>
       
         
        <td  class="center  hidden-phone"><c:out value="${valueMap['requestType']}"></c:out></td>
        <td  class="center  hidden-phone"><c:out value="${valueMap['requestdate']}"></c:out></td>
        <td  class="center  hidden-phone">
        <c:choose>
		<c:when test="${valueMap['requestStatus'] =='COMPLETE'}">
		<a class="showResponseDataModal" href="javascript:;"
		onclick="callShowResponseData('<c:out value="${valueMap['requestID']}"></c:out>','<c:out value="${valueMap['service']}" />')"><c:out
		value="${valueMap['requestStatus']}" /></a>
		</c:when>
		<c:when test="${valueMap['requestStatus'] =='CLOSED'}">
				<a class="showResponseDataModal" href="javascript:;"
	onclick="callShowResponseMsg('<c:out value="${valueMap['requestID']}"></c:out>')"><c:out value="${valueMap['requestStatus']}" /></a>
</c:when>   
        <c:otherwise>
	   <c:out value="${valueMap['requestStatus']}" />
		</c:otherwise>
		</c:choose>
        
        
        </td>

            <td>
<button class="btn btn-info <c:if test="${valueMap['isComplete']=='true' || valueMap['requestStatus']=='CANCELLED' || valueMap['complete']==true }"> <c:out value="disabled"/></c:if>" type="button" name="action" value="CANCEL" onclick="callCancelRequest('${valueMap['requestID']}')"> 
	<i class="fa fa-hand-o-right" ></i> Cancel</button>

</td>
	 
        </tr>
       </c:forEach>

        
        
                        </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div>
        
        
                               



             <!--==========Tab End Here==========-->
    </div>

                                                                                                      
<!--===========End Hotel / Guest House Request Form====-->    

<!-- ==confirmation Requests Tab start Here == -->


                <!-- ==confirmation Requests Tab End Here == -->
        
            <!-- Travel Admin Requests Start Here -->
           

            <!-- Travel Request End Here -->


    </div>
    <!--<div class="tab-pane" id="contact2">Contact</div>
    </div> -->
    
    
    </div>
    </section>
    <!-- Next form Fields E- Attachment  -->
    <!-- ===== Dyanmic Table Start Here ====== -->


    <!-- ===== Dynamic Table End Here ===== -->
    </div>  
      
      
    <!-- page heading end-->
        
        
        <!-- validation -->
        <!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body">
        
        <div class="panel-body">
                                        <div class="adv-table">
                                        <div
									id="hidden-table-info_wrapper"
									class="dataTables_wrapper form-inline" role="grid">
                                <!-- =====Passenger Confirmation Details Start==== -->
                                <div class="table">
                                <div class="row" id="txtRequestData"></div>
                                       <div class="row">
                                                  <div class="col-md-4">
												<label for="t_type">Booking Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalBookingType"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Booking For&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalBookingFor"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Guest Type&nbsp;&nbsp;</label><span
													id="modalGuestType"></span>
											</div>
                                         </div>
                                        <div class="row">
                                                  <div class="col-md-4">
												<label for="t_type">Employee ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalEmployeeID"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Mobile NO.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalMobileNo"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Checkin Date Time&nbsp;&nbsp;</label><span
													id="modalCheckinDateTime"></span>
											</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
												<label for="w_id">Checkout Date Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalCheckoutDateTime"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Hotel Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalHotelLocation"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="w_id">Avail Cab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalAvailCab"></span>
											</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
												<label for="w_id">Pickup Point&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalPickupPoint"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="w_id">Drop Point &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalDropPoint"></span>
											</div>
                                                   <div class="col-md-4">
												<label for="w_id">Cost Centre&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalCostCentre"></span>
											</div>
                                         
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
												<label for="w_id">Approved Attachment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span></span>
											</div>
                                                
                                         </div>
                                         
                                    </div>
                                    </div>
                                    </div>
                                    </div>
        
        
        
        </div>
        <div class="modal-footer"  style="margin-top: 260px; position: relative;">
          <center>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">CLOSE</button>
						</center>
        </div>
      </div>
      
    </div></div>
 
  
<!-- end of modal -->

        
         <!-- Start Of  Response Message (CLOSED) Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseMsgModal" role="dialog">
    <div class="modal-dialog">
  
  <!-- <form action="DownloadPDFServlet" method="POST"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Response Message </h4>
        </div>
        <div class="modal-body">
  
         </div>
       
       
        <div class="modal-footer" style="margin-top: 10px; position: relative;">
<center>								
<button class="btn btn-warning " type="submit"  data-dismiss="modal"> <i class="fa fa-hand-o-right"></i> Ok</button>
</center>
        </div>
    
      </div>
    </form>
    </div>
    
  </div>

<!-- End Of Response Status Modal -->

        
        
    </jsp:body>
</t:layout>