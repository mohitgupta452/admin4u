
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:form-layout>
	<jsp:attribute name="header">
<style>
.labelcolor {
	color: red;
}

#requestDataModal .modal-dialog  
{
width:60%;
margin: 10px auto!important;
}

#requestDataModal .modal-body 
{
    padding : 0px!important;
    line-height : 16px !important;
    font-size : 12px !important;
    margin-left:50px;
    margin-top:20px;
    color:#000000 !important;

} 

#responseDataModal .modal-dialog  
{
width:61%;
margin: 10px auto!important;
}
#responseDataModal .modal-body
{
    padding : 15px!important;
    line-height : 16px !important;
    font-size : 12px !important;
    margin-left:50px;
    margin-top:20px;
    color:#000000 !important;

} 

.form-group label {
	color: #37474F !important;
	font-weight: 600 !important;
	padding-top: 6px !important;
	font-family: inherit;
	font-size: 13px;
}

.t-tabs {
	background: #78909C;
	border-radius: 48px;
	text-align: center;
}

.t-tabs:hover {
	background-color: #37474F !important;
}

.nav-tabs li {
	margin-left: 65px;
}

.nav-tabs li:hover {
	background: gray;
	border-radius: 48px;
	text-align: center;
}

.tab-content {
	background-color: #fff;
	padding: 31px;
	border-top: 6px solid #90A4AE;
	-webkit-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
}

.panel-heading .nav {
	margin: 0px !important;
}

.panel-heading .nav>li.active {
	background-color: #ccc !important;
	color: #37474F !important;
}

.panel-heading .nav>li.active>a, .panel-heading .nav>li>a:hover {
	background: transparent !important;
}

.panel-heading .nav>li>a:hover {
	color: #fff !important;
}

.custom-tab.dark-tab {
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
	border-top: 1px solid #ccc;
}

.panel-body {
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
}

</style>
<!--dynamic table-->
<link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css"
			href="assets/css/bootstrap-fileupload.min.css" />
   <!--pickers css-->
 
   <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">
  
  
</jsp:attribute>
	<jsp:attribute name="extrajs">
  <script src ="assets/js/customJS/hotel_guesthouse.js"></script>
  <script src="assets/js/customJS/utility.js"></script>
<script
			src="assets/js/tourandtravel_dtable_init/air_dynamic_table_init.js"></script>
<script
			src="assets/js/tourandtravel_dtable_init/train_dynamic_table_init.js"></script>
<script
			src="assets/js/tourandtravel_dtable_init/hotel_guesthouse_dynamic_table_init.js"></script>
<script
			src="assets/js/tourandtravel_dtable_init/admin_dynamic_table_init.js"></script>
<script src ="assets/js/customJS/hotel_guesthouse.js"></script>
  
     <script src="assets/js/customJS/utility.js"></script>
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>
			<script src="assets/js/customJS/utility.js"></script>
			


<script type="text/javascript"
			src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/js/pickers-init.js"></script>

<script>
$(".opendialog-cancel").click(function () {
    $('#requestid').val($(this).data('id'));
    $('#user_modal_cancel').modal('show');
});
</script>


</jsp:attribute>

	<jsp:body>
   
   
   <input type="text" id="userName"
				value="${userSession.empData.displayName}" style="display: none;" />
<input type="text" id="userID" value="${userSession.empData.employeeID}"
				style="display: none;" />
   
     <div class="page-heading">
       <section class="panel">
                      
                      <header class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs">
                                <li class="active  col-md-3  t-tabs">
                                    <a href="#hotel_guesthouse"
					data-toggle="tab">Stay4u</a>
                                </li>
                            </ul>
                        </header>
                      
<div class="panel-body">
    <div class="tab-content">
        

<!-- ==========Form Hotel / Guest House Booking =======-->
    <div class="tab-pane active" id="hotel_guesthouse">
             <!--==========Tab Start Here========-->                    
                              
             <!--==========Row@2 ================-->
          <form class="cmxform form-horizontal adminex-form"
							enctype="multipart/form-data" action="HotelAndGuestHouseServlet"
							method="post" id="hotelGuestForm">

                <div class="row"> 
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Booking Type <span
												class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2"
												id="bookingType" name="service">
                <option value="">select...</option>
                                <option value="GUEST_HOUSE_BOOKING">Guest House Booking</option>
                                <option value="HOTEL_BOOKING">Hotel Booking</option>
                                
                              </select>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Booking For <span
												class="labelcolor">*</span></label>
                            </div>
                           <div class="col-sm-7">
                              <select class="form-control select2 bookingFor"
												id="bookingFor" name="bookingFor">
                                <option value="">Select...</option>
                	<c:forEach items="${dropDown.bookingForDropDown}"
													var="bookingForValue">
                                <option><c:out
															value="${bookingForValue}"></c:out></option>
                                </c:forEach>
                              </select>
                              <!--  -->
                               <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="hotel_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" onclick="hotelExtenalCloseBtn()" class="close" type="button">×</button>
                                        <h4 class="modal-title">hotel Booking For-External</h4>
                                    </div>
                                    <div class="modal-body">

                                        
                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Guest Type <span class="requiredField"> *</span> </label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="hotelExternalGuestType" name="hotelExternalGuestType">
                              <option value="">select..</option>
                                 <c:forEach items="${dropDown.guestTypeDropDown}" var="guestTypeValue">
                                <option><c:out value="${guestTypeValue}"></c:out></option>
                                
                               </c:forEach> 
                              </select>
<span id="hotel_guesttype_error_msg"></span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Cab Segment <span class="requiredField"> *</span> </label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="hotelExternalCabPref" name="hotelExternalCabPref">
                              
                                <option value="">select...</option>
                                 <c:forEach items="${dropDown.cabPreferenceDropDown}" var="cabPreferenceValue">
                                <option><c:out value="${cabPreferenceValue}"></c:out></option>
                                
                               </c:forEach> 
                                
                              </select>
<span id="hotel_cabpref_error_msg"></span>
                                                </div>
                                            </div>
                                                 <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button type="button" class="btn btn-primary" onclick="hotelExtenalBtn()">OK</button>
                                                </div>
                                            </div>
                                
                                    </div>

                                </div>
                            </div>
                        </div>
                            </div>
                    </div>
                    </div>
                    
<%--                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Guest Type <span
												class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2"
												id="guestType" name="guestType">
                                <option value="">Select...</option>
                                <c:forEach
													items="${dropDown.guestTypeDropDown}" var="guestTypeValue">
                                
                                <option><c:out
															value="${guestTypeValue}"></c:out></option> 
                                </c:forEach>   
                              </select>
                            </div>
                        </div>
                    </div> --%>
          <div class="col-sm-8 col-md-4">
                 <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Department <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  value="${dropDown.department}" readonly="" class="form-control" id="department" name="department" >
                            </div>
                    </div>
            </div>                    
                    
         
                </div>
                <br />
            <!-- ==================Row 3=============== -->
                <div class="row">

                    <div class="col-sm-8 col-md-4" id="removeSpan">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee ID <span
                        class="labelcolor" >*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type="text" maxlength="50" class="form-control" onblur="getEmployeeNameById(this,'guestName')"
                        id="employeeID1" name="employeeID" placeholder="Employee ID">
                             <!-- <a href="#" data-toggle="modal" data-target="#addPax" id="addP"><i class="fa fa-plus" aria-hidden="true"></i></a>  -->                        
                        
                            </div>
                             
                           <!--start of modal  -->
<!-- <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addPax" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Add Multiple Pax</h4>
            </div>
            <div class="modal-body">
<div class="paxName"> 
<div class="row">

<div class="col-sm-8 col-md-6">


<div class="form-group"> 
 <div class="col-sm-5">
        <label>Employee ID2 <span class="requiredField"> *</span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control empIdClass" id="employeeID2" name="employeeID2" placeholder="Employee ID">
    </div>
</div></div>

<div class="col-sm-8 col-md-6">
<div class="form-group"> 
    <div class="col-sm-5">
        <label>Pax Name2 <span class="requiredField"> *</span></label>
    </div>
    <div class="col-sm-7" id="paxName">
      <input type ="text" class="form-control alphabetOnly" id="passenger2" name="passenger2" placeholder="Pax Name">
</div></div></div>





</div></div>

<br>                                
<div class="form-group">
<div class="col-sm-5">
                            <button type="button" class="btn btn-primary" onclick="addPax('empIdClass','hotel')">Add</button>
                        </div>
<div class="col-sm-5">
                            <button type="button" class="btn btn-primary isMultiRequest" id="isMultiRequest" name="isMultiRequest" value="false" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
        
            </div>

        </div>
    </div>
</div>
   <input type="hidden" class="totalPassengers" name="totalPassengers" id="TotalPax" value="2" style="display: none;">   

 --><!-- end of modal -->                    
                           
                           
                           
                            
                    </div>
                    </div>

                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label id="guestNameLabel">Pax Name <span
                               
												class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type="text" maxlength="50" class="form-control alphabetOnly"

												id="guestName" name="guestName" placeholder="Pax Name">
 <a href="#" data-toggle="modal" data-target="#addPax" id="addP"><i class="fa fa-plus" aria-hidden="true">Add Pax</i></a> 
                            </div>
   <!-- modal starts -->                         
              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addPax" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Add Multiple Pax</h4>
            </div>
            <div class="modal-body">
<div class="paxName"> 
<div class="row">
<!-- 
<div class="col-sm-8 col-md-6">


<div class="form-group"> 
 <div class="col-sm-5">
        <label>Employee ID2 <span class="requiredField"> *</span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control empIdClass" id="employeeID2" name="employeeID2" placeholder="Employee ID">
    </div>
</div></div> -->

<div class="col-sm-8 col-md-10">
<div class="form-group"> 
    <div class="col-sm-5">
        <label>Pax Name2 <!-- <span class="requiredField"> *</span> --></label>
    </div>
    <div class="col-sm-7" id="paxName">
      <input type ="text" maxlength="50" class="form-control alphabetOnly" id="passenger2" name="passenger2" placeholder="Pax Name">
<span id="error_pax2"></span>
</div></div></div>

</div>
<br/>

</div>

<br>  
                              
<div class="form-group">
<div class="col-sm-5">
                            <button type="button" class="btn btn-primary" onclick="addPax('empIdClass','hotel')">Add</button>
                        </div>
<div class="col-sm-5">
                            <button type="button" class="btn btn-primary isMultiRequest" id="isMultiRequest" name="isMultiRequest" value="false" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
        
            </div>

        </div>
    </div>
</div>
   <input type="hidden" class="totalPassengers" name="totalPassengers" id="TotalPax" value="2" style="display: none;">   

  <!-- modal ends -->                          
                            
                            
                        </div>
                    </div>
                    
                  
            <!--  -->
            
         <div class="col-sm-8 col-md-4" >

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Landline &nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control numbersOnly" id="hotelLandline" name="hotelLandline" maxlength="14" placeholder="Landline">
      <p class="help-block">Ex. 1662245635</p>
        
      
    </div>
</div>


</div>      
                  
                </div>
                <br />
            <!-- ======================End Row @ 3==================== -->
            <!-- ==================Row start @4=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Checkin Date & Time<span
												class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                            			<div class="input-group date checkIn_form_datetime-component">
                                            <input type="text"
													class="form-control" readonly="" size="16"
													name="checkinDateTime" id="checkinDateTime" placeholder="CheckIn Date & Time">
                                                            <span
													class="input-group-btn">
                                                            <button
														type="button" class="btn btn-primary date-set">
														<i class="fa fa-calendar"></i>
													</button>
                                                            </span>
                                        </div>					
												
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Checkout Date / Time <span
												class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                             
				 <div class="input-group date checkOut_form_datetime-component">
                                            <input type="text"
													class="form-control" readonly="" size="16"
													name="checkoutDateTime" id="checkoutDateTime" placeholder="CheckOut Date & Time">
                                                            <span
													class="input-group-btn">
                                                            <button
														type="button" class="btn btn-primary date-set">
														<i class="fa fa-calendar"></i>
													</button>
                                                            </span>
                                        </div>								
												
												
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4" id="hotelDiv">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Location <span
												class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7" id="selectLocation">
                              <select class="form-control select2"
												id="hotelLocation"  name="hotelLocationOld">
												                                 <option value="">Select...</option> 
												
                                <c:forEach
													items="${dropDown.hotelLocationDropDown}"
													var="hotelLocationValue">
                                
                                  <option value="${hotelLocationValue}"><c:out
															value="${hotelLocationValue}"></c:out></option>  
                                  </c:forEach> 
                                 <option value="Others">Others</option> 
                              </select>
                            </div>
                             <div class="col-sm-7" id="inputLocation" style="display:none">
                         <input type="text"  class="form-control" id="hotelLocation1" name="hotelLocationOld" placeholder="Location"/>      
                            </div>
                        </div>
                    </div>
                    
                   <div class="col-sm-8 col-md-4" id="guestHouseDiv" style="display: none;">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Location <span
												class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7" id="selectLocationGuest">
                              <select class="form-control select2"
												id="guestHouseLocation" name="hotelLocationOld">
                                <option value="">Select...</option>
                                <c:forEach
													items="${dropDown.guestHouseLocationDropDown}"
													var="hotelLocationValue1">
                                
                                  <option value="${hotelLocationValue1}"><c:out
															value="${hotelLocationValue1}"></c:out></option>  
                                  </c:forEach> 
                                  <option value="Others">Others</option>  
                              </select>
                            </div>
                             <div class="col-sm-7" id="inputLocationGuest" style="display:none">
                         <input type="text"  class="form-control" id="guestHouseLocation1" name="hotelLocationOld" placeholder="Location"/>      
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            <!-- ======================End Row @ 4==================== -->
            
            <!-- ==================Row 3=============== -->
                <div class="row">
                     
                                     
  <div class="col-sm-8 col-md-4" >
                        <div class="form-group"> 
                         <div class="col-sm-5">
                              <label>Cost Centre <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
         <select class="form-control select2" id="costCentre" name="costCenter">
                              <option value="${dropDown.companyName},${dropDown.locationName}"><c:out value="${dropDown.companyName},${dropDown.locationName}"/></option>
                                
                                <c:forEach items="${dropDown.getCompanyNameList }" var="comp">
                                <option value="${comp[0]},${comp[1]}"><c:out value="${comp[0]},${comp[1]}"/></option>
                                
                               </c:forEach>
                              </select>                    
                           </div>
                    </div>
                    </div>
                    <!-- 
         <c:if test="${userSession.empRole[0]=='ADMIN' || userSession.empRole[0]=='SPOC' }">             
                    <div class="col-sm-8 col-md-4" style="display:none">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Cost Centre Code<span
												class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
         <input value="<c:out value="${dropDown.costcenter}"></c:out>" readonly="" class="form-control" id="costCenterCode" name="costCenterCode"/> 
                               
                            </div>
                        </div>
                    </div>
                    </c:if> -->
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Mobile No <span
                        class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type="text" class="form-control numbersOnly"
                        id="mobileNo" name="mobileNo" placeholder="Mobile No" maxlength="10">
                        
                            </div>
                        </div>
                    </div>
                    
            <div class="col-sm-8 col-md-4">
                                            <div class="form-group"> 
                                                <div class="col-sm-5">
                                                    <label>Approved Attachment<!-- <span
												class="labelcolor">*</span> --></label>
                                                
                                                
                                                
                                                </div>
                                                <div class="col-sm-7"
											style="padding-left: 6px">
                                                  <div
												class="controls col-md-9">
                                                        <div
													class="fileupload fileupload-new"
													data-provides="fileupload">
													<input type="hidden">
                                                             <span
														class="btn btn-default btn-file">
                                                                <span
														class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span
														class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input
														type="file" class="default" name="approvedAttachment"
														id="approvedAttachment" >
                                                                    </span>
                                        <label class="error" genrated="true" for="approvedAttachment"></label>
                                                                    
                                                                  <span
														class="fileupload-preview" style="margin-left: 5px;"></span>
                                                                  <a
														href="#" class="close fileupload-exists"
														data-dismiss="fileupload"
														style="float: none; margin-left: 5px;"></a>
                                                        </div>
                          <div id="hotelFileName" style="color:black;">
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                 
                                    
                                    
                                    
                                  
                            </div>  </div>     
              
                    
                    <%-- <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Avail Cab <!-- <span
                        class="labelcolor">*</span> --></label>
                            </div>
                            <div class="col-sm-7">
                                <select class="form-control select2"
                        id="availCab" name="availCab">
                    <option value="">No</option>
                                    <c:forEach
                          items="${dropDown.availCabDropDown}" var="availCabValue">
                                
                                  <option><c:out
                              value="${availCabValue}"></c:out></option>  
                                  </c:forEach>  
                                </select>
                            </div>
                        </div>
                    </div> --%>
               
                    
            <!-- ======================End Row @ 3==================== -->
             <!-- ==================Row 3=============== -->
                     
                 <!-- ======Row avail Start======= -->
                            <div class="row" id="avail_cab_row">
           <div class="col-sm-8 col-md-4" id="pickupdateShow">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PickUp Date & Time <span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
									<div class="input-group date form_datetime-component">
                                            <input type="text" class="form-control" readonly="" size="16" name="pickupDateTime" id="pickupDateTime"  placeholder="PickUp Date & Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>
									<label class="error" for="pick4uPickupDateTime" generated="true"></label>
                                
                            </div>
                        </div>
                    </div>                  
			
              <div class="col-sm-8 col-md-4" id="pickUpShow">
                                        <div class="form-group"> 
                                            <div class="col-sm-5">
                                                <label>Pickup Point<span
												class="requiredField"> *</span></label>
                                            </div>
                                            <div class="col-sm-7 ">
                                              <input type="text"
												class="form-control" id="pickupPoint " name="pickupPoint"
												placeholder="Pickup Point">
                                            </div>
                                        </div>
                                    </div>
                                    
              <div class="col-sm-8 col-md-4" id="dropShow">
                                        <div class="form-group"> 
                                         <div class="col-sm-5">
                                                <label>Drop Point<span
												class="requiredField"> *</span></label>
                                            </div>
                                            <div class="col-sm-7">
                                              <input type="text"
												class="form-control" id="dropPoint" name="dropPoint"
												placeholder="Drop Point">
                                            </div>
                                    </div>
                                    </div>
                                    
                
     </div>
     <br/>
     <div class="row">           
                <!--  <div class="col-sm-8 col-md-4">
                                            <div class="form-group"> 
                                                <div class="col-sm-5">
                                                    <label>Approved Attachment<span
												class="labelcolor">*</span></label>
                                                
                                                
                                                
                                                </div>
                                                <div class="col-sm-7"
											style="padding-left: 6px">
                                                  <div
												class="controls col-md-9">
                                                        <div
													class="fileupload fileupload-new"
													data-provides="fileupload">
													<input type="hidden">
                                                             <span
														class="btn btn-default btn-file">
                                                                <span
														class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span
														class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input
														type="file" class="default" name="approvedAttachment"
														id="approvedAttachment" >
                                                                    </span>
                                        <label class="error" genrated="true" for="approvedAttachment"></label>
                                                                    
                                                                  <span
														class="fileupload-preview" style="margin-left: 5px;"></span>
                                                                  <a
														href="#" class="close fileupload-exists"
														data-dismiss="fileupload"
														style="float: none; margin-left: 5px;"></a>
                                                        </div>
                          <div id="hotelFileName" style="color:black;">
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                 
                                    
                                    
                                    
                                  
                            </div> -->
            </div>
<br/><br/>
                <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center>
											<button class="btn btn-info " type="submit" id="submit12"
												name="action" value="GENERATED">
												<i class="fa fa-hand-o-right"></i> Submit</button>
										</center>
                        </div>
                    </div>
                </div>
</form>
          
            <!-- ======================Dynamic table Start here ====== -->
                       
                              <!--  <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           USER VIEW
            <span class="tools pull-right">
                      </span>
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
                        <table class="display table table-bordered "
											id="hotel_hidden-table-info">
                <thead class="cf">
        <tr>
            <th>WorkOrder</th>
            <th>Requester</th>
            
            <th class="hidden-phone">Booking For</th>
            <th class="hidden-phone">Request Date</th>
            <th class="hidden-phone">Status</th>
            <th>Action</th>
            <th>download</th>
        
												</tr>
        </thead>
        <tbody>
        <c:forEach  items="${hng.USERhngDataList}" var="valueMap">

        <tr class="gradeX" id="rid${valueMap['requestID']}">
         <td><a class="showRequestDataModal"href="javascript:;"  onclick="callShowRequestData(<c:out value="${valueMap['requestID']}">
         </c:out>,'<c:out value="${valueMap['service']}"/>')">
            <c:choose>
    <c:when test="${not empty valueMap['workorder']}">
        <c:out value="${valueMap['workorder']}"></c:out>
    </c:when>    
    <c:otherwise>
    <c:out value="${valueMap['requestID']}"></c:out>
    </c:otherwise>
       </c:choose>
       </a>
         </td>
         <td  class="hidden-phone"><c:out value="${valueMap['requester']}"></c:out></td>
       
         
        <td  class="center  hidden-phone"><c:out value="${valueMap['requestType']}"></c:out></td>
        <td  class="center  hidden-phone"><c:out value="${valueMap['requestdate']}"></c:out></td>
        <td  class="center  hidden-phone">
        <c:choose>
		<c:when test="${valueMap['requestStatus'] =='COMPLETE'}">
		<a class="showResponseDataModal" href="javascript:;" name="showResponseBtn"
		onclick="callShowResponseData('<c:out value="${valueMap['requestID']}"></c:out>','<c:out value="${valueMap['service']}" />',name)"><c:out
		value="${valueMap['requestStatus']}" /></a>
		</c:when>
		<c:when test="${valueMap['requestStatus'] =='CLOSED'}">
				<a class="showResponseDataModal" href="javascript:;"
	onclick="callShowResponseMsg('<c:out value="${valueMap['requestID']}"></c:out>')"><c:out value="${valueMap['requestStatus']}" /></a>
</c:when>   
        <c:otherwise>
	   <c:out value="${valueMap['requestStatus']}" />
		</c:otherwise>
		</c:choose>
        
        
        </td>

            <td>
<button class="btn btn-info opendialog-cancel<c:if test="${valueMap['isComplete']=='true' || valueMap['requestStatus']=='CANCELLED' || valueMap['complete']==true||valueMap['isActive']==true }"> <c:out value="disabled"/></c:if>" type="button" name="action" value="CANCEL" data-toggle="modal" data-id="${valueMap['requestID']}"> 
	<i class="fa fa-hand-o-right" ></i> Cancel</button>

</td>
	 <td><a class="<c:if test="${valueMap['requestStatus']!='COMPLETE'}"><c:out value="disableAnchor"/></c:if>"
											href="DownloadResponseServlet?fileName=${valueMap['responseAttachment']}&requestId=${valueMap['requestID']}"><button
													class="btn btn-warning 
	 <c:if test="${valueMap['requestStatus']!='CLOSED'}"> <c:out value="disabled "/></c:if>"
													type="button"> 
															<i class="fa fa-hand-o-right"></i> Download</button></a>
		
		</td>
	
        </tr>
       </c:forEach>
                </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div>
        
        
 -->

             <!--==========Data Table End Here==========-->
    </div>

                                                                                                      

                 <div class="tab-pane" id="admin_travel_requests">
                                    <!-- Form for Ticket Requests -->
                                  <div id="preapprove"
							style="width: 100%">

                                    </div>
                                    <br>
        <!-- ==== Dynamic table Start here ===== -->
           

 <!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="admin_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <table>
        <tr>
												<td>
          <label>Message: &nbsp;  </label>
												</td>
												<td style="margin-top: 10px;"><textarea rows="2"
														cols="30">Message area</textarea></td>
          </tr>
</table>

        </div>
        <div class="modal-footer">
          <center>
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Save</button>
										</center>
        </div>
      </div>
      
    </div>
  </div>
  


<!-- end of modal -->



<!-- start of modal -->
                        
 

                    
                     
            <!-- Travel Request End Here -->

    </div>
    
    </div>
        <div class="wrapper">
        </div>

       </section>
		</div>
	
	<!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body" style=" overflow :auto;">
        
        <div class="panel-body">
                                        <div class="adv-table">
                                        <div
									id="hidden-table-info_wrapper"
									class="dataTables_wrapper form-inline" role="grid">
                                <!-- =====Passenger Confirmation Details Start==== -->
                                <div class="table">
                                <div class="row" id="txtRequestData"></div>
                                       <div class="row">
                                                  <div class="col-md-4">
												<label for="t_type">Booking Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalBookingType"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Booking For&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalBookingFor"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Guest Type&nbsp;&nbsp;</label><span
													id="modalGuestType"></span>
											</div>
                                         </div>
                                        <div class="row">
                                                  <div class="col-md-4">
												<label for="t_type">Employee ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalEmployeeID"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Mobile NO.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalMobileNo"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Checkin Date Time&nbsp;&nbsp;</label><span
													id="modalCheckinDateTime"></span>
											</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
												<label for="w_id">Checkout Date Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalCheckoutDateTime"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Hotel Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalHotelLocation"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="w_id">Avail Cab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalAvailCab"></span>
											</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
												<label for="w_id">Pickup Point&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalPickupPoint"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="w_id">Drop Point &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalDropPoint"></span>
											</div>
                                                   <div class="col-md-4">
												<label for="w_id">Cost Centre&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalCostCentre"></span>
											</div>
                                         
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
												<label for="w_id">Approved Attachment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span></span>
											</div>
                                                
                                         </div>
                                         
                                    </div>
                                    </div>
                                    </div>
                                    </div>
        
        
        
        </div>
        <div class="modal-footer sticky-footer"  style="margin-top:0px !important; position: relative;">
          <center>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">CLOSE</button>
						</center>
        </div>
      </div>
      
    </div></div>
 
  
<!-- end of modal -->







<!-- Start Of  Response Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
  
  <form action="DownloadPDFServlet" method="POST" id="generatePdfFrm">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Details</h4>
        </div>
        <div class="modal-body">
  
         </div>
       
       
        <div class="modal-footer" style="margin-top: 70px; position: relative;">
<center>								
</center>
        </div>
    
      </div>
    </form>
    </div>
    
  </div>
 



  <!-- Start Of  Response Message (CLOSED) Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseMsgModal" role="dialog">
    <div class="modal-dialog">
  
  <!-- <form action="DownloadPDFServlet" method="POST"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Response Message </h4>
        </div>
        <div class="modal-body">
  
         </div>
       
       
        <div class="modal-footer" style="margin-top: 10px; position: relative;">
<center>								
<button class="btn btn-warning " type="submit"  data-dismiss="modal"> <i class="fa fa-hand-o-right"></i> Ok</button>
</center>
        </div>
    
      </div>
    </form>
    </div>
    
  </div>

<!-- End Of Response Status Modal -->
	<!-- =====================================End ofshow request data  modal================= -->
 
 <!--======== datetimepicker modal for checkin checkout Starts ======= -->
 
        <div class="modal fade" id="checkInCheckOutModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
                 <center><h4><b> 24:00 Hrs prior intimation is required for a successful booking<br/><br/>
                          Request will processed , Please wait for the confirmation'  </b></h4></center>
        </div>
        <br>
        <div class="modal-footer" style="position:relative; margin-top:0px !important">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 
  <!-- ======= datetimepicker modal for checkin checkout ends ====== -->  
 
 <!-- modal for location  starts --> 
   <div class="modal fade" id="locationModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Location</h4>
        </div>
        <div class="modal-body">
                 <center><h5>Do you want to enter the name manually ?</h5></center>
        </div>
        <br>
        <div class="modal-footer" style="position:relative; margin-top:0px !important">
          <!-- <button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterLocation()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button> -->
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for location  ends -->  
 
  <!-- modal for location  starts --> 
   <div class="modal fade" id="selectLocationModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Location</h4>
        </div>
        <div class="modal-body">
                 <center><h5>Do you want to select the location name from dropdown ?</h5></center>
        </div>
        <br>
        <div class="modal-footer" style="position:relative; margin-top:0px !important">
          <!-- <button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="selectLocation()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button> -->
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for location  ends -->  
 
 
 <!-- modal for Cancel  starts --> 
   <div class="modal fade" id="user_modal_cancel" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cancel Request</h4>
        </div>
        <div class="modal-body">
                <input type="hidden" name="requestId" id="requestid" class="user"/>
                 <center><h5><b>Are you sure you want to cancel it ?</b></h5></center>
        </div>
        <br>
        <div class="modal-footer" style="position:relative; margin-top:0px !important">
          <button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="callCancelRequest()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button> 
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for  cancel ends --> 
 
 <!-- modal for Internal BookingFor  starts --> 
   <div class="modal fade" id="internal_booking_modal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Internal Booking</h4>
        </div>
        <div class="modal-body">
                <input type="hidden" name="requestId" id="requestid" class="user"/>
                <center><h5><b>You can only request for your reporting manager</b></h5></center>
        </div>
        <br>
        <div class="modal-footer" style="position:relative; margin-top:0px !important">
          <button type="button" class="btn btn-danger"  data-dismiss="modal">Ok</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">No</button>  -->
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for Internal BookingFor  ends -->  

  
    </jsp:body>
</t:form-layout>
