<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:layout>

	<jsp:attribute name="header">
	 <link rel="stylesheet" type="text/css"
			href="assets/css/accordion.css">
</jsp:attribute>
  <jsp:attribute name="extrajs">
     <script src ="assets/js/customJS/worklocation.js"></script>
   </jsp:attribute>
	<jsp:body>
	<button class="accordion">DELETE LOCATION</button>
	<div class="panel">
 <form name="myform1" action="worklocation" id="myform1">
  <div class="page-heading">
       <section class="panel">
       <div class="panel-body">
<div class="row">
               <div class="col-sm-8 col-md-6">
                        <div class="form-group"> 
            
                             <div class="col-sm-7">
                              <select class="form-control" id="location" name="location">
                               <c:forEach items="${lobn.locationName}" var="wln">
                                <option><c:out value="${wln}"></c:out></option>
                                
                               </c:forEach> 

                                
                                
                              </select>
                              </div>
                    </div>
                     </div>
                                                  
     
</div>

</div>
  </section>
 </div>
<div class="page-heading">
       <div class="panel-body">
  
<div class="row">

               <div class="col-sm-8 col-md-6">
                        <div class="form-group"> 
            
                             <div class="col-sm-7">
                              
                              </div>
                    </div>
                    <div id="worklcn">
                    <c:forEach items="${lobn.workLocationByName}" var="wrln">
                    <label>
      <input type="checkbox" name="worklocation" id="worklocation" value="<c:out value="${wrln}"></c:out>" checked><c:out value="${wrln}"></c:out>
    </label>
    </c:forEach>
    </div>
       </div>
       <input type="hidden" name="action" value="DELETEWORKLOCATION">

</div>

</div>
 </div> 
      <div class="row" style="margin-left: 20px">                                     

<button class="btn btn-info " type="submit" id="submit" name="action" value="DELETEWORKLOCATION"><i class="fa fa-hand-o-right"></i>SUBMIT</button></br>
</div>
  </form>
  </div>
  
  
  <button class="accordion">ADD LOCATION</button>
<div class="panel">
 <form name="myForm2" action="" id="myForm2">
 
 <div class="page-heading">
       <section class="panel">
       <div class="panel-body">
       <div class="col-md-12">
<label>ADD LOCATION</label>
</div>
<div class="row">
               <div class="col-sm-8 col-md-6">
                        <div class="form-group"> 
            
                             <div class="col-sm-5">
                              <select class="form-control" id="locationName" name="locationName">
                               <c:forEach items="${lobn.locationName}" var="wln">
                                <option><c:out value="${wln}"></c:out></option>
                                
                               </c:forEach> 

                                
                                
                              </select>
                              </div>
                    </div>
                     </div>
                 <div class="form-group">
                  <div class="col-sm-3">
                  
                  <input type="text" class="form-control" id="worklocationName" name="worklocationName" placeholder="work location"/>
                  </div>
      <button class="btn btn-info " type="submit" id="submit" name="action" value="CREATEWORKLOCATION"><i class="fa fa-hand-o-right"></i> ADDWORKLOCATION</button>            
                  </div>                                
    
</div>

</div>
  </section>
 </div>
</form>
</div>
<button class="accordion">EDIT WORKLOCATION</button>
<div class="panel">
 <form name="myForm3" action="worklocation" id="myForm3">
 
 <div class="page-heading">
       <section class="panel">
       <div class="panel-body">
       <div class="col-md-12">
<label>EDIT WORKLOCATION</label>
</div>
<div class="row">
               <div class="col-sm-8 col-md-6">
                        <div class="form-group"> 
            
                             <div class="col-sm-5">
                              <select class="form-control" id="wkn" name="worklocationName">
                               <c:forEach items="${lobn.workLocationName}" var="wln">
                                <option><c:out value="${wln}"></c:out></option>
                                
                               </c:forEach> 

                                
                                
                              </select>
                              </div>
                    </div>
                     </div>
                 <div class="form-group">
                  <div class="col-sm-3">
                  
                  <input type="text" class="form-control" id="newworklocationName" name="newworklocationName" placeholder="work location"/>
                  </div>
      <button class="btn btn-info " type="submit" id="submit" name="action" value="EDITLOCATION"><i class="fa fa-hand-o-right"></i>UPDATELOCATION</button>            
                  </div>                                
    
</div>

</div>
  </section>
 </div>
</form>
</div>
</jsp:body>
</t:layout>