<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:admin-layout>

	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
    
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    
    <!--dynamic table-->
    <script type="text/javascript" language="javascript"src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"	src="assets/js/data-tables/DT_bootstrap.js"></script>
<script		src="assets/js/dynamic_table_init.js"></script>
    
    
    
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
 
    <script src="assets/js/customJS/repair_validation.js"></script>
        <script src="assets/js/customJS/tatService.js"></script>
    
<script src="assets/js/customJS/utility.js"></script>

<script>

$(document).ready(function(){

	$('#level1TatTime').select2().select2("readonly", true);
});
</script>
    </jsp:attribute>
    <jsp:body>
            <!-- Tat Service Start -->

             <div class="wrapper">
           <!--  Body contents goes here -->
<section class="panel">
<header class="panel-heading custom-tab dark-tab">Service Turn Around Time
</header>
    
<form action="TatServlet" method="post" id="tatServiceManagement" novalidate= "novalidate"  class="cmxform form-horizontal adminex-form" style="padding:30px;">
 
 <div class="row"> 
 <div class="col-sm-8 col-md-4">
      <div class="form-group"> 
        <div class="col-sm-5">
                <label>Turn Around Time</label>
          </div>
           <div class="col-sm-7">
            <select class="form-control select2" id="tatDuration" onclick="getpriority(this.value);" name="tatDuration">
              </select>            
                </div>
           </div>
</div>
 <div class="col-sm-8 col-md-4">
      <div class="form-group"> 
        <div class="col-sm-5">
                <label>Priority</label>
          </div>
           <div class="col-sm-7">
                      <input type="text"  class="form-control"name="priority"  id="tatServices" placeholder="Auto Priority"
                                                >
                </div>
           </div>
</div> 
<div class="col-sm-8 col-md-4">
      <div class="form-group"> 
        <div class="col-sm-1">
                <label>Service</label>
          </div>
           <div class="col-sm-11">
            <select class="form-control select2" id="tatService" name="tatService">
               <option value="">select...</option>
                      <c:forEach items="${tatservicebean.services}" var="service">

                        <option >${service}</option>
                        </c:forEach>
              </select>            
                </div>
           </div>
</div>
</div>
<div class="row">
<div class="col-sm-12">
     <div class="form-group">
        <table class="display table table-bordered mybody">
        <thead class ="cf">
            <tr class="mytd">
                <th>Escalation level</th>
                <th>Level 1</th>
                <th>Level 2</th>
                <th>Level 3</th>
                <th>Level 4</th>
            </tr>

        </thead>
        <tbody class="cb">
            <tr class="mytd">
                <td>Select TAT</td>
                <td><input type="text" id ="level1TatTime" name="level1TatTime" value="" readonly=""></td>
                <td><select class="form-control select2" id = "level2TatTime" name="level2TatTime"><option>Choose TAT</option></select></td>
                <td><select class="form-control select2" id = "level3TatTime" name="level3TatTime"><option>Choose TAT</option></select></td>
                <td><select class="form-control select2" id = "level4TatTime"name="level4TatTime"><option>Choose TAT</option></select></td>
            </tr>
        </tbody>
        
    </table>
    </div>
</div>
    
</div><div class="row">
<label>spocComment</label><input type="text" name="spocComment" value=""/>
</div>
<div class="row">
<div class="col-sm-8 col-md-4"></div>
<div class="col-sm-8 col-md-4">
      <div class="form-group"> 
            <button type="submit" name="action"  value="SAVEDATA" class="btn btn-info"  ><i class="fa fa-hand-o-right"></i>Done</button>          
           </div>
</div>
<div class="col-sm-8 col-md-4"></div>
</div>

            </form>
</section>                
                           
<!-- =========Modal code=========== -->




<!-- ==========End Modal==================== -->

 <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           TAT Service  History
           
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
                id="tatService-table-info">
        <thead class="cf">
        <tr>
            <th>TAT <code>In Hours</code></th>
            <th class="">Priority</th>
            <th class="">Services</th>
            <th class="">Level 1 <code>In Hours</code></th>
            <th class="">Level 2 <code>In Hours</code></th>
            <th class="">Level 3 <code>In Hours</code></th>
            <th class="">Level 4 <code>In Hours</code></th>
            <th>Action</th>
        </tr>
        </thead>
               

        <tbody>

             <c:forEach items="${tatservicebean.tatServiceList}" var="tatB">
        <tr class="gradeX" id="">
            <td>${tatB['level1']}</td>
            <td>${tatB['priority']}</td>
            <td>${tatB['service']}</td>
            <td>${tatB['level1']}</td>
            <td>${tatB['level2']}</td>
            <td>${tatB['level3']}</td>
            <td>${tatB['level4']}</td>
            
            <td><a class=""><button class="btn btn-warning deleteTat"  value="${tatB['id']}" name="action" type="button"><i class="fa fa-hand-o-right"></i>Remove</button></a></td>
        
        </tr>
        </c:forEach>
         
                  
         
                </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div>
          
          
          
 




        </div>
        <!--body wrapper end-->
            <!--Tat End Service -->
    </jsp:body>
</t:admin-layout>