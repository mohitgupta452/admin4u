<div class="tab-pane " id="contact_info">
             
  <form action="ContactInformationServlet" method="POST" id="contact_form" novalidate="novalidate" class="cmxform form-horizontal adminex-form"  >
             <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-12">
                                <label ><b>Vendor Contact Person Information</b></label>
                            </div>
                        </div>
                    </div>
                </div>
                </br>
                <div class="row">
                         <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Vendor Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2" id="companyName" name="companyName">
                                  <option value="">select Company</option>
                                   <option value="company">Company</option>
                              </select>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Salutation <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  placeholder="Salutation" class="form-control" id="salutation" name="salutation">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>First Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  placeholder="First Name" class="form-control" id="firstName" name="firstName">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5" >
                                <label >Last Name <span class="requiredField"> *</span></label>
                            </div>
                           <div class="col-sm-7">
                              <input type ="longtext"  placeholder="Last Name" class="form-control" id="lastName" name="lastName">
                            </div>
                        </div>
                    </div>
                    
                   
                </div>
                <br>
            <!-- ==================Row 3=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Designation <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" placeholder="Designation" class="form-control" id="designation" name="designation">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Department <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Department" class="form-control" id="companyDepart" name="companyDepart">
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Email <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" placeholder="Email" class="form-control" id="comEmail" name="comEmail">
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            <!-- ======================End Row @ 3==================== -->
            
             <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>State of Incorporation <span class="requiredField"> *</span> </label>
                            </div>
                            <div class="col-sm-7">
                               <input class="form-control" type="text" placeholder="State Incorporation" id ="contactStateIncorporation" name="contactStateIncorporation">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Year of Incorporation <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                <input class="form-control" type="text" placeholder="Year Incorporation"  id="contactIncorporationYear" name="contactIncorporationYear">
                            </div>
                    </div>
                    </div>
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Preferred Delivery Method <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                <input class="form-control" type="text" placeholder="Delivery method" id = "deliveryMethod" name="deliveryMethod">
                            </div>
                    </div>
                    </div>

                    
                   <!--  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Reporting Manager</label>
                            </div>
                            <div class="col-sm-7">
                              <input class="form-control" type="text" placeholder="Work Location" >
                            </div>
                        </div>
                    </div> -->
                </div>
                <br>
                <!-- ==================Row start @4=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Contact No <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                               <input class="form-control numberOnly" type="text" placeholder="Contact" id="contactNo" name="contactNo">
                            </div>
                        </div>
                    </div>
                    </div>

            <!-- ======================End Row @ 4==================== -->
               
           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center><button class="btn btn-info " type="submit" name="action" value="action"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            

      </form>    
