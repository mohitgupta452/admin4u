 <div class="tab-pane " id="payment_info">
             <!--==========Tab Start Here========-->                    
 <form action="PaymentTermsServlet" method="POST" id="payment_form" novalidate="novalidate" class="cmxform form-horizontal adminex-form" >
                              
             <!--==========Row@2 ================-->
             <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-12">
                                <label ><b>Vendor Payment Information</b></label>
                            </div>
                        </div>
                    </div>
                </div>
                </br>
                <div class="row">
                         <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Vendor Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2" name="paymentCompanyName" id="paymentCompanyName">
                                  <option value="">select Company</option>
                                  <option value="company">Company1</option>
                              </select>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Shipping Method<span class="requiredField"> *</span> </label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  placeholder="Ship Method" class="form-control" id="shippingMethod" name="shippingMethod">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Shipping Terms <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  placeholder="Shipping" class="form-control" id="shippingTerms" name="shippingTerms">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5" >
                                <label >Penality Terms <span class="requiredField"> *</span></label>
                            </div>
                           <div class="col-sm-7">
                              <input type ="longtext"  placeholder="Penality Terms" class="form-control" id="penalityTerm" name="penalityTerm">
                            </div>
                        </div>
                    </div>
                    
                   
                </div>
                <br>

            <!-- ==================Row 3=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Payment Terms <span class="requiredField"> *</span></label>
                            </div>
                             <div class="col-sm-7">
                              <select class="form-control" name="paymentTerms" id="paymentTerms">
                                  <option value="">select Term</option>
                                   <option value="payment">payment</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Advance <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Advance" name="advance" class="form-control" id="advance">
                            </div>
                    </div>
                    </div>
                    
                   
                </div>
                <br>
            <!-- ======================End Row @ 3==================== -->
            
           
               
           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center><button class="btn btn-info " type="submit" name="action" value="action"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            

   </form>       
