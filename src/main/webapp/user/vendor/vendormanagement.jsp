<!-- =====Tab Pannel Start Here -->
<div class="panel-body">
    <div class="tab-content">
    <div class="tab-pane active" id="vendor_info">
    
    <form action="VendorManagementServlet" method="POST" id="vendor_form" novalidate="novalidate" class="cmxform form-horizontal adminex-form" enctype="multipart/form-data" >
    
            
										<div class="row">
										  <div class="col-sm-8 col-md-4">
										      <div class="form-group"> 
										          <div class="col-sm-12">
										              <label ><b>Vendor  Basic Information</b></label>
										          </div>
										      </div>
										  </div>
										</div>
                </br>
															<div class="row">

															 <div class="col-sm-8 col-md-4">
															    <div class="form-group"> 
															        <div class="col-sm-5">
															            <label>Company Name <span class="requiredField"> *</span></label>
															        </div>
															        <div class="col-sm-7">
															          <input type ="text"  placeholder="Vendor Name" class="form-control" id="vendorName" name="vendorName" onblur = "automatevendor();" >
															        </div>
															    </div>
															</div>
															 <div class="col-sm-8 col-md-4">
															    <div class="form-group"> 
															     <div class="col-sm-5">
															            <label>Vendor Legal Name <span class="requiredField"> *</span></label>
															        </div>
															        <div class="col-sm-7">
															          <input type ="text"  placeholder="Legal Name" class="form-control" id="vLegalName" name="vLegalName">
															        </div>
															    </div>
															</div>
															<div class="col-sm-8 col-md-4">
															    <div class="form-group"> 
															        <div class="col-sm-5" >
															            <label >Business Description <span class="requiredField"> *</span></label>
															        </div>
															       <div class="col-sm-7">
															          <input type ="text"  placeholder="Description" class="form-control" id="businessDescription" name="businessDescription">
															        </div>
															    </div>
															</div>


															</div>
                <br>
            <!-- ==================Row 3=============== -->
                <div class="row">
                    <!-- <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label> Mailing Address <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" placeholder="Mailing" class="form-control" id="mailingAddress" name="mailingAddress">
                            </div>
                        </div>
                    </div> -->
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Company Phone <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Phone" class="form-control numbersOnly" id="companyPhone" name="companyPhone" maxlength="10">
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Company Fax <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" placeholder="Company Fax" class="form-control numberOnly" id="companyFax" name="companyFax">
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            <!-- ======================End Row @ 3==================== -->
            <!-- ==================Row start @4=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Service Tax No <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                               <input class="form-control numberOnly" type="text" placeholder="service Tax No" id="serviceTax" name="serviceTax">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Tin <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input class="form-control numberOnly" type="text" placeholder="Tin" id="tin" name="tin">   
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Identification No (EIN) or Social Security No SSN <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input class="form-control numberOnly" type="text" placeholder="Ein /SSN" id = "identificationNo" name="identificationNo">
                            </div>
                        </div>
                    </div>
                </div>

            <!-- ======================End Row @ 4==================== -->
             <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>State of Incorporation <span class="requiredField"> *</span> </label>
                            </div>
                            <div class="col-sm-7">
                               <input class="form-control" type="text" placeholder="State Of Incorporation" id ="stateIncorporation" name="stateIncorporation">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Year of Incorporation <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                <input class="form-control" type="text" placeholder="Year Of Incorporation"  id="incorporationYear" name="incorporationYear">
                            </div>
                    </div>
                    </div>
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Preferred Delivery Method <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                <input class="form-control" type="text" placeholder="Delivery method" id = "deliveryMethod" name="deliveryMethod">
                            </div>
                    </div>
                    </div>

                    
                   <!--  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Reporting Manager</label>
                            </div>
                            <div class="col-sm-7">
                              <input class="form-control" type="text" placeholder="Work Location" >
                            </div>
                        </div>
                    </div> -->
                </div>
                <br>
                <div class="row">
                     <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                
    
                                                
                                                    <label>Cancel Check<span class="requiredField"> *</span></label>
                                                    
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="cancelCheck" id="cancelCheck" >
                                                                    </span>
                                                <label class="error" for="cancelCheck" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                                                   <div id="cancelFileName" style="color:black;">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                    </div>

                    <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                
    
                                                
                                                    <label>Pan Card<span class="requiredField"> *</span></label>
                                                    
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new" ><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="panCard" id="panCard" >
                                                                    </span>
                                                <label class="error" for="panCard" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                                          <div id="panFileName" style="color:black;">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                    </div>

                 
                         <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                
    
                                                
                                                    <label>Declaration Letter<span class="requiredField"> *</span></label>
                                                    
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="decLetter" id="decLetter" >
                                                                    </span>
                                                <label class="error" for="decLetter" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                              <div id="decFileName" style="color:black;">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                    </div>
                
                </div>
                <div class="row">
                       <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                    <label>Escalation Attachment<span class="requiredField"> *</span></label>
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="escAttach" id="escAttach" >
                                                                    </span>
                                                <label class="error" for="escAttach" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                      <div id="escFileName" style="color:black;">
                                                        
                                                        </div>                     
                                                    </div>
                                                </div>
                                            </div>


  
                                    </div>

<!-- 
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Address Information <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="Address" id="addressInfo" name="addressInfo">   
                            </div>
                        </div>
                    </div> -->


                </div>
  
  <br/>
  <!-- Adreess fields -->
  <div class ="row"><div class="col-sm-12"><label><b>Address Information</b><span class="requiredField"> *</span></label></div></div>
  <br/>
  <div class="row">
          <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>City <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="City" id="city" name="city">   
                            </div>
                        </div>
                    </div> 
             <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Street <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="Street" id="street" name="street">   
                            </div>
                        </div>
                    </div> 
                    
                <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Locality <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="Locality" id="locality" name="locality">   
                            </div>
                        </div>
                    </div>  
  </div>
  <br/>
  <div class ="row">
              <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Pin <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="Pin" id="pin" name="pin">   
                            </div>
                        </div>
                    </div> 
                       <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>State <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="State" id="state" name="state">   
                            </div>
                        </div>
                    </div> 
                       <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Country <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="Country" id="country" name="country">   
                            </div>
                        </div>
                    </div>  
     
  </div>
<br/>    
       <!-- Address ends -->
 <div class="row">
                        
                        <div class="col-sm-12">
                        <input type="checkbox" id="sameAddress" name="sameAddress"/>
                        <label><b>Is mailing address is same as address ?</b><span class="requiredField"> *</span></label>         
                            </div>
                   
 </div>      
 <br/>
       <!--  mailing address information  -->
   <div class ="row"> <div class="col-sm-12"><label><b> Mailing Address Information</b><span class="requiredField"> *</span></label></div></div>
  <br/>
  <div class="row">
          <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>City <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="City" id="mailCity" name="mailCity">   
                            </div>
                        </div>
                    </div> 
             <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Street <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="Street" id="mailStreet" name="mailStreet">   
                            </div>
                        </div>
                    </div> 
                    
                <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Locality <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="Locality" id="mailLocality" name="mailLocality">   
                            </div>
                        </div>
                    </div>  
  </div>
  <br/>
  <div class ="row">
              <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Pin <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="Pin" id="mailPin" name="mailPin">   
                            </div>
                        </div>
                    </div> 
                       <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>State <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="State" id="mailState" name="mailState">   
                            </div>
                        </div>
                    </div> 
                       <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Country <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" type="text" placeholder="Country" id="mailCountry" name="mailCountry">   
                            </div>
                        </div>
                    </div>  
     
  </div>     
      
       <!--  mailing address information ends-->   
       <!--  vendor id  -->
       <div class ="row" style="display:none">
             <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Vendor ID <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                    <input class="form-control" value="0" readonly type="text" placeholder="Id" id="vendorInfoId" name="vendorInfoId">   
                            </div>
                        </div>
         </div>  
       </div>  
       <!--  vendor id ends -->   
<br/>
           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center><button class="btn btn-info " type="submit"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            

          
      </form>    
    