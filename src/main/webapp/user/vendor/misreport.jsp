<!-- ======================End Row @ 3==================== -->
            <!--========Start 1====  -->
           
             		<%-- <div class="row">
                         <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Vendor Name</label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control" name="companyName" id="companyName">
                                  <option value="">select Company</option>
                              </select>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

                <div class="row">
                        <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Description</label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Description" name="description" class="form-control" id="description">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PO Number</label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="PO Number" name="poNumber" class="form-control" id="poNumber">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PO Amount</label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="PO Amount" name="poNumber" class="form-control" id="poNumber">
                            </div>
                        </div>
                    </div>
                </div>
                </br>


           <!--=========End 1======  -->
               
           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center><button class="btn btn-info " type="button"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            

          
            <!-- ======================Dynamic table Start here ====== -->
                       
                               
 --%>
 <!-- Billing Report starts  -->
 <div class="billing">
     <form action="BillingServlet" method="post" id="billing_form" novalidate="novalidate" class="cmxform form-horizontal adminex-form" enctype="multipart/form-data">
                   <input type="hidden" name="misReport" value="Billing MIS"/>
         
<!-- first row -->

<div class ="row">
               <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Nature Of Expenses <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Expenses" name="expenses" class="form-control" id="expenses">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Vendor Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Vendor Name" name="bvendorName" class="form-control" id="bvendorName">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Company<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                           <!--   <input type ="text" placeholder="Company" name="company" class="form-control" id="company"> -->
           <!--       <select class="form-control" id="company" name="company">
                                  <option value="">select Company</option>
                                  <option value="company1"> Company1</option>
                   </select> -->
                   
                <select class="form-control select2" id="company" name="company">
                                <c:forEach items="${vendorManagBean.getCompanyNameList }" var="comp" varStatus="status">
                                <option value="${comp[0]}"><c:out value="${comp[0]}"/></option>
                                
                               </c:forEach>
                      </select> 
                            </div>
                        </div>
                    </div>
</div>

<!-- first row ends -->
<!-- second row starts -->
<div class ="row">
       <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PO No.<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="PO No." name="poNo" class="form-control" id="poNo">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Invoice Receiving Date<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                            <div class="input-group date form_datetime-component">
                             <input type ="text" placeholder="Invoice Receiving Date" name="invoiceReceiveDate" class="form-control" id="invoiceReceiveDate">
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="invoiceReceiveDate" generated="true"></label>                      
         
                             <!-- <input type ="text" placeholder="Invoice Receiving Date" name="invoiceReceiveDate" class="form-control" id="invoiceReceiveDate"> -->
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Invoice No.<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Invoice No." name="invoiceNo" class="form-control" id="invoiceNo">
                            </div>
                        </div>
                    </div>

</div>


<!-- second row ends -->
<!-- third row starts -->
<div class ="row">
          <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Invoice Date<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
         <div class="input-group date form_datetime-component">
                             <input type ="text" placeholder="Invoice Date" name="invoiceDate" class="form-control" id="invoiceDate">
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="invoiceDate" generated="true"></label>                      
         
                            <!--  <input type ="text" placeholder="Invoice Date" name="invoiceDate" class="form-control" id="invoiceDate"> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Invoice Amt <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Invoice Amt" name="invoiceAmt" class="form-control" id="invoiceAmt">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Invoice Process Amt <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Process Amt" name="processAmt" class="form-control" id="processAmt">
                            </div>
                        </div>
                    </div>
</div>
<!-- third row ends -->
<!-- fourth row starts -->
  <div class="row">
           <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Bill Submission to Account Date <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
           <div class="input-group date form_datetime-component">
                             <input type ="text" placeholder="Bill Date" name="billDate" class="form-control" id="billDate">
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="prCreationDate" generated="true"></label>                      
         
                            <!--  <input type ="text" placeholder="Bill Date" name="billDate" class="form-control" id="billDate"> -->
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Period <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
     <div class="input-group date form_datetime-component">
                             <input type ="text" placeholder="Period" name="period" class="form-control" id="period">
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="prCreationDate" generated="true"></label>                      
         
                          <!--    <input type ="text" placeholder="Period" name="period" class="form-control" id="period"> -->
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Cost Centre <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <select class="form-control select2" id="costCentre" name="costCentre">
                                <c:forEach items="${vendorManagBean.getCompanyNameList }" var="comp" varStatus="status">
                                <option value="${comp[0]},${comp[1]}"><c:out value="${comp[0]},${comp[1]}"/></option>
                                
                               </c:forEach>
                              </select>  
                           <!--   <input type ="text" placeholder="Cost Centre" name="costCentre" class="form-control" id="costCentre"> -->
                            </div>
                        </div>
                    </div>
  
  
  </div>
<!-- fourth row ends -->

<!-- fifth row starts  -->
<div class = "row">
        <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Location <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Location" name="billLocation" class="form-control" id="billLocation">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Check  receive Date <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
        <div class="input-group date form_datetime-component">
                             <input type ="text" placeholder="Receive Date" name="checkReceiveDate" class="form-control" id="checkReceiveDate">
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="prCreationDate" generated="true"></label>                      
         
                           <!--   <input type ="text" placeholder=" Receive Date" name="checkReceiveDate" class="form-control" id="checkReceiveDate"> -->
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Check No <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Check No" name="checkNo" class="form-control" id="checkNo">
                            </div>
                        </div>
                    </div>

</div>
<!--  fifth row ends  -->
<!-- sixth row starts -->
<div class="row">
     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Check Amt <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Check Amt" name="checkAmt" class="form-control" id="checkAmt">
                            </div>
                        </div>
                    </div>
     
     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Remark <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Remark" name="billRemark" class="form-control" id="billRemark">
                            </div>
                        </div>
                    </div>
     
 <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                    <label>Upload<span class="requiredField"> *</span></label>
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="billAttach" id="billAttach" >
                                                                    </span>
                                                <label class="error" for="escAttach" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                      <div id="billingFile" style="color:black;">
                                                        
                       </div>                     
                                                    </div>
                                                </div>
                                            </div>


  
                                    </div>


</div><br/>
<!-- sixth row ends -->

<!-- billing ID row -->
<div class="row" style="display:none">
      
           <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Biiling ID <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" readonly  value="0" placeholder="ID" name="billId" class="form-control" id="billId">
                            </div>
                        </div>
                    </div>
                    
</div>
<!--  billing ID row end -->
<div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
            <center><button class="btn btn-info " type="submit" id="submit" name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            
            </form>
            
      <!-- billing table  -->       
         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Billing Information
        </header>
        <div class="panel-body">
        <div class="adv-table">
        <section id="flip-scroll">
                
         <table class="display table table-bordered "
							id="mis_bill_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Check Amt</th>
           <!--  <th>Period</th> -->
            <th>Invoice Amt</th>
            <th>Invoice Receiving Date</th>
            <th>Invoice No.</th>       
             <th style="display:none">Nature Of Expenses</th>
            <!-- <th style="display:none">Vendor Name</th> -->
            <th style="display:none">Comapany</th> 
            <th style="display:none">Po No</th>
            <th style="display:none">Invoice Date </th> 
            <th style="display:none">Process Amt</th>
          <!--   <th style="display:none">Bill submission Date</th> -->
            <th style="display:none">Cost Centre</th> 
           <!-- <th style="display:none">Location</th>   --> 
            <th style="display:none">Check Receive Date</th> 
            <th style="display:none">Check No</th>
            <th style="display:none">Remark</th> 
          <!--   <th style="display:none">BillUpload</th>   -->
            <th style="display:none">ID</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
     <!-- body of the table starts-->  
    
  <c:forEach items="${vendorManagBean.billingDetailsList}" var="billing" varStatus="count">
    <tr id="billing${count.index}">
   <td><c:out value="${billing['checkAmt']}"></c:out></td> 
  <%--  <td><c:out value="${billing['_Period']}"></c:out></td>  --%>
   <td><c:out value="${billing['invoiceAmt']}"></c:out></td> 
   <td><c:out value="${billing['invoiceReceiveDate']}"></c:out></td>  
  <td><c:out value="${billing['invoiceNumber']}"></c:out></td> 
  <td style="display:none"><c:out value="${vendorManagBean.misDetailList[count.index].natureOfExpense}"/></td> 
 <%--  <td style="display:none"><c:out value="${billing['vendor_name']}"/></td> --%>
   <td style="display:none"><c:out value="${vendorManagBean.misDetailList[count.index].companyName}"/></td>
  <td style="display:none"><c:out value="${billing['poNo']}"/></td> 
  <td style="display:none"><c:out value="${billing['invoiceDate']}"/></td> 
  <td style="display:none"><c:out value="${billing['invoiceProcessAmt']}"/></td>
<%--   <td style="display:none"><c:out value="${billing['_Bill_submission_to_Account_Date']}"/></td> --%>
  <td style="display:none"><c:out value="${vendorManagBean.misDetailList[count.index].costCenter}"/></td>
<%--   <td style="display:none"><c:out value="${billing['location']}"/></td>   --%>
  <td style="display:none"><c:out value="${billing['checkReceiveDate']}"/></td>
  <td style="display:none"><c:out value="${billing['checkNo']}"/></td>
   <td style="display:none"><c:out value="${billing['remark']}"/></td> 
  <%-- <td style="display:none"><c:out value="${billing['upload']}"/></td> --%>
  <td style="display:none"><c:out value="${billing['id']}"/></td> 
  
  <td><button class="btn btn-warning callEdit" type="button" onclick="callBillingEditFunction(billing${count.index})"><i class="fa fa-hand-o-right"></i>Edit</button></td>
  </tr> 
  </c:forEach> 
    
     
     <!--  body of the table ends  -->         
         </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div> 
<!-- billing table ends --> 
    </div>
    <!-- billing report complete  -->
    
    <!-- Commerial MIS starts-->
    <div class="commercial boxx">
                           <form action="CommercialServlet" method="post" id="commercial_form" novalidate="novalidate" class="cmxform form-horizontal adminex-form" enctype="multipart/form-data">
                   <input type="hidden" name="misReport" value="Commercial MIS"/>
      <!-- first row starts -->
      
      <div class="row">
               <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Vendor Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Vendor Name " name="commercialVendorName" class="form-control" id="commercialVendorName">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Description <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Description" name="commercialDescription" class="form-control" id="commercialDescription">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Company Code <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Company code" name="commercialCompanyCode" class="form-control" id="commercialCompanyCode">
                            </div>
                        </div>
                    </div>
      
      
      </div>
      <!-- first row ends -->   
     <br/> 
  <!-- second row starts --> 
  <div class = "row">
         <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Cost Centre <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <select class="form-control select2" id="commercialCostCentre" name="commercialCostCentre">
                                <c:forEach items="${vendorManagBean.getCompanyNameList }" var="comp" varStatus="status">
                                <option value="${comp[0]},${comp[1]}"><c:out value="${comp[0]},${comp[1]}"/></option>
                                
                               </c:forEach>
                              </select>  
                         <!--     <input type ="text" placeholder="Cost Centre" name="commercialCostCentre" class="form-control" id="commercialCostCentre"> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Category<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Category" name="commercialCategory" class="form-control" id="commercialCategory">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PR Number<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="PR Number" name="prNumber" class="form-control" id="prNumber">
                            </div>
                        </div>
                    </div>
  </div>    
  <!--  second row ends -->
  <br/>
  <!--  third row starts --> 
  <div class="row">
           <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PR Account <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="PR Account" name="prAccount" class="form-control" id="prAccount">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                
                                <label>PR Final Date <span class="requiredField"> *</span></label>
                            </div>
                
                            <div class="col-sm-7">
          <div class="input-group date form_datetime-component">
                             <input type ="text" placeholder="PR Creation Date" name="prCreationDate" class="form-control" id="prCreationDate">
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="prCreationDate" generated="true"></label>                      
                            </div>
                        </div>
                    </div>
              <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PO Amount <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="PO Amount" name="poAmount" class="form-control" id="poAmount">
                            </div>
                        </div>
                    </div>               
                    
  </div>  
  <!-- third row ends -->  
  <br/>  
  <!-- fourth row starts -->
  <div class ="row">
              <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Released From FH <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                  <div class="input-group date form_datetime-component">           
                             <input type ="text" placeholder="Released From FH" name="releasedFromFh" class="form-control" id="releasedFromFh">
                           <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="releasedFromFh" generated="true"></label>   
                            
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Released From BHK <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
            <div class="input-group date form_datetime-component">                 
                             <input type ="text" placeholder="Released From GHK" name="releasedFromGhk" class="form-control" id="releasedFromGhk">
                 <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="releasedFromGhk" generated="true"></label>            
                           
                           
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PO Number <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="PO Number" name="poNumber" class="form-control" id="poNumber">
                            </div>
                        </div>
                    </div>
                    
  </div>
  <!-- fourth row ends -->
  <br/>
  <!-- fifth row starts -->
  <div class ="row">
          <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Released From Dept. GM <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
        <div class="input-group date form_datetime-component">
                             <input type ="text" placeholder="Released From Dept. GM" name="releasedFromDeptGm" class="form-control" id="releasedFromDeptGm">
        <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="releasedFromDeptGm" generated="true"></label>                      
                            
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Released From Comm. <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
          <div class="input-group date form_datetime-component">                    
                             <input type ="text" placeholder="Released From Comm." name="releasedFromComm" class="form-control" id="releasedFromComm">
         <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="releasedFromComm" generated="true"></label>            
                                              
                           
                            </div>
                        </div>
                    </div>
                   
       <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Rel-Status <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Rel-Status" name="relStatus" class="form-control" id="relStatus">
                            </div>
                        </div>
                    </div>              
                    
   </div>
  <!-- fifth row ends -->
  <br/>
  <!-- sixth row starts -->
  <div class="row">
  
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Released From Finance <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
           <div class="input-group date form_datetime-component">                      
                             <input type ="text" placeholder="Released From Finance" name="releasedFromFinance" class="form-control" id="releasedFromFinance">
     <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="releasedFromFinance" generated="true"></label>            
                                                 
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Created By <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
             <div class="input-group date form_datetime-component">                 
                             <input type ="text" placeholder="Created By" name="createdBy" class="form-control" id="createdBy">
              <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="createdBy" generated="true"></label>            
                                         
                           
                            </div>
                        </div>
                    </div>
                   
  </div>
  <!-- sixth row ends -->
  <br/>
  <!-- seventh row starts -->
  <div class="row">
               
                <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>From <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
            <div class="input-group date form_datetime-component">                 
                             <input type ="text" placeholder="From " name="from" class="form-control" id="from">
              <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="from" generated="true"></label>            
                                         
                                 
                            <!--  <input type ="text" placeholder="From To" name="fromTo" class="form-control" id="fromTo"> -->
                            </div>
                        </div>
                    </div>
                    
                <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label> To <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
             <div class="input-group date form_datetime-component">                 
                             <input type ="text" placeholder="To" name="to" class="form-control" id="to">
              <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="to" generated="true"></label>            
                            
                           <!--   <input type ="text" placeholder="From To" name="fromTo" class="form-control" id="fromTo"> -->
                            </div>
                        </div>
                    </div>
                    </div>
  <!-- 7th row ends -->
  <br/>
  <!-- 8th row starts -->
  <div class="row">             
             <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Advance terms <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Advance Terms" name="advanceTerms" class="form-control" id="advanceTerms">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Remarks <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Remarks" name="commercialRemark" class="form-control" id="commercialRemark">
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                    <label>Upload<span class="requiredField"> *</span></label>
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="commAttach" id="commAttach" >
                                                                    </span>
                                                <label class="error" for="escAttach" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                      <div id="commFile" style="color:black;">
                                                        
                                                        </div>                     
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                    
  </div><br/>
  <!-- 8th row ends -->   
  <br/> 
  <!-- commercial id  -->
          <div class="row" style="display:none" >
        <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PO Id<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" readonly placeholder="ID" value="0" name="poId" class="form-control" id="poId">
                            </div>
                        </div>
          </div> 
           <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Pr Id<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" readonly placeholder="ID" value="0" name="prId" class="form-control" id="prId">
                            </div>
                        </div>
          </div> 
    </div>
  
  <!-- id ends -->
  
  <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
            <center><button class="btn btn-info " type="submit" id="submit" name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            
                     
                   </form>
             
                  <!-- commercial  table starts  -->       
         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Commercial Information
        </header>
        <div class="panel-body">
        <div class="adv-table">
        <section id="flip-scroll">
                
         <table class="display table table-bordered "
							id="mis_comm_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>PO Amount</th>
            <th>PO Number</th>
            <th>Released From Comm</th>
            <th>Released From Finance</th>
            <th>Rel Status</th>
            <th style="display:none">Created By</th>
            <th style="display:none">From</th>
            <th style="display:none">To</th>
            <th style="display:none">Commercial Upload</th>
            
            <th style="display:none">PR Amount</th>
            <th style="display:none">PR Number</th>
            <th style="display:none">Released From BHR</th> 
            <th style="display:none">Released From Dept GM</th>
           <!--  <th style="display:none">Released From FH</th> -->
            <th style="display:none">Pr Creation Date</th>
            <th style="display:none">PO ID</th>
            <th style="display:none">Pr ID</th>
            
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>
     <!-- body of the table starts-->  
    
  <c:forEach items="${vendorManagBean.poDetailsList}" var="commercial" varStatus="count">
    <tr id="commercial${count.index}">
   <td><c:out value="${commercial['amount']}"></c:out></td> 
   <td><c:out value="${commercial['ponumber']}"></c:out></td> 
   <td><c:out value="${commercial['releasedFromComm']}"></c:out></td> 
   <td><c:out value="${commercial['releasedFromFinance']}"></c:out></td> 
  <td><c:out value="${commercial['relStatus']}"></c:out></td> 
  <td style="display:none"><c:out value="${commercial['createdBy']}"/></td>
  <td style="display:none"><c:out value="${commercial['poFrom']}"/></td>
  <td style="display:none"><c:out value="${commercial['to']}"/></td>
  <td style="display:none"><c:out value="${commercial['upload']}"/></td>
  
  <td style="display:none"><c:out value="${vendorManagBean.prDetailsList[count.index].amount}"/></td>
  <td style="display:none"><c:out value="${vendorManagBean.prDetailsList[count.index].number}"/></td>
  <td style="display:none"><c:out value="${vendorManagBean.prDetailsList[count.index].releasedFromBHR}"/></td>
  <td style="display:none"><c:out value="${vendorManagBean.prDetailsList[count.index].releasedFromDeptGM}"/></td>
 <%--  <td style="display:none"><c:out value="${vendorManagBean.prDetailsList[count.index].releasedFromFH}"/></td>  --%>
  <td style="display:none"><c:out value="${vendorManagBean.prDetailsList[count.index].creationDate}"/></td>
  <td style="display:none"><c:out value="${commercial['id']}"/></td>
  <td style="display:none"><c:out value="${vendorManagBean.prDetailsList[count.index].id}"/></td>

  <td><button class="btn btn-warning callEdit" type="button" onclick="callCommercialEditFunction(commercial${count.index})"><i class="fa fa-hand-o-right"></i>Edit</button></td>
  </tr> 
  </c:forEach> 
    
     
     <!--  body of the table ends  -->         
         </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div> 
<!-- commercial table ends -->
                   
                   </div>
    <!--  Commercial MIS ends-->
    
    <!-- contract Management starts -->
    <div class="contract boxx">
      <form action="ContractManagementServlet" method="post" id="contract_form" novalidate="novalidate" class="cmxform form-horizontal adminex-form" enctype="multipart/form-data">
                   <input type="hidden" name="misReport" value="Contract Manag"/>
   
    <!-- first row starts -->
        <div  class="row">
                <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Lesser Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Lesser Name" name="lesserName" class="form-control" id="lesserName">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Lessee Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Lessee Name" name="lesseeName" class="form-control" id="lesseeName">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Agreement Type <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Agreement Type" name="agreementType" class="form-control" id="agreementType">
                            </div>
                        </div>
                    </div>
        </div>
    <!-- first row ends -->
    <!-- sec row starts-->
     <div  class="row">
           <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Address <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Address" name="address" class="form-control" id="address">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Landlord Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Landlord Name" name="landlordName" class="form-control" id="landlordName">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Contact Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Contact Name" name="contactName" class="form-control" id="contactName">
                            </div>
                        </div>
                    </div>
                    
        </div>
    <!-- sec row ends -->
    <!--  third row starts -->
     <div  class="row">
              <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Lease End Date <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                  <div class="input-group date form_datetime-component">
                      <input type ="text" placeholder="Lease End Dat" name="leaseEndDate" class="form-control" id="leaseEndDate">
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="leaseEndDate" generated="true"></label>                      
         </div>
                            <!--  <input type ="text" placeholder="Lease End Date" name="leaseEndDate" class="form-control" id="leaseEndDate"> -->
                            </div>
                        </div>
                
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Lease Commencement Date <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
           <div class="input-group date form_datetime-component">                  
        <input type ="text" placeholder="Lease Commence Date" name="leaseCommDate" class="form-control" id="leaseCommDate">
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="leaseCommDate" generated="true"></label>                      
         </div>
                            <!--  <input type ="text" placeholder="Lease Commence Date" name="leaseCommDate" class="form-control" id="leaseCommDate"> -->
                            </div>
                        </div>

<div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Total Area <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Total Area" name="totalArea" class="form-control" id="totalArea">
                            </div>
                        </div>
                    </div>
  </div>
        
    <!-- third row ends -->
    <!-- 4th row starts -->
     <div  class="row">
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Lock in Period <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
               <div class="input-group date form_datetime-component">              
                   <input type ="text" placeholder="Lock in Period" name="lockPeriod" class="form-control" id="lockPeriod">
                            
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="lockPeriod" generated="true"></label>                      
      </div>   
             
                           <!--   <input type ="text" placeholder="Lock in Period" name="lockPeriod" class="form-control" id="lockPeriod"> -->
                            </div>
                        </div>
                    
                    
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Notice Period <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
            <div class="input-group date form_datetime-component">                 
          <input type ="text" placeholder="Notice Period" name="noticePeriod" class="form-control" id="noticePeriod">
                            
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="noticePeriod" generated="true"></label>                      
         </div>
                                                         </div>
                        </div>
           
           <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Process To be Initiated <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Initiated" name="initiated" class="form-control" id="initiated">
                            </div>
                        </div>
                    </div>
           
           
                    
                   
                    </div>
                    
        
    <!-- 4th row ends -->
    <!-- 5th row starts -->
     <div  class="row">
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Escalation <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Escalation" name="escalation" class="form-control" id="escalation">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Next Escalation Date <span class="requiredField"> *</span> </label>
                            </div>
                            <div class="col-sm-7">
           <div class="input-group date form_datetime-component">                  
                  <input type ="text" placeholder="Escalation Date" name="escalationDate" class="form-control" id="escalationDate">
                            
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="escalationDate" generated="true"></label>                      
        </div> 
                            <!--  <input type ="text" placeholder="Escalation Date" name="escalationDate" class="form-control" id="escalationDate"> -->
                            </div>
                        </div>
                    
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Security Deposit <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Security Deposit" name="securityDeposit" class="form-control" id="securityDeposit">
                            </div>
                        </div>
                    </div>
        </div>
    <!-- 5th row ends -->
    <!-- 6th row starts -->
     <div  class="row">
                  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Monthly Rent <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Monthly Rent" name="monthlyRent" class="form-control numbersOnly" id="monthlyRent" onkeyup="sum();">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Monthly Maintenance <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Monthly Maintenance" name="monthlyMaintenance" class="form-control numbersOnly" id="monthlyMaintenance" onkeyup="sum();">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Power Back Up Charges <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Power Back" name="powerBack" class="form-control numbersOnly" id="powerBack" onkeyup="sum();">
                            </div>
                        </div>
                    </div>
        </div>
        <!-- 6th row ends -->
    <!-- 7th row starts -->
     <div  class="row">
                  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Parking Charges <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Parking Charges" name="parkingCharges" class="form-control numbersOnly" id="parkingCharges" onkeyup="sum();">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Any Other Fixed Cost <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" placeholder="Cost" name="fixedCost" class="form-control numbersOnly" id="fixedCost" onkeyup="sum();">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Total Rent Payable <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" readonly placeholder="Total Rent Payable" name="totalRentPayable" class="form-control" id="totalRentPayable">
                            </div>
                        </div>
                    </div>
        </div>
    <!-- 7th row ends -->
    <!-- 8th row starts -->
     <div  class="row">
     
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Payment Date <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                  <div class="input-group date form_datetime-component">           
                    <input type ="text" placeholder="Payment Date" name="paymentDate" class="form-control" id="paymentDate">
                            
                    <span class="input-group-btn">
         <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
           </div>    
         <label class="error" for="paymentDate" generated="true"></label>                      
         </div>
                            <!--  <input type ="text" placeholder="Payment Date" name="paymentDate" class="form-control" id="paymentDate">
 -->                            </div>
                        </div>
          
           <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                    <label>Upload<span class="requiredField"> *</span></label>
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="contractAttach" id="contractAttach" >
                                                                    </span>
                                                <label class="error" for="escAttach" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                       
                                 <div id="conFile" style="color:black;"></div>              
                                                                            
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                        
                        
  </div>
                    
        <br/>
    <!-- 8th row ends -->
    <!--  contract id row -->
    <div class="row" style="display:none">
        <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Contract Id<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                             <input type ="text" readonly placeholder="ID" value="0" name="contractId" class="form-control" id="contractId">
                            </div>
                        </div>
          </div> 
    </div>
    <!-- contract Id row ends -->
    <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
            <center><button class="btn btn-info " type="submit" id="submit" name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            
         </form>
         
              <!-- contract table starts  -->       
         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Contract Information
        </header>
        <div class="panel-body">
        <div class="adv-table">
        <section id="flip-scroll">
                
         <table class="display table table-bordered "
							id="mis_contract_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Lesser Name</th>
            <th>Agreement Type</th>
            <th>Landlord Name</th>
            <th>Total Area</th>
            <th>Monthly Rent</th>
            <th style="display:none">Lessee Name</th>
        <!--     <th style="display:none">Address</th>
            <th style="display:none">Contact Number</th> -->
             <th style="display:none">Lease Commencement</th>
             <th style="display:none">Lease End Date </th> 
            <th style="display:none">Process Initiated</th>
            <th style="display:none">Notice Period</th>
            <th style="display:none">Lock In Period</th>
            <th style="display:none">Escalation</th>
            <th style="display:none">Next Escalation</th> 
            <th style="display:none">Security Deposit</th>
            <th style="display:none">Monthly Maintenance</th>
            <th style="display:none">Power Back Up</th>
            <th style="display:none">Parking Charges</th>
            <th style="display:none">Any Other Fixed Cost</th>
            <th style="display:none">Total Rent Payable</th>
            <th style="display:none">ID</th>
            <th style="display:none">Payment Date</th>
           <th style="display:none">Contract Upload</th> 
          
            
            <th>Action</th>
            
            
            
        </tr>
        </thead>
        <tbody>
     <!-- body of the table starts-->  
    
  <c:forEach items="${vendorManagBean.contractDetailsList}" var="contract" varStatus="count">
    <tr id="contract${count.index}">
   <td><c:out value="${contract['lessorName']}"></c:out></td> 
   <td><c:out value="${contract['agreementType']}"></c:out></td> 
   <td><c:out value="${contract['landlordName']}"></c:out></td> 
   <td><c:out value="${contract['totalArea']}"></c:out></td> 
  <td><c:out value="${contract['monthlyRent']}"></c:out></td>
  <td style="display:none"><c:out value="${contract['lesseeName']}"/></td>
<%--   <td style="display:none"><c:out value="${contract['']}"/></td>
  <td style="display:none"><c:out value="${contract['']}"/></td> --%>
 <td style="display:none"><c:out value="${contract['leaseCommencementDate']}"/></td>
  <td style="display:none"><c:out value="${contract['leaseEndDate']}"/></td>
  <td style="display:none"><c:out value="${contract['processToBeInitiated']}"/></td>
  <td style="display:none"><c:out value="${contract['noticePeriod']}"/></td>
  <td style="display:none"><c:out value="${contract['lockInPeriod']}"/></td>
  <td style="display:none"><c:out value="${contract['escalation']}"/></td>
  <td style="display:none"><c:out value="${contract['nextEscalationDate']}"/></td> 
  <td style="display:none"><c:out value="${contract['securityDeposit']}"/></td>
  <td style="display:none"><c:out value="${contract['monthlyMaintenance']}"/></td>
  <td style="display:none"><c:out value="${contract['powerBackUpCharges']}"/></td>
  <td style="display:none"><c:out value="${contract['parkingCharges']}"/></td>
  <td style="display:none"><c:out value="${contract['anyOtherFixedCost']}"/></td>
  <td style="display:none"><c:out value="${contract['totalRentPayable']}"/></td>
  <td style="display:none"><c:out value="${contract['id']}"/></td>
  <td style="display:none"><c:out value="${contract['paymentDate']}"/></td>   
    <td style="display:none"><c:out value="${contract['upload']}"/></td>   
  <td><button class="btn btn-warning" type="button" onclick="callContractEditFunction(contract${count.index})"><i class="fa fa-hand-o-right"></i>Edit</button></td> 
  </tr> 
  </c:forEach> 
    
     
     <!--  body of the table ends  -->         
         </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div> 
<!-- contract table ends -->
         
   </div>
    <!-- contract Management ends -->
    
    