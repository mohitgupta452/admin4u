<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:layout>

	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
    
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    
    <!--dynamic table-->
    <script type="text/javascript" language="javascript"src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"	src="assets/js/data-tables/DT_bootstrap.js"></script>
<script	src="assets/js/product_requisition_dtable_init/product_req_insert_dtable.js"></script>
<script	src="assets/js/product_requisition_dtable_init/product_req_get_dtable.js"></script>
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
 
        <script src="assets/js/customJS/pdt_requisition_table.js"></script>
    </jsp:attribute>
	<jsp:body>
	

      <!-- page heading start-->
        

        <div class="page-heading">
        <!-- Cab/Ticket -->
          <section class="panel">
                        <header class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs">
                                <!-- <li class="active">
                                    <a href="#ticket_requests" data-toggle="tab">Ticket Booking</a>
                                </li> -->
                               <!--  <li >
                                    <a href="#cab_requests" data-toggle="tab">Cab Request</a>
                                </li> -->
                                <li class="active">
                                    <a href="#item_req_info" data-toggle="tab">Product Requistion</a>
                                </li>
                               <!--  <li>
                                    <a href="#contact_info" data-toggle="tab">Contact Information</a>
                                </li>
                               
                                <li>
                                    <a href="#payment_info" data-toggle="tab">Payment Terms</a>
                                </li>
                                <li>
                                    <a href="#mis_report" data-toggle="tab">Mis Report</a>
                                </li> -->
                                
                                <!-- <li>
                                    <a href="#admin_travel_department" data-toggle="tab">Transport Admin</a>
                                </li> -->
                            </ul>
                        </header>

<!-- =====Tab Pannel Start Here -->
<div class="panel-body">
    <div class="tab-content">
        
<!--============= Form for Ticket Requests ==================-->

<!--============= End Here Ticket Requests ==================-->


<!-- ==========Form Hotel / Guest House Booking =======  -->
    <div class="tab-pane active" id="item_req_info">
    
    <form action="ProductReqServlet" method="post" id="itemRequistionForm"  onsubmit="getData()"> 

        			 <div class="row">
                        <div class="col-sm-12">
                <section class="panel">
               <!--  <header class="panel-heading">
                    Editable Table
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                     </span>
                </header> -->
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    <div class="btn-group">
                        <button id="product-requistion_new" class="btn btn-primary">
                            Add New <i class="fa fa-plus"></i>
                        </button>
                    </div>
                    <div class="btn-group pull-right">
                        <!-- <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                        </button> -->
                       <!--  <ul class="dropdown-menu pull-right">
                            <li><a href="#">Print</a></li>
                            <li><a href="#">Save as PDF</a></li>
                            <li><a href="#">Export to Excel</a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="space15"></div>
                <table class="table table-striped table-hover table-bordered" id="product_req_editable">
                <thead>
                <tr>
                    <th>Product Category</th>
                    <th>Product Brand</th>
                    <th>Product Name</th>
                    <th>Purpose</th>
                    <th>Qty Required</th>
                    <th style="display:none">Product Name</th>
                    <th style="display:none">Product Brand</th>
                    <th style="display:none"> Category</th>
                    
                    
                    <%-- <th style="display:none"></th> --%>
                </tr>
                </thead>
                <tbody >
                
                </tbody>
                </table>
                </div>
                </div>
                </section>
                </div>
        </div>
                           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center><button class="btn btn-info" name="action" value="insertProductReqData" id="pdtReqSubmit" type="submit"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        <input type="hidden" name="total" id="total" value=""/>
                        <input type="hidden" name="service" value="INVENTORY">
                           
                        </div>
                    </div>
            </div>
            </form>
            
            <!-- table starts -->
             
               <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Product Requisition Information
        </header>
        <div class="panel-body">
        <div class="adv-table">
        <section id="flip-scroll">
                
         <table class="display table table-bordered "
							id="product_req_get_editable">
        <thead class="cf">
        <tr>
                    <th>Product Category</th>
                    <th>Product Brand</th>
                    <th>Product Name</th>
                    <th>Purpose</th>
                    <th>Qty Required</th> 
           
            
        </tr>
        </thead>
        <tbody id="mytrtd">
     <!-- body of the table starts-->  

  
     <!--  body of the table ends  -->         
         </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div> 
            
            
            <!--  table ends -->
            
    </div>
    </div>
    </div>
    </section>
    <!-- Next form Fields E- Attachment  -->
    <!-- ===== Dyanmic Table Start Here ====== -->


    <!-- ===== Dynamic Table End Here ===== -->
    </div>
    <!-- page heading end-->
        
        <!-- validation -->
    </jsp:body>
</t:layout>