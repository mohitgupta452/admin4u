<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.admin4u.views.RoleBaseDashboardBean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:admin-layout>
<jsp:attribute name="header">

<style>
.main-content
{
	margin-left:0px !important;
}

.sticky-left-side
{
    height: 0% !important;
}
.left-side
{
    width:0px !important;
}
.dashboard-text
{
	color:#ccc;
	font-family:arial;
}
.btn-service
{
	background: transparent;
    color: #fff;
    border-radius: 20px;
    outline:none;
}
</style>
</jsp:attribute>


	<jsp:attribute name="extrajs">
   <script src="assets/js/customJS/index.js"></script>

</jsp:attribute>
	<jsp:body>
            
        <!--body wrapper start-->
                <div class="row" style="padding-left: 14px;padding-right: 14px;">
<!--             <section class="panel"> -->
            <div class="col-md-7 slider-box">
                        <div class="carousel slide auto panel-body"
					id="c-slide">
<!--                             <ol class="carousel-indicators out"> -->
<!--                                 <li data-target="#c-slide" -->
<!-- 							data-slide-to="0" class=""></li> -->
<!--                                 <li data-target="#c-slide" -->
<!-- 							data-slide-to="1" class=""></li> -->
<!--                                 <li data-target="#c-slide" -->
<!-- 							data-slide-to="2" class=""></li> -->
<!--                             </ol> -->
                            <div class="carousel-inner">
                                <div class="item text-center next left" >
                                    <h3 style="color:#fff;font-size:25px;font-family;arial;">Welcome !! Admin4U Portal</h3>
                                    <small class="text-muted"><p class="dashboard-text">Get Latest News Updates</p></small>
                                </div>
                                <div class="item text-center">
                                    <p class="dashboard-text">Good Morning</p>
                                    <small class="text-muted"></small>
                                </div>
                                <div
							class="item text-center active left">
                                    <p class="dashboard-text">Site Undermaintenance</p>
                                    <small class="text-muted"></small>
                                </div>
                            </div>
                            <a class="left carousel-control"
						href="#c-slide" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right carousel-control"
						href="#c-slide" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
			</div>
			 <div class="col-md-5 right-box">
			 	<!-- <p class="right-box-head">Reference Document</p> -->
			 	<!-- <ul class="list-inline element">
			 		<li>
			 			<div class="form-group">
			 			<select class="form-control ref-dropdown ref-select" id="selectFile" onchange="setFilePath()">
			 			<option value="">Select Option</option>
			 			<c:forEach var="policy" items="${dashboard.policyDocs}">
						    
						    <option value="${policy.path}">"${policy.docName}</option>
						 
						</c:forEach>
						 </select>
						</div>
			 		</li>
			 		<li>
			 			<a  id="downloadFile"><span><img
								src="assets/images/download-icon.png" data-toggle="tooltip" id="downloadImg"
								data-placement="left" title="Download"></span></a>
			 		</li>
			 		<li>
			 			<a target="_blank"  id="viewFile"><span><img
								src="assets/images/view-icon.png" data-toggle="tooltip" id="viewImg"
								data-placement="left" title="View Document"></span></a>
			 		</li>
			 	</ul> -->
			 	
			 	<p class="right-box-head"><i class="fa fa-phone" style="
    border: 1px solid;
    border-radius: 11px;
    padding: 4px;
    background-color: #3c763d;
"></i> Service</p>
			 	<ul class="list-inline element">
			 		<li><a href="user/cservice_log/chp_service.jsp"><button class="btn btn-default btn-service">Housekeeping Service</button></a></li>
			 		<li><a href="user/cservice_log/crm_service.jsp"><button class="btn btn-default btn-service">Repair & Maintainance</button></a></li>
			 	</ul>
			 </div>
<!--                     </section> -->
            </div>
        
 
<!--dashboard items  start -->
       <div class="col-md-12 grid-container"> 
                  <div class="row states-info">
                           
            <a href="user/admintable.jsp" class="atext">
	            <div class="col-md-3">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon">
	                    	<img src="assets/images/admin-icon.png" class="grid-img">
	                    </div>
	                    <div class="col-sm-7 grid-text">
	                    	<h2>User Request Table</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
        	</a>
            
            
             <a href="RoleManagementServlet?as=ADMIN&action=forwordRoleJSP" class="atext">
	            <div class="col-md-3">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon">
	                    	<img src="assets/images/admin-icon.png" class="grid-img">
	                    </div>
	                    <div class="col-sm-7 grid-text">
	                    	<h2>Role Management</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
        	</a>
            
            
             <a href="user/admin_panel.jsp" class="atext">
	            <div class="col-md-3">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon">
	                    	<img src="assets/images/admin-icon.png" class="grid-img">
	                    </div>
	                    <div class="col-sm-7 grid-text">
	                    	<h2>Admin Panel</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
        	</a>
        	 <a href="user/admin/tatService.jsp" class="atext">
	            <div class="col-md-3">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon">
	                    	<img src="assets/images/admin-icon.png" class="grid-img">
	                    </div>
	                    <div class="col-sm-7 grid-text">
	                    	<h2>Service TAT</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
        	</a>
  
        </div>
		</div>
   <!--end dashboard items  start -->
            
            
                <!-- ==================Next Button Start Here================= -->
        <div class="row"> 
                <ul class="pager">
                  
                </ul>
        </div>
                           
        <!--body wrapper end-->

        <!--footer section start-->

        <footer class="sticky-footer">
            2014 &copy; Admin4U by Turningcloud
        </footer>

        <!--footer section end-->
    <!-- main content end-->

  
        
    </jsp:body>
</t:admin-layout>