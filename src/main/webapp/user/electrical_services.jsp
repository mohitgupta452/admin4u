<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:form-layout>
	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
    
    
    <script type="text/javascript"
			src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/customJS/electrical_services.js"></script>
	      <script src ="assets/js/customJS/utility.js"></script>
	



<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
  
	
</jsp:attribute>
<jsp:attribute name="extrajs">
<script type="text/javascript"  src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="assets/js/dynamic_table_init.js"></script>

    <script src ="assets/js/customJS/electrical_services.js"></script>
    <script src="assets/js/customJS/utility.js"></script>
    </jsp:attribute>

	<jsp:body>

        <!-- page heading start-->
        

        <div class="page-heading">
        <!-- Cab/Ticket -->
          <section class="panel">
                        <header
				class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs">
                                <!-- <li class="active">
                                    <a href="#ticket_requests" data-toggle="tab">Ticket Booking</a>
                                </li> -->
                               <!--  <li >
                                    <a href="#cab_requests" data-toggle="tab">Cab Request</a>
                                </li> -->
                                <li class="active">
                                    <a href="#electrical_services"
					data-toggle="tab">Electrical Services</a>
                                </li>
                              
                                <c:if test="${sessionScope.userid == 'admin' }">
                                <li>
                                    <a href="#admin_travel_requests" data-toggle="tab">ADMIN</a>
                                </li>
                                <li>
                                    <a href="#user_confirmation"
					data-toggle="tab">User Confirmation</a>
                                </li>
                                </c:if>
                                
                                <!-- <li>
                                    <a href="#admin_travel_department" data-toggle="tab">Transport Admin</a>
                                </li> -->
                            </ul>
                        </header>

<!-- =====Tab Pannel Start Here -->
<div class="panel-body">
    <div class="tab-content">
        
<!-- ==========Form Hotel / Guest House Booking =======  -->
    <div class="tab-pane active" id="electrical_services">
             <!--==========Tab Start Here========-->                    
            
            <!-- ==================House Keeping form Starts====================== -->
            <form action="electricalServices" method="post"
							id="electrical_form" novalidate="novalidate"
							class="cmxform form-horizontal adminex-form">
  
   <div class="row">
             
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <!-- <label>Guest Type<br>&nbsp;</label> -->
                            </div>
                            <div class="col-sm-7">
                             
                            </div>
                        </div>
                    </div>
                    </div>
            <!-- ==================Row 3=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Name(Requestor)<span
												class="labelcolor">*</span></label>
                                </div>
                           <div class="col-sm-7">
                              <input type="text" class="form-control alphabetOnly"
												id="username" name="username" placeholder="Requestor" />
                      </div>
                      </div>
                      </div>
                   
                         <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Workstation<span
												class="labelcolor">*</span></label>
                            </div>
                             <div class="col-sm-7">
   <input type="text" class="form-control"
	id="workstation" name="workstation" placeholder="Workstation"/>
                                             
                              </div>
                     </div>
                     </div>
                    
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                    
                                <label>Category<span
												class="labelcolor">*</span></label>
                                </div>
                            
                           <div class="col-sm-7">
                              <!--  <input class="form-control" type="text"
												id="work" name="work" />-->
												  <select class="form-control select2" id="category"
												name="category">
                               <option value="">select...</option>
            <c:forEach items="${electricalService.categoryOfElectricalServices}" var="category">
                <option value="${category}"> <c:out value="${category}"/></option>               
             </c:forEach>
                              </select>
                        </div>
                    </div>
                    </div>
                    </div>
                    <br />
            <!-- ======================End Row @ 3==================== -->
            <!-- ==================Row start @4=============== -->
               
               <div class="row">
                    
           <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Problem Description<span
												class="labelcolor">*</span></label>
                          </div>
                           <div class="col-sm-7">
                                <input class="form-control" size="12"
												type="text" name="description" id="description" placeholder="Problem Description"/>
                     </div>
                     </div>
                     </div>
                    
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Warranty<span
												class="labelcolor">*</span></label>
                          </div>
                          
                           <div class="col-sm-7">
                              <!--  <input class="form-control" type="text"
												id="work" name="work" />-->
												  <select class="form-control select2" id="warranty"
												name="warranty">
                               <option value="">select...</option>
                               <option value="yes">Yes</option>
                               <option value="no">No</option>
                              </select>
                        </div>
                        </div>
                        </div>
                        
            <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>AMC<span
												class="labelcolor">*</span></label>
                          </div>
                          
                           <div class="col-sm-7">
                              <!--  <input class="form-control" type="text"
												id="work" name="work" />-->
												  <select class="form-control select2" id="amc"
												name="amc">
                               <option value="">select...</option>
                               <option value="yes">Yes</option> 
                               <option value="No">No</option>
                              </select>
                        </div>
                        </div>
                        </div>
                        </div>
                
            <!-- ======================End Row @ 4==================== -->
            
          
<center>
<button class="btn btn-info" type="submit" name="action" value="GENERATED" id="submit">
									<i class="fa fa-hand-o-right"></i> Submit</button>
							</center>
  </form>
  
            
            
            <!-- ===================House Keeping Form Ends============================== -->
            
            
                       
        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           USER VIEW
            <span class="tools pull-right">
                
             </span>
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
											id="hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Request ID</th>
            <th>Requester</th>
         <!--   <th class="hidden-phone">Request Type</th>-->
            
            <th class="hidden-phone">Booking For</th>
            <th class="hidden-phone">Request Date</th>
            <th class="hidden-phone">Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="req" items="${electricalService.USERDataList}">
        <tr class="gradeX" id="rid${req['requestID'] }">
           <td><a class="showRequestDataModal"href="javascript:;"  onclick="callShowRequestData(<c:out value="${req['requestID']}">
         </c:out>,'<c:out value="${req['service']}" />')"><c:out value="${req['requestID']}"></c:out></a></td>
            <td><c:out value="${req['requester']}"></c:out></td>

            <td class="hidden-phone"><c:out value="${req.requestType}"/></td>
            <td class="center hidden-phone"><c:out value="${req.requestdate}"/></td>
            <td class="center hidden-phone"><c:out value="${req.requestStatus}"/></td>
            <td>
<button class="btn btn-info <c:if test="${req['isComplete']=='true' || req['requestStatus']=='CANCELLED' || request['complete']==true}"> <c:out value="disabled"/></c:if>" type="button" onclick="callCancelRequest('${req['requestID']}')">
															<i class="fa fa-hand-o-right"></i> Cancel</button>
</td>
        </tr>
      </c:forEach>
       
        </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div>
        


             <!--==========Tab End Here==========-->
    </div>

                                                                                                      
<!--===========End Hotel / Guest House Request Form====-->    



<!-- =====ADMIN TABLE starts from here===== -->

      <div class="tab-pane"
						id="admin_travel_requests">
                                    <!-- Form for Ticket Requests -->
                                  <div id="preapprove"
							style="width: 100%">

                                    </div>
                                    <br>
        <!-- ==== Dynamic table Start here ===== -->
           

 <!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="admin_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <table>
        <tr>
												<td>
          <label>Message: &nbsp;  </label>
												</td>
												<td style="margin-top: 10px;"><textarea rows="2"
														cols="30">Message area</textarea></td>
          </tr>
</table>

        </div>
        <div class="modal-footer">
          <center>
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Save</button>
										</center>
        </div>
      </div>
      
    </div>
  </div>
  


<!-- end of modal -->

<!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="admin_modal_assign" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <div class="form-group">
          <label>Select Spoc:   </label><select class="form-control">
             <option>select....</option> 
             <option>Travel Spoc</option> 
             <option>Transport Spoc</option> 

          </select>
          </div>
        </div>
        <div class="modal-footer">
          <center>
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Save</button>
										</center>
        </div>
      </div>
      
    </div>
  </div>
  


<!-- end of modal -->
      

                    
                             <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Admin
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
											id="admin_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Entity Location</th>
            <th>Work Order</th>
            <th class="hidden-phone">Employee Id</th>
            <th class="hidden-phone">Department</th>
            <th class="hidden-phone">Booking Type</th>
            <th>Booking For</th>
            <th>Onward Journey</th>

            <th>Status</th>
            <th>Close</th>
            <th>Action</th>
            <th>Assign</th>


        </tr>
        </thead>
        <tbody>
        <tr class="gradeX">
            <td>Hanshalya Delhi</td>
            <td>E/W1</td>
            <td class="hidden-phone">Emp1</td>
            <td class="center hidden-phone">IT department</td>
            <td class="center hidden-phone">train</td>
         <td>Self</td>
            <td>12-feb-2016</td>
            <td>pending</td>
            <td><button class="btn btn-warning " type="button"
															data-toggle="modal" data-target="#admin_modal">
															<i class="fa fa-hand-o-right"></i> Close</button></td>
            <td><button class="btn btn-info " type="button">
															<i class="fa fa-hand-o-right"></i> Book Ticket</button>
</td>
            
            <td><button class="btn btn-info " type="button"
															data-toggle="modal" data-target="#admin_modal_assign">
															<i class="fa fa-hand-o-right"></i> Assign</button>
</td>
        </tr>
                </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div> 
      
</div>


     





<!--  ======ADMIN TABLE ends here  -->

<!-- ==confirmation Requests Tab start Here == -->
<div class="tab-pane" id="user_confirmation">
                                    <!-- Form for Ticket Requests -->
                                 <!--  <div id="preapprove"style="width:100%">

                                    </div> -->
                                  <!--   <br> -->
        <!-- ==== Dynamic table Start here ===== -->
           
                    <div class="panel " style="border: 1px solid black">
                            <div class="row">
                                 <div class="col-sm-12">
                                    <section class="panel">
                                    <header class="panel-heading">
                                        DataTables fo all booking details
                                        <!-- <span class="tools pull-right">
                                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                                            <a href="javascript:;" class="fa fa-times"></a>
                                         </span> -->
                                    </header>
                                     <div class="panel-body">
                                        <div class="adv-table">
                                        <div
												id="hidden-table-info_wrapper"
												class="dataTables_wrapper form-inline" role="grid">
                                <!-- =====Passenger Confirmation Details Start==== -->
                                <div class="table">
                                        <div class="row">
                                                  <div class="col-md-4">
															<label for="t_type">Booking Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Train Ticket</div>
                                                  <div class="col-md-4">
															<label for="t_type">Booking for&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Self Booking</div>
                                                  <div class="col-md-4">
															<label for="t_type">Preffered Time Slot&nbsp;&nbsp;</label>09:00 HRS - 11:00 HRS</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
															<label for="w_id">Travel Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>12-Feb-2016</div>
                                                  <div class="col-md-4">
															<label for="t_type">Travel Class&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>AC-III Tier</div>
                                                  <div class="col-md-4">
															<label for="w_id">Travel Purpose&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Offical Only</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
															<label for="w_id">Passenger&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</label>Manoj Singh</div>
                                                  <div class="col-md-4">
															<label for="w_id">Employee ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>EMP/DB/1234</div>
                                                  <div class="col-md-4">
															<label for="w_id">Mobile No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>9643827282</div>
                                         </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                </section>
                            </div>
                            </div>
<!-- Ticket Confirmation Start Here -->
                    <div class="row">
                                <div class="col-sm-12">
                                            <section class="panel">
                                                <header
										class="panel-heading">
                                                   Ticket Confirmation Details
                                                    <!-- <span class="tools pull-right">
                                                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                                                        <a href="javascript:;" class="fa fa-times"></a>
                                                     </span> -->
                                                </header>
                                                <div class="panel-body">
                                                    <div
											class="adv-table">
                                                        <div
												id="preapprove" style="width: 100%">
                                                              <form
													action="action">
                                                                  <div
														class="table">
                                                                         <div
															class="row">
                                                                                  <div
																class="col-md-4">
																<label for="w_id">E- Ticket Downloads&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="button" value="Download Ticket"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                                                 
                                                                         </div>
                                                                    </div>
                                                                <!-- <center><input type="submit" value="Submit For Approval"></center> -->
                                                                </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>

<!-- Ticket Confirmation End Here -->
<!-- Cab Confirmation Start Here -->

                            <div class="row">
                                <div class="col-sm-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       Cab Confirmation Details
                                        <!-- <span class="tools pull-right">
                                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                                            <a href="javascript:;" class="fa fa-times"></a>
                                         </span> -->
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                            <div id="preapprove"
												style="width: 100%">
                                                  <form action="action">
                                                      <div class="table">
                                                             <div
															class="row">
                                                                      <div
																class="col-md-4">
																<label for="w_id">Cab Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Innova Diesel</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Cab No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>DL-05 AC-1324</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Cab Provider &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>ABC Provider</div>
                                                             </div>
                                                             <div
															class="row">
                                                                      <div
																class="col-md-4">
																<label for="w_id">Driver Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Abc Name</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Driver No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>963524871</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">PickUp Date/Time</label>13-Feb-2015  16:30:00 Hrs</div>
                                                             </div>
                                                        </div>
                                                    <!-- <center><input type="submit" value="Submit For Approval"></center> -->
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
<!-- Cab Cofirmation End Here -->
<!-- Guest Confirmation Start Here -->
                <div class="row">
                                <div class="col-sm-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       Cab Confirmation Details
                                        <!-- <span class="tools pull-right">
                                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                                            <a href="javascript:;" class="fa fa-times"></a>             
                                         </span> -->
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                            <div id="preapprove"
												style="width: 100%">
                                                  <form action="action">
                                                      <div class="table">
                                                             <div
															class="row">
                                                                      <div
																class="col-md-4">
																<label for="w_id">Hotel/G.House&nbsp;&nbsp;&<noframes></noframes>bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Abc Hotel ,Delhi</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Contact Person&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>xyz Name</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Mobile No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>9157341872</div>
                                                             </div>
                                                        </div>
                                                    <center>
														<input type="submit" value="Download">
													</center>
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>

<!-- Guest house confirmation End Here -->
                    </div>
    </div>

                <!-- ==confirmation Requests Tab End Here == -->
        
            <!-- Travel Admin Requests Start Here -->
            <div class="tab-pane" id="admin_travel_department">
                                    <!-- Form for Ticket Requests -->
                                  <div id="preapprove"
							style="width: 100%">
                                  <form action="action">
                                  <div class="table">
                                        <div class="row">
                                                  <div class="col-md-4">
                                                  <label for="ext_date">One Way Journey</label>&nbsp;&nbsp;&nbsp;Yes</div>
                                                  <div class="col-md-4">
											<label for="ext_date">Return Journey</label>&nbsp;&nbsp;&nbsp;No</div>
                                                  <div class="col-md-4">
											<label for="ext_date">Food & Beverages (As per Policy)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yes</div>
                                         </div>

                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="t_type">Booking Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Train Ticket</div>
                                                  <div class="col-md-4">
											<label for="t_type">Booking for&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Self Booking</div>
                                                  <div class="col-md-4">
											<label for="t_type">Preffered Time Slot&nbsp;&nbsp;</label>09:00 HRS - 11:00 HRS</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">Travel Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>12-Feb-2016</div>
                                                  <div class="col-md-4">
											<label for="t_type">Travel Class&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>AC-III Tier</div>
                                                  <div class="col-md-4">
											<label for="w_id">Travel Purpose&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Offical Only</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">Passenger&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</label>Manoj Singh</div>
                                                  <div class="col-md-4">
											<label for="w_id">Employee ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>EMP/DB/1234</div>
                                                  <div class="col-md-4">
											<label for="w_id">Mobile No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>9643827282</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">Source&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>XYz Station</div>
                                                  <div class="col-md-4">
											<label for="w_id">Destination&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>XYZ Destination</div>
                                                  <div class="col-md-4">
											<label for="ext_date">Avail Cab</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;<label
												for="ext_date">Avail Hotel/Guest House</label>&nbsp;&nbsp;&nbsp;Yes</div>    
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">PickUp Point&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Xyz Address ,State</div>
                                                  <div class="col-md-4">
											<label for="w_id">End Point&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Xyz Address, State</div>
                                                  <div class="col-md-4">
											<label for="w_id">E-Attachment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>abc.pdf</div>    
                                         </div>
                                          
                                    </div>
                                    <!-- <center><input type="submit" value="Accept"><input type="submit" value="Reject"></center> -->
                                    </form>
                                    </div>
                                    <br>
        <!-- ==== Dynamic table Start here ===== -->
           
                    <div class="panel " style="border: 1px solid black">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                    <header class="panel-heading">
                                       Ticket Booking Details
                                        <!-- <span class="tools pull-right">
                                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                                            <a href="javascript:;" class="fa fa-times"></a>
                                         </span> -->
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                        <!-- lkflf -->
                                        <div id="preapprove"
												style="width: 100%">
                                  <form action="action">
                                  <div class="table">
                                         <div class="row">
                                                  <div class="col-md-4">
																<label for="w_id">Depature Date/Time</label><input
																	type="date" value="Depature Date"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Booking Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="date" value="" style="height: 21px; width: 175px"
																	id="w_id" background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">E-Ticket &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="button" value="Upload E-ticket"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                         </div>
                                         
                                         
                                    </div>
                                   <!--  <center><input type="submit" value="Submit For Approval"></center> -->
                                    </form>
                                    </div>

                                        <!-- ff -->
                                        </div>
                                    </div>
                                    </section>
                                    </div>
                                    </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                    <header class="panel-heading">
                                       Cab Booking Details
                                        <!-- <span class="tools pull-right">
                                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                                            <a href="javascript:;" class="fa fa-times"></a>
                                         </span> -->
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                        <!-- lkflf -->
                                        <div id="preapprove"
												style="width: 100%">
                                  <form action="action">
                                  <div class="table">
                                         <div class="row">
                                                  <div class="col-md-4">
																<label for="w_id">Cab Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vehicle Type"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Cab No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vechile No"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Cab Provider &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vendor Name"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
																<label for="w_id">Driver Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vehicle Type"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Driver No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vechile No"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">PickUp Date/Time</label><input
																	type="date" value="Vendor Name"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                         </div>
                                    </div>
                                    <!-- <center><input type="submit" value="Submit For Approval"></center> -->
                                    </form>
                                    </div>



                                        <!-- ff -->
                                        </div>
                                    </div>
                                    </section>
                                    </div>
                                    </div>
<!-- Row-2 End Here -->

              </div>

                <!-- ====Ticket Requests End Here -->
            </div>

            <!-- Travel Request End Here -->


    </div>
    <!--<div class="tab-pane" id="contact2">Contact</div>
    </div> -->
    
    
     <!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
             </div>
        <br>
        <div class="modal-footer" style="margin-top: 150px;position:relative; ">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of modal -->
    
    
     <!-- Start Of  Response Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        
        
        
             </div>
        
        
        <br>
       
        <div class="modal-footer"
						style="margin-top: 150px; position: relative;">
          <center>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Save</button>
						</center>
        </div>
      </div>
      
    </div>
  </div>
 


<!-- End Of Response Status Modal -->
    
    
    
    
    
    
    
    </div>
    </section>
    <!-- Next form Fields E- Attachment  -->
    <!-- ===== Dyanmic Table Start Here ====== -->


    <!-- ===== Dynamic Table End Here ===== -->
    </div>
    <!-- page heading end-->

<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script src="assets/js/customJS/repair_validation.js"></script>



    </jsp:body>
</t:form-layout>