<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:form-layout>

	<jsp:attribute name="header">

<!-- custom css -->
<style>
.boxx {
	display: none;
}
.requiredField {
	color: red;
}
</style>
<!--pickers css-->
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datepicker/css/bootstrap-datepicker2.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-timepicker/css/timepicker.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-colorpicker/css/colorpicker.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">
    
 <!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-fileupload.min.css" />
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-fileupload.min.css" />
   
 </jsp:attribute>

    <jsp:attribute name="extrajs">
    
    
    <!-- tabletojson js -->

<script type="text/javascript" src="assets/js/tabletojson-js/jquery.tabletojson.js"></script> -->
<script type="text/javascript" src="assets/js/tabletojson-js/jquery.tabletojson.min.js"></script>

<!-- <script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/jquery-1.10.2.min.js"></script> -->
<script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker2.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/js/pickers-init.js"></script>
<script src="assets/js/pickers-init2.js"></script>
    
    
    <!--dynamic table-->
<script type="text/javascript" language="javascript"src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"	src="assets/js/data-tables/DT_bootstrap.js"></script>
<script	src="assets/js/vendor_dtable_init/vendor_manage_dynamic_table_init.js"></script>
<script	src="assets/js/vendor_dtable_init/contact_info_dynamic_table_init.js"></script>
<script	src="assets/js/vendor_dtable_init/payment_terms_dynamic_table.js"></script>
<script	src="assets/js/vendor_dtable_init/mis_report_dynamic_table_init.js"></script>

<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script src ="assets/js/customJS/vendor.js"></script>
<script src="assets/js/customJS/utility.js"></script>
    </jsp:attribute>
	<jsp:body>

      <!-- page heading start-->
        <div class="page-heading">
       
          <section class="panel">
                        <header class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs" id="myTab">
                              
                                <li class="active">
                                    <a href="#vendor_info" data-toggle="tab">Vendor Management</a>
                                </li>
                                <li>
                                    <a href="#contact_info" data-toggle="tab">Contact Information</a>
                                </li>
                               
                                <li>
                                    <a href="#payment_info" data-toggle="tab">Payment Terms</a>
                                </li>
                                <li>
                                    <a href="#mis_report" data-toggle="tab">Mis Report</a>
                                </li>
                                
                            </ul>
                        </header>

<!--====== first tab vendor Management starts====== -->
<%@include file="vendor/vendormanagement.jsp"%> 

<!-- Table starts -->

         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Vendor Information
        </header>
        <div class="panel-body">
        <div class="adv-table">
        <section id="flip-scroll">
                
         <table class="display table table-bordered "
							id="hidden-table-info">
        <thead class="cf">
        <tr>
           
           
            <th>Company Name</th>
            <th>Vendor Name</th>
            <th>Buisness Description</th>
            <th>Company Phone</th>
            <th>Company Fax</th>
            <th style="display:none">Service Tax No</th>
            <th style="display:none">Tin</th>
            <th style="display:none">Identification No</th>
            <th style="display:none">State Of Incorporation</th>
            <th style="display:none">Year Of Incorporation</th>
            <th style="display:none">Preffered Delivery Method</th>
            <th style="display:none">Cancel Check</th>
            <th style="display:none">Pan Card</th>
            <th style="display:none">Declaration Letter </th>
            <th style="display:none">Escalation Attachment</th>  
            <th style="display:none">City</th>
            <th style="display:none">Street</th>
            <th style="display:none">Locality</th>
            <th style="display:none">Pin</th>
            <th style="display:none">State</th>
            <th style="display:none">Country</th>
            <th style="display:none">MCity</th>
            <th style="display:none">MStreet</th>
            <th style="display:none">MLocality</th>
            <th style="display:none">MPin</th>
            <th style="display:none">MState</th>
            <th style="display:none">MCountry</th>
            <th style="display:none">CheckBox</th>
            
            
            <th style="display:none">ID</th>
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>
     <!-- body of the table starts-->  

  <c:forEach items="${vendorManagBean.vendorInfoDataList}" var="vendor" varStatus="count">
  <tr id="vendor${count.index}" >
 
  <td><c:out value="${vendor['company']}"></c:out></td>
  <td><c:out value="${vendor['name']}"></c:out></td>
  <td><c:out value="${vendor['bussinessDescription']}"></c:out></td>
  <td><c:out value="${vendor['phone']}"></c:out></td>
  <td><c:out value="${vendor['fax']}"></c:out></td> 
  <td style="display:none"><c:out value="${vendor['servicetax'] }"/></td>
  <td style="display:none"><c:out value="${vendor['tin'] }"/></td>
  <td style="display:none"><c:out value="${vendor['einssn'] }"/></td>
  <td style="display:none"><c:out value="${vendor['stateOfIncorporation'] }"/></td>
  <td style="display:none"><c:out value="${vendor['yearOfIncorporation'] }"/></td>
  <td style="display:none"><c:out value="${vendor['preffDeliveryMethod'] }"/></td>
  <td style="display:none"><c:out value="${vendor['cancelCheck'] }"/></td>
  <td style="display:none"><c:out value="${vendor['panCard'] }"/></td>
  <td style="display:none"><c:out value="${vendor['declLetter'] }"/></td>
  <td style="display:none"><c:out value="${vendor['escalAttach'] }"/></td>  
  <td style="display:none"><c:out value="${vendor['addressId']['city']}"/></td> 
  <td style="display:none"><c:out value="${vendor['addressId']['street']}"/></td> 
  <td style="display:none"><c:out value="${vendor['addressId']['locality']}"/></td> 
  <td style="display:none"><c:out value="${vendor['addressId']['pin']}"/></td> 
  <td style="display:none"><c:out value="${vendor['addressId']['state']}"/></td> 
  <td style="display:none"><c:out value="${vendor['addressId']['country']}"/></td> 
  <td style="display:none"><c:out value="${vendor['address']['city']}"/></td> 
  <td style="display:none"><c:out value="${vendor['address']['street']}"/></td> 
  <td style="display:none"><c:out value="${vendor['address']['locality']}"/></td> 
  <td style="display:none"><c:out value="${vendor['address']['pin']}"/></td> 
  <td style="display:none"><c:out value="${vendor['address']['state']}"/></td> 
  <td style="display:none"><c:out value="${vendor['address']['country']}"/></td> 
  <td style="display:none"><c:out value="on"/></td>
          
 
  <td style="display:none"><c:out value="${vendor['id'] }"/></td>
  <td><button class="btn btn-warning callEdit" type="button" onclick="callVendorEditFunction(vendor${count.index})"><i class="fa fa-hand-o-right"></i>Edit</button></td>       
  </tr>
 
  </c:forEach> 
    
     
     <!--  body of the table ends  -->         
         </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div> 

<!-- Table end -->
</div>
 <!-- ========first End Tab ==========================-->                                                                                                     
<!-- ========Second Start Tab (contact)========================= -->
 <%@include file="vendor/contactinformation.jsp"%> 
 
 <!-- Table starts -->

         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Contact Information
        </header>
        <div class="panel-body">
        <div class="adv-table">
        <section id="flip-scroll">
                
         <table class="display table table-bordered "
							id="contact_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Name</th>
            <th>Salutation</th>
            <th>Department</th>
            <th>Designation</th>
            <th>State Of Incorporation</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
     <!-- body of the table starts-->  
    
  <c:forEach items="${vendorManagBean.contactDetailsList}" var="contact">
   <c:if test="${contact['active']== '1'}">
   <tr id="cid${contact['id']}">
  <td><c:out value="${contact['name']}"></c:out></td>
  <td><c:out value="${contact['salutation']}"></c:out></td>
  <td><c:out value="${contact['department']}"></c:out></td>
  <td><c:out value="${contact['designation']}"></c:out></td> 
  <td><c:out value="${contact['stateOfIncorporation']}"></c:out></td>  
   <td><button class="btn btn-warning" type="button" onclick="callContactInactive(${contact['id']})"><i class="fa fa-hand-o-right"></i>Inactive</button></td>    
  </tr>
  </c:if>
  </c:forEach> 
    
     
     <!--  body of the table ends  -->         
         </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div> 

<!-- Table end -->
</div>
<!-- ========Second End Tab (contact) ==========================-->
<!-- ========Third Start Tab (paymentInformation)========================= -->
 <%@include file="vendor/paymentinformation.jsp"%> 
 
 <!-- Table starts -->

         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Payment Terms
        </header>
        <div class="panel-body">
        <div class="adv-table">
        <section id="flip-scroll">
                
         <table class="display table table-bordered "
							id="payment_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Shipping Method</th>
            <th>Shipping Terms</th>
            <th>Penalty Terms</th>
            <th>Payment Terms</th>
            <th>Advance</th>
            <th>Action</th>
            
        </tr>
        </thead>
        <tbody>
     <!-- body of the table starts-->  
    
  <c:forEach items="${vendorManagBean.paymentDetailsList}" var="payment">
  
  <c:if test="${payment['isActive']== '1'}">
   <tr id="pid${payment['id']}">
  <td><c:out value="${payment['shippingMethod']}"></c:out></td>
  <td><c:out value="${payment['shippingTerms']}"></c:out></td>
  <td><c:out value="${payment['penaltyTerms']}"></c:out></td>
  <td><c:out value="${payment['paymentTerms']}"></c:out></td>
  <td><c:out value="${payment['advance']}"></c:out></td>    
  <td><button class="btn btn-warning" type="button" onclick="callPaymentInactive(${payment['id']})"><i class="fa fa-hand-o-right"></i>Inactive</button></td>   
  </tr>
  </c:if>
  </c:forEach> 
    
     
     <!--  body of the table ends  -->         
         </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div> 

<!-- Table end -->
</div>
<!-- ========Third end Tab========================= -->
<!-- ========Fourth tab starts (MIS Report)======= -->					
					 <div class="tab-pane " id="mis_report">
            
                <header class="panel-heading custom-tag dark-tab">
   <ul class="nav nav-tabs">
   <li class="active">
     <div>   
     <table style="width: 1000px;">
       <tr>
       <td> <label>Select Services <span
										class="requiredField"> *</span></label>
       </td>
								<td><div class="form-group" style="float: right;">
                                 <div class="col-lg-12">
                                <select class="form-control select2"
												id="misReport" name="misReport">
                              
                                <!-- <option value="PO Info" id="poInfo">PO Information</option> -->
                                <option value="Billing MIS" id="billingMIS">Billing MIS</option>
                                <option value="Commercial MIS" id="commMIS">Commercial MIS</option>
                                <option value="Contract Management" id="contManag">Contract Management</option>
                              </select>
                                                </div>
   </div></td>
       </tr>
    </table>
    </div>
    </li>
    </ul>
   </header>
        </br>    
<%@include file="vendor/misreport.jsp"%> 
<!-- Table starts -->


<!-- Table end -->

    </div> 
<!-- ========Fourth tab ends======= -->	
    </div>
    </div>
    </section>
    </div>
 
    </jsp:body>
</t:form-layout>