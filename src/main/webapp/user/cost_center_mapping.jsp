<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:layout>
<jsp:attribute name="header">
<style>
     .boxx
     { 
     
        display : none; 
     }

     .requiredField 
    {
       color: red;
    }
    {
        color:red;
    }

</style>


<!--ios7-->
    <link rel="stylesheet" type="text/css" href="assets/js/ios-switch/switchery.css" />
          <link rel="stylesheet" type="text/css" href="assets/js/jquery-multi-select/css/multi-select.css" />


 <!--pickers css-->
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datepicker/css/datepicker-custom.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-timepicker/css/timepicker.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-colorpicker/css/colorpicker.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">


<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-fileupload.min.css" />

    </jsp:attribute>
    
    <jsp:attribute name="extrajs">
    
   <script src ="assets/js/customJS/cost_center_mapping.js"></script>
   
    
    <!--dynamic table-->
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>


<!--multi-select-->
<script type="text/javascript" src="assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="assets/js/multi-select-init.js"></script>
<script 	src="assets/js/dynamic_table_init.js"></script>

  
    
    
    </jsp:attribute>
    <jsp:body>
         
                 <!-- page heading start-->
        

        <div class="page-heading">
                            
        
        
              </div>
    <!-- page heading end-->

    <!--body wrapper start-->
        <div class="wrapper">
           <!--  Body contents goes here -->




<div class="row">

<div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Cost Center Mapping
                        
                    </header>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form"  id="ccMapForm" action="ManageDropDownServlet" method="POST">
                             
                             
                              <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <div class="col-sm-5">
  
                                <label>Department</label>
                          </div>
                           <div class="col-sm-7">
                            <select class="form-control" id="departMent" name="department">
                               <option value="">select...</option>
                                 <c:forEach items="${costCenterMapBean.departmentList}" var="department">
                               
                               <option><c:out value="${department}"></c:out></option>
                                </c:forEach>
                                
                              </select>     
                              
                                                  
                                </div>
                                
                           </div>
                           </div>
                                             
                                                 
                           
                           
                           
                           
                           
                           
                        <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <div class="col-sm-5">
  
                                <label>Cost Center</label>
                          </div>
                           <div class="col-sm-7">
                            <select class="form-control" id="costCenter" name="costCenter">
                               <option value="">select...</option>
                               <c:forEach items="${costCenterMapBean.costCenterDropDown}" var="costCenter">
                               
                               <option><c:out value="${costCenter}"></c:out></option>
                                </c:forEach>
                              </select>     
                              
                                                  
                                </div>
                                
                           </div>
                           </div>
  
                           
                           
                           <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <button type="submit" class="btn btn-info"   name="action" value="MAPCOSTCENTDEPT" ><i class="fa fa-hand-o-right"></i> Assign</button>
                                
                           </div>
                           </div>
                             
                            
                        </form>
                    </div>
                    
                    
   <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           USER VIEW
           
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
								id="hidden-table-info">
        <thead class="cf">
        <tr>
            
            <th class="hidden-phone">Department</th>
            <th>Cost Center</th>
             <th >Action</th>
            
        </tr>
        </thead>
        <tbody>
        
          <c:forEach var="costdept" items="${costCenterMapBean.dropCostCenterMap}">
     <tr class="gradeX" id="deptCost${costdept['deptCostCenterMapID']}">
 <td class="hidden-phone"><c:out value="${costdept['department'] }" /></td>
  <td><c:out value="${costdept['costCenterCode']}"></c:out></td>
 <td><button type="button" class="btn btn-info removeButton"  onclick="removeDeptCostCentre(${costdept['deptCostCenterMapID']})"  ><i class="fa fa-hand-o-right"></i> Remove</button>
 </td>
     </tr>
        </c:forEach>
                   
  </tbody></table></section></div></div>                  
    
   </section>
      </div>
         </div>


        </div>
        <!--body wrapper end-->
        
    </jsp:body>
</t:layout>