<%@page import="com.admin4u.views.TourAndTravelBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="javax.inject.Inject"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:layout>
<jsp:attribute name="header">
<style>
     .boxx
     { 
     
        display : none; 
     }

     .requiredField 
    {
       color: red;
    }
    {
        color:red;
    }

</style>

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-fileupload.min.css" />

    </jsp:attribute>
    
    <jsp:attribute name="extrajs">
    
    <!--dynamic table-->
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>

 <script src ="assets/js/customJS/approveauthority.js"></script>
<script		src="assets/js/dynamic_table_init.js"></script>
 
   <script src="assets/js/customJS/utility.js"></script>
  

<script>



</script>
    
    </jsp:attribute>
    <jsp:body>
    <div class="panel">
    <div class="panel-body">
    
    
    	<h5 class="title">Notifications</h5>
                        <ul class="dropdown-list normal-list">
                        
             <c:forEach items="#{notification.allNotificationList}" var="notifi">
             <li class="new">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="name"><c:out value="${notifi['message']}"/>
                                        </div>
                                        <div class="col-md-6 time-align">
                                              <c:out value="${notifi['creationTime']}"/></span>
                                        </div>
                                    </div>
                                    

                                  
                                    </li>
                                    
             
             </c:forEach>  
             </ul>
             </div>
    
    </div>    
    </jsp:body>
</t:layout>