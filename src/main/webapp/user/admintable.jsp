<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="javax.inject.Inject"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:admin-layout>
	<jsp:attribute name="header">
<style>
.boxx {
	display: none;
}

input
{
 text-transform: capitalize;
}


.requiredField {
	color: red;
}

.datepicker {
	z-index: 9999 !important;
}

#requestDataModal .modal-dialog {
	width: 60%;
	margin: 10px auto !important;
}

#requestDataModal .modal-body {
	padding: 0px !important;
	line-height: 16px !important;
	font-size: 12px !important;
	margin-left: 50px;
	margin-top: 20px;
	color: #000000 !important;
}

.admin_modal .modal-dialog {
	width: 61%;
	margin: 10px auto !important;
}

.admin_modal .modal-body {
	padding: 0px !important;
	line-height: 16px !important;
	font-size: 12px !important;
	margin-left: 50px;
	margin-top: 00px;
	color: #000000 !important;
}

.form-control {
	font-size: 12px;
	padding: 7px;
}

.adv-table table.display thead th {
	white-space: nowrap;
	vertical-align: middle;
	padding-left: 14px;
	padding-right: 14px;
	background-color: #455A64;
	color: #fff;
}
</style>

   <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datepicker/css/bootstrap-datepicker2.css">
 
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-timepicker/css/timepicker.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-colorpicker/css/colorpicker.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">
  
<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css"
			href="assets/css/bootstrap-fileupload.min.css" />

    </jsp:attribute>

	<jsp:attribute name="extrajs">
    
    <script type="text/javascript"
			src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker2.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/js/pickers-init.js"></script>
<script src="assets/js/pickers-init2.js"></script>
   
    <!--dynamic table-->
<script type="text/javascript" language="javascript"
			src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"
			src="assets/js/data-tables/DT_bootstrap.js"></script>
<script src="assets/js/dynamic_table_init.js"></script>
<!--pickers plugins-->
<script src="assets/js/customJS/admintable.js"></script>
<script src="assets/js/customJS/space_management.js"></script>

<script src="assets/js/customJS/utility.js"></script>


<script type="text/javascript">
  $("#assignTo").change(function(){
    if(this.value=="other_spoc"){
      document.getElementById('selectOtherSpoc').style.display = 'block';
    }
    else{
      document.getElementById('selectOtherSpoc').style.display = 'none'; 
    }
  });


//*********



  
</script>
<script>
$(".opendialog-reject").click(function () {
	$('#closeMsg').val("");
    $('#actionid').val($(this).data('id'));
   // $('#admin_modal_reject').modal('show');
      $('#admin_modal_close').modal('show');
});
/* $(".opendialog-extend").click(function () {
	$('#extendMsg').val(" ");
	$('#extendTimeDate').val("");
    $('#extendactionId').val($(this).data('id'));
    $('#extend_modal').modal('show');
}); */


</script>

<script>
$(".opendialog-assign").click(function () {
    $('#actionId').val($(this).data('id'));
    $('#admin_modal_assign').modal('show');
    getAllSpocByLocation($(this).data('id2'));
});
</script>
<script>
$(".opendialog-accept").click(function () {
    $('#actionID').val($(this).data('id'));
    $('#admin_modal_accept').modal('show');
});
</script>

<script>
$(".opendialog-allocate").click(function () {
    $('#spaceActionId').val($(this).data('id'));
    $('#spaceAction').val("ADMIN_ALLOCATE");
    $('#admin_modal_allocate_seat').modal('show');

    var actionId=$(this).data('id');
	var action="getEmployeeUpn";
	var url="SMSpaceAllocationServlet";
	var jsonData={
			actionId:actionId,
			action:action,
	}
	//$('#seatDetailTableTbody').html("");
	  $.ajax({
	      url: url,
	      type: 'POST',
	      data: jsonData,
	      dataType: "json",
	      ContentType: "application/json",
	      success: function(result){
	           console.log(result['employeeUPN']);
	           
                
			$('#allocEmp').html('<option value="'+result['employeeUPN']+'" selected>'+result['employeeName']+'</option>')
	            
	        }}); 
    
});



</script>


    </jsp:attribute>
	<jsp:body>
     <input type="hidden" id="modalRequestId"></input>
                        <input type="hidden" id="modalActionId"></input>
         
         <div class="row">
         
        <div class="col-sm-12" style="background-color: #eff0f4;">
        <section class="panel">
        <header class="panel-heading">
          Admin

        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
							id="admin_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Request ID/Workorder</th>
            <th class="hidden-phone">Request By</th>
            <th class="hidden-phone">Service</th>
            <th class="hidden-phone">Request Status/Priority</th>
            <th>Request For</th>

            <th>Assign</th>
            <th>Action</th>
            <th>Close</th>


        </tr>
        </thead>
        <tbody>
        <c:forEach items="${admin.ADMINDataList}" var="request1">
   <tr
										class="<c:if test="${request1['isRead']==false}"> <c:out value="gradeX"/></c:if> <c:if test="${request1['isRead']==true}"> <c:out value="seen"/></c:if>"
										id="rid${request1['actionId']}">
            <td><a class="showRequestDataModal" href="javascript:;"
											onclick="callShowRequestData(<c:out value="${request1['requestID']}">
            </c:out>, '<c:out value="${request1['service']}" />')">
           
         <c:choose>
    <c:when test="${not empty request1['workorder']}">
        <c:out value="${request1['workorder']}"></c:out>
    </c:when>    
    <c:otherwise>
    <c:out value="${request1['requestID']}"></c:out>
    </c:otherwise>
       </c:choose>
										</a></td>
       
              <td class="hidden-phone"><c:out
												value="${request1['requester']}" /> </td>
            <td class="center hidden-phone"><c:out
												value="${request1['requestType']}" /></td>
            <td class="center hidden-phone">

  <c:choose>
		<c:when test="${request1['requestStatus'] =='AMEND'}">
		<a class="showResponseDataModal" href="javascript:;"
														onclick="callShowAmendMsg('<c:out value="${request1['requestID']}"></c:out>')"><c:out
															value="${request1['status']}" /></a>
       </c:when>   
       <c:otherwise>
	      <c:out value="${request1['status']}" />
       </c:otherwise>
			    
</c:choose>         
         
            <c:if test="${not empty request1['priority']}">/<c:out
													value="${request1['priority']}" />
											</c:if>
            </td>
<td><c:out value="${request1['allServices']}" /></td>

 <%-- <td><button class="btn btn-success opendialog-reject <c:if test="${request1['isComplete']==true||request1['isActive']==true}"> <c:out value="disabled "/></c:if>" type="button" data-toggle="modal" data-id="${request1['actionId']}" ><i class="fa fa-hand-o-right"></i> Close</button></td> --%>
  <td><button
												class="btn btn-warning opendialog-assign <c:if test="${request1['isComplete']==true}"> <c:out value="disabled "/></c:if>"
												type="button" data-toggle="modal"
												data-id="${request1['actionId']}"
												data-id2="${request1['location']}">
												<i class="fa fa-hand-o-right"></i> Assign</button></td>
 
 
 <td>
 <c:if test="${admin.approvalProcessList.contains(request1['service'])}">
 <button
													class="btn btn-info bookTicketButton <c:if test="${request1['isComplete']==true}"> <c:out value="disabled "/></c:if>"
													data-toggle="modal" type="button"
													value="${request1['requestID']}"
													data-id="${request1['actionId']}">
													<i class="fa fa-hand-o-right"></i> Book Ticket</button>
 </c:if>
 <c:if
												test="${admin.nonApprovalProcessList.contains(request1['service'])}">
 <button
													class="btn btn-info opendialog-extend<c:if test="${request1['isComplete']==true||request1['isExpire']==true}"> <c:out value="disabled "/></c:if>"
													type="button" value="ADMIN_COMPLETE"
													data-id="${request1['actionId']}" onclick="getExtendComments(${request1['requestID']},${request1['actionId']})">EXTEND</button>
 </c:if>
 
 
 <!-- start  code for space management-->
 <c:if test="${request1['service']=='SPACEMANAGEMENT'}">
 <button
													class="btn btn-info opendialog-allocate <c:if test="${request1['isComplete']==true || request1['status']=='ALLOCATED'}"> <c:out value="disabled "/></c:if>"
													type="button" data-toggle="modal" value="ADMIN_ALLOCATE"
													data-id="${request1['actionId']}">Allocate</button>
 </c:if>
 <!-- end spaceManagement code -->
 
 <!-- start  code for meeting/Conference room management-->
 <c:if test="${request1['service']=='MEETINGROOMMANAGEMENT'}">
 <button
													class="btn btn-info opendialog-approve <c:if test="${request1['isComplete']==true || request1['status']=='ALLOCATED'}"> <c:out value="disabled "/></c:if>"
													onclick="calladminServletApprove('ADMIN_APPROVE','${request1['actionId']}')"
													type="button" data-toggle="modal" value="ADMIN_APPROVE"
													data-id="${request1['actionId']}">Approve</button>
 
 </c:if>
 <!-- end meeting/Conference room code -->
 
  <c:if test="${request1['service']=='INVENTORY'}">
<a href="#" data-toggle="modal" data-target="#issueItemPerCategory"
													id="issueItems"><button class=" btn btn-warning"
														type="button" onclick="callShowRequestData(130)"
														name="action" value="">
														<i class="fa fa-hand-o-right"></i>Assign</button></a>
 <a href="#" data-toggle="modal" data-target="#rejectItemReq"
													id="rejectReqItem"><button class=" btn btn-warning"
														type="button" onclick="" name="action" value="">
														<i class="fa fa-hand-o-right"></i>Reject</button></a> 
 </c:if>
 
 
 </td>
 <td><button
												class="btn btn-success opendialog-reject <c:if test="${request1['isComplete']==true||request1['isActive']==true}"> <c:out value="disabled "/></c:if>"
												type="button" data-toggle="modal"
												data-id="${request1['actionId']}">
												<i class="fa fa-hand-o-right"></i> Close</button>

      <!-- start of modal -->
 <!-- Modal -->
  <div class="modal fade admin_modal"
												id="admin_request_user_cnf_modal${request1['actionId']}"
												role="dialog">
    <div class="modal-dialog " style="width: 75%;">
    
      <!-- Modal content-->
      <form enctype="multipart/form-data" id="admin_response_form"
														action="adminauth?requestID=${request1['requestID']}"
														method="post">
      <div class="modal-content" style="height:180vh">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Details</h4>
        </div>
        <div class="modal-body" style="min-height:500px !important;height:auto !important;overflow:auto !important;">
       
        <div class="panel-body">
        
                                        <div class="adv-table">
                                        <div
																			id="hidden-table-info_wrapper"
																			class="dataTables_wrapper form-inline" role="grid">
                                <!-- =====Passenger Confirmation Details Start==== -->
                                <div class="table">
    
<c:if test="${request1['serviceList'].contains('TRAINTICKET') ||request1['serviceList'].contains('AIRTICKET') ||  request1['serviceList'].contains('HOTEL') || request1['serviceList'].contains('CABDISPOSAL') ||request1['serviceList'].contains('CABAIRPORT')||request1['serviceList'].contains('CABDROP')||request1['serviceList'].contains('CABPICK')||request1['serviceList'].contains('GUEST_HOUSE_BOOKING')||request1['serviceList'].contains('HOTEL_BOOKING') }">  
  <br />

  
      <%@include file="services/services_details.jsp"%>   
</c:if>
<!-- ends -->
      <c:if test="${request1['serviceList'].contains('TRAINTICKET') ||request1['serviceList'].contains('AIRTICKET') ||request1['serviceList'].contains('CAB') ||request1['serviceList'].contains('CABDISPOSAL') ||request1['serviceList'].contains('CABAIRPORT')||request1['serviceList'].contains('CABDROP')||request1['serviceList'].contains('CABPICK') }">
<%-- 
    <div class="row">                              


          </div>  </div>
                  <input type="hidden" class="admin-input" value="${request1['actionId']}" name="actionId" id="req${request1['actionId']}"></div>
          </div>  --%> 
          
          
          <div class="row">
  <div class="col-md-6"></div>
       <div class="col-md-6">
																							<label for="t_type">upload ticket</label>
 <input type="file" name="upload[]" id="adminUpload" class="admin-input" multiple="multiple" />
    <div id="uploadFileName"> </div>   
                  <input type="hidden" class="admin-input" value="${request1['actionId']}" name="actionId"
									id="req${request1['actionId']}" />
          </div>
          
  </div>  
          
          </c:if>
          
    <div class="col-md-5" style="margin-top: -18px;">
		<label class="control-label">Comments</label>
        	<textarea class="form-control" rows="2" cols="50"
																					id="admin_comment${request1['actionId'] }"
																					name="comment"></textarea>
     </div>
    
 
      
          
     
                 <c:if test="${request1['serviceList'].contains('TRAINTICKET') ||request1['serviceList'].contains('AIRTICKET') ||request1['serviceList'].contains('CAB')|| request1['serviceList'].contains('CABDISPOSAL') ||request1['serviceList'].contains('CABAIRPORT')||request1['serviceList'].contains('CABDROP')||request1['serviceList'].contains('CABPICK')||request1['serviceList'].contains('HOTEL')||request1['serviceList'].contains('GUEST_HOUSE_BOOKING')||request1['serviceList'].contains('HOTEL_BOOKING')}">
           
          
	          <div class="col-md-5" style="margin-top: 8px;">
					<button type="submit" class="btn btn-primary" name="action"
																					value="ADMIN_SERVE">Submit</button>
			  </div>														
          
          </c:if></div></div>
                                    </div>
                                    </div>
        </div>
        </div>
     
        

         <!-- Data Collapse Start-->
         <!--  <div class="panel panel-success">
                        <div class="panel-heading heading-style">
                            <h3 class="panel-title">PAN India CAB Information Details</h3>
                        </div>
                      <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                           
                            <div class="well well-style">
                                <address>
                                     <div class="adv-table">
                <section id="flip-scroll">
         <table class="display table table-bordered mybody" height="10px">
        <thead class="cf cf-style">
           <tr class="mytd">
            <th>Designation</th>
            <td>Executive-Sr. Executive</td><td> Assistant Manager</td><td> Deputy Manager</td> <td> Manager-Sr.Manager</td><td> AGM-DGM</td><td> GM-Sr. GM</td><td> AED</td><td> DED</td><td> ED-Sr.ED</td><td>  CEO-Group CEO</td><td> DMD-MD</td><td>  External</td><td>  External</td><td> External</td>
        </tr>
        <tr class="mytd">
          <th>Primary Entitlement</th>
           <td>Indica/Indigo</td><td> Indica/Indigo</td><td> Dzire</td><td> Dzire</td><td> Dzire/Etios</td><td> Innova</td><td>  Innova</td><td>  Innova</td><td>  Toyota Corolla/Sunny</td><td>  Camry/Honda Accord</td><td>  Camry</td><td> fortuner</td><td>  Audi</td><td>  BMW</td>
        </tr>
        </thead>
        <tbody class="cb cb-style">
        </tbody>
        </table>
        </section>
        </div>
        
                                </address>
        
                            </div>
                        </div>
                       
                    </div>
                    
                   
                </div>
      
      
          </div>
 -->






            
          <!-- Data Collapse End -->
        
												
												
												</div>
       
      </div>


      </form>
    </div>
    
  </div> 
 <!-- end of modal --> 
</td>
									</tr>
         </c:forEach>
                   </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div> 
        
        
         <!-- Modal when accept the item request by Spoc/Administration start-->
        <!--==================Assign Modal Start=============  -->
                  <div aria-hidden="true" aria-labelledby="myModalLabel"
			role="dialog" tabindex="-1" id="issueItemPerCategory"
			class="modal fade">
                        <div class="modal-dialog" style="width: 75%">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true"
							data-dismiss="modal" class="close" type="button">Ã—</button>
                                        <h4 class="modal-title">Issue Item Info</h4>
                                    </div>
                <div class="modal-body">
                                    <div class="row">
                        <div class="col-sm-12">
                <section class="panel">
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    <div class="btn-group pull-right">
                    </div>
                </div>
                <div class="space15"></div>
                <table
											class="table table-striped table-hover table-bordered"
											id="editableSample">
                <thead>
                <tr>
                    <th>Product Category</th>
                    <th>Product Brand</th>
                    <th>Product Name</th>
                    <th>Qty Required</th>
                    <th>Qty Issued</th>
                    <th>Total Price</th>
                </tr>
                </thead>
                <tbody>
                <tr class="adminTable">
                    
                </tr>
                </tbody>
                </table>
                </div>
                </div>
                </section>
                </div>
                </div>
                     <div class="row">
                             <div class="col-sm-12">
                                <div class="form-group"> 
                                   <center>
										<button class="btn btn-info" name="action" value="">
											<i class="fa fa-hand-o-right"></i>Issue</button>
									</center>
                                </div>
                            </div>
                          </div>
                </div>
                </div>
                </div>
                </div>

        <!--========== Assign end of modal============= -->   
        <!--start of modal  -->
              <div aria-hidden="true" aria-labelledby="myModalLabel"
			role="dialog" tabindex="-1" id="rejectItemReq" class="modal fade">
                <div class="modal-dialog" style="width: 75%">
                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true"
							data-dismiss="modal" class="close" type="button">Ã—</button>
                                        <h4 class="modal-title">Important Message</h4>
                                    </div>
                    <div class="modal-body">
                             <div class="row">
                             <div class="col-sm-12">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Message<span
											class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type="longtext"
											class="form-control" id="closeStRequest"
											name="closeStRequest" />
                            </div>
                        </div>
                        </div>
                            </div>
                            <div class="row">
                             <div class="col-sm-12">
                                <div class="form-group"> 
                                   <center>
										<button class="btn btn-info" name="action" value="">
											<i class="fa fa-hand-o-right"></i>Submit</button>
									</center>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
                </div>
                </div>

        <!-- end of modal --> 
        <!-- All Modal End -->
        
        <!-- Space management Modal -->
  <div class="modal fade" id="admin_modal_allocate_seat" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assign</h4>
        </div>
        <div class="modal-body">
        
            <div id="space_spoc">
            
										<div class="row">
										  <div class="col-sm-8 col-md-4">
										      <div class="form-group"> 
										          <div class="col-sm-12">
										              <label><b>Seat Allocation Information</b></label>
										          </div>
										      </div>
										  </div>
										</div>
                </br>
                <form action="SMSpaceAllocationServlet" method="post"
								id="allocateSpaceForm">
                <input type="hidden" id="spaceActionId"
									name="spaceActionId" />
                <input type="hidden" id="spaceAction" name="spaceAction" />
                
															<div class="row">
                                                             <div
										class="col-sm-8 col-md-4">
                                                                <div
											class="form-group"> 
                                                                    <div
												class="col-sm-5">
                                                                        <label>Location </label>
                                                                    </div>
                                                                    <div
												class="col-sm-7">
                                                                      <input
													type="text" readonly="" value="${userSession.locationName}"
													class="form-control" id="slocation">
                                                                    </div>
                                                                </div>
                                                            </div>

															 <div class="col-sm-8 col-md-4">
															    <div class="form-group"> 
															        <div class="col-sm-5">
															            <label>Building-Floor</label>
															        </div>
															        <div class="col-sm-7">
                                                                       <select
													class="form-control" id="buildingFloor"
													name="buildingFloor">
                                                                         <option
														value="">select Building</option>
                                                                        <c:forEach
														items="#{spaceManagement.buildingFloor}" var="entry">
                                                                        <option
															value="${entry.key}"><c:out
																value="${entry.value}"></c:out></option>
                                                                        
                                                                        </c:forEach>
                                                                        </select>
															        </div>
															    </div>
															</div>
															 <div class="col-sm-8 col-md-4">
															    <div class="form-group"> 
															     <div class="col-sm-5">
															            <label>Space Station Category</label>
															        </div>
															        <div class="col-sm-7">
															           <select class="form-control"
													id="spaceStationCat" name="seatType">
                                                                         <option
														value="">select Category</option>
                                      									  <c:forEach
														items="#{spaceManagement.spaceCategoryList }"
														var="spaceCategory">
                                         								<option
															value="${spaceCategory }"><c:out
																value="${spaceCategory }"></c:out></option>
                                       									 </c:forEach>
                                                                        </select>
															        </div>
															    </div>
															</div>
												


															</div>
                <br>


            <!-- ==================start Table =============== -->
                
                 <div class="wrapper">
        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Responsive table
                        <span class="tools pull-right">
                            <!-- <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a> -->
                         </span>
            </header>
            <div class="panel-body">
                <section id="unseen">
                    <table
													class="table table-bordered table-striped table-condensed"
													id="seatDetailTable">
                        <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>Building</th>
                            <th>Seat Type</th>
                            <th class="numeric">Total</th>
                            <th class="numeric"> Assigned</th>
                            <th class="numeric">Available</th>
                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="seatDetailTableTbody">
                       
                        
                       
                        </tbody>
                    </table>
                </section>
            </div>
        </section>
      
     
        </div>
        </div>
        </div>
        <div class="row">
                         <div class="col-sm-8 col-md-4">
                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <label>Allocate to Employee </label>
                                </div>
                                <!-- <div class="col-sm-7">
                                    <select class="form-control" id="allocEmp">
                                     <option value="">select Employee</option>
                                      <option value="12345">Manoj Singh (EmpID - 12345)</option>
                                    </select>
                                </div> -->
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-4">
                            <div class="form-group"> 

                                <div class="col-sm-12">
                                    <select class="form-control"
													id="allocEmp" name="employeeUPN">
                                     <option value="">select Employee</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="col-sm-8 col-md-4">
                            <div class="form-group"> 
                                <div class="col-sm-5">
                                    <label>Available Seat</label>
                                </div>
                                <div class="col-sm-7">
                                   <select class="form-control"
													id="availSeat" name="seatRoom">
                                     <option value="">Select Seat</option>
                                      
                                    </select>
                                </div>
                            </div>
                        </div>
                         
            


                        </div>

            <!-- ======================End Table ==================== -->
           


                
              </br> 

           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center>
												<button class="btn btn-info" name="action" value="ALLOCATE">
													<i class="fa fa-hand-o-right"></i> Submit</button>
											</center>
                        </div>
                    </div>
            </div>
            
</form>
          
          
    </div>
        
             </div>
        <div class="modal-footer">
          <center>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</center>
        </div>
      </div>
      
    </div>
  </div>
  


<!-- end of space management modal -->
        
        
        
        
        
        
	<!-- Modal -->
  <div class="modal fade" id="admin_modal_assign" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assign</h4>
        </div>
        <div class="modal-body">
        <div class="form-group">
    <label>Select Spoc:   </label><select
									class="form-control admin-input" name="assignTo" id="assignTo"
									onchange="getOtherSpoc(value)">
             <option value="">select....</option> 
             
             <c:forEach items="#{admin.employeeRoles}" var="role">
             <option value="${role.userPrincipalName}_ADMIN"> <c:out
												value="${role.location}_ADMIN"></c:out></option>
             </c:forEach> 
             <option value="other_spoc" id="otherSpoc">Other Spoc</option>
          </select>
          <!-- 
          <input type="text" name="search_spoc" onblur="setSelectVal()" style="display:none;" id="searchspoc"/> -->
          <div id="selectOtherSpoc" style="display: none;">
          <select class="form-control select2 col-md-12 setSpoc"
										onchange="setSelectVal()" id="searchspoc" name="search_spoc">
           </select>
								</div>
          </div>
          <input type="hidden" name="actionId" id="actionId"
								class="admin-input" />
          <input type="hidden" name="requestID" id="requestID"
								class="admin-input" />
        </div>
        <div class="modal-footer">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal"
									onclick="calladminServlet('ADMIN_FORWORD')">Assign</button>
							</center>
        </div>
      </div>
      </form>
    </div>
  </div>
  


<!-- end of modal -->
<!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="admin_modal_reject" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Close Comments</h4>
        </div>
         <form>
        <div class="modal-body">
        <table>
        <tr><td>
        <input type="hidden" name="actionId" id="actionid" class="admin"/>
          <label>Comments: &nbsp;  </label></td><td style="margin-top:10px;"><textarea rows="2" cols="30" class="admin" id="closeMsg" name="responseMsg" maxlength="200"></textarea></td>
<%-- =======
        <tr>
									<td>
        <input type="hidden" name="actionId" id="actionid" class="admin" />
          <label>Comments: &nbsp;  </label>
									</td>
									<td style="margin-top: 10px;"><textarea rows="2" cols="30"
											class="admin" id="closeMsg" name="responseMsg"></textarea></td>
>>>>>>> 4104bc9fc34c6b12014aeb15377927fad946f204 --%>
          </tr>
</table>
        </div>
        <div class="modal-footer">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal"
									onclick="calladminServletReject('ADMIN_CLOSE')">Submit</button>
							</center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>
  


<!-- end of modal -->
	
	<!-- =======================================start show request data  modal===================== -->
    
    
    <!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade " id="requestDataModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body" style="overflow: auto;">
             </div>
        <br>
        <div class="modal-footer sticky-footer"
							style="position: relative; margin-top: 0px !important">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">close</button>
							</center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of modal -->
    
    
    
    <!-- =====================================End ofshow request data  modal================= -->
    <!-- start of modal admin accept-->
                        
 <!-- Modal -->
  <div class="modal fade" id="admin_modal_accept" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Accept</h4>
        </div>
         <form>
        <div class="modal-body">
       
        <table>
        <tr>
									<td>
        <input type="hidden" name="actionId" id="actionID"
										class="admin-accept" />
          <label>Set Priority</label>
									</td>
          <td style="margin-top: 10px;"><select
										class="admin-accept" name="priority">
          <option value="">select...</option>
          <option value="HIGH">High</option>
          <option value="NORMAL">Normal</option>
          <option value="LOW">Low</option>          
          </select></td>
          <td>
          
          
          </td>
          </tr>
</table>

        </div>
        <div class="modal-footer ">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal"
									onclick="calladminServletAccept('ADMIN_ACCEPT')">ACCEPT</button>
							</center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>
  


<!-- end of modal -->

<!--Amend msg  Modal -->
  <div class="modal fade" id="amendResponseMsgModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">	
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Amend Message</h4>
        </div>
        <div class="modal-body">
        
        
             </div>
        <br>
        <div class="modal-footer"
							style="margin-top: 20px; position: relative;">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">close</button>
							</center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of Amend msg modal -->


 <!-- modal for Close  starts --> 
   <div class="modal fade" id="admin_modal_close" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Close Request</h4>
        </div>
        <div class="modal-body">
                <input type="hidden" name="requestId" id="requestid"
								class="user" />
                 <center>
								<h5>
									<b>Are you sure you want to close the request ?</b>
								</h5>
							</center>
        </div>
        <br>
        <div class="modal-footer"
							style="position: relative; margin-top: 0px !important">
          <button type="button" class="btn btn-danger"
								data-toggle="modal" data-target="#admin_modal_reject"
								data-dismiss="modal">Yes</button>
          <button type="button" class="btn btn-default"
								data-dismiss="modal">No</button> 
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for  cancel ends -->  

 
 <!--  complete or extend modal -->
                        
<div class="modal fade" id="extend_modal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Extend Data</h4>
                   <input type="hidden" name="actionId" id="extendactionId" class="extend admin-extend" />
          		<input type="hidden" name="extendEventService" id="extendEventService" class="extend admin-extend" />
          			
          
        </div>
         <form id="houseKeeping" style="display: none;">
        <div class="modal-body" style="overflow: auto;">
<div class=row>
   <div class="col-sm-8 col-md-8">
                <div class="form-group"> 
    <div class="col-sm-5">
        <label>Extension Date & Time</label>
    </div>
    <div class="col-sm-7">
        <select   id="extendTimeDate" name="extendTimeDate" class="form-control" placeholder="Extend Date & Time">
        
        </select>
													
													
      
    </div>
</div>
            </div>
            </div>  
            <br />    
<div class="row">
   <div class="col-sm-8 col-md-8">
    <div class="form-group"> 
    <div class="col-sm-5">
     <label>Comments: &nbsp;  </label>
										</div>
     <div class="col-sm-7">
    <textarea rows="2" cols="30" class="extend admin-extend" id="extendMsg" name="extendMsg" maxlength="200"></textarea>
</div>
</div>
								</div>
        </div>
        </div>
        <br />
        <div class="modal-footer sticky-footer"
							style="position: relative; margin-top: 0px !important">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal"
									onclick="callAdminExtend('HOUSEKEEPING')">Submit</button>
							</center>
        </div>
        </form>
        
        <!-- repair maintenence extend form start -->
        <form id="repairMaintenance" style="display: none;">
        <div class="modal-body" style="overflow: auto;">
<div class=row>
   <div class="col-sm-8 col-md-12">
                <div class="form-group"> 
    <div class="col-sm-5">
        <label>Comment & Time</label>
    </div>
    <div class="col-sm-7">
        <select  id="extendTimeDate1" name="extendTimeDate" class="form-control" >
        <option value="">Select comment &amp; time...</option>
        </select>
      
    </div>
</div>
            </div>
            </div>  
            <br />    
<%-- <div class="row">
   <div class="col-sm-8 col-md-8">
    <div class="form-group"> 
    <div class="col-sm-5">
     <label>Comments: &nbsp;  </label>
										</div>
     <div class="col-sm-7">
    <textarea rows="2" cols="30" class="extend admin-extend" id="extendMsg" name="extendMsg" maxlength="200"></textarea>
</div>
</div>
								</div>
        </div>
 --%>        </div>
        <br />
        <div class="modal-footer sticky-footer"
							style="position: relative; margin-top: 0px !important">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal"
									onclick="callAdminExtend('REPAIRMAINTENANCE')">Submit</button>
							</center>
        </div>
        </form>
        
        <!-- repair maintenance extend form end -->
      </div>
      
    </div>
    
  </div>

  
<!-- complete or extend modal ends -->

  

	
	<!-- =======================================start show request data  modal===================== -->
	                  

  
        
    </jsp:body>
</t:admin-layout>
