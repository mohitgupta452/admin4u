<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:layout>
	<jsp:attribute name="header">
	

<style>
.boxx {
	display: none;
}

.requiredField {
	color: red;
}

#requestDataModal .modal-dialog {
	width: 60%;
	margin: 10px auto !important;
}

#requestDataModal .modal-body {
	padding: 0px !important;
	line-height: 16px !important;
	font-size: 12px !important;
	margin-left: 50px;
	margin-top: 20px;
	color: #000000 !important;
}

/* modal css starts here */
.modal-header
{
	background: #263238;
    border-bottom: 6px solid #78909C !important;
}
#requestDataModal .modal-body
{
	margin-left: 20px !important;
    margin-right: 20px !important;
    border: 1px solid #CFD8DC !important;
    background: #F5F5F5;
}
.col-label {
	display: inline-block;
	margin-bottom: 5px;
	font-weight: 700;
	font-size: 13px;
	color: #E64A19;
	font-family: tahoma;
}
.panel
{
	margin-bottom: 30px !important;
}
</style>
<!--select2-->
 <link rel="stylesheet" type="text/css"
			href="assets/select2/select2.css" />
 <link rel="stylesheet" type="text/css"
			href="assets/select2/select2-bootstrap.css" />
<!--select2-->
 <!--pickers css-->
   <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datepicker/css/bootstrap-datepicker2.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-timepicker/css/timepicker.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-colorpicker/css/colorpicker.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">
<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css"
			href="assets/css/bootstrap-fileupload.min.css" />
  <link rel="stylesheet" type="text/css"
			href="assets/css/bootstrap-fileupload.min.css" />
    </jsp:attribute>
	<jsp:attribute name="extrajs">
       
<script>
$(".opendialog-amend").click(function () {
	$('#amendMsg').val(" ");
    $('#requestId').val($(this).data('id'));
    $('#amend_request_modal').modal('show');
});
</script>
<script>
$(".opendialog-reopen").click(function () {
	$('#reopenMsg').val(" ");
    $('#reopenrequestId').val($(this).data('id'));
    $('#reopen_request_modal').modal('show');
});

</script>
    <!--select2-->
     <script src="assets/select2/select.js"></script>
    <!--select2-->
    <script src="assets/js/customJS/utility.js"></script>
    <script src="assets/js/customJS/hotel_guesthouse.js"></script>
    <script src="assets/js/customJS/repair_validation.js"></script>
    <script src="assets/js/customJS/tour_travel.js"></script>
    <!--dynamic table-->
<script type="text/javascript" language="javascript"
			src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"
			src="assets/js/data-tables/DT_bootstrap.js"></script>
<script src="assets/js/dynamic_table_init.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>

<!--pickers plugins-->
<script type="text/javascript"
			src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker2.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/js/pickers-init.js"></script>
<script src="assets/js/pickers-init2.js"></script>

    </jsp:attribute>

	<jsp:body>


        <div class="page-heading">
        
        <!-- Cab/Ticket -->
          <section class="panel">
                        <!-- <header
				class="panel-heading custom-tab dark-tab">
                            
                        </header> -->

<!-- <input type="text" id="userName"
				value="${userSession.empData.displayName}" style="display: none;" />
<input type="text" id="userID" value="${userSession.empData.employeeID}"
				style="display: none;" />
<input type="text" id="userRole" value="${userSession.empRole[0]}" style="display: none;"/> -->



                    <!-- ==== Dynamic table Start here ===== -->
    <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <!-- <header class="panel-heading">
           USER VIEW
        </header> -->
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "  
								id="hidden-table-info">
        <thead class="cf">
        <tr style="background-color: rgb(91, 192, 222);
    color: black;">
            <th>Request ID</th>
            <th class="hidden-phone">Requester</th>
            <th class="hidden-phone">Request Type</th>
            <th class="hidden-phone">Request Date</th>
            <th class="hidden-phone">Status</th>
            <th>Service</th>
            <th>Action</th>
            <th>Download</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="userReq" items="${userRequest.requestMasters}">
<tr class="gradeX" id="rid${userReq['requestID'] }">

<td><a class="showRequestDataModal" href="javascript:;"
												onclick="callShowRequestData(<c:out value="${userReq['requestID']}"></c:out>, '<c:out value="${userReq['service']}" />')">
												<c:choose>
    <c:when test="${not empty userReq['workorder']}">
        <c:out value="${userReq['workorder']}"></c:out>
    </c:when>    
    <c:otherwise>
    <c:out value="${userReq['requestID']}"></c:out>
    </c:otherwise>
       </c:choose></a></td>
<td class="hidden-phone"><c:out value="${userReq['requester']}" /></td>
 <td class="center hidden-phone"><c:out
													value="${userReq['requestType']}" /></td>
<td class="center hidden-phone"><c:out
													value="${userReq['requestdate']}" /></td> 
<td class="center hidden-phone">
 <c:choose>
	<c:when test="${userReq['requestStatus'] =='COMPLETE'}">
    <a class="showResponseDataModal" href="javascript:;"
															onclick="callShowResponseData('<c:out value="${userReq['requestID']}"></c:out>','<c:out value="${userReq['service']}" />')"><c:out
																value="${userReq['requestStatus']}" /></a>
	</c:when>
	<c:when test="${userReq['requestStatus'] =='CLOSED'}">
	<a class="showResponseDataModal" href="javascript:;"
						onclick="callShowResponseMsg('<c:out value="${userReq['requestID']}"></c:out>')"><c:out
																value="${userReq['requestStatus']}" /></a>
    </c:when> 
    <c:when test="${userReq['requestStatus'] =='AMEND'}">
	<a class="showResponseDataModal" href="javascript:;"
					onclick="callShowAmendMsg('<c:out value="${userReq['requestID']}"></c:out>')"><c:out
																value="${userReq['requestStatus']}" /></a>
    </c:when>   
<c:otherwise> 
	<c:out value="${userReq['requestStatus']}" />
 </c:otherwise>
</c:choose> 

</td>
<td><c:out value="${userReq['service']}" /></td>
<td>
 <c:if
													test="${userRequest.approvalProcessList.contains(userReq['service'])}">
     <button style="background-color: #ccc;border-color: gray;color: #333;"
														class="btn btn-danger <c:if test="${userReq['requestStatus']=='CANCELLED' || userReq['requestStatus']=='COMPLETE' || userReq['complete']==true}"> <c:out value="disabled"/></c:if>"
														type="button"
														onclick="callCancelRequest('${userReq['requestID']}')">
    <i class="fa fa-hand-o-right"></i> Cancel</button>
 </c:if>
 <c:if
													test="${userRequest.nonApprovalProcessList.contains(userReq['service'])}">
     <c:if test="${userReq['requestStatus'] !='CLOSED'}">
<button class="btn btn-info opendialog-amend" type="button" name="action" value="Amend" data-toggle="modal" data-id="${userReq['requestID']}"  onclick="ammendRequest('${userReq['requestID']}')"> 
	<i class="fa fa-hand-o-right" ></i> Ammend</button>
	</c:if>
	<c:if test="${userReq['requestStatus'] =='CLOSED'}">
<button class="btn btn-info opendialog-reopen" type="button" name="action" value="Reopen" data-toggle="modal" data-id="${userReq['requestID']}" > 
	<i class="fa fa-hand-o-right" ></i> Reopen</button>
</c:if>
 </c:if>
</td>
<td>
 <c:if
													test="${userRequest.approvalProcessList.contains(userReq['service'])}">
     <a class="<c:if test="${userReq['requestStatus']!='COMPLETE'}"><c:out value="disableAnchor"/></c:if>"
														href="DownloadResponseServlet?fileName=${userReq['responseAttachment']}&requestId=${userReq['requestID']}"><button
															class="btn btn-success" type="button"
															<c:if test="${userReq['requestStatus']!='COMPLETE'}"><c:out value="disabled"/></c:if>>  
	 <i class="fa fa-hand-o-right"></i> Download</button></a>
</c:if>	 
</td>
   
   </tr>
        </c:forEach>
      </tbody> 
       
       
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div>

<!-- =====train ticket  panel  end-->

                <!-- ====Ticket Requests End Here -->
            

		
		</div>
            </div>
    <!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body" style="overflow: auto;">
        </div>
        
        
        <br>
       
        <div class="modal-footer sticky-footer"
						style="position: relative; margin-top:0px !important">
          <center>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">CLOSE</button>
						</center>
        </div>
      </div>
      
    </div>
  </div>
 


<!-- end of modal -->




<!-- Start Of  Response Data Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
  
  <form action="DownloadPDFServlet" method="POST">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Details</h4>
        </div>
        <div class="modal-body">
  
         </div>
       
       
        <div class="modal-footer"
							style="margin-top: 70px; position: relative;">
<center>								
</center>
        </div>
    
      </div>
    </form>
    </div>
    
  </div>
 


                        
 <!-- Modal -->
  <div class="modal fade" id="responseMsgModal" role="dialog">
    <div class="modal-dialog">
  
  <!-- <form action="DownloadPDFServlet" method="POST"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Response Message </h4>
        </div>
        <div class="modal-body">
  
         </div>
       
       
        <div class="modal-footer"
						style="margin-top: 10px; position: relative;">
<center>								
<button class="btn btn-warning " type="submit" data-dismiss="modal"> <i
									class="fa fa-hand-o-right"></i> Ok</button>
</center>
        </div>
    
      </div>
    </form>
    </div>
    
  </div>
 
<!-- End Of Response Message Status Modal -->

<!--======== datetimepicker modal for checkin checkout Starts ======= -->
 
        <div class="modal fade" id="travelDateModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
                 <center>
								<h4>
									<b>Your Tickect is not confirmed.<br /><br />
                          It may be cancelled .</b>
								</h4>
							</center>
        </div>
        <br>
        <div class="modal-footer"
							style="position: relative; margin-top: 0px !important">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">close</button>
							</center>
        </div>
      </div>
      </form>
    </div>
  </div>
 
           
 
  <!-- ======= datetimepicker modal for checkin checkout ends ====== -->  


    
  
</div>
    
<!-- start of Amend message inserting modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="amend_request_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Amend</h4>
        </div>
         <form>
         <input type="hidden" name="requestId" id="requestId" class="admin-ammend"/>
         
        <div class="modal-body" style="overflow:auto;">
        </div>
        <div class="modal-footer sticky-footer" style="position:relative;margin-top:0px !important">
          <center><button type="button" class="btn btn-default" data-dismiss="modal" onclick="callAmendRequest()">Submit</button></center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>
  


<!-- Amend message inserting modal ends -->


<!-- reopen modal starts -->
                        
 <!-- Modal -->
  <div class="modal fade" id="reopen_request_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reopen Comments</h4>
          
        </div>
         <form>
         <input type="hidden" name="requestId" id="reopenrequestId" value="" class="reopen admin-reopen"/>
        <div class="modal-body" style="overflow:auto;">
       
         <table>
        <tr><td>
        
          <label>Comments: &nbsp;  </label></td><td style="margin-top:10px;"><textarea rows="2" cols="30" class="reopen admin-reopen" id="reopenMsg" name="reopenMsg"></textarea></td>
          </tr>
</table>
        </div>
        <div class="modal-footer sticky-footer" style="position:relative;margin-top:0px !important">
          <center><button type="button" class="btn btn-default" data-dismiss="modal" onclick="reopenRequest()">Submit</button></center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>
  


<!-- reopen message  modal ends -->

<!-- reopen msg Modal -->
  <div class="modal fade" id="reopenResponseMsgModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reopen Comments</h4>
        </div>
        <div class="modal-body">
             </div>
        <br>
        <div class="modal-footer" style="margin-top: 20px;position:relative; ">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of reopen msg modal --> 
        
    </jsp:body>
</t:layout>