<%@page import="com.admin4u.views.TourAndTravelBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="javax.inject.Inject"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:approver>
<jsp:attribute name="header">
<style>
     .requiredField 
    {
       color: red;
    }
    
#requestDataModal .modal-dialog  
{
width:60%;
margin: 10px auto!important;
}

#requestDataModal .modal-body 
{
    padding : 0px!important;
    line-height : 16px !important;
    font-size : 12px !important;
    margin-left:50px;
    margin-top:20px;
    color:#000000 !important;

}
.panel {
	-webkit-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	background: #fff !important;
	border-top: 2px solid #546E7A;
	border-radius:0px !important;
	margin-bottom:0px !important;
}
.adv-table table.display thead th
{
    white-space: nowrap;
    vertical-align: middle;
    padding-left: 14px;
    padding-right: 14px;
    background-color: #455A64;
    color: #fff;
} 
</style>
<link href="assets/js/multiple-upload/css/style_multi.css" rel="stylesheet" />
<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-fileupload.min.css" />

    </jsp:attribute>
    
    <jsp:attribute name="extrajs">
    
    <script src="assets/js/multiple-upload/js/jquery.knob.js"></script>

		<!-- jQuery File Upload Dependencies -->
		<script src="assets/js/multiple-upload/js/jquery.ui.widget.js"></script>
		<script src="assets/js/multiple-upload/js/jquery.iframe-transport.js"></script>
		<script src="assets/js/multiple-upload/js/jquery.fileupload.js"></script>
		
		<!-- Our main JS file -->
		<script src="assets/js/multiple-upload/js/script.multi.upload.js"></script>

    
    
    <!--dynamic table-->
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>

 <script src ="assets/js/customJS/approveauthority.js"></script>
<script		src="assets/js/dynamic_table_init.js"></script>
  <script src="assets/js/customJS/utility.js"></script>
<script>

$(document).ready(function(){
$('#bookTicketButton').on('click',function(){
    $("#admin_request_user_cnf_modal").modal("show");
    
    $("#txtRequestData").append($(this).closest('tr').children()[10].innerHTML);
    
});

});
</script>
    </jsp:attribute>
    <jsp:body>
          <!-- ======temp approval authouriy tab== -->

<div class="tab-pane" id="approval_authourity">

                    <!-- ==== Dynamic table Start here ===== -->
                       <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           approval authourity
        </header>  
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered " id="app_auth_hidden-table-info" style="background-color:#fff;">
        <thead class="cf">
        <tr>
            <th>Request ID / Work Order</th>
            <th class="hidden-phone">Request By</th>
            <th>Service</th>
          <th class="hidden-phone">Request Type</th>
            <th class="hidden-phone">Status</th>
           <th class="hidden-phone">Attachment</th>
            <th>Action</th>
            <th>Reject</th>
        </tr>
        </thead>
        <tbody id="tblbody">
        
                <c:forEach items="${approveauth.HODDataList}" var="request">
        
                <tr class="<c:if test="${request['isRead']==false}"> <c:out value="gradeX"/></c:if> <c:if test="${request['isRead']==true}"> <c:out value="seen"/></c:if>" id="rid<c:out value="${request['actionId']}"></c:out>">
            <td><a class="showRequestDataModal" href="javascript:;"  onclick="callShowRequestData(<c:out value="${request['requestID']}">
            </c:out>,'${request['service']}')">
               <c:choose>
    <c:when test="${not empty request['workorder']}">
        <c:out value="${request['workorder']}"></c:out>
    </c:when>    
    <c:otherwise>
    <c:out value="${request['requestID']}"></c:out>
    </c:otherwise>
       </c:choose>
            </a></td>
                 <td class="hidden-phone"><c:out value="${request['requester']}" /></td>
            <td><c:out value="${request['allServices']}" /></td>
      
            <td class="center hidden-phone"><c:out value="${request['requestType']}" /></td>
            <td class="center hidden-phone"><div id="div1"><c:out value="${request['status']}" /></div></td>
            <td>
            <c:if test="${not empty request['approovalAttachment']}">
            <a target="_blank" href="fileviwer?path=<c:out value="${request['approovalAttachment']}" />">Link</a> </c:if></td>
            <td>
    
            <button class=" btn btn-warning <c:if test="${request['status']!='PENDING'}"> <c:out value="disabled "/></c:if> "type="button" onclick="callapproveauthServlet('APPROVAL_AUTHORITY_ACCEPT',${request['actionId']})" name="action"   value="APPROVAL_AUTHORITY_REJECT">
	<i class="fa fa-hand-o-right"></i>Accept</button></td>
            <td> 
          <button class="btn btn-info <c:if test="${request['status']!='PENDING'}"> <c:out value="disabled "/></c:if> "type="button" name="action" value="APPROVAL_AUTHORITY_REJECT" onclick="callapproveauthServlet('APPROVAL_AUTHORITY_REJECT',${request['actionId']})">
	<i class="fa fa-hand-o-right"></i>Reject</button>  
       
          
        </td>
        
        </tr>
         </c:forEach>
        </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div>
        </div>
        

<!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body" style=" overflow :auto;">
             </div>
        <br>
        <div class="modal-footer sticky-footer" style="position:relative; margin-top:0px !important">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      
    </div>
  </div>

<!-- ======================end temp approval authorityn tab  -->
                    
    </jsp:body>
</t:approver>
