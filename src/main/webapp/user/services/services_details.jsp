<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${request1['serviceList'].contains('AIRTICKET') || request1['serviceList'].contains('TRAINTICKET')}" >
          <div class="row">
                <div class="col-sm-12">
             <section class="panel">
                  <header class="panel-heading" style="padding:6px !important ; font-size :12px!important">
                    Ticket Booking Details
                                                    <!-- <span class="tools pull-right">
                                                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                                                        <a href="javascript:;" class="fa fa-times"></a>
                                                     </span> -->
                    </header>
              <div class="panel-body">
           <div class="adv-table">
             <div id="hidden-table-info_wrapper"
									class="dataTables_wrapper form-inline" role="grid">
                                  
                                <div class="table">
                                                     <div class="row">
                                       
                                        <div class="col-md-4">
					<label for="t_type">Booking Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> 
                         
                          <div class="form-group">
          <div class="input-group date adminBookingDate">                
                                <input size="16" type="text" id="bookingDate${request1['requestID']}" name="bookingDate" class=" form-control " readonly="true" placeholder="mm/dd/yyyy">
                     <span class="input-group-btn">
 <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
        </span>
        </div>
            </div>
                                </div>              
                                                     
                                                     
                                                     
                                                  <div class="col-md-4">
												<label for="t_type">Departure Date & time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                  
													<div class="input-group date departureDate">
                                            <input type="text" class="form-control" readonly="" size="16" name="departureDateAndTime" id="departureDateAndTime${request1['requestID']}" placeholder="mm/dd/yyyy hh:mm:ss">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                                    </div>
											     </div>
                
                                                  
                        </div> </div>  </div></div></div>
     </section>  </div></div>
                                  
   <input type="hidden"  value="${request1['serviceList'][0]}" name="serviceList" id="serviceList"> 
 </c:if> 
 
   <c:if test="${request1['service']== 'CABDROP' || request1['service'] =='CABPICK'|| request1['service'] == 'CABDISPOSAL' || request1['service'] == 'CABAIRPORT'  ||request1['serviceList'].contains('CAB') }">
   
 <%--  <c:if test = "${request1['serviceList'].contains('CAB')}"> --%>
  

          <div class="row">
                                <div class="col-sm-12">
                                <section class="panel">
                                    <header class="panel-heading" style="padding:6px !important ; font-size :12px!important">
                                       Cab Booking Details
                                        <!-- <span class="tools pull-right">
                                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                                            <a href="javascript:;" class="fa fa-times"></a>
                                         </span> -->
				</header>
				<div class="panel-body">
					<div class="adv-table">
						<div id="preapprove" style="width: 100%">

							<div class="table">
								<div class="row">
									<div class="col-md-3 inp-box2">
										<label for="w_id" class="lbl">Cab
											Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>

										<%-- <input type="text" placeholder="Cab Type" class="form-control"
											id="cabType${request1['requestID']}" name="cabType" /> --%>

                    <select class="form-control select2" style=""
            id="cabType" name="cabType">
            <option value=""> Select Cab</option>
            <!-- <option value="Audi"> Audii </option> -->
             <c:forEach items="${dropDown.cabPreferenceDropDown}" var="cabPreferenceValue">
                                <option><c:out value="${cabPreferenceValue}"></c:out></option>
             </c:forEach> 
          </select>
          
									</div>
									<div class="col-md-3 inp-box2">
										<label for="w_id" class="lbl">Cab
											No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
										<input type="text" placeholder="Cab No" id="cabNo${request1['requestID']}"
											name="cabNo" class="form-control" />
                       <p class="help-block" style="color:green">Ref.Format:-" DL-01-AB-1234 "</p>

									</div>
									<div class="col-md-3 inp-box2">
										<label for="w_id" class="lbl">Cab Vendor
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> <!-- <input type="text"
											placeholder="Cab Provider" class="form-control"
											id="cabProvider" name="cabProvider" /> -->
	<select class="form-control select2" id="cabProvider${request1['requestID']}" name="cabProvider" style="width:170px">
     <option value="">select...</option>
       <c:forEach items="${dropDown.allVendorName}" var="vendorName">
        <option value="${vendorName}"><c:out value="${vendorName}"></c:out></option>
        
       </c:forEach> 
      </select>    
									</div>
								</div>
								<div class="row" style="margin-top: 10px;">
									<div class="col-md-3 inp-box2">
										<label for="w_id" class="lbl">Driver
											Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> <input
											type="text" placeholder="Driver Name" class="form-control"
											id="driverName${request1['requestID']}" name="driverName" />
									</div>
									<div class="col-md-3 inp-box2">
										<label for="w_id" class="lbl">Driver Mob
											No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> <input
											type="text" placeholder="Driver No" id="driverNo${request1['requestID']}"
											name="driverNo" class="form-control numbersOnly"
											maxlength="10" />
									</div>
									<div class="col-md-3 inp-box2">
										<label for="w_id" class="lbl">PickUp Date/Time</label>
										<div class="input-group date form_datetime-component">
											<input type="text" id="pickDateAndTime${request1['requestID']}" class="form-control"
												readonly="" size="16" name="pickDateAndTime"
												placeholder="mm/dd/yyyy hh:mm:ss"> <span
												class="input-group-btn">
												<button type="button" class="btn btn-primary date-set">
													<i class="fa fa-calendar"></i>
												</button>
											</span>
										</div>
									</div>
								</div>
							</div>
							<!-- <center><input type="submit" value="Submit For Approval"></center> -->

						</div>
					</div>
				</div>
			</section>
		</div>
    </div>

    <input type="hidden"  value="CAB" name="cabServiceList" id="cabServiceList"/>
    </c:if>
                                                    <!-- <center><input type="submit" value="Submit For Approval"></center> -->
                                                    
                                           <!--  </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div> -->
   <!-- Cab confirmation Ends HEre -->
  
   <!-- Guest House Starts Here -->
      <%--  <c:if test="${request1['serviceList'].contains('GUEST_HOUSE_BOOKING')} || ${request1['serviceList'].contains('HOTEL_BOOKING')} || ${request1['serviceList'].contains('HOTEL')}"> --%>
    <c:if test="${request1['serviceList'].contains('GUEST_HOUSE_BOOKING')||request1['serviceList'].contains('HOTEL_BOOKING')}">       
     
    
              
	<div class="row">
	<input type="hidden" name="guestHouseRoomId" id="guestHouseRoomId${request1['actionId'] }"/>
                       <!--  <div class="col-md-6">

		<!for manual filling -->
		<!-- Previous Code Accomodation Start -->
		<div class="col-md-5 inp-box">
			<div class="form-group">
				<div class="col-sm-7">
					<label class="lbl">Location<span class="labelcolor">*</span></label>
				</div>
				<div class="col-sm-5">
					<input type="text" class="form-control inp-field"

						placeholder="Location" id="location${request1['actionId']}" name="location" readonly/>
				</div>
			</div>
		</div>
		<!--  <div class="col-md-6">
													<label for="w_id">Location&nbsp;&nbsp;<noframes></noframes>&nbsp;&nbsp;</label>
													<input type="text" class="form-control" placeholder="Location" id="location" name="location"/>
												</div> -->
                        <div class="col-md-5 inp-box">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label class="lbl">Booking Type<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control guestHouseDD" style="padding: 7px;" id="guestHouse${request1['requestID']}" name="guestHouse" >
                          <option value="Guest_House">Guest House Booking</option>
                          <option value="Hotel">Hotel Booking</option>
                          </select>
                            </div>
                        </div>
                    </div>

                        <!-- <div
													class="col-md-6">
													<label for="w_id">Booking&nbsp;&nbsp;</label>
                          <select class="form-control" style="width: inherit; height: inherit;" id="guestHouse" name="guestHouse" class="guestHouse" >
                          <option value="Guest_House">Guest House Booking</option>
                          <option value="Hotel">Hotel Booking</option>
                          </select> -->
                          <!-- <input
														type="text" class="form-control" placeholder="Guest House" id="guestHouse" name="guestHouse"/> -->
												<!-- </div> -->
<!-- 
                        <div class="col-md-4">
													<label for="w_id">Available GH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                           <select class="form-control" id="availSheets" name="availSheets" >
                          <option value="Null">Select Booking</option>
                          <option value="Guest_House">Guest House Booking</option>
                          <option value="Hotel">Hotel Booking</option>
                          </select>
                          <input
														type="text" class="form-control" placeholder="Avail Sheets" id="availSheets"  name="availSheets"/>
												</div> -->

                        </div>
                                                             
                                                             <br>
                    <div class="row">
                    <div class="col-md-5 inp-box">
                        <div class="form-group"> 
                            <div class="col-sm-4">
                                <label class="lbl">Checkin Date &amp; Time<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-8">
                                  <div class="input-group date checkIn_admin_form_datetime-component">
                                            <input type="text" class="form-control checkinCheckoutDateTime" readonly="" size="16" name="checkinDateTime" id="checkInDateTime${request1['actionId']}" placeholder="CheckIn Date &amp; Time"  >
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set">
                            <i class="fa fa-calendar"></i>
                          </button>
                                                            </span>
                                        </div>          
                        
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 inp-box">
                        <div class="form-group"> 
                         <div class="col-sm-4">
                                <label class="lbl">Checkout Date / Time <span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-8">
                             
         <div class="input-group date checkOut_admin_form_datetime-component">
                                            <input type="text" class="form-control checkinCheckoutDateTime" readonly="" size="16" name="checkoutDateTime" id="checkOutDateTime${request1['actionId']}" placeholder="CheckOut Date &amp; Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set">
                            <i class="fa fa-calendar"></i>
                          </button>
                                                            </span>
                                        </div>                
                        
                        
                            </div>
                    </div>
                    </div>
                     <!--  <div class="col-md-4">
													<label for="w_id">Check In&nbsp;&nbsp;<noframes></noframes>&nbsp;&nbsp;&nbsp;</label>
								<div class="input-group date checkIn_admin_form_datetime-component">
              <input type="text" class="form-control" readonly="" size="16" name="checkIn" id="checkIn" placeholder="mm/dd/yyyy hh:mm:ss">
              <span class="input-group-btn">
              <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                  </span>
                                </div>
												</div> -->
<!-- 
                                                                      <div
													class="col-md-4">
													<label for="w_id">Check Out&nbsp;&nbsp;</label>&nbsp;&nbsp;&nbsp;
				<div class="input-group date checkOut_admin_form_datetime-component">
                                            <input type="text" class="form-control" readonly="" size="16" name="checkOut" id="checkOut" placeholder="mm/dd/yyyy hh:mm:ss">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                 </div>
												</div> -->

                                                                    <!--   <div
													class="col-md-4">
													<label for="w_id"> Contact Details&nbsp;&nbsp;</label><input
														type="text" class="form-control"  placeholder="Contact Details" id="contactDetail" name="contactDetail"/>
												</div>  -->
                                                           </div>
                                                        
	
	<!--  end manual filling-->
            <br>
            <!-- Hotel Content -->
            <div class="row" style="display:none" id="hotelBookingShow${request1['actionId'] }">
                <div class="col-md-12">
                          <label for="w_id">Hotel Name & Address&nbsp;&nbsp;<noframes></noframes>&nbsp;&nbsp;</label>
                          <input type="textarea" class="form-control" placeholder="Location" id="hotelBookConfirm${request1['requestID']}" name="hotelBookConfirm"/>
                  </div>
            </div>
            <br>

              <div class="row">
                    <div class="col-sm-12">
                      <section class="panel">
                          <div class="panel-body">
                              <div class="adv-table">
                                  <div id="preapprove" style="width: 100%">


        <div class="row" id ="ghNameShow${request1['actionId'] }" style="margin-top: -35px;">
        
        <!-- New Setting  Starts -->
          
           <div class="col-md-5">
                  <label>Select Guest House Availability<span class="requiredField"> *</span></label>
              </div>
              <div class="col-md-12">
                  <select style="border-radius:0px;width:100%;" class="form-control select-style ghAvailableReq" id="ghAvailableReq${request1['actionId']}" name="ghAvailableReq" style="width: inherit; height: inherit;">
                  <option value="">Select...</option>
                  <c:forEach items="${spoc.guestHouseList }" var="guestHouse">
                   <option value="${guestHouse.id }">${guestHouse.buildingName }</option>
                  
                  </c:forEach>
                  </select>
                </div>
            
                      <!-- End of Settings -->

        </div>
        <div class="row" style="margin-top:15px;" id="checkAction${request1['actionId'] }">
          <div class="col-md-6">
            <button style="border-radius:0px;" class="btn btn-info col-md-12 ghAvailOnly" id ="ghAvailOnly" type="button">Available button</button>
          </div>
           <div class="col-md-6">
            <button style="border-radius:0px;" class="btn btn-danger col-md-12 ghShowHistory" id ="ghShowHistory" type="button">Booked History button</button>
          </div>
        </div>
        
        
                                        
                                      
       


                          <div class ="pannel" id="ghInventoryShow${request1['actionId'] }">
                         <!--  <div class="alert alert-success fade in alert-style">
                                <button type="button" class="close close-sm" data-dismiss="alert">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>Guest House </strong>Booking Details 
                            </div> -->
                              <section class="panel section-style">
                        <header class="panel-heading custom-tab yellow-tab">
                            <ul class="nav nav-tabs">
                                <li>
                                    <a data-toggle="tab" href="#singleOcupancy${request1['actionId'] }">
                                        <i class="fa fa-user"></i>
                                        Single Ocupancy
                                    </a>
                                </li>
                                <li class="active">
                                    <a data-toggle="tab" href="#doubleOcupancy${request1['actionId'] }">
                                        <i class="fa fa-user"></i>
                                        Double Ocupancy
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#tripleOcupancy${request1['actionId'] }">
                                        <i class="fa fa-envelope-o"></i>
                                        Triple Ocupancy
                                    </a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content  content-style">
                                <div id="singleOcupancy${request1['actionId'] }" class="tab-pane ">
                                   <!-- Table Start -->
        <table class="table table-bordered table-striped table-condensed " >
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">Item1</th>
                            <th class="">Item2</th>
                            <th class="">Item3</th>
                            <th class="">Item4</th>
                            <th class="">Item5</th>
                            <th class="">Item6</th>
                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="singleOccupancyTable" class="singleOccupancyTable">
                       
                        </tbody>
                    </table>

                                   <!-- Table End -->
                                 
                                </div>
                                <div id="doubleOcupancy${request1['actionId'] }" class="tab-pane active"> 
                                <!-- Table 2 Start -->
         <table class="table table-bordered table-striped table-condensed" >
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">Item1</th>
                            <th class="">Item2</th>
                            <th class="">Item3</th>
                            <th class="">Item4</th>
                            <th class="">Item5</th>
                            <th class="">Item6</th>
                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="doubleOccupancyTable" class="doubleOccupancyTable">
                        
                        </tbody>
                    </table>

                                   <!-- Table End --></div>
                                <div id="tripleOcupancy${request1['actionId'] }" class="tab-pane">
                                   <!-- Table Start -->
         <table class="table table-bordered table-striped table-condensed"   >
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">Item1</th>
                            <th class="">Item2</th>
                            <th class="">Item3</th>
                            <th class="">Item4</th>
                            <th class="">Item5</th>
                            <th class="">Item6</th>
                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody class="tripleOccupancyTable" id="tripleOccupancyTable">
                        
                        </tbody>
                    </table>

                                   <!-- Table End -->
                                </div>
                            </div>
                        </div>
                    </section>
                            
                          </div>

                          <!-- Booked History -->
                          <div class ="pannel" id="ghBookedHistory${request1['actionId'] }" style="display:none">
                         <!--  <div class="alert alert-success fade in alert-style">
                                <button type="button" class="close close-sm" data-dismiss="alert">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>Guest House </strong>Booking Details 
                            </div> -->
                              <section class="panel section-style">
                        <header class="panel-heading custom-tab yellow-tab">
                            <ul class="nav nav-tabs">
                                <li>
                                    <a data-toggle="tab" href="#singleOcupancyHistory${request1['actionId'] }">
                                        <i class="fa fa-user"></i>
                                        Single Ocupancy
                                    </a>
                                </li>
                                <li class="active">
                                    <a data-toggle="tab" href="#doubleOcupancyHistory${request1['actionId'] }">
                                        <i class="fa fa-user"></i>
                                        Double Ocupancy
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#tripleOcupancyHistory${request1['actionId'] }">
                                        <i class="fa fa-envelope-o"></i>
                                        Triple Ocupancy
                                    </a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content  content-style">
                                <div id="singleOcupancyHistory${request1['actionId'] }" class="tab-pane ">
                                   <!-- Table Start -->
        <table class="table table-bordered table-striped table-condensed " >
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">History</th>
                        </tr>
                        </thead>
                        <tbody id="singleOccupancyTableHistory" class="singleOccupancyTableHistory">
                       
                        </tbody>
                    </table>

                                   <!-- Table End -->
                                 
                                </div>
                                <div id="doubleOcupancyHistory${request1['actionId'] }" class="tab-pane active"> 
                                <!-- Table 2 Start -->
         <table class="table table-bordered table-striped table-condensed" >
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">History</th>
                        </tr>
                        </thead>
                        <tbody id="doubleOccupancyTableHistory" class="doubleOccupancyTableHistory">
                        
                        </tbody>
                    </table>

                                   <!-- Table End --></div>
                                <div id="tripleOcupancyHistory${request1['actionId'] }" class="tab-pane">
                                   <!-- Table Start -->
         <table class="table table-bordered table-striped table-condensed "  >
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">History</th>
                          
                        </tr>
                        </thead>
                        <tbody id="tripleOccupancyTableHistory" class="tripleOccupancyTableHistory">
                        
                        </tbody>
                    </table>

                                   <!-- Table End -->
                                </div>
                            </div>
                        </div>
                    </section>
                            
                          </div>
                          <!-- End Booked History -->
                                    
                                </section>
                            </div>
                        </div>
                   <input type="hidden"  value="${request1['actionId'] }" name="actionId" id="actionId" >                    
                        
    
   <input type="hidden"  value="HOTEL" name="hotelServiceList" id="hotelServiceList">   
  <%--  <div class="row">                              
        	<center><button type="submit" style="display:none;" class="btn btn-primary hotelSubmitButton" name="action" value="SPOC_SERVE" id="hotelSubmitButton${request1['actionId'] }">Submit</button></center>
        </div>     --%>             
       </c:if>
       
<%-- <c:if test = "${request1['serviceList'].contains('TRAINTICKET') ||request1['serviceList'].contains('AIRTICKET') || request1['serviceList'].contains('HOTEL') || request1['serviceList'].contains('CAB')}"> --%>
  
 <%--  <c:if test = "${request1['serviceList'].contains('TRAINTICKET') ||request1['serviceList'].contains('AIRTICKET')}"> 
  <input type="hidden"  value="${request1['serviceList'][0]}" name="serviceList" id="serviceList">
</c:if> --%>
