<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:form-layout>
	<jsp:attribute name="header">
	

<style>
.boxx {
	display: none;
}

/* input
{
 text-transform: capitalize;
}
 */
 
.requiredField {
	color: red;
}

#requestDataModal .modal-dialog {
	width: 55%;
	margin: 10px auto !important;
}

#requestDataModal .modal-body {
	padding: 0px !important;
	line-height: 16px !important;
	font-size: 12px !important;
	margin-left: 50px;
	margin-top: 20px;
	color: #000000 !important;
}

#responseDataModal .modal-dialog {
	width: 61%;
	margin: 10px auto !important;
}

#responseDataModal .modal-body {
	padding: 15px !important;
	line-height: 16px !important;
	font-size: 12px !important;
	margin-left: 50px;
	margin-top: 20px;
	color: #000000 !important;
}

.form-group label {
	color: #37474F !important;
	font-weight: 600 !important;
	padding-top: 6px !important;
	font-family: inherit;
	font-size: 13px;
}

.t-tabs {
	background: #78909C;
	border-radius: 48px;
	text-align: center;
}

.t-tabs:hover {
	background-color: #37474F !important;
}

.nav-tabs li {
	margin-left: 65px;
}

.nav-tabs li:hover {
	background: gray;
	border-radius: 48px;
	text-align: center;
}

.tab-content {
	background-color: #fff;
	padding: 31px;
	border-top: 6px solid #90A4AE;
	-webkit-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
}

.panel-heading .nav {
	margin: 0px !important;
}

.panel-heading .nav>li.active {
	background-color: #ccc !important;
	color: #37474F !important;
}

.panel-heading .nav>li.active>a, .panel-heading .nav>li>a:hover {
	background: transparent !important;
}

.panel-heading .nav>li>a:hover {
	color: #fff !important;
}

.custom-tab.dark-tab {
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
	border-top: 1px solid #ccc;
}

.panel-body {
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
}
.fa-stylecs{
    border: 1px solid black;
    border-radius: 12px;
    background-color: #263238;
}
.fa-stylecsi.hover{
  text-color :white;
  color: white;
}
</style>

	
<!--select2-->
 <link rel="stylesheet" type="text/css"
			href="assets/select2/select2.css" />
 <link rel="stylesheet" type="text/css"
			href="assets/select2/select2-bootstrap.css" />
<!--select2-->

 <!--pickers css-->
 
   <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datepicker/css/bootstrap-datepicker2.css">
 

  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-timepicker/css/timepicker.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-colorpicker/css/colorpicker.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">


<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css"
			href="assets/css/bootstrap-fileupload.min.css" />
  <link rel="stylesheet" type="text/css"
			href="assets/css/bootstrap-fileupload.min.css" />


    </jsp:attribute>


	<jsp:attribute name="extrajs">
    
    
    <!--select2-->
     <script src="assets/select2/select.js"></script>
    <!--select2-->
    
   <script src="assets/js/customJS/tour_travel.js"></script>

    <script src="assets/js/customJS/utility.js"></script>

     
  
    
    <!--dynamic table-->
<script type="text/javascript" language="javascript"
			src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"
			src="assets/js/data-tables/DT_bootstrap.js"></script>

<script
			src="assets/js/tourandtravel_dtable_init/air_dynamic_table_init.js"></script>
<script
			src="assets/js/tourandtravel_dtable_init/train_dynamic_table_init.js"></script>
<script
			src="assets/js/tourandtravel_dtable_init/cab_dynamic_table_init.js"></script>
<script
			src="assets/js/tourandtravel_dtable_init/admin_dynamic_table_init.js"></script>
<script
			src="assets/js/tourandtravel_dtable_init/approval_authority_dynamic_table_init.js"></script>



<!--pickers plugins-->

<script type="text/javascript"
			src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker2.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/js/pickers-init.js"></script>
<script src="assets/js/pickers-init2.js"></script>

<script>
	var addNotification = function(requestID, status) {
		$("#notificationMenu")
				.append(
						'<li class="new"> <a href=""><span class="label label-danger"><i class="fa fa-bolt"></i></span><span class="name">Request ID:'
								+ requestID
								+ '  </span> <em class="small">Status: '
								+ status + '</em> </a> </li>');
	}

	$(document)
			.ready(
					function() {
						$('#bookTicketButton')
								.on(
										'click',
										function() {
											$("#admin_request_user_cnf_modal")
													.modal("show");

											$("#txtRequestData")
													.append(
															$(this).closest(
																	'tr')
																	.children()[10].innerHTML);

										});

						// Js to handle event on change two way journey selection
						$('#two')
								.change(
										function(event) {
											document
													.getElementById('travel_purpose').style.display = 'none';
											document
													.getElementById('return_Date').style.display = 'block';
											document
													.getElementById('trPurposeHides').style.display = 'block';
										});
						$('#one')
								.change(
										function(event) {
											document
													.getElementById('travel_purpose').style.display = 'block';
											document
													.getElementById('return_Date').style.display = 'none';
											document
													.getElementById('trPurposeHides').style.display = 'none';
										});
						$('#trainTwoWay')
								.change(
										function(event) {
											document
													.getElementById('trainTravelPurpDiv').style.display = 'none';
											document
													.getElementById('trainReturnTravelDateDiv').style.display = 'block';
											document
													.getElementById('trTravel_Purpose').style.display = 'block';
										});
						$('#trainOneWay')
								.change(
										function(event) {
											document
													.getElementById('trainTravelPurpDiv').style.display = 'block';
											document
													.getElementById('trainReturnTravelDateDiv').style.display = 'none';
											document
													.getElementById('trTravel_Purpose').style.display = 'none';
										});

					});
</script>

<script>
	$(".opendialog-cancel").click(function() {
		$('#requestid').val($(this).data('id'));
		$('#user_modal_cancel').modal('show');
	});

	// $('.downloadForm').click(function(event){
	// $('#myForm').submit(function(event){
	//  event.preventDefault();
	// });

	// });
</script>



    </jsp:attribute>

	<jsp:body>


        <div class="page-heading">
        
        <!-- Cab/Ticket -->
          <section class="panel">
                        <header
				class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs" id="myTab">
                       
                                <li class="active col-md-3 t-tabs">
                                    <a href="#air_requests"
					data-toggle="tab" id="airTicket">Air Ticket</a>
                                </li>
                    
                         
                                <li class="col-md-3 t-tabs">
                                    <a href="#cab_requests"
					data-toggle="tab" id="cabRequest">Cab Request</a>
                                </li>
                    
                     
                                <li class="col-md-3 t-tabs">
                                    <a href="#train_requests"
					data-toggle="tab" id="trainTicket">Train Tickets</a>
                                </li>
                           
                              
                                 
                            </ul>
                        </header>

<input type="text" id="userName"
				value="${userSession.empData.displayName}" style="display: none;" />
<input type="text" id="userID" value="${userSession.empData.employeeID}"
				style="display: none;" />
				
				
				
<%@include file="tourandtravel/airtourandtravel.jsp"%> 

                    <!-- ==== Dynamic table Start here ===== -->
    <!-- <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           USER VIEW
           
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
								id="air_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Request ID</th>
            <th class="hidden-phone">Requester</th>
            <th class="hidden-phone">Request Type</th>
                        <th class="hidden-phone">Request Date</th>
            
            <th class="hidden-phone">Status</th>
            <th>Action</th>
            <th>Download</th>
                        <th style="display: none;">Data</th>
            
        </tr>
        </thead>
        <tbody>
        
                    
         
        <c:forEach var="request" items="${tat.USERtatDataList}">
                     <tr class="gradeX" id="rid${request['requestID']}">
<td><a class="showRequestDataModal" href="javascript:;" onclick="callShowRequestData(<c:out value="${request['requestID']}"></c:out>, '<c:out value="${request['service']}" />')">
   <c:choose>
    <c:when test="${not empty request['workorder']}">
        <c:out value="${request['workorder']}"></c:out>
    </c:when>    
    <c:otherwise>
    <c:out value="${request['requestID']}"></c:out>
    </c:otherwise>
       </c:choose>

</a></td>

                    
            <td class="hidden-phone"><c:out
													value="${request['requester']}" /></td>
            <td class="center hidden-phone"><c:out
													value="${request['requestType']}" /></td>
             <td class="center hidden-phone"><c:out
													value="${request['requestdate']}" /></td>
 
            <td class="center hidden-phone"><c:choose>
												<c:when test="${request['requestStatus'] =='COMPLETE'}">

													<a class="showResponseDataModal" href="javascript:;"
															onclick="callShowResponseData('<c:out value="${request['requestID']}"></c:out>','<c:out value="${request['service']}" />')"><c:out
																value="${request['requestStatus']}" /></a>
</c:when>
<c:when test="${request['requestStatus'] =='CLOSED'}">
													<a class="showResponseDataModal" href="javascript:;"
															onclick="callShowResponseMsg('<c:out value="${request['requestID']}"></c:out>')"><c:out
																value="${request['requestStatus']}" /></a>
</c:when>
            <c:otherwise>
													<c:out value="${request['requestStatus']}" />
												</c:otherwise>
											</c:choose></td>
<td>
<button
													class="btn btn-info opendialog-cancel <c:if test="${request['requestStatus']=='CLOSED' || request['requestStatus']=='CANCELLED' || request['complete']==true ||request['isActive']==true}"> <c:out value="disabled "/></c:if>"
													type="button" -->
													<%-- onclick="callCancelRequest('${request['requestID']}')" --%> <!-- data-toggle="modal"
													data-id="${request['requestID']}">
													<i class="fa fa-hand-o-right"></i> Cancel</button>


</td>

            <td><a class="<c:if test="${request['requestStatus']!='COMPLETE'}"><c:out value="disableAnchor"/></c:if>"
											href="DownloadResponseServlet?fileName=${request['responseAttachment']}&requestId=${request['requestID']}"><button
													class="btn btn-warning 
	 <c:if test="${request['requestStatus']!='CLOSED'}"> <c:out value="disabled "/></c:if>"
													type="button"> 

															<i class="fa fa-hand-o-right"></i> Download</button></a></td>
                

        </tr>
        
         </c:forEach>
                  
         
                </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div> -->
<!-- Dynamic Table End -->
<!-- =====train ticket  panel  end-->

                <!-- ====Ticket Requests End Here -->
            

		
		
		
		</div>
		

<!--===============form for train request  -->

<%-- 
<!-- ======temp approval authouriy tab== -->

<div class="tab-pane" id="approval_authourity">
 
                    <!-- ==== Dynamic table Start here ===== -->
                       <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           approval authourity
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
								id="aa_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Request ID</th>
            <th>Location</th>
            <th class="hidden-phone">Request By</th>
            <th class="hidden-phone">Request Type</th>
            <th class="hidden-phone">Status</th>
            <th>Request For</th>
            <th>Action</th>
            <th>Link</th>
            <th style="display: none;">Hidden</th>
        </tr>
        </thead>
        <tbody>
        
        
                
           
        
                <c:forEach items="${tat.HODtatDataList}" var="request">
         <tr class="gradeX">
            <td><c:out value="${request['requestID']}" /></td>
            <td><c:out value="${request['reqLocation']}" /></td>
            <td class="hidden-phone"><c:out
													value="${request['requester']}" /></td>
            <td class="center hidden-phone"><c:out
													value="${request['requestType']}" /></td>
            <td class="center hidden-phone"><c:out
													value="${request['status']}" /></td>
<td><c:out value="${request.requestFor}" />
											<td>
    
            <button class="btn btn-warning" type="button"
													onclick="callHNGServlet('ACCEPT','${request['actionId']}')"
													name="action" value="ACCEPT">
	<i class="fa fa-hand-o-right"></i>Accept</button>
											</td>
            <td> 
          <button class="btn btn-info" type="button" name="action"
													value="REJECTED"
													onclick="callHNGServlet('REJECTED','${request['actionId']}')">
	<i class="fa fa-hand-o-right"></i>Reject</button>  
            
        <!--      <a class="btn btn-warning <c:if test="${request['requestStatus']=='rejected'}"><c:out value="disabled"/></c:if>" type="button" href="AirTicketRequestController?action=reject&requestID=<c:out value="${request['requestID']}" />" ><i class="fa fa-hand-o-right"></i> Reject</a>-->
        </td>

	            <td style="display: none;">
	            <!-- ==========start internal table of  table============ -->
                 <div class="row">
                 <div class="col-md-4">
                 <label>Cost Centre:</label>&nbsp;<c:out
															value="${request['cost']}" /> 
                 </div>
                 <div class="col-md-4">
                <label>Pref Time Slot:</label>&nbsp; <c:out
															value="${request['timeSlot']}" />
                </div>
                <div class="col-md-4">
                 <label>Travel Date:</label>&nbsp;  <c:out
															value="${request['travelDate']}" />
                 </div>
                 </div>
                 <div class="row">
               
               <div class="col-md-4">  
               <label>Travel Purpose:</label>&nbsp;  <c:out
															value="${request['travelPurpose']}" />
               </div>
               <div class="col-md-4">
               
                                  <label>Booking For:</label>&nbsp;  <c:out
															value="${request['bookingFor']}" />
                                  </div>
                                  <div class="col-md-4">
                                  
                                  <label>Pax Name:</label>&nbsp;<c:out
															value="${request['passenger']}" />
                                  </div>
                                  
                                </div> 
                                 
                           <div class="row"> 
                           
                           <div class="col-md-4">
                                                  <label>Employee ID</label> &nbsp; <c:out
															value="${request['employeeID']}" />
                                                  </div>
                           <div class="col-md-4">                     
                                 <label>Mobile: </label>&nbsp; <c:out
															value="${request['mob']}" />
                                 </div>
                                 <div class="col-md-4">
                                 <label>Source:</label>&nbsp;  <c:out
															value="${request['source']}" />
                                 </div>
                                 
                
                 			</div>
                 			<div class="row">
                 			<div class="col-md-4">
                                 <label>Destination: </label>&nbsp; <c:out
															value="${request['destination']}" />
                                 </div>
                 			 
                 		<div class="col-md-4">
                 		<label>Attachment:</label><a
															href="DownloadController?path=<c:out value="${request['upload']}" />">click here</a>
                 		</div>
                 			</div>
                 			
                 
                  <!-- ================End internal AA table============ -->
                 
            </td>
	
        </tr>
         </c:forEach>
         
         
                </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div>
        </div>
        

<!-- ======================end temp approval authorityn tab  --> --%>

<%@include file="tourandtravel/traintickettourandtravel.jsp"%>

                    <!-- ==== Dynamic table Start here ===== -->
                     <!-- <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Train booking 
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
                
         <table class="display table table-bordered "
							id="train_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Request ID</th>
          
            <th class="hidden-phone">Requester</th>
            <th class="hidden-phone">Booking For</th>
             <th class="hidden-phone">Request Date</th>
            
            <th class="hidden-phone">Status</th>
        
            <th>Action</th>
            <th>Download</th>
            
                        <th style="display: none;">Data</th>
            
        </tr>
        </thead>
        <tbody>
                <c:forEach items="${tat.USERtattrainDataList}"
									var="request">
        
                <tr class="gradeX" id="rid${request['requestID'] }">
                
           <td><a class="showRequestDataModal" href="javascript:;"
											onclick="callShowRequestData(<c:out value="${request['requestID']}"></c:out>, '<c:out value="${request['service']}" />')">
											
											   <c:choose>
    <c:when test="${not empty request['workorder']}">
        <c:out value="${request['workorder']}"></c:out>
    </c:when>    
    <c:otherwise>
    <c:out value="${request['requestID']}"></c:out>
    </c:otherwise>
       </c:choose>
											</a></td>
           
            <td class="hidden-phone"><c:out
												value="${request['requester']}" /></td>
            <td class="center hidden-phone"><c:out
												value="${request['requestType']}" /></td>
                        <td class="center hidden-phone"><c:out
												value="${request['requestdate']}" /></td>
            
            <td class="center hidden-phone">
                  <c:choose>
												<c:when test="${request['requestStatus'] =='COMPLETE'}">
													<a class="showResponseDataModal" href="javascript:;"
														onclick="callShowResponseData('<c:out value="${request['requestID']}"></c:out>','<c:out value="${request['service']}" />')"><c:out
															value="${request['requestStatus']}" /></a>
												</c:when>
	<c:when test="${request['requestStatus'] =='CLOSED'}">
				<a class="showResponseDataModal" href="javascript:;"
														onclick="callShowResponseMsg('<c:out value="${request['requestID']}"></c:out>')"><c:out
															value="${request['requestStatus']}" /></a>
</c:when>   
            <c:otherwise>
													<c:out value="${request['requestStatus']}" />
												</c:otherwise>
											</c:choose>
            
            </td>
            <td> -->
<!-- <button
												class="btn btn-info opendialog-cancel <c:if test="${request['requestStatus']=='CLOSED' || request['requestStatus']=='CANCELLED' || request['complete']==true||request['isActive']==true}"> <c:out value="disabled "/></c:if>"
												type="button" name="action" value="CANCEL"
												data-toggle="modal" --> <%--data-id="${request['requestID']}"--%>
		<%-- onclick="callCancelRequest('${request['requestID']}')" --%>
		<!-- > --> 
	<!-- <i class="fa fa-hand-o-right"></i> Cancel</button>
</td>
	 <td><a class="<c:if test="${request['requestStatus']!='COMPLETE'}"><c:out value="disableAnchor"/></c:if>"
											href="DownloadResponseServlet?fileName=${request['responseAttachment']}&requestId=${request['requestID']}"><button
													class="btn btn-warning 
	 <c:if test="${request['requestStatus']!='CLOSED'}"> <c:out value="disabled "/></c:if>"
													type="button"> 
															<i class="fa fa-hand-o-right"></i> Download</button></a></td>

               

        </tr>
        
         </c:forEach>
                </tbody>
        </table>
                
                
                
        </section>
        </div>
        </div>
        </section>
        </div>
        </div>  -->
        
                            

  
<!-- Data Table End Here  -->
<!-- =====train ticket  panel  end-->

                <!-- ====Ticket Requests End Here -->
            </div>




<!-- end form for train request -->
<!--  -->
 <div class="tab-pane" id="cab_requests">

    <!-- radio button -->

   <header class="panel-heading custom-tag dark-tab">
   
   <div class="row">
   	<div class="col-md-2">
   		<h5>Select Services</h5>
   	</div>
   	<div class="col-md-3 col-md-offset-6">
   		 <select class="form-control select2"
			id="cabServices" name="cabServices">                  
                <option value="pick" id="pick4">Pick up only</option>
                <option value="drop" id="drop4">Drop Only</option>
                <option value="disposal" id="disposal4">Cab at disposal</option>
                <option value="airport" id="airport4">Airport Transfer</option>    
         </select>
   	</div>
   </div>
   
   </header>
   
 <%@include file="tourandtravel/cubtourandtravel.jsp"%> 
    
  


         <!-- tabs ends -->

                                  
          <!-- =======Submit End Here======== -->
            <!-- ======================Dynamic table Start here ====== -->
                       
                        <!--         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           USER VIEW
          
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
								id="cab_hidden-table-info">
      <thead class="cf">
        <tr>
            <th>Request ID</th>
            <th class="hidden-phone">Requester</th>
            <th class="hidden-phone">Booking For</th>
                        <th class="hidden-phone">Request Date</th>
            
            <th class="hidden-phone">Status</th>
            <th>Service</th>
             <th>Action</th>
            <th>Download</th>
                        <th style="display: none;">Data</th>
            
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${tat.USERtatcabDataList}" var="request">
        
                <tr class="gradeX" id="rid${request['requestID'] }">
<td><a class="showRequestDataModal"href="javascript:;" onclick="callShowRequestData(<c:out value="${request['requestID']}"></c:out>, '<c:out value="${request['service']}" />')">
   <c:choose>
    <c:when test="${not empty request['workorder']}">
        <c:out value="${request['workorder']}"></c:out>
    </c:when>    
    <c:otherwise>
    <c:out value="${request['requestID']}"></c:out>
    </c:otherwise>
       </c:choose>

</a></td>
            <td class="hidden-phone"><c:out value="${request['requester']}" /></td>
            <td class="center hidden-phone"><c:out value="${request['requestType']}" /></td>
                        <td class="center hidden-phone"><c:out value="${request['requestdate']}" /></td>

            
            <td class="center hidden-phone"><c:choose>
												<c:when test="${request['requestStatus'] =='COMPLETE'}">
													<a class="showResponseDataModal" href="javascript:;"
															onclick="callShowResponseData('<c:out value="${request['requestID']}"></c:out>','<c:out value="${request['service']}" />')"><c:out
																value="${request['requestStatus']}" /></a>
												</c:when>
	<c:when test="${request['requestStatus'] =='CLOSED'}">
	<a class="showResponseDataModal" href="javascript:;"
															onclick="callShowResponseMsg('<c:out value="${request['requestID']}"></c:out>')"><c:out
																value="${request['requestStatus']}" /></a>
</c:when>   
            <c:otherwise>
													<c:out value="${request['requestStatus']}" />
												</c:otherwise>
											</c:choose></td>
<td><c:out value="${request['ServiceDisplay']}" /></td>
<td>
<button
													class="btn btn-info  opendialog-cancel <c:if test="${request['requestStatus']=='CANCELLED' || request['requestStatus']=='COMPLETE' || request['complete']==true||request['isActive']==true}"> <c:out value="disabled"/></c:if>"
													type="button" data-toggle="modal"
													data-id="${request['requestID']}"<%-- onclick="callCancelRequest('${request['requestID']}')" --%>>
															<i class="fa fa-hand-o-right"></i> Cancel</button>
</td>
            <td><a  class="<c:if test="${request['requestStatus']!='COMPLETE' &&  empty request['responseAttachment']}"><c:out value="disableAnchor"/></c:if>" href="DownloadResponseServlet?fileName=${request['responseAttachment']}&requestId=${request['requestID']}"><button
													class="btn btn-warning " type="button"
													<c:if test="${request['requestStatus']!='COMPLETE'}"><c:out value="disabled"/></c:if>>  

															<i class="fa fa-hand-o-right"></i> Download</button></a></td>

                 
                 
                

        </tr>
        
         </c:forEach>
                
         
         
                </tbody>
              
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div> -->
        <!-- Dyanmic Table End Here -->
<!-- =====cab ticket  panel  end-->

                <!-- ====cab Ticket Requests End Here -->
            </div>


    
    
    <!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog ">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body" style="overflow: auto;">
        </div>
        
        
        <br>
       
        <div class="modal-footer sticky-footer">
          <center>
							<button type="button"
								class="btn btn-default close_btn col-sm-4 col-sm-offset-4"
								data-dismiss="modal">CLOSE</button>
						</center>
        </div>
      </div>
      
    </div>
  </div>
<!-- end of modal -->

<!-- Start Of  Response Data Status modal -->
 <!-- Modal -->
  <div class="modal fade" id="responseDataModal" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Details</h4>
        </div>
        <div class="modal-body" style="overflow: auto;">
         </div>
        <div class="modal-footer sticky-footer"
						style="margin-top: 0px !important; position: relative;">
<center>								
</center>
        </div>
    
      </div>
    </div>
    
  </div>
 


<!-- End Of Response Data Status Modal -->
    
    
    <!-- Start Of  Response Message (CLOSED) Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseMsgModal" role="dialog">
    <div class="modal-dialog">
  
  <!-- <form action="DownloadPDFServlet" method="POST"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Response Message </h4>
        </div>
        <div class="modal-body">
  
         </div>
       
       
        <div class="modal-footer"
						style="margin-top: 10px; position: relative;">
<center>								
<button class="btn btn-warning " type="submit" data-dismiss="modal"> <i
									class="fa fa-hand-o-right"></i> Ok</button>
</center>
        </div>
    
      </div>
    </form>
    </div>
    
  </div>
 
<!-- End Of Response Message Status Modal -->

<!--======== datetimepicker modal for checkin checkout Starts ======= -->
 
        <div class="modal fade" id="travelDateModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
                 <center>
								<h4>
									<b>24:00 Hrs prior intimation is required for a successful booking<br /><br />
                          Request will processed , Please wait for the confirmation.</b>
								</h4>
							</center>
        </div>
        <br>
        <div class="modal-footer"
							style="position: relative; margin-top: 0px !important">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">close</button>
							</center>
        </div>
      </div>
      </form>
    </div>
  </div>
 
           
 
  <!-- ======= datetimepicker modal for checkin checkout ends ====== -->  
   <!-- modal for source location  starts --> 
   <div class="modal fade" id="sourcelocationModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Location</h4>
        </div>
        <div class="modal-body">
                 <center>
								<h5>Do you want to enter the name manually ?</h5>
							</center>
        </div>
        <br>
        <div class="modal-footer"
							style="position: relative; margin-top: 0px !important">
   
          <!-- <button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterSourceLocation()">Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button> -->
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for location  ends -->  
 
<!-- modal for location  starts --> 
   <div class="modal fade" id="selectLocationModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Location</h4>
        </div>
        <div class="modal-body">
                 <center>
								<h5>Do you want to select the name from dropdown ?</h5>
							</center>
        </div>
        <br>
        <div class="modal-footer"
							style="position: relative; margin-top: 0px !important">
          <button type="button" class="btn btn-danger"
								data-dismiss="modal" onclick="selectLocation()">Yes</button>
          <button type="button" class="btn btn-default"
								data-dismiss="modal">No</button>
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for location  ends --> 
 
 <!-- modal for Cancel  starts --> 
   <div class="modal fade" id="user_modal_cancel" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Cancel Request</h4>
        </div>
        <div class="modal-body">
                <input type="hidden" name="requestId" id="requestid"
								class="user" />
                 <center>
								<h5>
									<b>Are you sure you want to cancel it ?</b>
								</h5>
							</center>
        </div>
        <br>
        <div class="modal-footer"
							style="position: relative; margin-top: 0px !important">
          <button type="button" class="btn btn-danger"
								data-dismiss="modal" onclick="callCancelRequest()">Yes</button>
          <button type="button" class="btn btn-default"
								data-dismiss="modal">No</button> 
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for  cancel ends --> 
 
 <!-- modal for avail accomodation  starts --> 
   <div class="modal fade" id="avail_acc_modal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">	
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Avail Accomodation</h4>	
        </div>
        <div class="modal-body">
                <input type="hidden" name="requestId" id="requestid"
								class="user" />
                <center>
								<h5>
									<b>User preference will be given to Guest House as per company policy</b>
								</h5>
							</center>
        </div>
        <br>
        <div class="modal-footer"
							style="position: relative; margin-top: 0px !important">
          <button type="button" class="btn btn-danger"
								data-dismiss="modal" onclick="callAvailAcc()">Yes</button>
          <button type="button" class="btn btn-default"
								data-dismiss="modal" onclick="cancelAvailAcc()">No</button> 
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for  avail accomodation ends --> 
 
 <!-- modal for Internal BookingFor  starts --> 
   <div class="modal fade" id="internal_booking_modal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Internal Booking</h4>
        </div>
        <div class="modal-body">
                <input type="hidden" name="requestId" id="requestid"
								class="user" />
                <center>
								<h5>
									<b>You can only request for your reporting manager</b>
								</h5>
							</center>
        </div>
        <br>
        <div class="modal-footer"
							style="position: relative; margin-top: 0px !important">
          <button type="button" class="btn btn-danger"
								data-dismiss="modal">Ok</button>
          <!-- <button type="button" class="btn btn-default" data-dismiss="modal">No</button>  -->
        </div>
      </div>
      </form>
    </div>
  </div>
 <!-- modal for Internal BookingFor  ends --> 
  
  <!-- cab modal if pick up time less than 2 hrs  starts-->
  
   <div class="modal fade" id="cab_pickup_modal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
                 <center>
								<h4>
					<!-- <b>Pick time can not be less than two hours from now</b> -->
					<b>02:00 Hrs prior intimation is required for a successful booking<br/><br />
                       Request will processed , Please wait for the confirmation.</b>
								</h4>
							</center>
        </div>
        <br>
        <div class="modal-footer"
							style="position: relative; margin-top: 0px !important">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">close</button>
							</center>
        </div>
      </div>
      </form>
    </div>
  </div>
   <!-- cab modal if pick up time less than 2 hrs  ends-->
           
        
    </jsp:body>
</t:form-layout>
