<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:layout>

	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
    
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    
    <!--dynamic table-->
    <script type="text/javascript" language="javascript"src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"	src="assets/js/data-tables/DT_bootstrap.js"></script>
<script		src="assets/js/dynamic_table_init.js"></script>
    
    
    
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
 
    <script src="assets/js/customJS/repair_validation.js"></script>
        <script src="assets/js/customJS/itemRequistion.js"></script>
    
<script src="assets/js/customJS/utility.js"></script>

<script>

$(document).ready(function(){
});
</script>
    </jsp:attribute>
    <jsp:body>
          <!-- ======temp approval authouriy tab== -->

<div class="tab-pane" id="approval_authourity">

<!-- =========================== Dynamic table Start here ==================== -->
                       <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           Spoc Stationery & Printing View
        </header>  
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered " id="app_auth_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Request Id/Workorder</th>
            <th class="hidden-phone">Request By</th>
            <th class="hidden-phone">Request Type</th>
            <th class="hidden-phone " >Request Status</th>
            <th>Request For</th>
            <th>Action</th>
            <th>Close</th>
        </tr>
        </thead>
        <tbody id="tblbody">
                <tr >
                <td>1</td>
                <td>Manoj Singh</td>
                <td>Stationery</td>
                <td>Not Issued</td>
                <td>Self</td>
                <td>
                <a href="#" data-toggle="modal" data-target="#issueItemPerCategory" id="issueItems"><button class=" btn btn-warning"type="button" onclick="" name="action"   value=""><i class="fa fa-hand-o-right"></i>Assign</button></a></td>
                <td><a href="#" data-toggle="modal" data-target="#rejectItemReq" id="rejectReqItem"><button class=" btn btn-warning"type="button" onclick="" name="action"   value=""><i class="fa fa-hand-o-right"></i>Reject</button></a></td>
                </tr>
        </tbody>
        </table>
        </section>
        </div>
        <!-- Modal when accept the item request by Spoc/Administration start-->
        <!--==================Assign Modal Start=============  -->
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="issueItemPerCategory" class="modal fade">
                        <div class="modal-dialog" style="width:75%">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Issue Item Info</h4>
                                    </div>
                <div class="modal-body">
                                    <div class="row">
                        <div class="col-sm-12">
                <section class="panel">
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    <div class="btn-group pull-right">
                    </div>
                </div>
                <div class="space15"></div>
                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                <thead>
                <tr>
                    <th>Product Category</th>
                    <th>Product Brand</th>
                    <th>Product Name</th>
                    <th>Qty Required</th>
                    <th>Qty Issued</th>
                    <th>Total Price</th>
                </tr>
                </thead>
                <tbody>
                <tr class="">
                    <td>Mobile</td>
                    <td>Samsung</td>
                    <td>Note 7</td>
                    <td>2</td>
                    <td><input></input></td>
                    <td><input type="text" readonly="" value="760000"></input></td>
                </tr>
                </tbody>
                </table>
                </div>
                </div>
                </section>
                </div>
                </div>
                     <div class="row">
                             <div class="col-sm-12">
                                <div class="form-group"> 
                                   <center><button class="btn btn-info" name="action" value="" ><i class="fa fa-hand-o-right"></i>Issue</button></center>
                                </div>
                            </div>
                          </div>
                </div>
                </div>
                </div>
                </div>

        <!--========== Assign end of modal============= -->   
        <!--start of modal  -->
              <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="rejectItemReq" class="modal fade">
                <div class="modal-dialog" style="width:75%">
                    <div class="modal-content">
                                    <div class="modal-header">



                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Important Message</h4>
                                    </div>
                    <div class="modal-body">
                             <div class="row">
                             <div class="col-sm-12">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Message<span
                                                class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type="longtext" class="form-control"
                                                id="closeStRequest" name="closeStRequest"/>
                            </div>
                        </div>
                        </div>
                            </div>
                            <div class="row">
                             <div class="col-sm-12">
                                <div class="form-group"> 
                                   <center><button class="btn btn-info" name="action" value="" ><i class="fa fa-hand-o-right"></i>Submit</button></center>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
                </div>
                </div>

        <!-- end of modal --> 
        <!-- All Modal End -->

        </div>
        </section>
        </div>
        </div>
        </div>
        

<!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
             </div>
        <br>
        <div class="modal-footer" style="margin-top: 250px;position:relative; ">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      
    </div>
  </div>

<!-- ======================end temp approval authorityn tab  -->
                    
    </jsp:body>
</t:layout>