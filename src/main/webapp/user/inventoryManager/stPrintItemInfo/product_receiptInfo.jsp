<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:layout>

	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
    
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    
    <!--dynamic table-->
    <script type="text/javascript" language="javascript"src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"	src="assets/js/data-tables/DT_bootstrap.js"></script>
<script		src="assets/js/dynamic_table_init.js"></script>
    
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
 
        	<!--script for editable item receipt table-->
	<script src ="assets/js/customJS/item_receipt_table.js"></script>
	<script>
	 jQuery(document).ready(function() {
	        ItemReceiptTable.init();
	    });
	</script>
    </jsp:attribute>
	<jsp:body>
	

      <!-- page heading start-->
        

        <div class="page-heading">
        <!-- Cab/Ticket -->
          <section class="panel">
                        <header class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs">
                                <!-- <li class="active">
                                    <a href="#ticket_requests" data-toggle="tab">Ticket Booking</a>
                                </li> -->
                               <!--  <li >
                                    <a href="#cab_requests" data-toggle="tab">Cab Request</a>
                                </li> -->
                                <li class="active">
                                    <a href="#item_receipt_info" data-toggle="tab">Receipt Information</a>
                                </li>
                               <!--  <li>
                                    <a href="#contact_info" data-toggle="tab">Contact Information</a>
                                </li>
                               
                                <li>
                                    <a href="#payment_info" data-toggle="tab">Payment Terms</a>
                                </li>
                                <li>
                                    <a href="#mis_report" data-toggle="tab">Mis Report</a>
                                </li> -->
                                
                                <!-- <li>
                                    <a href="#admin_travel_department" data-toggle="tab">Transport Admin</a>
                                </li> -->
                            </ul>
                        </header>

<!-- =====Tab Pannel Start Here -->
<div class="panel-body">
    <div class="tab-content">
        
<!--============= Form for Ticket Requests ==================-->

<!--============= End Here Ticket Requests ==================-->


<!-- ==========Form Hotel / Guest House Booking =======  -->
    <div class="tab-pane active" id="item_receipt_info">
       <!--  <form action="ItemreciptInfoServlet" method="post" id="ItemReceiptServlet"> -->
        <form action="ItemreciptInfoServlet" method="post" id="ItemReceiptServlet"  onsubmit="return getData();">
        			
                <div class="row">
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Purchase Number</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" placeholder="Purchase Number" class="form-control" id="pNumber" name="pNumber">
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Choose Supplier<span
												class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2"
												id="prdRecSupplier" name="prdRecSupplier">
                                <option value="">Select...</option>
                                
                                <c:forEach var="vendor"  items="${itemRecieptInfo.vendorInfos}">
                                <option value="${vendor.id}"><c:out value="${vendor.name}"></c:out></option>
                                </c:forEach>
                                
                              </select>
                            </div>
                        </div>
                    </div>
                   
                  
                         
                </div>
                </br>
				<div class="row">
                        <div class="col-sm-12">
                <section class="panel">
               <!--  <header class="panel-heading">
                    Editable Table
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                     </span>
                </header> -->
                
                <div class="btn-group">
                        <button id="editable-sample_new" class="btn btn-primary" >
                            Add New <i class="fa fa-plus"></i>
                        </button>
                 </div>
                 <input type="hidden" name="total" id="total" value="1">
                    
                <div class="panel-body">
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    
                    <div class="btn-group pull-right">
                        <!-- <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                        </button> -->
                       <!--  <ul class="dropdown-menu pull-right">
                            <li><a href="#">Print</a></li>
                            <li><a href="#">Save as PDF</a></li>
                            <li><a href="#">Export to Excel</a></li>
                        </ul> -->
                    </div>
                </div>
                <div class="space15"></div>
                <table class="table table-striped table-hover table-bordered" id="editable-sample">
                <thead>
                <tr>
                    <th>Product Category</th>
                    <th>Prodcut Brand</th>
                    <th>Prodcut Name</th>
                    <th>Qty Ordered</th>
                    <th>Qty Received</th>
                    <th>Unit Price</th>
                </tr>
                </thead>
                <tbody id="mytrtd">
        <c:forEach items="${itemRecieptInfo.stockInfoList}" var="stock">
        <tr>
        <td><c:out value="${stock['productDetail']['categary']['categary']}"/></td>
        <td><c:out value="${stock['productDetail']['brand']}"/></td>
        <td><c:out value="${stock['productDetail']['name']}"/></td>
        <td><c:out value="${stock['availableStock']}"/></td>
        <td><c:out value="${stock['availableStock']}"/></td>
        <td><c:out value="${stock['availableStock']}"/></td>
        </tr>
        </c:forEach>       
 <%--  <tr class=""> 
                     <td>
          <select class="form-control small select2" name="receiptItemCat0" id="receiptItemCat0" style="width: 100%;" onchange="abledAddRowButtuon(0)">
          <option value="">Select Category</option>
          <option value="2">option1</option>
          <option value="2">option2</option>
          </select></td>
                    <td>
          <select class="form-control small select2" name="receiptItemBrand0" id="receiptItemBrand0" style="width: 100%;" onchange="abledAddRowButtuon(0)">
          <option value="">Select Category</option>
          <option value="1">option1</option>
          <option value="2">option2</option>
          </select>        
                    </td>
                    <td>
          <select class="form-control small select2" name="receiptItemName0" id="receiptItemName0" style="width: 100%;" onchange="abledAddRowButtuon(0)">
          <option value="">Select Category</option>
          <option value="1">option1</option>
          <option value="2">option2</option>
          </select>     
                    </td>
           <td class="center">
               <input type="text" class="form-control small" name="itemQtyOrd0" id="itemQtyOrd0" onkeyup="abledAddRowButtuon(0)">
           </td>
           <td>
               <input type="text" class="form-control small" name="itemQtyRec0" id="itemQtyRec0" onkeyup="abledAddRowButtuon(0)">
           </td>
            <td>
                <input type="text" class="form-control small" name="itemUnitPrice0" id="itemUnitPrice0" onkeyup="abledAddRowButtuon(0)">
            </td> 
                </tr>   --%>
                </tbody>
                </table>
                </div>
                </div>
                </section>
                </div>
				</div>
                <br>
           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center><button class="btn btn-info" id="pdtRecSubmit" type="submit"><i class="fa fa-hand-o-right"></i> Submit</button></center>

                        </div>
                    </div>
            </div>
            </form>
    </div>
    </div>
    </div>
    </section>
    <!-- Next form Fields E- Attachment  -->
    <!-- ===== Dyanmic Table Start Here ====== -->


    <!-- ===== Dynamic Table End Here ===== -->
    </div>
    <!-- page heading end-->
        
        <!-- validation -->
    </jsp:body>
</t:layout>