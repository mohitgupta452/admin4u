<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="com.admin4u.views.PriorityTatMapBean"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:layout>
<jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    
      <script src ="assets/js/customJS/prioritytatmap.js"></script>
    <!--dynamic table-->
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>
    </jsp:attribute>
    <jsp:body>
        <div class="page-heading">
              </div>
    <!-- page heading end-->

    <!--body wrapper start-->
        <div class="wrapper">
           <!--  Body contents goes here -->
<section class="panel">
<header class="panel-heading custom-tab dark-tab">Head
</header>
    
<form action="PriorityTatMapServlet" method="post" id="priorityTatMap" novalidate= "novalidate"  class="cmxform form-horizontal adminex-form" style="padding:30px;">
 
 
 <!-- =========Modal code=========== -->

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="editTatModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Edit</h4>
                                    </div>
                                    <div class="modal-body">
                                    <br>
                                    
                                     <div class="row">
                                            
                           
                    <div class="col-sm-12">
                        <div class="form-group"> 
                        <h3>Edit</h3>
                           <div class="">
                           
                           
                           <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <div class="col-sm-5">
  
                                <label>Priority</label>
                          </div>
                           <div class="col-sm-7">
                            <input class="form-control alphabetOnly" id="editPriority" name="editPriority"/>                         
                                
                              <br>
                                                  
                                </div>
                                
                           </div>
                           </div>
                           
                            <input class="form-control" id="priorityTatMapID" name="priorityTatMapID" style="display:none;"/>                         
                           
                           
                           <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <div class="col-sm-5">
  
                                <label>TAT</label>
                          </div>
                           <div class="col-sm-7">
                            <input class="form-control" id="editTat" name="editTat" type="number" min="0"/>                         

                                                  
                                </div>
                                
                           </div>
                           
                                                      
                           
                           </div>
                           
                           
                           
                           
                           <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <div class="col-sm-5">
  
                                <label>Service</label>
                          </div>
                           <div class="col-sm-7">
 <select class="form-control" id="editService" name="editService">
                            <option value="">Select...</option>
                            <c:forEach items="${priorityTat.services}" var="service">
                            <option value="${service}">${service}</option>
                            
                            </c:forEach>
                            
                            </select>     
                                                  
                                </div>
                                
                           </div>
                           
                                                      
                           
                           </div>
                           
                           
                               
                           
                                                 
                                </div>
                                
                           </div>
                                            <center><button type="button" class="btn btn-info "  data-dismiss="modal" class="close" onclick="callUpdatePriorityTatMapServlet()"><i class="fa fa-hand-o-right"></i> OK</button></center>
                      
                      </div>
                                          </div>
                                    
                                    
                                    
                                    

                                       
                                    </div>

                                </div>
                            </div>
                        </div>


<!-- ==========End Modal==================== -->
 
 
 
 
 
 
 
 <div class="row"> 
 <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <div class="col-sm-5">
  
                                <label>Priority</label>
                          </div>
                           <div class="col-sm-7">
                            <input class="form-control alphabetOnly" id="priority" name="priority"/>                         
                                
                              <br>
                                                  
                                </div>
                                
                           </div>
                           </div>
                           
                           <div class="col-sm-8 col-md-4">
                      <div class="form-group"> 
                        <div class="col-sm-5">
  
                                <label>TAT</label>
                          </div>
                           <div class="col-sm-7">
                            <input class="form-control" id="tat" name="tat" type="number" min="0"/>  in minutes                       

                                                  
                                </div>
                                
                           </div>
                           
                                                      
                           
                           </div>
                           
                           
                           
                           
                           <div class="form-group"> 
                        <div class="col-sm-3">
  
                                <label>Service</label>
                          </div>
                           <div class="col-sm-3">
                            <select class="form-control" id="service" name="service">
                            <option value="">Select...</option>
                            <c:forEach items="${priorityTat.services}" var="service">
                            <option value="${service}">${service}</option>
                            
                            </c:forEach>
                            
                            </select>                         
                                
                              <br>
                                                  
                                </div>
                                
                           </div>
<br>
             <button type="button" class="btn btn-info"  name="action" value="ADDNEWPRIORITY" onclick="callAddPriorityTatMapServlet()"  ><i class="fa fa-hand-o-right"></i> Add Priority</button>

<div id="targetDiv" class="ms-list" style="width:200px;height:150px;float: right;" >


</div>
          
          
          
  </form>


  <!-- =============================Table Start Here -->
<section class="panel">
<table class="table table-bordered table-striped table-condensed" id="myTable">
<tr>
 <th>Priority Id</th> 

 <th>Priority</th> 
 <th>TAT in Minutes</th>
 <th>Service Assingned</th>
 <th>Edit</th>  
</tr>
         <c:forEach  items="#{priorityTat.priorityTatList}" var="priority">

<tr id="rid${priority.id}">
<td><c:out value="${priority.id}"></c:out></td>
<td><c:out value="${priority.priority}"></c:out></td> <td><c:out value="${priority.turnAroundTime}"></c:out></td>
<td><c:out value="${priority.service}"></c:out></td> 

<td><button type="button" class="btn btn-info editButton" data-target="#editTatModal" data-toggle="modal" ><i class="fa fa-hand-o-right"></i> Edit</button></td></tr>

</c:forEach>
</table>

</section><br>
  <!-- ==============table end here -->
  


</section>



        </div>
        <!--body wrapper end-->
                      
       

  
        
    </jsp:body>
</t:layout>