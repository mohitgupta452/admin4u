<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="com.admin4u.views.RoleManagementBean"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:admin>
<jsp:attribute name="header">
    </jsp:attribute>
    <jsp:attribute name="extrajs">
     <script src ="assets/js/customJS/admin_service_panel.js"></script>

    <!--dynamic table-->
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>

    </jsp:attribute>
    <jsp:body>
        <div class="page-heading">
              </div>
    <!-- page heading end-->

    <!--body wrapper start-->
        <div class="wrapper">
           <!--  Body contents goes here -->
<section class="panel">
<header class="panel-heading custom-tab dark-tab">Head
</header>
    

<!--  manage designation starts -->
                  
                                    <div class="panel-body">
            
              
                       <div class="form-group">
                               <!--  <label  class="col-lg-8 col-sm-2 control-label">Designation</label> -->
                               <div class="row">
                        <div class="col-lg-6">        
                              <label  class=" control-label">Approval Designation</label> 
                                <select id="designation[]" name="selectdesig" size=10 class="form-control dd" multiple="multiple" >
                       
                         <c:forEach items="${ASPBean.designationList}" var="desig"> 
                    
                       <option value="${desig}"><c:out value="${desig}"/></option>
                      
                         </c:forEach>
                       </select>  
                      
                    </div>           
                
                                <div class="col-lg-6">
                                 <label  class=" control-label">Non-Approval Designation</label> 
            <select multiple id="myDesignation[]" size=10 class="form-control nadd ">
            
            <c:forEach items="${ASPBean.getNonApprovalDesignation}" var="npdesig">
            <option value="${npdesig}"><c:out value="${npdesig}"/></option>
            </c:forEach>
          </select>
   
                    </div>     
                                </div>
                                <br/><br/>
                                <div class ="row">
                                <div class="col-lg-6">  
                       <button  class="btn btn-info " value="add" id="add" onclick="nonApprovalDesignation(<c:out value="'add'"/>)">Submit</button>        
                          </div>
                          <div class="col-lg-6">  
                       <button  class="btn btn-info " value="remove" id="remove" onclick="nonApprovalDesignation(<c:out value="'remove'"/>)">Remove</button>  
                          </div>
                          </div>
                            </div>
                        
                            
                            </div>

  

<!--  manage designation ends -->

</section>
</div>
        <!--body wrapper end-->
                      
       

  
        
    </jsp:body>
</t:admin>
