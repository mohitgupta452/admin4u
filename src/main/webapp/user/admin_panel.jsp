<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@page import="com.admin4u.views.AdminServicePanelBean"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:admin-layout>
<jsp:attribute name="header">
<style>
     .boxx
     { 
     
        display : none; 
     }

     .requiredField 
    {
       color: red;
    }
#myModal2 .modal-dialog  
{
width:60%;
margin: 10px auto!important;
}
#myModal2 .modal-body
{
    padding : 0px!important;
    line-height : 16px !important;
    font-size : 12px !important;
    margin-left:00px;
    margin-top:20px;
    color:#000000 !important;
} 

.tab-container
{
	-webkit-box-shadow: 0px 0px 3px -1px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 0px 3px -1px rgba(0,0,0,0.75);
    box-shadow: 0px 0px 3px -1px rgba(0,0,0,0.75);
    background-color: #fff;
    padding: 10px 15px;
    border-top: 5px solid #546E7A;
}
.nav-tabs>li>a
{
	color: #546E7A !important;
    font-size: 11px !important;
    font-weight: 600;
}
.nav-tabs>.tb-list
{
	background: rgba(235,233,249,1);
	background: -moz-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(235,233,249,1)), color-stop(50%, rgba(216,208,239,0.98)), color-stop(51%, rgba(206,199,236,0.97)), color-stop(100%, rgba(193,191,234,0.95)));
	background: -webkit-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	background: -o-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	background: -ms-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	background: linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebe9f9', endColorstr='#c1bfea', GradientType=1 );
	border-radius:0px !important;
	cursor:pointer;
}
.nav>.tb-list>a:hover, .nav>.tb-list>a:focus
{
	background: rgba(235,233,249,1);
	background: -moz-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	background: -webkit-gradient(left bottom, right top, color-stop(0%, rgba(235,233,249,1)), color-stop(50%, rgba(216,208,239,0.98)), color-stop(51%, rgba(206,199,236,0.97)), color-stop(100%, rgba(193,191,234,0.95)));
	background: -webkit-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	background: -o-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	background: -ms-linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	background: linear-gradient(45deg, rgba(235,233,249,1) 0%, rgba(216,208,239,0.98) 50%, rgba(206,199,236,0.97) 51%, rgba(193,191,234,0.95) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebe9f9', endColorstr='#c1bfea', GradientType=1 );
	border-radius:0px !important;
 	filter: grayscale(100%);
 	-webkit-filter: grayscale(100%);
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus
{
	border-radius:0px !important;
}
.tnt
{
	height: auto;
    border: 1px solid #ccc;
    border-radius: 10px;
    margin: 13px;
    background: #ECEFF1;
}
.tnt-label
{
	color: #455A64;
    font-weight: 500 !important;
    font-family: inherit;
}
.tnt-labelhd
{
    font-weight: 700 !important;
    text-transform: uppercase;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus
{
	background-color: #fff;
    border: 0px !important;
}

.inner-box
{
	border: 1px solid #ccc;
    border-radius: 10px;
    background: #ECEFF1;
    padding-top: 20px;
}
.inner-box2
{
	border: 1px solid #ccc;
    border-radius: 10px;
    background: #ECEFF1;
    padding-top: 20px;
    padding-bottom: 20px;
    margin: 0px;
}
.table-bordered thead tr th
{
    white-space: nowrap;
    vertical-align: middle;
    padding-left: 14px;
    padding-right: 14px;
    background-color: #455A64;
    color: #fff;
}
.table.display thead tr th
{
	white-space: nowrap;
    vertical-align: middle;
    padding-left: 14px;
    padding-right: 14px;
    background-color: #455A64;
    color: #fff;
}
.tbl-th
{
	white-space: nowrap !important;
    vertical-align: middle !important;
    padding-left: 14px !important;
    padding-right: 14px !important;
    background-color: #455A64 !important;
    color: #fff !important;
}
.add_btn
{
	color: #fff;
    background: #455A64;
    padding: 5px;
    border-radius: 30px;
    margin-top: 2px !important;
}
</style>


<!--ios7-->
    <link rel="stylesheet" type="text/css" href="assets/js/ios-switch/switchery.css" />
          <link rel="stylesheet" type="text/css" href="assets/js/jquery-multi-select/css/multi-select.css" />


 <!--pickers css-->
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datepicker/css/datepicker-custom.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-timepicker/css/timepicker.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-colorpicker/css/colorpicker.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">


<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-fileupload.min.css" />

    </jsp:attribute>
    
    <jsp:attribute name="extrajs">
    
   <script src ="assets/js/customJS/admin_service_panel.js"></script>
    
    <!--dynamic table-->
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>
<script	src="assets/js/admin_panel_dtable_init/admin_panel_dynamic_init.js"></script>
<script type="text/javascript" src="assets/js/customJS/editable_ghSetting.js"></script>

<!--multi-select-->
<script type="text/javascript" src="assets/js/jquery-multi-select/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="assets/js/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="assets/js/multi-select-init.js"></script>
<script>
jQuery(document).ready(function(){
	GuestHouseInventory.init();
});
</script>
<script type="text/javascript">
  
  $(document).ready(function(){
    $('#setSpaceSetting').change(function(){
        $('#setPatternInfo')[ ($("option[value='Mannual']").is(":checked"))? "show" : "hide" ](); 
    });
    $('#spaceCategory').change(function(){
        console.log("exe");
      var floorCode = $('#spaceCategory option:selected').html();
      $('#spaceAllocType').val(floorCode.substring(0, 4));
      console.log("dddd"+floorCode.substr(0, 4)+floorCode+floorCode.substring(0, 4));
    });
    $('#buildingFloor').change(function(){
      var spaceCategoryVal = $('#buildingFloor option:selected').html();
        $('#spacefCode').val(spaceCategoryVal.substring(0, 7));
    });
  });
</script>
    </jsp:attribute>
    <jsp:body>
<!--start temp modal  -->   

<!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">

<div class="modal-body">

                                                   <!-- Ocupancy Setting content-->
                                                   <section class="panel">
                        <header class="panel-heading custom-tab yellow-tab">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#singleOcupancy">
                                        <i class="fa fa-user"></i>
                                        Single Ocupancy
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#doubleOcupancy"  >
                                        <i class="fa fa-user"></i>
                                        Double Ocupancy
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tripleOcupancy" >
                                        <i class="fa fa-envelope-o"></i>
                                        Triple Ocupancy
                                    </a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content  content-style">
                                <div id="singleOcupancy"  class="tab-pane active">
                                   <!-- Table Start -->
        <table class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">Bed Type</th>
                            <th class="">AC / Non AC</th>
                            <th class="">TV</th>
                            <th class="">Sofa in Room </th>
                            <th class="">Phone</th>
                            <th class="">Wi-fi Connectivity</th>
                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="singleOcupancytbl">
                        
                        </tbody>
                    </table>

                                   <!-- Table End -->
                                 
                                </div>
                                <div id="doubleOcupancy" class="tab-pane "> 
                                <!-- Table 2 Start -->
        <table class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">Bed Type</th>
                            <th class="">AC / Non AC</th>
                            <th class="">TV</th>
                            <th class="">Sofa in Room </th>
                            <th class="">Phone</th>
                            <th class="">Wi-fi Connectivity</th>

                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="doubleOcupancytbl">
                        
                        </tbody>
                    </table>

                                   <!-- Table End --></div>
                                <div id="tripleOcupancy" class="tab-pane">
                                   <!-- Table Start -->
        <table class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">Bed Type</th>
                            <th class="">AC / Non AC</th>
                            <th class="">TV</th>
                            <th class="">Sofa in Room </th>
                            <th class="">Phone</th>
                            <th class="">Wi-fi Connectivity</th>

                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="tripleOcupancytbl">
                        
                        </tbody>
                    </table>

                                   <!-- Table End -->
                                </div>
                            </div>
                        </div>
                    </section>
                                                   <!-- End Ocupancy Setting Content -->

                                                </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- end temp modal -->      
                 <!-- page heading start-->
        

        <div class="page-heading">
                            
        
        
              </div>
    <!-- page heading end-->

    <!--body wrapper start-->
        <div class="wrapper">
           <!--  Body contents goes here -->


<!-- =================manage service panel start here -->


<div class="row">
	<div class="col-md-12">
		<ul class="nav nav-tabs" id="myTab">
			<li class="active tb-list"><a data-target="#tab1" data-toggle="tab">MANAGE SERVICES</a></li>
			<li class="tb-list"><a data-target="#tab2" data-toggle="tab">MANAGE MEETING ROOM</a></li>
			<li class="tb-list"><a data-target="#tab3" data-toggle="tab">ADD NEW BUILDING</a></li>
			<li class="tb-list"><a data-target="#tab4" data-toggle="tab">STATIONERY/PRINTING MASTER</a></li>
			<li class="tb-list"><a data-target="#tab5" data-toggle="tab">GUEST HOUSE INVENTORY</a></li>
			<li class="tb-list"><a data-target="#tab6" data-toggle="tab">MANAGE REFERENCE DOC</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab1">
				<div class="col-md-12 tab-container">
					<form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label  class="col-lg-8 col-sm-2 tnt-labelhd">Tour & travel</label>
                                <div class="col-lg-4">
                                <a href="#tourTravelCollapse" data-toggle="collapse">  <i class="fa fa-chevron-circle-down" style="font-size: 19px;color: #37474F;"></i> <!-- <input type="checkbox" class="js-switch js-check-change-tour-travel"   /> --></a>
                                    
                                </div>

                                <!-- =====toggle tour travel panel======= -->
                                    &nbsp;
                                <div id="tourTravelCollapse" class="collapse tnt">
                               
                                <div class="form-group">
                                <label  class="col-lg-8 col-sm-2 control-label tnt-label">Air Ticket</label>
                                <div class="col-lg-4">
                                    <input type="checkbox" onclick="callSetOnOff(<c:out value="'AIRTICKET'"/>,this)" class="js-switch js-check-change-air-ticket" id="airTicketButton" <c:if test="${ASPBean.locationServicesMap['AIRTICKET']==1}"><c:out value="checked"/></c:if> />
                                    <!--<p class="help-block">Example block-level help text here.</p>-->
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-lg-8 col-sm-2 control-label tnt-label">Cab Booking</label>
                                <div class="col-lg-4">
                                    <input type="checkbox" onclick="callSetOnOff(<c:out value="'CAB'"/>,this)"  <c:if test="${ASPBean.locationServicesMap['CAB']==1}"><c:out value="checked"/></c:if> class="js-switch js-check-change-cab-booking" id="cabTicketButton" />
                                    <!--<p class="help-block">Example block-level help text here.</p>-->
                                </div>
                            </div>

                            <div class="form-group">
                                <label  class="col-lg-8 col-sm-2 control-label tnt-label">Train Ticket</label>
                                <div class="col-lg-4">
                                    <input type="checkbox" onclick="callSetOnOff(<c:out value="'TRAINTICKET'"/>,this)"  class="js-switch js-check-change-train-ticket" id="trainTicketButton" <c:if test="${ASPBean.locationServicesMap['TRAINTICKET']==1}"><c:out value="checked"/></c:if> />
                                    <!--<p class="help-block">Example block-level help text here.</p>-->
                                </div>

                            </div>

                                </div>

                                <!-- ===============end toggles tour and travel panel -->
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-8 col-sm-2 tnt-label">Hotel/Guest House</label>
                                <div class="col-lg-4">
                                    <input type="checkbox" class="js-switch"  onclick="callSetOnOff(<c:out value="'HOTEL'"/>,this)" <c:if test="${ASPBean.locationServicesMap['HOTEL']==1}"><c:out value="checked"/></c:if> />
                                    <!--<p class="help-block">Example block-level help text here.</p>-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-8 col-sm-2 tnt-label">Repair/Manintenance</label>
                                <div class="col-lg-4">
                                    <input type="checkbox" onclick="callSetOnOff(<c:out value="'REPAIRMAINTENANCE'"/>,this)" class="js-switch" <c:if test="${ASPBean.locationServicesMap['REPAIR_MAINTENANCE']==1}"><c:out value="checked"/></c:if>/>
<!--                                 <p class="help-block">Example block-level help text here.</p>
 -->                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-lg-8 col-sm-2 tnt-label">House Keeping</label>
                                <div class="col-lg-4">
                                    <input type="checkbox" onclick="callSetOnOff(<c:out value="'HOUSEKEEPING'"/>,this)" class="js-switch"  id="hkp" name="hkp" <c:if test="${ASPBean.locationServicesMap['HOUSEKEEPING']==1}"><c:out value="checked"/></c:if> />
                               </div>
                            </div>
 
                            
                        </form>
				</div>
			</div>
			<div class="tab-pane" id="tab2">
				<div class="col-md-12 tab-container">
				 
					  <form method="post" action="AdminServicePanelServlet" id="manageSpaceForm">
					  <div class="form-group inner-box2">
                    	<div class="row">
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-5">
                                        <label class="tnt-label">Building Name</label>
                                    </div>
                                    <div class="col-sm-7">
                                      <Select  class="form-control select2" id="buildingName" name="building">
                                      <option value="">Select</option>
                                      <c:forEach items="${ASPBean.buildingList}" var="building">
                                      <option value="${building.id}"><c:out value="${building.buildingName}"></c:out></option>
                                      </c:forEach>
                                      </Select>
                                    </div>
                                </div>
                            </div>
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                 <div class="col-sm-5">
                                        <label class="tnt-label">Select Floor</label>
                                    </div>
                                    <div class="col-sm-7">
                                      <select class="form-control select2" name="buildingFloor" id="buildingFloor">
                                        <option  value="">Select Floor...</option>
                                        
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-4">
                                <div class="form-group" style="display: none;"> 
                                    <div class="col-sm-5" >
                                        <label class="tnt-label">Area / Location</label>
                                    </div>
                                   <div class="col-sm-7">
                                      <input type ="text" value="" placeholder="Building location" class="form-control" id="buildingLocation" name="buildingLocation">
                                  </div>
                                </div>
                            </div>
                        </div>
                        
                           <br>
                        <div class="row">
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-5">
                                        <label class="tnt-label">Select Category</label>
                                    </div>
                                    <div class="col-sm-7">
                                     <select class="form-control select2" name="spaceCategory" id="spaceCategory">
                                        <option value="">Select</option>
                                        <c:forEach items="${ASPBean.spaceCategoryList }" var="spaceCategory">
                                         <option value="${spaceCategory }"><c:out value="${spaceCategory }"></c:out></option>
                                        </c:forEach>
                                        
                                    </select>
                                </div>
                            </div>
                            </div>
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                 <div class="col-sm-5">
                                        <label class="tnt-label">Availablity(NOs)</label>
                                    </div>
                                    <div class="col-sm-7">
                                     <input type ="text"  placeholder="Total Available" class="form-control" id="spaceAvail" name="spaceAvail">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-5" >
                                        <label class="tnt-label">Set Code(If Yes)</label>
                                    </div>
                                   <div class="col-sm-7">
                                      <select class="form-control select2" name="setSpaceSetting" id="setSpaceSetting">
                                        <option selected="None" value="None">None</option>
                                        <option value="Mannual">Mannual Setting</option>
                                    </select>
                                  </div>
                                </div>
                            </div>
                          </div>
                          
                          <br>
                        <div class="row" id="setPatternInfo" style="display:none">
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-5">
                                        <label>Start Patterns</label>
                                    </div>
                                    <div class="col-sm-7">
                                     <input type ="text"  placeholder="Set Code" class="form-control" id="spacePattern" name="buildingPattern">
                                </div>
                              </div>
                            </div>
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                 <div class="col-sm-7">
                                    <input type ="text"  readonly=""  class="form-control" id="spaceAllocType" name="spacePattern">
                                    </div>
                                    <div class="col-sm-5">
                                     <input type ="text"  readonly=""  class="form-control" id="spacefCode" name="floorPattern">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-6" >
                                        <input type ="text"  placeholder="Start Range" class="form-control" id="spaceStRange" name="startRange">
                                    </div>
                                   <div class="col-sm-6">
                                      <input type ="text"  placeholder="End Range" readonly="" class="form-control" id="spaceEnRange" name="endRange">
                                  </div>
                                </div>
                            </div>
                          </div>
                      <br>
                      
                       <div class="row">
                             <div class="col-sm-12">
                                <div class="form-group"> 
                                   <center><button class="btn btn-primary" name="action" value="MANAGESPACE" ><i class="fa fa-hand-o-right"></i> Submit</button></center>
                                </div>
                            </div>
                          </div>
                        
                        
                        
                        
                    </div>
                  </form>
                   
				</div>
			</div>
			
			
			
			
			
			<div class="tab-pane" id="tab3">
				<div class="col-md-12 tab-container">
					 <form method="post" action="AdminServicePanelServlet" id="manageBuildingForm">
                    	<div class="form-group inner-box2">
                            <div class="row">
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-5">
                                        <label class="tnt-label">Building Name</label>
                                    </div>
                                    <div class="col-sm-7">
                                      <input type ="text" value="" placeholder="Building Name" class="form-control"  name="buildingName">
                                    </div>
                                </div>
                            </div>
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                 <div class="col-sm-5">
                                        <label class="tnt-label">Select Floor</label>
                                    </div>
                                    <div class="col-sm-7">
                                      <select class="form-control select2" name="buildingFloor">
                                        <option  value="">select Floor</option>
                                        <option value="Floor-1">Floor-1</option>
                                        <option value="Floor-2">Floor-2</option>
                                        <option value="Floor-3">Floor-3</option>
                                        
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-5" >
                                        <label class="tnt-label">Area / Location</label>
                                    </div>
                                   <div class="col-sm-7">
                                      <input type ="text" value="" placeholder="Building location" class="form-control"  name="buildingLocation">
                                  </div>
                                </div>
                            </div>
                        </div>
                   <br>
                        
                       
                  
                                              <br>
                          <div class="row">
                             <div class="col-sm-12">
                                <div class="form-group"> 
                                   <center><button class="btn btn-primary" name="action" value="MANAGEBUILDING" ><i class="fa fa-hand-o-right"></i> Submit</button></center>
                                </div>
                            </div>
                          </div>
                           
                         </div>
                  
                  </form>
				</div>
			</div>
			<div class="tab-pane" id="tab4">
				<div class="col-md-12 tab-container">
					 <form method="post" action="ItemMasterServlet" id="masterItemForm">
                    
                 
                    <div class="panel-body">
                         <div class="form-group inner-box2">
                             <div class="row">
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-7">
                                        <label class="tnt-labelhd">Product Category</label>
                                    </div>
                                    <div class="col-sm-5">
                                    <!-- <input type ="text" value="" placeholder="Item Name" class="form-control" id="masterItemName" name="masterItemName"> -->
                                     
                                    </div>
                                </div>
                            </div>
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-7">
                                <!--         <label>Product Category</label> -->
                                    </div>
                                    <div class="col-sm-5">
                                    <!-- <input type ="text" value="" placeholder="Item Name" class="form-control" id="masterItemName" name="masterItemName"> -->
                                     
                                    </div>
                                </div>
                            </div>
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                 <div class="col-sm-5">
                                        <!-- <label>Select Category</label> -->
                                    </div>
                                    <div class="col-sm-7">
                                      <select class="form-control select2 addsubcat" name="masterCat" id="itemCatPdtMaster" onchange="getSubCategory(this.value)">
                                      <option value=""><c:out value="Select category"/></option>
                                       <c:forEach var="masterCat" items="${admin.masterCategary}">
                                       <option value="${masterCat.ancestor.id}"><c:out value="${masterCat.ancestor.categary}"/></option>
                                       </c:forEach>
                                    </select>
                                    </div>
                                </div>
                            </div>
                            </div>
                            </br>
                            <div class="row">
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-5">
                                        <label class="tnt-label">Product/Item Name</label>
                                    </div>
                                    <div class="col-sm-7">
                                    <input type ="text" value="" placeholder="Item Name" class="form-control" id="masterItemName" name="masterItemName">
                                     
                                    </div>
                                </div>
                            </div>
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                 <div class="col-sm-5">
                                        <label class="tnt-label">Select Category</label>
                                    </div>
                                    <div class="col-sm-7">
                                     <select class="form-control select2" name="itemCatMaster" id="itemCatMaster">
                                        <option selected value="None" id="ItemNoPdt">select Category</option>
                                        
                                    </select>
                                    <a href="#" data-toggle="modal" data-target="#addNewCategory" id="newCategory"><i class="fa fa-plus add_btn" aria-hidden="true">&nbsp;Add New Category</i></a>
                                    </div>
    <!--start of modal  -->
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addNewCategory" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Add New Category</h4>
                                    </div>
                <div class="modal-body">
                  <!--   <div class="paxName">  -->
                 <div class="row">
                 <div class="col-sm-8 col-md-6">
                          <div class="form-group"> 
                           <div class="col-sm-5">
                                  <label class="tnt-label">Enter New Category<span class="requiredField"> *</span><br>&nbsp;</label>
                              </div>
                              <div class="col-sm-7">
                                <input type ="text" class="form-control addsubcat" id="newCatName" name="subcat" placeholder="New Brand">
                              </div>
                      </div>
                  </div>
                  <div class="col-sm-8 col-md-4">
                    
                  </div>

                  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                                  <div class="col-sm-5">
                                  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="addSubCat()">Ok</button>
                                  </div>
                                </div>
                  </div>
                  </div>
                  <!-- </div> -->
                </div>
                </div>
                </div>
                </div>

<!-- end of modal -->                    
                                </div>
                            </div>
                           <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                 <div class="col-sm-5">
                                        <label class="tnt-label">Brand</label>
                                    </div>
                                    <div class="col-sm-7">
                                    <input type ="text" class="form-control " id="newBrandName" name="brandName" placeholder="Brand">
                                    </div>

<!--start of modal  -->
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addNewBrand" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Add New Brand</h4>
                                    </div>
                <div class="modal-body">
                  <!--   <div class="paxName">  -->
                 <div class="row">
                 <div class="col-sm-8 col-md-6">
                          <div class="form-group"> 
                           <div class="col-sm-5">
                                  <label class="tnt-label">Enter Brand Name <span class="requiredField"> *</span><br>&nbsp;</label>
                              </div>
                              <div class="col-sm-7">
                                <input type ="text" class="form-control " id="newBrandName" name="newBrandName" placeholder="New Brand">
                              </div>
                      </div>
                  </div>
                  <div class="col-sm-8 col-md-4">
                    
                  </div>

                  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                                  <div class="col-sm-5">
                                  <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                                  </div>
                                </div>
                  </div>
                  </div>
                  <!-- </div> -->
                </div>
                </div>
                </div>
                </div>

<!-- end of modal -->                    
                                </div>
                            </div>
                        </div>
                   <br>
                        <div class="row">
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-5">
                                        <label class="tnt-label">Pack Size</label>
                                    </div>
                                    <div class="col-sm-7">
                                      <input type ="text"  placeholder="Pack Size" class="form-control" id="itemPackSize" name="itemPackSize">
                                    </div>
                                </div>
                            </div>
                           
                             <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                 <div class="col-sm-5">
                                        <label class="tnt-label">Units</label>
                                    </div>
                                    <div class="col-sm-7">
                                    <select class="form-control select2" name="masterUnits" id="masterUnits">
                                        <option selected="" value="None">select Units</option>
                                        <option value="Piece" >Piece</option>
                                        <option value="Nos" >Numbers</option>
                                    </select>
                                    </div>
<!--start of modal  -->
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addNewUnits" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                        <h4 class="modal-title">Add New Units</h4>
                                    </div>
                <div class="modal-body">
                  <!--   <div class="paxName">  -->
                 <div class="row">
                 <div class="col-sm-8 col-md-6">
                          <div class="form-group"> 
                           <div class="col-sm-5">
                                  <label class="tnt-label">Enter New Units <span class="requiredField"> *</span><br>&nbsp;</label>
                              </div>
                              <div class="col-sm-7">
                                <input type ="text" class="form-control " id="newBrandName" name="newBrandName" placeholder="New Brand">
                              </div>
                      </div>
                  </div>
                  <div class="col-sm-8 col-md-4">
                    
                  </div>

                  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                                  <div class="col-sm-5">
                                  <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                                  </div>
                                </div>
                  </div>
                  </div>
                  <!-- </div> -->
                </div>
                </div>
                </div>
                </div>

<!-- end of modal -->   



                                </div>
                            </div>
                            <div class="col-sm-8 col-md-4">
                                <div class="form-group"> 
                                    <div class="col-sm-5" >
                                        <label class="tnt-label">Cost Price </label>
                                    </div>
                                   <div class="col-sm-7">
                                      <input type ="text"  placeholder="Cost Price" class="form-control" id="itemCostPrice" name="itemCostPrice">
                                    </div>
                                </div>
                            </div>
                          </div>
                      <br>
                          <div class="row">
                             <div class="col-sm-12">
                                <div class="form-group"> 
                                   <center><button class="btn btn-primary" name="action" value="MANAGESPACE" type="submit"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                                </div>
                            </div>
                          </div>
                           
                         </div>
                      </div>
                      </form>
      <!-- table of product category starts  -->
         
         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Product Information
        </header>
        <div class="panel-body inner-box2">
        <div class="adv-table">
        <section id="flip-scroll">
                
         <table class="display table table-bordered admin-tbl"
							id="admin_service_panel_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Product Name</th>
            <th>Category</th>
            <th>Brand</th>
            <th>Pack size</th>  
            <th>Units</th>    
            <th>Cost Price</th>  
        </tr>
        </thead>
        <tbody id="mytbody">
     <!-- body of the table starts-->  
                          
     <!--  body of the table ends  -->         
         </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div> 
      
      <!-- table of product categories ends -->   
				</div>
			</div>
			<div class="tab-pane" id="tab5">
				<div class="col-md-12 tab-container">
					<form class="form-horizontal" id="addGhInventoryForm" role="form">
                            <div class="row">
                             <!-- Editable Table -->
                                <div class="col-sm-12">
                <section class="panel">
                <div class="panel-body inner-box2">
                            
                <!--  -->
                
                <div class="adv-table editable-table ">
                <div class="clearfix">
                    <div class="btn-group" style="margin-bottom:10px;">
                        <button id="editable-sample_new" type="button" class="btn btn-primary">
                            Add New <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="space15"></div>
                <div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid"><table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
                <thead>
                <tr role="row">
                <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" aria-label="First Name" style="width: 550px;">GH Name & Address</th>
                <th class="sorting tbl-th" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Last Name: activate to sort column ascending" style="width: 177px;">Single Ocupancy</th>
                <th class="sorting tbl-th" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Points: activate to sort column ascending" style="width: 116px;">Double Ocupancy</th>
                <th class="sorting tbl-th" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 216px;">Triple Ocupancy</th>
                <th class="sorting tbl-th" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1" aria-label="Delete: activate to sort column ascending" style="width: 118px;">Inactive</th>
                <th class="sorting tbl-th" role="columnheader" tabindex="0" aria-controls="editable-sample" rowspan="1" colspan="1"  style="width: 82px;">Action</th></tr>
                </tr>
                </thead>
                
                      <tbody role="alert" aria-live="polite" aria-relevant="all" id="tableBody" >
                        
                      <c:forEach var="ghouse" items="${ASPBean.ghinventoryList}" varStatus="count">
                      <tr class="odd">
                          <td class=" sorting_1"><c:out value="${ghouse['GuestHouseName']}"/></td>
                          <td class=" "><c:out value="${ghouse['singleOccupancy']}"/></td>
                          <td class=" "><c:out value="${ghouse['doubleOccupancy']}"/></td>
                          <td class="center "><c:out value="${ghouse['tripleOccupancy']}"/></td>
                          <td class=" "><button type="button" class="btn btn-danger deleteGhouse" value="${ghouse['guestHouseId']}">Delete</button></td>
                          <td class=" "><button type="button" data-toggle="modal" data-id="5" value="${ghouse['guestHouseId']}"  class="btn btn-warning setting">Setting</button>
                        </td>
                        
                      </tr>
                      
                      </c:forEach>
                      </tbody>
                </table>
                </div>
                </div>
                </div>
                </section>
                </div>
                             <!-- End Editable Table -->
                            </div>

                          </form>
				</div>
			</div>
			<div class="tab-pane" id="tab6">
				<div class="col-md-12 tab-container">
					<form method="post" id="refUpload" action="PolicyUpload" enctype="multipart/form-data">
                    
                 
                    <div class="panel-body">
                         <div class="form-group inner-box2">
                             <div class="row">
                             
                                                        </div>
                            </br>
                            <div class="row">
                             <div class="col-sm-8 col-md-5">
                                <div class="form-group"> 
                                 <div class="col-sm-6">
                                        <label class="tnt-label">Select Ref Category</label>
                                    </div>
                                    <div class="col-sm-6">
                                     <select class="form-control select2" name ="document" id="document">
                                        <option selected value="None" id="ItemNoPdt">select Category</option>
                                        <option value="terms">terms</option>
                                        
                                    </select>
                                    </div>
                    
                                </div>
                            </div>
                              <div class="col-sm-8 col-md-5">
                                <div class="form-group"> 
                                 <div class="col-sm-6">
                              <label for="t_type" class="tnt-label">upload document</label>
                              </div>
                              <div class="col-sm-6">
                                    <input type="file" name="upload"/>
                              </div>
                              </div>
                              </div>
                        </div>
                   <br>
                       
                      <br>
                          <div class="row">
                             <div class="col-sm-12">
                                <div class="form-group"> 
                                   <center><button type="submit" class="btn btn-primary" name="submit">submit</button></center>
                                </div>
                            </div>
                          </div>
                           
                         </div>
                      </div>
                      </form>
      <!-- table of product category starts  -->
         
         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Document Information
        </header>
        <div class="panel-body">
        <div class="adv-table">
        <section id="flip-scroll">
                
         <table class="display table table-bordered "
              id="admin_service_panel_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Ref. Document</th>
            <th>Date & Time </th>
            <th>Attachement</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody id="mytbody">
     <!-- body of the table starts-->  
          <c:forEach var="policyDoc" items="${policyUp.policyDocs}">
          <tr id="policyDoctr${policyDoc['id']}">
          <td>${policyDoc['docName']}</td>
          <td>${policyDoc['updationTime']}</td>
          <td><a  href="fileviwer?path=${policyDoc['path']}" target="_blank">Veiw File</a></td>

          <td><button  class="btn btn-danger" onclick="deletePolicy(${policyDoc['id']})">Remove</button></td>
          </tr>
          </c:forEach>
                          
                          
     <!--  body of the table ends  -->         
         </tbody>
        </table>
        </section>
        </div>
        </div>
        </section>
        </div>
        </div> 
      
      <!-- table of product category ends -->                    
                      
                      
                      
                      
                                         
                                    <!-- Modal -->
                                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal3" class="modal fade" style="display: none;">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                    <h4 class="modal-title">Modal Tittle</h4>
                                                </div>
                                                <div class="modal-body">

                                                   <!-- Ocupancy Setting content-->
                                                   <section class="panel">
                        <header class="panel-heading custom-tab yellow-tab">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#singleOcupancy">
                                        <i class="fa fa-user"></i>
                                        Single Ocupancy
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#doubleOcupancy"  >
                                        <i class="fa fa-user"></i>
                                        Double Ocupancy
                                    </a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#tripleOcupancy" >
                                        <i class="fa fa-envelope-o"></i>
                                        Triple Ocupancy
                                    </a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content  content-style">
                                <div id="singleOcupancy"  class="tab-pane active">
                                   <!-- Table Start -->
        <table class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">Bed Type</th>
                            <th class="">AC / Non AC</th>
                            <th class="">TV</th>
                            <th class="">Sofa in Room </th>
                            <th class="">Phone</th>
                            <th class="">Wi-fi Connectivity</th>
                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="singleOcupancytbl">
                        
                        </tbody>
                    </table>

                                   <!-- Table End -->
                                 
                                </div>
                                <div id="doubleOcupancy" class="tab-pane "> 
                                <!-- Table 2 Start -->
        <table class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">Bed Type</th>
                            <th class="">AC / Non AC</th>
                            <th class="">TV</th>
                            <th class="">Sofa in Room </th>
                            <th class="">Phone</th>
                            <th class="">Wi-fi Connectivity</th>

                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="doubleOcupancytbl">
                        
                        </tbody>
                    </table>

                                   <!-- Table End --></div>
                                <div id="tripleOcupancy" class="tab-pane">
                                   <!-- Table Start -->
        <table class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                            <th class="">Room No</th>
                            <th class="">Bed Type</th>
                            <th class="">AC / Non AC</th>
                            <th class="">TV</th>
                            <th class="">Sofa in Room </th>
                            <th class="">Phone</th>
                            <th class="">Wi-fi Connectivity</th>

                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="tripleOcupancytbl">
                        
                        </tbody>
                    </table>

                                   <!-- Table End -->
                                </div>
                            </div>
                        </div>
                    </section>
                                                   <!-- End Ocupancy Setting Content -->

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-success">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   

                                </div>
				</div>
			</div>
		</div>
	</div>
</div>




<%-- 

<div class="row">


            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading ">
                      <label  class="col-lg-8 col-sm-2"> Manage Designation</label>
                      <div class="col-lg-4">
                                <a href="#designationCollapse" data-toggle="collapse">   <i class="fa fa-th-list"></i><!-- <input type="checkbox" class="js-switch js-check-change-tour-travel"   /> --></a>
                      </div> 
                       <br/> 
                      </header>
                        
                 <div id="designationCollapse" class="collapse">
                  <div class="panel-body">
            
              
                       <div class="form-group">
                               <!--  <label  class="col-lg-8 col-sm-2 control-label">Designation</label> -->
                               <div class="row">
                        <div class="col-lg-6">        
                              <label  class=" control-label">Approval Designation</label> 
                                <select id="designation[]" name="selectdesig" size=10 class="form-control dd select2" multiple="multiple" >
                       
                         <c:forEach items="${ASPBean.designationList}" var="desig"> 
                    
                       <option value="${desig}"><c:out value="${desig}"/></option>
                      
                         </c:forEach>
                       </select>  
                      
                    </div>           
                
                                <div class="col-lg-6">
                                 <label  class=" control-label">Non-Approval Designation</label> 
            <select multiple id="myDesignation[]" size=10 class="form-control nadd select2">
            
            <c:forEach items="${ASPBean.getNonApprovalDesignation}" var="npdesig">
            <option value="${npdesig}"><c:out value="${npdesig}"/></option>
            </c:forEach>
          </select>
   
                    </div>     
                                </div>
                                <br/><br/>
                                <div class ="row">
                                <div class="col-lg-6">  
                       <button  class="btn btn-info " value="add" id="add" onclick="nonApprovalDesignation(<c:out value="'add'"/>)">Submit</button>        
                          </div>
                          <div class="col-lg-6">  
                       <button  class="btn btn-info " value="remove" id="remove" onclick="nonApprovalDesignation(<c:out value="'remove'"/>)">Remove</button>  
                          </div>
                          </div>
                            </div>
                        
                            
                            </div>
                           
                  </div>
                   
                      
                   
                  
                </section>

            </div>
            <!-- ======Manage service panel end====================== -->

                    </div> --%>
              
    </jsp:body>
</t:admin-layout>
