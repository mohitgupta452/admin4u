<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="com.admin4u.views.RoleBaseDashboardBean"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layoutApproval>

<jsp:attribute name="extrajs">
   <script src ="assets/js/customJS/index.js"></script>

</jsp:attribute>
	<jsp:body>
            
        <!--body wrapper start-->
        
 

      
                   <div class="row states-info">
                           <c:forEach var="entry" items="${roleBaseDashboardBean.componentsToDisplayMap}">
                   
            <div class="col-md-3">
                <!-- <div class="panel red-bg"> -->
                <div class="panel">
                <a href="<c:out value="${entry.value}"/>" >
                    <div class="imgbg "
							style="background-image: url(assets/images/.jpg); background-position: right;">
                        <span class="custom_text"><center><c:out value="${entry.key}"/></center></span>
                        <span class="custom_text2" style="color:#444 !important;text-decoration:none;"><center><c:out value="${entry.key}"/></center></span>
                    </div>
                </a>
                </div>
            </div>
            
            </c:forEach>
            
                
        </div>
            
            
                <!-- ==================Next Button Start Here================= -->
        <div class="row"> 
                <ul class="pager">
                  
                </ul>
        </div>
                           
        <!--body wrapper end-->

        <!--footer section start-->

        <footer class="sticky-footer">
            2014 &copy; Admin4U by Turningcloud
        </footer>

        <!--footer section end-->
    <!-- main content end-->

  
        
    </jsp:body>
</t:layoutApproval>
