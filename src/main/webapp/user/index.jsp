<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:layout>
	<jsp:body>
        <!--body wrapper start-->
      
            <div class="row">
            <section class="panel" style="padding-left: 16px;padding-right: 16px;">
            <div class="col-md-7 slider-box">
                        <div class="carousel slide auto panel-body"
					id="c-slide">
<!--                             <ol class="carousel-indicators out"> -->
<!--                                 <li data-target="#c-slide" -->
<!-- 							data-slide-to="0" class=""></li> -->
<!--                                 <li data-target="#c-slide" -->
<!-- 							data-slide-to="1" class=""></li> -->
<!--                                 <li data-target="#c-slide" -->
<!-- 							data-slide-to="2" class=""></li> -->
<!--                             </ol> -->
                            <div class="carousel-inner">
                                <div class="item text-center next left" >
                                    <h3 style="color:#fff;font-size:25px;font-family;arial;">Welcome !! Admin4U Portal</h3>
                                    <small class="text-muted"><p class="dashboard-text">Get Latest News Updates</p></small>
                                </div>
                                <div class="item text-center">
                                    <p class="dashboard-text">Good Morning</p>
                                    <small class="text-muted"></small>
                                </div>
                                <div
							class="item text-center active left">
                                    <p class="dashboard-text">Site Undermaintenance</p>
                                    <small class="text-muted"></small>
                                </div>
                            </div>
                            <a class="left carousel-control"
						href="#c-slide" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right carousel-control"
						href="#c-slide" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
			</div>
			 <div class="col-md-5 right-box">
			 	<p class="right-box-head">Reference Document</p>
			 	<ul class="list-inline element">
			 		<li>
			 			<div class="form-group">
			 			<select class="form-control ref-dropdown ref-select" id="selectFile" onchange="setFilePath()">
			 			<option value= "">Select Option</option>
			 			<c:forEach var="policy" items="${dashboard.policyDocs}">
						    
						    <option value="${policy.path}">"${policy.docName}</option>
						 
						</c:forEach>
						 </select>
						</div>
			 		</li>
			 		<li>
			 			<a id="downloadFile"><span><img
								src="assets/images/download-icon.png" data-toggle="tooltip"
								data-placement="left" title="Download" id="downloadImg"></span></a>
			 		</li>
			 		<li>
			 			<a target="_blank" id="viewFile"><span><img
								src="assets/images/view-icon.png" data-toggle="tooltip"
								data-placement="left" title="View Document" id="viewImg"></span></a>
			 		</li>
			 	</ul>
			 </div>
                    </section>
            </div>
            
            
            
            
        <div class="col-md-12 grid-container">    
       <div class="row states-info">
       <a href="user/tour_travel.jsp" class="atext">
            <div class="col-md-3">
                <div class="panel">
                    <div class="imgbg">
                    <div class="col-sm-4 grid-icon">
                    	<img src="assets/images/tour.png" class="grid-img">
                    </div>
                    <div class="col-sm-7 grid-text">
                    	<h2>Tour Travel</h2>
								<br>
                    	<p>Know More</p>
                    </div>
                    </div>
                </div>
            </div>
        </a>
            
            
        <a href="user/hotel_guesthouse.jsp" class="atext">
            <div class="col-md-3" id="">
                <div class="panel">
                    <div class="imgbg">
                    <div class="col-sm-4 grid-icon2">
                    	<img src="assets/images/hotel-guest-house.png"
										class="grid-img2">
                    </div>
                    <div class="col-sm-7 grid-text2">
                    	<h2>Stay4u</h2>
								<br>
                    	<p>Know More</p>
                    </div>
                    </div>
                </div>
            </div>
        </a>
            
        <a href="user/repair_maintenance.jsp" class="atext">
            <div class="col-md-3" id="">
                <div class="panel">
                    <div class="imgbg">
                    <div class="col-sm-4 grid-icon2">
                    	<img src="assets/images/repair-maintainance.png"
										class="grid-img3">
                    </div>
                    <div class="col-sm-7 grid-text2">
                    	<h2>Repair Maintainance</h2>
								<br>
                    	<p>Know More</p>
                    </div>
                    </div>
                </div>
            </div>
        </a>
            
            
        <a href="user/house_keeping.jsp" class="atext">
             <div class="col-md-3" id="">
                <div class="panel">
                    <div class="imgbg">
                    <div class="col-sm-4 grid-icon2">
                    	<img src="assets/images/house-keeping.png"
										class="grid-img3">
                    </div>
                    <div class="col-sm-7 grid-text2">
                    	<h2>HouseKeeping</h2>
								<br>
                    	<p>Know More</p>
                    </div>
                    </div>
                </div>
            </div>
        </a>
            
      
            
            
<!--            =======second grid starts here========-->
       <a href="" onclick="return false;" class="atext">
            <div class="col-md-3">
                <div class="panel">
                    <div class="imgbg">
                    <div class="col-sm-4 grid-icon">
                    	<img src="assets/images/commingsoon.png" class="grid-img3">
                    </div>
                    <div class="col-sm-7 grid-text">
                    	<h2>PCM</h2>
								<br>
                    	<p>Know More</p>
                    </div>
                    </div>
                </div>
            </div>
            </a>
            
            
             <a href="user/vendor.jsp" onclick="return false;" class="atext">
	            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
							<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Vendor Management</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
            </a>
            
            
            <a href="" onclick="return false;" class="atext">
            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Utility Bill Management</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
            </a>
            
            <a href="" onclick="return false;" class="atext">
            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Dispatch Management</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
             </a>
            
     
<!--            ========end second grid row-->
            
            
<!--            =======Third grid starts here========-->
		<a href="" onclick="return false;" class="atext">
            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Meeting Room</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
          </a>
            
            
            <a href="user/electrical_services.jsp" onclick="return false;" class="atext">
            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Electrical Services</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
             </a>
            
            <a href="" onclick="return false;" class="atext">
            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Logistics Services</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
            </a>
            <a href="" onclick="return false;" class="atext">
            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Budget Management</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
             </a>
            
        
<!--            ========end third grid row-->
            
<!--            =======Fourth grid starts here========-->
  			<a href="" onclick="return false;" class="atext">
            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Real Estate Management</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
            </a>
            <a href="" onclick="return false;" class="atext">
	           <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Warehouse Management</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
            </a>
            <a href="" onclick="return false;" class="atext">
	            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Library Management</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
            </a>
            <a href="" onclick="return false;" class="atext">
	            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
	                    	<img src="assets/images/commingsoon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Stock And Inventory</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
            </a>
        </div>      
 
</div>


           
        
<!--            ========end Fifth grid row-->

        <!--body wrapper end-->

        <!--footer section start-->

        <footer class="sticky-footer foot">
            2014 &copy; Admin4U by Turningcloud
        </footer>

        <!--footer section end-->
    <!-- main content end-->

  
        
    </jsp:body>
</t:layout>
