<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:admin-layout>

	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
    
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    
    <!--dynamic table-->
    <script type="text/javascript" language="javascript"src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"	src="assets/js/data-tables/DT_bootstrap.js"></script>
<script		src="assets/js/dynamic_table_init.js"></script>
    
    
    
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
 
    <script src="assets/js/customJS/repair_validation.js"></script>
        <script src="assets/js/customJS/itemRequistion.js"></script>
    
<script src="assets/js/customJS/utility.js"></script>

<script>

$(document).ready(function(){
});
</script>
    </jsp:attribute>
    <jsp:body>
            <!-- Tat Service Start -->

             <div class="wrapper">
           <!--  Body contents goes here -->
<section class="panel">
<header class="panel-heading custom-tab dark-tab">Service Turn Around Time
</header>
    
<form action="" method="post" id="tatServiceManagement" novalidate= "novalidate"  class="cmxform form-horizontal adminex-form" style="padding:30px;">
 
 <div class="row"> 
 <div class="col-sm-8 col-md-4">
      <div class="form-group"> 
        <div class="col-sm-5">
                <label>Turn Around Time</label>
          </div>
           <div class="col-sm-7">
            <select class="form-control" id="tatDuration" name="tatDuration">
               <option value="">select...</option>
              </select>            
                </div>
           </div>
</div>
 <div class="col-sm-8 col-md-4">
      <div class="form-group"> 
        <div class="col-sm-5">
                <label>Priority</label>
          </div>
           <div class="col-sm-7">
                      <input type="text"  readonly="" class="form-control"name="tatServices"  id="tatServices"
                                                placeholder="Priority">
                </div>
           </div>
</div> 
<div class="col-sm-8 col-md-4">
      <div class="form-group"> 
        <div class="col-sm-5">
                <label>Service</label>
          </div>
           <div class="col-sm-7">
            <select class="form-control" id="tatService" name="role">
               <option value="">select...</option>
              </select>            
                </div>
           </div>
</div>
</div>
<div class="row">
<div class="col-sm-12">
     <div class="form-group">
        <table class="display table table-bordered mybody">
        <thead class ="cf">
            <tr class="mytd">
                <th>Escalation level</th>
                <th>Level 1</th>
                <th>Level 2</th>
                <th>Level 3</th>
                <th>Level 4</th>
            </tr>

        </thead>
        <tbody class="cb">
            <tr class="mytd">
                <td>Select TAT</td>
                <td><select class="form-control select2"><option>Choose TAT</option></select></td>
                <td><select class="form-control select2"><option>Choose TAT</option></select></td>
                <td><select class="form-control select2"><option>Choose TAT</option></select></td>
                <td><select class="form-control select2"><option>Choose TAT</option></select></td>
            </tr>
        </tbody>
        
    </table>
    </div>
</div>
    
</div>
<div class="row">
<div class="col-sm-8 col-md-4"></div>
<div class="col-sm-8 col-md-4">
      <div class="form-group"> 
            <button type="button" class="btn btn-info" onclick="" ><i class="fa fa-hand-o-right"></i>Done</button>          
           </div>
</div>
<div class="col-sm-8 col-md-4"></div>
</div>

                           
                           
<!-- =========Modal code=========== -->




<!-- ==========End Modal==================== -->


          
          
          
  </form>
</section>




        </div>
        <!--body wrapper end-->
            <!--Tat End Service -->
    </jsp:body>
</t:admin-layout>