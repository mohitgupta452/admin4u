<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:spoc>
	<jsp:attribute name="header">

 <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datepicker/css/bootstrap-datepicker2.css">
 
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-timepicker/css/timepicker.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-colorpicker/css/colorpicker.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">
  

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
      <link rel="stylesheet" type="text/css"
			href="assets/css/bootstrap-fileupload.min.css" />
			
<link rel="stylesheet" type="text/css" href="assets/css/custom2.css" />
<style>
.datepicker {
	z-index: 9999 !important;
}

#requestDataModal .modal-dialog {
	width: 60%;
	margin: 10px auto !important;
}

#requestDataModal .modal-body {
	padding: 0px !important;
	line-height: 16px !important;
	font-size: 12px !important;
	margin-left: 50px;
	margin-top: 20px;
	color: #000000 !important;
}

.admin_modal .modal-dialog {
	width: 60%;
	margin: 10px auto !important;
}

.admin_modal .modal-body {
	padding: 0px !important;
	line-height: 16px !important;
	font-size: 12px !important;
	/* margin-left: 50px; */
	margin-top: 00px;
	color: #000000 !important;
}

.form-control {
	font-size: 12px;
	padding: 10px;
}
/* modal custom css starts here */
.modal-header {
	background: #263238;
	border-bottom: 6px solid #78909C !important;
}

.panel-heading {
	border-bottom: 1px solid rgba(0, 0, 0, 0.1);
	text-align: left !important;
}

.sticky-footer {
	margin-top: 0px !important;
}

.close_btn {
	background: transparent;
	color: #fff;
}

.modal-header .close {
	background-color: #ECEFF1 !important;
	padding-left: 5px !important;
	padding-right: 5px !important;
	color: #263238 !important;
	border-radius: 33px !important;
}

.col-label {
	display: inline-block;
	margin-bottom: 5px;
	font-weight: 700;
	font-size: 13px;
	color: #E64A19;
	font-family: tahoma;
}

#requestDataModal .modal-body {
	margin-left: 20px !important;
	margin-right: 20px !important;
	border: 1px solid #CFD8DC !important;
	background: #F5F5F5;
}

.tbl-heading {
	text-transform: uppercase !important;
	font-size: 20px !important;
	color: #37474F !important;
	font-family: -webkit-pictograph !important;
}

.tab-btns {
	border-radius: 0px;
	font-weight: 600;
	font-family: tahoma;
}

.adv-table table.display thead th {
	white-space: nowrap;
	vertical-align: middle;
	padding-left: 14px;
	padding-right: 14px;
	background-color: #455A64;
	color: #fff;
}

.panel {
	-webkit-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	background: #fff !important;
	border-top: 4px solid #546E7A;
}
</style>
 

    </jsp:attribute>

	<jsp:attribute name="extrajs">
    
    <script type="text/javascript"
			src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker2.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/js/pickers-init.js"></script>
<script src="assets/js/pickers-init2.js"></script>
    
   <script src="assets/js/customJS/spoc.js"></script>
    <script src="assets/js/customJS/utility.js"></script>
      <script src="assets/js/customJS/space_management.js"></script>
    
    <!--dynamic table-->
<script type="text/javascript" language="javascript"
			src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"
			src="assets/js/data-tables/DT_bootstrap.js"></script>
<script src="assets/js/tourandtravel_dtable_init/spoc_dtable_init.js"></script>
<!-- datepicker -->
<script type="text/javascript"
			src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker2.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript"
			src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<script src="assets/js/pickers-init.js"></script>
<script src="assets/js/pickers-init2.js"></script>

<script>
$(".opendialog").click(function () {
  $('#spocCloseMsg').val(" ");
    $('#actionid').val($(this).data('id'));
    $('#spoc_modal_reject').modal('show');
});


$(".guestHouseDD").change(function(){
	var actionId=$('#modalActionId').val();
	
   if(this.value=="Hotel"){
    /* document.getElementById('hotelBookingShow').style.display = 'block';
    document.getElementById('ghNameShow').style.display = 'none';
    document.getElementById('checkAction').style.display ='none';
    document.getElementById('ghInventoryShow').style.display ='none';
    document.getElementById('ghBookedHistory').style.display='none'; */

    
	$('#hotelBookingShow'+actionId).show();
	$('#ghNameShow'+actionId).hide();
	$('#checkAction'+actionId).hide();
	$('#ghInventoryShow'+actionId).hide();
	$('#ghBookedHistory'+actionId).hide();
	
	
	
	$('#hotelSubmitButton'+actionId).show();
    
    }
    else{ 
   /*  document.getElementById('hotelBookingShow').style.display = 'none';
    document.getElementById('ghNameShow').style.display = 'block';
    document.getElementById('checkAction').style.display ='block';
    document.getElementById('ghInventoryShow').style.display ='none';
    document.getElementById('ghBookedHistory').style.display='block';

 */
 $('#hotelBookingShow'+actionId).hide();
	$('#ghNameShow'+actionId).show();
	$('#checkAction'+actionId).show();
	$('#ghInventoryShow'+actionId).hide();
	$('#ghBookedHistory'+actionId).show();
	
 
	$('#hotelSubmitButton'+actionId).hide();
    
    }
});

$(".ghAvailOnly").click(function(){
	var actionId=$('#modalActionId').val();
	
	$('#ghInventoryShow'+actionId).show();
	$('#ghBookedHistory'+actionId).hide();
});
$(".ghShowHistory").click(function(){
var actionId=$('#modalActionId').val();
	
	$('#ghInventoryShow'+actionId).hide();
	$('#ghBookedHistory'+actionId).show();});


$(".bookTicketButton").click(function () {
	$('.singleOccupancyTable').html("");
	$('.doubleOccupancyTable').html("");
	$('.tripleOccupancyTable').html("");
	
	$('.singleOccupancyTableHistory').html("");
	$('.doubleOccupancyTableHistory').html("");
	$('.tripleOccupancyTableHistory').html("");
	
	var modalId=$(this).data('id');
	console.log("modal reqId"+modalId);
	console.log("val"+$(this).val())
	$('#modalRequestId').val($(this).val());
	$('#modalActionId').val(modalId);
	var requestId=$('#modalRequestId').val();
	
	var actionId=modalId;
	console.log("aId"+actionId);
	console.log("MId"+modalId);
	
	

//code to get checkin checkout time
			var url="GuestHouseInventoryServlet";
		
			var action="REQUESTDETAILS";
			var jsonData={
					requestId:requestId,
					action:action,
			}
		      console.log("called");
			
			
			$.ajax({
			      url: url,
			      type: 'POST',
			      data: jsonData,
			      dataType: "json",
			      ContentType: "application/json",
			      success: function(result){
				      console.log("response7"+actionId);
			    	  $("#checkInDateTime"+actionId).val(result['checkInTime']);
			    	  $("#checkOutDateTime"+actionId).val(result['checkOutTime']);
			    	  $("#location"+actionId).val(result['location']);
			    	  
			    	  			    	  
			        },
			        error: function (data, textStatus, $XHR) {
		              //	showAlertMessage("Failure","you could not perform this operation");
		            	},
			});
				

//end code


	
	
   // $('#actionid').val($(this).data('id'));
    $('#spoc_modal'+modalId).modal('show');


    
});

//end**********
</script>
<script type="text/javascript">
  
  // var cabArr = ['Indica/Indigo','Dzire', 'Dzire/Etios','Innova','Toyota','Corolla/Sunny','Camry/Honda','Accord','Camry','fortuner','Audi','BMW'];
  // var cabOpt;
  // for (var i = 0; i < cabArr.length; i++) {
  //    cabOpt+=('<option value="'+cabArr[i]+'" selected>'+cabArr[i]+'</option>');
  // };
  // $('#cabType').html(cabOpt);
</script>

<script>
$(".opendialog-allocate").click(function () {
    $('#spaceActionId').val($(this).data('id'));
    $('#spaceAction').val("SPOC_ALLOCATE");
    
    $('#spoc_modal_allocate_seat').modal('show');


    var actionId=$(this).data('id');
  var action="getEmployeeUpn";
  var url="SMSpaceAllocationServlet";
  var jsonData={
      actionId:actionId,
      action:action,
  }
  //$('#seatDetailTableTbody').html("");
    $.ajax({
        url: url,
        type: 'POST',
        data: jsonData,
        dataType: "json",
        ContentType: "application/json",
        success: function(result){
             console.log(result['employeeUPN']);
             
                
      $('#allocEmp').html('<option value="'+result['employeeUPN']+'" selected>'+result['employeeName']+'</option>')
              
          }}); 


    
});
</script>

    </jsp:attribute>
	<jsp:body>
            
            <input type="hidden" id="modalRequestId"></input>
                        <input type="hidden" id="modalActionId"></input>
            
         <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Spoc

        </header> 
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
							id="spoc_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Request Id/Workorder</th>
            <th class="hidden-phone">Request By</th>
            <th class="hidden-phone">Request Type</th>
            <th class="hidden-phone ">Request Status</th>
            <th>Request For</th>

            <th>Close</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        
        <c:forEach items="${spoc.spocDataList}" var="request1"> 
            <tr
										class="<c:if test="${request1['isRead']==false}"> <c:out value="gradeX"/></c:if> <c:if test="${request1['isRead']==true}"> <c:out value="seen"/></c:if>"
										id="rid<c:out value="${request1['actionId']}"></c:out>">
            <td><a class="showRequestDataModal" href="javascript:;"
											onclick="callShowRequestData(<c:out value="${request1['requestID']}">
            </c:out>,'${request1['service']}')">
               <c:choose>
    <c:when test="${not empty request1['workorder']}">
        <c:out value="${request1['workorder']}"></c:out>
    </c:when>    
    <c:otherwise>
    <c:out value="${request1['requestID']}"></c:out>
    </c:otherwise>
       </c:choose>
            </a></td>
            <td class="hidden-phone"><c:out
												value="${request1['requester']}" /></td>
            <td class="center hidden-phone"><c:out
												value="${request1['requestType']}" /></td>
            <td class="center hidden-phone">
  <c:choose>
    <c:when test="${request1['requestStatus'] =='REOPEN'}">
               <a class="showResponseDataModal" href="javascript:;"
														onclick="callShowReopenMsg('<c:out value="${request1['requestID']}"></c:out>')">
                   <c:out value="${request1['status']}" />
													</a>
       </c:when>   
       <c:otherwise>
													<c:out value="${request1['status']}" />
												</c:otherwise>
</c:choose>     
           
            <c:if test="${not empty request1['priority']}">/<c:out
													value="${request1['priority']}" />
											</c:if>
            </td>
<td><c:out value="${request1['ServiceDisplay']}" /></td>
            <td>
<button
												class="btn btn-warning opendialog <c:if test="${request1['isComplete']=='true' || request1['status'] !='PENDING'}"> <c:out value="disabled "/></c:if>"
												type="button" data-toggle="modal"
												data-id="${request1['actionId']}">
<i class="fa fa-hand-o-right"></i> Close</button>
										</td>
            <td>

<c:if test="${admin.approvalProcessList.contains(request1['service'])}">          
<button
													class="btn btn-info bookTicketButton <c:if test="${request1['isComplete']=='true'|| request1['status'] !='PENDING'}"> <c:out value="disabled "/></c:if>"
													type="button" data-toggle="modal"
													data-id="${request1['actionId']}"
													value="${request1['requestID']}"
													onclick="callAutoLocationAndPickup(${request1['requestID']})">
                 <i class="fa fa-hand-o-right"></i> Book Ticket</button>
</c:if>
<c:if
												test="${admin.nonApprovalProcessList.contains(request1['service'])}">
<button
													class="btn btn-info <c:if test="${request1['isComplete']=='true'}"> <c:out value="disabled "/></c:if>"
													type="button" onclick="getExtendComments(${request1['requestID']},${request1['actionId']})"
													data-id="${request1['actionId']}">EXTEND</button>
</c:if>  
<!-- start  code for space management-->
 <c:if test="${request1['service']=='SPACEMANAGEMENT'}">
 <button
													class="btn btn-info opendialog-allocate <c:if test="${request1['isComplete']==true}"> <c:out value="disabled "/></c:if>"
													type="button" data-toggle="modal" value="ALLOCATE"
													data-id="${request1['actionId']}">Allocate</button>
 </c:if>
 <!-- end spaceManagement code --><!-- start of modal -->
 <!-- Modal -->
  <div class="modal fade admin_modal"
												id="spoc_modal${request1['actionId']}" role="dialog">
    <div class="modal-dialog modal-lg"
													style="width: 75%; margin-top: 5% !important;">
    
      <!-- Modal content-->
      <form enctype="multipart/form-data" id="spoc_response_form"
														action="SpocServlet" method="post">
      <div class="modal-content"
															style="height: 1000px; min-height: 1000px; overflow: auto;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Details</h4>
        </div>
        <div class="modal-body">
    
  <c:if
																	test="${ request1['service'].contains('HOTEL_BOOKING') || request1['service'].contains('GUEST_HOUSE_BOOKING')}">
     <div class="requestDataSPOC">
     </div>
  </c:if>
        <div class="panel-body">
        	<div class="adv-table">
            	<div id="hidden-table-info_wrapper"
																			class="dataTables_wrapper form-inline" role="grid">
                	<!-- =====Passenger Confirmation Details Start==== -->
                    	<div class="table">                          
<c:if test="${request1['serviceList'].contains('TRAINTICKET') ||request1['serviceList'].contains('AIRTICKET') ||  request1['serviceList'].contains('HOTEL') || request1['serviceList'].contains('CAB') ||request1['serviceList'].contains('GUEST_HOUSE_BOOKING')||request1['serviceList'].contains('HOTEL_BOOKING')|| request1['serviceList'].contains('CABDISPOSAL') ||request1['serviceList'].contains('CABAIRPORT')||request1['serviceList'].contains('CABDROP')||request1['serviceList'].contains('CABPICK')}">  
      <%@include file="services/services_details.jsp"%>  
</c:if>

<!-- ends -->
      

      <c:if
																					test="${request1['serviceList'].contains('TRAINTICKET') ||request1['serviceList'].contains('AIRTICKET') ||request1['serviceList'].contains('CAB') || request1['serviceList'].contains('CABDISPOSAL') ||request1['serviceList'].contains('CABAIRPORT')||request1['serviceList'].contains('CABDROP')||request1['serviceList'].contains('CABPICK') }">
                            <div class="row">
                                                    <div
																							class="col-md-3 inp-box2"
																							style="margin-left: 50px; padding-bottom: 38px;">
																							<label for="t_type" class="lbl">upload Document</label>

                                                     <input type="file" name="upload[]" class="admin-input"
																								multiple="multiple" />

                                                  <input type="hidden"
																								value="${request1['actionId']}" name="actionId"
																								id="req${request1['actionId']}">
																						</div>
        </c:if>
        <div class="row">
          <div class="col-md-5">
            <label>Comments</label>
                  <textarea rows="2" cols="50" id="spoc_comment${request1['actionId']}"name="comment"></textarea>
            </div>
        </div>
        
     
      
      	<div class="row">                              
        	<center>
          <button type="submit" class="btn btn-primary" name="action" value="SPOC_SERVE">Submit</button></center>
        </div>


     </div>
   </div>
 </div>
</div>   
      </div>
      </div>
              <!-- Pan India  -->
              <!-- Data Collapse Starts-->

          <div class="panel panel-success pannel-style">
                       <!--  <div class="panel-heading heading-style">
                            <h3 class="panel-title">PAN India CAB Information Details</h3>
                        </div> -->
                      <div class="panel-body" style="margin-left: 4%;">
                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                          <strong><h3 class="tbl-heading">PAN India CAB Information</h3></strong>
                                        </center>
                            <div class="well well-style">
                                <address>
                                     <div class="adv-table">
                <section id="flip-scroll">
         <table class="display table table-bordered mybody"
                                                height="10px">
        <thead class="cf cf-style">
        <tr class="mytd">
            <th>Designation</th>
            <td>Executive-Sr. Executive</td>
                                                    <td> Assistant Manager</td>
                                                    <td> Deputy Manager</td> <td> Manager-Sr.Manager</td>
                                                    <td> AGM-DGM</td>
                                                    <td> GM-Sr. GM</td>
                                                    <td> AED</td>
                                                    <td> DED</td>
                                                    <td> ED-Sr.ED</td>
                                                    <td>  CEO-Group CEO</td>
                                                    <td> DMD-MD</td>
                                                    <td>  External</td>
                                                    <td>  External</td>
                                                    <td> External</td>
        </tr>
        <tr class="mytd">
          <th>Primary Entitlement</th>
           <td>Indica/Indigo</td>
                                                    <td> Indica/Indigo</td>
                                                    <td> Dzire</td>
                                                    <td> Dzire</td>
                                                    <td> Dzire/Etios</td>
                                                    <td> Innova</td>
                                                    <td>  Innova</td>
                                                    <td>  Innova</td>
                                                    <td>  Toyota Corolla/Sunny</td>
                                                    <td>  Camry/Honda Accord</td>
                                                    <td>  Camry</td>
                                                    <td> fortuner</td>
                                                    <td>  Audi</td>
                                                    <td>  BMW</td>
        </tr>
        
        </thead>
        <tbody class="cb cb-style">
        </tbody>
        </table>
        </section>
        </div>
                                </address>
                            </div>
                        </div>
                       
                    </div>
                    
                   
                </div>
          
       </div>







            
          <!-- Data Collapse End -->
              <!-- Close Pan Indai -->

      </form>
    </div>
    
  </div>
  
</td>
 </tr>
         </c:forEach>
                   </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div> 
        
        
        
          <!-- Space management Modal -->
  <div class="modal fade" id="spoc_modal_allocate_seat" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assign</h4>
        </div>
        <div class="modal-body">
        
            <div id="space_spoc">
            
                    <div class="row">
                      <div class="col-sm-8 col-md-4">
                          <div class="form-group"> 
                              <div class="col-sm-12">
                                  <label><b>Seat Allocation Information</b></label>
                              </div>
                          </div>
                      </div>
                    </div>
                </br>
                <form action="SMSpaceAllocationServlet" method="post"
								id="allocateSpaceForm">
                <input type="hidden" id="spaceActionId"
									name="spaceActionId" />
                <input type="hidden" id="spaceAction" name="spaceAction" />
                
                              <div class="row">
                                                             <div
										class="col-sm-8 col-md-4">
                                                                <div
											class="form-group"> 
                                                                    <div
												class="col-sm-5">
                                                                        <label>Location </label>
                                                                    </div>
                                                                    <div
												class="col-sm-7">
                                                                      <input
													type="text" readonly="" value="${userSession.locationName}"
													class="form-control" id="slocation">
                                                                    </div>
                                                                </div>
                                                            </div>

                               <div class="col-sm-8 col-md-4">
                                  <div class="form-group"> 
                                      <div class="col-sm-5">
                                          <label>Building-Floor</label>
                                      </div>
                                      <div class="col-sm-7">
                                                                       <select
													class="form-control" id="buildingFloor"
													name="buildingFloor">
                                                                         <option
														value="">select Building</option>
                                                                        <c:forEach
														items="#{spaceManagement.buildingFloor}" var="entry">
                                                                        <option
															value="${entry.key}"><c:out
																value="${entry.value}"></c:out></option>
                                                                        
                                                                        </c:forEach>
                                                                        </select>
                                      </div>
                                  </div>
                              </div>
                               <div class="col-sm-8 col-md-4">
                                  <div class="form-group"> 
                                   <div class="col-sm-5">
                                          <label>Space Station Category</label>
                                      </div>
                                      <div class="col-sm-7">
                                         <select class="form-control"
													id="spaceStationCat" name="seatType">
                                                                         <option
														value="">select Category</option>
                                                                          <c:forEach
														items="#{spaceManagement.spaceCategoryList }"
														var="spaceCategory">
                                                        <option
															value="${spaceCategory }"><c:out
																value="${spaceCategory }"></c:out></option>
                                                         </c:forEach>
                                                                        </select>
                                      </div>
                                  </div>
                              </div>
                        


                              </div>
                <br>


            <!-- ==================start Table =============== -->
                
                 <div class="wrapper">
        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
            <header class="panel-heading">
                Responsive table
                        <span class="tools pull-right">
                            <!-- <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a> -->
                         </span>
            </header>
            <div class="panel-body">
                <section id="unseen">
                    <table
													class="table table-bordered table-striped table-condensed"
													id="seatDetailTable">
                        <thead>
                        <tr>
                            <th>S.NO</th>
                            <th>Building</th>
                            <th>Seat Type</th>
                            <th class="numeric">Total</th>
                            <th class="numeric"> Assigned</th>
                            <th class="numeric">Available</th>
                            <th class="">Action</th>
                        </tr>
                        </thead>
                        <tbody id="seatDetailTableTbody">
                       
                        
                       
                        </tbody>
                    </table>
                </section>
            </div>
        </section>
      
     
        </div>
        </div>
        </div>
        <div class="row">
                         <div class="col-sm-8 col-md-4">
                            <div class="form-group"> 
                                <div class="col-sm-12">
                                    <label>Allocate to Employee </label>
                                </div>
                                <!-- <div class="col-sm-7">
                                    <select class="form-control" id="allocEmp">
                                     <option value="">select Employee</option>
                                      <option value="12345">Manoj Singh (EmpID - 12345)</option>
                                    </select>
                                </div> -->
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-4">
                            <div class="form-group"> 

                                <div class="col-sm-12">
                                    <select class="form-control"
													id="allocEmp" name="employeeUPN">
                                     <option value="">select Employee</option>
                                      <option
														value="peter@dalmiabharat.com">Manoj Singh (EmpID - 12345)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="col-sm-8 col-md-4">
                            <div class="form-group"> 
                                <div class="col-sm-5">
                                    <label>Available Seat</label>
                                </div>
                                <div class="col-sm-7">
                                   <select class="form-control"
													id="availSeat" name="seatRoom">
                                     <option value="">Select Seat</option>
                                      
                                    </select>
                                </div>
                            </div>
                        </div>
                         
            


                        </div>

            <!-- ======================End Table ==================== -->
           


                
              </br> 

           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center>
												<button class="btn btn-info" name="action" value="ALLOCATE">
													<i class="fa fa-hand-o-right"></i> Submit</button>
											</center>
                        </div>
                    </div>
            </div>
            
</form>
          
          
    </div>
        
             </div>
        <div class="modal-footer">
          <center>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Close</button>
						</center>
        </div>
      </div>
      
    </div>
  </div>
  


<!-- end of space management modal -->
        
        
        
        
        
         <!-- Modal -->
  <div class="modal fade" id="spoc_modal_reject" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reject Comment</h4>
        </div>
         <form>
        <div class="modal-body">
        
        <div class="form-group lbl">
        	<input type="hidden" name="actionId" id="actionid"
									class="admin-input" />
		    <label for="comment">Comments:</label>
		    <textarea rows="2" cols="30" class="admin-input form-control"

									id="spocCloseMsg" name="responseMsg" style="resize:none;" maxlength="200"></textarea>
<%-- =======
									id="spocCloseMsg" name="responseMsg" style="resize: none;"></textarea>
>>>>>>> 69a2fa71f9592783c8dfe066716480b6b35d895a --%>
		</div>
       


        </div>
        <div class="modal-footer">
          <center>
								<button type="button" class="btn btn-primary"
									data-dismiss="modal" onclick="callSpocServlet('SPOC_CLOSE')">Submit</button>
							</center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>
  


<!-- end of modal -->
  
  <!-- =======================================start show request data  modal===================== -->
    
    
    <!-- start of modal -->
                        
 <!-- Request Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body" style="overflow: auto;">
             </div>
        <br>
        <div class="modal-footer sticky-footer">
          <center>
								<button type="button"
									class="btn btn-default close_btn col-sm-4 col-sm-offset-4"
									data-dismiss="modal">close</button>
							</center>
        </div>
      </div>
      </form>
    </div>
  </div>
                            
<!-- Request msg End  -->


<!--Amend msg  Modal -->
  <div class="modal fade" id="amendResponseMsgModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">  
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Amend Comment</h4>
        </div>
        <div class="modal-body">
        
        
             </div>
        <br>
        <div class="modal-footer"
							style="margin-top: 20px; position: relative;">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">close</button>
							</center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of Amend msg modal -->

<!--  complete or extend modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="extend_modal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Extend Data</h4>
                   <input type="hidden" name="actionId" id="extendactionId" class="extend admin-extend" />
          		<input type="hidden" name="extendEventService" id="extendEventService" class="extend admin-extend" />
          			
          
        </div>
         <form id="houseKeeping" style="display: none;">
        <div class="modal-body" style="overflow: auto;">
<div class=row>
   <div class="col-sm-8 col-md-8">
                <div class="form-group"> 
    <div class="col-sm-5">
        <label>Extension Date & Time</label>
    </div>
    <div class="col-sm-7">
        <select   id="extendTimeDate" name="extendTimeDate" class="form-control" placeholder="Extend Date & Time">
        
        </select>
													
													
      
    </div>
</div>
            </div>
            </div>  
            <br />    
<div class="row">
   <div class="col-sm-8 col-md-8">
    <div class="form-group"> 
    <div class="col-sm-5">
     <label>Comments: &nbsp;  </label>
										</div>
     <div class="col-sm-7">
    <textarea rows="2" cols="30" class="extend admin-extend" id="extendMsg" name="extendMsg" maxlength="200"></textarea>
</div>
</div>
								</div>
        </div>
        </div>
        <br />
        <div class="modal-footer sticky-footer"
							style="position: relative; margin-top: 0px !important">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal"
									onclick="callSpocExtend('HOUSEKEEPING')">Submit</button>
							</center>
        </div>
        </form>
        
        <!-- repair maintenence extend form start -->
        <form id="repairMaintenance" style="display: none;">
        <div class="modal-body" style="overflow: auto;">
<div class=row>
   <div class="col-sm-8 col-md-12">
                <div class="form-group"> 
    <div class="col-sm-5">
        <label>Comment & Time</label>
    </div>
    <div class="col-sm-7">
        <select  id="extendTimeDate1" name="extendTimeDate" class="form-control" >
        <option value="">Select comment &amp; time...</option>
        </select>
      
    </div>
</div>
            </div>
            </div>  
            <br />    
<%-- <div class="row">
   <div class="col-sm-8 col-md-8">
    <div class="form-group"> 
    <div class="col-sm-5">
     <label>Comments: &nbsp;  </label>
										</div>
     <div class="col-sm-7">
    <textarea rows="2" cols="30" class="extend admin-extend" id="extendMsg" name="extendMsg" maxlength="200"></textarea>
</div>
</div>
								</div>
        </div>
 --%>        </div>
        <br />
        <div class="modal-footer sticky-footer"
							style="position: relative; margin-top: 0px !important">
          <center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal"
									onclick="callSpocExtend('REPAIRMAINTENANCE')">Submit</button>
							</center>
        </div>
        </form>
        
        <!-- repair maintenance extend form end -->
      </div>
      
    </div>
    
  </div>

<!-- complete or extend modal ends -->

  
        
    </jsp:body>
</t:spoc>
