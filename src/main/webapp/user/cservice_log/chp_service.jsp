<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:admin-layout>
	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
    
    
    <script type="text/javascript"
			src="assets/js/jquery.validate.min.js"></script>
	<script src="assets/js/customJS/house_keeping.js"></script>
	      <script src ="assets/js/customJS/utility.js"></script>
	
<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
  
	
</jsp:attribute>
<jsp:attribute name="extrajs">
<script type="text/javascript"  src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="assets/js/dynamic_table_init.js"></script>

    <script src ="assets/js/customJS/house_keeping.js"></script>
    <script src="assets/js/customJS/utility.js"></script>
    <script>
$(".opendialog-amend").click(function () {
	$('#amendMsgHK').val(" ");
    $('#requestId').val($(this).data('id'));
    $('#amend_request_modal').modal('show');
});
</script>
    </jsp:attribute>

	<jsp:body>

        <!-- page heading start-->
        

        <div class="page-heading">
        <!-- Cab/Ticket -->
          <section class="panel">
                        <header
				class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs">
                                
                                <li class="active">
                                    <a href="#house_keeping"
					data-toggle="tab">House Keeping</a>
                                </li>
                               
                                <c:if test="${sessionScope.userid == 'admin' }">
                                <li>
                                    <a href="#admin_travel_requests" data-toggle="tab">ADMIN</a>
                                </li>
                                <li>
                                    <a href="#user_confirmation"
					data-toggle="tab">User Confirmation</a>
                                </li>
                                </c:if>
                                
                            </ul>
                        </header>


<!-- =====Tab Pannel Start Here -->
<div class="panel-body">
    <div class="tab-content">
        
<!--============= Form for Ticket Requests ==================-->

<!--============= End Here Ticket Requests ==================-->


<!-- ==========Form Hotel / Guest House Booking =======  -->
    <div class="tab-pane active" id="house_keeping">
             <!--==========Tab Start Here========-->                    
                              
             <!--==========Row@2 ================-->
             
             <!-- form.... -->
            
            <!-- ==================House Keeping form Starts====================== -->
            <form action="CsServiceHkServlet" method="post"
							id="register-form" novalidate="novalidate"
							class="cmxform form-horizontal adminex-form">
  
   <div class="row">
   
   
   
   
   
             
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <!-- <label>Guest Type<br>&nbsp;</label> -->
                            </div>
                            <div class="col-sm-7">
                             
                            </div>
                        </div>
                    </div>
                    </div>
            <!-- ==================Row 3=============== -->
                <div class="row">
                <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Employee ID<span
												class="labelcolor">*</span></label>
                                </div>
                           <div class="col-sm-7">
                              <input type="text" class="form-control"
												id="employeeID" name="employeeID" onblur="getEmployeeDataById(this)" />
                      </div>
                      </div>
                      </div>
   
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Name(Requestor)<span
												class="labelcolor">*</span></label>
                                </div>
                           <div class="col-sm-7">
                              <input type="text" class="form-control alphabetOnly"
												id="username" name="username" />
                      </div>
                      </div>
                      </div>
                   
                         <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Department<span
												class="labelcolor">*</span></label>
                            </div>
                             <div class="col-sm-7">
    <input type ="text"  value="" class="form-control" id="department" name="department" >
                             
                              </div>
                     </div>
                     </div>
                    
                     
                    </div>
                    <br />
            <!-- ======================End Row @ 3==================== -->
            <!-- ==================Row start @4=============== -->
               
               <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Nautre Of Work<span
												class="labelcolor">*</span></label>
                                </div>
                            <div class="col-sm-7">
                               <select class="form-control select2" id="nature"
												name="nature">
                               <option value="">select...</option>
                                <c:forEach items="${chpServiceBean.natureOfWorkDropDown}" var="natureOfWorkValue">
                                
                                  <option><c:out value="${natureOfWorkValue}"></c:out></option>  
                                  </c:forEach>  
                              </select>
                              </div>
                       </div>
                       </div>
                    
                   <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Problem Description<span
												class="labelcolor">*</span></label>
                          </div>
                           <div class="col-sm-7">
                                <input class="form-control" size="12"
												type="text" name="description" id="description" placeholder="Problem Description"/>
                     </div>
                     </div>
                     </div>
                    
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Work Location<span
												class="labelcolor">*</span></label>
                          </div>
                          
                           <div class="col-sm-7">
                              						  <select class="form-control select2" id="work"
												name="work">
                               <option value="">select...</option>
                                <c:forEach items="${lobn.currentWorklocation}" var="cwl">
                                
                                  <option><c:out value="${cwl}"></c:out></option>  
                                  </c:forEach>  
                              </select>
                        </div>
                        </div>
                        </div>
                        </div>
                
            <!-- ======================End Row @ 4==================== -->
       
       <div class="row">
        <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                    
                                <label>Mobile No<span
												class="labelcolor">*</span></label>
                                </div>
                           <div class="col-sm-7">
                              <input type="text" class="form-control numbersOnly"
												id="mob" name="mob" placeholder="Mobile No" maxlength="10"/>
                    </div>
                    </div>
                    </div>
       
       
       </div>     
          
<center>
<button class="btn btn-info" type="submit" name="action" value="GENERATED" id="submit">
									<i class="fa fa-hand-o-right"></i> Submit</button>
							</center>
  </form>
  
            
            
            <!-- ===================House Keeping Form Ends============================== -->
            
            
                       
        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           USER VIEW
            <span class="tools pull-right">
                
             </span>
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
											id="hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Request ID</th>
            <th>Requester</th>
         <!--   <th class="hidden-phone">Request Type</th>-->
            
            <th class="hidden-phone">Booking For</th>
            <th class="hidden-phone">Request Date</th>
            <th class="hidden-phone">Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="req" items="${chpServiceBean.USERhngDataList}">
        <tr class="gradeX" id="rid${req['requestID'] }">
           <td><a class="showRequestDataModal"href="javascript:;"  onclick="callShowRequestData(<c:out value="${req['requestID']}">
         </c:out>,'<c:out value="${req['service']}" />')"><c:out value="${req['requestID']}"></c:out></a></td>
            <td><c:out value="${req['requester']}"></c:out></td>

            <td class="hidden-phone"><c:out value="${req.requestType}"/></td>
            <td class="center hidden-phone"><c:out value="${req.requestdate}"/></td>
            <td class="center hidden-phone">
            
   <c:choose>
							<c:when test="${req['requestStatus'] =='AMEND'}">
													<a class="showResponseDataModal" href="javascript:;"
														onclick="callShowAmendMsg('<c:out value="${req['requestID']}"></c:out>')"><c:out value="${req['requestStatus']}" /></a>
</c:when>   
<c:when test="${req['requestStatus'] =='CLOSED'}">
				<a class="showResponseDataModal" href="javascript:;"
	onclick="callShowResponseMsg('<c:out value="${req['requestID']}"></c:out>')"><c:out value="${req['requestStatus']}" /></a>
</c:when>   
      <c:otherwise>
	      <c:out value="${req['requestStatus']}" />
      </c:otherwise>
			    
</c:choose>         
                   
            
            
            
            
            </td>
            <td>
<button class="btn btn-info opendialog-amend" type="button" name="action" value="Amend" data-toggle="modal" data-id="${req['requestID']}"> 
	<i class="fa fa-hand-o-right" ></i> Amend</button>
</td>
        </tr>
      </c:forEach>
       
        </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div>
        


             <!--==========Tab End Here==========-->
    </div>

                                                                                  
<!--===========End Hotel / Guest House Request Form====-->    



<!-- =====ADMIN TABLE starts from here===== -->

      <div class="tab-pane"
						id="admin_travel_requests">
                                    <!-- Form for Ticket Requests -->
                                  <div id="preapprove"
							style="width: 100%">

                                    </div>
                                    <br>
        <!-- ==== Dynamic table Start here ===== -->
           

 <!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="admin_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <table>
        <tr>
												<td>
          <label>Message: &nbsp;  </label>
												</td>
												<td style="margin-top: 10px;"><textarea rows="2"
														cols="30">Message area</textarea></td>
          </tr>
</table>

        </div>
        <div class="modal-footer">
          <center>
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Save</button>
										</center>
        </div>
      </div>
      
    </div>
  </div>
  


<!-- end of modal -->

<!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="admin_modal_assign" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
        <div class="form-group">
          <label>Select Spoc:   </label><select class="form-control">
             <option>select....</option> 
             <option>Travel Spoc</option> 
             <option>Transport Spoc</option> 

          </select>
          </div>
        </div>
        <div class="modal-footer">
          <center>
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Save</button>
										</center>
        </div>
      </div>
      
    </div>
  </div>
  


<!-- end of modal -->
      

                    
                             <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Admin
            <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-times"></a>
             </span>
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
											id="admin_hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Entity Location</th>
            <th>Work Order</th>
            <th class="hidden-phone">Employee Id</th>
            <th class="hidden-phone">Department</th>
            <th class="hidden-phone">Booking Type</th>
            <th>Booking For</th>
            <th>Onward Journey</th>

            <th>Status</th>
            <th>Close</th>
            <th>Action</th>
            <th>Assign</th>


        </tr>
        </thead>
        <tbody>
        <tr class="gradeX">
            <td>Hanshalya Delhi</td>
            <td>E/W1</td>
            <td class="hidden-phone">Emp1</td>
            <td class="center hidden-phone">IT department</td>
            <td class="center hidden-phone">train</td>
         <td>Self</td>
            <td>12-feb-2016</td>
            <td>pending</td>
            <td><button class="btn btn-warning " type="button"
															data-toggle="modal" data-target="#admin_modal">
															<i class="fa fa-hand-o-right"></i> Close</button></td>
            <td><button class="btn btn-info " type="button">
															<i class="fa fa-hand-o-right"></i> Book Ticket</button>
</td>
            
            <td><button class="btn btn-info " type="button"
															data-toggle="modal" data-target="#admin_modal_assign">
															<i class="fa fa-hand-o-right"></i> Assign</button>
</td>
        </tr>
                </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div> 
      
</div>


     





<!--  ======ADMIN TABLE ends here  -->

<!-- ==confirmation Requests Tab start Here == -->
<div class="tab-pane" id="user_confirmation">
                                    
        <!-- ==== Dynamic table Start here ===== -->
           
                    <div class="panel " style="border: 1px solid black">
                            <div class="row">
                                 <div class="col-sm-12">
                                    <section class="panel">
                                    <header class="panel-heading">
                                        DataTables fo all booking details
                                       
                                    </header>
                                     <div class="panel-body">
                                        <div class="adv-table">
                                        <div
												id="hidden-table-info_wrapper"
												class="dataTables_wrapper form-inline" role="grid">
                                <!-- =====Passenger Confirmation Details Start==== -->
                                <div class="table">
                                        <div class="row">
                                                  <div class="col-md-4">
															<label for="t_type">Booking Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Train Ticket</div>
                                                  <div class="col-md-4">
															<label for="t_type">Booking for&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Self Booking</div>
                                                  <div class="col-md-4">
															<label for="t_type">Preffered Time Slot&nbsp;&nbsp;</label>09:00 HRS - 11:00 HRS</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
															<label for="w_id">Travel Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>12-Feb-2016</div>
                                                  <div class="col-md-4">
															<label for="t_type">Travel Class&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>AC-III Tier</div>
                                                  <div class="col-md-4">
															<label for="w_id">Travel Purpose&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Offical Only</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
															<label for="w_id">Passenger&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</label>Manoj Singh</div>
                                                  <div class="col-md-4">
															<label for="w_id">Employee ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>EMP/DB/1234</div>
                                                  <div class="col-md-4">
															<label for="w_id">Mobile No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>9643827282</div>
                                         </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                </section>
                            </div>
                            </div>
<!-- Ticket Confirmation Start Here -->
                    <div class="row">
                                <div class="col-sm-12">
                                            <section class="panel">
                                                <header
										class="panel-heading">
                                                   Ticket Confirmation Details
                                                    
                                                </header>
                                                <div class="panel-body">
                                                    <div
											class="adv-table">
                                                        <div
												id="preapprove" style="width: 100%">
                                                              <form
													action="action">
                                                                  <div
														class="table">
                                                                         <div
															class="row">
                                                                                  <div
																class="col-md-4">
																<label for="w_id">E- Ticket Downloads&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="button" value="Download Ticket"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                                                 
                                                                         </div>
                                                                    </div>
                                                                </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>

<!-- Ticket Confirmation End Here -->
<!-- Cab Confirmation Start Here -->

                            <div class="row">
                                <div class="col-sm-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       Cab Confirmation Details
                                        
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                            <div id="preapprove"
												style="width: 100%">
                                                  <form action="action">
                                                      <div class="table">
                                                             <div
															class="row">
                                                                      <div
																class="col-md-4">
																<label for="w_id">Cab Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Innova Diesel</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Cab No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>DL-05 AC-1324</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Cab Provider &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>ABC Provider</div>
                                                             </div>
                                                             <div
															class="row">
                                                                      <div
																class="col-md-4">
																<label for="w_id">Driver Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Abc Name</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Driver No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>963524871</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">PickUp Date/Time</label>13-Feb-2015  16:30:00 Hrs</div>
                                                             </div>
                                                        </div>
                                                    <!-- <center><input type="submit" value="Submit For Approval"></center> -->
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
<!-- Cab Cofirmation End Here -->
<!-- Guest Confirmation Start Here -->
                <div class="row">
                                <div class="col-sm-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       Cab Confirmation Details
                                        
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                            <div id="preapprove"
												style="width: 100%">
                                                  <form action="action">
                                                      <div class="table">
                                                             <div
															class="row">
                                                                      <div
																class="col-md-4">
																<label for="w_id">Hotel/G.House&nbsp;&nbsp;&<noframes></noframes>bsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Abc Hotel ,Delhi</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Contact Person&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>xyz Name</div>
                                                                      <div
																class="col-md-4">
																<label for="w_id">Mobile No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>9157341872</div>
                                                             </div>
                                                        </div>
                                                    <center>
														<input type="submit" value="Download">
													</center>
                                                    </form>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>

<!-- Guest house confirmation End Here -->
                    </div>
    </div>

                <!-- ==confirmation Requests Tab End Here == -->
        
            <!-- Travel Admin Requests Start Here -->
            <div class="tab-pane" id="admin_travel_department">
                                    <!-- Form for Ticket Requests -->
                                  <div id="preapprove"
							style="width: 100%">
                                  <form action="action">
                                  <div class="table">
                                        <div class="row">
                                                  <div class="col-md-4">
                                                  <label for="ext_date">One Way Journey</label>&nbsp;&nbsp;&nbsp;Yes</div>
                                                  <div class="col-md-4">
											<label for="ext_date">Return Journey</label>&nbsp;&nbsp;&nbsp;No</div>
                                                  <div class="col-md-4">
											<label for="ext_date">Food & Beverages (As per Policy)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yes</div>
                                         </div>

                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="t_type">Booking Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Train Ticket</div>
                                                  <div class="col-md-4">
											<label for="t_type">Booking for&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Self Booking</div>
                                                  <div class="col-md-4">
											<label for="t_type">Preffered Time Slot&nbsp;&nbsp;</label>09:00 HRS - 11:00 HRS</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">Travel Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>12-Feb-2016</div>
                                                  <div class="col-md-4">
											<label for="t_type">Travel Class&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>AC-III Tier</div>
                                                  <div class="col-md-4">
											<label for="w_id">Travel Purpose&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Offical Only</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">Passenger&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</label>Manoj Singh</div>
                                                  <div class="col-md-4">
											<label for="w_id">Employee ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>EMP/DB/1234</div>
                                                  <div class="col-md-4">
											<label for="w_id">Mobile No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>9643827282</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">Source&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>XYz Station</div>
                                                  <div class="col-md-4">
											<label for="w_id">Destination&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>XYZ Destination</div>
                                                  <div class="col-md-4">
											<label for="ext_date">Avail Cab</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;<label
												for="ext_date">Avail Hotel/Guest House</label>&nbsp;&nbsp;&nbsp;Yes</div>    
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">PickUp Point&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Xyz Address ,State</div>
                                                  <div class="col-md-4">
											<label for="w_id">End Point&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Xyz Address, State</div>
                                                  <div class="col-md-4">
											<label for="w_id">E-Attachment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>abc.pdf</div>    
                                         </div>
                                          
                                    </div>
                                    <!-- <center><input type="submit" value="Accept"><input type="submit" value="Reject"></center> -->
                                    </form>
                                    </div>
                                    <br>
        <!-- ==== Dynamic table Start here ===== -->
           
                    <div class="panel " style="border: 1px solid black">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                    <header class="panel-heading">
                                       Ticket Booking Details
                                        <!-- <span class="tools pull-right">
                                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                                            <a href="javascript:;" class="fa fa-times"></a>
                                         </span> -->
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                        <!-- lkflf -->
                                        <div id="preapprove"
												style="width: 100%">
                                  <form action="action">
                                  <div class="table">
                                         <div class="row">
                                                  <div class="col-md-4">
																<label for="w_id">Depature Date/Time</label><input
																	type="date" value="Depature Date"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Booking Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="date" value="" style="height: 21px; width: 175px"
																	id="w_id" background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">E-Ticket &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="button" value="Upload E-ticket"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                         </div>
                                         
                                         
                                    </div>
                                    </form>
                                    </div>

                                        </div>
                                    </div>
                                    </section>
                                    </div>
                                    </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                    <header class="panel-heading">
                                       Cab Booking Details
                                       
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                        <div id="preapprove"
												style="width: 100%">
                                  <form action="action">
                                  <div class="table">
                                         <div class="row">
                                                  <div class="col-md-4">
																<label for="w_id">Cab Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vehicle Type"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Cab No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vechile No"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Cab Provider &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vendor Name"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
																<label for="w_id">Driver Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vehicle Type"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Driver No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vechile No"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">PickUp Date/Time</label><input
																	type="date" value="Vendor Name"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                         </div>
                                    </div>
                                    </form>
                                    </div>



                                        </div>
                                    </div>
                                    </section>
                                    </div>
                                    </div>
<!-- Row-2 End Here -->

              </div>

                <!-- ====Ticket Requests End Here -->
            </div>

            <!-- Travel Request End Here -->


    </div>
    
    
     <!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body" style=" overflow :auto;">
             </div>
        <br>
        <div class="modal-footer sticky-footer" style="position:relative;margin-top:0px !important ">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of modal -->
    
    
     <!-- Start Of  Response Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
        
        
        
             </div>
        
        
        <br>
       
        <div class="modal-footer"
						style="margin-top: 150px; position: relative;">
          <center>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Save</button>
						</center>
        </div>
      </div>
      
    </div>
  </div>
 


<!-- End Of Response Status Modal -->
    
  <!-- Amend msg Modal -->
  <div class="modal fade" id="amendResponseMsgModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Amend Message</h4>
        </div>
        <div class="modal-body">
        
        
             </div>
        <br>
        <div class="modal-footer" style="margin-top: 20px;position:relative; ">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of amend msg modal -->  

                        
 <!-- Amend msg inserting Modal starts-->
  <div class="modal fade" id="amend_request_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Amend Message</h4>
        </div>
         <form>
        <div class="modal-body">
       
        <table>
        <tr><td>
        <input type="hidden" name="requestId" id="requestId" class="amend"/>
          <label>Message: &nbsp;  </label></td><td style="margin-top:10px;"><textarea rows="2" cols="30" class="amend" id="amendMsgHK" name="message"></textarea></td>
          </tr>
</table>

        </div>
        <div class="modal-footer">
          <center><button type="button" class="btn btn-default" data-dismiss="modal" onclick="callAmendRequest()">Amend</button></center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>

<!-- amend msg inserting  modal ends -->  
    
    <!-- Start Of  Response Message (CLOSED) Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseMsgModal" role="dialog">
    <div class="modal-dialog">
  
  <!-- <form action="DownloadPDFServlet" method="POST"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Response Message </h4>
        </div>
        <div class="modal-body">
  
         </div>
       
       
        <div class="modal-footer" style="margin-top: 10px; position: relative;">
<center>								
<button class="btn btn-warning " type="submit"  data-dismiss="modal"> <i class="fa fa-hand-o-right"></i> Ok</button>
</center>
        </div>
    
      </div>
    </form>
    </div>
    
  </div>  
    
    
    
    </div>
    </section>
    <!-- Next form Fields E- Attachment  -->
    <!-- ===== Dyanmic Table Start Here ====== -->


    <!-- ===== Dynamic Table End Here ===== -->
    </div>
    <!-- page heading end-->

<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script src="assets/js/customJS/repair_validation.js"></script>



    </jsp:body>
</t:admin-layout>