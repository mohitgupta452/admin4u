<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:layout>

	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css"
			href="assets/js/bootstrap-datepicker/css/bootstrap-datepicker2.css">
 
    
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    <script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker2.js"></script>
    
    <!--dynamic table-->
    <script type="text/javascript" language="javascript"src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.min.js"></script>
    
<script type="text/javascript"	src="assets/js/data-tables/DT_bootstrap.js"></script>
<script		src="assets/js/dynamic_table_init.js"></script>
    
    
    
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
 
    <script src="assets/js/customJS/space_management.js"></script>
<script src="assets/js/customJS/utility.js"></script>
<script src="assets/js/pickers-init2.js"></script>



    </jsp:attribute>
	<jsp:body>
	
	<!--  
<c:if test="${not empty sessionScope.msg}"> 
   <div class="alert alert-success" style="width:100%;height:40px; font-size:20px;text-align:center;">
    ${sessionScope.msg}
    ${ session.removeAttribute("msg")}
    </div>
</c:if> 

-->
      <div class="page-heading">
        <!-- Cab/Ticket -->
          <section class="panel">
                        <header class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs">
                                <!-- <li class="active">
                                    <a href="#ticket_requests" data-toggle="tab">Ticket Booking</a>
                                </li> -->
                               <!--  <li >
                                    <a href="#cab_requests" data-toggle="tab">Cab Request</a>
                                </li> -->
                                <li class="active">
                                    <a href="#repair_maintenance" data-toggle="tab">Space Management</a>
                                </li>
                               <!--  <li>
                                    <a href="#Requests" data-toggle="tab">Requests</a>
                                </li> -->
                                <!-- <li>
                                    <a href="#admin_requests" data-toggle="tab">Booking View</a>
                                </li> -->
                               <!--  <li>
                                    <a href="#admin_travel_requests" data-toggle="tab">Admin</a>
                                </li> -->
                               <!--  <li>
                                    <a href="#user_confirmation" data-toggle="tab">User Confirmation</a>
                                </li> -->
                                
                                <!-- <li>
                                    <a href="#admin_travel_department" data-toggle="tab">Transport Admin</a>
                                </li> -->
                            </ul>
                        </header>

<!-- =====Tab Pannel Start Here -->
<div class="panel-body">
    <div class="tab-content">
        
<!--============= Form for Ticket Requests ==================-->

<!--============= End Here Ticket Requests ==================-->


<!-- ==========Form Hotel / Guest House Booking =======  -->
    <div class="tab-pane active" id="repair_maintenance">
             <!--==========Tab Start Here========-->                    
                              
             <!--==========Row@2 ================-->
             <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-12">
                                <label ><b>Employee Information</b></label>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                             <form action="SpaceManagementServlet" method="post" id="spaceManagementForm" enctype="multipart/form-data">
                
                <div class="row">
                    
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Space Allocation Type <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select  class="form-control allocationCategory" id="allocationCategory" name="allocationType">
                                <option value="">Select...</option>
                                  
                                  <option value="Transfer">Transfer</option>
                                  <option value="NewJoinee">New Joinee</option>
                              </select> 
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee ID <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" placeholder="Emp ID IfAny" class="form-control autoFilled" id="employeeId" name="employeeId">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5" >
                                <label >Location / Area <span class="requiredField"> *</span></label>
                            </div>
                           <div class="col-sm-7">
                              <select  class="form-control" class="Userlocation" id="Userlocation" name="location">
                                   <option value="">Select...</option>
                                <c:forEach items="${spaceManagement.locationDropDown}" var="location">
                                
                                <option value="${location}"><c:out	value="${location}"></c:out></option> 
                                </c:forEach>  
                              </select> 
                            </div>
                        </div>
                    </div>
                    
                   
                </div>
                <br>
            <!-- ==================Row 3=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label> Employee Name <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" placeholder="Emp. Name" class="form-control autoFilled" id="employeeName" name="employeeName">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Date of Joining</label>
                            </div>
                            <div class="col-sm-7">
                             <input  placeholder="Join Date" readonly="" class="form_datetime form-control dp1 autoFilled" id="dateOfJoin" name="dateOfJoining">
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Designation <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" placeholder="Desgination" class="form-control autoFilled" id="desgination" name="designation">
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            <!-- ======================End Row @ 3==================== -->
            <!-- ==================Row start @4=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Company<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                               <input class="form-control autoFilled" type="text" placeholder="company" name="company" id="company" >
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Department<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                <select  class="form-control allocationCategory" id="department" name="department">
                                 <option value="">Select...</option>
                                <c:forEach items="${spaceManagement.departmentDropDown}" var="department">
                                
                                <option value="${department}"><c:out	value="${department}"></c:out></option> 
                                </c:forEach>                                </select>
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Reporting Manager<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input class="form-control autoFilled" type="text" placeholder="Reporting Manager" name="reportingManager" id="reportingManager" >
                            </div>
                        </div>
                    </div>
                </div>

            <!-- ======================End Row @ 4==================== -->
             <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Contact No<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                               <input class="form-control autoFilled" type="text" placeholder="ContactNo" name="contactNumber" id="contactNumber" >
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Email ID<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                                <input class="form-control autoFilled" type="text" placeholder="email Id"  name="emailId" id="emailId">
                            </div>
                    </div>
                    </div>
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Current Seat No</label>
                            </div>
                            <div class="col-sm-7">
                                <input class="form-control autoFilled" type="text" placeholder="Seat No"  name="currentSeatNumber" id="currentSeat">
                            </div>
                    </div>
                    </div>

                    
                   <!--  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Reporting Manager</label>
                            </div>
                            <div class="col-sm-7">
                              <input class="form-control" type="text" placeholder="Work Location" >
                            </div>
                        </div>
                    </div> -->
                </div>
                <br>
                <div class="row">
                     <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                
    
                                                
                                                    <label>Approved Attachment<span class="requiredField"> *</span></label>
                                                    
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new" name="upload" id="upload"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="upload" id="upload" >
                                                                    </span>
                                                <label class="error" for="upload" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                    </div>
                </div>
           <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
                           <center><button class="btn btn-info "  name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            
</form>
          
            <!-- ======================Dynamic table Start here ====== -->
                       
                                                        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           USER VIEW
            <span class="tools pull-right">
                      </span>
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
                        <table class="display table table-bordered "
											id="hidden-table-info">
                <thead class="cf">
        <tr>
            <th>Request ID</th>
            <th>Requester</th>
            
            <th class="hidden-phone">Booking For</th>
            <th class="hidden-phone">Request Date</th>
            <th class="hidden-phone">Status</th>
            <th>Action</th>
           
        
												</tr>
        </thead>
        <tbody>
       <c:forEach  items="${spaceManagement.USERsmDataList}" var="valueMap">

        <tr class="gradeX" id="rid${valueMap['requestID']}">
         <td><a class="showRequestDataModal"href="javascript:;"  onclick="callShowRequestData(<c:out value="${valueMap['requestID']}">
         </c:out>,'<c:out value="${valueMap['service']}"/>')"><c:out value="${valueMap['requestID']}"></c:out></a></td>
         <td  class="hidden-phone"><c:out value="${valueMap['requester']}"></c:out></td>
       
         
        <td  class="center  hidden-phone"><c:out value="${valueMap['requestType']}"></c:out></td>
        <td  class="center  hidden-phone"><c:out value="${valueMap['requestdate']}"></c:out></td>
        <td  class="center  hidden-phone">
        <c:choose>
		<c:when test="${valueMap['requestStatus'] =='COMPLETE'}">
		<a class="showResponseDataModal" href="javascript:;"
		onclick="callShowResponseData('<c:out value="${valueMap['requestID']}"></c:out>','<c:out value="${valueMap['service']}" />')"><c:out
		value="${valueMap['requestStatus']}" /></a>
		</c:when>
		<c:when test="${valueMap['requestStatus'] =='CLOSED'}">
				<a class="showResponseDataModal" href="javascript:;"
	onclick="callShowResponseMsg('<c:out value="${valueMap['requestID']}"></c:out>')"><c:out value="${valueMap['requestStatus']}" /></a>
</c:when>   
        <c:otherwise>
	   <c:out value="${valueMap['requestStatus']}" />
		</c:otherwise>
		</c:choose>
        
        
        </td>

            <td>
<button class="btn btn-info <c:if test="${valueMap['isComplete']=='true' || valueMap['requestStatus']=='CANCELLED' || valueMap['complete']==true }"> <c:out value="disabled"/></c:if>" type="button" name="action" value="CANCEL" onclick="callCancelRequest('${valueMap['requestID']}')"> 
	<i class="fa fa-hand-o-right" ></i> Cancel</button>

</td>
	
	
        </tr>
       </c:forEach>

        
        
                        </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div>
        
        
                               



             <!--==========Tab End Here==========-->
    </div>

                                                                                                      
<!--===========End Hotel / Guest House Request Form====-->    

<!-- ==confirmation Requests Tab start Here == -->


                <!-- ==confirmation Requests Tab End Here == -->
        
            <!-- Travel Admin Requests Start Here -->
           

            <!-- Travel Request End Here -->


    </div>
    <!--<div class="tab-pane" id="contact2">Contact</div>
    </div> -->
    
    
    </div>
    </section>
    <!-- Next form Fields E- Attachment  -->
    <!-- ===== Dyanmic Table Start Here ====== -->


    <!-- ===== Dynamic Table End Here ===== -->
    </div>  
      
      
    <!-- page heading end-->
        
        
        <!-- validation -->
        <!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body">
        
        <div class="panel-body">
                                        <div class="adv-table">
                                        <div
									id="hidden-table-info_wrapper"
									class="dataTables_wrapper form-inline" role="grid">
                                <!-- =====Passenger Confirmation Details Start==== -->
                                <div class="table">
                                <div class="row" id="txtRequestData"></div>
                                       <div class="row">
                                                  <div class="col-md-4">
												<label for="t_type">Booking Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalBookingType"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Booking For&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalBookingFor"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Guest Type&nbsp;&nbsp;</label><span
													id="modalGuestType"></span>
											</div>
                                         </div>
                                        <div class="row">
                                                  <div class="col-md-4">
												<label for="t_type">Employee ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalEmployeeID"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Mobile NO.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalMobileNo"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Checkin Date Time&nbsp;&nbsp;</label><span
													id="modalCheckinDateTime"></span>
											</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
												<label for="w_id">Checkout Date Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalCheckoutDateTime"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="t_type">Hotel Location&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalHotelLocation"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="w_id">Avail Cab&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalAvailCab"></span>
											</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
												<label for="w_id">Pickup Point&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalPickupPoint"></span>
											</div>
                                                  <div class="col-md-4">
												<label for="w_id">Drop Point &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalDropPoint"></span>
											</div>
                                                   <div class="col-md-4">
												<label for="w_id">Cost Centre&nbsp;&nbsp;&nbsp;&nbsp;</label><span
													id="modalCostCentre"></span>
											</div>
                                         
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
												<label for="w_id">Approved Attachment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><span></span>
											</div>
                                                
                                         </div>
                                         
                                    </div>
                                    </div>
                                    </div>
                                    </div>
        
        
        
        </div>
        <div class="modal-footer"  style="margin-top: 260px; position: relative;">
          <center>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">CLOSE</button>
						</center>
        </div>
      </div>
      
    </div></div>
 
  
<!-- end of modal -->

        
  <!-- Start Of  Response Message (CLOSED) Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseMsgModal" role="dialog">
    <div class="modal-dialog">
  
  <!-- <form action="DownloadPDFServlet" method="POST"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Response Message </h4>
        </div>
        <div class="modal-body">
  
         </div>
       
       
        <div class="modal-footer" style="margin-top: 10px; position: relative;">
<center>								
<button class="btn btn-warning " type="submit"  data-dismiss="modal"> <i class="fa fa-hand-o-right"></i> Ok</button>
</center>
        </div>
    
      </div>
    </form>
    </div>
    
  </div>

<!-- End Of Response Status Modal -->
        
        
    </jsp:body>
</t:layout>