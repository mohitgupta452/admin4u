<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:form-layout>

	<jsp:attribute name="header">

<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css"
			rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css"
			rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
    <link href="assets/css/table-responsive.css" rel="stylesheet" />
   
 <style>
#requestDataModal .modal-dialog  
{
width:60%;
margin: 10px auto!important;
}


#requestDataModal .modal-body 
{
    padding : 0px!important;
    line-height : 16px !important;
    font-size : 12px !important;
    margin-left:50px;
    margin-top:20px;
    color:#000000 !important;
}
    
#amend_request_modal .modal-dialog  
{
width:60%;
margin: 10px auto!important;
}

#amend_request_modal .modal-body 
{
    padding : 0px!important;
    line-height : 16px !important;
    font-size : 12px !important;
    margin-left:50px;
    margin-top:20px;
    color:#000000 !important;

}

.form-group label {
	color: #37474F !important;
	font-weight: 600 !important;
	padding-top: 6px !important;
	font-family: inherit;
	font-size: 13px;
}

.t-tabs {
	background: #78909C;
	border-radius: 48px;
	text-align: center;
}

.t-tabs:hover {
	background-color: #37474F !important;
}

.nav-tabs li {
	margin-left: 65px;
}

.nav-tabs li:hover {
	background: gray;
	border-radius: 48px;
	text-align: center;
}

.tab-content {
	background-color: #fff;
	padding: 31px;
	border-top: 6px solid #90A4AE;
	-webkit-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	-moz-box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
	box-shadow: -7px 7px 5px -8px rgba(0, 0, 0, 0.75);
}

.panel-heading .nav {
	margin: 0px !important;
}

.panel-heading .nav>li.active {
	background-color: #ccc !important;
	color: #37474F !important;
}

.panel-heading .nav>li.active>a, .panel-heading .nav>li>a:hover {
	background: transparent !important;
}

.panel-heading .nav>li>a:hover {
	color: #fff !important;
}

.custom-tab.dark-tab {
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
	border-top: 1px solid #ccc;
}

.panel-body {
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
}





</style>	  
    
    
    
    
    </jsp:attribute>
    <jsp:attribute name="extrajs">
    
    <!--dynamic table-->
    <script type="text/javascript" language="javascript"src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript"	src="assets/js/data-tables/DT_bootstrap.js"></script>
<script		src="assets/js/dynamic_table_init.js"></script>
    
    
    
    <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
 
    <script src="assets/js/customJS/repair_validation.js"></script>
<script src="assets/js/customJS/utility.js"></script>

<script>
$(".opendialog-amend").click(function () {
	$('#amendMsg').val(" ");
    $('#requestId').val($(this).data('id'));
    $('#amend_request_modal').modal('show');
});

$(".opendialog-reopen").click(function () {
	$('#reopenMsg').val(" ");
    $('#reopenrequestId').val($(this).data('id'));
    $('#reopen_request_modal').modal('show');
});

</script>

    </jsp:attribute>
	<jsp:body>
	
	
        
        <div class="page-heading">
        <!-- Cab/Ticket -->
          <section class="panel">
                        <header
				class="panel-heading custom-tab dark-tab">
                            <ul class="nav nav-tabs">
                                
                                <li class="active col-md-3 t-tabs">
                                    <a href="#repair_maintenance"
					data-toggle="tab">Repair / Maintenance</a>
                                </li>
                               
                            </ul>
                        </header>

 <!------------ user name and user id ------->
<input type="text" id="userName"
				value="${userSession.empData.displayName}" style="display: none;" />
<input type="text" id="userID" value="${userSession.empData.employeeID}"
				style="display: none;" />
<input type="text" id="locationName" value="${userSession.locationName}"
				style="display: none;" />
<input type="text" id="userRole" value="${userSession.empRole[0]}" style="display: none;"/>


<!-- =====Tab Pannel Start Here -->
<div class="panel-body">
    <div class="tab-content">
        

<!-- ==========Form Hotel / Guest House Booking =======  -->
    <div class="tab-pane active" id="repair_maintenance">
             <!--==========Tab Start Here========-->                    
                              

  <!--  The form that will be parsed by jQuery before submit  -->
  <form action="repairmaintaince" method="post"
							id="register-form" novalidate="novalidate"
							class="cmxform form-horizontal adminex-form">
  
   <div class="row">
             
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                            </div>
                            <div class="col-sm-7">
                             
                            </div>
                        </div>
                    </div>
                    </div>
            <!-- ==================Row 3=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Name(Requestor)<!-- <span
												class="labelcolor">*</span> --></label>
                                </div>
                           <div class="col-sm-7">
                              <input type="text" class="form-control alphabetOnly"
												id="username" name="username" placeholder="Requestor" readonly="" />
                      </div>
                      </div>
                      </div>
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Employee ID<!-- <span
                        class="labelcolor">*</span> --></label>
                                </div>
                           <div class="col-sm-7">
                              <input type="text" class="form-control alphabetOnly"
                        id="employeeID" name="employeeID" readonly="" />
                      </div>
                      </div>
                      </div>
                   
                         <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Department<!-- <span
												class="labelcolor">*</span> --></label>
                            </div>
                             <div class="col-sm-7">
         <input type ="text"  value="${dropDown.department}" readonly="" class="form-control" id="department" name="department" >
                            
                              </div>
                     </div>
                     </div>
                    
                    
                    </div>
                    <br />
            <!-- ======================End Row @ 3==================== -->
            <!-- ==================Row start @4=============== -->
               
               <div class="row">
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                    
                                <label>Mobile No<span
                        class="labelcolor">*</span></label>
                                </div>
                           <div class="col-sm-7">
                              <input type="text" class="form-control numbersOnly"
                        id="mob" name="mob" placeholder="Mobile No" maxlength="10"/>
                    </div>
                    </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Nautre Of Work<span
												class="labelcolor">*</span></label>
                                </div>
                            <div class="col-sm-7">
                            
                            <input type="hidden" id="now" value='${dropDown.repairNatureOfWork}'/>
                               <select class="form-control select2" id="workservi" onchange="setSub(value)"
												name="natureOfWork">
							<option value="">select...</option>
                                <c:forEach items="${dropDown.repairNatureOfWork.entrySet()}" var="natureOfWorkValue">
                                  <option><c:out value="${natureOfWorkValue.key}"></c:out></option>  
                                  </c:forEach>  
                              </select>
                              </div>
                       </div>
                       </div>
                       <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Sub-Nature Of Work<span
                        class="labelcolor">*</span></label>
                                </div>
                            <div class="col-sm-7">
                               <select class="form-control select2" id="nature"
                        name="subNatureofWork">
                               <option value="">select...</option>
                              </select>
                              </div>
                       </div>
                       </div>
                    
                  <!--  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Problem Description<span
												class="labelcolor">*</span></label>
                          </div>
                           <div class="col-sm-7">
                                <input class="form-control" size="12" maxlength="50"
												type="text" name="description" id="description" placeholder="Problem Description"/>
                     </div>
                     </div>
                     </div>
                     -->
                        </div>
                
            <!-- ======================End Row @ 4==================== -->
      
       <div class="row">
       
                      <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Work Location<span
                        class="labelcolor">*</span></label>
                          </div>
                          
                           <div class="col-sm-7">
                                        <select class="form-control select2" id="work"
                        name="work">
                               <option value="">select...</option>
                                <c:forEach items="${dropDown.workLocationDropDown}" var="cwl">
                                
                                  <option><c:out value="${cwl}"></c:out></option>  
                                  </c:forEach>  
                              </select>
                        </div>
                        </div>
                        </div>
        <div class="col-sm-8 col-md-4">
        <div class="form-group"> 
        <div class="col-sm-3">
         <input type="text" class="form-control" name="loc_repair" id="loc_repair" style="width:80px;" readonly />
         </div>
         <div class="col-sm-9">
         <input type="text" class="form-control" name="loc1_repair" id="loc1_repair" style="width:213px ;" placeholder="Enter Specific Location"/>     
        </div>
        </div>
   </div>


    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Problem Description<span
                        class="labelcolor">*</span></label>
                          </div>
                           <div class="col-sm-7">
                                <input class="form-control" size="12" maxlength="50"
                        type="text" name="description" id="description" placeholder="Problem Description"/>
                     </div>
                     </div>
                     </div>
   
    <!-- <div class="col-sm-8 col-md-4" style="display:none">

   
   

        <div class="form-group"> 
        <div class="col-sm-5">
<label>Request Purpose</label>        
                 </div>
         <div class="col-sm-7">
         <input type="text" class="form-control" value="For office use only" readonly/>     
        </div>
        </div>
   </div>  -->
  </div>
  <div class="row"> 
   <div class="col-sm-8 col-md-4" >

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Landline &nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control numbersOnly" id="repairLandline" name="repairLandline" maxlength="14"  placeholder="Landline">
      <p class="help-block">Ex. 1662245635</p>
      </div>
   <!--  <div class="col-sm-5">
      <input type ="text" class="form-control" id="airNumber" name="airNumber" maxlength="7" style="width:106px" placeholder="Number">
    </div> -->
</div>


</div>
   
   
   
       </div>     
                
    <br/>      
<center>
<button class="btn btn-info" type="submit" name="action" value="GENERATED" id="submit">
									<i class="fa fa-hand-o-right"></i> Submit</button>
							</center>
  </form>
  
  
  <!-- Dyanamic Table Start -->
  <!--  <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
           USER VIEW
            <span class="tools pull-right">
                
             </span>
        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered "
											id="hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Workorder No</th>
                        <th>Requester</th>
            
            <th class="hidden-phone">Booking For</th>
            
            <th class="hidden-phone">Request Date</th>
            
            <th class="hidden-phone">Status</th>
            
            <th>Action</th>
        </tr>
        </thead>
        <c:forEach  items="${rpm.USERrpmDataList}" var="valueMap">

        <tr class="gradeX reopentr" id="rid${valueMap['requestID'] }"><td>
<a class="showRequestDataModal"href="javascript:;"  onclick="callShowRequestData(<c:out value="${valueMap['requestID']}"></c:out>, '<c:out value="${valueMap['service']}" />')"><c:out value="${valueMap['workorder']}"></c:out></a></td>         
         <td><c:out value="${valueMap['requester']}"></c:out></td>
       
        <td><c:out value="${valueMap['service']}"></c:out></td>
        <td><c:out value="${valueMap['requestdate']}"></c:out></td>
         <td>
<c:choose>
	<c:when test="${valueMap['requestStatus'] =='REOPEN'}">
	<a class="showResponseDataModal" href="javascript:;"
	onclick="callShowReopenMsg('<c:out value="${valueMap['requestID']}"></c:out>')"><c:out value="${valueMap['requestStatus']}" /></a>
    </c:when>   
<c:when test="${valueMap['requestStatus'] =='CLOSED'}">
													<a class="showResponseDataModal" href="javascript:;"
														onclick="callShowResponseMsg('<c:out value="${valueMap['requestID']}"></c:out>')"><c:out value="${valueMap['requestStatus']}" /></a>
</c:when> 
   
      <c:otherwise>
	      <c:out value="${valueMap['requestStatus']}" />
      </c:otherwise>
			    
</c:choose>         
        
            <td>
            <c:if test="${valueMap['requestStatus'] !='CLOSED'}">
<button class="btn btn-info opendialog-amend" type="button" name="action" value="Amend" data-toggle="modal" data-id="${valueMap['requestID']}"  onclick="ammendRequest('${valueMap['requestID']}')"> 
	<i class="fa fa-hand-o-right" ></i> Ammend</button>
	</c:if>
	<c:if test="${valueMap['requestStatus'] =='CLOSED'}">
<button class="btn btn-info opendialog-reopen" type="button" name="action" value="Reopen" data-toggle="modal" data-id="${valueMap['requestID']}" > 
	<i class="fa fa-hand-o-right" ></i> Reopen</button>
</c:if>
</td>
        </tr>
       </c:forEach>
                </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div> -->
        <!-- Dyanamic Table End -->
    </div>

                                                                                                      
<!--===========End Hotel / Guest House Request Form====-->    

        
            <!-- Travel Admin Requests Start Here -->
            <div class="tab-pane" id="admin_travel_department">
                                    <!-- Form for Ticket Requests -->
                                  <div id="preapprove"
							style="width: 100%">
                                  <form action="action">
                                  <div class="table">
                                        <div class="row">
                                                  <div class="col-md-4">
                                                  <label for="ext_date">One Way Journey</label>&nbsp;&nbsp;&nbsp;Yes</div>
                                                  <div class="col-md-4">
											<label for="ext_date">Return Journey</label>&nbsp;&nbsp;&nbsp;No</div>
                                                  <div class="col-md-4">
											<label for="ext_date">Food & Beverages (As per Policy)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yes</div>
                                         </div>

                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="t_type">Booking Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Train Ticket</div>
                                                  <div class="col-md-4">
											<label for="t_type">Booking for&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Self Booking</div>
                                                  <div class="col-md-4">
											<label for="t_type">Preffered Time Slot&nbsp;&nbsp;</label>09:00 HRS - 11:00 HRS</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">Travel Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>12-Feb-2016</div>
                                                  <div class="col-md-4">
											<label for="t_type">Travel Class&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>AC-III Tier</div>
                                                  <div class="col-md-4">
											<label for="w_id">Travel Purpose&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Offical Only</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">Passenger&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</label>Manoj Singh</div>
                                                  <div class="col-md-4">
											<label for="w_id">Employee ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>EMP/DB/1234</div>
                                                  <div class="col-md-4">
											<label for="w_id">Mobile No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>9643827282</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">Source&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>XYz Station</div>
                                                  <div class="col-md-4">
											<label for="w_id">Destination&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>XYZ Destination</div>
                                                  <div class="col-md-4">
											<label for="ext_date">Avail Cab</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yes&nbsp;&nbsp;&nbsp;<label
												for="ext_date">Avail Hotel/Guest House</label>&nbsp;&nbsp;&nbsp;Yes</div>    
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
											<label for="w_id">PickUp Point&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Xyz Address ,State</div>
                                                  <div class="col-md-4">
											<label for="w_id">End Point&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>Xyz Address, State</div>
                                                  <div class="col-md-4">
											<label for="w_id">E-Attachment&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>abc.pdf</div>    
                                         </div>
                                          
                                    </div>
                                    </form>
                                    </div>
                                    <br>
        <!-- ==== Dynamic table Start here ===== -->
           
                    <div class="panel " style="border: 1px solid black">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                    <header class="panel-heading">
                                       Ticket Booking Details
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                        <div id="preapprove"
												style="width: 100%">
                                  <form action="action">
                                  <div class="table">
                                         <div class="row">
                                                  <div class="col-md-4">
																<label for="w_id">Depature Date/Time</label><input
																	type="date" value="Depature Date"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Booking Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="date" value="" style="height: 21px; width: 175px"
																	id="w_id" background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">E-Ticket &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="button" value="Upload E-ticket"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                         </div>
                                         
                                         
                                    </div>
                                    </form>
                                    </div>

                                        </div>
                                    </div>
                                    </section>
                                    </div>
                                    </div>


                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                    <header class="panel-heading">
                                       Cab Booking Details
                                        <!-- <span class="tools pull-right">
                                           
                                         </span> -->
                                    </header>
                                    <div class="panel-body">
                                        <div class="adv-table">
                                        <div id="preapprove"
												style="width: 100%">
                                  <form action="action">
                                  <div class="table">
                                         <div class="row">
                                                  <div class="col-md-4">
																<label for="w_id">Cab Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vehicle Type"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Cab No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vechile No"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Cab Provider &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vendor Name"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                         </div>
                                         <div class="row">
                                                  <div class="col-md-4">
																<label for="w_id">Driver Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vehicle Type"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">Driver No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
																	type="text" value="Vechile No"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                                  <div class="col-md-4">
																<label for="w_id">PickUp Date/Time</label><input
																	type="date" value="Vendor Name"
																	style="height: 21px; width: 175px" id="w_id"
																	background-color="#45a049" name="workorderid">
															</div>
                                         </div>
                                    </div>
                                    </form>
                                    </div>



                                        </div>
                                    </div>
                                    </section>
                                    </div>
                                    </div>
<!-- Row-2 End Here -->


                    </div>

                <!-- ====Ticket Requests End Here -->
            </div>



    </div>
   
  <!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">REQUEST DATA</h4>
        </div>
        <div class="modal-body" style=" overflow :auto;">
             </div>
        <br>
        <div class="modal-footer sticky-footer" style="position:relative; margin-top:0px !important">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of modal -->

<!-- Amend Modal Starts -->
  <div class="modal fade" id="reopenRequestModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reopen</h4>
        </div>
        <div class="modal-body">
        
        
             </div>
        <br>
        <div class="modal-footer" style="margin-top: 20px;position:relative; ">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of Amend modal -->



 <!-- Amend Modal Starts -->
  <div class="modal fade" id="amendResponseMsgModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Amend Message</h4>
        </div>
        <div class="modal-body">
        
        
             </div>
        <br>
        <div class="modal-footer" style="margin-top: 20px;position:relative; ">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of Amend modal -->

<!-- start of Amend message inserting modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="amend_request_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Amend</h4>
        </div>
         <form>
         <input type="hidden" name="requestId" id="requestId" class="admin-ammend"/>
         
        <div class="modal-body" style="overflow:auto;">
        </div>
        <div class="modal-footer sticky-footer" style="position:relative;margin-top:0px !important">
          <center><button type="button" class="btn btn-default" data-dismiss="modal" onclick="callAmendRequest()">Submit</button></center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>
  


<!-- Amend message inserting modal ends -->


<!-- reopen modal starts -->
                        
 <!-- Modal -->
  <div class="modal fade" id="reopen_request_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reopen Comments</h4>
          
        </div>
         <form>
         <input type="hidden" name="reopenrequestId" id="reopenrequestId" value="" class="reopen admin-reopen"/>
        <div class="modal-body" style="overflow:auto;">
       
         <table>
        <tr><td>
        
          <label>Comments: &nbsp;  </label></td><td style="margin-top:10px;"><textarea rows="2" cols="30" class="reopen admin-reopen" id="reopenMsg" name="reopenMsg" maxlength="200"></textarea></td>
          </tr>
</table>
        </div>
        <div class="modal-footer sticky-footer" style="position:relative;margin-top:0px !important">
          <center><button type="button" class="btn btn-default" data-dismiss="modal" onclick="reopenRequest()">Submit</button></center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>
  


<!-- reopen message  modal ends -->

<!-- reopen msg Modal -->
  <div class="modal fade" id="reopenResponseMsgModal" role="dialog">
    <div class="modal-dialog">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reopen Comments</h4>
        </div>
        <div class="modal-body">
             </div>
        <br>
        <div class="modal-footer" style="margin-top: 20px;position:relative; ">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of reopen msg modal -->  



 
   <!-- Start Of  Response Message (CLOSED) Status modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="responseMsgModal" role="dialog">
    <div class="modal-dialog">
  
  <!-- <form action="DownloadPDFServlet" method="POST"> -->
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Response Message </h4>
        </div>
        <div class="modal-body">
  
         </div>
       
       
        <div class="modal-footer" style="margin-top: 10px; position: relative;">
<center>								
<button class="btn btn-warning " type="submit"  data-dismiss="modal"> <i class="fa fa-hand-o-right"></i> Ok</button>
</center>
        </div>
    
      </div>
   <!--  </form> -->
    </div>
    
  </div>   
   
 
 <!--  end of CLOSED Response Data -->  
    
    </div>
    </section>
    <!-- ===== Dynamic Table End Here ===== -->
    </div>
    </jsp:body>
</t:form-layout>