<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:layout>

	<jsp:attribute name="header">

</jsp:attribute>
 <jsp:attribute name="extrajs">
    
   <script src ="assets/js/customJS/manager_dropdown.js"></script>
   
    </jsp:attribute>


	<jsp:body>
  <div class="page-heading">
       <section class="panel">
       <div class="panel-body">
  
<div class="row">
 <form name="myForm" id="myForm" action="ManageDropDownServlet">
               <div class="col-sm-8 col-md-6">
                        <div class="form-group"> 
            
                             <div class="col-sm-7">
                              <select class="form-control" id="download" name="ddTemplate">
                                <option value="">select..</option>
                                <option value="cost_centre">Cost Center</option>
                                <option value="guest_type">Guest Type</option>
                                <option value="hotel_location">Hotel Location</option>
                                <option value="nature_of_work">Nature Of Work</option>
                                <option value="source">Source</option>                              
                                <option value="destination"> Destination</option>
                                <option value="start_point">Start Point</option>
                                <option value="end_point">End Point</option>
                                 <option value="travel_class">Travel Class</option>
                                 <option value="cab_preference">Cab Preference</option>
                                <option value="preffered_time_slot">Pref Time & Slot</option>
                                <option value="airport_list">Airport List</option>
                                    <option value="electrical">Electrical</option>
                                    <option value="hk_natureofwork">HouseKeeping Nature of Work</option>
                                    <option value="plumbing">Plumbing</option>
                                    <option value="hvca">HAVC</option>
                                    <option value="carpentery">carpentery</option>
                                    <option value="civil">Civil</option>
                              </select>
                              </div>
                    </div>
                     </div>
                                                   <input type="text" name="action" style="display: none;" value="download">
                     
                     
      <div class="col-sm-8 col-md-3">
           <div class="form-group"> 
                    <button type="submit">Download Template</button>
           </div>
      </div>
 </form> 
</div>

</div>
  </section>
 </div>
 
 <section class="panel">
       <div class="panel-body">
  
<div class="row">

<form name="myForm2" id="myForm2" action="ManageDropDownServlet" method="post" enctype="multipart/form-data">
               <div class="col-sm-8 col-md-6">
                        <div class="form-group"> 
            
                             <div class="col-sm-7">
                              <input type="file" name="uploadedFile">
                              <input type="text" name="action" style="display: none;" value="upload">
                              
                              </div>
                    </div>
                     </div>
                     
                     
      <div class="col-sm-8 col-md-3">
           <div class="form-group"> 
                    <button type="submit">Upload Template</button>
           </div>
      </div>
 </form> 


</div>
</div>
  </section>
 
</jsp:body>
</t:layout>