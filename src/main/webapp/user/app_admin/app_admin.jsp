<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.admin4u.views.RoleBaseDashboardBean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:appadmin>

	<jsp:attribute name="extrajs">
   <script src="assets/js/customJS/index.js"></script>

</jsp:attribute>
	<jsp:body>
            
        <!--body wrapper start-->
        
 
<!--dashboard items  start -->
      <div class="grid-container col-md-12">
                  <div class="row states-info">
                   
            
            
              <a
					href="RoleManagementServlet?as=APPLICATION_ADMIN&action=forwordRoleJSP"
					class="atext">
	            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
							<img src="assets/images/admin-icon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Role Management</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
            </a>
            
            
            
               
               
                 <a href="user/managedesignation.jsp" class="atext">
	            <div class="col-md-3" id="">
	                <div class="panel">
	                    <div class="imgbg">
	                    <div class="col-sm-4 grid-icon2">
							<img src="assets/images/admin-icon.png" class="grid-img3">
	                    </div>
	                    <div class="col-sm-7 grid-text2">
	                    	<h2>Manage Designation</h2>
									<br>
	                    	<p>Know More</p>
	                    </div>
	                    </div>
	                </div>
	            </div>
            </a>
            
                
      
            
            
            
                
        </div>
        </div>
   <!--end dashboard items  start -->
            
            
                <!-- ==================Next Button Start Here================= -->
        <div class="row"> 
                <ul class="pager">
                  
                </ul>
        </div>
                           
        <!--body wrapper end-->

        <!--footer section start-->

        <footer class="sticky-footer">
            2014 &copy; Admin4U by Turningcloud
        </footer>

        <!--footer section end-->
    <!-- main content end-->

    </jsp:body>
</t:appadmin>