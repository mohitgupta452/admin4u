<!-- ===========Form for Cab Requests=========================-->
 
   
        <!-- radio button ends -->

         <!-- tabs starts-->
         </br>
                   <div class="pick">
                           <form action="cabrequestservlet" method="post" id="cab-form" novalidate="novalidate" class="cmxform form-horizontal adminex-form">
                   <input type="hidden" name="cabselcect" value="CABPICK"/>
            <div class="row">
                    <div class="col-sm-8 col-md-4">
                        
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                        
                    </div>
                    </div>
                </div>
                                                
                                           <!-- ==================Row 3=============== -->
                <div class="row">
                <!-- Pick Booking for content start -->
                <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Booking For<span class="labelcolor" >*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2 bookingFor" id="cab_book" name="bookingFor">
                              <option value="">Select...</option>
                                 <c:forEach items="${dropDown.bookingForDropDown}" var="bookingForValue">
                                <option><c:out value="${bookingForValue}"></c:out></option>
                                
                               </c:forEach> 
                              </select>




<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="cab_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" onclick="pickExtenalCloseBtn()" class="close" type="button">�</button>
                                        <h4 class="modal-title">Booking For-External</h4>
                                    </div>
                                    <div class="modal-body">

                                        
                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Guest Type<span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="pick4uExternalGuestType" name="pick4uExternalGuestType">
                              <option value="">select..</option>
                                <c:forEach items="${dropDown.guestTypeDropDown}" var="guestTypeValue">
                                <option><c:out value="${guestTypeValue}"></c:out></option>
                                
                               </c:forEach> 

                              </select>
 <span id="pick_guesttype_error_msg"></span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Cab Segment<span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="Pick4uExternalCabPref" name="pick4uExternalCabPref">
                              
                                <option value="">select...</option>
                                 <c:forEach items="${dropDown.cabPreferenceDropDown}" var="cabPreferenceValue">
                                <option><c:out value="${cabPreferenceValue}"></c:out></option>
                                
                               </c:forEach> 
                                
                              </select>
 <span id="pick_cabpref_error_msg"></span>
                                                </div>
                                            </div>
                                                 <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button type="button" class="btn btn-primary" onclick="pickExtenalBtn()">OK</button>
                                                </div>
                                            </div>
                                
                                    </div>

                                </div>
                            </div>
                        </div>

                            </div>
                        </div>
                    </div>
                <!-- Pick Content End-->
               
                    <div class="col-sm-8 col-md-4" id="pickremoveSpan">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee ID <span class="labelcolor" >*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" maxlength="50" onblur="getEmployeeNameById(this,'cab_passenger')" class="form-control" id="cab_employee" name="cab_employee" placeholder="Employee ID">
                            </div>
                    </div>
                    </div>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Pax Name <span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" maxlength="50" class="form-control alphabetOnly" id="cab_passenger" name="cab_passenger" placeholder="Pax Name">
                            </div>
                        </div>
                    </div>
                </div></br>
                
            <!-- ======================End Row @ 3==================== -->
            <!-- ==================Row 3=============== -->
                <div class="row">
                 <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Mobile No<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" class="form-control numbersOnly" id="cab_mob" name="cab_mob" placeholder="Mobile No." maxlength="10">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                 <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Department <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  value="${dropDown.department}" readonly="" class="form-control" id="cab_department" name="cab_department" >
                            </div>
                    </div>
 </div> 
  <!-- landline code -->
  
  <div class="col-sm-8 col-md-4" >

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Landline &nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control numbersOnly" id="cab_landline" name="cab_landline" maxlength="14" placeholder="Landline">
      <p class="help-block">Ex. 1662245635</p>
        
      
    </div>
</div>


</div>
  
  
  
                </div>
            <!-- ======================End Row @ 3==================== -->      
            <!--               ====row 2=====                     -->
                <div class="row">
                     <div class="col-sm-8 col-md-4" >
                        <div class="form-group"> 
                         <div class="col-sm-5">
                              <label>Cost Centre <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
         <select class="form-control select2" id="cab_costcenter" name="cab_costcenter">
                              <option value="${dropDown.companyName},${dropDown.locationName}"><c:out value="${dropDown.companyName},${dropDown.locationName}"/></option>
                                
                                <c:forEach items="${dropDown.getCompanyNameList }" var="comp">
                                <option value="${comp[0]},${comp[1]}"><c:out value="${comp[0]},${comp[1]}"/></option>
                                
                               </c:forEach>
                              </select>                    
                           </div>
                    </div>
                    </div>
                      <c:if test="${userSession.empRole[0]=='ADMIN' || userSession.empRole[0]=='SPOC' }">
                    <div class="col-sm-8 col-md-4" style="display:none">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Cost Centre Code<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
 <input type="text" value="<c:out value="${dropDown.costcenter}"></c:out>" readonly="" class="form-control" id="cab_costcentercode"
    name="cab_costcentercode">                                     
                            </div>
                        </div>
                    </div>
                    </c:if>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Start Point<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                               <input type ="text" value="" maxlength="50" class="form-control" id="cab_start" name="cab_start" placeholder="Start Point">
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>End Point <span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" maxlength="50" class="form-control" id="cab_end" name="cab_end" placeholder="End Point">
                            </div>
                        </div>
                    </div>
                </div>
                
            <!-- ==================Row 3=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PickUp Date & Time <span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
									<div class="input-group date form_datetime-component">
                                            <input type="text" class="form-control" readonly="" size="16" name="pick4uPickupDateTime" id="pick4uPickupDateTime"  placeholder="PickUp Date & Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>
									<label class="error" for="pick4uPickupDateTime" generated="true"></label>
                                
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>End Date & Time <!-- <span class="labelcolor">*</span> --></label>
                         </div>
                            <div class="col-sm-7">
                               
<div class="input-group date form_datetime-component2">
                                            <input type="text" class="form-control " readonly="" size="16" name="pick4uEndDateTime"  id="pick4uEndDateTime" placeholder="End Date & Time" >
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>                                                           
                                        
									<label class="error" for="pick4uEndDateTime" generated="true"></label>

<!--                                 <input size="16" type="text" value=" " class="form_datetime form-control" >
 -->                            </div>
                    </div>
                    </div>
                    
       <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Travel Purpose<!-- <span class="labelcolor">*</span> --></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="For office use only" readonly="" class="form-control" id="cab_travel" name="cab_travel" placeholder="Travel Purpose">
                            </div>
                        </div>
                    </div>
       
                    
                   
                </div>
            <!-- ======================End Row @ 3==================== -->

          <!-- =======Submit Start Here====== -->
          <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
            <center><button class="btn btn-info " type="submit" id="submit" name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            
            </form>

    </div>
    <!-- pick4U ends -->
    
    <!-- drop4U starts -->
     <div class="drop boxx">
                           <form action="cabrequestservlet" method="post" id="cab-drop-form" novalidate="novalidate" class="cmxform form-horizontal adminex-form">
              <input type="hidden" name="cabselcect" value="CABDROP"/>
            <div class="row">
            
                    <div class="col-sm-8 col-md-4">
                        
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         
                    </div>
                    </div>
                </div>
                                                
                                                
            <!--====row 2=====                     -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Booking For<span class="labelcolor" >*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2 bookingFor" id="cab_drop_book" name="bookingFor">
                              <option value="">Select...</option>
                                <c:forEach items="${dropDown.bookingForDropDown}" var="bookingForValue">
                                <option><c:out value="${bookingForValue}"></c:out></option>
                                
                               </c:forEach> 
                              </select>




<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="cab_drop_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" onclick="dropExtenalCloseBtn()" class="close" type="button">�</button>
                                        <h4 class="modal-title">Drop4u Booking For-External</h4>
                                    </div>
                                    <div class="modal-body">

                                        
                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Guest Type<span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="cabDropExternalGuestType" name="cabDropExternalGuestType">
                              <option value="">select..</option>
                                 <c:forEach items="${dropDown.guestTypeDropDown}" var="guestTypeValue">
                                <option><c:out value="${guestTypeValue}"></c:out></option>
                                
                               </c:forEach> 
                              </select>
<span id="drop_guesttype_error_msg"></span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Cab Segment<span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="cabDropExternalCabPref" name="cabDropExternalCabPref">
                              
                                <option value="">select...</option>
                                 <c:forEach items="${dropDown.cabPreferenceDropDown}" var="cabPreferenceValue">
                                <option><c:out value="${cabPreferenceValue}"></c:out></option>
                                
                               </c:forEach> 
                                
                              </select>
<span id="drop_cabpref_error_msg"></span>
                                                </div>
                                            </div>
                                                 <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button type="button" class="btn btn-primary" onclick="dropExtenalBtn()">OK</button>
                                                </div>
                                            </div>
                                
                                    </div>

                                </div>
                            </div>
                        </div>

                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-sm-8 col-md-4" id="dropremoveSpan">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee ID<span class="labelcolor" >*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" maxlength="50" onblur="getEmployeeNameById(this,'cab_drop_passenger')" class="form-control" id="cab_drop_employee" name="cab_drop_employee" placeholder="Employee ID">
                            </div>
                    </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Pax Name<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" maxlength="50" class="form-control alphabetOnly" id="cab_drop_passenger" name="cab_drop_passenger" placeholder="Pax Name">
                            </div>
                        </div>
                    </div>
                    
                   
                </div>
                </br>
                <!-- ==================Row 3=============== -->
                <div class="row">
                 <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Mobile No<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" class="form-control numbersOnly" id="cab_drop_mob" name="cab_drop_mob" placeholder="Mobile No"  maxlength="10" >
                            </div>
                        </div>
                    </div>
<div class="col-sm-8 col-md-4">
                 <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Department <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  value="${dropDown.department}" readonly="" class="form-control" id="cab_drop_department" name="cab_drop_department" >
                            </div>
                    </div>
 </div>
  <!-- <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Travel Purpose<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="For office use only" readonly=""  class="form-control" id="cab_drop_travel" name="cab_drop_travel" placeholder="Travel Purpose">
                            </div>
                        </div>
                    </div>
                   -->
<div class="col-sm-8 col-md-4" >

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Landline &nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control numbersOnly" id="cab_drop_landline" name="cab_drop_landline" maxlength="14" placeholder="Landline">
      <p class="help-block">Ex. 1662245635</p>
        
      
    </div>
</div>


</div>                    

                </div>
                
            <!-- ======================End Row @ 3==================== -->

           
            <!-- ==================Row 3=============== -->
                <div class="row">
                
                <div class="col-sm-8 col-md-4" >
                        <div class="form-group"> 
                         <div class="col-sm-5">
                              <label>Cost Centre <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
         <select class="form-control select2" id="cab_drop_costcenter" name="cab_drop_costcenter">
                              <option value="${dropDown.companyName},${dropDown.locationName}"><c:out value="${dropDown.companyName},${dropDown.locationName}"/></option>
                                
                                <c:forEach items="${dropDown.getCompanyNameList }" var="comp">
                                <option value="${comp[0]},${comp[1]}"><c:out value="${comp[0]},${comp[1]}"/></option>
                                
                               </c:forEach>
                              </select>                    
                           </div>
                    </div>
                    </div>
             <!--          <c:if test="${userSession.empRole[0]=='ADMIN' || userSession.empRole[0]=='SPOC' }">
                    <div class="col-sm-8 col-md-4" style="display:none">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Cost Centre Code<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
           <input type="text" value="<c:out value="${dropDown.costcenter}"></c:out>" readonly="" class="form-control" id="cab_drop_costcenter"
			  name="cab_drop_costcenter">                                     
                            </div>
                        </div>
                    </div>
                    </c:if>  -->
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Start Point<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                               <input type ="text" value="" maxlength="50" class="form-control" id="cab_drop_start" name="cab_drop_start" placeholder="Start Point">
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>End Point<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" maxlength="50" class="form-control" id="cab_drop_end" name="cab_drop_end" placeholder="End Date">
                            </div>
                        </div>
                    </div>
                    
                   
                </div>
                
            <!-- ======================End Row @ 3==================== -->
                         <!-- ==================Row 3=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>PickUp Date & Time<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">

                                <div class="input-group date drop_form_datetime-component">
                                            <input type="text" class="form-control" readonly="" size="16" id="cab_drop_pick" name="cab_drop_pick" placeholder="PickUp Date & Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>

                                                  <label class="error" for="cab_drop_pick" generated="true"></label>
                                
                            </div>
                        </div>
                    </div>
                    
                   <!--  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>End Date / Time<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                               <div class="input-group date drop_form_datetime-component2">
                                            <input type="text" class="form-control" readonly="" size="16" id="cab_drop_date" name="cab_drop_date" placeholder="End Date & Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>
                               
                                         <label class="error" for="cab_drop_date" generated="true"></label>
                               
                            </div>
                    </div>
                    </div> -->
   <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Travel Purpose<!-- <span class="labelcolor">*</span> --></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="For office use only" readonly="" class="form-control" id="cab_drop_travel" name="cab_drop_travel" placeholder="Travel Purpose">
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            <!-- ======================End Row @ 3==================== -->
          <!-- =======Submit Start Here====== -->
          <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
            <center> <button class="btn btn-info " type="submit" id="submit" name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            
            </form>

    </div>
    
    <!-- drop4U ends -->
    
    
    <!-- disposal4U starts -->
    
    <div class="disposal boxx">
    
       <form action="cabrequestservlet" method="post" id="cab-disposal-form" novalidate="novalidate" class="cmxform form-horizontal adminex-form">
          <input type="hidden" name="cabselcect" value="CABDISPOSAL"/>                 
            <!--               ====row 2=====                     -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Booking For<span class="labelcolor" >*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2 bookingFor" id="cab_disposal_book" name="bookingFor">
                              <option value="">Select...</option>
                                 <c:forEach items="${dropDown.bookingForDropDown}" var="bookingForValue">
                                <option><c:out value="${bookingForValue}"></c:out></option>
                                
                               </c:forEach> 
                              </select>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="cab_disposal_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" onclick="disposalExtenalCloseBtn()" class="close" type="button">�</button>
                                        <h4 class="modal-title">Booking For-External</h4>
                                    </div>
                                    <div class="modal-body">

                                        
                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Guest Type<span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="cabDisposalExternalGuestType" name="cabDisposalExternalGuestType">
                              <option value="">select..</option>
                                 <c:forEach items="${dropDown.guestTypeDropDown}" var="guestTypeValue">
                                <option><c:out value="${guestTypeValue}"></c:out></option>
                               </c:forEach> 
                              </select>
            <span id="disposal_guesttype_error_msg"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Cab Segment<span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="cabDisposalExternalCabPref" name="cabDisposalExternalCabPref">
                              
                                <option value="">select...</option>
                                 <c:forEach items="${dropDown.cabPreferenceDropDown}" var="cabPreferenceValue">
                                <option><c:out value="${cabPreferenceValue}"></c:out></option>
                                
                               </c:forEach> 
                                
                              </select> 
          <span id="disposal_cabpref_error_msg"></span>                      
                                                                            </div>
                                            </div>
                                                 <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button type="button" class="btn btn-primary" onclick="disposalExtenalBtn()">OK</button>
                                                </div>
                                            </div>
                                
                                    </div>

                                </div>
                            </div>
                        </div>

                            </div>
                        </div>
                    </div>
                     <div class="col-sm-8 col-md-4" id="disposalremoveSpan">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee ID<span class="labelcolor" >*</span></label>
                            </div>
                            <div class="col-sm-7">
                               <input type ="text" maxlength="50" value="" onblur="getEmployeeNameById(this,'cab_disposal_passenger')" class="form-control " id="cab_disposal_employeeID" name="cab_disposal_employeeID" placeholder="EmployeeID">
                            </div>
                    </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Pax Name<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" maxlength="50" value="" class="form-control alphabetOnly" id="cab_disposal_passenger" name="cab_disposal_passenger" placeholder="Pax Name">
                            </div>
                        </div>
                    </div>
                </div>
                </br>
                <div class="row">
                 <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Mobile No<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" class="form-control numbersOnly" id="cab_disposal_mob" name="cab_disposal_mob" placeholder="Mobile No" maxlength="10">
                            </div>
                        </div>
                  </div>
                   
                   
                   <div class="col-sm-8 col-md-4">
                 <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Department <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  value="${dropDown.department}" readonly="" class="form-control" id="cab_disposal_department" name="cab_disposal_department" >
                            </div>
                    </div>
 </div>
 <!-- <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                        
                                  <div class="col-sm-5">
                                <label>Travel Purpose<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="For office use only" readonly=""  class="form-control" id="cab_disposal_travel" name="cab_disposal_travel" placeholder="Travel Purpose">
                            <br/>
                            </div>
                                               
                            
                        </div>
                    </div> -->
                    
 <div class="col-sm-8 col-md-4" >

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Landline &nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control numbersOnly" id="cab_disposal_landline" name="cab_disposal_landline" maxlength="14" placeholder="Landline">
      <p class="help-block">Ex. 1662245635</p>
        
      
    </div>
</div>


</div>
                </div>
            <!-- ==================Row 3=============== -->
                <div class="row">
                  <div class="col-sm-8 col-md-4" >
                        <div class="form-group"> 
                         <div class="col-sm-5">
                              <label>Cost Centre <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
         <select class="form-control select2" id="cab_disposal_costcenter" name="cab_disposal_costcenter">
                              <option value="${dropDown.companyName},${dropDown.locationName}"><c:out value="${dropDown.companyName},${dropDown.locationName}"/></option>
                                
                                <c:forEach items="${dropDown.getCompanyNameList }" var="comp">
                                <option value="${comp[0]},${comp[1]}"><c:out value="${comp[0]},${comp[1]}"/></option>
                                
                               </c:forEach>
                              </select>                    
                           </div>
                    </div>
                    </div>

<!-- 
  <c:if test="${userSession.empRole[0]=='ADMIN' || userSession.empRole[0]=='SPOC' }">
                     <div class="col-sm-8 col-md-4" style="display: none">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Cost Centre Code<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
<input type="text" value="<c:out value="${dropDown.costcenter}"></c:out>" readonly="" class="form-control" id="cab_disposal_costcenter"
       name="cab_disposal_costcenter">                 
                            </div>
                        </div>
                    </div>
                    </c:if>   -->
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Start Point<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                               <input type ="text" maxlength="50" value="" class="form-control" id="cab_disposal_start" name="cab_disposal_start" placeholder="Start Point">
                            </div>
                    </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Visit Location<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" maxlength="50" value="" class="form-control" id="cab_disposal_visit_location" name="cab_disposal_visit_location" placeholder="Visit Location" >
                            </div>
                        </div>
                   </div>
                </div>
            
            <div class="row">
              <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                        
                        <div class="col-sm-5">
                                <label>PickUp Date & Time<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">

                                <div class="input-group date disposal_form_datetime-component">
                                            <input type="text" class="form-control" readonly="" size="16" id="cab_disposal_pick" name="cab_disposal_pick" placeholder="PickUp Date & Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>

                                                        <label class="error" for="cab_disposal_pick" generated="true"></label>
                                
                            </div>
                        
                        
                        
                            
                            
                            
                        </div>
                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                        
                        <div class="col-sm-5">
                                <label>End Date & Time<span class="labelcolor"></span></label>
                            </div>
                            <div class="col-sm-7">

                                <div class="input-group date disposal_form_datetime-component2">
                                            <input type="text" class="form-control" readonly="" size="16" id="cab_disposal_end" name="cab_disposal_end" placeholder="End Date & Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>

                                                        <label class="error" for="cab_disposal_end" generated="true"></label>
                            </div>
                        </div>
                    </div>
           <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                        
                                  <div class="col-sm-5">
                                <label>Travel Purpose<!-- <span class="labelcolor">*</span> --></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="For office use only" readonly=""  class="form-control" id="cab_disposal_travel" name="cab_disposal_travel" placeholder="Travel Purpose">
                            <br/>
                            </div>
                                               
                            
                        </div>
                    </div>
              
            </div>
                
            <!-- ======================End Row @ 3==================== -->
            
            
         
          <!-- =======Submit Start Here====== -->
          <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
           <center> <button class="btn btn-info " type="submit" id="submit" name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            
            </form>

    </div>
    <!-- disposal4U ends -->
      <!-- airport4U starts -->
    <div class="airport boxx">
          
<form action="cabrequestservlet" method="post" id="cab-airport-form" novalidate="novalidate" class="cmxform form-horizontal adminex-form">        
          
          
          <!--====row 1=====                     -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Booking For<span class="labelcolor" >*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2 bookingFor" id="cab_airport_book" name="bookingFor">
                              <option value="">Select...</option>
                                <c:forEach items="${dropDown.bookingForDropDown}" var="bookingForValue">
                                <option><c:out value="${bookingForValue}"></c:out></option>
                                
                               </c:forEach> 
                              </select>




<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="cab_airportExternal_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" data-dismiss="modal" onclick="airportExtenalCloseBtn()" class="close" type="button">�</button>
                                        <h4 class="modal-title">Airport4u Booking For-External</h4>
                                    </div>
                                    <div class="modal-body">

                                        
                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Guest Type<span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="cabAirportExternalGuestType" name="cabAirportExternalGuestType">
                              <option value="">select..</option>
                                 <c:forEach items="${dropDown.guestTypeDropDown}" var="guestTypeValue">
                                <option><c:out value="${guestTypeValue}"></c:out></option>
                                
                               </c:forEach> 
                              </select>
 <span id="airport_guesttype_error_msg"></span>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Cab Segment<span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="cabAirportExternalCabPref" name="cabAirportExternalCabPref">
                              
                                <option value="">select...</option>
                                 <c:forEach items="${dropDown.cabPreferenceDropDown}" var="cabPreferenceValue">
                                <option><c:out value="${cabPreferenceValue}"></c:out></option>
                                
                               </c:forEach> 
                                
                              </select>
     <span id="airport_cabpref_error_msg"></span>

                                                </div>
                                            </div>
                                                 <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button type="button" class="btn btn-primary" onclick="airportExtenalBtn()">OK</button>
                                                </div>
                                            </div>
                                
                                    </div>

                                </div>
                            </div>
                        </div>

                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-sm-8 col-md-4" id="airportremoveSpan">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee ID<span class="labelcolor" >*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" maxlength="50" value="" class="form-control" onblur="getEmployeeNameById(this,'cab_airport_passenger')" id="cab_airport_employee" name="cab_airport_employee" placeholder="Employee ID">
                            </div>
                    </div>
                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Pax Name<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" maxlength="50" value="" class="form-control alphabetOnly" id="cab_airport_passenger" name="cab_airport_passenger" placeholder="Pax Name">
                            </div>
                        </div>
                    </div>
                    
                   
                </div>
                <!-- row 1 end -->
                </br>
                <!-- ==================Row 2=============== -->
                <div class="row">
                 <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Mobile No<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="" class="form-control numbersOnly" id="cab_airport_mob" name="cab_airport_mob" placeholder="Mobile No"  maxlength="10" >
                            </div>
                        </div>
                    </div>
<div class="col-sm-8 col-md-4">
                 <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Department <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  value="${dropDown.department}" readonly="" class="form-control" id="cab_airport_department" name="cab_airport_department" >
                            </div>
                    </div>
 </div>
 <!--  <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Travel Purpose<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="For office use only" readonly=""  class="form-control" id="cab_airport_travel" name="cab_airport_travel" placeholder="Travel Purpose">
                            </div>
                        </div>
                    </div>
                     -->
 <div class="col-sm-8 col-md-4" >

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Landline &nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control numbersOnly" id="cab_airport_landline" name="cab_airport_landline" maxlength="14" placeholder="Landline">
      <p class="help-block">Ex. 1662245635</p>
        
      
    </div>
</div>


</div>                  
                    

                </div>
                
            <!-- ======================End Row @ 2==================== -->
          
          
          
          
          
          
            <!--               ====row 3=====                     -->
                <div class="row">
      
      			<div class="col-sm-8 col-md-4" >
                        <div class="form-group"> 
                         <div class="col-sm-5">
                              <label>Cost Centre <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
         <select class="form-control select2" id="cab_airport_costcenter" name="cab_airport_costcenter">
                              <option value="${dropDown.companyName},${dropDown.locationName}"><c:out value="${dropDown.companyName},${dropDown.locationName}"/></option>
                                
                                <c:forEach items="${dropDown.getCompanyNameList }" var="comp">
                                <option value="${comp[0]},${comp[1]}"><c:out value="${comp[0]},${comp[1]}"/></option>
                                
                               </c:forEach>
                              </select>                    
                           </div>
                    </div>
                    </div>          
                
                
                
    <!-- start airport starts -->
    <input type="hidden" name="cabselcect" value="CABAIRPORT"/>
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Start Point<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
       <input type ="text"  class="form-control" maxlength="50" id="cab_start_airport" name="cab_start_airport" placeholder="Start Point">
                             </div>
                             </div></div>
                             
                              <%-- <select class="form-control select2 cab_start_airport" id="cab_start_airport" name="cab_start_airport">
                              <option value="">Select...</option>
                                <c:forEach items="${dropDown.startPointDropDown}" var="startPointValue">
                                <option><c:out value="${startPointValue}"></c:out></option>
                                
                               </c:forEach> 
                                </select> --%>
<%-- <!-- airport starts -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="cab_airport_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>
                                        <h4 class="modal-title">AIRPORT LIST</h4>
                                    </div>
                                    <div class="modal-body">

                                         <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Select Type</label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="external_modal_dropdown" name="bookingFor">
                              <option value="">select..</option>
                                <c:forEach items="${dropDown.airportDropDown}" var="airValue">
                                <option><c:out value="${airValue}"></c:out></option>
                                
                               </c:forEach> 
                     </select>

 </div></div><div class="form-group"> <div class="col-lg-offset-2 col-lg-10">
<center><button type="button" class="btn btn-primary" data-dismiss="modal" id="airportDropDownBtnOk">OK</button></center>
                                                </div>
                                            </div> </div> </div></div></div> 

<!-- airport ends -->
<!-- for others -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
				tabindex="-1" id="cab_other_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true"
								data-dismiss="modal" class="close" type="button">�</button>
                                        <h4 class="modal-title">Other</h4>
                                    </div>
                                    <div class="modal-body">

                                         <div class="form-group">
                                                <label for="inputEmail1"
									class="col-lg-2 col-sm-2 control-label"></label>
                                                <div class="col-lg-10">
                                                   
                        <input type="text" id="others" name="others" />
  </div>
 </div>
							<div class="form-group"> <div
									class="col-lg-offset-2 col-lg-10">
<button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                                                </div>
                                            </div> </div> </div>
				</div>
			</div>


<!-- for others end -->
 </div> </div>  </div> --%>
      <!-- start airport end -->              
                    

 <!-- end airport starts -->
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>End Point<span
						class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
     <input type ="text"  class="form-control" maxlength="50" id="cab_end_airport" name="cab_end_airport" placeholder="End Point">
            </div></div></div>                  
                              
                              
                              <%-- <select class="form-control select2 cab_end_airport"
						id="cab_end_airport" name="cab_end_airport">
                              <option value="">Select...</option>
                                <option>Airport</option>
                             
                                </select>
            <input  type="text"  class="form-control" placeholder="other" id="other1" name="other1"  style="display:none;"/>                 
                                
            <input type="text" name="cabAT_startPoint" id="cabAT_startPoint"  style="display:none;"/> 
             <input type="text" name="cabAT_endPoint"  id="cabAT_endPoint" style="display:none;"/> 
            
            
            
            
                               
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
						tabindex="-1" id="cab_airport_end_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true"
										data-dismiss="modal" class="close" type="button">�</button>
                                        <h4 class="modal-title">AIRPORT LIST</h4>
                                    </div>
                                    <div class="modal-body">

                                         <div class="form-group">
                                                <label for="inputEmail1"
											class="col-lg-2 col-sm-2 control-label">Select Type</label>
                                                <div class="col-lg-10">
                                                    <select
												class="form-control" id="external_modal_dropdown1"
												name="bookingFor">
                       <option value="">select..</option>
                                <c:forEach items="${dropDown.airportDropDown}" var="airValue">
                                <option><c:out value="${airValue}"></c:out></option>
                                
                               </c:forEach> 
                     </select>
 </div>
									</div> <div class="form-group">
										<div class="col-lg-offset-2 col-lg-10">
         <button type="button" class="btn btn-primary"
												data-dismiss="modal">OK</button>
                 </div> </div>
								</div> </div>
						</div>
					</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog"
						tabindex="-1" id="cab_other_end_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true"
										data-dismiss="modal" class="close" type="button">�</button>
                                        <h4 class="modal-title">Other</h4>
                                    </div>
                                    <div class="modal-body">

                                         <div class="form-group">
                                                <input type="text"
											id="others" name="others" />
 </div>
								</div> <div class="form-group">
									<div class="col-lg-offset-2 col-lg-10">
         <button type="button" class="btn btn-primary"
											data-dismiss="modal">OK</button>
                 </div> </div>
							</div> </div>
					</div>

                            </div>
			</div> </div>
			 --%>
			
			
			<!--cost center  -->


<!-- 
  <c:if test="${userSession.empRole[0]=='ADMIN' || userSession.empRole[0]=='SPOC' }">
                     <div class="col-sm-8 col-md-4" style="display: none">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Cost Centre Code<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">
<input type="text" value="<c:out value="${dropDown.costcenter}"></c:out>" readonly="" class="form-control" id="cab_airport_costcenter"
       name="cab_airport_costcenter">                 
                            </div>
                        </div>
                    </div>
                    </c:if> -->
			
			<!-- end  -->
			
			
			

      			 </div>
<!-- pickup date tiem row-start  -->
			 <div class="row">
			 
			 <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                        
                        <div class="col-sm-5">
                                <label>PickUp Date & Time<span class="labelcolor">*</span></label>
                            </div>
                            <div class="col-sm-7">

                                <div class="input-group date form_datetime-component">
                                            <input type="text" class="form-control" readonly="" size="16" id="cab_airport_pick" name="cab_airport_pick" placeholder="PickUp Date & Time">
                                                            <span class="input-group-btn">
                                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                                            </span>
                                        </div>

                                                        <label class="error" for="cab_airport_pick" generated="true"></label>
                                
                            </div>
                            
                        </div>
                    </div>
                    
              <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Travel Purpose<!-- <span class="labelcolor">*</span> --></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" value="For office use only" readonly=""  class="form-control" id="cab_airport_travel" name="cab_airport_travel" placeholder="Travel Purpose">
                            </div>
                        </div>
                    </div>
                    
			 
			 </div>
			 
			 <!--end pickup dat4e time row -->
			 
           <!-- - -->
       <!-- ======Row hidden Start======= -->
                            <div class="row" id="airport_startother"  style="display:none;">
                                    <div class="col-sm-8 col-md-4">
                                        <div class="form-group"> 
                                            <div class="col-sm-5">
                                                <label>Other<span class="requiredField"> *</span></label>
                                            </div>
                                            <div class="col-sm-7 ">
                                              <input type ="text" class="form-control" id="other" name="airstartother" placeholder="other">
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                            
            <!-- ======Row @7 End========= -->
          <!-- ======Row hidden2 Start======= -->
                            <div class="row" id="airport_endother"  style="display:none;">
                                    <div class="col-sm-8 col-md-4">
                                        <div class="form-group"> 
                                            <div class="col-sm-5">
                                                <label>Other<span class="requiredField"> *</span></label>
                                            </div>
                                            <div class="col-sm-7 ">
                                              <input type ="text" class="form-control" name="airendother" placeholder="other">
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                            
            <!-- ======Row @7 End========= -->
            <!-- ======================End Row @ 3==================== -->
          <!-- =======Submit Start Here====== -->
          <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
           <center> <button class="btn btn-info " type="submit"
							id="submit" name="action" value="GENERATED">
							<i class="fa fa-hand-o-right"></i> Submit</button>
					</center>
                        </div>
                    </div>
            </div>
            
            </form>       
     
</div>
