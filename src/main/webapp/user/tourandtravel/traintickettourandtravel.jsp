<div class="tab-pane" id="train_requests">
<!--============= Form for Ticket Requests ==================-->
 <form action="TrainTicketRequestServlet" method="post" enctype="multipart/form-data" id="trainRequestForm" novalidate="novalidate" class="cmxform form-horizontal adminex-form">
               
               <!-- hidden input box for location -->
                				<input type=text value="" class="hiddenLocation" name="trainLocation1" id="trainLocation1" style="display:none;"> 
                                <!-- hidden input box for location -->
                
               
                <div class="row">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group">

                        <div class="col-sm-6">
                                <label>One Way Journey<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-6">
                                  <input type="radio"  value="one way" id="trainOneWay" name="trainOneWay" checked/>
                            </div>
                    </div>
                    </div>
                    
 <div class="col-sm-8 col-md-4">
      <div class="form-group"> 
      <div class="col-sm-6">
      <label>Two Way Journey</label>
      </div>
       <div class="col-sm-6">
<input type="radio"  value="two way" id="trainTwoWay" name="trainOneWay" data-toggle="modal" data-target="#train_two_way_modal"/>


                                  <!-- start of modal -->

<!-- <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="train_two_way_modal" class="modal fade">
                            <div class="modal-dialog" style="min-width:60%;">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>
                                        <h4 class="modal-title">Return journey</h4>
                                    </div>
                                    <div class="modal-body">


<div class="form-group">
  <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label"></label>
                                                <div class="col-lg-10">
                                                    <b>Message :</b></br>
                                                    1.Your journey source station will be preffered as destination for returning journey .</br>
                                                    2.Your journey destination station will be preffered as source for returning journey .

                                                </div>
  </div>
  
  <div class="row">
                   
   <div class="col-sm-8 col-md-6">

<div class="form-group"> 
    <div class="col-sm-5">
        <label> Return Source <span class="requiredField"> </span></label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control" id="trainReturnSource" name="trainReturnSource" placeholder="Return Source">
    </div>
</div> 



</div>

<div class="col-sm-8 col-md-6">

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Return Destination<span class="requiredField"> </span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">

    <input type ="text" class="form-control" id="trainReturnDestination" name="trainReturnDestination" placeholder="Return Destionation" >
</div>
</div>


</div>  </div>               
                    
  
  
  
                                                 <div class="form-group">
                                                <div class="col-lg-offset-5 col-lg-7">
                                                    <button type="button" class="btn btn-primary col-sm-2" data-dismiss="modal">OK</button>
                                                </div>
                                            </div>



                                
                                    </div>

                                </div>
                            </div>
                        </div> -->

<!-- end of modal -->
 </div>
                    </div>
                    </div>
                    
                    
                    <div class="col-sm-8 col-md-4" >
                        <div class="form-group"> 
                         <div class="col-sm-5">
                              <label>Cost Centre <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
         <select class="form-control select2" id="trainCostCenter" name="trainCostCenter">
                              <option value="${dropDown.companyName},${dropDown.locationName}"><c:out value="${dropDown.companyName},${dropDown.locationName}"/></option>
                                
                                <c:forEach items="${dropDown.getCompanyNameList }" var="comp">
                                <option value="${comp[0]},${comp[1]}"><c:out value="${comp[0]},${comp[1]}"/></option>
                                
                               </c:forEach>
                              </select>                    
                           </div>
                    </div>
                    </div>

                </div>
                                                       
            <!--   ====row 2=====   -->
                <div class="row">
                    
                    
                    <div class="col-sm-8 col-md-4">
                        
<div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Travel Date<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                     <div class="input-group date traindp1">
                                <input size="16" type="text" id="trainTravelDate" name="trainTravelDate" readonly="true" class=" form-control" placeholder="Travel Date">
                               <span class="input-group-btn">
             <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                               </span>
                     </div> 
                                                                          
                            </div>
                        </div>
                    </div>
                    
         <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Preffered Time Slot <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2" id="trainPrefTimeSlot" name="trainPrefTimeSlot">
                              <option value="">Select...</option>
                                <c:forEach items="${dropDown.prefTimeSlotDropDown}" var="prefTimeSlotValue">
                                <option><c:out value="${prefTimeSlotValue}"></c:out></option>
                                
                               </c:forEach> 
                              </select>
                            </div>
                        </div>

                       
                    </div>
                    
                   <!--  <div class="col-sm-8 col-md-4" id="travel_purpose"> -->

               <div class="col-sm-8 col-md-4" >
                    <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Travel Purpose&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" class="form-control" id="trainTravelPurpose" name="trainTravelPurpose" readonly="" value="For office use only">
                                
                              
                            </div>
                        </div>

                        
                    </div>
                    
                    

<!-- Train Travel Return Date -->
                      <!-- <div class="col-sm-8 col-md-4" style="display:none" id="return_Date">
                              <div class="form-group">
                               <div class="col-sm-5">
                                                <label for="inputEmail1" class="control-label">Return Date<span class="requiredField">*</span></label>
                              </div>
                            <div class="col-sm-7">         
                               <div class="input-group date return_date_form_datetime-component">
                                      <input class="form-control" readonly="" size="16" id="trainReturnTravelDate" name="trainReturnTravelDate" type="text">
                                      <span class="input-group-btn">
                                      <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                      </span>
                                </div>

                              </div>
                            </div>
                        </div> -->
<!--  -->
<!-- ======= -->
                    
                    
                    
         <!--Start Popup -  On change event two way joiurney , Return Date journey will be popluated-->
                     <!-- <div class="col-sm-8 col-md-4" style="display:none" id="trainReturnTravelDateDiv">
                        <div class="form-group">
                            <div class="col-sm-5">
                                <label for="inputEmail1" class="control-label">Return Date<span class="requiredField">*</span></label>
                            </div>
                                <div class="col-sm-7">
                                <div class="input-group date train_return_date_form_datetime-component">
                                    <input class="form-control" readonly="" size="16" id="trainReturnTravelDate" name="trainReturnTravelDate" type="text">
                                            <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                            </span>
                                  </div>
                                            
                                    
                                </div>
                                </div>
                        </div> -->
                    <!-- End Popup -->             
                    

                </div>
                  <!-- ==================Row 3=============== -->
            
                  
                  
                <div class="row">
                   
                    
                    <div class="col-sm-8 col-md-4">
                       
                       <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Source<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7" id="trainSourceSelectLocation">
                            
                              <select class="form-control select2" id="trainSource" name="trainSource" >
                             <option value="">select...</option>
                                <c:forEach items="${dropDown.sourceDropDown}" var="sourceValue">
                                <option value="${sourceValue}"><c:out value="${sourceValue}"></c:out></option>
                                
                               </c:forEach> 
                                  <option value="OtherSource">Others</option> 
                              </select> 
                              
                            
                            </div>
 <div class="col-sm-7" id="trainSourceInputLocation" style="display:none">
 <input type="text"  class="form-control" id="trainSource1" name="trainSource" placeholder="Source"/>      
 </div>
                        </div> 



                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Destination<span class="requiredField"> *</span><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7" id="trainDestinationSelectLocation">
<select class="form-control select2 othersOption" id="trainDestination" name="trainDestination">
                             <option value="">select...</option>
                                <c:forEach items="${dropDown.destinationDropDown}" var="destinationValue">
                                <option value="${destinationValue}"><c:out value="${destinationValue}"></c:out></option>
                                
                               </c:forEach> 
                                  <option value="OtherDestination">Others</option> 
                              </select>                           
                               </div>
                              
 <div class="col-sm-7" id="trainDestinationInputLocation" style="display:none">
 <input type="text"  class="form-control" id="trainDestination1" name="trainDestination" placeholder="Destination"/>      
 </div>                
                    </div>


                    </div>
                     <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Mobile No<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  class="form-control numbersOnly" id="trainMob" name="trainMob" placeholder="Mobile Number" maxlength="10">
                            </div>
                        </div>



                    </div>
                </div>
            <!-- ======================End Row @ 3==================== -->
   <!-- return source and return destination row starts -->
    <div class="row" style="display:none" id="trainReturnTravelDateDiv">
    
    <div class="col-sm-8 col-md-4">
                        <div class="form-group">
                            <div class="col-sm-5">
                                <label for="inputEmail1" class="control-label">Return Date<span class="requiredField">*</span></label>
                            </div>
                                <div class="col-sm-7">
                                <div class="input-group date train_return_date_form_datetime-component">
                                    <input class="form-control" readonly="" size="16" id="trainReturnTravelDate" name="trainReturnTravelDate" type="text" placeholder="Return Date">
                                            <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                            </span>
                                  </div>
                                            
                                    
                                </div>
                                </div>
                        </div>
    
    
                   
   <div class="col-sm-8 col-md-4">
<div class="form-group"> 
    <div class="col-sm-5">
        <label> Return Source <span class="requiredField">*</span></label>
    </div>
    <div class="col-sm-7" id="trainReturnSourceSelectLocation">
      <!-- <input type ="text" class="form-control" id="trainReturnSource" name="trainReturnSource" placeholder="Return Source"> -->
   <select class="form-control select2" id="trainReturnSource" name="trainReturnSource" >
                             <option value="">select...</option>
                                <c:forEach items="${dropDown.sourceDropDown}" var="sourceValue">
                                <option value="${sourceValue}"><c:out value="${sourceValue}"></c:out></option>
                                
                               </c:forEach> 
                                  <option value="OtherSource">Others</option> 
   </select>  
   
    </div>
 <div class="col-sm-7" id="trainReturnSourceInputLocation" style="display:none">
 <input type="text"  class="form-control" id="trainReturnSource1" name="trainReturnSource" placeholder="Return Source"/>      
 </div>
</div> 
</div>



<div class="col-sm-8 col-md-4">
<div class="form-group"> 
 <div class="col-sm-5">
        <label>Return Destination<span class="requiredField">*</span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7" id="trainReturnDestinationSelectLocation">
   <!--  <input type ="text" class="form-control" id="trainReturnDestination" name="trainReturnDestination" placeholder="Return Destionation" > -->
 <select class="form-control select2" id="trainReturnDestination" name="trainReturnDestination" >
                             <option value="">select...</option>
                                <c:forEach items="${dropDown.sourceDropDown}" var="sourceValue">
                                <option value="${sourceValue}"><c:out value="${sourceValue}"></c:out></option>
                                
                               </c:forEach> 
                                  <option value="OtherDestination">Others</option> 
                              </select> 
</div>
<div class="col-sm-7" id="trainReturnDestinationInputLocation" style="display:none">
 <input type="text"  class="form-control" id="trainReturnDestination1" name="trainReturnDestination" placeholder="Return Destination"/>      
 </div>

</div>
</div> 
 </div>            
            
            <!-- return source and return destination row ends -->         
            
            
            <!-- =F=================Row 3=============== -->
                <div class="row">
                    <div class="col-sm-8 col-md-4">


                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Booking for<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2 bookingFor" id="trainBookingFor" name="trainBookingFor">
                              <option value="">select..</option>
                                <c:forEach items="${dropDown.bookingForDropDown}" var="bookingForValue">
                                <option><c:out value="${bookingForValue}"></c:out></option>
                                
                               </c:forEach> 
                              </select>
       
                        
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="train_modal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" onclick="trainExtenalCloseBtn()" class="close" type="button">�</button>
                                        <h4 class="modal-title">Booking For-External</h4>
                                    </div>
                                    <div class="modal-body">

                                        
                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Guest Type <span class="requiredField"> *</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="trainExternalGuestType" name="trainExternalGuestType">
                              <option value="">select..</option>
                                <c:forEach items="${dropDown.guestTypeDropDown}" var="guestTypeValue">
                                <option><c:out value="${guestTypeValue}"></c:out></option>
                                
                               </c:forEach> 

                              </select>
                      <span id="train_guesttype_error_msg"></span>

                                                </div>
                                            </div>
                                            
                                            
     <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Travel Class<span class="requiredField">*</span></label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="trainTravelClass" name="trainTravelClass">
                              
                                <option value="">select...</option>
                                <c:forEach items="${dropDown.travelClassDropDown}" var="travelClassValue">
                                <option><c:out value="${travelClassValue}"></c:out></option>
                                
                               </c:forEach> 
                                
                              </select>
                 <span id="train_travelclass_error_msg"></span>               
<span id="error_pax2"></span>
                                                </div>
                                            </div>                                        


                                            <div class="form-group">
                                                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Company Name<span class="requiredField">*</span></label>
                                                <div class="col-lg-10"  id="trainECDiv">
                                                <input class="form-control" id="trainExteranalCompany" name="trainExternalCompany"/>
                                              <span id="train_companyname_error_msg"></span>
                                                <p class="help-block">Example.i.e Abc Pvt Ltd ( Please provide Guest's Company Name)</p>
                                                   <!-- <select class="form-control" id="trainExteranalCompany" name="trainExternalCompany" >
                              
                                <option value="">select...</option>
                                <c:forEach items="${dropDown.companyDropDown}" var="companyValue">
                                <option><c:out value="${companyValue}"></c:out></option>
                               </c:forEach> 
                               <option value="Others">Others</option>
                                
                              </select>
 -->
                                                </div>
  <div class="col-lg-10" id="trainECInputDiv" style="display:none">
 <input type="text"  class="form-control" id="trainExternalCompany1" name="trainExternalCompany" placeholder="Company Name"/>      
 </div>        
                         
                                            </div>
                                                 <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button type="button" class="btn btn-primary" onclick="trainExtenalBtn()">OK</button>
                                                </div>
                                            </div>
                                
                                    </div>

                                </div>
                            </div>
                        </div>
                            </div>
                    </div>
                        
                        
                    </div>
                    
                    <div class="col-sm-8 col-md-4"  id="trainremoveSpan">


                        <div class="form-group"> 
                          <div class="col-sm-5">
                                <label>Employee ID<span class="requiredField" > *</span><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" class="form-control trainEmpIdClass" maxlength="50" id="trainEmployeeID1" name="employeeID1" placeholder="Employee ID">
<!--                                    <a href="#" data-toggle="modal" data-target="#addTPax" id="addTP"><i class="fa fa-plus" aria-hidden="true"></i></a>                     
 -->                            
                            
                            </div>
                            
      <!--start of modal  -->
<!-- <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addTPax" class="modal fade">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>
                                        <h4 class="modal-title">Add Multiple Pax</h4>
                                    </div>
                                    <div class="modal-body">
 <div class="trainPaxName"> 
 <div class="row">
 <div class="col-sm-8 col-md-4">
 <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee ID2 <span class="requiredField"> *</span><br>&nbsp;</label>
                            </div>
                           <div class="col-sm-7">
                              <input type ="text" class="form-control trainEmpIdClass" id="trainEmployeeID2" name="employeeID2" placeholder="Employee ID">
                            </div>
                    </div>

</div>
<div class="col-sm-8 col-md-4">


<div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Pax Name2 <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7" id="paxName">
                              <input type ="text" class="form-control alphabetOnly" id="trainPassenger2" name="passenger2" placeholder="Pax Name">
</div></div>
                        
                    
                    
                    
                    
                    
                    </div>
                    
                    
                    
                    <div class="col-sm-8 col-md-4">
 <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee Age2 <span class="requiredField"> *</span><br>&nbsp;</label>
                            </div>
                           <div class="col-sm-7">
                              <input type ="text" class="form-control "  name="age2" placeholder="Age">
                            </div>
                    </div>

</div>
                    
                    
                    
                    
                    
                    </div></div>
 
     <br>                                
<div class="form-group">
                  <div class="col-sm-5">
                                                    <button type="button" class="btn btn-primary" onclick="addPax('trainEmpIdClass','train')">Add</button>
                                                </div>
                   <div class="col-sm-5">
                                                    <button type="button" class="btn btn-primary isMultiRequest" data-dismiss="modal" id="isMultiRequest" name="isMultiRequest" value="false">Ok</button>
                                                </div>
                                            </div>
                                
                                    </div>

                                </div>
                            </div>
                        </div>
 -->
<!-- end of modal -->                    
                                                  
                                    <!--  <input type="hidden" class="totalPassengers" name="totalPassengers" id="airTotalPax" value="2" style="display: none;">  -->  
                            
                        </div>





                    </div>
                    
                    <div class="col-sm-8 col-md-4">


                        <div class="form-group"> 
                        
                        <div class="col-sm-5">
                                <label>Pax Name<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" maxlength="50" class="form-control alphabetOnly" id="trainPassenger1" name="passenger1" placeholder="Pax Name">
       <a href="#" data-toggle="modal" data-target="#addTPax" id="addTP"><i class="fa fa-plus fa-stylecs" aria-hidden="true">Add Pax</i></a>                        
                          
                            </div>
                 <!-- modal start  -->           
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addTPax" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>
                                        <h4 class="modal-title">Add Multiple Pax</h4>
                                    </div>
                                    <div class="modal-body">
 <div class="trainPaxName"> 
 <div class="row">
<!--  <div class="col-sm-8 col-md-4">
 <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee ID2 <span class="requiredField"> *</span><br>&nbsp;</label>
                            </div>
                           <div class="col-sm-7">
                              <input type ="text" class="form-control trainEmpIdClass" id="trainEmployeeID2" name="employeeID2" placeholder="Employee ID">
                            </div>
                    </div>
</div> -->
<div class="col-sm-8 col-md-10">
<div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Pax Name2 <!-- <span class="requiredField"> *</span> --></label>
                            </div>
                            <div class="col-sm-7" id="paxName">
                              <input type ="text" maxlength="50" class="form-control alphabetOnly" id="trainPassenger2" name="passenger2" placeholder="Pax Name">
</div></div>
                    </div>
<!--                     <div class="col-sm-8 col-md-4">
 <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Employee Age2 <span class="requiredField"> *</span><br>&nbsp;</label>
                            </div>
                           <div class="col-sm-7">
                              <input type ="text" class="form-control "  name="age2" placeholder="Age">
                            </div>
                    </div>

</div> -->
          </div>
          
          <br/>
          </div>
     <br>                                
<div class="form-group">
                  <div class="col-sm-5">
                                                    <button type="button" class="btn btn-primary" onclick="addPax('trainEmpIdClass','train')">Add</button>
                                                </div>
                   <div class="col-sm-5">
                                                    <button type="button" class="btn btn-primary isMultiRequest"  id="isTMultiRequest" name="isMultiRequest" value="false" data-dismiss="modal">Ok</button>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               <!-- end modal -->
      <input type="hidden" class="totalPassengers" name="totalPassengers" id="airTotalPax" value="2" style="display: none;">  
                    </div>
                </div>
            <!-- ======================End Row @ 3==================== -->

<!-- ==================Row hidden two way journey=============== -->
               <!--  <div class="row" id="train_two_way_hidden_row" style="display:none">
                    <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Return Date<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                            
                            <input id="travelDate" name="travelDate" class="form_datetime form-control dp1 dp1"  size="16" type="text" value="" id="trainReturnDate" name="trainReturnDate" />



                            </div>
                        </div>



                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                       
                       <div class="form-group"> 
                            <div class="col-sm-5">
                                <label> Return Source <span class="requiredField"> </span></label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" class="form-control" id="trainReturnSource" name="trainReturnSource" placeholder="Return Source" readonly="">
                            </div>
                        </div> 



                    </div>
                    
                    <div class="col-sm-8 col-md-4">
                        
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Return Destination<span class="requiredField"> </span><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">

                            <input type ="text" class="form-control" id="trainReturnDestination" name="trainReturnDestination" placeholder="Return Destionation" readonly="">
                   </div>
                    </div>


                    </div>
                </div> -->
            <!-- ======================End Row hidden two way journey==================== -->

            <!-- ======Row @6 start======= -->
                             <div class="row">
                             <div class="col-sm-8 col-md-4">
                   
                                <div class="form-group"> 
                                    <div class="col-sm-5">
                                        <label>Age<span class="requiredField"> *</span></label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input class="form-control numbersOnly" type="text" id="trainAge" name="age1" placeholder="Age" maxlength=2 >
                                   </div>
                                </div>
                            </div> 
                             
                       <div class="col-sm-8 col-md-4">
                         <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Department <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text"  value="${dropDown.department}" readonly="" class="form-control" id="trainDepartment" name="trainDepartment" >
                            </div>
                          </div>
                       </div>
                
                
                    <!--  <div class="col-sm-8 col-md-4" id="trTravel_Purpose" style="display:none">
                    <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Travel Purpose&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                              <input type ="text" class="form-control" id="trainTravelPurpose" name="trainTravelPurpose" readonly="" value="For office use only">
                                
                              
                            </div>
                        </div>

                        
                    </div>    -->   
                    
      <div class="col-sm-8 col-md-4" >

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Landline &nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control numbersOnly" id="trainLandline" name="trainLandline" maxlength="14" placeholder="Landline">
      <p class="help-block">Ex. 1662245635</p>
        
      
    </div>
</div>


</div>        
            
            
               
      </div>     
      <!-- row  6 ends -->
      <!-- row starts -->       
                  <div class="row">          
                             
                             
                                    <div class="col-sm-8 col-md-4">
                                        <div class="form-group"> 
                                        <div class="col-sm-6">
                                                <label>Avail Cab</label>
                                            </div>
                                            <div class="col-sm-6">
                                                  <input type="checkbox"  value="cab" id="trainAvailCab" name="trainAvailCab"/>
                                            </div>
                                    </div>
                                    </div>
                                    
                                    <div class="col-sm-8 col-md-4">
                                        <div class="form-group"> 
                                         <div class="col-sm-6">
                                                <label>Avail Accomodation</label>
                                            </div>
                                            <div class="col-sm-6">
                                                  <input type="checkbox"  value="HOTEL" id="trainAvailAccomodation" name="trainAvailAccomodation"/>
                                            </div>
                                    </div>
                                    </div>

                                     <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                    <label>Approved Attachment<!-- <span class="requiredField"> *</span> --></label>
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="trainApprovedAttachment" id="trainApprovedAttachment" >
                                                                    </span>
                                                                    <label class="error" for="trainApprovedAttachment" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                                                        <div id="trainFileName" style="color:black;">
                                                    </div>
                                                </div>
                                            </div>



</div>

   

                                    </div>
                                    
                                   
                                </div>

            <!-- ======Row  Ends========= -->
            <!-- ======Row @7 Start======= -->
                            <div class="row" id="train_avail_cab_row"  style="display:none;">
                                    <div class="col-sm-8 col-md-4">
                                        <div class="form-group"> 
                                            <div class="col-sm-5">
                                                <label>Pickup Point<span class="requiredField"> *</span></label>
                                            </div>
                                            <div class="col-sm-7 ">
                                              <input type ="text" maxlength="50" class="form-control" id="trainPickupPoint" name="trainPickupPoint" placeholder="Pickup Point">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-8 col-md-4" id="dropPT">
                                        <div class="form-group"> 
                                         <div class="col-sm-5">
                                                <label>Drop Point<span class="requiredField"> *</span></label>
                                            </div>
                                            <div class="col-sm-7">
                                              <input type ="text" maxlength="50" class="form-control" id="trainDropPoint" name="trainDropPoint" placeholder="Drop Point">
                                            </div>
                                    </div>
                                    </div>
                                    
                                    <div class="col-sm-8 col-md-4">

                                    <div class="form-group"> 
                                         <div class="col-sm-5">
                                                <label>Disposal<!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                                            </div>
                                            <div class="col-sm-7">
                                              <!-- <input type ="text" class="form-control" id="trainDisposal" name="trainDisposal" placeholder="Disposal"> -->
                                            
                                             <select class="form-control" id="trainDisposal" name="trainDisposal">
                                <option value="">select..</option>
                                   
                                <option>yes</option>
                                <option selected="selected">no</option>
                               
                                </select>
                                            </div>
                                    </div>
                                            
                                    </div>
                            </div>
                            
            <!-- ======Row @7 End========= -->

            <!-- ======new hidden avail accomodation row========= -->


            <div class="row" id="train_avail_accomodation_row" style="display:none;">
                                    <div class="col-sm-8 col-md-4">
                                        <div class="form-group"> 
                                            <div class="col-sm-5">
                                                <label>Location<span class="requiredField"> *</span></label>
                                            </div>
                                            <div class="col-sm-7" id="trainAccomodationSelectLocation">
                                            <!--   <input type ="text" class="form-control alphabetOnly" id="trainLocation" name="trainLocation" placeholder="Location"> -->
      <select class="form-control select2" id="trainLocation" name="trainLocation">
            <option value="">Select...</option>
          <c:forEach items="${dropDown.hotelLocationDropDown}" var="hotelLocationValue">
           <option><c:out value="${hotelLocationValue}"></c:out></option>  
          </c:forEach> 
          <option value="Others">Others</option> 
         </select>
                                             </div>
    <div class="col-sm-7" id="trainAccomodationInputLocation" style="display:none">
 <input type="text"  class="form-control" id="trainLocationInput" name="trainLocation" placeholder="Location" />      
 </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-8 col-md-4">
                                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Checkin Date & Time<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
            <div class="input-group date checkIn_train_dp">                    

                                        <input readonly=true"" size="16" type="text" id="trainCheckinDate" name="trainCheckinDate" class="form-control" placeholder="Travel Date" >
<span class="input-group-btn">
 <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
</span>
</div>
                            </div>
                        </div>
                                    </div>
                                    
                                    <div class="col-sm-8 col-md-4">

                                    <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Checkout Date & Time<span class="requiredField"> *</span><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
                     <div class="input-group date  checkOut_train_dp">                  
                                        <input readonly="true" size="16" type="text" id="trainCheckoutDate" name="trainCheckoutDate" class="form-control" placeholder="Checkout Date" >
<span class="input-group-btn">
  <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
</span>
</div>

                            </div>
                        </div>

                                            
                                    </div>
                            </div>
                            
            <!-- ======new hidden row End========= -->
            
            
             <!-- ======row @8 starts========= -->
             <div class="row">
                            <!--  <div class="col-sm-8 col-md-4">
                                        
                                        <div class="form-group"> 
                                                <div class="col-sm-5">
                                                    <label>Approved Attachment<span class="requiredField"> *</span></label>
                                                </div>
                                                <div class="col-sm-7" style="padding-left:6px">
                                                  <div class="controls col-md-9">
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                             <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i>Upload File</span>
                                                                 <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                                                  <input type="file" class="default" name="trainApprovedAttachment" id="trainApprovedAttachment" >
                                                                    </span>
                                                                    <label class="error" for="trainApprovedAttachment" generated="true"></label>
                                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                        </div>
                                                        <div id="trainFileName" style="color:black;">
                                                    </div>
                                                </div>
                                            </div>



</div>

   

                                    </div> --></div>
             <!-- ======row @8 ends========= -->
             
             
             
            <!-- ======Submit Button Start Here===== -->
              <div class="row">
                     <div class="col-sm-12">
                        <div class="form-group"> 
 <center><button class="btn btn-info " type="submit" id="submit" name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
                        </div>
                    </div>
            </div>
            </form>
            <!-- ======End Here===================== -->
            