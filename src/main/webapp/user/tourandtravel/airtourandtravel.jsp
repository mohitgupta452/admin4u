
<!--***************************Air Ticket User Pannel*************************************** -->


<div class="panel-body">
<div class="tab-content">
<div class="tab-pane active " id="air_requests">
<!-- Air ticket user requisition form start -->
<form action="AirTicketRequestServlet" method="POST" id="register-form" novalidate="novalidate" class="cmxform form-horizontal adminex-form" enctype="multipart/form-data" >

<!-- hidden input box for location -->
    <input type=text value="" class="hiddenLocation" name="airLocation" id="airLocation" style="display: none"> 
<!-- hidden input box for location -->


<div class="row">
    <div class="col-sm-8 col-md-4">
        <div class="form-group">
            <div class="col-sm-6">
                <label>One Way Journey<span class="requiredField"> *</span></label>
            </div>
            <div class="col-sm-6">
                <input type="radio"  value="one way" id="one" name="one" checked/>
            </div>
        </div>
    </div>

<div class="col-sm-8 col-md-4">
<div class="form-group"> 
    <div class="col-sm-6">
        <label>Two Way Journey</label>
    </div>
    <div class="col-sm-6">
          <input type="radio"  value="two way" id="two" name="one" data-toggle="modal" data-target="#air_two_way_modal"/>
<!--           <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="air_two_way_modal" class="modal fade">
    <div class="modal-dialog" style="min-width:60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>
                <h4 class="modal-title">Return journey</h4>
            </div>
            <div class="modal-body">
            

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label"></label>
                        <div class="col-lg-10">
                            <b>Message :</b></br>
                            1.Your journey source station will be preffered as destination for returning journey .</br>
                            2.Your journey destination station will be preffered as source for returning journey .

                        </div>
             
                    </div>
                    <div class="row">
                   
   <div class="col-sm-8 col-md-6">

<div class="form-group"> 
    <div class="col-sm-5">
        <label> Return Source <span class="requiredField"> </span></label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control" id="returnSource" name="returnSource" placeholder="Return Source">
    </div>
</div> 



</div>

<div class="col-sm-8 col-md-6">

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Return Destination<span class="requiredField"> </span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">

    <input type ="text" class="form-control" id="returnDestination" name="returnDestination" placeholder="Return Destionation" >
</div>
</div>


</div>  </div>               
                    
                    
                    
                         <div class="form-group">
                        <div class="col-lg-offset-5 col-lg-7">
                            <button type="button" class="btn btn-primary col-sm-2" data-dismiss="modal">OK</button>
                        </div>
                    </div>
        
            </div>

        </div>
    </div>
</div> -->

<!-- end of modal -->
                    </div>
                    </div>
                    </div>
                 <div class="col-sm-8 col-md-4" >
                        <div class="form-group"> 
                         <div class="col-sm-5">
                              <label>Cost Centre <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                            </div>
                            <div class="col-sm-7">
         <select class="form-control select2" id="centercode" name="centercode" style="overflow-x:auto !important;">
                              <option value="${dropDown.companyName}"><c:out value="${dropDown.companyName},${dropDown.locationName}"  /></option>
                                <c:forEach items="${dropDown.getCompanyNameList }" var="comp" varStatus="status">
                                <option value="${comp[0]},${comp[1]}" ><c:out value="${comp[0]},${comp[1]}"  /></option>
                                
                               </c:forEach>
                              </select>                    
                           </div>
                    </div>
                    </div>
                 

                </div>
                       
                                                
            <!--               ====row 2=====                     -->
                <div class="row">
                    
                    
                    <div class="col-sm-8 col-md-4">
                   
                        <div class="form-group"> 
                            <div class="col-sm-5">
                                <label>Travel Date<span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
         <div class="input-group date dp1">
                                <input size="16" type="text" id="travelDate" name="travelDate" class="form-control " readonly="true" placeholder="Travel Date">
                                <span class="input-group-btn">
                                      <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                                </span>
                           </div>
                    </div>
                        </div>
                    </div>
                    
                <div class="col-sm-8 col-md-4">
                        <div class="form-group"> 
                         <div class="col-sm-5">
                                <label>Preffered Time Slot <span class="requiredField"> *</span></label>
                            </div>
                            <div class="col-sm-7">
                              <select class="form-control select2" id="timeSlot" name="timeSlot">
                              <option value="">Select...</option>
                                
                                <c:forEach items="${dropDown.prefTimeSlotDropDown}" var="prefTimeSlotValue">
                                <option><c:out value="${prefTimeSlotValue}"></c:out></option>
                                
                               </c:forEach>
                              </select>
                            </div>
                        </div>

                       
                    </div>   
                 
                    
                    <div class="col-sm-8 col-md-4" >



<div class="form-group"> 
 <div class="col-sm-5">
        <label>Travel Purpose &nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control" id="travelPurpose" name="travelPurpose" readonly="" value="For office use only">
        
      
    </div>
</div>


</div>

<!--Start Popup -  On change event two way joiurney , Return Date journey will be popluated-->
<!-- <div class="col-sm-8 col-md-4" style="display:none" id="return_Date">
<div class="form-group">
    <div class="col-sm-5">
        <label for="inputEmail1" class="control-label">Return Date<span class="requiredField">*</span></label>
    </div>
        <div class="col-sm-7">
        <div class="input-group date return_date_form_datetime-component">
            <input class="form-control" readonly="" size="16" id="returnTravelDate" name="returnTravelDate" type="text">
                    <span class="input-group-btn">
                    <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
          </div>
                    
            
        </div>
        </div>
</div> -->
<!-- End Popup --> 
</div>


<!-- ==================Source Start=============== -->
<div class="row">
<div class="col-sm-8 col-md-4">
<div class="form-group"> 
    <div class="col-sm-5">
        <label>Source <span class="requiredField"> *</span></label>
    </div>
    <div class="col-sm-7"  id="sourceSelectLocation">
      <select class="form-control select2" id="source" name="source">
     <option value="">select...</option>
        <c:forEach items="${dropDown.airportDropDown}" var="airValue">
        <option value="${airValue}"><c:out value="${airValue}"></c:out></option>
       </c:forEach> 
      <option value="OtherSource">Others</option>  
      </select> 
      
    </div>
 <div class="col-sm-7" id="sourceInputLocation" style="display:none">
 <input type="text"  class="form-control" id="source1" name="source" placeholder="Source"/>      
 </div>
</div> 
</div>

<div class="col-sm-8 col-md-4">

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Destination<span class="requiredField"> *</span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7" id="destinationSelectLocation">
<select class="form-control select2" id="destination" name="destination">
     <option value="">select...</option>
       <c:forEach items="${dropDown.airportDropDown}" var="airValue">
        <option value="${airValue}"><c:out value="${airValue}"></c:out></option>
        
       </c:forEach> 
          <option value="OtherDestination">Others</option> 
      </select>                            
      </div>
  <div class="col-sm-7" id="destinationInputLocation" style="display:none">
 <input type="text"  class="form-control" id="destination1" name="destination" placeholder="Destination"/>      
 </div>
      
</div>
</div>
<div class="col-sm-8 col-md-4">
<div class="form-group"> 
 <div class="col-sm-5">
        <label>Mobile No<span class="requiredField"> *</span></label>
    </div>
    <div class="col-sm-7">
      <input type ="text"  maxlength="10"  class="form-control numbersOnly" id="mob" name="mob" placeholder="Mobile Number">
    </div>
</div>
</div>
</div>

<!--  return source and return destination row starts-->
<div class="row" id="return_Date" style="display:none">
    
    <div class="col-sm-8 col-md-4" >
<div class="form-group">
    <div class="col-sm-5">
        <label for="inputEmail1" class="control-label">Return Date<span class="requiredField">*</span></label>
    </div>
        <div class="col-sm-7">
        <div class="input-group date return_date_form_datetime-component">
            <input class="form-control" readonly="" size="16" id="returnTravelDate" name="returnTravelDate" type="text" placeholder ="Return Date">
                    <span class="input-group-btn">
                    <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
                    </span>
          </div>
                    
            
        </div>
        </div>
</div>
    
    
                   
   <div class="col-sm-8 col-md-4">
   <div class="form-group"> 
   <div class="col-sm-5">
   <label> Return Source <span class="requiredField">* </span></label>
    </div>
    <div class="col-sm-7"  id="returnSourceSelectLocation">
   <!--  <input type ="text" class="form-control" id="returnSource" name="returnSource" placeholder="Return Source"> -->
            <select class="form-control select2" id="returnSource" name="returnSource">
            <option value="">select...</option>
    <c:forEach items="${dropDown.airportDropDown}" var="airValue">
        <option value="${airValue}"><c:out value="${airValue}"></c:out></option>
    </c:forEach> 
      <option value="OtherSource">Others</option>  
      </select> 
    </div>
 <div class="col-sm-7" id="returnSourceInputLocation" style="display:none">
 <input type="text"  class="form-control" id="returnSource1" name="returnSource" placeholder="Return Source"/>      
 </div>
    </div> 
    </div>

<div class="col-sm-8 col-md-4">
<div class="form-group"> 
 <div class="col-sm-5">
 <label>Return Destination<span class="requiredField">*</span><br>&nbsp;</label>
 </div>
 <div class="col-sm-7" id="returnDestinationSelectLocation">
<!-- <input type ="text" class="form-control" id="returnDestination" name="returnDestination" placeholder="Return Destionation" > -->
  <select class="form-control select2" id="returnDestination" name="returnDestination">
     <option value="">select...</option>
        <c:forEach items="${dropDown.airportDropDown}" var="airValue">
        <option value="${airValue}"><c:out value="${airValue}"></c:out></option>
       </c:forEach> 
      <option value="OtherDestination">Others</option>  
    </select> 
</div>
<div class="col-sm-7" id="returnDestinationInputLocation" style="display:none">
 <input type="text"  class="form-control" id="returnDestination1" name="returnDestination" placeholder="Return Destination"/>      
 </div>
</div>
</div>  

</div>               
                    


<!--  return source and return destination row ends-->

<!-- ====================== Source End==================== -->
<!-- ==================Booking For Row Start =============== -->
<div class="row">
<div class="col-sm-8 col-md-4">


<div class="form-group"> 
 <div class="col-sm-5">
        <label>Booking for<span class="requiredField" > *</span></label>
    </div>
    <div class="col-sm-7">
      <select class="form-control select2 bookingFor" id="bookingFor" name="bookingFor">
      <option value="">select..</option>
        <c:forEach items="${dropDown.bookingForDropDown}" var="bookingForValue">
        <option><c:out value="${bookingForValue}"></c:out></option>
        
       </c:forEach>
      </select>



<!--start of modal  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="air_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" onclick="airExtenalCloseBtn()" class="close" type="button">�</button>
                <h4 class="modal-title">Booking For-External</h4>
            </div>
            <div class="modal-body">
                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Guest Type<span class="requiredField"> *</span></label>
                        <div class="col-lg-10">
                            <select class="form-control" id="airExternalGuestType" name="airGuestType">
      <option value="">select..</option>
        <c:forEach items="${dropDown.guestTypeDropDown}" var="guestTypeValue">
        <option><c:out value="${guestTypeValue}"></c:out></option>
        
       </c:forEach>
      </select>
        <span id="air_guesttype_error_msg"></span>
                        </div>
     
                    </div>


                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Company Name<span class="requiredField"> *</span></label>
                        <div class="col-lg-10" id="airECDiv">
                         <input class="form-control" id="airExteranalCompany" name="airExternalCompany" maxlength="50"/>
                             <span id="air_companyname_error_msg"></span>
                                                <p class="help-block">Example.i.e Abc Pvt Ltd ( Please provide Guest's Company Name)</p>
                            <!-- <select class="form-control" id="airExteranalCompany" name="airExternalCompany" >
      
        <option value="">select...</option>
        <c:forEach items="${dropDown.companyDropDown}" var="companyValue">
        <option><c:out value="${companyValue}"></c:out></option>
        
       </c:forEach>
       <option value="Others">Others</option>
        
      </select> -->

                        </div>
   
  <div class="col-lg-10" id="airECInputDiv" style="display:none">
 <input type="text"  class="form-control" id="airExternalCompany1" name="airExternalCompany" placeholder="Company Name"/>      
 </div>                        
             
                    </div>
                         <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="button" class="btn btn-primary" name="airOk" onclick="airExtenalBtn()">OK</button>
                        </div>
                    </div>
        
            </div>

        </div>
    </div>
</div>

<!-- end of modal -->
    </div>
</div>


</div>

<div class="col-sm-8 col-md-4" id="removeSpan">


<div class="form-group"> 


<div class="col-sm-5">
        <label>Employee ID <span class="requiredField" > *</span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control empIdClass" maxlength="50" id="employeeID1" name="employeeID1" placeholder="Employee ID">
     <!-- <a href="#" data-toggle="modal" data-target="#addPax" id="addP"><i class="fa fa-plus" aria-hidden="true"></i></a>                         
   -->  
    </div>

<!--start of modal  -->
<!-- <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addPax" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>
                <h4 class="modal-title">Add Multiple Pax</h4>
            </div>
            <div class="modal-body">
<div class="paxName"> 
<div class="row">

<div class="col-sm-8 col-md-4">


<div class="form-group"> 
 <div class="col-sm-5">
        <label>Employee ID2 <span class="requiredField"> *</span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control empIdClass" id="employeeID2" name="employeeID2" placeholder="Employee ID">
    </div>
</div></div>

<div class="col-sm-8 col-md-4">
<div class="form-group"> 
    <div class="col-sm-5">
        <label>Pax Name2 <span class="requiredField"> *</span></label>
    </div>
    <div class="col-sm-7" id="paxName">
      <input type ="text" class="form-control alphabetOnly" id="passenger2" name="passenger2" placeholder="Pax Name">
</div></div></div>

<div class="col-sm-8 col-md-4">
<div class="form-group"> 
    <div class="col-sm-5">
        <label>Age <span class="requiredField"> *</span></label>
    </div>
    <div class="col-sm-4" >
      <input type ="text" class="form-control" id="age2" name="age2" placeholder="Age">
</div></div></div>



</div></div>

<br>                                
<div class="form-group">
<div class="col-sm-5">
                            <button type="button" class="btn btn-primary" onclick="addPax('empIdClass','air')">Add</button>
                        </div>
<div class="col-sm-5">
                            <button type="button" class="btn btn-primary isMultiRequest" id="isMultiRequest" name="isMultiRequest" value="false" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
        
            </div>

        </div>
    </div>
</div> -->

<!-- end of modal -->                    
<!-- <input type="hidden" class="totalPassengers" name="totalPassengers" id="airTotalPax" value="2" style="display: none;">    -->
   
    
</div>

</div>

<div class="col-sm-8 col-md-4">


<div class="form-group"> 
  <div class="col-sm-5">
        <label>Pax Name <span class="requiredField"> *</span></label>
    </div>
    <div class="col-sm-7">
<input type ="text" class="form-control alphabetOnly" maxlength="50" id="passenger1" name="passenger1" placeholder="Pax Name">
<a href="#" data-toggle="modal" data-target="#addPax" id="addP"><i class="fa fa-plus fa-stylecs" aria-hidden="true">Add Pax</i></a>                         
   </div>
  
  
<!--start of modal  -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addPax" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">�</button>
                <h4 class="modal-title">Add Multiple Pax</h4>
            </div>
            <div class="modal-body">
<div class="paxName"> 
<div class="row">

<!-- <div class="col-sm-8 col-md-4">


<div class="form-group"> 
 <div class="col-sm-5">
        <label>Employee ID2 <span class="requiredField"> *</span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control empIdClass" id="employeeID2" name="employeeID2" placeholder="Employee ID">
    </div>
</div></div> -->

<div class="col-sm-8 col-md-10">
<div class="form-group"> 
    <div class="col-sm-5">
        <label>Pax Name2 <!-- <span class="requiredField"> *</span> --></label>
    </div>
    <div class="col-sm-7" id="paxName">
      <input type ="text" class="form-control alphabetOnly" maxlength="50" id="passenger2" name="passenger2" placeholder="Pax Name">
<span id="error_pax2"></span>
</div></div></div>

<!-- <div class="col-sm-8 col-md-4">
<div class="form-group"> 
    <div class="col-sm-5">
        <label>Age <span class="requiredField"> *</span></label>
    </div>
    <div class="col-sm-4" >
      <input type ="text" class="form-control" id="age2" name="age2" placeholder="Age">
</div></div></div> -->

</div>
<br/></div>

<br>                                
<div class="form-group">
<div class="col-sm-5">
                            <button type="button" class="btn btn-primary" onclick="addPax('empIdClass','air')">Add</button>
                        </div>
<div class="col-sm-5">
                            <button type="button" class="btn btn-primary isMultiRequest" id="isMultiRequest" name="isMultiRequest" value="false" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
        
            </div>

        </div>
    </div>
</div>
<!-- end of modal -->                    
<input type="hidden" class="totalPassengers" name="totalPassengers" id="airTotalPax" value="2" style="display: none;"> 
   
</div>

</div>
</div>
<!-- ======================End Row @ 3==================== -->


<!-- ==================Row hidden two way journey=============== -->
<!-- <div class="row" id="two_way_hidden_row" style="display:none">
<div class="col-sm-8 col-md-4">
<div class="form-group"> 
 <div class="col-sm-5">
        <label>Return Date<span class="requiredField"> *</span></label>
    </div>
    <div class="col-sm-7">
    
    <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" id="returnDate" name="returnDate" />


    </div>
</div>



</div>

<div class="col-sm-8 col-md-4">

<div class="form-group"> 
    <div class="col-sm-5">
        <label> Return Source <span class="requiredField"> </span></label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control" id="returnSource" name="returnSource" placeholder="Return Source" readonly="">
    </div>
</div> 



</div>

<div class="col-sm-8 col-md-4">

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Return Destination<span class="requiredField"> </span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">

    <input type ="text" class="form-control" id="returnDestination" name="returnDestination" placeholder="Return Destionation" readonly="">
</div>
</div>


</div>
</div> -->
<!-- ======================End Row hidden two way journey==================== -->

<!-- ======Row @6 start======= -->
<div class="row">
<div class="col-sm-8 col-md-4">

<div class="form-group"> 
    <div class="col-sm-5">
        <label>Age<span class="requiredField"> *</span></label>
    </div>
    <div class="col-sm-7">
        <input class="form-control numbersOnly" type="text" id="age" name="age1" placeholder="Age" maxlength=2>
   </div>
</div>


</div>     
<div class="col-sm-8 col-md-4">
<div class="form-group"> 
 <div class="col-sm-5">
        <label>Department <!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text"  value="${dropDown.department}" readonly="" class="form-control" id="department" name="department" >
    </div>
</div>
</div>
<div class="col-sm-8 col-md-4" >

<div class="form-group"> 
 <div class="col-sm-5">
        <label>Landline &nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="col-sm-7">
      <input type ="text" class="form-control numbersOnly" id="airLandline" name="airLandline" maxlength="14"  placeholder="Landline">
       <p class="help-block">Ex. 1662245635</p>
      </div>
   
    
</div>


</div>

             
        

</div>




<!-- row starts -->
<div class="row">


            <div class="col-sm-8 col-md-4">
                <div class="form-group"> 
                <div class="col-sm-6">
                        <label>Avail Cab</label>
                    </div>
                    <div class="col-sm-6">
                          <input type="checkbox" id="cab" name="avail_cab" value="CAB"/>
                    </div>
            </div>
            </div>
            
            <div class="col-sm-8 col-md-4">
                <div class="form-group"> 
                 <div class="col-sm-6">
                        <label>Avail Accomodation</label>
                    </div>
                    <div class="col-sm-6">
                          <input type="checkbox" id="hotel" name="avail_accomodation" value="HOTEL"/>
                    </div>
            </div>
            </div>
            <div class="col-sm-8 col-md-4">
                
                <div class="form-group"> 
                        <div class="col-sm-5">
                        

                        
                            <label>Approved Attachment<!-- <span class="requiredField"> *</span> --></label>
                            
                        </div>
                        <div class="col-sm-7" style="padding-left:6px">
                          <div class="controls col-md-9">
                                <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                     <span class="btn btn-default btn-file">
                                        <span class="fileupload-new" name="upload"><i class="fa fa-paper-clip"></i>Upload File</span>
                                         <span class="fileupload-exists"><i class="fa fa-undo"></i> Change File</span>
                                          <input type="file" class="default" name="upload" id="upload" >
                                            </span>
                        <label class="error" for="upload" generated="true"></label>
                                          <span class="fileupload-preview" style="margin-left:5px;"></span>
                                          <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                </div>
                                <div id="fileName" style="color:black;">
                                
                                </div>
                            </div>
                        </div>
                    </div>

        </div> 
            
         
</div>

<!-- ======Row @6 End========= -->



<!-- ======Row @7 Start======= -->
    <div class="row" id="avail_cab_row"  style="display:none;">
            <div class="col-sm-8 col-md-4">
                <div class="form-group"> 
                    <div class="col-sm-5">
                        <label>Pickup Point<span class="requiredField"> *</span></label>
                    </div>
                    <div class="col-sm-7 ">
                      <input type ="text" class="form-control" maxlength="50" id="pickup" name="pickup" placeholder="Pickup Point">
                    </div>
                </div>
            </div>
            
            <div class="col-sm-8 col-md-4" id="dropP">
                <div class="form-group"> 
                 <div class="col-sm-5">
                        <label>Drop Point<span class="requiredField"> *</span></label>
                    </div>
                    <div class="col-sm-7">
                      <input type ="text" class="form-control" maxlength="50" id="dropPoint" name="dropPoint" placeholder="Drop Point">
                    </div>
            </div>
            </div>
            
            <div class="col-sm-8 col-md-4">

            <div class="form-group"> 
                 <div class="col-sm-5">
                        <label>Disposal<!-- <span class="requiredField"> *</span> --><br>&nbsp;</label>
                    </div>
                    <div class="col-sm-7">
                     <!-- <input type ="text" class="form-control" id="disposal" name="disposal" placeholder="Disposal"> --> 
                      <select class="form-control" id="disposal" name="disposal">
        <option value="">select..</option>
           
        <option>yes</option>
        <option selected="selected">no</option>
       
        </select>
                      
                      
                    </div>
            </div>
                    
            </div>
    </div>
    
<!-- ======Row @7 End========= -->

<!-- ======new hidden avail accomodation row========= -->


<div class="row" id="avail_accomodation_row" style="display:none;">
            <div class="col-sm-8 col-md-4">
                <div class="form-group"> 
                    <div class="col-sm-5">
                        <label>Location<span class="requiredField"> *</span></label>
                    </div>
                    <div class="col-sm-7" id="accomodationSelectLocation">
         <select class="form-control select2" id="airlocation" name="airlocation">
            <option value="">Select...</option>
          <c:forEach items="${dropDown.hotelLocationDropDown}" var="hotelLocationValue">
       <option><c:out value="${hotelLocationValue}"></c:out></option>  
          </c:forEach> 
          <option value="Others">Others</option> 
         </select>
                     <!--  <input type ="text" class="form-control alphabetOnly" id="location" name="location" placeholder="Location"> -->
                    </div>
   <div class="col-sm-7" id="accomodationInputLocation" style="display:none">
 <input type="text"  class="form-control" id="airlocation1" name="airlocation" placeholder="Location"/>      
 </div>
                </div>
            </div>
            
            <div class="col-sm-8 col-md-4">
                <div class="form-group"> 
    <div class="col-sm-5">
        <label>Checkin Date & Time<span class="requiredField" > *</span></label>
    </div>
    <div class="col-sm-7">
     <div class="input-group date checkIn_dp">
        <input size="16" type="text" readonly="true" id="checkinDate" name="checkinDate" class="form-control" placeholder="Checkin Date">
       <span class="input-group-btn">
 <button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
        </span>
    </div>
    </div>
</div>
            </div>
            
            <div class="col-sm-8 col-md-4">

            <div class="form-group"> 
    <div class="col-sm-5">
        <label>Checkout Date & Time<span class="requiredField"> *</span><br>&nbsp;</label>
    </div>
    <div class="col-sm-7">
     <div class="input-group date checkOut_dp">
        <input size="16" type="text" readonly="true" id="checkoutDate" name="checkoutDate" class="form-control" placeholder="Checkout Date">
  <span class="input-group-btn">
<button type="button" class="btn btn-primary date-set"><i class="fa fa-calendar"></i></button>
  </span> 
    </div>
    </div>
</div>

                    
            </div>
    </div>
    
<!-- ======new hidden row End========= -->

<!-- ======Row @8 Starts========= -->
<div class="row">
<!-- Approval Attachement content previously -->
</div>



<!-- ======Row @8 End========= -->

<!-- ======Submit Button Start Here===== -->
<div class="row">
<div class="col-sm-12">
<div class="form-group"> 
<center><button class="btn btn-info " type="submit" id="submit" name="action" value="GENERATED"><i class="fa fa-hand-o-right"></i> Submit</button></center>
</div>
</div>
</div>
</form>


<!-- ======End Here===================== -->
