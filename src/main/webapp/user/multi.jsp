<%@page import="com.admin4u.views.TourAndTravelBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="javax.inject.Inject"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<t:layout>
<jsp:attribute name="header">
<style>
     .boxx
     { 
     
        display : none; 
     }

     .requiredField 
    {
       color: red;
    }
   
    
  .datepicker
    {
     z-index:9999!important;
     }

</style>
   <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datepicker/css/bootstrap-datepicker2.css">
 
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-timepicker/css/timepicker.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-colorpicker/css/colorpicker.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" type="text/css" href="assets/js/bootstrap-datetimepicker/css/datetimepicker-custom.css">
  
<!--dynamic table-->
  <link href="assets/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="assets/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="assets/js/data-tables/DT_bootstrap.css" />
  <link href="assets/css/table-responsive.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-fileupload.min.css" />

    </jsp:attribute>
    
    <jsp:attribute name="extrajs">
    
    <script type="text/javascript" src="assets/js/bootstrap-datepicker/js/bootstrap-datepicker2.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/js/pickers-init.js"></script>
<script src="assets/js/pickers-init2.js"></script>
   
    <!--dynamic table-->
<script type="text/javascript" language="javascript" src="assets/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/data-tables/DT_bootstrap.js"></script>
<script src="assets/js/dynamic_table_init.js"></script>
<!--pickers plugins-->
<script src="assets/js/customJS/admintable.js"></script>
<script src="assets/js/customJS/utility.js"></script>
<script>
$(".opendialog-reject").click(function () {
    $('#actionid').val($(this).data('id'));
    $('#admin_modal_reject').modal('show');
});
</script>
<script>
$(".opendialog").click(function () {
    $('#actionId').val($(this).data('id'));
    $('#admin_modal_assign').modal('show');
});
</script>
<script>
$(".opendialog-accept").click(function () {
    $('#actionID').val($(this).data('id'));
    $('#admin_modal_accept').modal('show');
});
</script>
    </jsp:attribute>
    <jsp:body>
         
         <div class="row">
         
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
          Admin

        </header>
        <div class="panel-body">
        <div class="adv-table">
                <section id="flip-scroll">
        <table class="display table table-bordered " id="hidden-table-info">
        <thead class="cf">
        <tr>
            <th>Request ID/Workorder</th>
            <th class="hidden-phone">Request By</th>
            <th class="hidden-phone">Request Type</th>
            <th class="hidden-phone">Request Status/Priority</th>
            <th>Request For</th>

            <th>Close</th>
            <th>Action</th>
            <th>Assign</th>


        </tr>
        </thead>
        <tbody>
        <c:forEach items="#{admin.ADMINDataList}" var="request1">
   <tr class="<c:if test="${request1['isRead']==false}"> <c:out value="gradeX"/></c:if> <c:if test="${request1['isRead']==true}"> <c:out value="seen"/></c:if>"  id="rid${request1['actionId']}">
            <td><a class="showRequestDataModal"href="javascript:;"  onclick="callShowRequestData(<c:out value="${request1['requestID']}">
            </c:out>, '<c:out value="${request1['service']}" />')"><c:out value="${request1['requestID']}"></c:out>
            <c:if test="${not empty request1['workorder']}">/<c:out value="${request1['workorder']}"></c:out></c:if></a></td>
              <td class="hidden-phone"><c:out value="${request1['requester']}" /> </td>
            <td class="center hidden-phone"><c:out value="${request1['requestType']}" /></td>
            <td class="center hidden-phone"><c:out value="${request1['status']}"/>
            <c:if test="${not empty request1['priority']}">/<c:out value="${request1['priority']}"/></c:if></td>
<td><c:out value="${request1['service']}" /></td>

 <td><button class="btn btn-warning opendialog-reject <c:if test="${request1['isComplete']=='true'}"> <c:out value="disabled "/></c:if>" type="button" data-toggle="modal" data-id="${request1['actionId']}" ><i class="fa fa-hand-o-right"></i> Close</button></td>
 <td>
 <c:if test="${admin.approvalProcessList.contains(request1['service'])}">
 <button class="btn btn-info bookTicketButton <c:if test="${request1['status']!='PENDING'}"> <c:out value="disabled "/></c:if>" data-toggle="modal" type="button" data-target="#admin_request_user_cnf_modal${request1['actionId']}"><i class="fa fa-hand-o-right"></i> Book Ticket</button>
 </c:if>
 <c:if test="${admin.nonApprovalProcessList.contains(request1['service'])}">
 <button class="btn btn-info opendialog-accept" type="button" data-toggle="modal" data-id="${request1['actionId']}">Accept</button>
 </c:if>
 </td>
 <td><button class="btn btn-info opendialog <c:if test="${request1['status']!='PENDING'}"> <c:out value="disabled "/></c:if>" type="button" data-toggle="modal" data-id="${request1['actionId']}" ><i class="fa fa-hand-o-right"></i> Assign</button></td>
      <!-- start of modal -->
 <!-- Modal -->
  <div class="modal fade" id="admin_request_user_cnf_modal${request1['actionId']}" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <form enctype="multipart/form-data" action="adminauth?requestID=${request1['requestID']}" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Details</h4>
        </div>
        <div class="modal-body">
       
        <div class="panel-body">
        
                                        <div class="adv-table">
                                        <div id="hidden-table-info_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                <!-- =====Passenger Confirmation Details Start==== -->
                                <div class="table">
    
<c:if test="${request1['serviceList'].contains('TRAINTICKET') ||request1['serviceList'].contains('AIRTICKET') || request1['serviceList'].contains('HOTEL_BOOKING') || request1['serviceList'].contains('GUEST_HOUSE_BOOKING') || request1['service']== 'CABDROP' || request1['service'] =='CABPICK'|| request1['service'] == 'CABDISPOSAL' || request1['service'] == 'CABAIRPORT' }">
      <%@include file="services/services_details.jsp"%>   
</c:if>
<!-- ends -->

 
    <div class="row">                              
        <div class="col-md-4"><label for="t_type">upload ticket&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                  <input type="file" name="upload[]" class="admin-input" multiple="multiple" >
       
                  <input type="hidden" class="admin-input" value="${request1['actionId']}" name="actionId" id="req${request1['actionId']}"></div>
          </div>  </div>
          <div class="row">
 <center><button type="submit" class="btn btn-primary" name="action" value="ADMIN_SERVE" >ticket upload</button></center>
       </div>
                                    </div>
                                    </div>
                                    </div>
        </div>
      
      </div>
      </form>
    </div>
    
  </div>
  
</td>
 </tr>

<!-- end of modal -->

         </c:forEach>
                   </tbody>
        </table>
</section>
        </div>
        </div>
        </section>
        </div>
        </div> 
        
	<!-- Modal -->
  <div class="modal fade" id="admin_modal_assign" role="dialog">
    <div class="modal-dialog">
    <form >
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assign</h4>
        </div>
        <div class="modal-body">
        <div class="form-group">
          <label>Select Spoc:   </label><select class="form-control admin-input" name="assignTo">
             <option>select....</option> 
             <option value="TRANSPORT_SPOC">Transport Spoc</option> 
             <option value="HOTEL_SPOC">Hotel Spoc</option>
             <option value="GUEST_HOUSE_SPOC">Guest House Spoc</option>
             <option value="HOUSEKEEPING_SPOC">House Keeping Spoc</option>
             <option value="REPAIRMANITAINCE_SPOC">Repair Maintenance Spoc </option>

          </select>
          </div>
          <input type="hidden" name="actionId" id="actionId" class="admin-input" />
          <input type="hidden" name="requestID" id="requestID" class="admin-input" />
        </div>
        <div class="modal-footer">
          <center><button type="button" class="btn btn-default" data-dismiss="modal" onclick="calladminServlet('ADMIN_FORWORD')">Assign</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
  


<!-- end of modal -->
<!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="admin_modal_reject" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Close</h4>
        </div>
         <form>
        <div class="modal-body">
       
        <table>
        <tr><td>
        <input type="hidden" name="actionId" id="actionid" class="admin"/>
          <label>Message: &nbsp;  </label></td><td style="margin-top:10px;"><textarea rows="2" cols="30" class="admin" name="responseMsg"></textarea></td>
          </tr>
</table>

        </div>
        <div class="modal-footer">
          <center><button type="button" class="btn btn-default" data-dismiss="modal" onclick="calladminServletReject('ADMIN_CLOSE')">Close</button></center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>
  


<!-- end of modal -->
	
	<!-- =======================================start show request data  modal===================== -->
    
    
    <!-- start of modal -->
                        
 <!-- Modal -->
  <div class="modal fade" id="requestDataModal" role="dialog">
    <div class="modal-dialog modal-lg">
    <form>
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Request Details</h4>
        </div>
        <div class="modal-body">
             </div>
        <br>
        <div class="modal-footer" style="margin-top: 150px;position:relative; ">
          <center><button type="button" class="btn btn-default" data-dismiss="modal">close</button></center>
        </div>
      </div>
      </form>
    </div>
  </div>
 


<!-- end of modal -->
    
    
    
    <!-- =====================================End ofshow request data  modal================= -->
    <!-- start of modal admin accept-->
                        
 <!-- Modal -->
  <div class="modal fade" id="admin_modal_accept" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Accept</h4>
        </div>
         <form>
        <div class="modal-body">
       
        <table>
        <tr><td>
        <input type="hidden" name="actionId" id="actionID" class="admin-accept"/>
          <label>Set Priority</label></td>
          <td style="margin-top:10px;"><select class="admin-accept" name="priority">
          <option value="">select...</option>
          <option value="HIGH">High</option>
          <option value="NORMAL">Normal</option>
          <option value="LOW">Low</option>          
          </select></td>
          <td>
          
          
          </td>
          </tr>
</table>

        </div>
        <div class="modal-footer">
          <center><button type="button" class="btn btn-default" data-dismiss="modal" onclick="calladminServletAccept('ADMIN_ACCEPT')">ACCEPT</button></center>
        </div>
        </form>
      </div>
      
    </div>
    
  </div>
  


<!-- end of modal -->
	
	<!-- =======================================start show request data  modal===================== -->
	                
    </jsp:body>
</t:layout>