/**
 * 
 */

/**
 * 
 */

function fnFormatDetails ( oTable, nTr )
{
    var aData = oTable.fnGetData( nTr );
    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
    sOut += '<tr><td>'+aData[1]+' '+aData[4]+'</td></tr>';
  
    sOut += '</table>';

    return sOut;
}

$(document).ready(function() {

   /* $('#dynamic-table').dataTable( {
        "aaSorting": [[ 4, "desc" ]]
    } );
*/
    /*
     * Insert a 'details' column to the table
     */

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#payment_hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[1, 'asc']]
    });
    
    
    $('#admin_hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[3, 'desc']]
    });

    
    $('#app_auth_hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[4, 'desc']]
    });
    
    
    $('#spoc_hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[3, 'desc']]
    });
    
 
} );