function fnFormatDetails ( oTable, nTr )
{
    var aData = oTable.fnGetData( nTr );
    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
    sOut += '<tr><td>'+aData[1]+' '+aData[4]+'</td></tr>';
  
    sOut += '</table>';

    return sOut;
}

$(document).ready(function() {

   /* $('#dynamic-table').dataTable( {
        "aaSorting": [[ 4, "desc" ]]
    } );
*/
    /*
     * Insert a 'details' column to the table
     */

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[1, 'asc']]
    });
    
    
    $('#admin_hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[3, 'desc']]
    });

    
    $('#app_auth_hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[4, 'desc']]
    });
    
    
    $('#spoc_hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [ 0 ] }
        ],
        "aaSorting": [[3, 'desc']]
    });
    
    

    
    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    //$(document).on('click','#hidden-table-info tbody td img',function () {
      //  var nTr = $(this).parents('tr')[0];
      //  if ( oTable.fnIsOpen(nTr) )
      //  {
            /* This row is already open - close it */
          //  this.src = "assets/images/details_open.png";
      //      oTable.fnClose( nTr );
      //  }
     //   else
      //  {
            /* Open this row */
           // this.src = "assets/images/details_close.png";
        //    oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
       // }
 //   } );
} );