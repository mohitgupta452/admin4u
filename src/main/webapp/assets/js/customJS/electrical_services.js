

// only number will be enter in the field
$('.numbersOnly').keyup(function () {
    if (this.value != this.value.replace(/[^0-9]/g, '')) {
       this.value = this.value.replace(/[^0-9]/g, '');
    }
});

//only alphabets are allowed in the field /[^a-z]/g
$('.alphabetOnly').keyup(function () {
    if (this.value != this.value.replace( /[^a-zA-Z ]/g, '')) {
       this.value = this.value.replace( /[^a-zA-Z ]/g, '');
    }
});


$(function() {
    // Setup form validation on the #register-form element
    $("#electrical_form").validate({
 rules: {
	
     username : "required",
     workstation:"required",
     category : "required",
     warranty : "required",
     description : "required",
     amc : "required"
     },
     
     // Specify the validation error messages
     messages: {
    	 
         username : "Please enter  name",
         workstation:"please enter workstation",
         category : "Please select category",
       
         warranty : "Please enter warranty",
         description : "Please enter problem description",
         amc : "Please select amc"
         
     }
     });

});
