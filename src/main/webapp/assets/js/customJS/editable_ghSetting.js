var count=0;
/*
function saveGH(count,action)
{
	var jsonData={row:count,
			action:action};
	$(".admin-gh").each(function() {
	       var name = $(this).attr("name");
	       var value = $(this).val();
	       jsonData[name] = value;
	   })
	 $.ajax({
         url:"GuestHouseInventoryServlet",
         type: 'POST',
         data: jsonData,
         dataType: "json",
         success: function (data,textStatus, $XHR) {
        	 
        	 console.log(data);
        	 
        	 var ghId= data["ghId"];
        	
        	 $('#ghSetting'+count).attr('data-id' ,ghId); 
        	
         }
	 });
}
var roomDetails="";
function ghSetting(id)
{
	var gHId=$('#'+id).data("id");
	
	console.log(gHId);
	
	var jsonData={action:"ROOMLIST",
			ghId:gHId};
	
	console.log(jsonData);
         $.ajax({
             url:"GuestHouseInventoryServlet",
             type: 'POST',
             data: jsonData,
             dataType: "json",
             success: function (data,textStatus, $XHR) {
            	       console.log("first" +data);
            	   roomDetails=data;  
            	  // alert(roomDetails);
            	   console.log(roomDetails);
            	   $('#myModal2').modal('show');
            	   setRoomDetails("singleOcupancy");
            	
             }
    	 });
	}

function setRoomDetails(occupency)
{
	console.log("roomdetails "+roomDetails+occupency);
	 $('#'+occupency+'tbl').html("");
	
	var roomHtml="";
	//alert(roomDetails);
    $.each(roomDetails, function(k,v){
    	
    	console.log(k,v)
 	   if(k===occupency)
 	 {
 	   for(var i in v){
 	   console.log(v[i].roomId);
 	   console.log(v[i].roomNumber);

      roomHtml =roomHtml+'<tr>'+'<td><label class="roomNo'+i+'">'+v[i].roomNumber+'</label></td>'
      var chkBxHtml="";
      for(i=0;i<=7;i++)
     {
     	 chkBxHtml=chkBxHtml+'<td class=""><input class ="ghroom_item'+i+'" type="checkbox" name="" value=""></td>'
      
 	 }
 	   roomHtml=roomHtml+chkBxHtml+'<td class=""><button id="inactiveItem'+i+'"  class="btn btn-warning">Delete</button></td></tr>'
 	   }
 	  
 		   }
 	   });  
    console.log("+"+roomHtml);
    
   $('#'+occupency+'tbl').prepend(roomHtml);
   $('#'+occupency).tab('show');

}

var GuestHouseInventory = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="textarea" rows="2" class="form-control admin-gh"style="width: 550px;" value="' + aData[0] + '" name="guestHouse'+count+'">';
                jqTds[1].innerHTML = '<input type="text" class="form-control small admin-gh" value="' + aData[1] + '"  name="single'+count+'">';
                jqTds[2].innerHTML = '<input type="text" class="form-control small admin-gh" value="' + aData[2] + '" name="double'+count+'">';
                jqTds[3].innerHTML = '<input type="text" class="form-control small admin-gh" value="' + aData[3] + '" name="tripple'+count+'">';
                jqTds[4].innerHTML = '<a class="edit"  name="action" onclick=saveGH('+count+',"SUBMIT")  href="javascript:void(0)" >Save</a>';
                jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
                // document.getElementById("ghSetting").disabled = true;
                jqTds[6].innerHTML = '<a href="javascript:void(0)" id ="ghSetting'+count+'" onclick="ghSetting(this.id)" data-id="" data-toggle="modal" class="btn btn-warning">Setting</a>';
          
            
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                document.getElementById("ghSetting").disabled = false;
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 5, false);
                oTable.fnUpdate('<a href="#myModal2" id ="ghSetting"  data-toggle="modal" class="btn btn-warning">Setting</a>' ,nRow ,6 ,false);
                oTable.fnDraw();
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                document.getElementById("ghSetting").disabled = false;
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                oTable.fnDraw();
            }

            var oTable = $('#editable-sample').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

            var nEditing = null;

            $('#editable-sample_new').click(function (e) {
                count++;
            	e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '',
                        '<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                nEditing = nRow;
            });

            $('#editable-sample a.delete').live('click', function (e) {
                e.preventDefault();

                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }

                var nRow = $(this).parents('tr')[0];
                oTable.fnDeleteRow(nRow);
            });

            $('#editable-sample a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#editable-sample a.edit').live('click', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                     Editing this row and want to save it 
                    saveRow(oTable, nEditing);
                    nEditing = null;
                } else {
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };

}();*/




$(document).ready(function(){
	$(document).on("click",'.setting',function(){
		
		$('singleOcupancytbl').html("");
		$('doubleOcupancytbl').html("");
		$('tripleOcupancytbl').html("");

	      $('#myModal2').modal('show');

		var url="GuestHouseInventoryServlet";
		//var buildingId=$('.ghAvailableReq').val();
		var action="ROOMLIST";
		var buildingId=$(this).val();
		console.log("building id_______________________________________________"+buildingId);

		var jsonData={
				ghId:buildingId,
				action:action,
				
		}
		
		
		//ajax to get available seat or cabin
		
		
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	  console.log(result['doubleOcupancy']);
		    	  var table="";
		    	  $('#singleOcupancytbl').html("");
		  		$('#doubleOcupancytbl').html("");
		  		$('#tripleOcupancytbl').html("");
		           $.each(result['singleOcupancy'],function(key,val){
		        		var isAc="checked",isTv="checked",isSofa="checked",isPhone="checked",isWifi="checked";
			        	   if(val['AC']===undefined)
			        		  isAc="Unchecked";
			        	   if(val['TV']===undefined)
				        		  isTv="Unchecked";
			        	   if(val['SOFA']===undefined)
				        		  isSofa="Unchecked";
			        	   if(val['PHONE']===undefined)
				        		  isPhone="Unchecked";
			        	   if(val['WI-FI']===undefined)
				        		  isWifi="Unchecked";
			        	   if(val['bed']===undefined)
			        		   val['bed']="NA";
			        	   
			    	  console.log("key"+key+"val"+val['roomNumber']);
			    	  table= '<tr><td><input type="text" data-id="'+val['roomId']+'"  class="form-control roomCode" value="'+val['roomNumber']+'"/></td><td><input type="text" data-id="'+val['roomId']+'" class="form-control updateBedType" value="'+val['bed']+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox" '+isAc+' class="form-control items" value="'+"AC"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox"  '+isTv+'  class="form-control items" value="'+"TV"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox"  '+isSofa+'  class="form-control items" value="'+"SOFA"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox" '+isPhone+' class="form-control items" value="'+"PHONE"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox" '+isWifi+'  class="form-control items" value="'+"WI-FI"+'"/></td><td><button type="button"  class="deleteButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Delete</button></td>'+'</tr>';
				    	  $('#singleOcupancytbl').append(table);
 	  
				    	  	console.log(table);

		            	//$('#availSeat').append('<option value="'+key+'">'+val+'</option>');
		            });
		           	table="";
		           	
		           $.each(result['doubleOcupancy'],function(key,val){
		        	   console.log("inside double occupacy");
		        		var isAc="checked",isTv="checked",isSofa="checked",isPhone="checked",isWifi="checked";
			        	   if(val['AC']===undefined)
			        		  isAc="Unchecked";
			        	   if(val['TV']===undefined)
				        		  isTv="Unchecked";
			        	   if(val['SOFA']===undefined)
				        		  isSofa="Unchecked";
			        	   if(val['PHONE']===undefined)
				        		  isPhone="Unchecked";
			        	   if(val['WI-FI']===undefined)
				        		  isWifi="Unchecked";
			        	   if(val['bed']===undefined)
			        		   val['bed']="NA";
			        	   
			    	  console.log("key"+key+"val"+val['roomNumber']);
			    	  table= '<tr><td><input type="text" data-id="'+val['roomId']+'"  class="form-control roomCode" value="'+val['roomNumber']+'"/></td><td><input type="text" data-id="'+val['roomId']+'" class="form-control updateBedType" value="'+val['bed']+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox" '+isAc+' class="form-control items" value="'+"AC"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox"  '+isTv+'  class="form-control items" value="'+"TV"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox"  '+isSofa+'  class="form-control items" value="'+"SOFA"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox" '+isPhone+' class="form-control items" value="'+"PHONE"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox" '+isWifi+'  class="form-control items" value="'+"WI-FI"+'"/></td><td><button type="button"  class="deleteButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Delete</button></td>'+'</tr>';

			    	  $('#doubleOcupancytbl').append(table);
	  				    	  

		            });
		           table="";
		           $.each(result['tripleOcupancy'],function(key,val){
				    	  console.log("key"+key);
				    	  			var isAc="checked",isTv="checked",isSofa="checked",isPhone="checked",isWifi="checked";
				        	   if(val['AC']===undefined)
				        		  isAc="Unchecked";
				        	   if(val['TV']===undefined)
					        		  isTv="Unchecked";
				        	   if(val['SOFA']===undefined)
					        		  isSofa="Unchecked";
				        	   if(val['PHONE']===undefined)
					        		  isPhone="Unchecked";
				        	   if(val['WI-FI']===undefined)
					        		  isWifi="Unchecked";
				        	   if(val['bed']===undefined)
				        		   val['bed']="NA";
				        	   
				    	  console.log("key"+key+"val"+val['roomNumber']);
				    	  table= '<tr><td><input type="text" data-id="'+val['roomId']+'"  class="form-control roomCode" value="'+val['roomNumber']+'"/></td><td><input type="text" data-id="'+val['roomId']+'" class="form-control updateBedType" value="'+val['bed']+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox" '+isAc+' class="form-control items" value="'+"AC"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox"  '+isTv+'  class="form-control items" value="'+"TV"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox"  '+isSofa+'  class="form-control items" value="'+"SOFA"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox" '+isPhone+' class="form-control items" value="'+"PHONE"+'"/></td><td><input data-id="'+val['roomId']+'" type="checkbox" '+isWifi+'  class="form-control items" value="'+"WI-FI"+'"/></td><td><button type="button"  class="deleteButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Delete</button></td>'+'</tr>';

				    	  $('#tripleOcupancytbl').append(table);
 	  

		            });
		           
		        	   
		           
		           
		           
		        }});
		
		
		
		
			});
        	
        });



$(document).ready(function(){
	$(document).on("click",'.items',function(){
			
		var ischecked=$(this).is(":checked");
		var roomId=$(this).data('id');
		var stuffName=$(this).val();

		var url="GuestHouseInventoryServlet";
		//var buildingId=$('.ghAvailableReq').val();
		var action="DELETEROOMSTUFF";
		if(ischecked===true)
			action="ADDROOMSTUFF"
			
		console.log("building id_______________________________________________");

		var jsonData={
				stuffName:stuffName,
				action:action,
				roomId:roomId,
				
				
		}
		
		
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	 
		    	  
		          
		        	   
		           
		           
		           
		        }});
		
		
		
		
			});
        	
        });


$(document).ready(function(){
	$(document).on("click",'.deleteButton',function(){
			
		var roomId=$(this).val();

		var url="GuestHouseInventoryServlet";
		//var buildingId=$('.ghAvailableReq').val();
		var action="DELETEROOM";
		var currentRow= $(this).closest('tr');
		
			
		console.log("building id_______________________________________________");

		var jsonData={
				action:action,
				roomId:roomId,
				
				
		}
  	 

		
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	 		    	  
		        	   		           currentRow.remove();
		           
		        }});
		
		
		
		
			});
        	
        });

$(document).ready(function(){
	$(document).on("change",'.updateBedType',function(){
			
		var roomId=$(this).data('id');
		var bedType=$(this).val();

		var url="GuestHouseInventoryServlet";
		//var buildingId=$('.ghAvailableReq').val();
		var action="UPDATEBEDTYPE";
		
			
		console.log("building id_______________________________________________");

		var jsonData={
				action:action,
				roomId:roomId,
				bedType:bedType,
				
				
		}
		
		
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	 		    	  
			         	showAlertMessage("SUCCESS",result['msg']);
  
		        	   		           
		           
		           
		        }});
		
		
		
		
			});
        	
        });

$(document).ready(function(){
	$(document).on("change",'.roomCode',function(){
			
		var roomId=$(this).data('id');
		var roomCode=$(this).val();

		var url="GuestHouseInventoryServlet";
		//var buildingId=$('.ghAvailableReq').val();
		var action="UPDATEROOMCODE";
		
			
		console.log("building id_______________________________________________");

		var jsonData={
				action:action,
				roomId:roomId,
				roomCode:roomCode,
				
				
		}
		
		
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	 		    	  
			         	showAlertMessage("SUCCESS",result['msg']);
			         	
  
		        	   		           
		           
		           
		        }});
		
		
		
		
			});
        	
        });


$(document).ready(function(){
	$(document).on("click",'.saveButton',function(){
		
		$("#addGhInventoryForm").validate({
	        rules: {
	            ghNameAddress: "required",
		single:{
			required:true,
			digits:true,
		},
		double:{
			required:true,
			digits:true,
		},
		tripple:{
			required:true,
			digits:true,
		},
	        },   
	        messages: {
	           ghNameAddress: "Please enter Guest House Name & Address",
	           single:{
	       		required:"Please enter single occupancy count",
	       		digits:"Only numbers are allowed",
	       	},
	       	double:{
	       		required:"Please enter double occupancy count",
	       		digits:"Only numbers are allowed",
	       	},
	       	tripple:{
	       		required:"Please enter tripple occupancy count",
	       		digits:"Only numbers are allowed",
	       	},
	        }
	    });

		
		
		console.log("Executed");
		//alert($('#addGhInventoryForm').valid());
		if($('#addGhInventoryForm').valid()){
		var url="GuestHouseInventoryServlet";
		//var buildingId=$('.ghAvailableReq').val();
		var action="SUBMIT";
		var guestHouse=$('#guestHouse'+count).val();
		var single=$('#single'+count).val();
		var double=$('#double'+count).val();
		var tripple=$('#tripple'+count).val();

		
			
		console.log("building id_______________________________________________");

		var jsonData={
				action:action,
				guestHouse:guestHouse,
				single:single,
				double:double,
				tripple:tripple,
				
				
		}
		
		
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	 		    	  
			        	showAlertMessage("SUCCESS","New Guest House Successfully Added");
		    	  console.log("succcess!!!!!!!!!")
		    	  $('.newRow').attr("readonly",true);
		    	  $('#sButton'+count).val(result['ghId']);
		        	   		           
		          
		           
		        }});
		
		 $(this).removeClass('saveButton');
         $(this).addClass('deleteGhouse');
         $(this).html("Delete");
		}
		
		
		
			});
        	
        });
$(document).ready(function(){
	$(document).on('click','#editable-sample_new',function(){
		count++;
		row='<tr class="odd">'
      +  '<td class=" sorting_1"><input type="text" class="form-control newRow"  name="ghNameAddress" id="guestHouse'+count+'"  /></td>'
      +  '<td class=" "><input type="text" class="form-control newRow" name="single" value="0" id="single'+count+'"  /></td>'
        +'<td class=" "><input type="text" class="form-control newRow" name="double" value="0" id="double'+count+'"  /></td>'
      +'  <td class="center "><input type="text" class="form-control newRow" name="tripple" value="0" id="tripple'+count+'"  /></td>'
      +'  <td class=" "><button type="button"   class="btn btn-warning saveButton">Save</button></td>'
       +' <td class=" "><button type="button" data-toggle="modal" id="sButton'+count+'"  class="btn btn-warning setting">Setting</button></td>'
       +' </tr>';
		
		$('#tableBody').prepend(row);
		console.log(row);
	});
	
	
	
});

$(document).ready(function(){
	$(document).on("click",'.deleteGhouse',function(){
			
		var buildingId=$(this).val();

		var url="GuestHouseInventoryServlet";
		//var buildingId=$('.ghAvailableReq').val();
		var action="DELETEGUESTHOUSE";
		var currentRow= $(this).closest('tr');
		
			
		console.log("building id_______________________________________________");

		var jsonData={
				action:action,
				buildingId:buildingId,
				
				
		}
  	 

		
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	 		    	  
		        	   		           currentRow.remove();
		           
		        }});
		
		
		
		
			});
        	
        });

