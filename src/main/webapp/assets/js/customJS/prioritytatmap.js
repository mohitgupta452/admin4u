
//only alphabets are allowed in the field /[^a-z]/g
$('.alphabetOnly').keyup(function () {
    if (this.value != this.value.replace( /[^a-zA-Z ]/g, '')) {
       this.value = this.value.replace( /[^a-zA-Z ]/g, '');
    }
});



$(function() {
        // validate ticketBookingForm  form on keyup and submit
        $("#priorityTatMap").validate({
            rules: {
                priority: "required",
		        tat:"required",
		
		
		},
               
                
            messages: {
                priority: "Please Enter Priority",
		tat:"Please Enter TAT",
		
            },
		
        });

});

	
	$(document).on('click','.editButton',function(){
		
		
  	 $('#editPriority').val( $(this).closest('tr').children().eq(1).html());
  	 $('#editTat').val( $(this).closest('tr').children().eq(2).html());
  	$('#priorityTatMapID').val($(this).closest('tr').children().eq(0).html());
  	
  	var editServiceValue=$(this).closest('tr').children().eq(3).html();
  	$('#editService').val(editServiceValue);

  	console.log($(this).closest('tr').children().eq(3).html());

console.log($('#editService').val());
			
			
		

	});
	
	


function callUpdatePriorityTatMapServlet(){   

		var priority=$('#editPriority').val();
		var tat=$('#editTat').val();
		var priorityTatMapID=$('#priorityTatMapID').val();
		var action="UPDATE";
		var service=$('#editService').val();


	jsonData={
			 priority:priority,
			 turnAroundTime:tat,
			 priorityTatMapID:priorityTatMapID,
			 action:action,
			 service:service
			
	 }
	
	 
   $.ajax({
       url: "PriorityTatMapServlet",
       type: 'POST',
       data: jsonData,
       dataType: "json",
       success: function (data,textStatus, $XHR) {
    	 
  	var parsedJson=data;
  
  $('#rid'+parsedJson['id']).children().eq(1).html(parsedJson['priority']);
  $('#rid'+parsedJson['id']).children().eq(2).html(parsedJson['tat']);
  $('#rid'+parsedJson['id']).children().eq(3).html(parsedJson['service']);

	showAlertMessage("SUCCESS"," Priority Tat Successfully Updated");


        	},
       error: function (data, textStatus, $XHR) {
        
    	   
    		showAlertMessage("FAILURE","Failed To Update");

        	},
         });  
    }



function callAddPriorityTatMapServlet(){   
	
if($('#priorityTatMap').valid()==true)
{

	var priority=$('#priority').val();
	var tat=$('#tat').val();
	var action="ADDNEWPRIORITY";
	var service=$('#service').val();


jsonData={
		 priority:priority,
		 tat:tat,
		 action:action,
		 service:service
		
 }

 
$.ajax({
   url: "PriorityTatMapServlet",
   type: 'POST',
   data: jsonData,
   dataType: "json",
   success: function (data,textStatus, $XHR) {
	  	var parsedJson=data;
	  
	
		$('#myTable tbody').append("<tr><td>"+parsedJson['id']+"</td><td>"+parsedJson['priority']+"</td><td>"+parsedJson['tat']+"</td><td>"+parsedJson['service']+'</td><td><button type="button" class="btn btn-info editButton" data-target="#editTatModal" data-toggle="modal" ><i class="fa fa-hand-o-right"></i> Edit</button></td></tr>') 

	   $('#priority').val("");
	   $('#tat').val("");
	   
		showAlertMessage("SUCCESS","New Priority Successfully Added");

    	},
   error: function (data, textStatus, $XHR) {

		showAlertMessage("FAILURE"," Failed To Add New Priority");

	   

    	},
     });  
}

}


