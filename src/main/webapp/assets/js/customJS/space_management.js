$(document).ready(function(){
	$(document).on("change",'#spaceStationCat',function(){
		var url="SMSpaceAllocationServlet";
		var floorId=$('#buildingFloor').val();
		var seatType=$(this).val();
		var action="SEATINFO";
		var jsonData={
				floorId:floorId,
				seatType:seatType,
				action:action,
		}
		//$('#seatDetailTableTbody').html("");
		 $.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		            $('#seatDetailTableTbody').html('<tr><td>1</td><td>'+result['buildingFloor']+'</td><td>'+result['seatType']+'</td><td class="numeric">'+result['total']+'</td>'
                           +' <td class="numeric">'+result['assigned']+'</td>'
                            +'<td class="numeric">'+result['available']+'</td>'
                            +'<td class=""><button>Allocate Space</button></td></tr>' );
		            console.log(""+floorId+seatType+result['available']);
                    

		            
		        }});
		
		
		//ajax to get available seat or cabin
		action="GETAVAILABLESPACE";
		
		jsonData={
				floorId:floorId,
				seatType:seatType,
				action:action,
		}
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	  console.log("second methodd");
	            	$('#availSeat').html('<option value="">Select...</option>');

		            $.each(result,function(key,val){
				    	  console.log("key"+key);

		            	$('#availSeat').append('<option value="'+key+'">'+val+'</option>');
		            });
		        }});
		
		
		
		
			});
        	
        });
		
		

$(document).ready(function(){
	$(document).on("blur",'.getEmpDataByIdClass',function(){
		console.log("called");
		var url="UtilityServlet?action=getEmpDetail&employeeId="+$(this).val();
		var employeeId=$(this).attr('id');
		/*console.log(employeeId);
		var passengerId=employeeId.replace("trainEmployeeID","trainPassenger");
		console.log(passengerId);*/
        $.getJSON(url, function(result){
    		console.log("workings"+result['displayName']);
    		$('#employeeName').val(result['displayName']);
           $('#desgination').val(result['designation']);
           $('#company').val(result['company']);
           $('#department').html('<option value="'+result['department']+'" selected>'+result['department']+'</option>');
          // $('#department').val(result['department']);
           $('#emailId').val(result['mail']);
           $('#contactNumber').val(result['mobile']);
           $('#currentSeat').val(result['employeeSeat']);
           $('#reportingManager').val(result['managerDN']);

           //$('#Userlocation').val(result['l']);
          // $('#Userlocation').html('<option value="'+result['l']+'" selected>'+result['l']+'</option>');



           //$('#'+passengerId).remove();

        	
        });
		
		
		
	});
	
	
	
});

$(document).ready(function(){
	$('#allocationCategory').on('change',function(){
		console.log("executed");
		if($(this).val()==="Transfer"){
			$('#employeeId').addClass('getEmpDataByIdClass');
			$('.autoFilled').val("");

			
		}
		else if($(this).val()==="NewJoinee"){
			$('#employeeId').removeClass('getEmpDataByIdClass');
			$('.autoFilled').val("");

		}
		
	});
	
});



$(document).ready(function(){
	$('#spaceManagementForm').validate({
		
		rules:{
			allocationType:"required",
			employeeId:"required",
			location:"required",
			employeeName:"required",
			dateOfJoining:"required",
			designation:"required",
			company:"required",
			department:"required",
			reportingManager:"required"	,
			upload:{required:function(){
				return $('#allocationCategory').val()==='Transfer';
			}
			},
		},
		messages:{
		
		}
		
	});
	
});


$(document).ready(function(){
	$('#allocateSpaceForm').validate({
		
		rules:{
			buildingFloor:"required",
			seatType:"required",
			employeeUPN:"required",
			seatRoom:"required",
			
		},
		messages:{
		}
		
	});
	
});
