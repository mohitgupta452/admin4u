$(function(){
	
	$('#myForm').validate({
		
		rules:{
			
			ddTemplate:"required",
			
		},
		messages:{
			
			ddTemplate:"Please select template",
		}
		
		
	});
});


$(function(){
	
	$('#myForm2').validate({
		
		rules:{
			
			uploadedFile:{
				required:true,
				extension:"xls|csv|xlsx",
			},
			
		},
		messages:{
			
			uploadedFile:{
				required:"please select a Excel sheet",
				extension:"Only Excel sheet is allowed",
			}
		}
		
		
	});
});