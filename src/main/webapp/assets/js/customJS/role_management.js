$(function() {
        // validate ticketBookingForm  form on keyup and submit
        $("#roleManagement").validate({
            rules: {
                role: "required",
		location:"required",
		employeesUPN:"required",
		
		
		},
               
                
            messages: {
                role: "Please select a role",
		location:"Please select location",
		employeeesUPN:"please select at least one employee",
		
            },
		
        });

});




$(document).ready(function(){
	

$('#modalOkButton').click(function(){
	$('#targetDiv').html("");
	var role=$('#role').val();
	$('#sourceDiv input[name="employeesUPN"]:checked').each(function() {
		
		   $('#targetDiv').append('<div class="row" ><label >'+$(this).next('label').text()+'</label></div>');
	});


});
	
});

//Function to move selected value in modal about div
$(document).ready(function(){
	
	$('.selectedEmployees').change(function(){
		
		if(this.checked)
			{
			
			$('#about').append($(this).closest('tr').clone());
		 		}
		else
			{

			$('#about input[value="'+$(this).val()+'"]').closest('tr').remove();

			
			}
		
	});
	
	
});

//End of Function to move selected value in modal about div


//modal upper div checkbox events
$(document).ready(function(){
	
	$('#about').on('click','input',function(){
		
		

		$(this).closest('tr').remove();
			
						$('#sourceDiv input[value="'+$(this).val()+'"]').prop('checked',false);
			
		

	});
	
	
});

//end code 



$(document).ready(function(){
    $('#role').on('change', function() {
  
if(this.value==="SPOC")
{
	
$('#serviceModal').modal("show");	


}




});
});


function callDeleteRole(employeeRoleID)
{
    var url="RoleManagementServlet?action=DELETEROLE&employeeRoleID="+employeeRoleID;
        $.getJSON(url, function(result){
        	
        	$('#rid'+employeeRoleID).remove();

        	showAlertMessage("SUCCESS","Role Successfuly Revoked");
        });
}
