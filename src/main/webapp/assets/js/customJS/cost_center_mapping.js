$(function(){
	
	$('#ccMapForm').validate({
		
		rules:{
			
			costCenter:"required",
			department:"required",
			
		},
		messages:{
			costCenter:"Please select cost centre",
			department:"Please select department",
			
		},
		
		  submitHandler: function(form) {
	    	  event.preventDefault();
	          form.submit();
	     }

	
		
		
	});
	
});

function removeDeptCostCentre(deptCostMapId){
	jsonData={
			 action:"REMOVEDEPTCOSTCENTREMAP",
			 deptCostMapId:deptCostMapId
			
	 }
  $.ajax({
      url: "ManageDropDownServlet",
      type: 'POST',
      data: jsonData,
      dataType: "json",
      success: function (data, textStatus, $XHR) {
    	  console.log("deleted");
   		 console.log("executed");
   		 $('#deptCost'+deptCostMapId).remove();
 	
         
       	},
      error: function (data, textStatus, $XHR) {
   	   

       	},
        });  
	
}