$(window).load(function(){
var service , tatDuration , priority ,servicehrs ,hrs = 24 ;
priority =  '<input type="text"  readonly="" value = "Priority1" class="form-control"name="tatServices"  id="tatServices" placeholder="Priority">'
var htmls = '<option value="">select...</option>'
serviceHours = ["Immediate","01 Hours","02 Hours","03 Hours","04 Hours","05 Hours","06 Hours","07 Hours","08 Hours","09 Hours","10 hours","11 Hours" ,"12 Hours","13 Hours" ,"14 Hours","15 Hours","16 Hours" ,"17 Hours","18 Hours","19 Hours" ,"20 Hours","21 Hours","22 Hours","23 Hours","24 Hours"];
serviceDays  = ["0 Working Days","1 Working Days","2 Working Days","3 Working Days","4 Working Days","5 Working Days","6 Working Days","7 Working Days","8 Working Days","9 Working Days","10 Working Days","11 Working Days","12 Working Days","13 Working Days","14 Working Days","15 Working Days","16 Working Days","17 Working Days","18 Working Days","19 Working Days","20 Working days","21 Working Days","22 Working Days","23 Working Days","24 Working Days","25 Working Days","26 Working Days","27 Working Days","28 Working Days","29 Working Days","30 Working Days","31 Working Days"];
for (var i = 0; i <= serviceDays.length; i++) {
	if(serviceHours[i]!= null){
		htmls+='<option value="'+i+'">'+serviceHours[i]+'</option>';
	};
};



for (var dc = 1; dc <=serviceDays.length ; dc++) {
			if(serviceDays[dc]!= null){
		htmls+='<option value="'+parseInt(serviceDays[dc])*hrs+'">'+serviceDays[dc]+'</option>';
		};
	};
$('#houseKeeping #extendTimeDate').html(htmls);
//$("#level1TatTime").html(htmls);
});


function getExtendComments(requestId,actId)
{
	$('#extendMsg').val(" ");
    $('#extendactionId').val(actId);

	
	
    $('#extend_modal').modal('show');
    
    var action="GETCOMMENTS";
    
    var jsonData={
    		action:action,
    		requestId:requestId
    	}

	 $.ajax({
         url: "SpocServlet",
         type: 'POST',
	      data: jsonData,
	      dataType: "json",
	      ContentType: "application/json",
         success: function (result) {
        	 if(result['service'].includes("HOUSE")){
    			 $('#repairMaintenance').hide();

    			 $('#houseKeeping').show();
    			 console.log("inside house")
        	 }
    		 else if(result['service'].includes("REPAIR")){
    			 $('#houseKeeping').hide();

    			 $('#repairMaintenance').show();
    			 console.log("inside repair")

    		 }
        	 
        	 console.log(result);
        	 $.each(result,function(key,val){
        		 $('#extendEventService').val(result['service']);

        		 console.log("key---->"+key+"val------>"+val);
        		 if(key!="service"){
        			 
        			 $('#repairMaintenance #extendTimeDate1').append('<option value="'+key+'-'+val+'">comment:'+key+'  Time:'+val+' Hours</option>')
        		 }
        		 
        	 });
        	 
        	 
        	 
      

      
         }
});
	 }






function callSpocExtend(service)
{
	console.log("callSpocExtend");
    var actionId=$('#extendactionId').val();
    var action="SETEXTENDDETAILS";
	if(service.includes("REPAIR"))
	{
	    var extendTimeDate=$('#extendTimeDate1').val();
	    var tatTime=extendTimeDate.split("-")[1];;
	    var tatComment=extendTimeDate.split("-")[0];
    
    var jsonData={
    		action:action,
    		actionId:actionId,
    		tatTime:tatTime,
    		comment:tatComment,
    	}
	}
	else if(service.includes("HOUSE")){
		var tatTime=$('#extendTimeDate').val();
    var tatComment=$('#extendMsg').val();
    
    var jsonData={
    		action:action,
    		actionId:actionId,
    		tatTime:tatTime,
    		comment:tatComment,
    	}
		
	}
	 $.ajax({
         url: "SpocServlet",
         type: 'POST',
	      data: jsonData,
	      dataType: "json",
	      ContentType: "application/json",
         success: function (result) {
        	 console.log("success");
        	 showAlertMessage("SUCCESS","Successfully Extended");
       	  $('#rid'+actionId).children().eq(3).html(result['status']);

       	  $('#rid'+actionId+' :button').attr("disabled", true);
       	  
         }
});
	 }


function callSpocServlet(action){   
		jsonData={
				 action:action,
			 }
		 $(".admin-input").each(function() {
		        var name = $(this).attr("name");
		        var value = $(this).val();
		        jsonData[name] = value;
		    })
		 
       $.ajax({
           url: "SpocServlet",
           type: 'POST',
           data: jsonData,
           dataType: "json",
           success: function (data,textStatus, $XHR) {
        	 
      	var parsedJson=data;
      	 $('#rid'+jsonData['actionId']).removeClass();
   	  $('#rid'+jsonData['actionId']).addClass("seen");

    	  $('#rid'+jsonData['actionId']).children().eq(3).html(parsedJson['status']);
    	  //  $('#rid'+parsedJson['requestID']+' :button').attr("disabled", true);
    	  $('#rid'+jsonData['actionId']+' :button').attr("disabled", true);
       	showAlertMessage("SUCCESS","Request Successfully Rejected");

              
            	},
           error: function (data, textStatus, $XHR) {

              	showAlertMessage("FAILURE","you could not perform this operation");
        	   

            	},
             });  
	    }
        
	
	
	function callCompleteSpocServlet(action){   
	
		var actionId = $('#extendactionId').val();
		var extendMsg = $('#extendMsg').val();
		var extendTimeDate = $('#extendTimeDate').val();
		jsonData={
				 action:action,
				 actionId:actionId,
				 extendMsg:extendMsg,
				 extendTimeDate:extendTimeDate
		 }
		
       $.ajax({
           url: "SpocServlet",
           type: 'POST',
           data: jsonData,
           dataType: "json",
           success: function (data,textStatus, $XHR) {

      	var parsedJson=data;
      $('#rid'+jsonData['actionId']).removeClass();
   	  $('#rid'+jsonData['actionId']).addClass("seen");

    	  $('#rid'+jsonData['actionId']).children().eq(3).html(parsedJson['status']);
    	  $('#rid'+jsonData['actionId']+' :button').attr("disabled", true);
    	  
         	showAlertMessage("SUCCESS","Request Has Been Completed");
            	},
           error: function (data, textStatus, $XHR) {
              	showAlertMessage("Failure","you could not perform this operation");
            	},
             });  
	    }
        
	
	$(document).ready(function(){
		$(document).on("click",'.confirmButton',function(){
			
			var actionId=$('#modalActionId').val();
   			var roomId=$(this).val();
   			$('#guestHouseRoomId'+actionId).val(roomId);
   			$('.confirmButton').html("Select");
   			$(this).html("Selected");
   			
   			
			/*var url="GuestHouseInventoryServlet";
			var requestId=$('#modalRequestId').val();
			var actionId=$('#modalActionId').val();
			var checkInDateTime=$("#checkInDateTime"+actionId).val();
			var checkOutDateTime=$("#checkOutDateTime"+actionId).val();
			var spocComment=$("#spoc_comment"+actionId).val();

			var roomId=$(this).val();
			
			var action="SPOC_SERV";
			var jsonData={
					requestId:requestId,
					roomId:roomId,
					action:action,
					actionId:actionId,
					checkInDateTime:checkInDateTime,
					checkOutDateTime:checkOutDateTime,
					spocComment:spocComment,
			}
			
			$.ajax({
			      url: url,
			      type: 'POST',
			      data: jsonData,
			      dataType: "json",
			      ContentType: "application/json",
			      success: function(result){
			    	  
			    	  if(result['status']==='true'){
			         	showAlertMessage("SUCCESS",result['msg']);

			         	$('#rid'+actionId).removeClass();
			         	  $('#rid'+actionId).addClass("seen");

			          	  $('#rid'+actionId).children().eq(3).html('COMPLETE');
			          	  $('#rid'+actionId+' :button').attr("disabled", true);
			          	 $('#spoc_modal'+actionId).modal('toggle');
			    	  }
			    	  else
			    		  {
			    		  showAlertMessage("Failure",result['msg']);

			    		  }
			    	  
			        },
			        error: function (data, textStatus, $XHR) {
		              	showAlertMessage("Failure","you could not perform this operation");
		            	},
			});*/
			
			
			
				});
	        });

	// Driver Mobile No		
	   
   	//restricted mobileno continuous pattern
   	jQuery.validator.addMethod("matchMob", function (value, element,param) {
   	if(value.match(new RegExp( param ))){
   	 return value.match(new RegExp( param ));
   	};  
   	});
   	
   	
 // only number will be enter in the field
   	$('.numbersOnly').keyup(function () {
   	    if (this.value != this.value.replace(/[^0-9]/g, '')) {
   	       this.value = this.value.replace(/[^0-9]/g, '');
   	    }
   	});
  
    $(function() {
        
        // validate ticketBookingForm  form on keyup and submit
        $("#spoc_response_form").validate({
            rules: {
            	         driverNo:
            	         {
            	 minlength:10  , 
            	         }
            	 },
            	messages :
            	{
            		driverNo : 
            		{ 
            			minlength : "please enter exact 10 digits",
            		}
            		
            	}
          })
    }); 	 
           	 
            	 
   	   
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

