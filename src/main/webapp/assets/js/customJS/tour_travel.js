//escape click
/*window.addEventListener('click',function(){
	if($('#bookingFor').val()=== 'External'){
		if($('#airExternalGuestType').val() ==="" || $('#airExteranalCompany').val()=== ""){
		$('#bookingFor').val("").trigger('change');
		}
		}
})*/

$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
    		if($('#bookingFor').val()=== 'External'){
    	 if($('#airExternalGuestType').val() ==="" || $('#airExteranalCompany').val()=== ""){
    	  $('#bookingFor').val("").trigger('change');
    	  }}
    		if($('#trainBookingFor').val()=== 'External'){
    	 if($('#trainExternalGuestType').val()==="" || $('#trainTravelClass').val() === "" || $('#trainExteranalCompany').val() === ""){
    	  $('#trainBookingFor').val("").trigger('change'); 
          }}
    		if($('#cab_book').val()=== 'External'){
    	 if($('#pick4uExternalGuestType').val() === "" ||$('#Pick4uExternalCabPref').val() === "" ){
    		 $('#cab_book').val("").trigger('change');  
    	 }}
    		if($('#cab_drop_book').val()=== 'External'){
    	 if($('#cabDropExternalGuestType').val() === "" || $('#cabDropExternalCabPref').val() === ""){
    		 $('#cab_drop_book').val("").trigger('change');  
    	 }}
    		if($('#cab_disposal_book').val()=== 'External'){
    	 if($('#cabDisposalExternalGuestType').val() === "" || $('#cabDisposalExternalCabPref').val() === ""){
    		 $('#cab_disposal_book').val("").trigger('change');  
    	 }}
    		if($('#cab_airport_book').val()=== 'External'){
    	 if($('#cabAirportExternalGuestType').val() === "" || $('#cabAirportExternalCabPref').val() === ""){
    		 $('#cab_airport_book').val("").trigger('change');  
    	 }}
     
     
     }
});



// External close button

function airExtenalCloseBtn(){
	 $('#bookingFor').val("").trigger('change');
 }
function trainExtenalCloseBtn(){
	$('#trainBookingFor').val("").trigger('change'); 
}
function pickExtenalCloseBtn(){
	$('#cab_book').val("").trigger('change'); 
}
function dropExtenalCloseBtn(){
	$('#cab_drop_book').val("").trigger('change'); 
}
function disposalExtenalCloseBtn(){
	$('#cab_disposal_book').val("").trigger('change'); 
}
function airportExtenalCloseBtn(){
	$('#cab_airport_book').val("").trigger('change'); 
}



// For internal booking
$('.bookingFor').on('change' , function(){
	if(this.value === "Internal Emp.")
	{
		$('#internal_booking_modal').modal('show');
		
	}
});

// avail accomodation in airticket
$('#hotel').on('click' , function(){
 if ($('#hotel').is(':checked')) {
	 $('#avail_acc_modal').modal('show');
 }
 else
	 $('#avail_accomodation_row').hide();
});

 // avail accomodation in trainticket
$('#trainAvailAccomodation').on('click' , function(){
 if ($('#trainAvailAccomodation').is(':checked')) {
	 $('#avail_acc_modal').modal('show');
   }
	 else
		 $('#train_avail_accomodation_row').hide();
 
});

//for avail accomodation checkbox
/*$(document).ready(function(){*/
function callAvailAcc(){
	
 /*   $('#hotel').click(function() {*/
    if($('#hotel').is(':checked'))
    {
        $('#avail_accomodation_row').show();
    }
    if($('#trainAvailAccomodation').is(':checked'))
    {
    	$('#train_avail_accomodation_row').show();
    }
   /* else if(!($('#hotel').is(':checked'))) {
    	$('#avail_accomodation_row').hide();
    }
    else if(!($('#trainAvailAccomodation').is(':checked'))){
    $('#train_avail_accomodation_row').hide();
    }*/
//});

} 

function cancelAvailAcc(){
	if($('#hotel').is(':checked'))
    {
		$('#hotel').removeAttr('checked')
    }
  if($('#trainAvailAccomodation').is(':checked'))
	 {
	  $('#trainAvailAccomodation').removeAttr('checked')
	 }
}



//  remove asterik 
$('#bookingFor').change(function(){
	
	if($('#bookingFor option:selected').text() === 'External')
	{
	    $('#removeSpan').hide();
	}
	else 
		{
		 $('#removeSpan').show();
		}
		
})
$('#cab_book').change(function(){
	if($('#cab_book option:selected').text() === 'External')
	{
	    $('#pickremoveSpan').hide();
	}
	else 
	{
	 $('#pickremoveSpan').show();
	}
	
})
$('#cab_drop_book').change(function(){
	if($('#cab_drop_book option:selected').text() === 'External')
	{
	    $('#dropremoveSpan').hide();
	}
	else 
	{
	 $('#dropremoveSpan').show();
	}
	
})
$('#cab_disposal_book').change(function(){
	if($('#cab_disposal_book option:selected').text() === 'External')
	{
	    $('#disposalremoveSpan').hide();
	}
	else 
	{
	 $('#disposalremoveSpan').show();
	}
	
})
$('#cab_airport_book').change(function(){
	if($('#cab_airport_book option:selected').text() === 'External')
	{
	    $('#airportremoveSpan').hide();
	}
	else 
	{
	 $('#airportremoveSpan').show();
	}
	
})
$('#trainBookingFor').change(function(){
	if($('#trainBookingFor option:selected').text() === 'External')
	{
	    $('#trainremoveSpan').hide();
	}
	else 
	{
	 $('#trainremoveSpan').show();
	}
})


// Others (Avail Acc Location in airticket)
$('#airlocation').change(function(){
	if($('#airlocation option:selected').val() === 'Others')
		{
		$('#sourcelocationModal .modal-footer').html('');
		 $('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterAccomodationLocation()">Yes</button>'
		         +'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectAccomodationLocation()">No</button> '); 
		    $('#sourcelocationModal').modal('show');
		}
})
function enterAccomodationLocation()
{
	$('#accomodationSelectLocation').hide();
	$('#accomodationInputLocation').show();
	$('#airlocation').attr('disabled' , true);
	$('#airlocation1').attr('disabled' , false);

	//$('#selectLocationModal').modal('show');
}

$('#airlocation1').on('blur',function(){
	if($('#airlocation1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectAccomodationLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterAccomodationLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#airlocation option:selected').val($('#airlocation1').val());
})

function selectAccomodationLocation()
{
	$("#airlocation").val("").trigger('change');
	$('#accomodationInputLocation').hide();
	$('#accomodationSelectLocation').show();
	$('#airlocation').attr('disabled' , false);
	$('#airlocation1').attr('disabled' , true);
}

//Others (Avail Acc Location in trainticket)
$('#trainLocation').change(function(){
	if($('#trainLocation option:selected').val() === 'Others')
		{
		$('#sourcelocationModal .modal-footer').html('');
		 $('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterTrainAccomodationLocation()">Yes</button>'
		         +'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectTrainAccomodationLocation()">No</button> '); 
		    $('#sourcelocationModal').modal('show');
		}
})
function enterTrainAccomodationLocation()
{
	$('#trainAccomodationSelectLocation').hide();
	$('#trainAccomodationInputLocation').show();
	$('#trainLocation').attr('disabled' , true);
	$('#trainLocationInput').attr('disabled' , false);

	//$('#selectLocationModal').modal('show');
}

/*$('#trainLocation1').on('blur' , function(){
	alert("innsideee");
})
*/
/*function trainLocation(){
	alert("inside");
}*/
$('#trainLocationInput').on('blur',function(){
	if($('#trainLocationInput').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectTrainAccomodationLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterTrainAccomodationLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#trainLocation option:selected').val($('#trainLocationInput').val());
})

function selectTrainAccomodationLocation()
{
	$("#trainLocation").val("").trigger('change');
	$('#trainAccomodationInputLocation').hide();
	$('#trainAccomodationSelectLocation').show();
	$('#trainLocation').attr('disabled' , false);
	$('#trainLocationInput').attr('disabled' , true);
}


//Others (Return source in airticket)
$('#returnSource').change(function(){
	if($('#returnSource option:selected').val() === 'OtherSource')
		{
		$('#sourcelocationModal .modal-footer').html('');
		 $('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterReturnSourceLocation()">Yes</button>'
		         +'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectReturnSourceLocation()">No</button> '); 
		    $('#sourcelocationModal').modal('show');
		}
})
function enterReturnSourceLocation()
{
	$('#returnSourceSelectLocation').hide();
	$('#returnSourceInputLocation').show();
	$('#returnSource').attr('disabled' , true);
	$('#returnSource1').attr('disabled' , false);

	//$('#selectLocationModal').modal('show');
}

$('#returnSource1').on('blur',function(){
	if($('#returnSource1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectReturnSourceLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterReturnSourceLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#returnSource option:selected').val($('#returnSource1').val());
})

function selectReturnSourceLocation()
{
	$("#returnSource").val("").trigger('change');
	$('#returnSourceInputLocation').hide();
	$('#returnSourceSelectLocation').show();
	$('#returnSource').attr('disabled' , false);
	$('#returnSource1').attr('disabled' , true);
	
}

//Others (Return Destination in airticket)
$('#returnDestination').change(function(){
	if($('#returnDestination option:selected').val() === 'OtherDestination')
		{
		$('#sourcelocationModal .modal-footer').html('');
		 $('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterReturnDestinationLocation()">Yes</button>'
		         +'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectReturnDestinationLocation()">No</button> '); 
		    $('#sourcelocationModal').modal('show');
		}
})
function enterReturnDestinationLocation()
{
	$('#returnDestinationSelectLocation').hide();
	$('#returnDestinationInputLocation').show();
	$('#returnDestination').attr('disabled' , true);
	$('#returnDestination1').attr('disabled' , false);

	//$('#selectLocationModal').modal('show');
}

$('#returnDestination1').on('blur',function(){
	if($('#returnDestination1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectReturnDestinationLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterReturnDestinationLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#returnDestination option:selected').val($('#returnDestination1').val());
})

function selectReturnDestinationLocation()
{
	$("#returnDestination").val("").trigger('change');
	$('#returnDestinationInputLocation').hide();
	$('#returnDestinationSelectLocation').show();
	$('#returnDestination').attr('disabled' , false);
	$('#returnDestination1').attr('disabled' , true);
	
}


// Others (Return Source in trainticket)

$('#trainReturnSource').change(function(){
	if($('#trainReturnSource option:selected').val() === 'OtherSource')
		{
		$('#sourcelocationModal .modal-footer').html('');
		 $('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterTrainReturnSourceLocation()">Yes</button>'
		         +'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectTrainReturnSourceLocation()">No</button> '); 
		    $('#sourcelocationModal').modal('show');
		}
})
function enterTrainReturnSourceLocation()
{
	$('#trainReturnSourceSelectLocation').hide();
	$('#trainReturnSourceInputLocation').show();
	$('#trainReturnSource').attr('disabled' , true);
	$('#trainReturnSource1').attr('disabled' , false);

	//$('#selectLocationModal').modal('show');
}

$('#trainReturnSource1').on('blur',function(){
	if($('#trainReturnSource1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectTrainReturnSourceLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterTrainReturnSourceLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#trainReturnSource option:selected').val($('#trainReturnSource1').val());
})

function selectTrainReturnSourceLocation()
{
	$("#trainReturnSource").val("").trigger('change');
	$('#trainReturnSourceInputLocation').hide();
	$('#trainReturnSourceSelectLocation').show();
	$('#trainReturnSource').attr('disabled' , false);
	$('#trainReturnSource1').attr('disabled' , true);
	
}

//Others (Return Destination in trainticket)
$('#trainReturnDestination').change(function(){
	if($('#trainReturnDestination option:selected').val() === 'OtherDestination')
		{
		$('#sourcelocationModal .modal-footer').html('');
		 $('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterTrainReturnDestinationLocation()">Yes</button>'
		         +'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectTrainReturnDestinationLocation()">No</button> '); 
		    $('#sourcelocationModal').modal('show');
		}
})
function enterTrainReturnDestinationLocation()
{
	$('#trainReturnDestinationSelectLocation').hide();
	$('#trainReturnDestinationInputLocation').show();
	$('#trainReturnDestination').attr('disabled' , true);
	$('#trainReturnDestination1').attr('disabled' , false);

	//$('#selectLocationModal').modal('show');
}

$('#trainReturnDestination1').on('blur',function(){
	if($('#trainReturnDestination1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectTrainReturnDestinationLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterTrainReturnDestinationLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#trainReturnDestination option:selected').val($('#trainReturnDestination1').val());
})

function selectTrainReturnDestinationLocation()
{
	$("#trainReturnDestination").val("").trigger('change');
	$('#trainReturnDestinationInputLocation').hide();
	$('#trainReturnDestinationSelectLocation').show();
	$('#trainReturnDestination').attr('disabled' , false);
	$('#trainReturnDestination1').attr('disabled' , true);
	
}







// Others (source in airticket)
$('#source').change(function(){
	if($('#source option:selected').val() === 'OtherSource')
		{
		$('#sourcelocationModal .modal-footer').html('');
		 $('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterSourceLocation()">Yes</button>'
		         +'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectSourceLocation()">No</button> '); 
		    $('#sourcelocationModal').modal('show');
		}
})
function enterSourceLocation()
{
	$('#sourceSelectLocation').hide();
	$('#sourceInputLocation').show();
	$('#source').attr('disabled' , true);
	$('#source1').attr('disabled' , false);

	//$('#selectLocationModal').modal('show');
}

$('#source1').on('blur',function(){
	if($('#source1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectSourceLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterSourceLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#source option:selected').val($('#source1').val());
})

function selectSourceLocation()
{

	$("#source").val("").trigger('change');
	$('#sourceInputLocation').hide();
	$('#sourceSelectLocation').show();
	$('#source').attr('disabled' , false);
	$('#source1').attr('disabled' , true);
	
	
}

// Others (destination in airticket)
$('#destination').change(function(){
	if($('#destination option:selected').val() === 'OtherDestination')
		{
		$('#sourcelocationModal .modal-footer').html("");
$('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
		+' data-dismiss="modal" onclick="enterDestinationLocation()">Yes</button>'
+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectDestinationLocation()">No</button> ');

		    $('#sourcelocationModal').modal('show');
		}
})
function enterDestinationLocation()
{
	$('#destinationSelectLocation').hide();
	$('#destinationInputLocation').show();
	//$('#selectLocationModal').modal('show');
	$('#destination').attr('disabled' , true);
	$('#destination1').attr('disabled' , false);
}

$('#destination1').on('blur',function(){
	if($('#destination1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectDestinationLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterDestinationLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#destination option:selected').val($('#destination1').val());
})

function selectDestinationLocation()
{
	$('#destination').val("").trigger('change');
	$('#destinationSelectLocation').show();
	$('#destinationInputLocation').hide();
	$('#destination').attr('disabled' , false);
	$('#destination1').attr('disabled' , true);
}


// Others (source in train)
$('#trainSource').change(function(){
	if($('#trainSource option:selected').val() === 'OtherSource')
		{
		$('#sourcelocationModal .modal-footer').html("");
		$('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
				+' data-dismiss="modal" onclick="enterTrainSourceLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectTrainSourceLocation()">No</button> ');	   
		    $('#sourcelocationModal').modal('show');
		}
})
function enterTrainSourceLocation()
{
	$('#trainSourceSelectLocation').hide();
	$('#trainSourceInputLocation').show();
	//$('#selectLocationModal').modal('show');
	$('#trainSource').attr('disabled' , true);
	$('#trainSource1').attr('disabled' , false);
}


$('#trainSource1').on('blur',function(){
	if($('#trainSource1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectTrainSourceLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterTrainSourceLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#trainSource option:selected').val($('#trainSource1').val());
})

function selectTrainSourceLocation()
{
	$('#trainSource').val("").trigger('change');
	$('#trainSourceSelectLocation').show();
	$('#trainSourceInputLocation').hide();
	$('#trainSource').attr('disabled' , false);
	$('#trainSource1').attr('disabled' , true);
}

//Others (destination in train)
$('#trainDestination').change(function(){
	if($('#trainDestination option:selected').val() === 'OtherDestination')
		{
		$('#sourcelocationModal .modal-footer').html("");
		$('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
				+' data-dismiss="modal" onclick="enterTrainDestinationLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectTrainDestinationLocation()">No</button> ');
		    $('#sourcelocationModal').modal('show');
		}
})
function enterTrainDestinationLocation()
{
	$('#trainDestinationSelectLocation').hide();
	$('#trainDestinationInputLocation').show();
	//$('#selectLocationModal').modal('show');
	$('#trainDestination').attr('disabled' , true);
	$('#trainDestination1').attr('disabled' , false);
}

$('#trainDestination1').on('blur',function(){
	if($('#trainDestination1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectTrainDestinationLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterTrainDestinationLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#trainDestination option:selected').val($('#trainDestination1').val());
})

function selectTrainDestinationLocation()
{
	$('#trainDestination').val("").trigger('change');
	$('#trainDestinationSelectLocation').show();
	$('#trainDestinationInputLocation').hide();
	$('#trainDestination').attr('disabled' , false);
	$('#trainDestination1').attr('disabled' , true);
}




// Others (company Name in airticket)

$('#airExteranalCompany').change(function(){
	if($('#airExteranalCompany option:selected').val() === 'Others')
		{
		$('#sourcelocationModal .modal-footer').html("");
		$('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
				+' data-dismiss="modal" onclick="enterAirExternalCompanyName()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectAirExternalLocation()">No</button> ');
		$('#air_modal').modal('hide');
		    $('#sourcelocationModal').modal('show');
		}
})
function enterAirExternalCompanyName()
{
	$('#air_modal').modal('show');
	$('#airECDiv').hide();
	$('#airECInputDiv').show();
	//$('#selectLocationModal').modal('show');
	$('#airExteranalCompany').attr('disabled' , true);
	$('#airExteranalCompany1').attr('disabled' , false);
}

$('#airExternalCompany1').on('blur',function(){
	if($('#airExternalCompany1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectAirExternalLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterAirExternalCompanyName()">No</button> ');
		$('#air_modal').modal('hide');
		$('#selectLocationModal').modal('show');   
	}
	$('#airExteranalCompany option:selected').val($('#airExternalCompany1').val());
})

function selectAirExternalLocation()
{
	$('#trainDestination').val("").trigger('change');
	$('#air_modal').modal('show');
	$('#airECDiv').show();
	$('#airECInputDiv').hide();
	$('#airExteranalCompany').attr('disabled' , false);
	$('#airExteranalCompany1').attr('disabled' , true);
	
}


// Others (company Name in trainticket)
$('#trainExteranalCompany').change(function(){
	if($('#trainExteranalCompany option:selected').val() === 'Others')
		{
		$('#sourcelocationModal .modal-footer').html("");
		$('#sourcelocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
				+' data-dismiss="modal" onclick="enterTrainExternalCompanyName()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectTrainExternalLocation()">No</button> ');
		$('#train_modal').modal('hide');
		 $('#sourcelocationModal').modal('show');
		}
})
function enterTrainExternalCompanyName()
{
	$('#train_modal').modal('show');
	$('#trainECDiv').hide();
	$('#trainECInputDiv').show();
	//$('#selectLocationModal').modal('show');
	$('#trainExteranalCompany').attr('disabled' , true);
	$('#trainExteranalCompany1').attr('disabled' , false);
}

$('#trainExternalCompany1').on('blur',function(){
	if($('#trainExternalCompany1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectTrainExternalLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterTrainExternalCompanyName()">No</button> ');
		$('#train_modal').modal('hide');
		$('#selectLocationModal').modal('show');   
	}
	$('#trainExteranalCompany option:selected').val($('#trainExternalCompany1').val());
	
	
})

function selectTrainExternalLocation()
{
	$('#trainDestination').val("").trigger('change');
	$('#train_modal').modal('hide');
	$('#trainECDiv').show();
	$('#trainECInputDiv').hide();
	$('#trainExteranalCompany').attr('disabled' , false);
	$('#trainExteranalCompany1').attr('disabled' , true);
}




// two way journey for train ticket

$('#trainTwoWay').change(function(){
	//$("#trainTravelPurpDiv").hide();
	$("#trainReturnTravelDateDiv").show();

});

// two way journey in airticket
$('#two').change(function(){
	//$("#travel_purpose").hide();
	$("#return_Date").show();

});

// one way journey in trainticket
$('#trainOneWay').change(function(){
	$("#trainReturnTravelDateDiv").hide();
	//$("#trainTravelPurpDiv").show();
});

//one way journey in airticket
$('#one').change(function(){
	$("#return_Date").hide();
	//
	//$("#travel_purpose").show();

});


// only number will be enter in the field
$('.numbersOnly').keyup(function () {
    if (this.value != this.value.replace(/[^0-9]/g, '')) {
       this.value = this.value.replace(/[^0-9]/g, '');
    }
});

/*//only number will be enter in the field
$('.numbersOnlyWithoutZero').keyup(function () {
    if (this.value != this.value.replace(/[^1-9]/g, '')) {
       this.value = this.value.replace(/[^1-9]/g, '');
    }
});*/

//only alphabets are allowed in the field /[^a-z]/g
$('.alphabetOnly').keyup(function () {
	
    if (this.value != this.value.replace( /[^a-zA-Z ]/g, '')) {
       this.value = this.value.replace( /[^a-zA-Z ]/g, '');
    }
});


// pax name only alphabet validation in modal 
$("body").on('keyup','.alphabetOnly',function(){
	  if (this.value != this.value.replace( /[^a-zA-Z ]/g, '')) {
	       this.value = this.value.replace( /[^a-zA-Z ]/g, '');
	    }
});

// airticket upload file name 

$("#upload").on('change',function(){
	$('#fileName').html("");
	var filename = $('input[name=upload]').val().replace(/C:\\fakepath\\/i, '');
$('#fileName').append(filename);

	});
//train upload file name
$("#trainApprovedAttachment").on('change',function(){
		$('#trainFileName').html("");
	var tFile = $('input[name=trainApprovedAttachment]').val().replace(/C:\\fakepath\\/i, '');
$('#trainFileName').append(tFile);

	});



//redirect response on current open tab instead of active tab
$(document).ready(function(){
$('#myTab a').click(function(e) {
	  e.preventDefault();
	  $(this).tab('show');
	});

	// store the currently selected tab in the window.name which is stabled till the window is closed
$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
	  var id = $(e.target).attr("href").substr(1);
	  window.name = id;
	});

//select the type of cab
$('#cabServices').on('change',function(){
	var cabType = $(this).attr("value");
	localStorage.setItem('selectedTab', cabType);
});
var hashID = "#"+window.name;

	// on load of the page: switch to the currently selected tab
    $('#myTab a[href= "' + hashID + '"]').tab('show');
    var cabID = localStorage.getItem('selectedTab');
if(window.name === 'cab_requests'){
    if(cabID === 'pick'){
    	 $(".drop ,.disposal ,.airport").hide();
    	// document.getElementById('cabServices').getElementsByTagName('option')[0].selected = true;
    	 $('#cabServices').val('pick').trigger('change');
     	 $("."+cabID).show();
    }
    else if(cabID === 'drop'){
    	 $(".pick , .disposal ,.airport").hide();
    	 //document.getElementById('cabServices').getElementsByTagName('option')[1].selected = true;
    	 $('#cabServices').val('drop').trigger('change');
    	 $("."+cabID).show();
        }
    else if(cabID === 'disposal'){
    	 $(".drop , .airport ,.pick ").hide();
    	// document.getElementById('cabServices').getElementsByTagName('option')[2].selected = true;
    	 $('#cabServices').val('disposal').trigger('change');
    	 $("."+cabID).show();
        }
    else if(cabID === 'airport'){
    	 $(".drop , .disposal , .pick").hide();
    	// document.getElementById('cabServices').getElementsByTagName('option')[3].selected = true;
    	 $('#cabServices').val('airport').trigger('change');
    	 $("."+cabID).show();
     }
}
});

//script to select the type of cab request by cab services 
$(document).ready(function(){
    $('#cabServices').on('change',function(){
        if($(this).attr("value")=="pick"){
            $(".pick").show();
             $(".drop").hide();
             $(".disposal").hide();
             $(".airport").hide();
           
        }
        if($(this).attr("value")=="drop"){
            $(".drop").show();
             $(".pick").hide();
             $(".airport").hide();
              $(".disposal").hide();
        }
        if($(this).attr("value")=="disposal"){
            $(".disposal").show();
             $(".pick").hide();
             $(".airport").hide();
             $(".drop").hide();
        }
         if($(this).attr("value")=="airport"){
            $(".airport").show();
             $(".pick").hide();
             $(".drop").hide();
             $(".disposal").hide();
        }
    });
});


//Return Source and Return Destination
//Air  return source and return destination should not be same
var Returnoptions = $("#returnSource > option").clone();

$(document).ready(function(){
	
	var returnSource = $("select[name='returnSource']");
	var returnDestination = $("select[name='returnDestination']");
 var optionS;
 var optionD;
 var selS;
 var selD;
	returnSource.change(function() {
		   selS = $(this).val();
		  //$('#destination').append(optionD);
		   $("#returnDestination option:first").after(optionD);
		
	   returnDestination.find('option').prop("disabled", false);
	 
	    optionD  =  returnDestination.find('option[value="' + selS + '"]')
	    if (selS) {
	                      	 optionD.remove();
}
	  });
		
returnDestination.change(function() {
	 selD = $('#returnDestination').val();
		    //$('#source').append(optionS);
		    $("#returnSource option:first").after(optionS);
	    optionS  =  returnSource.find('option[value="' + selD + '"]')
	    if (selD) {
	                      	 optionS.remove();
	    }
	  
	});
	});


//Train source and destination should not be same
var trainReturnOptions = $("#trainReturnSource > option").clone();
$(document).ready(function(){
	
	var trainReturnSource = $("select[name='trainReturnSource']");
	var trainReturnDestination = $("select[name='trainReturnDestination']");
	var selItem;
	var selDes;
	var optionDes;
	var optionSource;
trainReturnSource.change(function() {
	
	   selItem = $(this).val();
		    //$(trainDestination).append(optionDes);
		    $("#trainReturnDestination option:first").after(optionDes);
		   
		  trainReturnDestination.find('option').prop("disabled", false);
	 
	    optionDes  =  trainReturnDestination.find('option[value="' + selItem + '"]');
	    if (selItem) {
	    	              optionDes.remove();
	    }
	   
	});

trainReturnDestination.change(function(){
	  selDes = $('#trainReturnDestination').val();
	           // $(trainSource).append(optionSource);
	  $("#trainReturnSource option:first").after(optionSource);
	           
	           
	    optionSource  =  trainReturnSource.find('option[value="' + selDes + '"]');
	    if (selDes) {
	                      	 optionSource.remove();
	    }
	   
});

	});





// Source and Destination
// Air  source and destination should not be same
var options = $("#source > option").clone();

$(document).ready(function(){
	
	var source = $("select[name='source']");
	var destination = $("select[name='destination']");
    var optionS;
    var optionD;
    var selS;
    var selD;
	source.change(function() {
		   selS = $(this).val();
		  //$('#destination').append(optionD);
		   $("#destination option:first").after(optionD);
		
	   destination.find('option').prop("disabled", false);
	 
	    optionD  =  destination.find('option[value="' + selS + '"]')
	    if (selS) {
	                      	 optionD.remove();
}
	  });
		
destination.change(function() {
	 selD = $('#destination').val();
		    //$('#source').append(optionS);
		    $("#source option:first").after(optionS);
	    optionS  =  source.find('option[value="' + selD + '"]')
	    if (selD) {
	                      	 optionS.remove();
	    }
	  
	});
	});


// Train source and destination should not be same
var trainOptions = $("#trainSource > option").clone();
$(document).ready(function(){
	
	var trainSource = $("select[name='trainSource']");
	var trainDestination = $("select[name='trainDestination']");
	var selItem;
	var selDes;
	var optionDes;
	var optionSource;
  trainSource.change(function() {
	
	   selItem = $(this).val();
		    //$(trainDestination).append(optionDes);
		    $("#trainDestination option:first").after(optionDes);
		   
		  trainDestination.find('option').prop("disabled", false);
	 
	    optionDes  =  trainDestination.find('option[value="' + selItem + '"]');
	    if (selItem) {
	    	              optionDes.remove();
	    }
	   
	});
  
  trainDestination.change(function(){
	  selDes = $('#trainDestination').val();
	           // $(trainSource).append(optionSource);
	  $("#trainSource option:first").after(optionSource);
	           
	           
	    optionSource  =  trainSource.find('option[value="' + selDes + '"]');
	    if (selDes) {
	                      	 optionSource.remove();
	    }
	   
  });
  
	});

//start point and end point should not be same
jQuery.validator.addMethod("matchValue", function (value, element,param) {
	
	   if(value === $("[name="+param+"]").val())
	   { }
	   else
	   {
		  return value ; 
	   }
	  
});

// add multiple pax 
var count = 2;
function addPax(eClassName,service)

{
	if(service==="train"){
	var prefixPax="trainPassenger";
	//var prefixEmp="trainEmployeeID";
	var appendingDivId="trainPaxName";
	//var age="age";

	}
	else if(service=="hotel"){
		
	}
	else{
		var prefixPax="passenger";
		//var prefixEmp="employeeID";
		var appendingDivId="paxName";
		//var age="age"
	}
count += 1;
	$('.'+appendingDivId).append('<div class="row">' /*<div class="col-sm-8 col-md-4">'
			    +'<div class="form-group"> <div class="col-sm-5"><label>Employee ID'+count+'<span class="requiredField"> *</span><br>&nbsp;</label>'
                +'</div><div class="col-sm-7"><input type ="text" class="form-control '+eClassName+' " id="'+prefixEmp+count+'" name="employeeID'+count+'" placeholder="Employee ID">'
                +'</div></div></div>'*/
                +' <div class="col-sm-8 col-md-10"><div class="form-group"><div class="col-sm-5"><label>Pax Name'+count+'</label>'
                +'</div> <div class="col-sm-7"><input type ="text" class="form-control alphabetOnly" id="'+prefixPax+count+'"'
                +'name="passenger'+count+'" placeholder="Pax Name"><span id="error_pax'+count+'"></span></div></div></div></div><br/>'
                /*+' <div class="col-sm-8 col-md-4"><div class="form-group"><div class="col-sm-5"><label>Pax Age'+count+'<span class="requiredField"> *</span></label>'
                +'</div> <div class="col-sm-7"><input type ="text" class="form-control" id="'+age+count+'"'
                +'name="age'+count+'" placeholder="Age"></div></div> </div>' */

                
	);	$('.totalPassengers').val(count);
	//alert($('.totalPassengers').val());
}

//
function callHNGServlet(action,actionId){   
jsonData={
action:action,
actionId:actionId
}

$(".admin-input").each(function() {
       var name = $(this).attr("name");
       var value = $(this).val();
       jsonData[name] = value;
   })

      $.ajax({
          url: "AirTicketRequestController",
          type: 'POST',
          data: jsonData,
          dataType: "json",
          complete: function (data, textStatus, $XHR) {
        	  
          },
          error: function (data, textStatus, $XHR) {
          
           	},
            });  
   }


// for disposal in airticket

$(document).ready(function(){
	
	$('#disposal').on('change' , function(){
		if(this.value === "yes")
			{
			    $("#dropP").hide();
			}
		else
			{
			    $("#dropP").show();
			}
	})
})

// for disposal in trainticket

$(document).ready(function(){
	
	$('#trainDisposal').on('change' , function(){
		if(this.value === "yes")
			{
			    $("#dropPT").hide();
			}
		else
			{
			    $("#dropPT").show();
			}
	})
})



//===end date picker limit validation====
$('#addP').hide();
$(document).ready(function(){
    $('#bookingFor').on('change', function() {
  
if(this.value==="Self Booking")
{
	
	$('#passenger1').val($('#userName').val());
	$('#employeeID1').val($('#userID').val());

$('#passenger1').attr('readonly', true);
     $('#passenger1').addClass('input-disabled');
$('#employeeID1').attr('readonly', true);
     $('#employeeID1').addClass('input-disabled');
     $('#addP').hide(); 
     
}
else
{

	$('#passenger1').val("");
	$('#employeeID1').val("");
$('#passenger1').attr('readonly', false);
     $('#passenger1').removeClass();
$('#employeeID1').attr('readonly',false);
     $('#employeeID1').addClass('input-disabled');

}
if(this.value==="External")
{

	$('#passenger1').val("");
	$('#employeeID1').val("");
$('#air_modal').modal('show');
$('#addP').show(); 
}
if(this.value ==="Internal Emp.")
{
	$('#addP').hide(); 
}

});
});

//end of air ticket custom js


// custom js for cab drop request 


$(document).ready(function(){
    $('#cab_drop_book').on('change', function() {
  
if(this.value==="Self Booking")
{
	$('#cab_drop_passenger').val($('#userName').val());
	$('#cab_drop_employee').val($('#userID').val());
$('#cab_drop_passenger').attr('readonly', true);
     $('#cab_drop_passenger').addClass('input-disabled');
$('#cab_drop_employee').attr('readonly', true);
     $('#cab_drop_employee').addClass('input-disabled');


}


else
{

	$('#cab_drop_passenger').val("");
	$('#cab_drop_employee').val("");
$('#cab_drop_passenger').attr('readonly', false);
     $('#cab_drop_passenger').removeClass();
$('#cab_drop_employee').attr('readonly',false);
     $('#cab_drop_employee').addClass('input-disabled');

}
if(this.value==="External")
{
	$('#cab_drop_passenger').val("");
	$('#cab_drop_employee').val("");
$('#cab_drop_modal').modal('show');
}


});
});


// end of cab drop request


//custom js for cab airport request 


$(document).ready(function(){
    $('#cab_airport_book').on('change', function() {
  
if(this.value==="Self Booking")
{
	$('#cab_airport_passenger').val($('#userName').val());
	$('#cab_airport_employee').val($('#userID').val());
$('#cab_airport_passenger').attr('readonly', true);
     $('#cab_airport_passenger').addClass('input-disabled');
$('#cab_airport_employee').attr('readonly', true);
     $('#cab_airport_employee').addClass('input-disabled');


}


else
{

	$('#cab_airport_passenger').val("");
	$('#cab_airport_employee').val("");
$('#cab_airport_passenger').attr('readonly', false);
     $('#cab_airport_passenger').removeClass();
$('#cab_airport_employee').attr('readonly',false);
     $('#cab_airport_employee').addClass('input-disabled');

}
if(this.value==="External")
{
	$('#cab_airport_passenger').val("");
	$('#cab_airport_employee').val("");
$('#cab_airportExternal_modal').modal('show');
}


});
});


// end of cab airport request








//custom js for disposal

$(document).ready(function(){
    $('#cab_disposal_book').on('change', function() {
  
if(this.value==="Self Booking")
{
	$('#cab_disposal_passenger').val($('#userName').val());
	$('#cab_disposal_employeeID').val($('#userID').val());
	
$('#cab_disposal_passenger').attr('readonly', true);
     $('#cab_disposal_passenger').addClass('input-disabled');
$('#cab_disposal_employeeID').attr('readonly', true);
 $('#cab_disposal_employeeID').addClass('input-disabled');

}

else
{
	$('#cab_disposal_passenger').val("");
	$('#cab_disposal_employeeID').val("");
	
$('#cab_disposal_passenger').attr('readonly', false);
     $('#cab_disposal_passenger').removeClass();
$('#cab_disposal_employeeID').attr('readonly',false);
    $('#cab_disposal_employeeID').addClass('input-disabled');

}
if(this.value==="External")
{
	$('#cab_disposal_passenger').val("");
	$('#cab_disposal_employeeID').val("");
$('#cab_disposal_modal').modal('show');
}


});
});

//end of custom js for diposal

//custom cab booking starts


$(document).ready(function(){
    $('#cab_book').on('change', function() {
  
if(this.value==="Self Booking")
{
	
	$('#cab_passenger').val($('#userName').val());
	$('#cab_employee').val($('#userID').val());
$('#cab_passenger').attr('readonly', true);
     $('#cab_passenger').addClass('input-disabled');
$('#cab_employee').attr('readonly', true);
     $('#cab_employee').addClass('input-disabled');


}


else
{
	
	$('#cab_passenger').val("");
	$('#cab_employee').val("");

$('#cab_passenger').attr('readonly', false);
     $('#cab_passenger').removeClass();
$('#cab_employee').attr('readonly',false);
     $('#cab_employee').addClass('input-disabled');
$('#cab_mob').attr('readonly', false);
     $('#cab_mob').removeClass();





}
if(this.value==="External")
{
	$('#cab_passenger').val("");
	$('#cab_employee').val("");
$('#cab_modal').modal('show');
}


});
});

//end of air ticket custom js



/*//code to bind the value of journey source to return journey destionation and vice versa
$(document).ready(function(){
    $('#source').keyup(function(e) {
    var txtVal = $(this).val();
    $('#returnDestination').val(txtVal);
});

}); 





$(document).ready(function(){
    $('#destination').on('change', function() {
$("#returnSource").val($(this).val());

});
});


//code to bind the value of journey source to return journey destionation and vice versa
$(document).ready(function(){
    $('#trainSource').keyup(function(e) {
    var txtVal = $(this).val();
    $('#trainReturnDestination').val(txtVal);
});

}); 


$(document).ready(function(){
    $('#trainDestination').on('change', function() {
$("#trainReturnSource").val($(this).val());

});
});

//End of code to bind the value of journey source to return journey destionation and vice versa
*/

//================end of train ticket one/two way journey hidden fields toggle js


//train ticket booking for drop down js


$('#addTP').hide();
$(document).ready(function(){
    $('#trainBookingFor').on('change', function() {
  
if(this.value==="Self Booking")
{
	$('#trainPassenger1').val($('#userName').val());
	$('#trainEmployeeID1').val($('#userID').val());

$('#trainPassenger1').attr('readonly', true);
     $('#trainPassenger1').addClass('input-disabled');
$('#trainEmployeeID1').attr('readonly', true);
     $('#trainEmployeeID1').addClass('input-disabled');
$('#addTP').hide();
}
else
{
	$('#trainPassenger1').val("");
	$('#trainEmployeeID1').val("");

$('#trainPassenger1').attr('readonly', false);
     $('#trainPassenger1').removeClass();
$('#trainEmployeeID1').attr('readonly',false);
     $('#trainEmployeeID1').addClass('input-disabled');
$('#trainMob').attr('readonly', false);
     $('#trainMob').removeClass();
}
if(this.value==="External")
{

	$('#trainPassenger1').val("");
	$('#trainEmployeeID1').val("");
	$('#addTP').show();

$('#train_modal').modal('show');
}
if(this.value==="Internal Emp.")
{
$('#addTP').hide();
}


});
});

//end of train  ticket booking foir custom js


//for avail cab  checkboxe
$(document).ready(function(){
    $('#cab').click(function() {
    if($(this).is(':checked'))
    {
        

        $('#avail_cab_row').show();

    }
    else
    {
        $('#avail_cab_row').hide();
    }
});

}); 


    
//=================================train avail cab checkbox

   //for avail cab  checkboxe
$(document).ready(function(){
   $('#trainAvailCab').click(function() {
    if($(this).is(':checked'))
    {
        

        $('#train_avail_cab_row').show();

    }
    else
    {
        $('#train_avail_cab_row').hide();
    }

});

}); 


//for avail accomodation checkbox
/*$(document).ready(function(){
    $('#trainAvailAccomodation').click(function() {
    if($(this).is(':checked'))
    {
        

        $('#train_avail_accomodation_row').show();

    }
    else
    {
        $('#train_avail_accomodation_row').hide();
    }
});

}); */

// this custom validation matches the value we have entered in the field with the pattern we have given    
//(pickup point and drop point must have atleast one alphabet bt not only numbers)
jQuery.validator.addMethod("accept", function(value, element, param) {
	if(value.match(new RegExp( param ))){
	  return value.match(new RegExp( param ));
} 

});


// filesize of upload pdf should be 10MB

$.validator.addMethod('filesize', function(value, element, param) {
    // param = size (in bytes) 
    // element = element to validate (<input>)
    // value = value of the element (file name)
    return this.optional(element) || (element.files[0].size <= param) 
});

//age should be greater than "0"
jQuery.validator.addMethod("greaterThanEighteen", function(value, element) {
    return this.optional(element) || (parseFloat(value) > 18);
});

/*// pickup point and drop point must have atleast one alphabet bt not only numbers
$.validator.addMethod('acceptPattern' , function(value , element ,param){
	return value.match(new RegExp(param));
});	*/  


//restricted mobileno continuous pattern
jQuery.validator.addMethod("matchMob", function (value, element,param) {
//if(value !== null && value !== ''){

if(value.match(new RegExp( param ))){
 return value.match(new RegExp( param ));
 
	}
//}
});


/*//restricted mobileno continuous pattern
jQuery.validator.addMethod("noContinuousNine", function (value, element,param) {
if(value.match(new RegExp( param ))){
 return value.match(new RegExp( param ));
};  
});*/

//start mob no with 7-9
jQuery.validator.addMethod("startFrom", function (value, element,param) {
if(value.match(new RegExp( param ))){
 return value.match(new RegExp( param ));
};  
});




//===========================end train avail cab check box
 $(function() {
        
        // validate ticketBookingForm  form on keyup and submit
        $("#register-form").validate({
            rules: {
            	costcentre:"required",
            	 age1:{required:true,
            		 greaterThanEighteen:true
            	 },
            	
                 one:"required",
                 two: "required",
                 food: "required",
                 bookingType : "required",
                 bookingFor : "required",
                 timeSlot : "required",
                 travelDate : "required",
                 travelClass : "required",
                 travelPurpose : "required",
                 passenger1 :{
                	 required: true,
                 },
                 employeeID1 :{
                     required: function() {
                         return $('#bookingFor').val() == 'Self Booking' || $('#bookingFor').val() == 'Internal Emp.' || $('#bookingFor').val() == '';
                       }
                     },
                 mob : {

                    required:true,
                    startFrom : /^[7-9]\d+$/,
                    minlength:10,
                   // noContinuousNine : "(?![1]{9}|[2]{9}|[3]{9}|[4]{9}|[5]{9}|[6]{9}|[7]{9}|[8]{9}|[9]{9}|[0]{9})[0-9]{9}",
                    matchMob : "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}",
                    
                 },
                 airLandline: {
                	 minlength:10,
                	 matchMob : function(){
            if($('#airLandline').val()!== ""){ 
                	return "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}";
                		}
                	 }
                	 },
                /* airNumber: {
                	 minlength:6,
                	 matchMob : "(?![1]{7}|[2]{7}|[3]{7}|[4]{7}|[5]{7}|[6]{7}|[7]{7}|[8]{7}|[9]{7}|[0]{7})[0-9]{7}",
                 },*/
                 source :{required:true,
                	 matchValue:"destination",
                 },
                 destination : {
                	 required: true,
                	 matchValue:"source",
                	
                 },
                 cost : "required",
                 returnTravelDate:{
                         required:"#two:checked",
                      },
                      returnSource:{required:"#two:checked"},
                      returnDestination : {required:"#two:checked"},
                 guestHouse : "required",
                 pickup : {
                	 required:true,
                	// accept: /^(?:[0-9]+[a-z]|[a-z]+[0-9])[a-z0-9]*$/i
                	 accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i,
                	 matchValue:"dropPoint",
                	
                 },
                 dropPoint: {required:true,
                	 accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i,
                	 matchValue:"pickup"
                 },
                 disposal: "required",
                 airlocation: {
                	 required:true,
                 },
                 checkinDate:
                 { 
                    required:true,
                    format: 'MM/DD/YYYY h:m A',
                 },
                 checkoutDate: "required",
                 upload: {
                     extension: "pdf",
                     filesize:10000000
                 },
                // airGuestType :{required : true}
        },
            messages: {
            	costcenter:"Please select cost centre",
            	age1:{required : "Please enter age",
            		greaterThanEighteen:"Age must be greater than eighteen"
            	},
one:"<br>Please choose one field",
two:"Please choose one field",
food:"Please enter required field",
bookingType:"Please enter required field",
bookingFor:"Please select booking",
timeSlot:"Please select time slot",
travelDate:"Please select travel date",
travelClass:"Please enter required field",
travelPurpose:"Please enter required field",
passenger1:{
	required : "Please enter passenger",
},
employeeID1:"Please enter employeeID",
mob:{
    required:"Please enter mobile no",
    startFrom : "Please start mobile number either from 7,8 or 9",
    minlength:"enter exact 10 digits",
    matchMob: "please do not enter continuous number",
},
airLandline: {
	 minlength:"Please enter 10 digit number",
	 matchMob : "Please do not enter continuous number",
},
/*airNumber: {
	 minlength:"Please enter atleast 6 digit number",
	 matchMob : "Please do not enter continuous number",
},*/
source:{required : "Please select source",
	matchValue:"Please enter different value than destination",
},
destination:{ required :"Please select destination",
	matchValue:"Please enter different value than source",
},
returnTravelDate : {required : "Please select return date"},
returnSource:{required : "Please select return source"},
returnDestination :{required :"Please select return destination"},
cost:"Please select cost center",
pickup:{
	required : "Please enter pick date",
	accept : "please enter atlest one alphabet",
	matchValue : "different from drop point"
},
disposal:"Please enter disposal",
guestHouse:"Please enter guest house",
dropPoint: { required : "Please enter drop point",
	accept :"please enter atleast one alphabet",
	matchValue : "different from pickup point"
	
},
location:{
	required : "Please enter city location",
},
checkinDate:"Please select check in date",
checkoutDate: "Please select check out",
upload :{ 
	extension: "File must be Pdf ",
	filesize:"File size must be less than 10MB"
		
		},
//airGuestType :{required : "please select guest Type"}
            }
            });             
});
 

 
// validation in external modal
//$('#airExtenalBtn').on('click' , function(){
 function airExtenalBtn(){
	
	if($('#airExternalGuestType').val() === ""){
		 $('#air_guesttype_error_msg').html("please select Guest Type");
		   
		 }
	else {
		$('#air_guesttype_error_msg').html("");
		}

	if($('#airExteranalCompany').val() === ""){
		 $('#air_companyname_error_msg').html("please select Company Name");
	 }
	else {
		$('#air_companyname_error_msg').html("");
	}
	if($('#airExternalGuestType').val() !== "" && $('#airExteranalCompany').val() !== ""){
		$('#air_modal').modal('hide');
	}

}

/*//validation in Add multiple Pax in airticket
$('#isMultiRequest').on('click' , function(){
	
	var totalPax = $('#airTotalPax').val();
	for(var count=2 ; count<= totalPax ;count++)
	{
	  //if( $('#passenger'+count).val() === ""){
		 // $('#error_pax'+count).html("please enter Pax Name"+count);
		console.log($('input').attr('name' , 'passenger'+count).val());
		  if($('input').attr('name' , 'passenger'+count).val() === ""){
			  $('#error_pax'+count).html("please enter Pax Name"+count);  
		  }
	  
	  else {
		  $('#error_pax'+count).html("");
	  }
	}
	
});

//validation in Add multiple Pax in airticket
$('#isTMultiRequest').on('click' , function(){
	
	var totalPax = $('#airTotalPax').val();
	for(var count=2 ; count<= totalPax ;count++)
	{
	  //if( $('#passenger'+count).val() === ""){
		 // $('#error_pax'+count).html("please enter Pax Name"+count);
		console.log($('input').attr('name' , 'passenger'+count).val());
		  if($('input').attr('name' , 'passenger'+count).val() === ""){
			  $('#error_pax'+count).html("please enter Pax Name"+count);  
		  }
	  
	  else {
		  $('#error_pax'+count).html("");
	  }
	}
	
});*/
 
//validate train ticket booking
$(function() {
        // validate ticketBookingForm  form on keyup and submit
        $("#trainRequestForm").validate({
            rules: {
            	trainCostCentre:"required",
            	age1:{ required:true,
            		greaterThanEighteen:true
            	},
                 trainOneWay:"required",
                 trainTwoWay: "required",
                 trainBookingFor:"required",
                 trainCostCentre : "required",
                 trainPrefTimeSlot : "required",
                 trainTravelDate : "required",
                 trainTravelClass : "required",
                 trainTravelPurpose : "required",
                 trainLandline: {
                	 minlength:10,
                	 matchMob : function(){
                	if($('#trainLandline').val()!==""){	 
                	return "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}";
                	}
                	 }
                	},
                 passenger1 :{
                	 required:true,
                 },
                 employeeID1 :{
                     required: function() {

                         return $('#trainBookingFor').val() == 'Self Booking' || $('#trainBookingFor').val() == 'Internal Emp.' || $('#trainBookingFor').val() == 'select..';
                       }
                     },
                 trainMob : {
                    required:true,
                    startFrom : /^[7-9]\d+$/,
                    minlength:10,
                    matchMob : "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}",                
                 },
                 trainSource : {required:true,
                	 matchValue:"trainDestination "
                 },
                 trainDestination : { required: true ,
                	 matchValue:"trainSource"
                 },
                 trainReturnTravelDate :{required :'#trainTwoWay'},
                 trainReturnSource : {required : '#trainTwoWay'},
                 trainReturnDestination : {required : '#trainTwoWay'},
                 trainPickupPoint : { required:true,
                	// accept: /^(?:[0-9]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i
                		 accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i ,
                		 matchValue:"trainDropPoint"
                },
                trainLocation : { required : true,
                },
                trainCheckinDate : "required",
                trainCheckoutDate : "required",
                trainDisposal : "required",
                
                trainDropPoint : {required:true,
                	accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i,
                	 matchValue:" trainPickupPoint"
                },
                
                trainApprovedAttachment: {
                    extension: "pdf",
                    filesize:10000000
                }
        },
            messages: {
            	trainCostCentre:"please select cost centre",
            	age1 :{required: "Please enter age",
            		greaterThanEighteen:"Age must be greater than eighteen"
            	},
 trainOneWay:"<br>Please choose one field",
 trainTwoWay:"Please enter required field",
 trainBookingFor:"Please enter BookingFor",
 trainCostCentre:"Please select cost center",
 trainPrefTimeSlot:"Please select time slot",
 trainTravelDate:"Please select travel date",
 trainTravelClass:"Please enter required field",
trainTravelPurpose:"Please enter required field",
trainLandline: {
	 minlength:"Please enter 10 digit number",
	 matchMob : "Please do not enter continuous number",
},
 passenger1:{required: "Please enter passenger name",

 },
 
 employeeID1:"Please enter employeeId",
 trainMob:{
    required:"Please enter mobile no",
    startFrom : "Please start mobile number either from 7,8 or 9",
    minlength:"enter exact 10 digits",
    matchMob : "please do not enter continuous number"
  
},
trainSource:{required :  "Please enter source",
	matchValue : "please enter different value than destination",
},
trainDestination:{required : "Please select destination",
	matchValue : "please enter different value than source",
},
trainReturnTravelDate : {required : "Please select return date"},
trainReturnSource : {required :"Please select return source"},
trainReturnDestination : {required :"Please select return destination"},

trainPickupPoint : {
	required: "Please enter  pickup point",
	accept:"Please enter atlest one alphabet",
	matchValue : "different from drop point"
},
 trainLocation : { required : "Please enter city location",
 },
 trainCheckinDate : "Please select checkin Date",
trainCheckoutDate :"Please select checkout ",
trainDisposal : "Please enter disposal",
trainDropPoint : { required : "Please enter drop point",
	accept:"Please enter atlest one alphabet",
	matchValue : "different from pickup point"
},
trainApprovedAttachment : {
 	extension:"Select  pdf file only",
 	filesize:"File size must be less than 10MB"
 
 }
                
            }
        });

});

// validation in external modal
//$('#trainExtenalBtn').on('click' , function(){
	function trainExtenalBtn(){
	if($('#trainExternalGuestType').val() === ""){
		 $('#train_guesttype_error_msg').html("please select Guest Type");
		   
		 }
	else {
		$('#train_guesttype_error_msg').html("");
		}
	if($('#trainTravelClass').val() === ""){
		 $('#train_travelclass_error_msg').html("please select Travel Class");
		   
		 }
	else {
		$('#train_travelclass_error_msg').html("");
		}

	if($('#trainExteranalCompany').val() === ""){
		 $('#train_companyname_error_msg').html("please select Company Name");
	 }
	else {
		$('#train_companyname_error_msg').html("");
	}
	if($('#trainExternalGuestType').val() !== "" && $('#trainTravelClass') && $('#trainExteranalCompany').val() !== ""){
		$('#train_modal').modal('hide');
	}
}

//validation in Add multiple Pax
/*$('#isMultiRequest').on('click' , function(){
	var totalPax = $('#airTotalPax').val();
	for(var count=2 ; count<= totalPax ;count++)
	{
	  if( $('#passenger'+count).val() === ""){
		  $('#error_pax'+count).html("please enter Pax Name"+count);
		  
	  }	
	  else {
		  $('#error_pax'+count).html("");
	  }
	}
	
});
*/






//train booking validation end      

 $(function() {
        
 // validate ticketBookingForm  form on keyup and submit
        $("#cab-form").validate({
            rules: {
                 cab_costcentre:"required",
                 bookingFor : "required",
                 cab_start :{required:true,
                	 accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i ,
                	 matchValue:"cab_end"
                 },
                 cab_end : {required:true,
                	 accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i ,
                     matchValue:"cab_start"
                 },
                 pick4uPickupDateTime:{
                	 required : true
                 },
               
                 cab_pick : "required",
                 cab_date : "required",
                 cab_travel : "required",
                 
                 cab_passenger : { required:true ,
                 },
                 cab_employee : {
                	 required: function() {
                return $('#cab_book').val() == 'Self Booking' || $('#cab_book').val() == 'Internal Emp.' || $('#cab_book').val() == '';
                	 }
                 },
                 cab_mob : {required:true,
                	 startFrom : /^[7-9]\d+$/,
                	 minlength:10,
                     matchMob : "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}",
                
                 },
                 cab_landline: {
                	 minlength:10,
                	 matchMob : function(){
                		 if($('#cab_landline').val()!== ""){
                		return "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}";
                	 }
                 }
                	 
                	 },
                 cab_cost : "required",
                
                 
               },

          messages : 
          {
            cab_costcentre:"Please select cost centre",
             bookingFor:" Please select bookingfor ",
             cab_start : {required :"Please enter start point",
            	 accept:"Please enter atleast one alphabet",
            	 matchValue : "Please enter different name than end point"
             },
             cab_end :  {required :"Please enter end point",
            	 accept:"Please enter atleast one alphabet",
            	 matchValue : "Please enter different name than start point"
             },
             pick4uPickupDateTime:{ required : "Please select pickup date"},
            
             cab_pick : " Please select pick date",
             cab_date : " Please enter  date",
             cab_travel : "Please enter travel purpose",
             cab_passenger : {required : "Please enter pax name",
             },
             cab_employee :{required : "Please enter employeeID"},
             cab_mob :{required:"Please enter mobile no",
            	 startFrom : "Please start mobile number either from 7,8 or 9 ",
            	 minlength:"Please enter exact 10 digits",
            	 matchMob : "Please do not enter continuous number"
            	   	 },
            cab_landline: {
                    	 minlength:"Please enter 10 digits",
                    	 matchMob : "Please do not enter continuous number",
                     },
             cab_cost: "Please select cost center",
             

          }

});
});


// validation in external modal
//$('#pickExtenalBtn').on('click' , function(){
function  pickExtenalBtn(){	
	if($('#pick4uExternalGuestType').val() === ""){
		 $('#pick_guesttype_error_msg').html("please select Guest Type");
		   
		 }
	else {
		$('#pick_guesttype_error_msg').html("");
		}

	if($('#Pick4uExternalCabPref').val() === ""){
		 $('#pick_cabpref_error_msg').html("please select Cab Pref");
	 }
	else {
		$('#pick_cabpref_error_msg').html("");
	}
	if($('#pick4uExternalGuestType').val() !== "" && $('#Pick4uExternalCabPref').val() !== ""){
		$('#cab_modal').modal('hide');
	}
}

// validation in external modal
//$('#dropExtenalBtn').on('click' , function(){
function  dropExtenalBtn(){	
	if($('#cabDropExternalGuestType').val() === ""){
		 $('#drop_guesttype_error_msg').html("please select Guest Type");
		   
		 }
	else {
		$('#drop_guesttype_error_msg').html("");
		}

	if($('#cabDropExternalCabPref').val() === ""){
		 $('#drop_cabpref_error_msg').html("please select Cab Pref");
	 }
	else {
		$('#drop_cabpref_error_msg').html("");
	}
	if($('#cabDropExternalGuestType').val() !== "" && $('#cabDropExternalCabPref').val() !== ""){
		$('#cab_drop_modal').modal('hide');
	}
}

// validation in external modal
//$('#disposalExtenalBtn').on('click' , function(){
	
function disposalExtenalBtn(){
	if($('#cabDisposalExternalGuestType').val() === ""){
		 $('#disposal_guesttype_error_msg').html("please select Guest Type");
		   
		 }
	else {
		$('#disposal_guesttype_error_msg').html("");
		}

	if($('#cabDisposalExternalCabPref').val() === ""){
		 $('#disposal_cabpref_error_msg').html("please select Cab Pref");
	 }
	else {
		$('#disposal_cabpref_error_msg').html("");
	}
	if($('#cabDisposalExternalGuestType').val() !== "" && $('#cabDisposalExternalCabPref').val() !== ""){
		$('#cab_disposal_modal').modal('hide');
	}
}

// validation in external modal
//$('#airportExtenalBtn').on('click' , function(){
	function airportExtenalBtn(){
	if($('#cabAirportExternalGuestType').val() === ""){
		 $('#airport_guesttype_error_msg').html("please select Guest Type");
		   
		 }
	else {
		$('#airport_guesttype_error_msg').html("");
		}

	if($('#cabAirportExternalCabPref').val() === ""){
		 $('#airport_cabpref_error_msg').html("please select Cab Pref");
	 }
	else {
		$('#airport_cabpref_error_msg').html("");
	}
	if($('#cabAirportExternalGuestType').val() !== "" && $('#cabAirportExternalCabPref').val() !== ""){
		$('#cab_airportExternal_modal').modal('hide');
	}
}





// cab pick validation ends 
 $(function() {
        
 // validate ticketBookingForm  form on keyup and submit
        $("#cab-drop-form").validate({
            rules: {
                 
            	cab_drop_costcentre:"required",
                 bookingFor : "required",
                 cab_drop_start : {required:true,
                	 accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i ,
                	 matchValue:"cab_drop_end "
                 },
                 cab_drop_end : {required:true,
                	 accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i ,
                     matchValue:"cab_drop_start "
                 },
                 cab_drop_pick : "required",
                 cab_drop_travel : "required",
                 cab_drop_passenger : { required :true,
                 },
                 cab_drop_employee : { required: function() {

                     return $('#cab_drop_book').val() == 'Self Booking' || $('#cab_drop_book').val() == 'Internal Emp.' || $('#cab_drop_book').val() == '' ;
                 }   },
                 cab_drop_mob :{required:true,
                	 startFrom : /^[7-9]\d+$/,
                	 minlength:10,
                     matchMob : "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}",
                    	 
                 },
                 cab_drop_landline: {
                	 minlength:10,
                	 matchMob : function(){
                		if($('#cab_drop_landline').val()!==""){
                		 return "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}";
                
                	 }
                 }
                	 },
                 cab_drop_cost : "required"
                
               },

          messages : 
          {
             cab_drop_costcentre:"Please select cost centre",
             bookingFor : "Please select booking for",
             cab_drop_start : {required :"Please enter start point",
            	 accept:"Please enter atleast one alphabet",
            	 matchValue : "Please enter different name than end point"
            	
             },
             cab_drop_end :{required :"Please enter end point",
            	 accept:"Please enter atleast one alphabet",
            	 matchValue : "Please enter different name than start point"
             },
             cab_drop_pick : " Please select Pickup Date",
             cab_drop_travel : "Please enter travel purpose",
             cab_drop_passenger : {required : "Please enter pax name",
             },
             cab_drop_employee :{required :"Please enter employeeID"},
             cab_drop_mob :{required:"Please enter mobile no",
            	 startFrom : "Please start mobile number either from 7,8 or 9",
            	 minlength:"Please enter exact 10 digits",
            	 matchMob : "Please do not enter continuous number",
             },
             caLb_drop_landline: {
            	 minlength:"Please enter 10 digits",
            	 matchMob : "Please do not enter continuous number",
             },
             cab_drop_cost: "Please select cost"
          }

});
});

 $(function() {
        
 // validate ticketBookingForm  form on keyup and submit
        $("#cab-disposal-form").validate({
            rules: {
                
            	cab_disposal_costcentre:"required",
            	 bookingFor : "required",
                 cab_disposal_start :{required:true,
                	accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i ,
                     
                 },
                 cab_disposal_pick : "required",
                 cab_disposal_travel : "required",
                 cab_disposal_employeeID:{
    required: function() {
     return $('#cab_disposal_book').val() == 'Self Booking' || $('#cab_disposal_book').val() == 'Internal Emp.' || $('#cab_disposal_book').val() == '' ;
                     }  
                 },
                 cab_disposal_passenger : { required : true ,
                	 
                 },
                 cab_disposal_visit_location :{required:true},
                 cab_disposal_mob : {required:true ,
                	 startFrom : /^[7-9]\d+$/,
                	 minlength:10,
                     matchMob : "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}",
                 },
                 cab_disposal_landline: {
                	 minlength:10,
                	 matchMob : function()
                	 { if($('#cab_disposal_landline').val() !== ""){
                		return "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}";
                	 }}
                	 },
                 cab_disposal_cost : "required"
                
               },

          messages : 
          {
          
        	  cab_disposal_costcentre:"Please select cost centre",
             bookingFor :" Please select booking for",
             
             cab_disposal_start : {required :"Please enter start point",
            	 accept:"Please enter atleast one alphabet",
            	
             },
             cab_disposal_visit_location :{required : "Please enter visit location"},
             cab_disposal_pick : " Please select Pickup date",
             cab_disposal_travel : "Please enter travel purpose",
             cab_disposal_employeeID :{required : "Please enter employeeID"},
             cab_disposal_passenger :  { required : "Please enter pax name ", 
             },
             cab_disposal_mob :{required:"Please enter mobile no",
            	 startFrom : "Please start mobile number either from 7,8 or 9",
            	 minlength:"Please enter exact 10 digits",
            	 matchMob:"Please do not enter continuous number"
             
             },
             cab_disposal_landline: {
            	 minlength:"Please enter 10 digits",
            	 matchMob : "Please do not enter continuous number",
             },
             cab_disposal_cost: "Please select cost center"
            

          }

});
});


 

 $(function() {
     
	 // validate ticketBookingForm  form on keyup and submit
	        $("#cab-airport-form").validate({
	            rules: {
	                
	            	cab_airport_costcentre:"required",
	            	 bookingFor : "required",
	                cab_start_airport :{required:true,
	                	accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i ,
	                	matchValue:"cab_end_airport"
	                 },
	                 cab_end_airport : {required:true,
	                	 accept: /^(?:[0-9 ]+[a-z]|[a-z]+[0-9]|[a-z])[a-z0-9 ]*$/i ,
	                     matchValue:"cab_start_airport"
	                 },
	                 cab_airport_pick : "required",
	                 cab_airport_travel : "required",
	                 cab_airport_employee :{
	    required: function() {
	    return $('#cab_airport_book').val() == 'Self Booking' || $('#cab_airport_book').val() == 'Internal Emp.' || $('#cab_airport_book').val() == '' ;
	                	      }  
	                 },
	                 cab_airport_passenger : { required : true ,
	                	 
	                 },
	                // cab_disposal_visit_location :{required:true},
	                 cab_airport_mob : {required:true,
	                	 startFrom : /^[7-9]\d+$/,
	                	 minlength : 10,
	                     matchMob : "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}",
	                	 
	                 },
	                 cab_airport_landline: {
	                	 minlength:10,
	                	 matchMob :  function() 
	                	 {
	                		 if($('#cab_airport_landline').val()!==""){
	                		return"(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}";
	                	 }}
	                	 },
	                 cab_airport_cost : "required",
	                	 other1:"required",
	                	 airstartother:"required",
		                 cab_end_airport : "required",

	                	 
	                
	               },

	          messages : 
	          {
	          
	        	 cab_airport_costcentre:"Please select cost centre",
	             bookingFor :" Please select booking for",
	             cab_start_airport : {required :"Please enter start point",
	            	 accept:"Please enter atleast one alphabet",
	            	 matchValue : "Please enter different name than end point"

	             },
	             cab_end_airport : {
	            	 required :"Please enter end point",
	            	 accept:"Please enter atleast one alphabet",
	            	 matchValue : "Please enter different name than start point"

                 },
	             //cab_disposal_visit_location :{required : "please enter visit location"},
	             cab_airport_pick : " Please select Pickup date",
	             cab_airport_travel : "Please enter travel purpose",
	             cab_airport_employee :"Please enter employeeID",
	             cab_airport_passenger :  { required : "Please enter pax name ", 
	             },
	             cab_airport_mob :{required:"Please enter mobile no",
	            	 startFrom :"Please start mobile number either from 7,8 or 9",
	            	 minlength:"Please enter exact 10 digits",
	            	matchMob:"Please do not enter continuous number"	 
	            	 
	             
	             },
	             cab_airport_landline: {
                	 minlength:"Please enter 10 digits",
                	 matchMob : "Please do not enter continuous number",
                 },
	             cab_airport_cost: "Please select cost center"
	            

	          }

	});
	});






//custom cab booking starts


$(document).ready(function(){
    $('#cab_start_airport').on('change', function() {
if(this.value==="Airport")
{ 
	 $('.cab_end_airport').hide();
	 $('#other1').show();
	 $('#airport_startother').hide();
 $('#cab_airport_modal').modal('show');
 $("#temp2").remove();
 

}
if(this.value==="Other")
{
 
    $('#airport_startother').show();
    $('.cab_end_airport').show();
	 $('#other1').hide();
	 $("#temp1").remove();
   
}


});
});

//end of air ticket custom js


//custom cab booking starts

$(document).ready(function(){
    $('#cab_end_airport').on('change', function() {
if(this.value==="Airport")
{
	$('#airport_endother').hide();
$('#cab_airport_modal').modal('show');
}


});
});

//end of air ticket custom js

//Ajax function to show/hide services or tabs according to thir status in data base

$(document).ready(function(){
	
	var url="UtilityServlet?action=GETSERVICESTATUS";
	
	$.getJSON(url, function(result){
		
		if(result['AIRTICKET']===0)
			   $('#airTicket').hide();
		if(result['TRAINTICKET']===0)
			   $('#trainTicket').hide();
		
		if(result['CAB']===0)
			   $('#cabRequest').hide();
			
	   
		   
	});
	
});





//End Ajax function to show/hide services or tabs according to thir status in data base

$(document).ready(function(){
	
	 $("form").submit(function(){
	        
	   
if($('#cab_start_airport').val()==="Airport")
{
	$("#cabAT_startPoint").val($("#external_modal_dropdown").val());
	
	$("#cabAT_endPoint").val($("#other1").val());

 

}
else
	{
	$("#cabAT_startPoint").val($("#other").val());
	$("#cabAT_endPoint").val($("#external_modal_dropdown").val());

	
	}




	 });
});


$(document).ready(function(){
	
	$('.isMultiRequest').on('click',function(){
		
		$(this).val("true");
	});
	
	
	$('#airportDropDownBtnOk').on('click',function(){
		
		if($('#cab_start_airport').val()==="Airport")
		{
			$('<input class="form-control" placeholder="other" readonly=""  style="display: block;" id="temp1" type="text">').insertAfter($('#cab_start_airport'));
			$("#temp1").val($("#external_modal_dropdown").val());
			
			$("#temp2").remove();


		 

		}
		else
			{
			
			$('<input class="form-control" placeholder="other" readonly=""  style="display: block;" id="temp2" type="text">').insertAfter($('#cab_end_airport'));
			$("#temp2").val($("#external_modal_dropdown").val());
			$("#temp1").remove();

			
			
			}

	});
	
	
	
	
	
});


