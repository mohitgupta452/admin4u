/**
 * @author Manoj Singh
 * 
 */

/*getAllStockDetails("INVENTORY");
function getAllStockDetails(service)
{
   //alert("inside");
	var url="/admin4u/rest/data/getallstockinfo/"+service;		
    $.getJSON(url, function(result){
    	 $.each(result, function(key, val){
    		 
    		// alert("inside2");
   $('#mytrtd').append('<tr><td>'+val.productDetail.categary.categary+'</td>'
		   +'<td>'+val.productDetail.brand+'</td>'
		   +'<td>'+val.productDetail.name+'</td>'
		   +'<td>'+val.availableStock+'</td>'
		   +'<td>'+val.availableStock+'</td>'
		   +'<td>'+val.availableStock+'</td>'
		   +'</tr>');
    	 });

    });
}*/

brandHtml='<option value ="">Select Brand</option>';
productsHtml='<option value ="">Select Product</option>';
products="";
function getBrandOnCat(categoryId,counter)
{
	brandHtml='<option value ="">Select Brand</option>';
	
	console.log("get product by category");
	var url="/admin4u/rest/data/getprodbycategory/"+categoryId;
	 $.getJSON(url, function(result){
		 products=result;
	        $.each(result, function(key, val)
	        		{
	        		brandHtml+='<option value="' +val.brand + '">'+val.brand+'</option>';
	        		
	        	console.log(val);
	        });
	        $("#receiptItemBrand"+counter).html(brandHtml);
	 });
	}

function getProdByBrand(brand,counter)
{
	productsHtml='<option value ="">Select Product</option>';
	console.log("get product by brand");
  	
	 $.each(products, function(key, val)
     		{
     		
		 if(val.brand === brand)
			 {
			 
			 productsHtml+='<option value="' +val.id + '">'+val.name+'</option>';
			 
			 console.log(brand,val.brand)
			 }
     	console.log(val);	
     });
     $("#receiptItemName"+counter).html(productsHtml);
}

var a=0;
var category="";
function getData(){
            	$('#total').val(a);
            	return true;
            }


function getAllcategory(counter)
{
	
	//console.log("getall category called");
	var url="/admin4u/rest/data/getallcategory";		
    $.getJSON(url, function(result){
$('#receiptItemCat'+counter).html(" ")
$('#receiptItemCat'+counter).html('<option value="">Select Category</option>');
    $.each(result, function(key, val){
        	
  $('#receiptItemCat'+counter).append('<option value="' +val.descendant.id + '">'+val.descendant['categary']+'</option>');
        	
        	console.log(val.descendant['categary']);
        	console.log(val.categary);
        });
       
    });
}

var ItemReceiptTable = function () {

	
    return {

        // main function to initiate the module
      
        init: function () {
        	
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }
            
            function editRow(oTable, nRow ,Counter) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                
                jqTds[0].innerHTML = '<select onchange="getBrandOnCat(this.value,' +Counter+');addRowButton('+Counter+') " class="form-control small select2" name="receiptItemCat'+Counter+'"'+'id="receiptItemCat'+Counter+'"'+'style="width: 100%;"></select><span id="error_msg_cat'+Counter+'" style="color:red"></span>';
                jqTds[1].innerHTML = '<select onchange="getProdByBrand(this.value,'+Counter+'); addRowButton('+Counter+')" class="form-control small select2" name="receiptItemBrand'+Counter+'"'+'id="receiptItemBrand'+Counter+'"'+'style="width: 100%;">'+'</select><span id="error_msg_brand'+Counter+'" style="color:red"></span>';
                jqTds[2].innerHTML = '<select onchange="addRowButton('+Counter+')" class="form-control small select2" name="receiptItemName'+Counter+'"'+'id="receiptItemName'+Counter+'"'+'style="width: 100%;">'+'</select><span id="error_msg_name'+Counter+'" style="color:red"></span>';
                jqTds[3].innerHTML = '<input  onkeyup="addRowButton('+Counter+')" type="text" class="form-control small" name="itemQtyOrd'+Counter+'"'+'id="itemQtyOrd'+Counter+'"'+'><span id="error_msg_ord'+Counter+'" style="color:red"></span>';
                jqTds[4].innerHTML = '<input  onkeyup="addRowButton('+Counter+')" type="text" class="form-control small" name="itemQtyRec'+Counter+'"'+'id="itemQtyRec'+Counter+'"'+'><span id="error_msg_rec'+Counter+'" style="color:red"></span>';
                jqTds[5].innerHTML = '<input  onkeyup="addRowButton('+Counter+')" type="text" class="form-control small" name="itemUnitPrice'+Counter+'"'+'id="itemUnitPrice'+Counter+'"'+'><span id="error_msg_price'+Counter+'" style="color:red"></span>';
                          a = Counter;  

            }
            var oTable = $('#editable-sample').dataTable({
                "aLengthMenu": [
                    [-1],
                    ["All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": -1,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify
																											// table
																											// search
																											// input
            jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify
																											// table
																											// per
																											// page
																											// dropdown

            var nEditing = null;
            var globalCounter = 0;
            if(a == 0)
                getAllcategory();
            $('#editable-sample_new').click(function (e) {
            	
           
            	$('#editable-sample_new').attr('disabled' , true);
              

            	globalCounter++;
            	getAllcategory(globalCounter);
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '',
                        '', ''
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow ,globalCounter);
                nEditing = nRow;
            });
         }
    };
}();


/*
//$('#editable-sample_new').attr('disabled' , true);

function abledAddRowButtuon(counter)
{
	alert(counter);

	if($('#receiptItemCat'+counter).val() !=="" && $('#receiptItemBrand'+counter).val() !=="" && $('#receiptItemName'+counter).val()!=="" && $('#itemQtyOrd'+counter).val()!=="" && $('#itemQtyRec'+counter).val()!=="" && $('#itemUnitPrice'+counter).val()!=="")
{
	$('#editable-sample_new').attr('disabled' , false);
}	
else
	{
	$('#editable-sample_new').attr('disabled' , true);
	}

}*/

function addRowButton(counter)
{
    if($('#receiptItemCat'+counter).val() !=="" && $('#receiptItemBrand'+counter).val() !=="" && $('#receiptItemName'+counter).val()!=="" && $('#itemQtyOrd'+counter).val()!=="" && $('#itemQtyRec'+counter).val()!=="" && $('#itemUnitPrice'+counter).val()!=="")
    {
    	$('#editable-sample_new').attr('disabled' , false);
    }	
    else
    	{
    	$('#editable-sample_new').attr('disabled' , true);
    	}
    
    if($('#receiptItemCat'+counter).val() !=="")
    {  $('#error_msg_cat'+counter).html("");
    }
    if ($('#receiptItemBrand'+counter).val() !=="")
    {	  $('#error_msg_brand'+counter).html("");}
    if ($('#receiptItemName'+counter).val() !=="")
    {	  $('#error_msg_name'+counter).html(""); }
    if($('#itemQtyOrd'+counter).val() !=="")
    {      $('#error_msg_ord'+counter).html("");	}
    if($('#itemQtyRec'+counter).val() !=="")
    {	  $('#error_msg_rec'+counter).html("");}
    if($('#itemUnitPrice'+counter).val() !=="")
    {	  $('#error_msg_price'+counter).html("");}
    
}

//validation

$(function() {

$("#ItemReceiptServlet").validate({
    rules: {
    	pNumber : {required : true},
    	prdRecSupplier : {required : true},
    	
    },
    messages:{
    	pNumber : {required : "please  enter the number"},
    	prdRecSupplier : {required : "please select supplier"},
    	
    	}
    });

});



$('#pdtRecSubmit').on('click' , function(){
var counter = $('#editable-sample >tbody >tr').length;
for (var i =1; i<=counter ; i++)
	{
if($('#receiptItemCat'+i).val() ==="")
{  $('#error_msg_cat'+i).html("please select category");
}
if ($('#receiptItemBrand'+i).val() ==="")
{	  $('#error_msg_brand'+i).html("please select brand");}
if ($('#receiptItemName'+i).val() ==="")
{	  $('#error_msg_name'+i).html("please select name"); }
if($('#itemQtyOrd'+i).val() ==="")
{      $('#error_msg_ord'+i).html("please select Ord");	}
if($('#itemQtyRec'+i).val() ==="")
{	  $('#error_msg_rec'+i).html("please enter Rec");}
if($('#itemUnitPrice'+i).val() ==="")
{	  $('#error_msg_price'+i).html("please enter price");}

	}
});



