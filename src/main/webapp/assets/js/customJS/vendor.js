


// only number will be enter in the field
$('.numbersOnly').keyup(function () {
    if (this.value != this.value.replace(/[^0-9]/g, '')) {
       this.value = this.value.replace(/[^0-9]/g, '');
    }
});


// vendor cancel check upload file name 
$("#cancelCheck").on('change',function(){
	$('#cancelFileName').html("");
	var cFile = $('input[name=cancelCheck]').val().replace(/C:\\fakepath\\/i, '');
$('#cancelFileName').append(cFile + ' <a href="javascript:callCrossButtonOfCancel();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	});

//vendor pancard upload file name
$("#panCard").on('change',function(){
		$('#panFileName').html("");
	var pFile = $('input[name=panCard]').val().replace(/C:\\fakepath\\/i, '');
$('#panFileName').append(pFile + ' <a href="javascript:callCrossButtonOfPan();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	});

//vendor dec Letter upload file name
$("#decLetter").on('change',function(){
		$('#decFileName').html("");
	var dFile = $('input[name=decLetter]').val().replace(/C:\\fakepath\\/i, '');
$('#decFileName').append( dFile + ' <a href="javascript:callCrossButtonOfDec();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	});

//vendor esc Attach  upload file name
$("#escAttach").on('change',function(){
		$('#escFileName').html("");
	var eFile = $('input[name=escAttach]').val().replace(/C:\\fakepath\\/i, '');
$('#escFileName').append(eFile + ' <a href="javascript:callCrossButtonOfEsc();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	});

// mis billing upload file name 
$("#billAttach").on('change',function(){
	$('#billingFile').html("");
	var filename = $('input[name=billAttach]').val().replace(/C:\\fakepath\\/i, '');
$('#billingFile').append(filename + ' <a href="javascript:callCrossButtonOfBilling();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	});

//mis contract upload file name 
$("#contractAttach").on('change',function(){
	$('#conFile').html("");
	var filename = $('input[name=contractAttach]').val().replace(/C:\\fakepath\\/i, '');
$('#conFile').append(filename + ' <a href="javascript:callCrossButtonOfContract();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	});

//mis commercial upload file name 
$("#commAttach").on('change',function(){
	$('#commFile').html("");
	var filename = $('input[name=commAttach]').val().replace(/C:\\fakepath\\/i, '');
$('#commFile').append(filename + ' <a href="javascript:callCrossButtonOfCommercial();"><i class="fa fa-times" aria-hidden="true" ></i></a>');
	});

function callCrossButtonOfCancel(){

	$('#cancelFileName').html("");
	$("#cancelCheck").val("");
}

function callCrossButtonOfPan(){

	$('#panFileName').html("");
	$("#panCard").val("");
}

function callCrossButtonOfDec(){

	$('#decFileName').html("");
	$("#decLetter").val("");
}

function callCrossButtonOfEsc(){

	$('#escFileName').html("");
	$("#escAttach").val("");
}

function callCrossButtonOfContract(){

	$('#conFile').html("");
	$("#contractAttach").val("");
}

function callCrossButtonOfBilling(){

	$('#billingFile').html("");
	$("#billAttach").val("");
}

function callCrossButtonOfCommercial(){

	$('#commFile').html("");
	$("#commAttach").val("");
}



//redirect response on current open tab instead of active tab
$(document).ready(function(){
$('#myTab a').click(function(e) {
	  e.preventDefault();
	  $(this).tab('show');
	});

// store the currently selected tab in the window.name which is stabled till the window is closed
$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
	  var id = $(e.target).attr("href").substr(1);
	  window.name = id;
	});

//select the type of MIS
$('#misReport').on('change',function(){
	var misType = $(this).attr("value");
	localStorage.setItem('selectedTab', misType);
});
var hashID = "#"+window.name;

	// on load of the page: switch to the currently selected tab
    $('#myTab a[href= "' + hashID + '"]').tab('show');
    var misID = localStorage.getItem('selectedTab');
if(window.name === 'mis_report'){
    if(misID === 'Billing MIS'){
    	 $(".commercial ,.contract").hide();
    	// document.getElementById('misReport').getElementsByTagName('option')[0].selected = true;
    	 $('#misReport').val('Billing MIS').trigger('change');
     	 $(".billing").show();
    }
    else if(misID === 'Commercial MIS'){
    	 $(".contract, .billing").hide();
    	// document.getElementById('misReport').getElementsByTagName('option')[1].selected = true;
    	 $('#misReport').val('Commercial MIS').trigger('change');
    	 $(".commercial").show();
        }
    else if(misID === 'Contract Management'){
    	 $(".commercial , .billing ").hide();
    	// document.getElementById('misReport').getElementsByTagName('option')[2].selected = true;
    	 $('#misReport').val('Contract Management').trigger('change');
    	 $(".contract").show();
        }
}
});



//MIS Report dropdown 

$(document).ready(function(){
    $('#misReport').on('change',function(){
        if($(this).attr("value")=="Billing MIS"){
        	   $(".commercial").hide();
               $(".contract").hide();
               $(".billing").show();
           
        }
        if($(this).attr("value")=="Commercial MIS"){ 
        	$(".billing").hide();
            $(".contract").hide();
            $(".commercial").show();
              
        }
        if($(this).attr("value")=="Contract Management"){
        	 $(".billing").hide();
             $(".commercial").hide();
             $(".contract").show();
            
            
        }
    });
});


//If Mailing address is same as address then autofill the mailing address fields

$('#sameAddress').click(function(){
	   if ($('#sameAddress').attr('checked')) {
	$('#mailCity').val($('#city').val());
	$('#mailCity').attr('readonly', true);
    
	$('#mailStreet').val($('#street').val());
	$('#mailStreet').attr('readonly', true);
    
	$('#mailLocality').val($('#locality').val());
	$('#mailLocality').attr('readonly', true);
    
	$('#mailPin').val($('#pin').val());
	$('#mailPin').attr('readonly', true);
    
	$('#mailCountry').val($('#country').val());
	$('#mailCountry').attr('readonly', true);
    
	$('#mailState').val($('#state').val());
	$('#mailState').attr('readonly', true);
	   }
	   else
		   {
			$('#mailCity').val("");
			$('#mailCity').attr('readonly', false);
			
			$('#mailStreet').val("");
			$('#mailStreet').attr('readonly', false);
			
			$('#mailLocality').val("");
			$('#mailLocality').attr('readonly', false);
			
			$('#mailPin').val("");
			$('#mailPin').attr('readonly', false);
			
			$('#mailCountry').val("");
			$('#mailCountry').attr('readonly', false);
			
			$('#mailState').val("");
			$('#mailState').attr('readonly', false);
		   }
	
});


//Total Payable Rent Value
$('#totalRentPayable').val(0)
function sum(){
var result = parseInt($('#monthlyRent').val())+parseInt($('#monthlyMaintenance').val())+parseInt($('#powerBack').val())+parseInt($('#parkingCharges').val())+parseInt($('#fixedCost').val());
$('#totalRentPayable').val(result) ;
}

// automate vendor name

function automatevendor(){
	var vendor = $('#vendorName').val();
	
	var option = $("<option>").val(vendor).text(vendor);
	$('#companyName option:first').after(option);
	$('#companyName').val(vendor).trigger('change');
	//$('#companyName').find(option).prop('selected', true);
	//var option = $("<option>").val(vendor).text(vendor);
	$('#paymentCompanyName option:first').after(option);
	$('#paymentCompanyName').val(vendor).trigger('change');
	//$('#paymentCompanyName').find(option).prop('selected', true);
	$('#bvendorName').val(vendor).attr("readonly" , true);
	$('#commercialVendorName').val(vendor).attr("readonly" , true);
}


//restricted mobileno continuous pattern
jQuery.validator.addMethod("matchMob", function (value, element,param) {
if(value.match(new RegExp( param ))){
 return value.match(new RegExp( param ));
	}
});


// jquery validation

// vendor Management validation
$(function() {

    $("#vendor_form").validate({
        rules: {
        	vendorName :{ required : true},
        	vLegalName : {required : true},
        	businessDescription : {required : true},
        	mailingAddress :{required : true},
        	companyPhone : {required : true,
      // startFrom : /^[7-9]\d+$/,
       minlength:10,
       matchMob : "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}",
        	},
        	companyFax :{required : true},
        	serviceTax : {required : true},
        	tin : {required : true},
        	identificationNo:{required : true},
        	stateIncorporation:{required : true},
        	incorporationYear : {required : true},
        	deliveryMethod :{required : true},
        	cancelCheck : {required : true},
        	panCard : {required : true},
        	decLetter :{required : true},
        	escAttach :{required : true},
        	addressInfo :{required : true},
        	city:{required :true},
        	street:{required :true},
        	locality:{required :true},
        	country:{required :true},
        	state:{required :true},
        	pin:{required :true},
        	mailCity:{required :true},
        	mailStreet:{required :true},
        	mailLocality:{required :true},
        	mailCountry:{required :true},
        	mailState:{required :true},
        	mailPin:{required :true},
        	
        	
        	
         },
        messages:{
        	
        	vendorName :{ required :"Please enter vendor name "},
        	vLegalName : {required :"Please enter vendor legal name"},
        	businessDescription : {required :"Please enter business description"},
        	mailingAddress :{required :"Please enter mailing adddress"},
        	companyPhone : {required :"Please enter company phone",
        		//startFrom : "Please start mobile number either from 7,8 or 9",
        	    minlength:"Please enter exact 10 digits",
        	    matchMob: "Please do not enter continuous number",
        	},
        	companyFax :{required :"Please enter company fax "},
        	serviceTax : {required :"Please enter service fax"},
        	tin : {required :"Please enter tin"},
        	identificationNo:{required :"Please enter identification no "},
        	stateIncorporation:{required :"Please enter state incorporation"},
        	incorporationYear : {required :"Please enter incorporation year "},
        	deliveryMethod :{required :"Please enter delivery method"},
        	cancelCheck : {required :"Please attach cancel check"},
        	panCard : {required :"Please attach pan card"},
        	decLetter :{required :"Please attach declaration letter"},
        	escAttach :{required :"Please attach escalation attach"},
        	addressInfo :{required :"Please enter address information"},
        	city:{required :"Please enter city"},
        	street:{required :"Please enter street"},
        	locality:{required :"Please enter locality"},
        	country:{required :"Please enter country"},
        	state:{required :"Please enter state"},
        	pin:{required :"Please enter pin"},
        	mailCity:{required :"Please enter city"},
        	mailStreet:{required :"Please enter street"},
        	mailLocality:{required :"Please enter locality"},
        	mailCountry:{required :"Please enter country"},
        	mailState:{required :"Please enter state"},
        	mailPin:{required :"Please enter pin"},
        	
        }
    });
});

// contact information validation
$(function(){
	
	$("#contact_form").validate({
		rules: {
			    companyName:{required : true},
			    salutation:{required : true},
				firstName:{required : true},
				lastName :{required : true},
				designation:{required : true},
				companyDepart:{required : true},
				comEmail :{required : true},
				contactStateIncorporation:{required : true},
				contactIncorporationYear:{required : true},
				deliveryMethod:{required : true},
				contactNo:{required : true}
		},
	messages :{
		
		companyName:{required:"Please enter company name"},
	    salutation:{required:"Please enter salutation"},
		firstName:{required:"Please enter first name "},
		lastName :{required:"Please enter last name"},
		designation:{required:"Please enter designation"},
		companyDepart:{required:"Please enter department"},
		comEmail :{required:"Please enter email"},
		contactStateIncorporation:{required:"Please enter state incorporation"},
		contactIncorporationYear:{required:"Please enter incorporation year"},
		deliveryMethod:{required:"Please enter delivery method"},
		contactNo:{required:"Please enter contact no "}
	}
	});	
	
});


// payment terms validation
$(function(){
	$("#payment_form").validate({
		
		rules:{
			paymentCompanyName:{required:true},
			paymentTerms :{required:true},
			shippingTerms:{required:true},
			shippingMethod:{required:true},
			penalityTerm:{required:true},
			advance:{required:true},
			
		},
		messages:{
			paymentCompanyName:{required :"Please enter company name"},
			paymentTerms :{required:"Please enter payment terms"},
			shippingTerms:{required:"Please enter shipping terms"},
			shippingMethod:{required:"Please enter shipping method"},
			penalityTerm:{required:"Please enter penality terms"},
			advance:{required:"Please enter advance"},
			
			
		}
	});
});

// MIS report validation

//billing MIS  validation
$(function(){
	
	$("#billing_form").validate({
		
		rules:{
			expenses :{required : true},
			bvendorName:{required : true},
			company:{required : true},
			poNo:{required : true},
			invoiceReceiveDate:{required : true},
			invoiceNo :{required : true},
			invoiceDate:{required : true},
			invoiceAmt:{required : true},
			processAmt:{required : true},
			billDate:{required : true},
			period:{required : true},
			costCentre:{required : true},
			billLocation:{required : true},
			checkReceiveDate:{required : true},
			checkNo:{required : true},
			checkAmt:{required : true},
			billRemark:{required : true}
			
			
		},
		messages :{
			
			expenses :{required : "Please enter expenses"},
			bvendorName:{required : "Please enter vendor name"},
			company:{required : "Please enter comapny"},
			poNo:{required : "Please enter po no"},
			invoiceReceiveDate:{required : "Please enter invoice receive date"},
			invoiceNo :{required : "Please enter invoice no"},
			invoiceDate:{required : "Please enter invoice date "},
			invoiceAmt:{required : "Please enter invoice amt"},
			processAmt:{required : "Please enter process amt"},
			billDate:{required : "Please enter bill date"},
			period:{required : "Please enter period"},
			costCentre:{required : "Please enter cost centre"},
			billLocation:{required : "Please enter location"},
			checkReceiveDate:{required : "Please enter check receive date"},
			checkNo:{required : "Please enter check no"},
			checkAmt:{required : "Please enter check amt"},
			billRemark:{required : "Please enter remark"}
			
		}
		
	});
});

// commercial MIS validation
$(function(){
	
	$("#commercial_form").validate({

		rules:{
			   commercialVendorName:{required : true},
			   commercialDescription:{required : true},
			   commercialCompanyCode:{required : true},
			   commercialCostCentre:{required : true},
			   commercialCategory:{required : true},
			   prNumber:{required : true},
			   prAccount:{required : true},
			   prCreationDate:{required : true},
			   releasedFromDeptGm:{required : true},
			   releasedFromFh:{required : true},
			   releasedFromGhk:{required : true},
			   poNumber:{required : true},
			   poAmount:{required : true},
			   releasedFromComm:{required : true},
			   releasedFromFinance:{required : true},
			   relStatus:{required : true},
			   createdBy:{required : true},
			   fromTo:{required : true},
			   advanceTerms:{required : true},
			   commercialRemark:{required : true}
			
		},
		messages:{
			
			   commercialVendorName:{required :"Please enter vendor name"},
			   commercialDescription:{required :"Please enter description"},
			   commercialCompanyCode:{required :"Please enter company code"},
			   commercialCostCentre:{required :"Please enter cost centre"},
			   commercialCategory:{required :"Please enter category"},
			   prNumber:{required :"Please enter pr number"},
			   prAccount:{required :"Please enter pr account"},
			   prCreationDate:{required :"Please enter pr creation date"},
			   releasedFromDeptGm:{required :"Please enter released from dept GM"},
			   releasedFromFh:{required :"Please enter released from FH"},
			   releasedFromGhk:{required :"Please enter released From GHK"},
			   poNumber:{required :"Please enter  po number"},
			   poAmount:{required :"Please enter po amount"},
			   releasedFromComm:{required :"Please enter released from comm"},
			   releasedFromFinance:{required :"Please enter relesed from finance "},
			   relStatus:{required :"Please enter rel status"},
			   createdBy:{required :"Please enter created by"},
			   fromTo:{required :"Please enter from to"},
			   advanceTerms:{required :"Please enter advance terms"},
			   commercialRemark:{required :"Please enter remark"}
		},
	});
});
	
// contract management validation
$(function(){
	$("#contract_form").validate({
		rules:{
			lesserName:{required:true},
			lesseeName:{required:true},
			agreementType:{required:true},
			address:{required:true},
			landlordName:{required:true},
			contactName:{required:true},
			totalArea:{required:true},
			leaseCommDate:{required:true},
			leaseEndDate:{required:true},
			initiated:{required:true},
			noticePeriod:{required:true},
			lockPeriod :{required:true},
			escalation : {required:true},
			escalationDate:{required:true},
			securityDeposit:{required:true},
			monthlyRent:{required:true},
			monthlyMaintenance:{required:true},
			powerBack:{required:true},
			parkingCharges:{required:true},
			fixedCost:{required:true},
			totalRentPayable:{required:true},
			paymentDate:{required:true}
			
		},
	messages :{
		
		lesserName:{required:"Please enter lesser name"},
		lesseeName:{required:"Please enter lessee name"},
		agreementType:{required:"Please enter agreement type"},
		address:{required:"Please enter address "},
		landlordName:{required:"Please enter landlord name"},
		contactName:{required:"Please enter contact name"},
		totalArea:{required:"Please enter total name"},
		leaseCommDate:{required:"Please enter lease comm date "},
		leaseEndDate:{required:"Please enter lease end date "},
		initiated:{required:"Please enter initiated"},
		noticePeriod:{required:"Please enter notice period"},
		lockPeriod :{required:"Please enter lock period"},
		escalation : {required:"Please enter escalation"},
		escalationDate:{required:"Please enter escalation Date"},
		securityDeposit:{required:"Please enter security deposit"},
		monthlyRent:{required:"Please enter monthly rent",
			
			},
		monthlyMaintenance:{required:"Please enter monthly maintenance"},
		powerBack:{required:"Please enter power back"},
		parkingCharges:{required:"Please enter parking charges"},
		fixedCost:{required:"Please enter fixed cost "},
		totalRentPayable:{required:"Please enter total rent payable "},
		paymentDate:{required:"Please enter payment date"}
		
	}
		
	});
});


//  inactive function for payment terms

function callPaymentInactive(id)
{
	var url="PaymentTermsServlet?action=callPaymentInActive&ID="+id;
	 $.getJSON(url, function(result){
		 $("#pid"+id).hide();
		 });
}

//inactive function for contact terms
function callContactInactive(id)
{
	var url="ContactInformationServlet?action=callContactInActive&ID="+id;
	 $.getJSON(url, function(result){
		 $("#cid"+id).hide();
		 });
}


// Edit the vendor information  by clicking on edit buttton 
function callVendorEditFunction(id){

	   var table = $('#hidden-table-info').tableToJSON({});
	 var t = JSON.stringify(table);
	 var tp = JSON.parse(t);
	 var cancelFile = tp[id.rowIndex-1]['Cancel Check'].replace(/^.*[\\\/]/, '');
	 $('#cancelFileName').html("");
	 $('#cancelFileName').append(cancelFile+ '<a href="javascript:callCrossButtonOfCancel();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	 var escFile = tp[id.rowIndex-1]['Escalation Attachment'].replace(/^.*[\\\/]/, '');
	 $('#escFileName').html("");
	 $('#escFileName').append(escFile+ '<a href="javascript:callCrossButtonOfEsc();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	 var panFile = tp[id.rowIndex-1]['Pan Card'].replace(/^.*[\\\/]/, '');
	 $('#panFileName').html("");
	 $('#panFileName').append(panFile+ '<a href="javascript:callCrossButtonOfPan();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	 var decFile = tp[id.rowIndex-1]['Declaration Letter'].replace(/^.*[\\\/]/, '');
	 $('#decFileName').html("");
	 $('#decFileName').append(decFile+ '<a href="javascript:callCrossButtonOfDec();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	        $('#vendorInfoId').val(tp[id.rowIndex-1]['ID'])
			$('#vendorName').val(tp[id.rowIndex-1]['Company Name']);
	    	$('#vLegalName').val(tp[id.rowIndex-1]['Vendor Name']);
	    	$('#businessDescription').val(tp[id.rowIndex-1]['Buisness Description']);
	    	$('#companyPhone').val(tp[id.rowIndex-1]['Company Phone']);
	    	$('#companyFax').val(tp[id.rowIndex-1]['Company Fax']);
	    	$('#serviceTax').val(tp[id.rowIndex-1]['Service Tax No']);
	    	$('#identificationNo').val(tp[id.rowIndex-1]['Identification No']);
	    	$('#incorporationYear').val(tp[id.rowIndex-1]['Year Of Incorporation']);
	    	$('#tin').val(tp[id.rowIndex-1]['Tin']);
	    	$('#stateIncorporation').val(tp[id.rowIndex-1]['State Of Incorporation']);
	    	$('#deliveryMethod').val(tp[id.rowIndex-1]['Preffered Delivery Method']);
	    	$('#city').val(tp[id.rowIndex-1]['City']);	
	    	$('#street').val(tp[id.rowIndex-1]['Street']);	
	    	$('#locality').val(tp[id.rowIndex-1]['Locality']);	
	    	$('#pin').val(tp[id.rowIndex-1]['Pin']);	
	    	$('#state').val(tp[id.rowIndex-1]['State']);	
	    	$('#country').val(tp[id.rowIndex-1]['Country']);	
	    	$('#mailCity').val(tp[id.rowIndex-1]['MCity']);	
	    	$('#mailStreet').val(tp[id.rowIndex-1]['MStreet']);	
	    	$('#mailLocality').val(tp[id.rowIndex-1]['MLocality']);	
	    	$('#mailPin').val(tp[id.rowIndex-1]['MPin']);	
	    	$('#mailState').val(tp[id.rowIndex-1]['MState']);	
	    	$('#mailCountry').val(tp[id.rowIndex-1]['MCountry']);	
		   	if(tp[id.rowIndex-1]['CheckBox'] === "on")
		 {
		   		$('#sameAddress').attr('Checked' , true);
		   		$('#mailCity').attr('readonly', true);	
		    	$('#mailStreet').attr('readonly', true);	
		    	$('#mailLocality').attr('readonly', true);
		    	$('#mailPin').attr('readonly', true);
		    	$('#mailState').attr('readonly', true);	
		    	$('#mailCountry').attr('readonly', true);	
		 }  
	    	
	    	
	    	// file upload
	    	$('#cancelCheck').val(tp[id.rowIndex-1]['Cancel Check']);
	    	$('#panCard').val(tp[id.rowIndex-1]['Pan Card']);
	    	$('#decLetter').val(tp[id.rowIndex-1]['Declaration Letter']);
	    	$('#escAttach').val(tp[id.rowIndex-1]['Escalation Attachment']);
	    	
}


//Edit the Billing information  by clicking on edit buttton 
function callBillingEditFunction(billingRowId){
	
      var table =	$('#mis_bill_hidden-table-info').tableToJSON({});
      var t = JSON.stringify(table);
  	  var tp = JSON.parse(t);
	 // var bFile = tp[billingRowId.rowIndex-1]['BillUpload'].replace(/^.*[\\\/]/, '');
	//  $('#billingFile').html("");
    //  $('#billingFile').append(bFile+ '<a href="javascript:callCrossButtonOfBilling();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

 
 	  $('#expenses').val(tp[billingRowId.rowIndex-1]['Nature Of Expenses']);
  	 /* $('#vendorName').val(tp[billingRowId.rowIndex-1]['Vendor Name']);*/
  	  $('#company').val(tp[billingRowId.rowIndex-1]['Comapany']);
  	  $('#poNo').val(tp[billingRowId.rowIndex-1]['Po No']);
  	  $('#invoiceReceiveDate').val(tp[billingRowId.rowIndex-1]['Invoice Receiving Date'].replace(":00.0" , ""));
  	  $('#invoiceNo').val(tp[billingRowId.rowIndex-1]['Invoice No.']);
  	  $('#invoiceDate').val(tp[billingRowId.rowIndex-1]['Invoice Date'].replace(":00.0" , ""));
  	  $('#invoiceAmt').val(tp[billingRowId.rowIndex-1]['Invoice Amt']);
  	  $('#processAmt').val(tp[billingRowId.rowIndex-1]['Process Amt']);
  	 /* $('#billDate').val(tp[billingRowId.rowIndex-1]['Bill submission Date'].replace(":00.0" , ""));*/
  	 /* $('#period').val(tp[billingRowId.rowIndex-1]['Period'].replace(":00.0" , ""));*/
  	  $('#costCentre').val(tp[billingRowId.rowIndex-1]['Cost Centre']);
  	 /* $('#billLocation').val(tp[billingRowId.rowIndex-1]['Location']);*/
  	  $('#checkReceiveDate').val(tp[billingRowId.rowIndex-1]['Check Receive Date'].replace(":00.0" , ""));
  	  $('#checkNo').val(tp[billingRowId.rowIndex-1]['Check No']);
  	  $('#checkAmt').val(tp[billingRowId.rowIndex-1]['Check Amt']);
  	  $('#billRemark').val(tp[billingRowId.rowIndex-1]['Remark']);
  	  $('#billId').val(tp[billingRowId.rowIndex-1]['ID']);
  	 /* $('#billAttach').val(bFile);*/
}

//Edit the Contract information  by clicking on edit buttton 
function callContractEditFunction(contractRowId)
{
    var table =	$('#mis_contract_hidden-table-info').tableToJSON({});
   // alert(table);
    var t = JSON.stringify(table);
   // alert(t);
	var tp = JSON.parse(t);
	//alert(tp)
    var cFile = tp[contractRowId.rowIndex-1]['Contract Upload'].replace(/^.*[\\\/]/, '');
    $('#conFile').html("");
	$('#conFile').append(cFile+ '<a href="javascript:callCrossButtonOfContract();"><i class="fa fa-times" aria-hidden="true" ></i></a>');
	
	$('#contractId').val(tp[contractRowId.rowIndex-1]['ID']);
	$('#lesserName').val(tp[contractRowId.rowIndex-1]['Lesser Name']);
	$('#lesseeName').val(tp[contractRowId.rowIndex-1]['Lessee Name']);
	$('#agreementType').val(tp[contractRowId.rowIndex-1]['Agreement Type']);
/*	$('#address').val(tp[contractRowId.rowIndex-1]['Address']);*/
	$('#landlordName').val(tp[contractRowId.rowIndex-1]['Landlord Name']);
/*	$('#contactName').val(tp[contractRowId.rowIndex-1]['Contact Number']);*/
	$('#leaseEndDate').val(tp[contractRowId.rowIndex-1]['Lease End Date'].replace(":00.0" , ""));
	$('#leaseCommDate').val(tp[contractRowId.rowIndex-1]['Lease Commencement'].replace(":00.0" , ""));
	$('#totalArea').val(tp[contractRowId.rowIndex-1]['Total Area']);
	$('#lockPeriod').val(tp[contractRowId.rowIndex-1]['Lock In Period'].replace(":00.0" , ""));
	$('#noticePeriod').val(tp[contractRowId.rowIndex-1]['Notice Period'].replace(":00.0" , ""));
	$('#initiated').val(tp[contractRowId.rowIndex-1]['Process Initiated']);
	$('#escalation').val(tp[contractRowId.rowIndex-1]['Escalation']);
	$('#escalationDate').val(tp[contractRowId.rowIndex-1]['Next Escalation'].replace(":00.0" , ""));
	$('#securityDeposit').val(tp[contractRowId.rowIndex-1]['Security Deposit']);
	$('#monthlyRent').val(tp[contractRowId.rowIndex-1]['Monthly Rent']);
	$('#monthlyMaintenance').val(tp[contractRowId.rowIndex-1]['Monthly Maintenance']);
	$('#powerBack').val(tp[contractRowId.rowIndex-1]['Power Back Up']);
	$('#parkingCharges').val(tp[contractRowId.rowIndex-1]['Parking Charges']);
	$('#fixedCost').val(tp[contractRowId.rowIndex-1]['Any Other Fixed Cost']);
	$('#totalRentPayable').val(tp[contractRowId.rowIndex-1]['Total Rent Payable']);
	$('#paymentDate').val(tp[contractRowId.rowIndex-1]['Payment Date'].replace(":00.0" , "")); 
	$('#contractAttach').val(/C:\\fakepath\\/i+cFile);

	  
	}
 

function  callCommercialEditFunction(commercialRowId)
{
    var table =	$('#mis_comm_hidden-table-info').tableToJSON({});
    var t = JSON.stringify(table);
	var tp = JSON.parse(t);
	var commFile = tp[commercialRowId.rowIndex-1]['Commercial Upload'].replace(/^.*[\\\/]/, '');$('#commFile').html("");
$('#commFile').html("");
$('#commFile').append(commFile+ '<a href="javascript:callCrossButtonOfCommercial();"><i class="fa fa-times" aria-hidden="true" ></i></a>');

	
	$('#poAmount').val(tp[commercialRowId.rowIndex-1]['PO Amount']);
	$('#poNumber').val(tp[commercialRowId.rowIndex-1]['PO Number']);
	$('#releasedFromComm').val(tp[commercialRowId.rowIndex-1]['Released From Comm'].replace(":00.0" , ""));
	$('#releasedFromFinance').val(tp[commercialRowId.rowIndex-1]['Released From Finance'].replace(":00.0" , ""));
	$('#relStatus').val(tp[commercialRowId.rowIndex-1]['Rel Status']);
	$('#createdBy').val(tp[commercialRowId.rowIndex-1]['Created By'].replace(":00.0" , ""));
	$('#from').val(tp[commercialRowId.rowIndex-1]['From'].replace(":00.0" , ""));
	$('#to').val(tp[commercialRowId.rowIndex-1]['To'].replace(":00.0" , ""));
	$('#prAccount').val(tp[commercialRowId.rowIndex-1]['PR Amount']);
	$('#prNumber').val(tp[commercialRowId.rowIndex-1]['PR Number']);
	$('#releasedFromGhk').val(tp[commercialRowId.rowIndex-1]['Released From BHR'].replace(":00.0" , ""));
	$('#releasedFromDeptGm').val(tp[commercialRowId.rowIndex-1]['Released From Dept GM'].replace(":00.0" , ""));
	/*$('#releasedFromFh').val(tp[commercialRowId.rowIndex-1]['Released From FH'].replace(":00.0" , ""));*/
	$('#prCreationDate').val(tp[commercialRowId.rowIndex-1]['Pr Creation Date'].replace(":00.0" , ""));
	$('#poId').val(tp[commercialRowId.rowIndex-1]['PO ID']);
	$('#prId').val(tp[commercialRowId.rowIndex-1]['Pr ID']);
	$('#commAttach').val(/C:\\fakepath\\/i+commFile);

	
}



