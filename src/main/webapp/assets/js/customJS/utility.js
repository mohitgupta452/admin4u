


//automate location  and pickup Date & Time in response modal

function callAutoLocationAndPickup(requestID)
{
        var url="/admin4u/rest/data/showrequestdata/"+requestID;

            $.getJSON(url, function(result)
   {

   var t = result;

   if((t['Service'] === 'TRAINTICKET') || (t['Service'] === 'AIRTICKET'))  {
	 // alert(t['location']);
	     $('#location'+requestID).val(t['location']);
   }
   if((t['Service'] === 'CABPICK') || (t['Service'] === 'CABDROP') || (t['Service'] === 'CABDISPOSAL') || (t['Service'] === 'CABAIRPORT')){
       
	   var Date = t['pickupdatetime']; 
	   var onlyD =  Date.substring(5,16);
	   var onlyT = Date.substring(21,26);
	   var DT = onlyD+onlyT
	   //alert(DT);
	   $('#pickDateAndTime'+requestID).val(DT);
	   
   }
   if((t['Service'] === 'HOTEL_BOOKING') || (t['Service'] === 'GUEST_HOUSE_BOOKING')){
	  // alert(t['hotellocation']);
	   $('#location'+requestID).val(t['hotellocation']);
   }
     });
}


function callShowRequestData(requestID)
{
        var url="/admin4u/rest/data/showrequestdata/"+requestID;
            $('#requestDataModal .modal-content .modal-footer').css('margin-top','300px');
            $('#requestDataModal .modal-content .modal-body').html("");
            $.getJSON(url, function(result){

            	var t = result;

   if((t['Service'] === 'TRAINTICKET') || (t['Service'] === 'AIRTICKET'))  {
	   
$('#requestDataModal .modal-content .modal-body').append('<div class="col-sm-12 col-box"><section class="panel"><header class="panel-heading" style="padding:6px!important"><center>USER REQUEST DATA</center></header></section></div>');	   
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Journey</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+String(t['Journey'])+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Cost Centre</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['costcentre']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Preffered Time Slot</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['timeslot']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Travel Date</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['traveldate']+'</div></span></div>'); 


$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Source</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['source']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Destination</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['destination']+'</div></span></div>');
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Travel Purpose</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['travelpurpose']+'</div></span></div>');


if((t['Journey'] === 'two way')){
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Return Date</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['returndate']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Return Source</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['returnsource']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Return Destination</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['returndestination']+'</div></span></div>'); 
}
 
 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Mobile No</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ">'+t['mobileno']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Booking For</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['bookingfor']+'</div></span></div>'); 

if((t['bookingfor'])=== 'External'){
	$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Guest Type</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['GuestType']+'</div></span></div>');	
	$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Company Name</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Company']+'</div></span></div>');
	if((t['Service'])==='TRAINTICKET')	{
	$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Travel Class</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['TravelClass']+'</div></span></div>');
}
	if(typeof(t['totalPax'])!== 'undefined'){
	$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Pax Name</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['passenger']+'</div></span></div>');
	}
	for(var count=1 ; count <= t['totalPax'] ;count++){
    if(typeof(t['Passenger'+count+'_Name'])!== "undefined"){
		$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Pax Name'+count+'</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Passenger'+count+'_Name']+'</div></span></div>'); 	  
    }}
}
else{
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Employee ID</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['employeeId']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Pax Name</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['passenger']+'</div></span></div>'); 
}

$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Age</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['age']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Department</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['department']+'</div></span></div>'); 
if(typeof(t['landline'])!== "undefined"){
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Landline</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ">'+t['landline']+'</div></span></div>'); 
}
if((typeof(t['availcab'])!== "undefined" ) && (typeof(t['pickup'])!== "undefined") && (typeof(t['droppoint'])!== "undefined") && (typeof(t['disposal'])!== "undefined")){
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Avail Cab</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['availcab']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Pick Point</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['pickup']+'</div></span></div>'); 
if((t['disposal']=== 'no')){
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Drop Point</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['droppoint']+'</div></span></div>'); 
}
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Disposal</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['disposal']+'</div></span></div>'); 
}
if((typeof(t['availacc'])!== "undefined")  && (typeof(t['hotellocation'])!== "undefined") && ( typeof(t['checkindatetime'])!== "undefined") && (typeof(t['checkoutdatetime'])!== "undefined")){
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Avail Accomodation</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['availacc']+'</div></span></div>');
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['hotellocation']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Check In</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['checkindatetime']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Check Out</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['checkoutdatetime']+'</div></span></div>'); 
}

$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Requestor</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Requester']+'</div></span></div>'); 
//$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Booking For</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['Booking For']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Request Date</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Request Date']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Service</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Service']+'</div></span></div>'); 
$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Location']+'</div></span></div>'); 

   }
   if((t['Service'] === 'CABPICK') || (t['Service'] === 'CABDROP') || (t['Service'] === 'CABDISPOSAL') || (t['Service'] === 'CABAIRPORT'))
  {
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-sm-12 col-box"><section class="panel"><header class="panel-heading" style="padding:6px!important"><center>USER REQUEST DATA</center></header></section></div>');	   
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Booking For</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['bookingfor']+'</div></span></div>'); 
	   
	   if(t['bookingfor']=== 'External'){
	  $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Guest Type</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['GuestType']+'</div></span></div>');
	  $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Cab Segment</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['CabPref']+'</div></span></div>');
	   }else{
	  $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">EmployeeID</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['employeeid']+'</div></span></div>');
       }
  
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Pax Name</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['passenger']+'</div></span></div>'); 
    
   
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Mobile No</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ">'+t['mobileno']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Department</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ; text-transform: capitalize">'+t['department']+'</div></span></div>'); 
	   if(typeof(t['landline'])!== "undefined"){
		   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Landline</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ">'+t['landline']+'</div></span></div>'); 
		   }
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Travel Purpose</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word  text-transform: capitalize">'+t['travelpurpose']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Cost Centre</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word  text-transform: capitalize">'+t['costcentre']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Start Point</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ; text-transform: capitalize">'+t['startpoint']+'</div></span></div>'); 
	  
	   if((t['Service'] === 'CABDISPOSAL'))
	   { 
		   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Visit Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['visitlocation']+'</div></span></div>');
	   }
	   else
	   {
		   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">End Point</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Endpoint']+'</div></span></div>'); 
	   }
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Pickup Date&Time</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['pickupdatetime']+'</div></span></div>'); 
	  
	   if((t['Service'] === 'CABPICK') || (t['Service'] === 'CABDISPOSAL'))
	   {
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">End Date&Time</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['enddatetime']+'</div></span></div>'); 
	   }
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Requestor</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Requester']+'</div></span></div>'); 
	//   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Booking For</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['Booking For']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Request Date</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Request Date']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Service</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Service']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Location']+'</div></span></div>'); 
  }
   if((t['Service'] === 'HOTEL_BOOKING') || (t['Service'] === 'GUEST_HOUSE_BOOKING'))
   {
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-sm-12"><section class="panel"><header class="panel-heading" style="padding:6px!important"><center>USER REQUEST DATA</center></header></section></div>');	   
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Booking Type</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['bookingtype']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Booking For</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['bookingfor']+'</div></span></div>'); 
	   
	   if((t['bookingfor'] === 'External')){
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Guest Type</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['guesttype']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Cab Segment</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['CabPref']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Pax Name</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['guestname']+'</div></span></div>');	
	   for(var count=1 ; count < t['totalPax'] ;count++){
		   if(typeof(t['Passenger'+count+'_Name'])!== "undefined"){
				$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Pax Name'+count+'</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Passenger'+count+'_Name']+'</div></span></div>'); 	  
		   } }
		}
	   else{
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Employee ID</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['employeeid']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Pax Name</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['guestname']+'</div></span></div>'); 
	   }
	   if(typeof(t['landline'])!=="undefined"){
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Landline</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['landline']+'</div></span></div>'); 
	   }
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Department</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['department']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">CheckIn Date&Time</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['checkindatetime']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">CheckOut Date&Time</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['checkoutdatetime']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Hotel Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['hotellocation']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Cost Centre</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['costcenter']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Mobile No</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['mobileno']+'</div></span></div>'); 
	 
	   /*$('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4" ><label>Avail Cab</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['availcab']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4" ><label>Pickup Date&Time</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['pickupDatetime']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4" ><label>Pickup Point</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['pickuppoint']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4" ><label>Drop Point</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['droppoint']+'</div></span></div>'); 
*/	 
	   
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Requestor</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Requester']+'</div></span></div>'); 
	  // $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Booking For</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['Booking For']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Request Date</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Request Date']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Service</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Service']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Location']+'</div></span></div>'); 

   }
   if((t['Service'] === 'REPAIRMAINTENANCE') || (t['Service'] === 'HOUSEKEEPING')){
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-sm-12"><section class="panel"><header class="panel-heading" style="padding:6px!important"><center>USER REQUEST DATA</center></header></section></div>');
	   
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Username</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['username']+'</div></span></div>'); 
	  
	   if(typeof(t['employeeid'])!== "undefined"){
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">EmployeeID</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['employeeid']+'</div></span></div>'); 
	   }
	   
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Department</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['department']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Mobile No</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['mobile']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Nature Of Work</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['natureOfWork']+'</div></span></div>'); 
	  if(typeof(t['subnatureofwork']) !== "undefined"){
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Sub-Nature Of Work</label><span style="padding-left: 0.2cm ;"><div style="width:200px ; word-wrap: break-word ;textTransform : capitalize">'+t['subnatureofwork']+'</div></span></div>'); 
	  }
	   
	    
	   var ap =t['AmmendProblem'];
	   if(ap != undefined)
	   for(var count=0 ; count< ap.length ;count++)
	   {
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Ammended Problem '+count+'</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+ap[count]+'</div></span></div>');
	   }
	   
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Work Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['workLocation']+'</div></span></div>');
	   var awl = t['AmmendWorkLocation'];
	   if(awl != undefined)
	   for(var count=0 ; count< awl.length ;count++)
	   {
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Ammended Work Location '+count+'</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+awl[count]+'</div></span></div>');
	   }
	   
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['autolocation']+''+t['editlocation']+'</div></span></div>');
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Problem Description</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;;  text-transform: capitalize">'+t['description']+'</div></span></div>');
	  if(typeof(t['landline'])!=="undefined"){
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Landline Number</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['landline']+'</div></span></div>');
	  }
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Requestor</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Requester']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Booking For</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Booking For']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Request Date</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Request Date']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Service</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Service']+'</div></span></div>'); 
	   $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4 col-data" ><label class="col-label">Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word ;  text-transform: capitalize">'+t['Location']+'</div></span></div>'); 

	   //  $('#requestDataModal .modal-content .modal-body').append('<div class="col-md-4" ><label></label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['']+'</div></span></div>'); 

   
   }

});
        $('#requestDataModal').modal("show");
}





function callCancelRequest(requestID)
{
    var url="/admin4u/rest/update/cancelrequest/"+requestID;		
        $.getJSON(url, function(result){
            $.each(result, function(key, val){
           	  $('#rid'+requestID).children().eq(4).html(val);
           	  $('#rid'+requestID+' :button').attr("disabled",true);
            });
		    showAlertMessage("SUCCESS","Request Successfully Cancelled");
        });
}









function callAmendRequest()
{
	var value;
	var requestID;
	var jsonData ={}
	$(".admin-ammend").each(function() {
	         var name = $(this).attr("name");
	         value = $(this).val();
	         if(name == 'requestId')
	         requestID=value;
	        jsonData[name] = value;
	    })
	    
  $.ajax({
      url: "/admin4u/rest/update/amendrequest",
      type: 'POST',
      data: JSON.stringify(jsonData),
      dataType: "json",
      ContentType: "application/json",
      Accept: "application/x-www-form-urlencoded",
      success: function(data)
      {
    	  console.log(data);
    	  $.each(data, function(key, val){
           	  $('#rid'+requestID).children().eq(4).html(val);
            });
		    showAlertMessage("SUCCESS","Request Successfully ammended");
      }
  
      
	});
}

function callShowResponseData(requestID ,service,btnName)
{	
	var url="/admin4u/rest/data/showresponsedata/"+requestID;
	    $.getJSON(url, function(result){
	        console.log("result"+result);

       $('#responseDataModal .modal-content .modal-body').html("");
      var t = result;
      console.log(t);
 if((t['serviceList'] === 'TRAINTICKET') || (t['serviceList'] === 'AIRTICKET') || (t['serviceList'] === 'HOTEL_BOOKING') ||  (t['cabServiceList'] === 'CAB') || (t['hotelServiceList'] === 'HOTEL') ||(t['serviceList'] === 'GUEST_HOUSE_BOOKING')) {
   if((t['serviceList'] === 'TRAINTICKET') || (t['serviceList'] === 'AIRTICKET'))  
	   {
  $('#responseDataModal .modal-content .modal-body').append('<div class="row"><div class="col-sm-12"><section class="panel"><header class="panel-heading" style="padding:6px!important">Ticket Booking Details </header>');                                     
  $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Departure Date & Time</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['departureDateAndTime']+'" readonly="true"  name="departureDateAndTime" id="departureDateAndTime" style=" border-color: transparent;"/></span></div>');
  $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Booking Date</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['bookingDate']+'" readonly="true"  name="bookingDate" id="bookingDate" style="border-color: transparent;"/></span></div></div></section></div>');
  $('#responseDataModal .modal-content .modal-body').append('<input value='+service+' readonly="true" class="form-control" name="service" id="service" style="display:none;"/>');  
  
 }
   
   if( (t['cabServiceList'] === 'CAB'))
	   {
	   $('#responseDataModal .modal-content .modal-body').append('<div class="row"><div class="col-sm-12"><section class="panel"><header class="panel-heading" style="padding:6px!important">Cab Booking Details</header>'); 
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Cab Type</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['cabType']+'" readonly="true"  name="cabType" id="cabType" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Cab No</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['cabNo']+'" readonly="true"  name="cabNo" id="cabNo" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Cab Provider</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['cabProvider']+'" readonly="true"  name="cabProvider" id="cabProvider" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Driver Name</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['driverName']+'" readonly="true"  name="driverName" id="driverName" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Driver No</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['driverNo']+'" readonly="true"  name="driverNo" id="driverNo" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Pickup Date & Time</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['pickDateAndTime']+'" readonly="true"  name="pickDateAndTime" id="pickDateAndTime" style="border-color: transparent;"/></span></div></section></div></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<input value='+service+' readonly="true" class="form-control" name="service" id="service" style="display:none;"/>');  
	   }
   if((t['hotelServiceList'] === 'HOTEL'))
	   {
	 
	   
	     
	  /* $('#responseDataModal .modal-content .modal-body').append('<div class="row"><div class="col-sm-12"><section class="panel"><header class="panel-heading" style="padding:6px!important">Accomodation Details</header>'); 
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Location</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['location']+'" readonly="true"  name="location" id="location" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Booking Type</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['guestHouse']+'" readonly="true"  name="guestHouse" id="guestHouse" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Check In</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['checkinDateTime']+'" readonly="true"  name="checkIn" id="checkIn" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Check Out</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['checkoutDateTime']+'" readonly="true"  name="checkOut" id="checkOut" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Hotel Name and Address</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['hotelBookConfirm']+'" readonly="true"  name="contactDetail" id="contactDetail" style="border-color: transparent;"/></span></div></section></div></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Message</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['comment']+'" readonly="true"  name="comment" id="comment" style="border-color: transparent;"/></span></div></section></div></div>');

	   $('#responseDataModal .modal-content .modal-body').append('<input value='+service+' readonly="true" class="form-control" name="service" id="service" style="display:none;"/>');   
*/
	   
	   if((t['guestHouse'] === 'Guest_House'))
	   {

	   $('#responseDataModal .modal-content .modal-body').append('<div class="row"><div class="col-sm-12"><section class="panel"><header class="panel-heading" style="padding:6px!important">Accomodation Details</header>'); 
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Guest House Address</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['nameAddress ']+'" readonly="true"  name="name" id="location" style="border-color: transparent;"/></span></div>');
	  // $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Addreess</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['address']+'" readonly="true"  name="address" id="guestHouse" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Room Type</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['typeType']+'" readonly="true"  name="roomType" id="availSheets" style="border-color: transparent;"/></span></div></div></br>');
	  
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Room No.</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['roomNumber']+'" readonly="true"  name="roomNumber" id="checkIn" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Check In</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['checkoutDateTime']+'" readonly="true"  name="checkIn" id="checkIn" style="border-color: transparent;"/></span></div>');

	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Check Out</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['checkoutDateTime']+'" readonly="true"  name="checkOut" id="checkOut" style="border-color: transparent;"/></span></div>');
	   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Comment</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['comment']+'" readonly="true"  name="comment" id="comment" style="border-color: transparent;"/></span></div>');

	   $('#responseDataModal .modal-content .modal-body').append('<input value='+service+' readonly="true" class="form-control" name="service" id="service" style="display:none;"/>');   

	}else{
		
		$('#responseDataModal .modal-content .modal-body').append('<div class="row"><div class="col-sm-12"><section class="panel"><header class="panel-heading" style="padding:6px!important">Accomodation Details</header>'); 
		   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Hotel Name and  Address</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['hotelBookConfirm']+'" readonly="true"  name="address" id="address" style="border-color: transparent;"/></span></div>');
		   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Check In</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['checkinDateTime']+'" readonly="true"  name="checkIn" id="checkIn" style="border-color: transparent;"/></span></div>');

		   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Check Out</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['checkoutDateTime']+'" readonly="true"  name="checkOut" id="checkOut" style="border-color: transparent;"/></span></div>');
		   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Comment</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['comment']+'" readonly="true"  name="comment" id="comment" style="border-color: transparent;"/></span></div>');

		   
		  
		   $('#responseDataModal .modal-content .modal-body').append('<input value='+service+' readonly="true" class="form-control" name="service" id="service" style="display:none;"/>');   

		
		
	}
	   
	   
	   
}
   
   /*if((t['serviceList'] === 'GUEST_HOUSE_BOOKING')) //you can delete this block
   {

   $('#responseDataModal .modal-content .modal-body').append('<div class="row"><div class="col-sm-12"><section class="panel"><header class="panel-heading" style="padding:6px!important">Accomodation Details</header>'); 
   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Guest House Name</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['name']+'" readonly="true"  name="name" id="location" style="border-color: transparent;"/></span></div>');
   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Addreess</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['address']+'" readonly="true"  name="address" id="guestHouse" style="border-color: transparent;"/></span></div>');
   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Room Type</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['typeType']+'" readonly="true"  name="roomType" id="availSheets" style="border-color: transparent;"/></span></div></div>');
   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Room No.</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['roomNumber']+'" readonly="true"  name="roomNumber" id="checkIn" style="border-color: transparent;"/></span></div>');
   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Check In</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['checkIn']+'" readonly="true"  name="checkIn" id="checkIn" style="border-color: transparent;"/></span></div>');

   $('#responseDataModal .modal-content .modal-body').append('<div class="col-md-3" ><label >Check Out</label></div><div class="col-md-3"><span style="padding-left: 0.5cm"><input value="'+t['checkOut']+'" readonly="true"  name="checkOut" id="checkOut" style="border-color: transparent;"/></span></div>');
  
   $('#responseDataModal .modal-content .modal-body').append('<input value='+service+' readonly="true" class="form-control" name="service" id="service" style="display:none;"/>');   

}*/
   
       }
 
 $('#responseDataModal .modal-content .modal-body').append('<input value="'+t['serviceList']+'" readonly="true" class="form-control" name="serviceList" id="serviceList" style="display:none;"/>');  
 $('#responseDataModal .modal-content .modal-body').append('<input value="'+t['cabServiceList']+'" readonly="true" class="form-control" name="cabServiceList" id="cabServiceList" style="display:none;"/>');  
 $('#responseDataModal .modal-content .modal-body').append('<input value="'+t['hotelServiceList']+'" readonly="true" class="form-control" name="hotelServiceList" id="hotelServiceList" style="display:none;"/>');  

            });
	    
	   //if(btnName === "showResponseBtn")
        $('#responseDataModal').modal("show");
	   
   }

function downloadAttachment(requestID ,service,responseAttachMent)
{
	console.log("download js");
	//callShowResponseData(requestID ,service,"btnName");
	var target = document.getElementById("generatePdfFrm");
//		target.submit(function( event ) {
//		
//		  event.preventDefault();
//		});
	$("form[name='form_name']").submit()
	

	document.getElementById("submitResponse12345").addEventListener("click",function(){
		form.submit();
	});
	
	
	 
	
}


function callShowResponseMsg(requestID)

{
	 $('#responseMsgModal .modal-content .modal-body').html(""); 
	var url="/admin4u/rest/data/showresponsemsg/"+requestID;
	 $.getJSON(url, function(result){
		

		 $.each(result, function(key, val){	
		 $('#responseMsgModal .modal-content .modal-body').append(val);
	 })
	 })
	 $('#responseMsgModal').modal("show");
}

$(document).ready(function(){
	$(document).on("blur",'.empIdClass',function(){
		var url="UtilityServlet?action=getEmpName&employeeId="+$(this).val();
		var employeeId=$(this).attr('id');
		if($(this).val()){
		var passengerId=employeeId.replace("employeeID","passenger");

        $.getJSON(url, function(result){
    		console.log("workings"+result['employeeName']);
    		
           $('#'+passengerId).val(result['employeeName']);
        	
        });
		
		}
		
	});
	
	
	
});

function getEmployeeNameById(empIdInputBox,targetEmpNameInputId){
	var employeeId=empIdInputBox.value;
	var url="UtilityServlet?action=getEmpName&employeeId="+employeeId;
	if(employeeId){

    $.getJSON(url, function(result){
		console.log("workings"+result['employeeName']);
		
       $('#'+targetEmpNameInputId).val(result['employeeName']);
    	
    });
	
	}
	
}


function getEmployeeDataById(empIdInputBox){
	var employeeId=empIdInputBox.value;
	var url="UtilityServlet?action=EMPLOYEEDATA&employeeId="+employeeId;
	if(employeeId){

    $.getJSON(url, function(result){
		console.log("workings"+result['employeeName']);
		
       $('#username').val(result['employeeName']);
       $('#department').val(result['department']);

    	
    });
	
	}
	
}



$(document).ready(function(){
	$(document).on("blur",'.trainEmpIdClass',function(){
		var url="UtilityServlet?action=getEmpName&employeeId="+$(this).val();
		var employeeId=$(this).attr('id');
		if($(this).val()){
		console.log(employeeId);
		var passengerId=employeeId.replace("trainEmployeeID","trainPassenger");
		console.log(passengerId);
        $.getJSON(url, function(result){
    		console.log("workings"+result['employeeName']);
    		
           $('#'+passengerId).val(result['employeeName']);

        	
        });
		}
		
		
		
	});
	
	
	
});








$(document).ready(function(){
	$(document).on("change",'#spaceStationCat',function(){
		var url="SMSpaceAllocationServlet";
		var floorId=$('#buildingFloor').val();
		var seatType=$(this).val();
		var action="SEATINFO";
		var jsonData={
				floorId:floorId,
				seatType:seatType,
				action:action,
		}
		//$('#seatDetailTableTbody').html("");
		 $.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		            $('#seatDetailTableTbody').html('<tr><td>1</td><td>'+result['buildingFloor']+'</td><td>'+result['seatType']+'</td><td class="numeric">'+result['total']+'</td>'
                           +' <td class="numeric">'+result['assigned']+'</td>'
                            +'<td class="numeric">'+result['available']+'</td>'
                            +'<td class=""><button>Allocate Space</button></td></tr>' );
		            console.log(""+floorId+seatType+result['available']);
                    

		            
		        }});
		
		
		//ajax to get available seat or cabin
		action="GETAVAILABLESPACE";
		
		jsonData={
				floorId:floorId,
				seatType:seatType,
				action:action,
		}
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	  console.log("second methodd");
	            	$('#availSeat').html('<option value="">Select...</option>');

		            $.each(result,function(key,val){
				    	  console.log("key"+key);

		            	$('#availSeat').append('<option value="'+key+'">'+val+'</option>');
		            });
		        }});
		
		
		
		
			});
        	
        });
		
		
function callSPOCShowRequestData(requestID)
{
    var url="/admin4u/rest/data/showrequestdata/"+requestID;
        $.getJSON(url, function(result){
            $('.admin_modal .modal-content .modal-footer').css('margin-top','300px');
        	
            $('.admin_modal .modal-content .modal-body .requestDataSPOC').html("");
            
            $.each(result, function(key, val){
               $('.admin_modal .modal-content .modal-body .requestDataSPOC').prepend('<div class = "panel"><div class="col-md-4" ><label>'+key+'</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+val+'</div></span></div></div>');
            });
           
        });
}


$(document).ready(function(){
	$(document).on("change",'.ghAvailableReq',function(){
		
		$('singleOccupancyTable').html("");
		$('doubleOccupancyTable').html("");
		$('tripleOccupancyTable').html("");

		
		var url="GuestHouseInventoryServlet";
		//var buildingId=$('.ghAvailableReq').val();
		var action="AVAILABLEROOMLIST";
		var actionId=$('#modalActionId').val();
		var buildingId=$('#ghAvailableReq'+actionId).val();
		console.log("building id_______________________________________________"+buildingId);

		var checkInDateTime=$("#checkInDateTime"+actionId).val();
		var checkOutDateTime=$("#checkOutDateTime"+actionId).val();
		var jsonData={
				buildingId:buildingId,
				action:action,
				checkInDateTime:checkInDateTime,
				checkOutDateTime:checkOutDateTime,
		}
		
		
		//ajax to get available seat or cabin
		
		
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	  console.log(result['doubleOccupacy']);
		    	  var table="";
		    	  $('.singleOccupancyTable').html("");
		    	  $('.doubleleOccupancyTable').html("");
		    	  $('.tripleOccupancyTable').html("");
		           $.each(result['singleOccupacy'],function(key,val){
		        	   for(var i=1;i<=7;i++){
		        	   if(val['stuff'+i]===undefined)
		        		   val['stuff'+i]="N/A";
		        	   }
				    	  console.log("key"+key+"val"+val['roomNumber']);
				    	  table= '<tr><td>'+val['roomNumber']+'</td><td>'+val['stuff1']+'</td><td>'+val['stuff2']+'</td><td>'+val['stuff3']+'</td><td>'+val['stuff4']+'</td><td>'+val['stuff5']+'</td><td>'+val['stuff6']+'</td><td><button type="button"  class="confirmButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Select</button></td>'+'</tr>';
				    	  $('.singleOccupancyTable').append(table);
 	  
				    	  	console.log(table);

		            	//$('#availSeat').append('<option value="'+key+'">'+val+'</option>');
		            });
		           	table="";
		           	
		           $.each(result['doubleOccupacy'],function(key,val){
		        	   console.log("inside double occupacy");
		        	   for(var i=1;i<=8;i++){
			        	   if(val['stuff'+i]===undefined)
			        		   val['stuff'+i]="N/A";
			        	   }
				    	  table= '<tr><td>'+val['roomNumber']+'</td><td>'+val['stuff1']+'</td><td>'+val['stuff2']+'</td><td>'+val['stuff3']+'</td><td>'+val['stuff4']+'</td><td>'+val['stuff5']+'</td><td>'+val['stuff6']+'</td><td><button type="button"  class="confirmButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Select</button></td>'+'</tr>';
				    	
		        	   $('.doubleOccupancyTable').append(table);
	  				    	  

		            });
		           table="";
		           $.each(result['tripleOccupacy'],function(key,val){
				    	  console.log("key"+key);
				    	  for(var i=1;i<=8;i++){
				        	   if(val['stuff'+i]===undefined)
				        		   val['stuff'+i]="N/A";
				        	   }
				    	  console.log("key"+key+"val"+val['roomNumber']);
				    	  table= '<tr><td>'+val['roomNumber']+'</td><td>'+val['stuff1']+'</td><td>'+val['stuff2']+'</td><td>'+val['stuff3']+'</td><td>'+val['stuff4']+'</td><td>'+val['stuff5']+'</td><td>'+val['stuff6']+'</td><td><button type="button"  class="confirmButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Select</button></td>'+'</tr>';

				    	  $('.tripleOccupancyTable').append(table);
 	  

		            });
		           
		        	   
		           
		           $.each(result['unavailableRoomList'],function(key,val){
			           var historyTableRow="";

		        	   var historyList="";
				    	  console.log("key"+key+"val"+val);
				    	  console.log("inside unavailbaleRoomList"+val['roomNumber']);
				    	  
				    	  
				           $.each(val['history'],function(key,history){
				        	 console.log("inside history");
				        	   historyList+='<li>checkInTime:'+history['checkInTime']+'checkOutTime:'+	history['checkOutTime']+'</li>';
				        	  /* history['checInTime'],
				        	   history['checOutTime']*/

				           });
					    historyTableRow='<tr><td>'+val['roomNumber']+'</td><td>'+historyList+'</td></tr>';
					    console.log("history row"+historyTableRow);
				           
				           if(val['type']==='SINGLE'){
				        	   $('.singleOccupancyTableHistory').append(historyTableRow);
				        	   
				           }
				           if(val['type']==='DOUBLE'){
				        	   $('.doubleOccupancyTableHistory').append(historyTableRow);
				        	   
				        	   
				           }
				           if(val['type']==='TRIPLE'){
				        	   $('.tripleOccupancyTableHistory').append(historyTableRow);

				        	   
				           }

				    	  /*for(var i=1;i<=8;i++){
				        	   if(val['stuff'+i]===undefined)
				        		   val['stuff'+i]="N/A";
				        	   }
				    	  console.log("key"+key+"val"+val['roomNumber']);
				    	  table= '<tr><td>'+val['roomNumber']+'</td><td>'+val['stuff1']+'</td><td>'+val['stuff2']+'</td><td>'+val['stuff3']+'</td><td>'+val['stuff4']+'</td><td>'+val['stuff5']+'</td><td>'+val['stuff6']+'</td><td>'+val['stuff7']+'</td><td>'+val['stuff8']+'</td><td><button type="button"  class="confirmButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Confirm / Book</button></td>'+'</tr>';
				    	  $('.tripleOccupancyTable').append(table);*/
	  

		            }); $.each(result, function(key, val){
		             	  $('#rid'+requestID).children().eq(4).html(val);
		               	  $('#rid'+requestID+' :button').attr("disabled",true);
		                });
		    		    showAlertMessage("SUCCESS","Request Successfully Cancelled");
		           
		        }});
		
		
		
		
			});
        	
        });

/*
 * 
 *start event on changeing chekin checkout time
*/


$(document).ready(function(){
	$(document).on("change",'.checkinCheckoutDateTime',function(){
		
		$('.singleOccupancyTable').html("");
		$('.doubleOccupancyTable').html("");
		$('.tripleOccupancyTable').html("");
		
		$('.singleOccupancyTableHistory').html("");
		$('.doubleOccupancyTableHistory').html("");
		$('.tripleOccupancyTableHistory').html("");

		var url="GuestHouseInventoryServlet";
		var action="AVAILABLEROOMLIST";
		var actionId=$('#modalActionId').val();
		var buildingId=$('#ghAvailableReq'+actionId).val();

		var checkInDateTime=$("#checkInDateTime"+actionId).val();
		var checkOutDateTime=$("#checkOutDateTime"+actionId).val();
		var jsonData={
				buildingId:buildingId,
				action:action,
				checkInDateTime:checkInDateTime,
				checkOutDateTime:checkOutDateTime,
		}
		
		
		//ajax to get available seat or cabin
		
		
		$.ajax({
		      url: url,
		      type: 'POST',
		      data: jsonData,
		      dataType: "json",
		      ContentType: "application/json",
		      success: function(result){
		    	  console.log(result['doubleOccupacy']);
		    	  var table="";
		    	  $('.singleOccupancyTable').html("");
		    	  $('.doubleleOccupancyTable').html("");
		    	  $('.tripleOccupancyTable').html("");
		    	  
		    	  $('.singleOccupancyTableHistory').html("");
		  		$('.doubleOccupancyTableHistory').html("");
		  		$('.tripleOccupancyTableHistory').html("");
		  		
		           $.each(result['singleOccupacy'],function(key,val){
		        	   for(var i=1;i<=8;i++){
		        	   if(val['stuff'+i]===undefined)
		        		   val['stuff'+i]="N/A";
		        	   }
				    	  console.log("key"+key+"val"+val['roomNumber']);
				    	  table= '<tr><td>'+val['roomNumber']+'</td><td>'+val['stuff1']+'</td><td>'+val['stuff2']+'</td><td>'+val['stuff3']+'</td><td>'+val['stuff4']+'</td><td>'+val['stuff5']+'</td><td>'+val['stuff6']+'</td><td><button type="button"  class="confirmButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Select</button></td>'+'</tr>';

				    	  $('.singleOccupancyTable').append(table);
 	  
				    	  	console.log(table);

		            	//$('#availSeat').append('<option value="'+key+'">'+val+'</option>');
		            });
		           	table="";
		           	
		           $.each(result['doubleOccupacy'],function(key,val){
		        	   console.log("inside double occupacy");
		        	   for(var i=1;i<=8;i++){
			        	   if(val['stuff'+i]===undefined)
			        		   val['stuff'+i]="N/A";
			        	   }
				    	  table= '<tr><td>'+val['roomNumber']+'</td><td>'+val['stuff1']+'</td><td>'+val['stuff2']+'</td><td>'+val['stuff3']+'</td><td>'+val['stuff4']+'</td><td>'+val['stuff5']+'</td><td>'+val['stuff6']+'</td><td><button type="button"  class="confirmButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Select</button></td>'+'</tr>';

		        	   $('.doubleOccupancyTable').append(table);
	  				    	  

		            });
		           table="";
		           $.each(result['tripleOccupacy'],function(key,val){
				    	  console.log("key"+key);
				    	  for(var i=1;i<=8;i++){
				        	   if(val['stuff'+i]===undefined)
				        		   val['stuff'+i]="N/A";
				        	   }
				    	  console.log("key"+key+"val"+val['roomNumber']);
				    	  table= '<tr><td>'+val['roomNumber']+'</td><td>'+val['stuff1']+'</td><td>'+val['stuff2']+'</td><td>'+val['stuff3']+'</td><td>'+val['stuff4']+'</td><td>'+val['stuff5']+'</td><td>'+val['stuff6']+'</td><td><button type="button"  class="confirmButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Select</button></td>'+'</tr>';

				    	  $('.tripleOccupancyTable').append(table);
 	  

		            });
		           
		        	   
		           
		           $.each(result['unavailableRoomList'],function(key,val){
			           var historyTableRow="";

		        	   var historyList="";
				    	  console.log("key"+key+"val"+val);
				    	  console.log("inside unavailbaleRoomList"+val['roomNumber']);
				    	  
				    	  
				           $.each(val['history'],function(key,history){
				        	 
				        	   historyList+='<li>checkInTime:'+history['checkInTime']+'checkOutTime:'+	history['checkOutTime']+'</li>';
				        	  /* history['checInTime'],
				        	   history['checOutTime']*/

				           });
					    historyTableRow='<tr><td>'+val['roomNumber']+'</td><td>'+historyList+'</td></tr>';
					    console.log(historyTableRow);
				           
				           if(val['type']==='SINGLE'){
				        	   console.log("inside unavailable single");
				        	   $('.singleOccupancyTableHistory').append(historyTableRow);
				        	   
				           }
				           if(val['type']==='DOUBLE'){
				        	   $('.doubleOccupancyTableHistory').append(historyTableRow);
				        	   
				        	   
				           }
				           if(val['type']==='TRIPLE'){
				        	   $('.tripleOccupancyTableHistory').append(historyTableRow);

				        	   
				           }

				    	  /*for(var i=1;i<=8;i++){
				        	   if(val['stuff'+i]===undefined)
				        		   val['stuff'+i]="N/A";
				        	   }
				    	  console.log("key"+key+"val"+val['roomNumber']);
				    	  table= '<tr><td>'+val['roomNumber']+'</td><td>'+val['stuff1']+'</td><td>'+val['stuff2']+'</td><td>'+val['stuff3']+'</td><td>'+val['stuff4']+'</td><td>'+val['stuff5']+'</td><td>'+val['stuff6']+'</td><td>'+val['stuff7']+'</td><td>'+val['stuff8']+'</td><td><button type="button"  class="confirmButton btn btn-warning " name="roomId" value="'+val['roomId']+'">Confirm / Book</button></td>'+'</tr>';
				    	  $('.tripleOccupancyTable').append(table);*/
	  

		            });
		           
		        }});
		
		
		
		
			});
        	
        });




/*
ednd event on change date time
*/
// request Data for repairmaintenance and housekeeping (amend)

function ammendRequest(requestID)
{
	
        var url="/admin4u/rest/data/showrequestdata/"+requestID;
           // $('#amend_request_modal .modal-content .modal-footer').css('margin-top','300px');
            $('#amend_request_modal .modal-content .modal-body').html(" ");
            
            $('#requestId').val($(this).data('id'));
            $.getJSON(url, function(result){
            	//alert(result);
var t = result;

if((t['Service'] === 'REPAIRMAINTENANCE') || (t['Service'] === 'HOUSEKEEPING')){
	
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-sm-12"><section class="panel"><header class="panel-heading" style="padding:6px!important"><center>USER REQUEST DATA</center></header></section></div>');
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Username</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['username']+'</div></span></div>'); 
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Department</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['department']+'</div></span></div>'); 
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Mobile No</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['mobile']+'</div></span></div>'); 
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Nature Of Work</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['nature']+'</div></span></div>'); 
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Problem Description</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['description']+'</div></span></div>'); 
	   var ap =t['AmmendProblem'];
	   if(ap != undefined)
	   for(var count=0 ; count< ap.length ;count++)
	   {
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Ammended Problem '+count+'</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+ap[count]+'</div></span></div>');
	   }
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Work Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['workLocation']+'</div></span></div>'); 
	   var awl = t['AmmendWorkLocation'];
	   if(awl != undefined)
	   for(var count=0 ; count< awl.length ;count++)
	   {
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label class="col-label">Ammended Work Location '+count+'</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+awl[count]+'</div></span></div>');
	   }

	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Requestor</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['Requester']+'</div></span></div>'); 
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Booking For</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['Booking For']+'</div></span></div>'); 
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Request Date</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['Request Date']+'</div></span></div>'); 
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Service</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['Service']+'</div></span></div>'); 
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Location</label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word">'+t['Location']+'</div></span></div>'); 
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label></label><span style="padding-left: 0.2cm ;"><div style="width:200px; word-wrap: break-word"></div></span></div>');
	   $('#amend_request_modal .modal-content .modal-body').append('<div class ="row"><div class="col-sm-12"><div class="col-md-4" ><label>Problem Description</label><span style="padding-left: 0.2cm ;"><input class="form-control admin-ammend" size="12"'
				+'type="text" name="amend_description" id="amend_description" placeholder="Problem Description"/></span></div>');
	   $('#amend_request_modal .modal-content .modal-body').append('<div class="col-md-4" ><label>Work Location</label><span style="padding-left: 0.2cm ;"> <input class="form-control admin-ammend" size="12"'
				+'type="text" name="amend_work" id="amend_work" placeholder="Work Location"/></span></div></div></div>');

}

});
     
}

/*function callAmendRequest()
{
	jsonData={};

	var requestID = $('#requestId').val();
		
			$(".admin-ammend").each(function() {
			       var name = $(this).attr("name");
			       var value = $(this).val();
			       jsonData[name] = value;
			   })
			   
			  // alert(requestID);
			   console.log(jsonData);
			   $.ajax({
          url:"/admin4u/rest/update/amendrequest",
          type: 'POST',
          data:JSON.stringify(jsonData),
          dataType: "json",
          success: function (data,textStatus, $XHR) {
        	  $.each(data, function(key, val){
               	  $('#rid'+requestID).children().eq(4).html(val);
                });
    		    showAlertMessage("SUCCESS","Request Successfully Ammended");
          
	console.log(data);
          }
          });
	}
*/
/*function callShowAmendMsg(requestID)
{
    $('#amendResponseMsgModal .modal-content .modal-body').html("");  
	var url="/admin4u/rest/data/showamendmsg/"+requestID;
	$.getJSON(url, function(result){
		 $.each(result, function(key, val){		
		 $('#amendResponseMsgModal .modal-content .modal-body').append('<div style=" word-wrap: break-word">'+val+'</div>');
		 })
	 });
	 $('#amendResponseMsgModal').modal("show");
	
}*/

function reopenRequest()
{
	jsonData={};
	
	var requestID = $('#reopenrequestId').val();
			$(".admin-reopen").each(function() {
			       var name = $(this).attr("name");
			      var value = $(this).val();
			       jsonData[name] = value;
			   })
			  // alert();
			   
		 console.log(jsonData);
	      $.ajax({
          url:"/admin4u/rest/update/reopenrequest",
          type: 'POST',
          data:JSON.stringify(jsonData),
          dataType: "json",
          success: function (data,textStatus, $XHR) {
        	  $.each(data, function(key, val){
               	  $('#rid'+requestID).children().eq(4).html('<a onclick="callShowReopenMsg('+requestID+')">'+val+'</a>');
               	  //$('#rid'+requestID+' :button').attr("disabled",true);
                });
    		    showAlertMessage("SUCCESS","Request Successfully Reopened");
	console.log(data);
          }
          });
	}

function callShowReopenMsg(requestID)
{
    $('#reopenResponseMsgModal .modal-content .modal-body').html("");  
	var url="/admin4u/rest/data/showreopenmsg/"+requestID;
	$.getJSON(url, function(result){
		 $.each(result, function(key, val){		
		 $('#reopenResponseMsgModal .modal-content .modal-body').append('<div style=" word-wrap: break-word">'+val+'</div>');
		 })
	 });
	 $('#reopenResponseMsgModal').modal("show");
	
}

$(window).load(function(){
 var serviceWork ,htmls;
 htmls='<option value="">select Work Nature</option>';
 serviceWork = ["Electrical","Plumbing","Civil","Carpentery","HVAC","Others"];
 for (var i = 0; i < serviceWork.length; i++) {
 	htmls+='<option value="'+serviceWork[i]+'"'+'>'+serviceWork[i]+'</option>';
 };
 $("#workservice").html(htmls);
});


