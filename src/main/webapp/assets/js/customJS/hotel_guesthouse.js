$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
    		if($('#bookingFor').val()=== 'External'){
    	 if($('#hotelExternalGuestType').val() ==="" || $('#hotelExternalCabPref').val()=== ""){
    	  $('#bookingFor').val("").trigger('change');
    	  }}
     }
});


function hotelExtenalCloseBtn()
{
	$('#bookingFor').val("").trigger('change');
}


// For internal booking
$('.bookingFor').on('change' , function(){
	if(this.value === "Internal Emp.")
	{
		$('#internal_booking_modal').modal('show');
		
	}
});


//  remove asterik 
$('#bookingFor').change(function(){
	if($('#bookingFor option:selected').text() === 'External')
	{
	    $('#removeSpan').hide();
	}
	else 
	{
	 $('#removeSpan').show();
	}
	
})


//hotel Booking and guestHouse Booking Location

$('#bookingType').on('change' , function(){
	if($('#bookingType option:selected').text() === 'Guest House Booking'){
		//alert(':)');
		$('#guestHouseDiv').show();
		 $('#hotelDiv').hide();
		 $('#hotelLocation').attr('disabled' , true);
		 $('#hotelLocation1').attr('disabled',true);
		 $('#guestHouseLocation1').attr('disabled' , true);
		 $('#guestHouseLocation').attr('disabled' , false);
		 
	}
		else{
		 $('#guestHouseLocation').attr('disabled' , true);
		 $('#hotelLocation').attr('disabled' , false);
	     $('#hotelDiv').show();
	     $('#guestHouseDiv').hide();
	     $('#hotelLocation1').attr('disabled',true);
		 $('#guestHouseLocation1').attr('disabled' , true);
		}
});


// when we select others in  dropdown (HOUSE_BOOKING Location)
$('#hotelLocation').change(function(){
	if($('#hotelLocation option:selected').text() === 'Others')
		{
		$('#locationModal .modal-footer').html('');
		 $('#locationModal .modal-footer').append('<button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterHotelLocation()">Yes</button>'
		         +'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectHotelLocation()">No</button> '); 
	
		    $('#locationModal').modal('show');
		}
})

function enterHotelLocation()
{
	$('#selectLocation').hide();
	$('#inputLocation').show();
	 $('#hotelLocation1').attr('disabled',false);
	 $('#hotelLocation').attr('disabled',true);
	//$('#selectLocationModal').modal('show');
}

$('#hotelLocation1').on('blur',function(){
	if($('#hotelLocation1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectHotelLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterHotelLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#hotelLocation option:selected').val($('#hotelLocation1').val());
})

function selectHotelLocation()
{
	$("#hotelLocation").val("").trigger('change');
	$('#selectLocation').show();
	$('#inputLocation').hide();
    $('#hotelLocation1').attr('disabled',true);
	$('#hotelLocation').attr('disabled',false);
}



//when we select others in  dropdown (GUEST_HOUSE_BOOKING Location)

$('#guestHouseLocation').change(function(){
	if($('#guestHouseLocation option:selected').text() === 'Others')
		{
		$('#locationModal .modal-footer').html('');
		 $('#locationModal .modal-footer').append('<button type="button" class="btn btn-danger"  data-dismiss="modal" onclick="enterGuestLocation()">Yes</button>'
		         +'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="selectGuestLocation()">No</button> '); 
	
		    $('#locationModal').modal('show');
		}
})

function enterGuestLocation()
{
	$('#selectLocationGuest').hide();
	$('#inputLocationGuest').show();
	 $('#guestHouseLocation1').attr('disabled',false);
	 $('#guestHouseLocation').attr('disabled',true);
	//$('#selectLocationModal').modal('show');
}

$('#guestHouseLocation1').on('blur',function(){
	if($('#guestHouseLocation1').val() === "")
	{
		$('#selectLocationModal .modal-footer').html("");
		$('#selectLocationModal .modal-footer').append('<button type="button" class="btn btn-danger" '
	    +' data-dismiss="modal" onclick="selectGuestLocation()">Yes</button>'
		+'<button type="button" class="btn btn-default" data-dismiss="modal" onclick="enterGuestLocation()">No</button> ');
		$('#selectLocationModal').modal('show');   
	}
	else
	$('#guestHouseLocation option:selected').val($('#guestHouseLocation1').val());
})

function selectGuestLocation()
{
	$("#guestHouseLocation").val("").trigger('change');
	$('#selectLocationGuest').show();
	$('#inputLocationGuest').hide();
    $('#guestHouseLocation1').attr('disabled',true);
	$('#guestHouseLocation').attr('disabled',false);
}


// only number will be enter in the field
$('.numbersOnly').keyup(function () {
    if (this.value != this.value.replace(/[^0-9]/g, '')) {
       this.value = this.value.replace(/[^0-9]/g, '');
    }
});

//only alphabets are allowed in the field /[^a-z]/g
$('.alphabetOnly').keyup(function () {
    if (this.value != this.value.replace( /[^a-zA-Z ]/g, '')) {
       this.value = this.value.replace( /[^a-zA-Z ]/g, '');
    }
});

//start point and end point should not be same
jQuery.validator.addMethod("matchValue", function (value, element,param) {
	
	   if(value === $("[name="+param+"]").val())
	   { }
	   else
	   {
		  return value ; 
	   }
	  
});




//hotel upload file name

$("#approvedAttachment").on('change',function(){
	$('#hotelFileName').html("");
	var hFile = $('input[name=approvedAttachment]').val().replace(/C:\\fakepath\\/i, '');
	$('#hotelFileName').append(hFile);
});
//end Ajax function for showing request Data on modal

	function callHNGServlet(action,actionId){   

		
		jsonData={
				 action:action,
				 actionId:actionId
				
		 }
       $.ajax({
           url: "HotelAndGuestHouseServlet",
           type: 'POST',
           data: jsonData,
           dataType: "json",
           complete: function (data) {
        	 
      	var v=data.responseText;
      	var parsedJson=$.parseJSON(v);
      	   
    	  $('#rid'+parsedJson['requestID']).children().eq(3).html(parsedJson['status']);
    	  $('#rid'+parsedJson['requestID']+' :button').attr("disabled", true);
        	  
              
            	},
           error: function (data, textStatus, $XHR) {
        	   

            	},
             });  
	    };

	function callAdminHNGServlet(action){   
		
		jsonData={
				 action:action
		 }
		 $(".admin-input").each(function() {
		        var name = $(this).attr("name");
		        var value = $(this).val();
		        jsonData[name] = value;
		    })
		 
       $.ajax({
           url: "HotelAndGuestHouseServlet",
           type: 'POST',
           data: jsonData,
           dataType: "json",
           complete: function (data) {
        	 
      	var v=data.responseText;
      	var parsedJson=$.parseJSON(v);
      	   
    	  $('#rid'+parsedJson['requestID']).children().eq(3).html(parsedJson['status']);
    	  $('#rid'+parsedJson['requestID']+' :button').attr("disabled", true);
        	  
              
            	},
           error: function (data, textStatus, $XHR) {
              
        	   

            	},
             });  
	    };
	  
	  
	    $(document).ready(function(){
	    	 $('#pickUpShow').hide();
	   	     $('#dropShow').hide();
	   	     $('#pickupdateShow').hide();

	         $('#availCab').on('change', function() {
	     
	    if(this.value==="Pickuponly")
	    {
	    	   $('#pickUpShow').show();
	    	   $('#dropShow').show();
	    	   $('#pickupdateShow').show();
		   	   $('#guestNameLabel').html('Pax Name <span class="labelcolor">*</span>');

	    }
	    else  if(this.value==="Droponly")
	    {
	    	   $('#pickUpShow').show();
	    	   $('#dropShow').show();
	    	   $('#pickupdateShow').show();
		   	   $('#guestNameLabel').html('Pax Name <span class="labelcolor">*</span>');

	    }
	    else  if(this.value==="Cab at disposal")
	    {
	    	   $('#pickUpShow').show();
	    	   $('#dropShow').hide();
	    	   $('#pickupdateShow').show();
		   	     $('#guestNameLabel').html('Pax Name <span class="labelcolor">*</span>');

	    }
	    else 
	    {
	    	 $('#pickUpShow').hide();
	   	     $('#dropShow').hide();
	   	     $('#pickupdateShow').hide();
	   	     $('#guestNameLabel').html('Guest Name <span class="labelcolor">*</span>');
	    }

	    });
	    });
	    
	     $(document).ready(function(){
	        $('#bookingFor').on('change', function() {
	    if(this.value==="Self Booking")
	    {
	    	$('#guestName').val($('#userName').val());
	    	$('#employeeID1').val($('#userID').val());
	    	
	         $('#guestName').attr('readonly', true);
	         $('#guestName').addClass('input-disabled');
	         $('#employeeID1').attr('readonly', true);
	         $('#employeeID1').addClass('input-disabled');
	         $('#addP').hide();
	    }

	    else
	    {
	    	$('#guestName').val("");
	    	$('#employeeID1').val("");
	    	
	         $('#guestName').attr('readonly', false);
	         $('#guestName').removeClass();
	         $('#employeeID1').attr('readonly',false);
	         $('#employeeID1').addClass('input-disabled');
	         $('#addP').hide();

	    }
	    if(this.value==="External")
	    {
	    	$('#guestName').val("");
	    	$('#employeeID1').val("");
	    	$('#addP').show();
	   
	    $('#hotel_modal').modal('show');
	    }
	    if(this.value==="Internal Emp.")
	    {
	    	$('#addP').hide();	//multi pax request
	    }

	    });
	    });
	     
	     
  // filesize of upload pdf should be 10MB

    $.validator.addMethod('filesize', function(value, element, param) {
	      // param = size (in bytes) 
	      // element = element to validate (<input>)
	      // value = value of the element (file name)
	      return this.optional(element) || (element.files[0].size <= param) 
 });	     
	 
    // pickup point and drop point must have atleast one alphabet bt not only numbers
$.validator.addMethod('acceptPattern' , function(value , element ,param){
	return value.match(new RegExp(param));
});	  

//restricted mobileno continuous pattern
jQuery.validator.addMethod("matchMob", function (value, element,param) {
if(value.match(new RegExp( param ))){
 return value.match(new RegExp( param ));
};  
});

//start mob no with 7-9
jQuery.validator.addMethod("startFrom", function (value, element,param) {
if(value.match(new RegExp( param ))){
 return value.match(new RegExp( param ));
};  
});

	     
	     // jquery validation for hotel guest house
$(function() {
        // validate HotelBookingForm  form on keyup and submit
        $("#hotelGuestForm").validate({
            rules: {
                employeeID:{
                    required: function() {

                        return $('#bookingFor').val() == 'Self Booking' || $('#bookingFor').val() == 'Internal Emp.' ||  $('#bookingFor').val() == '';
                      }
                    },
		service:"required",
		bookingFor:"required",
		guestType:"required",
		guestName:{required: true,
		},
		mobileNo:{required:true ,
			startFrom : /^[7-9]\d+$/,
			minlength:10,
	        matchMob : "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}",
		},
		 hotelLandline: {
        	 minlength:10,
        	 matchMob : function(){
        	if($('#hotelLandline').val()!==""){ 
        		return "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}";
        		}
         }
       },
		checkinDateTime:"required",
		checkoutDateTime:"required",
		hotelLocationOld:"required",
		pickupDateTime:"required",
		
		pickupPoint:{ required: true, 
			matchValue:"dropPoint",
			acceptPattern : '^[0-9]*[a-zA-Z]+[a-zA-Z0-9]*$'
			
			
			},
		dropPoint:{required:true, 
			matchValue:"pickupPoint",
			acceptPattern : '^[0-9]*[a-zA-Z]+[a-zA-Z0-9]*$'
			
			},
		costCentre:"required",
		approvedAttachment:{
            //required: true,
            extension: "pdf",
            filesize : 10000000
        }
		
		},
               
                
            messages: {
                employeeID: {required : "Please enter Employee ID"},
		service:"Please select Booking Type",
		bookingFor:"Please select Booking For",
		//guestType:"Please select Guest Type",
		guestName:{required : "Please enter Pax Name",
			pickupDateTime:"Please select pickup date time",
		},
		mobileNo:{required : "Please enter mobile Number",
			 startFrom : "Please start mobile number either from 7,8 or 9",
			 minlength:"Please enter exact 10 digits",
			 matchMob:"Please do not enter continuous number"
			
		},
		hotelLandline: {
        	 minlength:"Please enter 10 diigits",
        	 matchMob : "Please do not enter continuous number",
         },
		checkinDateTime:"Please enter checkin Date time",
		checkoutDateTime:"Please enter checkout Date time",
		hotelLocationOld:"Please select hotel location",
	    pickupPoint:{required : "Please enter pickup point",
	    	matchValue : "Please enter different value than drop point",
	    	acceptPattern:"Please enter atleast one alphabet"
	    },
		dropPoint:{required : "Please enter Drop Point",
			matchValue : " Please enter different value than pickup point",
			acceptPattern:"Please enter atleast one alphabet"
		},
		costCentre:"Please select cost Centre",
		approvedAttachment:{
		 	extension:"Please select pdf file only",
		 	filesize : "File size must be less than 10MB"
		 
		 }
                
            },
		
        });

});


	
	
	// add multiple pax 
	var count = 2;
	function addPax(eClassName,service)

	{
		
		 if(service=="hotel"){
			var prefixPax="passenger";
			//var prefixEmp="employeeID";
			var appendingDivId="paxName";
		}
		
	count += 1;
		$('.'+appendingDivId).append(' <div class="row">' /*<div class="col-sm-8 col-md-6">'
				    +'<div class="form-group"> <div class="col-sm-5"><label>Employee ID'+count+'<span class="requiredField"> *</span><br>&nbsp;</label>'
	                +'</div><div class="col-sm-7"><input type ="text" class="form-control '+eClassName+' " id="'+prefixEmp+count+'" name="employeeID'+count+'" placeholder="Employee ID">'
	                +'</div></div></div>'*/
	                +' <div class="col-sm-8 col-md-10"><div class="form-group"><div class="col-sm-5"><label>Pax Name'+count+'</label>'
	                +'</div> <div class="col-sm-7"><input type ="text" class="form-control alphabetOnly" id="'+prefixPax+count+'"'
	                +'name="passenger'+count+'" placeholder="Pax Name"><span id="error_pax'+count+'"></span></div></div></div><br/><br/>'
	               
	                
		);	$('.totalPassengers').val(count);
	}

	//

	$(document).ready(function(){
		
		$('#addP').hide();
	});
	

 
	
	
// validation in modals
	
	// validation in external modal
	//$('#hotelExtenalBtn').on('click' , function(){
	function hotelExtenalBtn(){
		if($('#hotelExternalGuestType').val() === ""){
			 $('#hotel_guesttype_error_msg').html("please select Guest Type");
			   
			 }
		else {
			$('#hotel_guesttype_error_msg').html("");
			}

		if($('#hotelExternalCabPref').val() === ""){
			 $('#hotel_cabpref_error_msg').html("please select Cab Segment");
		 }
		else {
			$('#hotel_cabpref_error_msg').html("");
		}
		if($('#hotelExternalGuestType').val() !== "" && $('#hotelExternalCabPref').val() !== ""){
			$('#hotel_modal').modal('hide');
		}
	}

	/*//validation in Add multiple Pax
	$('#isMultiRequest').on('click' , function(){
		
		var totalPax = $('#TotalPax').val();
		for(var count=2 ; count<= totalPax ;count++)
		{
		  if( $('#passenger'+count).val() === ""){
			  $('#error_pax'+count).html("please enter Pax Name"+count);
			  
		  }	
		  else {
			  $('#error_pax'+count).html("");
		  }
		}
		//$('#addPax').modal('hide');
		
		var globalObj;
		for(var i=2 ; i<= totalPax ; i++){
			if(('#passenger'+i).val() !== ""){
				alert("eeee");
				globalObj = true;
	         }
		}
		
		if(globalObj === true){
			alert("ghfgh");
			
		//}
		
		
		
	});
*/