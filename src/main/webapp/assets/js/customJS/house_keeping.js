
// work loaction
$('#work').on('change' , function(){
	
	if($('#work option:selected').text() !== 'select...'){
		
	var loc = $('#locationName').val();
	var loc = loc.substring(0, 3);
    $('#loc_house').val(loc);	
	$('#loc_house').show();
	$('#loc1_house').show();
	}
	else
		{
		$('#loc_house').hide();
		$('#loc1_house').hide();
		
		}
});


// only number will be enter in the field
$('.numbersOnly').keyup(function () {
    if (this.value != this.value.replace(/[^0-9]/g, '')) {
       this.value = this.value.replace(/[^0-9]/g, '');
    }
});

//only alphabets are allowed in the field /[^a-z]/g
$('.alphabetOnly').keyup(function () {
    if (this.value != this.value.replace( /[^a-zA-Z ]/g, '')) {
       this.value = this.value.replace( /[^a-zA-Z ]/g, '');
    }
});

// auto username and userid
$(document).ready(function(){
	
	$('#username').val($('#userName').val());
	$('#employeeID').val($('#userID').val());
});

//restricted mobileno continuous pattern
jQuery.validator.addMethod("matchMob", function (value, element,param) {
if(value.match(new RegExp( param ))){
 return value.match(new RegExp( param ));
};  
});


//start mob no with 7-9
jQuery.validator.addMethod("startFrom", function (value, element,param) {
if(value.match(new RegExp( param ))){
 return value.match(new RegExp( param ));
};  
});



$(function() {
    // Setup form validation on the #register-form element
    $("#register-form").validate({
 rules: {
	 order: { 
		 required: true,
         number: true,
        
	 },
     date: 
     {
    	 required:true,
         
    	 
     },
     username : "required",
     department : "required",
     mob : { 
		 required: true,
		 startFrom:/^[7-9]\d+$/,
		 minlength:10,
         matchMob : "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}",
        
	 },
	 houseLandline: {
    	 minlength:10,
    	 matchMob : function(){
    		 if($('#houseLandline').val()!==""){
    		return "(?![1]{10}|[2]{10}|[3]{10}|[4]{10}|[5]{10}|[6]{10}|[7]{10}|[8]{10}|[9]{10}|[0]{10})[0-9]{10}";
    		 }
    	 }
    	 },
     work : "required",
    // workservice : "required", 
     description : "required",
     nature : "required"
     },
     
     // Specify the validation error messages
     messages: {
    	 order: {
    		 required:"Please enter work order no",
    		
    		
    	 },
         date: 
         {
        	required: "Please enter Date"
         },
         username : "Please enter  username",
         department : "Please enter your department",
         mob : 
       {
           required:"Please enter mobile no",
           startFrom : "Please start mobile number either from 7,8 or 9",
           minlength:"Please enter exact 10 digits",
           matchMob : "Please do not enter continuous number",
		     },
     houseLandline: {
            	 minlength:"Please enter 10 digits",
            	 matchMob : "Please do not enter continuous number",
             },
         work : "Please enter work location",
        // workservice : "Please select nature of work",
         description : "Please enter problem description",
         nature : "Please select nature of work"
         
     }


   

    
 });

});

