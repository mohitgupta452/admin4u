/**
 * @author Manoj Singh
 */


// product Requisition table data  starts
$(document).ready(function(){

	var url="/admin4u/rest/data/getproductreqdata";
	 $.getJSON(url, function(result){
		 $('#mytrtd').html("");
		 var count =1;
		 var t = JSON.stringify(result);
		 var tp = JSON.parse(t);
	 $.each(tp,function(i,key){
		 
	 var tpreq = JSON.parse(tp[i]['requestData']);	
	      getProductDetail(tpreq);
		 }); 
	 });

});

function getProductDetail(tpreq)
{
	$.ajax({
		
		url:"/admin4u/rest/data/getproductdetail/"+tpreq['inventory.productid1'],
		 success: function (data,textStatus, $XHR) {
	 $.each(data,function(key,value){
	      $('#mytrtd').append('<tr>'
		            +'<td >'+value.categary.categary+'</td>'
		            +'<td>'+value.name+'</td>'
		            +'<td>'+value.brand+'</td>'
		            +'<td>'+tpreq['inventory.purpose1']+'</td>'
		            +'<td>'+tpreq['inventory.quanity1']+'</td>'
		            +'</tr>');

      });
			 
		 }
	});
}

// ends 


	    $(document).ready(function() {
	        ProductRequisition.init();
	    });
	    
	    
	    brandHtml='<option value ="">Select Brand</option>';
	    productsHtml='<option value ="">Select Product</option>';
	    products="";
	    categories="";
	    function getBrandByCat(categoryID,counter)
	    {
	    	//$('#productName'+counter).val("");
	    	//alert(categoryID);
	    	//alert(categoryText);
	    
	    	brandHtml='<option value ="">Select Brand</option>';
	    	
	    	  categoryId= categoryID.split("|")[0];
	    	  categoryName=categoryID.split("|")[1];
	    	 // console.log(categoryId,Category);
	    		    	//alert( $('productName'+counter).val(categoryText));
	    	 // alert(categoryId);
	    	 // alert(categoryName);
	    	console.log("get product by category");
	    	var url="/admin4u/rest/data/getprodbycategory/"+categoryId;
	    	 $.getJSON(url, function(result){
	    		 products=result;
	    	        $.each(result, function(key, val)
	    	        		{
	    	        		brandHtml+='<option value="' +val.brand + '">'+val.brand+'</option>';
	    	        	console.log(val);
	    	        });
	    	           
	    	        $('#categoryName').val(categoryName);
	    	        
	    	        abledAddRowButtuon(counter);
	    	        $("#reqPdtBrand"+counter).html(brandHtml);
	    	 });
	    	}
	    
	    
	    function getProdByBrand(brand,counter)
	    {
	    	productsHtml='<option value ="">Select Product</option>';
	    	console.log("get product by brand");
	  	
	    	 $.each(products, function(key, val)
	         		{
	    		 if(val.brand === brand)
	    			 {
	    			 productsHtml+='<option value="' +val.id +'">'+val.name+'</option>';
	    			 console.log(brand,val.brand)
	    			 }
	         	console.log(val);	
	         });
	    	abledAddRowButtuon(counter);
	         $("#reqPdtName"+counter).html(productsHtml);
	    }

	    var a=0;
	    var category="";
	    function getData(){
	                	$('#total').val(a);
	                	return true;
	                }

	    var category="";
	    var a=0;
	    function getData(){
	                	$('#total').val(a);
	                	return true;
	                }
	    
function getAllcategory(counter)
    	{
    		console.log("getall category called");
    		var url="/admin4u/rest/data/getallcategory";		
    	    $.getJSON(url, function(result){
    	    	$('#reqPdtCat'+counter).html(""); 
    	    	$('#reqPdtCat'+counter).append('<option value="">Select Category</option>');
    	        $.each(result, function(key, val){

        $('#reqPdtCat'+counter).append('<option value="' +val.descendant.id+ '">'+val.descendant['categary']+'</option>');
    	        	
    	        	console.log(val);
    	        	console.log(val.descendant['categary']);
    	        });
    	       
    	    });
    	}
	    
	    
	    
var ProductRequisition = function () {

    return {

        //main function to initiate the module
      
        init: function () {
        	
        	
/*        	function getAllcategory()
        	{
        		console.log("getall category called");
        		var url="/admin4u/rest/data/getallcategory";		
        	    $.getJSON(url, function(result){
        	        $.each(result, function(key, val){
        	        	
        	        	categories=result;
            category+='<option value="' +val.descendant.id+"|"+val.descendant['categary']+'">'+val.descendant['categary']+'</option>';
        	        	
        	        	console.log(val);
        	        	console.log(val.category);
        	        });
        	       
        	    });
        	}*/
             
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }
            
            function editRow(oTable, nRow ,Counter) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                
                jqTds[0].innerHTML = '<select onchange="getBrandByCat(this.value,'+Counter+'); abledAddRowButtuon('+Counter+')" class="form-control small select2 validatefunc" name="reqPdtCat'+Counter+'"'+'id="reqPdtCat'+Counter+'"'+'style="width: 100%;"></select><span id="error_msg_cat'+Counter+'" style="color:red"></span>';
                jqTds[1].innerHTML = '<select onchange="getProdByBrand(this.value,' +Counter+') ; abledAddRowButtuon('+Counter+')" class="form-control small select2" name="reqPdtBrand'+Counter+'"'+'id="reqPdtBrand'+Counter+'"'+'style="width: 100%;"></select><span id="error_msg_brand'+Counter+'" style="color:red"></span>';
                jqTds[2].innerHTML = '<select onchange="abledAddRowButtuon('+Counter+')" class="form-control small select2" name="reqPdtName'+Counter+'"'+'id="reqPdtName'+Counter+'"'+'style="width: 100%;"></select><span id="error_msg_name'+Counter+'" style="color:red"></span>';
                jqTds[3].innerHTML = '<select onchange="abledAddRowButtuon('+Counter+')" class="form-control small select2" name="reqPdtPurpose'+Counter+'"'+'id="reqPdtPurpose'+Counter+'"'+'style="width: 100%;"><option value="">Select purpose</option><option value="1">purpose</option></select><span id="error_msg_purpose'+Counter+'" style="color:red"></span>';
                jqTds[4].innerHTML = '<input  onkeyup="abledAddRowButtuon('+Counter+')" type="text" class="form-control small" name="reqPdtQty'+Counter+'"'+'id="reqPdtQty'+Counter+'">'+'<span id="error_msg_qty'+Counter+'" style="color:red"></span>';
                jqTds[5].innerHTML = '<input  type="hidden" readonly  class="form-control small" name="productName'+Counter+'"'+'id="productName'+Counter+'" style="display:none">';
                jqTds[6].innerHTML = '<input  type="hidden" readonly  class="form-control small" name="brand'+Counter+'"'+'id="brand'+Counter+'style="display:none">';
                jqTds[7].innerHTML = '<input  type="hidden" readonly  class="form-control small" name="categoryId'+Counter+'"'+'id="categoryId'+Counter+'style="display:none">';
                
                a=Counter;   
            }
            var oTable = $('#product_req_editable').dataTable({
                "aLengthMenu": [
                    [-1],
                    ["All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": -1,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }
                ]
            });

            jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

            var nEditing = null;
            var globalCounter = 0;
            if(globalCounter == 0)
               
            $('#product-requistion_new').click(function (e) {
            $('#product-requistion_new').attr('disabled' , true);
                globalCounter++;
                getAllcategory(globalCounter);
               
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '',
                        '', ''
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow ,globalCounter);
                nEditing = nRow;
            });
         }
    };
}();


function abledAddRowButtuon(counter)
{
 $('#categoryId'+counter).val($('#reqPdtCat'+counter).find('option:selected').text());
 $('#brand'+counter).val($('#reqPdtBrand'+counter).find('option:selected').text());
 $('#productName'+counter).val($('#reqPdtName'+counter).find('option:selected').text());
	
	
	if($('#reqPdtCat'+counter).val() !=="" && $('#reqPdtBrand'+counter).val() !=="" && $('#reqPdtName'+counter).val()!=="" && $('#reqPdtPurpose'+counter).val()!=="" && $('#reqPdtQty'+counter).val()!=="")
{
	$('#product-requistion_new').attr('disabled' , false);
}	
else
	{
	$('#product-requistion_new').attr('disabled' , true);
	}
	
	if($('#reqPdtCat'+counter).val() !=="")
	{  $('#error_msg_cat'+counter).html("");}
	if ($('#reqPdtBrand'+counter).val() !=="")
	{	  $('#error_msg_brand'+counter).html("");}
	if ($('#reqPdtName'+counter).val() !=="")
	{	  $('#error_msg_name'+counter).html(""); }
	if($('#reqPdtPurpose'+counter).val() !=="")
	{  $('#error_msg_purpose'+counter).html("");	}
	if($('#reqPdtQty'+counter).val() !=="")
	{	  $('#error_msg_qty'+counter).html("");}
}


$('#pdtReqSubmit').on('click' , function(){
var counter = $('#product_req_editable >tbody >tr').length;
for (var i =1; i<=counter ; i++)
	{
if($('#reqPdtCat'+i).val() ==="")
{  $('#error_msg_cat'+i).html("please select category");}
if ($('#reqPdtBrand'+i).val() ==="")
{	  $('#error_msg_brand'+i).html("please select brand");}
if ($('#reqPdtName'+i).val() ==="")
{	  $('#error_msg_name'+i).html("please select name"); }
if($('#reqPdtPurpose'+i).val() ==="")
{  $('#error_msg_purpose'+i).html("please select purpose");	}
if($('#reqPdtQty'+i).val() ==="")
{	  $('#error_msg_qty'+i).html("please enter quantity");}
   }
});
