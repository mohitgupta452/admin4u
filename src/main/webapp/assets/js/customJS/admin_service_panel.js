function addSubCat()
{
	var jsonData ={}

	$(".addsubcat").each(function() {
        var name = $(this).attr("name");
        var value = $(this).val();
        jsonData[name] = value;
    })
    
	$.ajax({
		url:"/admin4u/rest/update/addsubcategory",
		type:"post",
		data: JSON.stringify(jsonData),
	      dataType: "json",
	      ContentType: "application/json",
		 success: function (data,textStatus, $XHR) {
				 cathtml='<option value="'+data.descendant.id+'">'+data.descendant.categary+'</option>';
			 $("#itemCatMaster").append(cathtml); 
		 }
	});
	}



function getSubCategory(ancestor)
{
	cathtml="";
	$.ajax({
		url:"/admin4u/rest/data/getsubcategory/"+ancestor,
		type:"get",
		 success: function (data,textStatus, $XHR) {
			 $.each(data,function(key,value){
				 
				 cathtml+='<option value="'+value.descendant.id+'">'+value.descendant.categary+'</option>';
			 });
			 $("#itemCatMaster").html(cathtml); 
		 }
	});
}

function callSetOnOff(serviceName,thisID)
{
		var isActive;
		if(thisID.checked)
			isActive=1;
		else
			isActive=0;
		//alert(thisID);
		var url="AdminServicePanelServlet?action=manageServices&serviceName="+serviceName+"&isActive="+isActive;
		
		$('.alert').remove();
		
		$.getJSON(url, function(result){
			

			showAlertMessage("SUCCESS",result['success']);
			
	      
		});
		
}

//end Ajax function for showing request Data on modal on click of request id




// approved and non approved designation

function nonApprovalDesignation(action)
{
	if(action==='add')
	{
	var selectedValues = [];    
    $(".dd option:selected").each(function(){
        selectedValues.push($(this).val()); 
    });
    $(".dd option:selected").remove().appendTo($(".nadd"));
    isActive =1;
	}
 if(action === 'remove')
 {
	 var selectedValues = [];    
	    $(".nadd option:selected").each(function(){
	        selectedValues.push($(this).val()); 
	    });
	    $(".nadd option:selected").remove().appendTo($(".dd"));
	    isActive =0;
}
	var url="AdminServicePanelServlet?action=manageDesignation&designation="+JSON.stringify(selectedValues)+"&isActive="+isActive; 

	$('.alert').remove();
	
	$.getJSON(url, function(result){

		showAlertMessage("SUCCESS",result['success']);
	});
		
}



$(document).ready(function(){
	$('#buildingName').on('change',function(){
	
	var url="SMSpaceAllocationServlet";
	var buildingId=$('#buildingName').val();
	
	var action="FLOORINFO";
	var jsonData={
			buildingId:buildingId,
			action:action,
	}
	
	$.ajax({
	      url: url,
	      type: 'POST',
	      data: jsonData,
	      dataType: "json",
	      ContentType: "application/json",
	      success: function(result){
	    	  $('#buildingFloor').html('<option value="">Select...</option>');
	    	  $.each(result,function(key,val){
		    	  console.log("key"+key+"  value "+val);

            	$('#buildingFloor').append('<option value="'+key+'">'+val+'</option>');
            });
              

	            
	        }});
	
	});
	
});

$(document).ready(function(){
	$('#manageSpaceForm').validate({
		rules:{
			building:"required",
			buildingFloor:"required",
			spaceCategory:"required",
			spaceAvail:{required:true,digits:true},
			buildingPattern:"required",
			startRange:{required:true,digits:true},
			endRange:"required",
		},
		messages:{
			
		},
		
		
	});
});


$(document).ready(function(){
	$('#manageBuildingForm').validate({
		rules:{
			buildingName:"required",
			buildingLocation:"required",
			buildingFloor:"required"
			
		},
		messages:{
			
		},
		
		
	});
});

$(document).ready(function(){
	
	$('#spaceStRange').on('blur',function(){
		$('#spaceEnRange').val(parseInt($(this).val())+parseInt($('#spaceAvail').val())-1);
	});
	
});


//  data for table
function getProductData(){
	
	$.ajax({
		url:"/admin4u/rest/data/getproductdetails",
		 success: function (data,textStatus, $XHR) {
			 $('#mytbody').html(""); 
	 $.each(data,function(key,value){
	 $('#mytbody').append('<tr><td>'+value.name+'</td>'
			 +'<td>'+value.categary.categary+'</td>'
			 +'<td>'+value.brand+'</td>'
			 +'<td>'+value.name+'</td>'
			 +'<td>'+value.unit+'</td>'
			 +'<td>'+value.price+'</td>'
			 +'</tr>');
      });
			 
		 }
	});
	
}

 function deletePolicy(id)
{
	 $.ajax({
	 url:"PolicyUpload?policyId="+id+"&action=DELETEPOLICY",
	 type:"get",
		 success: function (data,textStatus, $XHR) {
			 
			 console.log("data"+data);
		 }
	});
	}

 
//filesize of upload pdf should be 10MB
 $.validator.addMethod('filesize', function(value, element, param) {
     // param = size (in bytes) 
     // element = element to validate (<input>)
     // value = value of the element (file name)
     return this.optional(element) || (element.files[0].size <= param) 
 });
 
 
 // validation in manage reference Document
 
 $(function() { 
 $('#refUpload').validate({
	 rules:{
		 upload : {
			 extension: "pdf",
             filesize:5000000
		 } 
	 },
	 messages:{
		 upload :{
		 extension: "Please select only pdf file",
         filesize:"File size must be less than 5MB"
		 } 
	 }
 });
 });

 
// remove policyDoc tr  
function deletePolicy(policyId)
{
      $('#deletePolicy'+policyId).closest ('tr').remove()
}




