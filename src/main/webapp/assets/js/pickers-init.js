//date picker start

if (top.location != location) {
    top.location.href = document.location.href ;
}
$(function(){
    window.prettyPrint && prettyPrint();
    $('.default-date-picker').datepicker({
        format: 'mm-dd-yyyy',
        minDate:0
    });
    $('.dpYears').datepicker();
    $('.dpMonths').datepicker();


    var startDate = new Date(2012,1,20);
    var endDate = new Date(2012,1,25);
    $('.dp4').datepicker()
        .on('changeDate', function(ev){
            if (ev.date.valueOf() > endDate.valueOf()){
                $('.alert').show().find('strong').text('The start date can not be greater then the end date');
            } else {
                $('.alert').hide();
                startDate = new Date(ev.date);
                $('#startDate').text($('.dp4').data('date'));
            }
            $('.dp4').datepicker('hide');
        });
    $('.dp5').datepicker()
        .on('changeDate', function(ev){
            if (ev.date.valueOf() < startDate.valueOf()){
                $('.alert').show().find('strong').text('The end date can not be less then the start date');
            } else {
                $('.alert').hide();
                endDate = new Date(ev.date);
                $('.endDate').text($('.dp5').data('date'));
            }
            $('.dp5').datepicker('hide');
        });

    // disabling dates
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('.dpd1').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('.dpd2')[0].focus();
        }).data('datepicker');
    var checkout = $('.dpd2').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
});


$('.datepicker tbody').on('click', function(){  $('.datepicker').hide() });

//# pickUp Date&Time and End Date&Time in pick up only

//var numberOfMinutes = 5;
var d = new Date();
d.setMinutes(d.getMinutes() + 5);
$(".form_datetime-component").datetimepicker({
language:  'en',
weekStart: 1,
autoclose: true,
todayHighlight: 1,
startView: 2,
forceParse: 0,
startDate: d,
pickerPosition : 'top-left'

});


$('.form_datetime-component')
.datetimepicker({ autoclose: true})
.on('changeDate', function(ev){

	$('#pick4uEndDateTime').val("");
	var d = new Date();
	d.setMinutes(d.getMinutes() + 5);
	$(".form_datetime-component").datetimepicker({  pickerPosition : 'top-left' , autoclose: "true"}).datetimepicker('setStartDate', d ).focus();
	
	
	var pickUp = $('#pick4uPickupDateTime').val();

	var pd = new Date(pickUp);
	var diff = pd.valueOf()-d.valueOf();
	var hours = diff/1000/60/60; 
	if(hours < 2)
	{
		$('#cab_pickup_modal').modal('show');
	}
	
	
	//alert(pd);
	//alert(pd);
	//var numberOfHrs = 5;
	//var numberOfMin =30;
   // var ed = pd;
	//pd.setHours(pd.getHours()-numberOfHrs);
	//pd.setMinutes(pd.getMinutes()-numberOfMin);
	pd.setMinutes(pd.getMinutes()+5);
	//alert(pd);
	
	$('.form_datetime-component2').datetimepicker({pickerPosition : 'top-left',autoclose: true}).datetimepicker('setStartDate', pd);
	
    if (ev.date.valueOf() <= pd.valueOf()){
    
    	$(".form_datetime-component2").datetimepicker(
    {
    	language:  'en',
        weekStart: 1,
    	autoclose: true,
    	todayHighlight: 1,
    	startView: 2,
        forceParse: 0,
    	startDate: pd,
    	});
    }
});

// pickup date&time and end date&time  in pickup ends


//pickup date&time and end date&time  in droppoint starts
   
var dd = new Date();
dd.setMinutes(dd.getMinutes() + 5);
$(".drop_form_datetime-component").datetimepicker({
language:  'en',
weekStart: 1,
autoclose: true,
todayHighlight: 1,
startView: 2,
forceParse: 0,
startDate: dd,
pickerPosition:'top-left'

});


$('.drop_form_datetime-component')
.datetimepicker({ autoclose: true})
.on('changeDate', function(ev){

	//$('#cab_drop_date').val("");
	var dd = new Date();
	dd.setMinutes(dd.getMinutes() + 5);
	$(".drop_form_datetime-component").datetimepicker({pickerPosition:'top-left' , autoclose: true}).datetimepicker('setStartDate', dd ).focus();
	
	
	var pickUp = $('#cab_drop_pick').val();
	
	var pd = new Date(pickUp);
	var diff = pd.valueOf()-d.valueOf();
	var hours = diff/1000/60/60; 
	if(hours < 2)
	{
		$('#cab_pickup_modal').modal('show');
	}
	
	//alert(pd);
	//alert(pd);
	//var numberOfHrs = 5;
	//var numberOfMin =30;
   // var ed = pd;
	//pd.setHours(pd.getHours()-numberOfHrs);
	//pd.setMinutes(pd.getMinutes()-numberOfMin);
	pd.setMinutes(pd.getMinutes()+5);
	//alert(pd);
	
	/*$('.drop_form_datetime-component2').datetimepicker({ autoclose: true}).datetimepicker('setStartDate', pd);
	
    if (ev.date.valueOf() <= pd.valueOf()){
    
    	$(".drop_form_datetime-component2").datetimepicker(
    {
    	language:  'en',
        weekStart: 1,
    	autoclose: true,
    	todayHighlight: 1,
    	startView: 2,
        forceParse: 0,
    	startDate: pd,
    	});
    }*/
});


//pickup date&time and end date&time  in droppoint ends





//pickup date&time and end date&time  in disposal starts

var disd = new Date();
disd.setMinutes(dd.getMinutes() + 5);
$(".disposal_form_datetime-component").datetimepicker({
language:  'en',
weekStart: 1,
autoclose: true,
todayHighlight: 1,
startView: 2,
forceParse: 0,
startDate: disd,
pickerPosition : 'top-left'

});


$('.disposal_form_datetime-component')
.datetimepicker({ autoclose: true})
.on('changeDate', function(ev){

	$('#cab_disposal_end').val("");
	var dd = new Date();
	dd.setMinutes(dd.getMinutes() + 5);
	$(".disposal_form_datetime-component").datetimepicker({pickerPosition:'top-left' , autoclose: true}).datetimepicker('setStartDate', dd ).focus();
	
	
	var pickUp = $('#cab_disposal_pick').val();
	
	var pd = new Date(pickUp);
	var diff = pd.valueOf()-d.valueOf();
	var hours = diff/1000/60/60; 
	if(hours < 2)
	{
		$('#cab_pickup_modal').modal('show');
	}
	//alert(pd);
	//alert(pd);
	//var numberOfHrs = 5;
	//var numberOfMin =30;
   // var ed = pd;
	//pd.setHours(pd.getHours()-numberOfHrs);
	//pd.setMinutes(pd.getMinutes()-numberOfMin);
	pd.setMinutes(pd.getMinutes()+5);
	//alert(pd);
	
	$('.disposal_form_datetime-component2').datetimepicker({pickerPosition : 'top-left', autoclose: true}).datetimepicker('setStartDate', pd).focus();
	
    if (ev.date.valueOf() <= pd.valueOf()){
    
    	$(".disposal_form_datetime-component2").datetimepicker(
    {
    	language:  'en',
        weekStart: 1,
    	autoclose: true,
    	todayHighlight: 1,
    	startView: 2,
        forceParse: 0,
    	startDate: pd,
    	});
    }
});


//pickup date&time and end date&time  in disposal ends




//# Hotel guest house checkIn Date&time and CheckOut Date&Time

//var numberOfMinutes = 5;


var cd = new Date();
cd.setMinutes(cd.getMinutes() + 5);
$(".checkIn_form_datetime-component").datetimepicker({
	
language:  'en',
weekStart: 1,
//todayBtn:  1,
autoclose: true,
todayHighlight: 1,
startView: 2,
forceParse: 0,
startDate: cd,


});

$('.checkIn_form_datetime-component')
.datetimepicker({ autoclose: true})
.on('changeDate', function(ev){

	$('#checkoutDateTime').val("");
	
	var cd = new Date();
	cd.setMinutes(cd.getMinutes() + 5);
	$(".checkIn_form_datetime-component").datetimepicker({ autoclose: "true"}).datetimepicker('setStartDate', cd ).focus();
	
	// when diff bw current date and travel date is less than 24 hrs 
	//then not confirmed pop up will be generated	
	
	var checkIn = $('#checkinDateTime').val();
	var diff =  new Date(checkIn).valueOf() - new Date().valueOf() ;
	var hours = diff/1000/60/60;
	if(hours < 24)
		{
   $('#checkInCheckOutModal').modal('show');
		}
	
	// ends
	
	//var pd = ev.date;
	//alert(pd);
	var pd =  new Date(checkIn);
	//alert(pd);
	//var numberOfHrs = 5;
	//var numberOfMin =30;
   // var ed = pd;	
	//pd.setHours(pd.getHours()-numberOfHrs);
    //  pd.setMinutes(pd.getMinutes()-numberOfMin);
	pd.setMinutes(pd.getMinutes()+5);
	//alert(pd);
	
	$('.checkOut_form_datetime-component').datetimepicker({ autoclose: true}).datetimepicker('setStartDate', pd ).focus();
    
    if (ev.date.valueOf() <= pd.valueOf())
    {
    	$(".checkOut_form_datetime-component").datetimepicker(
      {
    	
    	language:  'en',
        weekStart: 1,
    	autoclose: true,
    	todayHighlight: 1,
    	startView: 2,
      	forceParse: 0,
        startDate: pd

    	});

    }

});

//hotel guest house checkin checkout datetimepicker ends



//checkin  & checkout Date in admin table (BookTicket) starts

//var numberOfMinutes = 5;
var cid = new Date();
cid.setMinutes(d.getMinutes() + 5);
$(".checkIn_admin_form_datetime-component").datetimepicker({
language:  'en',
weekStart: 1,
autoclose: true,
todayHighlight: 1,
startView: 2,
forceParse: 0,
startDate: cid

});


$('.checkIn_admin_form_datetime-component')
.datetimepicker({ autoclose: true})
.on('changeDate', function(ev){

	var actionID = $('#actionId').val();
	var cid = new Date();
	cid.setMinutes(cid.getMinutes() + 5);
	$(".checkIn_admin_form_datetime-component").datetimepicker('setStartDate', cid ).focus();
	
	var pickUp = $('#checkInDateTime'+actionID).val();
	var pd = new Date(pickUp);
	pd.setMinutes(pd.getMinutes()+5);
	
	$('.checkOut_admin_form_datetime-component').datetimepicker({ autoclose: true}).datetimepicker('setStartDate', pd);
	
  if (ev.date.valueOf() <= pd.valueOf()){
  
  	$(".checkOut_admin_form_datetime-component").datetimepicker(
  {
  	language:  'en',
    weekStart: 1,
  	autoclose: true,
  	todayHighlight: 1,
  	startView: 2,
    forceParse: 0,
  	startDate: pd,
  	});
  }
});

// checkin  & checkout Date in admin table (BookTicket) ends


$(".form_datetime-adv").datetimepicker({
    format: "dd-mm-yyyy - hh:ii",
    autoclose: true,
    todayBtn: true,
    startDate: "2013-02-14 10:00",
    minuteStep: 10
});

$(".form_datetime-meridian").datetimepicker({
    format: "dd MM yyyy - HH:ii P",
    showMeridian: true,
    autoclose: true,
    todayBtn: true
});

//datetime picker end

//timepicker start
$('.timepicker-default').timepicker();


$('.timepicker-24').timepicker({
    autoclose: true,
    minuteStep: 1,
    showSeconds: true,
    showMeridian: false
});

//timepicker end

//colorpicker start

$('.colorpicker-default').colorpicker({
    format: 'hex'
});
$('.colorpicker-rgba').colorpicker();

//colorpicker end



