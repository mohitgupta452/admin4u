package com.admin4u.filters;


import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class SessionFilter
 *
 * @author Ashish Singh Dev
 */
@WebFilter(description = "Session Filter", urlPatterns = { "/*" })
public class SessionFilter implements Filter {

	private static final String AJAX_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<partial-response><redirect url=\"%s\"></redirect></partial-response>";
	
	private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(new HashSet<>(
	        Arrays.asList("/login.jsp", "/loginservlet", "/LogoutServlet","/locationmapping.jsp")));

	public SessionFilter() {
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpSession session = request.getSession(false);
		String loginURL = request.getContextPath() + "/login.jsp";

		boolean loggedIn = (session != null) && (session.getAttribute("userID") != null);
		boolean loginRequest = request.getRequestURI().equals(loginURL);
		String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$", "");
		 boolean allowedPath = ALLOWED_PATHS.contains(path);
		
		boolean resourceRequest = request.getRequestURI()
				.startsWith(request.getContextPath() +"/assets");
		boolean ajaxRequest = "XMLHttpRequest".equals(request.getHeader("X-Requested-With")) ;

		if (loggedIn || loginRequest || resourceRequest ||allowedPath) {
			if (!resourceRequest) {
				response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
				response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
				response.setDateHeader("Expires", 0); // Proxies.
			}

			chain.doFilter(request, response); // So, just continue request.
		} else if (ajaxRequest) {
			response.setContentType("text/xml");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().printf(AJAX_REDIRECT_XML, loginURL);
		} else {
			response.sendRedirect(loginURL);
		}
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}

}
