package com.admin4u.filters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.service.bussiness.adstoreUtil.ADStore;
import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.util.enums.Roles;
import com.admin4u.views.RoleBaseDashboardBean;
import com.admin4u.views.UserSessionBean;

//@WebFilter(urlPatterns="/user/*")
public class RoleBasedAccess implements Filter {

	private Logger log= LoggerFactory.getLogger(getClass());

	@Inject 
	UserSessionBean userSessionBean;
	
	@Inject
	private ADStore adStore;
	
	
	List<String> roleList=new ArrayList<String>();
	
	List<String>restrictedUrlList;
	
	List<String>authenticatedUrlList;
	
	
	public RoleBasedAccess() {
		restrictedUrlList=new ArrayList<>();
		restrictedUrlList.add("approvalauthority.jsp");
		restrictedUrlList.add("admintable.jsp");
		restrictedUrlList.add("admin_panel.jsp");
		restrictedUrlList.add("role_management.jsp");
		restrictedUrlList.add("spoc.jsp");
		restrictedUrlList.add("manager_dropdown.jsp");
		restrictedUrlList.add("role_management.jsp");
		restrictedUrlList.add("cost_center_mapping.jsp");
		restrictedUrlList.add("priorityTatMap.jsp");
		
				
		
		
		
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		
			
		 

		
		 
		

		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		
		
		authenticatedUrlList=new ArrayList<>();
		
		
		 roleList=userSessionBean.getEmpRole();
			
		   if(adStore.isManager(userSessionBean.getEmpData().getUserPrincipalName()))
		 {
				authenticatedUrlList.add("approvalauthority.jsp");
				
			}
		 for(String role:roleList){

		if("Admin".equalsIgnoreCase(role))
		{
			authenticatedUrlList.add("admintable.jsp");
			authenticatedUrlList.add("admin_panel.jsp");
			authenticatedUrlList.add("role_management.jsp");
		  //authenticatedUrlList.put("Drop Down Manager","user/manager_dropdown.jsp");
		}
		 if("Spoc".equalsIgnoreCase(role) || Roles.OTHERSPOC.toString().equalsIgnoreCase(role))
		{
			authenticatedUrlList.add("spoc.jsp");
			
		}
		 
		 if(Roles.APPLICATION_ADMIN.toString().equalsIgnoreCase(role))
			{
				authenticatedUrlList.add("manager_dropdown.jsp");
				authenticatedUrlList.add("role_management.jsp");
				authenticatedUrlList.add("cost_center_mapping.jsp");
				authenticatedUrlList.add("priorityTatMap.jsp");


			}
		
		
		 }
				
		
		
		try {

			HttpServletRequest reqt = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			HttpSession ses = reqt.getSession(false);

			String reqURI = reqt.getRequestURI();
			File file=new File(reqURI);
			String jspFileName=file.getName();
			log.debug("Filter called   {}",reqURI);
			if(restrictedUrlList.contains(jspFileName)&& ses!=null){
			boolean isAuthenticatedURL=false;
			if(authenticatedUrlList.contains(jspFileName))
				isAuthenticatedURL=true;
				else
				isAuthenticatedURL=false;
				log.debug("inside get jsp file name  {}",isAuthenticatedURL);
					
				if(isAuthenticatedURL){
					chain.doFilter(request, response);
				}
				else
				{
					log.debug("inside 404.htl  ");

					resp.sendRedirect(reqt.getContextPath() + "/user/notFound404.jsp");

				}
					
				
			}
						
			 else{
					chain.doFilter(request, response);

			 }
		} catch (Exception e) {
		}

		
	}

	@Override
	public void destroy() {

	}
}