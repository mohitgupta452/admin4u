package com.admin4u.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class Test
 */
//@WebFilter(urlPatterns="*")

public class Test implements Filter {
    public Test() {
    }
    
	public void destroy() {
	}
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		String loginURL = request.getContextPath() + "/login.jsp";
		String AJAX_REDIRECT_XML = "301";
		boolean ajaxRequest = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
		HttpSession session = request.getSession(false);
		if (ajaxRequest) {
			System.out.println("inside ajax request ");
			response.setContentType("text/xml");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write("{status: 301}");
		}
		else
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
