package com.admin4u.filters;


import java.lang.invoke.MethodHandles;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.persistence.CrudService;

@WebListener
public class HttpSessionClass implements HttpSessionListener {

	private static final Logger log = LoggerFactory.getLogger(HttpSessionClass.class);

	@Inject
	private CrudService crudService;

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		// TODO Auto-generated method stub

	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		
		// TODO Auto-generated method stub
		/*String loginUpn = (String) se.getSession().getAttribute("userID");
		log.info("Session destroyed for upn : " + loginUpn);
		List<LoginDetails> details = crudService.findWithNamedQuery("LoginDetails.findAllBYUpn",
				QueryParameter.with("upn", loginUpn).parameters());
		if (!(details.isEmpty())) {
			for (LoginDetails ld : details)
				crudService.delete(LoginDetails.class, ld.getID());
		}*/

	}

}
