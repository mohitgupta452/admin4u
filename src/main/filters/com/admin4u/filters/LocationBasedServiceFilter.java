package com.admin4u.filters;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admin4u.service.bussiness.contract.LocationManger;
import com.admin4u.views.UserSessionBean;

@WebFilter(filterName = "locationBasedServiceFilter")
public class LocationBasedServiceFilter implements Filter {

	private Logger log= LoggerFactory.getLogger(getClass());

	@Inject 
	private LocationManger locationManger;
	
	@Inject 
	private UserSessionBean userSession;
	
	private Map<String,String> serviceURLMap;
	
	public LocationBasedServiceFilter() {
		
		serviceURLMap=new HashMap<String,String>();
		serviceURLMap.put("tour_travel.jsp","TAT");
		serviceURLMap.put("hotel_guesthouse.jsp","HOTEL");
		serviceURLMap.put("repair_maintenance.jsp","REPAIR_MAINTENANCE");
		serviceURLMap.put("house_keeping.jsp","HOUSEKEEPING");

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {

			HttpServletRequest reqt = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			HttpSession ses = reqt.getSession(false);

			String reqURI = reqt.getRequestURI();
			File file=new File(reqURI);
			String jspFileName=file.getName();
			
			log.debug("Filter called   {}",reqURI);
			if(serviceURLMap.get(jspFileName)!=null){
				boolean isAuthenticatedURL;
				if(jspFileName.equalsIgnoreCase("tour_travel.jsp")){
					boolean isAir=locationManger.getServiceStatusByLocation("AIRTICKET",userSession.getLocationName());
					boolean isCab=locationManger.getServiceStatusByLocation("CAB",userSession.getLocationName());
					boolean isTrain=locationManger.getServiceStatusByLocation("TRAINTICKET",userSession.getLocationName());
					if(isAir || isCab || isTrain)
						isAuthenticatedURL=true;
					else
					isAuthenticatedURL=false;	
					
				}
				else
					 isAuthenticatedURL=locationManger.getServiceStatusByLocation(serviceURLMap.get(jspFileName),userSession.getLocationName());
				log.debug("inside get jsp file name  {}",isAuthenticatedURL);
					
				if(isAuthenticatedURL){
					chain.doFilter(request, response);
				}
				else
				{
					log.debug("inside 404.htl  ");

					resp.sendRedirect(reqt.getContextPath() + "/user/notFound404.jsp");

				}
					
				
			}
						
			 else{
					chain.doFilter(request, response);

			 }
		} catch (Exception e) {
		}
	}

	@Override
	public void destroy() {

	}
}